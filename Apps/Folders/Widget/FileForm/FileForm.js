
var FileForm = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
FileForm.Init = function(){
    Event.AddById("btnSaveFile", "click", FileForm.SendForm);
};

/***
* Obtient l'id de l'itinéraire courant 
*/
FileForm.GetId = function(){
    return Form.GetId(" fileForm");
};

/*
* Sauvegare l'itineraire
*/
FileForm.SendForm = function(e){
   
    if(Form.IsValid("fileForm"))
    {

    var data = "Class=Folders&Methode=SaveFile&App=Folders";
        data +=  Form.Serialize("fileForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.statut == "Error"){
                Form.RenderError("fileForm", data.message);
            } else{

                Form.SetId("fileForm", data.data.Id);
                Dialog.Close();
                
                Folders.GetDirectory(Folders.CurrentFolderId);
            }
        });
    }
};


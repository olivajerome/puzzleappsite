<section class='container'>

    {{GetControl(Hidden,annonce,{Value={{Annonce->IdEntite}},Id=annonceId,)})}}

    <h1>{{GetCode(Annonce.Annonce)}}
        {{Annonce->Title->Value}}
    </h1>

    <div>
        <div class='col-md-2' id='imageContainer'>
            {{Apps\Annonce\Decorator\AnnonceDecorator->RenderImages(Annonce)}}
        </div>
        <div class='col-md-10'>
            <p>{{Annonce->Description->Value}}</p>
        </div>

    </div>    

    
    <div class='marginTop col-md-12'> 
     {{votePlugin}}    
     
     {{registerPlugin}}
    </div> 
     
        
    <div class='marginTop col-md-12'>        
        {{if ShowAddReponse == true}}   

        <h2>{{GetCode(Annonce.AddYourReponse)}}</h2>
  
        {{ResponseForm}}
        {{/if ShowAddReponse == true}}

        {{if ShowAddReponse == false}}   
        {{GetCode(Annonce.MustBeConnectedToRespond)}}
        {{/if ShowAddReponse == false}}
    </div>


</section>

   
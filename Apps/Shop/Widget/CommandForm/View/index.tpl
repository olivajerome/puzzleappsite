<h2>
    <ul class='breadCrumb'>
        <li class='active'>1.{{GetCode(Shop.CartDetail)}}</li>
        <li> 2.{{GetCode(Shop.Address)}}</li> 
        <li>3.{{GetCode(Shop.Payment)}}</li>
    </ul>    
</h2>

<div class='col-md-12' id='step1'>
    <h3>{{GetCode(Shop.Summary)}}</h3>
    {{table}}
</div>

<div class='col-md-12' style='display:none' id='step2'>
    {{adressForm}}
</div>

<div class='col-md-6' style='display:none' id='step3'>
    {{PayPlugin}}
</div>

<div class='col-md-6' style='display:none' id='step4'>
   
</div>

<div class='col-md-12 center marginTop' id='buttonStep' >   
    {{form->Render(btnPrevious)}}
    {{form->Render(btnNext)}}
</div>  

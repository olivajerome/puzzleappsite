<?php

namespace Apps\Market\Widget\AdminDashBoard;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;
use Core\Entity\Entity\Argument;
use Apps\Market\Entity\MarketProduct;
use Apps\Market\Entity\MarketProductOffer;


class AdminDashBoard {
    
    public function __construct($core) {
        $this->Core = $core;
    }
    
    /***
     * Render the widget
     */
    public function Render(){
        $view = new View(__DIR__ . "/View/adminDashboard.tpl", $this->Core);
       
        $products = new MarketProduct($this->Core);
        $view->AddElement(new ElementView("Products", $products->Find( " Id > 0 Order by Id desc limit 0,5 " )));
        
        $offers = new MarketProductOffer($this->Core);
        $view->AddElement(new ElementView("OffersSended", count($offers->Find( " Status = ".MarketProductOffer::SENDED." "))));
        $view->AddElement(new ElementView("OffersAccepted", count($offers->Find( " Status = ".MarketProductOffer::ACCEPTED." "))));
        $view->AddElement(new ElementView("OffersRejected", count($offers->Find( " Status = ".MarketProductOffer::REJECTED." "))));
   
        return $view->Render();
    }
}

var AdressForm = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
AdressForm.Init = function(){
    Event.AddById("btnSave", "click", AdressForm.SaveElement);
};

/***
* Obtient l'id de l'itin�raire courant 
*/
AdressForm.GetId = function(){
    return Form.GetId("AdressFormForm");
};

/*
* Sauvegare l'itineraire
*/
AdressForm.SaveElement = function(e){
   
    if(Form.IsValid("AdressFormForm"))
    {

    var data = "Class=AdressForm&Methode=SaveElement&App=AdressForm";
        data +=  Form.Serialize("AdressFormForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.statut == "Error"){
                Form.RenderError("AdressFormForm", data.message);
            } else{

                Form.SetId("AdressFormForm", data.data.Id);
            }
        });
    }
};


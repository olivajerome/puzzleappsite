<?php 
/*
* PuzzleApp
* Webemyos
* Jérôme Oliva
* GNU Licence
*/

namespace Apps\Comunity\Entity;
use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class ComunityComment extends Entity  
{
	//Constructeur
	function __construct($core)
	{
		//Version
		$this->Version ="2.0.0.0"; 

		//Nom de la table 
		$this->Core=$core; 
		$this->TableName="ComunityComment"; 
		$this->Alias = "ComunityComment"; 

		$this->UserId = new Property("UserId", "UserId", NUMERICBOX,  true, $this->Alias); 
		$this->Message = new Property("Message", "Message", TEXTAREA,  true, $this->Alias); 
		$this->DateCreated = new Property("DateCreated", "DateCreated", DATEBOX,  true, $this->Alias); 
		$this->MessageId = new Property("MessageId", "MessageId", NUMERICBOX,  false, $this->Alias); 
                $this->ParentId = new Property("ParentId", "ParentId", NUMERICBOX,  false, $this->Alias); 
                $this->ParentType = new Property("ParentType", "ParentType", NUMERICBOX,  false, $this->Alias); 

		//Creation de l entité 
		$this->Create(); 
	}
}
?>
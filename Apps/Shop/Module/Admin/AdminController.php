<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Shop\Module\Admin;

use Core\Control\Button\Button;
use Core\Controller\Controller;
use Core\View\View;
use Core\View\ElementView;
use Core\Control\TabStrip\TabStrip;
use Core\Control\EntityGrid\EntityGrid;
use Core\Control\EntityGrid\EntityColumn;
use Core\Control\EntityGrid\EntityIconColumn;
use Core\Control\EntityGrid\EntityFunctionColumn;
use Core\Entity\Entity\Argument;
use Apps\Shop\Helper\ShopHelper;
use Apps\Shop\Widget\ShopForm\ShopForm;

/*
 * 
 */
 class AdminController extends Controller
 {
    /**
     * Constructeur
     */
    function _construct($core="")
    {
          $this->Core = $core;
    }

    /**
     * Creation
     */
    function Create()
    {
    }

    /**
     * Initialisation
     */
    function Init()
    {
    }

    /**
     * Affichage du module
     */
    function Show($all=true)
    {
         return $this->Index();
    }
    
    /*
    * Get the home page
    */
   function Index()
   {
      $shop = ShopHelper::GetShop($this->Core);

       if(count($shop) > 0){
         
         $shop = $shop[0];
         $view = new View(__DIR__."/View/index.tpl", $this->Core);
        
         $tabShop = new TabStrip("tabShop", "Shop"); 
         $tabShop->AddTab($this->Core->GetCode("Shop.Shop"), $this->GetTabShop($shop));
         $tabShop->AddTab($this->Core->GetCode("Shop.Categories"), $this->GetTabCategory());
         $tabShop->AddTab($this->Core->GetCode("Shop.Caractertistiques"), $this->GetTabCaracteristique());
         $tabShop->AddTab($this->Core->GetCode("Shop.Product"), $this->GetTabProduct());
         $tabShop->AddTab($this->Core->GetCode("Shop.Command"), $this->GetTabCommand());
         
         $view->AddElement(new ElementView("tabShop", $tabShop->Render()));

        } else {
         $view = new View(__DIR__."/View/noShop.tpl", $this->Core);

         $shopForm = new ShopForm($this->Core);
         $view->AddElement(new ElementView("shopForm", $shopForm->Render()));
       }

       return $view->Render();
   }

   /***
    * Onglet de la boutique
    */
   function GetTabShop($shop){
      $shopForm = new ShopForm($this->Core);
      $shopForm->Load($shop);
      return $shopForm->Render();
   }

   /***
    * Categorie de la shop
    */
   function GetTabCategory(){

      $gdCategory = new EntityGrid("gdCategory", $this->Core);
      $gdCategory->Entity = "Apps\Shop\Entity\ShopCategory";
      $gdCategory->App = "Shop";
      $gdCategory->Action = "GetTabCategory";

      $btnAdd = new Button(BUTTON, "btnAddCategory");
      $btnAdd->Value = $this->Core->GetCode("Shop.AddCategory");

      $gdCategory->AddButton($btnAdd);

      $gdCategory->AddColumn(new EntityColumn("Name", "Name"));
      
      $gdCategory->AddColumn(new EntityIconColumn("Action", 
                                                array(array("EditIcone", "Shop.EditCategory", "Shop.EditCategory"),
                                                      array("DeleteIcone", "Shop.DeleteCategory", "Shop.DeleteCategory"),
                                                )    
                        ));

      return $gdCategory->Render();
   }
    
   /***
    * Caracteristiques des produits
    */
   function GetTabCaracteristique(){
      $gdCaracteristique = new EntityGrid("gdCaracteristique", $this->Core);
      $gdCaracteristique->Entity = "Apps\Shop\Entity\ShopCaracteristique";
      $gdCaracteristique->App = "Shop";
      $gdCaracteristique->Action = "GetTabCaracteristique";

      $btnAdd = new Button(BUTTON, "btnAddCaracteristique");
      $btnAdd->Value = $this->Core->GetCode("Shop.AddCaracteristique");

      $gdCaracteristique->AddButton($btnAdd);

      $gdCaracteristique->AddColumn(new EntityColumn("Name", "Name"));
      
      $gdCaracteristique->AddColumn(new EntityIconColumn("Action", 
                                                array(array("EditIcone", "Shop.EditCaracteristique", "Shop.EditCaracteristique"),
                                                      array("DeleteIcone", "Shop.DeleteCaracteristique", "Shop.DeleteCaracteristique"),
                                                )    
                        ));

      return $gdCaracteristique->Render();
   }

   /**
    * Produit
    */
   public function GetTabProduct(){
  
      $gdProduct = new EntityGrid("gdProduct", $this->Core);
      $gdProduct->Entity = "Apps\Shop\Entity\ShopProduct";
      $gdProduct->App = "Shop";
      $gdProduct->Action = "GetTabProduct";

      $btnAdd = new Button(BUTTON, "btnAddProduct");
      $btnAdd->Value = $this->Core->GetCode("Shop.AddProduct");

      $gdProduct->AddButton($btnAdd);

      $gdProduct->AddColumn(new EntityColumn("Name", "Name"));
      $gdProduct->AddColumn(new EntityColumn("Price", "Price"));
      
      $gdProduct->AddColumn(new EntityIconColumn("Action", 
                                                array(array("EditIcone", "Shop.EditProduct", "Shop.EditProduct"),
                                                )    
                        ));

      return $gdProduct->Render();
   }

   /***
    * Command
    */
   public function GetTabCommand(){

      $gdCommand = new EntityGrid("gdCommand", $this->Core);
      $gdCommand->Entity = "Apps\Shop\Entity\ShopCommand";
      $gdCommand->App = "Shop";
      $gdCommand->Action = "GetTabCommand";

      $gdCommand->AddOrder("DateCreated desc");
      $gdCommand->AddColumn(new EntityColumn("Name", "Name"));
      $gdCommand->AddColumn(new EntityColumn("DateCreated", "DateCreated"));
      
      $gdCommand->AddColumn(new EntityColumn("Status", "Status"));
      
      $gdCommand->AddColumn(new EntityIconColumn("Action", 
                                                array(array("EditIcone", "Shop.EditCommand", "Shop.EditCommand"),
                                                )    
                        ));

      return $gdCommand->Render();
   }

          /*action*/
 }
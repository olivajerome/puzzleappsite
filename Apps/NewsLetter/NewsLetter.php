<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\NewsLetter;

use Core\Core\Core;
use Core\Core\Request;
use Core\Core\Response;
use Apps\Base\Base;
use Apps\NewsLetter\Helper\EmailHelper;
use Apps\NewsLetter\Helper\CampagneHelper;
use Apps\NewsLetter\Module\Admin\AdminController;
use Apps\NewsLetter\Entity\NewsLetterEmail;
use Apps\NewsLetter\Entity\NewsLetterListeEmail;
use Apps\NewsLetter\Entity\NewsLetterEmailCampagne;

class NewsLetter extends Base {

    /**
     * Auteur et version
     * */
    public $Author = 'Webemyos';
    public $Version = '1.0.0';

    /**
     * Constructeur
     * */
    function __construct($core) {
        $this->Core = Core::getInstance();
        parent::__construct($this->Core, "NewsLetter");
    }

    /**
     * Execution de l'application
     */
    function Run() {
        echo parent::RunApp($this->Core, "NewsLetter", "NewsLetter");
    }

    /**
     * Définie les routes publiques
     */
    function GetRoute($routes = "") {
        parent::GetRoute(array());
        return $this->Route;
    }

    /***
     * Load Email By Selected App
     */
    function LoadEmailAppByApp() {
        return EmailHelper::LoadEmailAppByApp($this->Core, Request::GetPost("AppId"));
    }

    /**
     * Sauvegarde de la configuration
     */
    function SaveConfig() {
        return EmailHelper::SaveConfig($this->Core, Request::GetPosts());
    }

    /*     * *
     * Save Email
     */

    function SaveEmail() {
        if (EmailHelper::SaveEmail($this->Core, Request::GetPosts())) {
            $typeId = Request::GetPost("TypeId");
            return $this->ReponseByType($typeId);
        }
    }

    /*     * *
     * Delete a email
     */

    function DeleteEmail() {

        $email = new NewsLetterEmail($this->Core);
        $email->GetById(Request::GetPost("id"));

        $type = $email->TypeId->Value;
        $email->Delete();

        return $this->ReponseByType($typeId);
    }

    /*     * *
     * Return the Html for the Type Email
     */

    function ReponseByType($typeId) {

        $adminController = new AdminController($this->Core);

        switch ($typeId) {
            case 1:
                return Response::Success(array("typeId" => $typeId, "html" => $adminController->GetTabEmailBase()));
                break;
            case 2:
                return Response::Success(array("typeId" => $typeId, "html" => $adminController->GetTabEmailProgramme()));
                break;
            case 3;
                return Response::Success(array("typeId" => $typeId, "html" => $adminController->GetTabEmailEvent()));
                break;
        }
    }

    /*     * *
     * Save List
     */

    function SaveListEmail() {
        if (EmailHelper::SaveListEmail($this->Core, Request::GetPosts())) {

            $adminController = new AdminController($this->Core);
            return Response::Success(array("html" => $adminController->GetTabListEmail()));
        }
    }

    /*     * *
     * Delete a list of email
     */

    function DeleteListEmail() {

        $listEmail = new NewsLetterListeEmail($this->Core);
        $listEmail->GetById(Request::GetPost("id"));
        $listEmail->Delete();

        $adminController = new AdminController($this->Core);
        return Response::Success(array("html" => $adminController->GetTabListEmail()));
    }

/*     * *
     * Save List
     */

     function SaveCampagne() {
        if (CampagneHelper::SaveCampagne($this->Core, Request::GetPosts())) {

            $adminController = new AdminController($this->Core);
            return Response::Success(array("html" => $adminController->GetTabCampagne()));
        }
    }
    
    /*     * *
     * Delete a campagne
     */

     function DeleteCampagne() {

        $listEmail = new NewsLetterEmailCampagne($this->Core);
        $listEmail->GetById(Request::GetPost("id"));
        $listEmail->Delete();

        $adminController = new AdminController($this->Core);
        return Response::Success(array("html" => $adminController->GetTabCampagne()));
    }


     /***
     * Get list of Email can be user ine Newsletter
     */
    public function GetListEmail(){
        return array();
    }

    /***
     * Send simple Email or un email user
     */
    public function SendEmail($code, $email, $params){
    
        $code = str_replace("Base", "NewsLetter", $code);
        return EmailHelper::SendEmail($this->Core, $code, $email, $params);
    }

    /***
     * Envoi une cmpagne à une liste d'email
     */
    public function SendCampagne(){

        return CampagneHelper::SendCampagne($this->Core, Request::GetPosts());

    }

}

?>
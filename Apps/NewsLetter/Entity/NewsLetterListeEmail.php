<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\NewsLetter\Entity;

use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class NewsLetterListeEmail extends Entity {

    //Constructeur
    function __construct($core) {
        //Version
        $this->Version = "2.0.0.0";

        //Nom de la table 
        $this->Core = $core;
        $this->TableName = "NewsLetterListeEmail";
        $this->Alias = "NewsLetterListeEmail";

        $this->Libelle = new Property("Libelle", "Libelle", TEXTBOX, false, $this->Alias);
        $this->Description = new Property("Description", "Description", TEXTAREA, false, $this->Alias);
        $this->Emails = new Property("Emails", "Emails", TEXTAREA, false, $this->Alias);

        //Creation de l entité 
        $this->Create();
    }
}

?>
<?php 
/*
* PuzzleApp
* Webemyos
* Jérôme Oliva
* GNU Licence
*/

namespace Apps\Message\Entity;
use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class MessageUser extends Entity  
{
    
    protected $User;
    
    //Constructeur
    function __construct($core)
    {
            //Version
            $this->Version ="2.0.0.0"; 

            //Nom de la table 
            $this->Core=$core; 
            $this->TableName="MessageUser"; 
            $this->Alias = "MessageUser"; 

            $this->ConversationId = new Property("ConversationId", "ConversationId", NUMERICBOX,  true, $this->Alias); 
            $this->UserId = new Property("UserId", "UserId", NUMERICBOX,  true, $this->Alias); 
            $this->User = new EntityProperty("Core\Entity\User\User", "UserId"); 
            $this->Status = new Property("Status", "Status", NUMERICBOX,  true, $this->Alias); 
            $this->Readed = new Property("Readed", "Readed", NUMERICBOX,  true, $this->Alias); 

            //Creation de l entité 
            $this->Create(); 
    }
    
    
    public function GetSendMessage()
    {
        if($this->UserId->Value != $this->Core->User->IdEntite ){
         return "<i id='".$this->UserId->Value."'  class='fa fa-comment contactMember'>&nbsp</i> ";
        }
      
    }
    
}
?>
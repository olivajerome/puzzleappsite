<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Downloader\Module\Member;

use Core\Control\Button\Button;
use Core\Controller\Controller;
use Core\View\View;
use Core\Utility\Format\Format;

use Apps\Downloader\Widget\UserRessourceForm\UserRessourceForm;
use Apps\Downloader\Entity\DownloaderUserRessource;

/*
 * 
 */
 class MemberController extends Controller
 {
    /**
     * Constructeur
     */
    function _construct($core="")
    {
          $this->Core = $core;
    }

    /**
     * Create
     */
    function Create()
    {
    }

    /**
     * Init
     */
    function Init()
    {
    }

    /**
     * Render
     */
    function Show($all=true)
    {
         return $this->Index();
    }
    
    /*
    * Get the home page
    */
   function Index()
   {
       $view = new View(__DIR__."/View/index.tpl", $this->Core);
       return $view->Render();
   }

   /***
     * Save a ressource
     */
    function SaveUserRessource($data){

        $userRessourceForm = new UserRessourceForm($this->Core, $data);
  
          if ($userRessourceForm->Validate($data)) {
  
              $ressource = new DownloaderUserRessource($this->Core);
  
              if ($data["Id"] != "") {
                  $ressource->GetById($data["Id"]);
              } else {
                 $ressource->Code->Value = Format::ReplaceForUrl($data["Name"]);
                 $ressource->UserId->Value = $this->Core->User->IdEntite;
              }
  
              $userRessourceForm->Populate($ressource);
              $ressource->Save();

              //Obligatory for add the document in the Identifier directory
              if($data["Id"] == ""){
                $ressource->IdEntite = $this->Core->Db->GetInsertedId();
              }
              
              $ressource->SaveDocument($data["dvUpload-UploadfileToUpload"]);
                
              return true;
          }
  
          return false;
      }
          
          /*action*/
 }?>
{{form->Open()}}
{{form->Error()}}
{{form->Success()}}

{{form->Render(Id)}}
{{form->Render(PageId)}}


    <div>
        <label>{{GetCode(Cms.ContainerName)}}</label> 
        {{form->Render(Name)}}
    </div>

    <div>
        <label>{{GetCode(Cms.ContainerPosition)}}</label> 
        {{form->Render(Position)}}
    </div>

    <div>
        <label>{{GetCode(Cms.ContainerCssClass)}}</label> 
        {{form->Render(CssClass)}}
    </div>
    
     <div>
        <label>{{GetCode(Cms.ContainerStyle)}}</label> 
        {{form->Render(Style)}}
    </div>

    <div class='center marginTop' >   
        {{form->Render(btnSave)}}
    </div>  

{{form->Close()}}
var LayoutEditor = function () {};

LayoutEditor.Init = function () {
    Event.AddByClass("btnAddBlock", "click", LayoutEditor.ShowAddBlock);
    Event.AddByClass("btnEditBlock", "click", LayoutEditor.EditBlock);
    Event.AddByClass("btnRemoveBlock", "click", LayoutEditor.RemoveBlock);
    Event.AddByClass("blockContent", "click", LayoutEditor.ShowToolContentBlock);
    //Event.AddByClass("blockContent", "blur", LayoutEditor.UpdateContentBlock);
    Event.AddByClass("btnAddItemMenu", "click", LayoutEditor.AddItemMenu);
    Event.AddByClass("linkBlock", "click", LayoutEditor.EditItemMenu);
    Event.AddByClass("btnRemoveLinkItem", "click", LayoutEditor.RemoveItemMenu);

    Event.AddByClass("bntEditImage", "click", LayoutEditor.EditImage);
    Event.AddByClass("btnEditSource", "click", LayoutEditor.btnEditSource);
};

/***
 * Dialog d'ajout d'un block
 * @param {type} e
 * @returns {undefined}
 */
LayoutEditor.ShowAddBlock = function (e) {
    let block = e.srcElement;
    let container = block.parentNode.parentNode.dataset.block;

    Dialog.open('', {"title": Dashboard.GetCode("Cms.AddBlock"),
        "app": "Cms",
        "class": "DialogCms",
        "method": "ShowAddBlock",
        "type": "left",
        "params": container
    });
};

/***
 * Edition d'un block
 * @param {type} e
 * @returns {undefined}
 */
LayoutEditor.EditBlock = function (e) {
    let block = e.srcElement;
    let container = block.parentNode.parentNode.id;

    Dialog.open('', {"title": Dashboard.GetCode("Cms.AddBlock"),
        "app": "Cms",
        "class": "DialogCms",
        "method": "ShowAddBlock",
        "type": "left",
        "params": container
    });
};

/***
 * Supprime un block
 * @param {type} e
 * @returns {undefined}
 */
LayoutEditor.RemoveBlock = function (e) {
    let block = e.srcElement;

    Animation.Confirm(Language.GetCode("Cms.ConfirmRemoveBlock"), function () {
        let data = "Class=Cms&Methode=RemoveBlock&App=Cms";
        data += "&BlockId=" + block.parentNode.parentNode.id;

        Request.Post("Ajax.php", data).then(data => {
            Cms.RefreshTool();
        });
    });
};

/***
 * Add tools to format texte 
 * @param {type} e
 * @returns {undefined}
 */
LayoutEditor.ShowToolContentBlock = function (e) {

    let parent = e.srcElement;

    while (parent.className != "blockContent") {
        parent = parent.parentNode;
    }

    //Créer TextEditor
    LayoutEditor.txtEditor = new TextRichEditor(parent,
            {tools: [ "ImageTool", "SourceCode", "SaveContent"],
                events: [{"tool": "SaveContent", "events": [{"type": "mousedown", "handler": LayoutEditor.SaveContent}]},
                    {"tool": "ImageTool", "events": [{"type": "mousedown", "handler": LayoutEditor.OpenImageLibrary}]},
                    {"tool": "Link", "events": [{"type": "mousedown", "handler": linkSelected}]},
                    {"tool": "Table", "events": [{"type": "mousedown", "handler": TableSelected}]},
                ],
            }
    );

    function linkSelected() {
        console.log("OPEN LINK");
    }

    function TableSelected() {
        console.log("OPEN TABLE");
    }

};

/**
 * Update teh Content of the block 
 * @param {type} editor
 * @returns {undefined}
 */
LayoutEditor.SaveContent = function (editor) {
    let blockId = editor.sourceControl.parentNode.id;
    let content = LayoutEditor.txtEditor.getContent();

    LayoutEditor.UpdateContentBlock(blockId, content);
    LayoutEditor.txtEditor.quit();
};

/**
 * Open library Image
 * @param {type} editor
 * @param {type} node
 * @returns {undefined}
 */
LayoutEditor.OpenImageLibrary = function (editor, node) {

    if (node.tagName != "DIV") {
        Animation.Notify(Language.GetCode("Base.TextRichEditorCantAddImageAtThisEmplacement"));
        return;
    }

    editor.setSelectedNode(node);
    Dashboard.currentEditor = editor;

    let blockId = editor.sourceControl.parentNode.id;

    Dialog.open('', {"title": Dashboard.GetCode("Cms.Addmage"),
        "app": "Cms",
        "class": "DialogCms",
        "method": "AddImage",
        "type": "left",
        "params": blockId
    });
};

/**
 * Update the content of a block
 * @returns {undefined}
 */
LayoutEditor.UpdateContentBlock = function (blockId, content) {

    let data = "Class=Cms&Methode=UpdateContentBlock&App=Cms";
    data += "&BlockId=" + blockId;
    data += "&Content=" + content;

    Request.Post("Ajax.php", data).then(data => {
    });
};

/***
 * Add Item to o menu Block
 */
LayoutEditor.AddItemMenu = function (e) {

    let block = e.srcElement;

    Dialog.open('', {"title": Dashboard.GetCode("Cms.AddItemMenu"),
        "app": "Cms",
        "class": "DialogCms",
        "method": "AddItemMenu",
        "type": "left",
        "params": block.parentNode.id
    });
};

/***
 * Edit a item menu
 */
LayoutEditor.EditItemMenu = function (e) {

    e.preventDefault();

    let block = e.srcElement;

    Dialog.open('', {"title": Dashboard.GetCode("Cms.EditItemMenu"),
        "app": "Cms",
        "class": "DialogCms",
        "method": "EditItemMenu",
        "type": "left",
        "params": block.id
    });
};

/***
 * Supprimer l'item d'un menu
 */
LayoutEditor.RemoveItemMenu = function (e) {
    e.preventDefault();

    let block = e.srcElement;

    Animation.Confirm(Language.GetCode("Cms.ConfirmRemoveItem"), function () {

        let data = "Class=Cms&Methode=RemoveItemMenu&App=Cms";
        data += "&ItemId=" + block.id;

        Request.Post("Ajax.php", data).then(data => {
            Cms.RefreshTool();
        });
    });
};

/***
 * Edite une image
 */
LayoutEditor.EditImage = function (e) {
    let block = e.srcElement;

    Dialog.open('', {"title": Dashboard.GetCode("Cms.EditImage"),
        "app": "Cms",
        "class": "DialogCms",
        "method": "EditImage",
        "type": "left",
        "params": block.parentNode.parentNode.id
    });
};

/***
 * Affichage du code source
 * @param {type} e
 * @returns {undefined}
 */
LayoutEditor.btnEditSource = function (e) {
    let container = e.srcElement.parentNode.parentNode;
    let contentEdit = container.getElementsByClassName("editContent");
    container.innerHTML = "<textarea class='form-control sourceEditor' onblur='LayoutEditor.updateContent(this)'>" + contentEdit[0].innerHTML + "</textarea>" + container.innerHTML;
};

/***
 * Met à jour le contenu d'un block
 * @param {type} e
 * @returns {undefined}
 */
LayoutEditor.updateContent = function (e) {

    let blockId = e.parentNode.id;
    let newContent = e.value;

    let data = "Class=Cms&Methode=UpdateContentBlock&App=Cms";
    data += "&BlockId=" + blockId;
    data += "&Content=" + newContent;

    Request.Post("Ajax.php", data).then(data => {
        Cms.RefreshTool();
    });

};
<?php

namespace Apps\Shop\Widget\LastProductWidget;

use Core\View\View;
use Core\View\ElementView;

use Apps\Shop\Helper\ProductHelper;


class LastProductWidget {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "") {
        $this->Core = $core;
        $this->Data = $data;
    }

     /**
     * Caroussel des derniers produit
     */
    function Render(){

        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);

        $view->AddElement(new ElementView("Product", ProductHelper::GetLast($this->Core)));

        return $view->Render();
    }

}
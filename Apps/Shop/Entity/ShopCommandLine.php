<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Shop\Entity;

use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class ShopCommandLine extends Entity {

    //Constructeur
    function __construct($core) {
        //Version
        $this->Version = "2.0.0.0";

        //Nom de la table 
        $this->Core = $core;
        $this->TableName = "ShopCommandLine";
        $this->Alias = "ShopCommandLine";

        $this->CommandId = new Property("CommandId", "CommandId", NUMERICBOX, false, $this->Alias);
        $this->EntityName = new Property("EntityName", "EntityName", TEXTBOX, false, $this->Alias);
        $this->EntityId = new Property("EntityId", "EntityId", NUMERICBOX, false, $this->Alias);
        $this->Quantity = new Property("Quantity", "Quantity", NUMERICBOX, false, $this->Alias);
        $this->Price = new Property("Price", "Price", TEXTBOX, false, $this->Alias);
        $this->Data = new Property("Data", "Data", TEXTBOX, false, $this->Alias);

        //Creation de l entité 
        $this->Create();
    }

    /*     * *
     * 
     */

    function GetName() {
        $entityName = $this->EntityName->Value;
        $product = new $entityName($this->Core);
        $product->GetById($this->EntityId->Value);
        return $product->GetName();
    }

}

?>
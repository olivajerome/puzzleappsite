var LastMessageWidget = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
LastMessageWidget.Init = function(){
    Event.AddById("btnSave", "click", LastMessageWidget.SaveElement);
};

/***
* Obtient l'id de l'itin�raire courant 
*/
LastMessageWidget.GetId = function(){
    return Form.GetId("LastMessageWidgetForm");
};

/*
* Sauvegare l'itineraire
*/
LastMessageWidget.SaveElement = function(e){
   
    if(Form.IsValid("LastMessageWidgetForm"))
    {

    var data = "Class=LastMessageWidget&Methode=SaveElement&App=LastMessageWidget";
        data +=  Form.Serialize("LastMessageWidgetForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.statut == "Error"){
                Form.RenderError("LastMessageWidgetForm", data.message);
            } else{

                Form.SetId("LastMessageWidgetForm", data.data.Id);
            }
        });
    }
};


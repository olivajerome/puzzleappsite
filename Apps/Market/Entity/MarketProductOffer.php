<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Market\Entity;

use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class MarketProductOffer extends Entity {
    
    const SENDED = 1;
    const ACCEPTED = 2;
    const PRODUCT_SENDED = 3;
    const PRODUCT_RECEVEIVED = 4;
 
    const PRODUCT_NO_RECEVEIVED = 97;
    const REJECTED = 98;

    protected $Product;

    //Constructeur
    function __construct($core) {
        
        //Version
        $this->Version = "2.0.0.0";

        //Nom de la table 
        $this->Core = $core;
        $this->TableName = "MarketProductOffer";
        $this->Alias = "MarketProductOffer";

        $this->ProductId = new Property("ProductId", "ProductId", NUMERICBOX, false, $this->Alias);
        $this->Product = new EntityProperty("Apps\Market\Entity\MarketProduct", "ProductId");
        $this->UserId = new Property("UserId", "UserId", NUMERICBOX, false, $this->Alias);
        $this->Price = new Property("Price", "Price", TEXTBOX, false, $this->Alias);
        $this->DateCreated = new Property("DateCreated", "DateCreated", DATEBOX, false, $this->Alias);
        $this->Status = new Property("Status", "Status", NUMERICBOX, false, $this->Alias);

        //Creation de l entité 
        $this->Create();
    }
}

?>
<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Core\Control\Upload;

use Core\Core\Core;
use Core\Control\IControl;
use Core\Control\Control;

class Upload extends Control implements IControl
{
    private $app;
    private $idElement;
    private $callBack;
    private $action;
    private $idUpload;
    

    //Constructeur
    function __construct($app, $id ="", $callBack ="", $action = false, $idUpload ="fileToUpload")
    {
        //Version
        $this->Version ="2.0.0.0";
        $this->app = $app;
        $this->idElement = $id;
        $this->callBack = $callBack;
        $this->action = $action;
        $this->idUpload = $idUpload;
    }

    /*
    * Render the control
    */
    function Show($show = false)
    {
        $core = Core::getInstance();

        $html = "<div id='dvUpload-$this->idUpload' class='uploaderDiv' style='display:none'>";

        if(isset($this->Accept))
        {
            $accept = $this->Accept;
            $text = $core->GetCode("Base.ClickHereToUpoladDocument");
        }
        else
        {        
            $accept =  "image/x-png,image/gif,image/jpeg";
            $text = "<i class='fa fa-image' >".$core->GetCode("Base.ClickHereToUpoladImage")."</i>";
        }


        $html .="<input style='display:none' accept='".$accept."'  ' type='file' onchange='upload.fileChanged(this)' id='fileUpload-$this->idUpload' name='fileUpload-$this->idUpload' />";
        
        $html .= "<input type='hidden' id='hdApp' name = 'hdApp'  value='".$this->app."'  /> ";
        $html .= "<input type='hidden' id='hdIdElement' name='hdIdElement' value='".$this->idElement."'  /> ";
        $html .= "<input type='hidden' id='hdIdSourceElement' name='hdIdSourceElement' value='".$this->idElement."'  /> ";
        $html .= "<input type='hidden' id='hdIdValidator' class='validators' value='".$this->Validators."'  /> ";
        
        $html .= "<input type='hidden' id='hdCallBack' name='hdCallBack' value='".$this->callBack."' /> ";
        $html .= "<input type='hidden' id='hdAction' name='hdAction' value='".$this->action."' /> ";
        $html .= "<input type='hidden' id='hdIdUpload'  name='hdIdUpload' value='".$this->idUpload."' /> ";

        //Frame From Upload
        if($core->Debug)
        {
           $html .= "<iframe id='frUpload-$this->idUpload' src='".$core->GetPath("\upload")."' style='display:block' >";
        }
        else
        {
          $html .= "<iframe id='frUpload-$this->idUpload' src='".$core->GetPath("\upload")."' style='display:block' >";
        }

        $html .= "</iframe>";

        $html .= "</div>";
        $html .="<div class='uploadButtons'>";
      
        $html .= "<span data-id='". $this->idUpload."'  class='btn btn-upload' onclick='upload.onpenFile(this)'>".$text."</span>";
        $html .= "<input id='btnSendUpload-$this->idUpload' style='display:none' type='button' class='btn btn-info' value='".$core->GetCode("Submit")."' onclick='upload.doUpload(this)' /> ";
        $html .= "<img id='uploadLoading' style='display:none;width:50px' src='".$core->GetPath("/images/loading/load.gif")."' alt='Loading' />";

        $html .= "</div>";

        $html .= "<div class='uploadImages' style='border:none' id='uploadImages-$this->idUpload'>";
        $html .= "</div>";

        return $html;
    }

    /*
    * Get the Iframe Content uploader
    */
    public static function ShowUploader()
    {
        $html = "<form action='' method='POST' id='formUpload' enctype='multipart/form-data'>";
        $html .= "<h2>Uploader</h2>";
        $html .= "<input type='hidden' name='hdPost'  value='posted' >";

        $html .= "</form>";

        return $html;
    }

    /*
     * Call the DoUpload App
     */
    public static function DoUpload($appName, $idElement, $tmpFileName, $fileName, $action)
    {
        $appName = "\\Apps\\".$appName . "\\".$appName;

        $app = new $appName();
        $app->DoUploadFile($idElement, $tmpFileName, $fileName, $action);
    }
}
?>

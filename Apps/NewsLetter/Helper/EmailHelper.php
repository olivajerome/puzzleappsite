<?php

namespace Apps\NewsLetter\Helper;

use Core\Utility\Date\Date;
use Apps\NewsLetter\Widget\NewsLetterEmailForm\NewsLetterEmailForm;
use Apps\NewsLetter\Widget\NewsLetterConfigForm\NewsLetterConfigForm;
use Apps\NewsLetter\Widget\NewsLetterListEmailForm\NewsLetterListEmailForm;

use Apps\NewsLetter\Entity\NewsLetterConfig;
use Apps\NewsLetter\Entity\NewsLetterEmail;
use Apps\NewsLetter\Entity\NewsLetterListeEmail;

use Core\Control\ListBox\ListBox;

use Core\Utility\Email\Email;


class EmailHelper {
    /*     * *
     * Save Config
     */

    public static function SaveConfig($core, $data) {

        $configForm = new NewsLetterConfigForm($core, $data);

        if ($configForm->Validate($data)) {

            $newsLetterConfig = new NewsLetterConfig($core);

            if ($data["Id"] != "") {
                $newsLetterConfig->GetById($data["Id"]);
            }

            $configForm->Populate($newsLetterConfig);
            $newsLetterConfig->Save();

            return $configForm->Success(array("Message" => $core->GetCode("NewsLetter.EmailSaved"), "Id" => $core->Db->GetInsertedId()));
        } else {

            return $configForm->Error();
        }
    }

    /*     * *
     * Save Emil
     */
    public static function SaveEmail($core, $data) {

        $emailForm = new NewsLetterEmailForm($core, $data);

        if ($emailForm->Validate($data)) {

            $newsLetterEmail = new NewsLetterEmail($core);

            if ($data["Id"] != "") {
                $newsLetterEmail->GetById($data["Id"]);
            } else {
                $newsLetterEmail->Status->Value = 1;
                $newsLetterEmail->DateCreated->Value = Date::Now();
                $newsLetterEmail->TypeId->Value = 1;
            }

            $emailForm->Populate($newsLetterEmail);
            $newsLetterEmail->Save();
            return true;
        } else {

            return false;
        }
    }
    
    
     /*     * *
     * Save List
     */

    public static function SaveListEmail($core, $data) {

        $listEmailForm = new NewsLetterListEmailForm($core, $data);

        if ($listEmailForm->Validate($data)) {

            $newsLetterListEmail = new NewsLetterListeEmail($core);

            if ($data["Id"] != "") {
                $newsLetterListEmail->GetById($data["Id"]);
            } 

            $listEmailForm->Populate($newsLetterListEmail);
            $newsLetterListEmail->Save();
            return true;
        } else {

            return false;
        }
    }

    /***
     * Get Email for Apps
     */
    public static function LoadEmailAppByApp($core, $appId){
     
        //Ajout des widgets de Base
        $app = new \Apps\EeApp\Entity\EeAppApp($core);
        $app->GetById($appId);
        $widgetsBlock = new ListBox("Widget");
        $descriptionWidget = "";
        $iWidget = 0;    
  
      
            $appName = $app->Name->Value;
            $path = "\\Apps\\" . $appName . "\\" . $appName;
            $app = new $path($core);
            $listWidget = $app->GetListEmail();
         
            $widgetsBlock->Add($core->GetCode("NewsLetter.ChoseWidget"), "");
          
            if (count($listWidget) > 0) {
                foreach ($listWidget as $widget) {

                    if(isset($widget["Name"])){  

                        $name = $widget["Name"];    
                        $widgetsBlock->Add($appName . "-" .$name, $appName . "-" . $name);
                        $style ="";

                        if($iWidget > 0){
                            $style='display:none';
                        }
                        $descriptionWidget .= "<p class='descriptionWidget info' id='descriptionWiget-".$appName . "-" .$name."' style='".$style."'><i class='fa fa-info fa-2x'></i>"  . $widget["Description"] . "</p>";
                        $iWidget ++;
                    }
                }
        }
        return "<label>" . $core->GetCode("NewsLetter.EmailAvailable") . "</label>" .$widgetsBlock->Show().$descriptionWidget;
  }


    /***
     * Send User Email or base Email
     */
    public static function SendEmail($core, $code, $email, $params){

        $newsLetterEmail = new newsLetterEmail($core);
        $newsLetterEmail = $newsLetterEmail->Find("Code='". $code."'");
        
        $body = $newsLetterEmail[0]->Content->Value;

        foreach ($params as $key => $value) {
            $body = str_replace($key, $value, $body);
        }

        self::Send($core, "Test", $body, $email);
    }

    /***
     * Send the Email 
     */
    public static function Send($core, $object, $body, $emailUser){

        $pluginEmail = \Apps\EeApp\Helper\AppHelper::GetPluginType($core, "NewsLetter", "Email");
        
        if($pluginEmail != ""){
            $pluginEmail->Send($object, $body, $emailUser);
        } else{
       
            echo "SEND BY BASE";
            $email = new Email($core);
            $email->Title = $object;
            $email->Body = $body;
            $email->Send($emailUser);
        }
    }
}

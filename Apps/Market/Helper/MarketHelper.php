<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */


 namespace Apps\Market\Helper;

use Apps\Market\Entity\MarketMarket;

class MarketHelper
{
    /***
     * Get the Defaut market
     */
    public static function GetMarket($core){
        $market = new MarketMarket($core);
        return $market->Find("Id > 0");
    }
}
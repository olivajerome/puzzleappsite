var CarouselItemForm = function(){};

/***
 * Init the form
 * @returns {undefined}
 */
CarouselItemForm.Init = function(callBack){
    Event.AddById("btnSaveItem", "click", CarouselItemForm.SaveElement);
    
    let content = document.querySelector("#CarouselItemFormForm #Content");
    
    CarouselItemForm.txtEditor = new TextRichEditor(content,
            {tools: ["ImageTool", "SourceCode" , "HtmlToCode"],
                events: [{"tool": "ImageTool", "events": [{"type": "mousedown", "handler": CarouselItemForm.OpenImageLibrary}]}],
            }
    );
    
    CarouselItemForm.callBack = callBack;
 
};


CarouselItemForm.OpenImageLibrary = function(editor, node){
     
     if (node.tagName != "DIV") {
        Animation.Notify(Language.GetCode("Base.TextRichEditorCantAddImageAtThisEmplacement"));
        return;
    }

    editor.setSelectedNode(node);
    Dashboard.currentEditor = editor;

    let blockId = editor.sourceControl.parentNode.id;

    Dialog.open('', {"title": Dashboard.GetCode("Cms.Addmage"),
        "app": "Cms",
        "class": "DialogCms",
        "method": "AddImage",
        "type": "left",
        "params": blockId
    });
};

/***
* Get the Id of element 
*/
CarouselItemForm.GetId = function(){
    return Form.GetId("CarouselItemFormForm");
};

/*
* Save the Element
*/
CarouselItemForm.SaveElement = function(e){
   
    if(Form.IsValid("CarouselItemFormForm"))
    {

    let data = "Class=Carousel&Methode=SaveItem&App=Carousel";
        data +=  Form.Serialize("CarouselItemFormForm");
        data += "&Content=" + CarouselItemForm.txtEditor.getCleanContent(); 
     
        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);
      
            if(data.status == "error"){
                Form.RenderError("CarouselItemFormForm", data.data.message);
            } else{

                Form.SetId("CarouselItemFormForm", data.data.Id);
                
                Animation.Notify(Language.GetCode("Carousel.CarouselItemUpdated"));
                if(CarouselItemForm.callBack){
                    CarouselItemForm.callBack(data.data.items);
                }
            }
        });
    }
};



  {{form->Open()}}
  {{form->Error()}}
  {{form->Success()}}

  {{form->Render(Id)}}
  {{form->Render(FolderId)}}

   <div>
        <label>{{GetCode(Base.Name)}}</label> 
       {{form->Render(Name)}}
   </div>
   <div>
        <label>{{GetCode(Base.Description)}}</label> 
       {{form->Render(Description)}}
   </div>
   <div>
        <label>{{GetCode(Folders.File)}}</label> 
       {{form->Render(Upload)}}
   </div>

   <div class='center' >   
       {{form->Render(btnSaveFile)}}
    </div>  
    
  {{form->Close()}}

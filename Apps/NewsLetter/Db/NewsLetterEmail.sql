CREATE TABLE IF NOT EXISTS `NewsLetterEmail` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`TypeId` INT  NOT NULL,
`Code` VARCHAR(200)  NOT NULL,
`Libelle` VARCHAR(200)  NOT NULL,
`Description` TEXT  NOT NULL,
`Content` TEXT  NOT NULL,
`DateCreated` DATE  NOT NULL,
`Status` INT  NOT NULL,
PRIMARY KEY (`Id`),
CONSTRAINT `NewsLetterEmailType_NewsLetterEmail` FOREIGN KEY (`TypeId`) REFERENCES `NewsLetterEmailType`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
var DialogEeAppController = function () {};


DialogEeAppController.ShowUpdateVersion = function () {

    Event.AddById("btnUpdateFramework", "click", () => {

        Animation.Confirm(Language.GetCode("EeApp.ConfirmUpdateFramework"), function () {

            var data = "Class=EeApp&Methode=UpdateFramework&App=EeApp";

            Request.Post("Ajax.php", data).then(data => {
                Animation.Notify(Language.GetCode("EeApp.FrameworkUpdated"));
            });
            
            data = "Class=EeApp&Methode=UpdateApp&App=EeApp";
            data +="&app=Base";
            Request.Post("Ajax.php", data).then(data => {
                Animation.Notify(Language.GetCode("EeApp.AppUpdated"));
            });
            
        });
    });

    Event.AddByClass("btnUpdateApp", "click", function (e) {

        let container = e.srcElement.parentNode;
        let appName = container.getElementsByTagName("input")[0].value;
            
        Animation.Confirm(Language.GetCode("EeApp.ConfirmUpdateApp"), function () {

        let data = "Class=EeApp&Methode=UpdateApp&App=EeApp";
            data +="&app="+appName;
            Request.Post("Ajax.php", data).then(data => {
                Animation.Notify(Language.GetCode("EeApp.AppUpdated"));
            });
        });
    });
};

/***
 * Add plugin to a app
 */
DialogEeAppController.ShowAddPlugin = function(){
    PluginForm.Init();
};

/***
 * Add plugin to a app
 */
DialogEeAppController.AddPluginApp = function(){
    PluginAppForm.Init();
};

/***
 * Add template
 */
DialogEeAppController.ShowAddTemplate = function(){
    TemplateForm.Init();
};

/***
 * Add Or Edti parameter for a plugin
 */
DialogEeAppController.EditPlugin = function(){

};
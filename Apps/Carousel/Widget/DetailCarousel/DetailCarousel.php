<?php

namespace Apps\Carousel\Widget\DetailCarousel;

use Core\View\View;
use Core\View\ElementView;

use Apps\Carousel\Entity\CarouselCarousel;

class DetailCarousel {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "") {
        $this->Core = $core;
        $this->Data = $data;
    }
    
    /***
     * Redner a carousel
     */
    function Render($params){
        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);
       
        $carousel = new CarouselCarousel($this->Core);
        $carousel->GetById($params);
        
        $view->AddElement(new ElementView("Carousel", $carousel));
        $view->AddElement(new ElementView("Items", $carousel->GetItems()));
        
        return $view->Render();
    }
    
    
    
}
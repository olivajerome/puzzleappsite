<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */


namespace Apps\Market\Helper;

use Apps\Market\Entity\MarketProduct;
use Core\Dashboard\DashBoardManager;

use Apps\Market\Entity\MarketProductOffer;


class NotifyHelper
{
    
    /****
     * Send a offer
     */
    public static function SendOffer($core, $offer){
        $notify = DashBoardManager::GetApp("Notify", $core);
     
        $notify->AddNotify( $core->User->IdEntite,
                            "Market-SendOffer",
                            $offer->Product->Value->UserId->Value, 
                            "Market",
                            $offer->IdEntite,
                            $core->GetCode("Market.AcceptOfferTitle"),
                            $core->GetCode("Market.AcceptOfferMessage"),
                         );
    }
    
    /***
     * Accept Offer
     */
    public static function AcceptOffer($core, $offer){

        $notify = DashBoardManager::GetApp("Notify", $core);
     
        $notify->AddNotify( $core->User->IdEntite,
                            "Market-AcceptOffer",
                            $offer->UserId->Value, 
                            "Market",
                            $offer->IdEntite,
                            $core->GetCode("Market.AcceptOfferTitle"),
                            $core->GetCode("Market.AcceptOfferMessage"),
                         );
    }
    
    /***
     * Get Detail Notify
     */
    public static function GetDetailNotify($core, $notify){
        
        switch($notify->Code->Value ){
            
            case "Market-SendOffer":
                
                $offer = new MarketProductOffer($core);
                $offer->GetById($notify->EntityId->Value);
                
                $view = "<div>";
                $view .= "<h1>".$core->GetCode("Market.NewOffer")."</h1>";
                $view .= "<div class='col-md-4'><img src='".$offer->Product->Value->GetImagePath()."' style='width:100%'/></div>";
                
                $view .= "<div class='col-md-8'><p><b>" .$notify->GetUser() ."</b> ". $core->GetCode("Market.NewOfferOnYourProduct"); 
                $view .= "<b>".$annonce->Title->Value."</b></p>";
                
                $view .= "<span class='date'>".$notify->DateCreate->Value."</span>";
                $view .= "</div></div>"; 
                
                break;
        }
        
        return $view; 
       
        
    }
    
}
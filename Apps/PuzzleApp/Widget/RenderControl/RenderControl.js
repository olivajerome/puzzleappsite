var RenderControl = function(){};

/***
 * Init the form
 * @returns {undefined}
 */
RenderControl.Init = function(){
    Event.AddById("btnSave", "click", RenderControl.SaveElement);
};

/***
* Get the Id of element 
*/
RenderControl.GetId = function(){
    return Form.GetId("RenderControlForm");
};

/*
* Save the Element
*/
RenderControl.SaveElement = function(e){
   
    if(Form.IsValid("RenderControlForm"))
    {

    let data = "Class=RenderControl&Methode=SaveElement&App=RenderControl";
        data +=  Form.Serialize("RenderControlForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.status == "error"){
                Form.RenderError("RenderControlForm", data.data.message);
            } else{

                Form.SetId("RenderControlForm", data.data.Id);
            }
        });
    }
};


var RessourceForm = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
RessourceForm.Init = function(){
    
    let shortDescription = Dom.GetById("LongDescription");
    
    //Créer TextEditor
    LayoutEditor.txtEditor = new TextRichEditor(shortDescription,
            {tools: [ "ImageTool", "SourceCode"]}
    );
    
    
    Event.AddById("btnSaveRessource", "click", RessourceForm.SaveElement);
};

/***
* Obtient l'id de l'itin�raire courant 
*/
RessourceForm.GetId = function(){
    return Form.GetId("RessourceFormForm");
};

/*
* Sauvegare l'itineraire
*/
RessourceForm.SaveElement = function(e){
   
    if(Form.IsValid("RessourceFormForm"))
    {

    var data = "Class=Downloader&Methode=SaveRessource&App=Downloader";
        data +=  Form.Serialize("RessourceFormForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.statut == "Error"){
                Form.RenderError("RessourceFormForm", data.message);
            } else{

                Form.SetId("RessourceFormForm", data.data.Id);
                Dialog.Close();
                DownloaderAction.LoadMyRessource();
            }
        });
    }
};


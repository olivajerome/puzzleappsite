<div class='dialogAdminTool' id='dialogAdminTool' style='display:none'>
    <i class='fa fa-times' id='btnCloseAdminTool' ></i>
    <div id='adminToolContent'>
    </div>
</div>

<div class='adminTools'>
    <i class='fa fa-language fa-2x' title='{{GetCode(Admin.Translate)}}' id='btnAdminLang'></i>
    <i class='fa fa-database fa-2x' title='{{GetCode(Admin.Request)}}' id='btnAdminRequest'></i>
    <i class='fa fa-file fa-2x' title='{{GetCode(Admin.Log)}}' id='btnAdminLog'></i>
</div>
</div>


<script>
    AdminTools.Init();
</script>
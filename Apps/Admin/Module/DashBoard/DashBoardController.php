<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Admin\Module\DashBoard;

use Core\Controller\Controller;
use Core\Core\Core;
use Core\Dashboard\DashBoardManager;
use Core\View\ElementView;
use Core\View\View;
use Apps\EeApp\Entity\EeAppApp;
use Apps\Admin\Helper\UserWidgetHelper;

/**
 * Description of FrontBlock
 *
 * @author jerome
 */
class DashBoardController extends Controller {

    /**
     * Constructeur
     */
    function __construct($core = "") {
        $this->Core = Core::getInstance();
    }

    /*
     * Get le master modele
     */

    function GetMasterView() {
        $view = new View(__DIR__ . "/View/master.tpl", $this->Core);

        //Replace All Element in the Template
        //TODO METTRE EN CACHE UNE PARTIE DE ELEMENT
        $view->AddElement(new ElementView("infoNotify", DashBoardManager::GetInfoNotify($this->Core)));
        $view->AddElement(new ElementView("appUser", DashBoardManager::LoadUserApp($this->Core)));
        $view->AddElement(new ElementView("content", self::Index()));


        return $view;
    }

    /*
     * Get the home page
     */

    function Index() {
        $view = new View(__DIR__ . "/View/index.tpl");

        //Ajout des tuilles pas application si elle on un methode GetWidget=> AdminDashBoard
        $app = new EeAppApp($this->Core);
        $apps = $app->GetAll();

        $view->AddElement(new ElementView("Apps", $apps));

        return $view->Render();
    }

    /***
     * Récupére les widgets de l'utilisateur
     */
    function LoadWidget(){
        $view = new View(__DIR__."/View/widgets.tpl", $this->Core);

        $widgets = UserWidgetHelper::GetByUser($this->Core);
 
        if(count($widgets) > 0){
            $view->AddElement(new ElementView("Apps", $widgets));
            return $view->Render();
        } else {
            return "<div class='info'><i class='fa fa-info'></i>".$this->Core->GetCode("EeApp.NoUserWidget")."</div>" ;
        }
    }
}
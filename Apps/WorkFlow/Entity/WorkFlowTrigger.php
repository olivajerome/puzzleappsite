<?php 
/*
* PuzzleApp
* Webemyos
* Jérôme Oliva
* GNU Licence
*/

namespace Apps\WorkFlow\Entity;
use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class WorkFlowTrigger extends Entity  
{
	//Constructeur
	function __construct($core)
	{
		//Version
		$this->Version ="2.0.0.0"; 

		//Nom de la table 
		$this->Core=$core; 
		$this->TableName="WorkFlowTrigger"; 
		$this->Alias = "WorkFlowTrigger"; 

		$this->WorkFlowId = new Property("WorkFlowId", "WorkFlowId", NUMERICBOX,  false, $this->Alias); 
		$this->Event = new Property("Event", "Event", TEXTAREA,  false, $this->Alias); 
		$this->DateExecution = new Property("DateExecution", "DateExecution", TEXTAREA,  false, $this->Alias); 

		//Creation de l entité 
		$this->Create(); 
	}
}
?>
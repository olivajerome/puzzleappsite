<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */


namespace Apps\Market\Helper;

use Core\Utility\Format\Format;
use Core\Utility\File\File;
use Apps\Market\Entity\MarketProduct;
use Apps\Market\Entity\MarketProductCategory;
use Apps\Market\Entity\MarketProductCaracteristique;
use Apps\Market\Widget\ProductForm\ProductForm;
use Apps\Market\Helper\MarketHelper;

class ProductHelper
{
    /**
     * Sauvegarde un produit
     */
    public static function SaveProduct($core, $data){

        $productForm = new ProductForm($core, $data);

        if($productForm->Validate($data)){

            $product = new MarketProduct($core);
           
            if($data["Id"] != ""){
                $id = $data["Id"];
                $product->GetById($data["Id"]); 
            } else {
                $shop = MarketHelper::GetMarket($core);
                $shop = $shop[0];
                $product->MarketId->Value = $shop->IdEntite;
                $product->Status->Value = 1;
            }

            $productForm->Populate($shop);
            $product->Save();
            
            if(!isset($id)){
              $id = $core->Db->GetInsertedId();  
            }

            //Base Directory
            File::CreateDirectory("Data/Apps/Market");
            File::CreateDirectory("Data/Apps/Market/Product/");
            

            //Sauveagerde des images
            $directory = $product->DirectoryImage . $id . "/";

            File::CreateDirectory($directory);

            foreach (explode(",", $data["dvUpload-UploadfileToUpload"]) as $image) {

                $source = str_replace($core->GetPath("/"), "", $image);
                $filename = explode("/", $source);

                rename($source, $directory . $filename[count($filename) - 1]);
            }
            
            return $productForm->Success(array());
        
        }  else {
              return $productForm->Error();
        }
    } 

    /***
     * Le produit a déjà la catégorie
     */
    public static function HaveCategory($core, $productId, $categoryId){
        $productCategory = new MarketProductCategory($core);
        $category = $productCategory->Find("ProductId= " .$productId. " And CategoryId=".$categoryId );

        return (count($category) > 0 );
    }

    /**
     * Ajoute une catégorie à un produit
     */
    public static function AddCategoryProduct($core, $data){

        if(!self::HaveCategory($core, $data["productId"], $data["categoryId"])){
        
            $productCategory = new MarketProductCategory($core);
            $productCategory->ProductId->Value = $data["productId"];
            $productCategory->CategoryId->Value = $data["categoryId"];
            $productCategory->Save();
        }

        return self::GetCategoryByProduct($core, $data["productId"]);
    }

    /***
     * Obtient les catégories d'un produit
     */
    public static function GetCategoryByProduct($core, $productId){
       
       $productCategory = new MarketProductCategory($core);
       $productCategory->Select("category.Name", "CategoryName");
       $productCategory->Join("MarketCategory", "category", "left", "category.Id = MarketProductCategory.CategoryId");
       
       $categories = $productCategory->Find("ProductId= " . $productId);
       $categoriesArray = array();

       foreach($categories as $category){
            $categoryArray = $category->ToArray();
            $categoryArray["CategorieName"] = $category->CategoryName->Value;
            $categoriesArray[] = $categoryArray;
       }

       return $categoriesArray;
    }

    /**
     * Supprime une catégorie de produit
     */
    public static function RemoveCategorieProduct($core, $productId){
        $productCategory = new MarketProductCategory($core);
        $productCategory->GetById($productId);
        $productCategory->Delete();
    }

    /***
     * Obtient les caracteristique d'un produit
     */
    public static function GetCaracteristiqueByProduct($core, $productId, $returnType = "array"){

        $productCaracteristique = new MarketProductCaracteristique($core);
        $productCaracteristique->Select("caracteristique.Name", "CaracteristiqueName");
        $productCaracteristique->Select("caracteristiqueItem.Label", "CaracteristiqueItemName");
        
        $productCaracteristique->Join("MarketCaracteristique", "caracteristique", "left", "caracteristique.Id = MarketProductCaracteristique.CaracteristiqueId");
        $productCaracteristique->Join("MarketCaracteristiqueItem", "caracteristiqueItem", "left", "caracteristiqueItem.Id = MarketProductCaracteristique.ItemId");
        
        $caracteristiques = $productCaracteristique->Find("ProductId= " . $productId);

        if($returnType != "array"){
            return $caracteristiques;
        }

        $caracteristiquesArray = array();
 
        foreach($caracteristiques as $caracteristique){
             $caracteristiqueArray = $caracteristique->ToArray();
             $caracteristiqueArray["CaracteristiqueName"] = $caracteristique->CaracteristiqueName->Value;
             $caracteristiqueArray["CaracteristiqueItemName"] = $caracteristique->CaracteristiqueItemName->Value;
             $caracteristiquesArray[] = $caracteristiqueArray;
        }
 
        return $caracteristiquesArray;
    }

    /***
     * Add Caracteristique to a product
     */
    public static function AddCaracteristiqueProduct($core, $data){

        $productCaracteristique = new MarketProductCaracteristique($core);
        $productCaracteristique->ProductId->Value = $data["productId"];
        $productCaracteristique->CaracteristiqueId->Value = $data["caracteristiqueId"];
        $productCaracteristique->ItemId->Value = $data["itemId"];
        $productCaracteristique->Save();

        return self::GetCaracteristiqueByProduct($core, $data["productId"]);
    }

    /***
     * Remove a caracteristique for à product
     */
    public static function RemoveCaracteristiqueProduct($core, $productId){
        $productCaracteristique = new MarketProductCaracteristique($core);
        $productCaracteristique->GetById($productId);
        $productCaracteristique->Delete();
    }
    
    /***
     * Get The Product of a category
     */
    public static function GetByCategorie($core, $categoryId){
       
        $product = new MarketProduct($core);
        $product->Join("MarketProductCategory", "productCategory", "left", "productCategory.ProductId = MarketProduct.Id");
        
        return $product->Find("productCategory.CategoryId=" . $categoryId);
    }

    /****
     * Get the Product of a user
     */
    public static function GetByUser($core, $userId){

        $product = new MarketProduct($core);
        return $product->Find("UserId=" . $userId);
    }

    /***
     * Get Last added Product
     */
    public static function GetLast($core, $returnElement = false) {
        $product = new MarketProduct($core);
        $products = $product->Find("id > 0 order by Id desc limit 0,5");
      
        if ($returnElement) {
            return $products[0];
        } else {
            return $products;
        }
    }

}
<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Folders\Module\Share;

use Core\Control\Button\Button;
use Core\Controller\Controller;
use Core\View\View;
use Core\View\ElementView;

use Apps\Folders\Helper\FolderHelper;
use Core\Control\AutoCompleteBox\AutoCompleteBox;

/*
 * 
 */
 class ShareController extends Controller
 {
    /**
     * Constructeur
     */
    function _construct($core="")
    {
          $this->Core = $core;
    }

    /**
     * Creation
     */
    function Create()
    {
    }

    /**
     * Initialisation
     */
    function Init()
    {
    }

    /**
     * Affichage du module
     */
    function Show($all=true)
    {
         return $this->Index();
    }
    
    /*
    * Get the home page
    */
   function Share($folderId)
   {
       $view = new View(__DIR__."/View/share.tpl", $this->Core);
       $view->AddElement(new ElementView("folderShareId", $folderId));
       
       $tbContact = new AutoCompleteBox("tbContact", $this->Core);
       $tbContact->PlaceHolder = "Rechercher un destinataire";
       $tbContact->Entity = "Core/Entity/User/User";
       $tbContact->Methode = "SearchUser";
       $tbContact->Parameter = "AddButton=1&AddAction=Folders.UserSelected()";
       $view->AddElement($tbContact);
       
       //User rattaché
       $users = FolderHelper::GetFolderUsers($this->Core, $folderId);
       $view->AddElement(new ElementView("Users", $users));
       
       return $view->Render();
   }
   
   /*
    * Get the home page
    */
   function ShareFile($fileId)
   {
       $view = new View(__DIR__."/View/share.tpl", $this->Core);
       $view->AddElement(new ElementView("folderShareId", $fileId));
       
       $tbContact = new AutoCompleteBox("tbContact", $this->Core);
       $tbContact->PlaceHolder = "Rechercher un destinataire";
       $tbContact->Entity = "Core/Entity/User/User";
       $tbContact->Methode = "SearchUser";
       $tbContact->Parameter = "AddButton=1&AddAction=Folders.UserFileSelected()";
       $view->AddElement($tbContact);
       
       //User rattaché
       $users = FolderHelper::GetFileUsers($this->Core, $fileId);
       $view->AddElement(new ElementView("Users", $users));
       
       return $view->Render();
   }
   
          
          /*action*/
 }?>
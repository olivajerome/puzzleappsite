<div class='content-panel'>
    <table class='grid'>
        <tr>
            <th>{{GetCode(Downloader.Name)}}</th>
            <th>{{GetCode(Downloader.Description)}}</th>
            <th></th>
        </tr>  
        {{foreach}}
            <tr>
                <td>{{element->Name->Value}}</td>
                <td>{{element->Description->Value}}</td>
                <td><a class='btn btn-primary' href='{{element->GetUrl()}}' >{{GetCode(Download.Download)}}</a></td>
            </tr>
        {{/foreach}}
    </table>    
</div>    


 DROP TABLE IF EXISTS MarketProductShipping;
 DROP TABLE IF EXISTS MarketProductOffer;
 DROP TABLE IF EXISTS MarketProductCaracteristique;
 DROP TABLE IF EXISTS MarketProductCategory;
 DROP TABLE IF EXISTS MarketProduct;
 DROP TABLE IF EXISTS MarketCaracteristiqueItem;
 DROP TABLE IF EXISTS MarketCaracteristique;
 DROP TABLE IF EXISTS MarketCategory;
 DROP TABLE IF EXISTS MarketMarket;
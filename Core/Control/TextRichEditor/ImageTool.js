class ImageTool extends BaseTool {
    getIcone() {
        return "<i class='fa fa-image'></i>";
    }

    getName() {
        return Language.GetCode("Base.TextRichEditorImage");
    }

    setEvent(events) {
        this.events = events;
    }
    execute(e) {
        e.preventDefault();
        for (let event in this.events) {
            if (this.events[event]["type"] == e.type) {
                let node = window.getSelection().anchorNode;
                
                this.events[event]['handler'](this.editor, node);
            }
        }
    }
}


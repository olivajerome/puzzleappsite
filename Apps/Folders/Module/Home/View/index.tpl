<h1>{{GetCode(Folders.Folders)}}</h1>

    <div class='tools'>
        <i class='fa fa-plus' id='btnAddFolder' title='{{GetCode(Folders.AddFolder)}}' >
            {{GetCode(Folder.New)}}
        </i>
        <i class='fa fa-share' id='btnSharedMe' title='{{GetCode(Folders.SharedWhithMe)}}' >
            {{GetCode(Folders.SharedWhithMe)}}
        </i>    


    </div>

    <div class='row' id='root'>
        {{foreach Folders}}
            <div class='col-md-1 folder' id='{{element->IdEntite}}'  
                        title='{{element->Description->Value}}'>
                <i class='fa fa-folder fa-2x'>&nbsp;</i>
                <div class='button' >
                    <i class='fa fa-edit btnEditFolder'>&nbsp;</i>
                    <i class='fa fa-user btnShareFolder'>&nbsp;</i>
                    <i class='fa fa-trash btnDeleteFolder'>&nbsp;</i>
                </div> 
                <h5>{{element->Name->Value}}</h5> 
                  
            </div>
        {{/foreach Folders}}

        {{foreach Files}}
            <div class='col-md-1 folder' id='{{element->IdEntite}}'  
                        title='{{element->Description->Value}}'>
                <i class='fa fa-file fa-2x'>&nbsp;</i>
                <div class='button' >
                    <a target='_blank' href='{{element->Url->Value}}' class='fa fa-download btnDownLoadFile'>&nbsp;</a>
                    <i class='fa fa-user btnShareFile'>&nbsp;</i>
                    <i class='fa fa-trash btnDeleteFile'>&nbsp;</i>
                </div> 
                <h5>{{element->Name->Value}}</h5> 
                  
            </div>
        {{/foreach Files}}    


    </div>

</div>

<script src='{{GetPath(/script.php?s=Dashboard)}}' ></script>  
<script src='{{GetPath(/script.php?a=Membre)}}' ></script>  

<link href="{{GetPath(/style.php?a=Membre)}}" rel="stylesheet">

<div style='display:none'>
     {{tbApp}}
</div>

<div id='dvCenter' >
 
    <section class='container memberDashBoard'>
        <h1>{{GetCode(Membre.Welcome)}}
            
            {{GetPseudo()}}</h1>
        <div class='row'>
            <div class='col-md-2'>
                <div class='block'>
                    <div>
                        {{GetWidget(Membre,Menu,App)}}
                  
                    </div> 
                    <div>    
                        <a href="{{GetPath(/Disconnect)}}"> 
                            <i class='fa fa-sign-out fa-2x' title='Se déconnecter' >{{GetCode(Base.Logout)}}</i>
                        </a>
                    </div>
                </div>            
            </div>


            <div class='col-md-10' id='memberDashBoard' style='padding:0px'>
                <div class='centerBlock' >
                    <h1>{{GetCode(Membre.Home)}}</h1>
                    <p>{{GetCode(Membre.HomeDescription)}}</p>
                </div>
            </div>
        </div> 
    </section>
</div>


<div id='context'>
</div>

<script>        

    Membre.Load();

   {{if showTips == true}}
       if(typeof(Tips) != "undefined"){
            Tips.Show();
       }
   {{/if showTips == true}}
 </script>                 
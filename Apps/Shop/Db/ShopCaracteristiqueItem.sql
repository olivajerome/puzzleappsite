CREATE TABLE IF NOT EXISTS `ShopCaracteristiqueItem` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`CaracteristiqueId` INT  NULL ,
`Label` VARCHAR(200)  NULL ,
PRIMARY KEY (`Id`),
CONSTRAINT `ShopCaracteristique_ShopCaracteristiqueItem` FOREIGN KEY (`CaracteristiqueId`) REFERENCES `ShopCaracteristique`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
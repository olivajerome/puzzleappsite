<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Shop\Module\Front;

use Core\Control\Button\Button;
use Core\Controller\Controller;
use Core\View\View;
use Core\Core\Request;
use Core\View\ElementView;
use Apps\Shop\Entity\ShopShop;
use Apps\Shop\Entity\ShopCategory;
use Apps\Shop\Entity\ShopProduct;

use Apps\Shop\Helper\ProductHelper;
use Apps\Shop\Helper\ShopHelper;
use Apps\Shop\Helper\CartHelper;
use Apps\Shop\Widget\CommandForm\CommandForm;


/*
 * 
 */

class FrontController extends Controller {

    /**
     * Constructeur
     */
    function _construct($core = "") {
        $this->Core = $core;
    }

    /**
     * Creation
     */
    function Create() {
        
    }

    /**
     * Initialisation
     */
    function Init() {
        
    }

    /**
     * Affichage du module
     */
    function Show($all = true) {
        return $this->Index();
    }

    /*
     * Get the home page
     */

    function Index() {
        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);

        $shop = ShopHelper::GetDefault($this->Core);
        $view->AddElement(new ElementView("Shop", $shop));

        //Category 
        $category = new ShopCategory($this->Core);
        $view->AddElement(new ElementView("Categories", $category->Find("ShopId=" . $shop->IdEntite)));

        return $view->Render();
    }

    /*     * *
     * Product of the selected category
     */

    function Category($categoryCode) {
        $category = new ShopCategory($this->Core);
        $category = $category->GetByCode($categoryCode);

        if($category != null){
            $this->Core->MasterView->Set("Title", $this->Core->GetCode("Shop.HomePageCategory") . "|" . $category->Name->Value);

            $view = new View(__DIR__ . "/View/category.tpl", $this->Core);
            $view->AddElement(new ElementView("Category", $category));
            $view->AddElement(new ElementView("Products", ProductHelper::GetByCategorie($this->Core, $category->IdEntite)));

            return $view->Render();
        } else{
            $this->Core->Redirect("/");
        }
    }

     /*     * *
     * Detail of a Product 
     */

    function Product($productCode) {
        $product = new ShopProduct($this->Core);
        $product = $product->GetByCode($productCode);

        if($product != null){
            $this->Core->MasterView->Set("Title", $this->Core->GetCode("Shop.HomePageProduct") . "|" . $product->Name->Value);

            $view = new View(__DIR__ . "/View/product.tpl", $this->Core);
            $view->AddElement(new ElementView("Product", $product));
         
            $caracteristiques = ProductHelper::GetCaracteristiqueByProduct($this->Core, $product->IdEntite, "entity");
            $view->AddElement(new ElementView("Caracteristiques", $caracteristiques));
         
            $socialPlugin = \Apps\EeApp\Helper\AppHelper::GetPluginType($this->Core, "Shop", "Social");
        
            if($socialPlugin != ""){
                $view->AddElement(new ElementView("socialPlugin", $socialPlugin->Render()));
            } else{
                $view->AddElement(new ElementView("socialPlugin", ""));
            }


            return $view->Render();
        } else{
            $this->Core->Redirect("/");
        }
    }
    
    /***
     * Cart Page
     */
    function Cart(){
        $this->Core->MasterView->Set("Title", $this->Core->GetCode("Shop.HomePageCard"));

        
        if(CartHelper::GetNumberElement() != 0){
            $view = new View(__DIR__ . "/View/cart.tpl", $this->Core);
            $commandForm = new CommandForm($this->Core);

            $view->AddElement(new ElementView("commandForm", $commandForm->Render()));
        }
        else{
            $view = new View(__DIR__ . "/View/cartEmpty.tpl", $this->Core);
        }
        return $view->Render();
    }
    
    /* action */
}


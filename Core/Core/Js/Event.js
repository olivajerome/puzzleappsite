
/***
 * Nouvelle classe D'event
 * @returns {undefined}
 */
Event = function () {};


/***
 * Gestionnaire d'évenement général
 * @returns {undefined}
 */
Event.Init = function () {

    //fermeture des Div, des contextMenu ...;
    document.addEventListener("click", Event.CheckAndClose);
    document.addEventListener("scroll", Event.CloseContextMenu);
};

/**
 * Vérifie et ferme ce qui doit être fermé.
 * @returns {undefined}
 */
Event.CheckAndClose = function (e) {

   let divResult = document.getElementById("divResult");
   let element = e.srcElement;
   
   if (divResult != undefined) {
    
        let removeResult = true;
        let parent = element;
        
        for(let i = 0; i < 4; i ++){
            
            parent = parent.parentNode;
            
            if(parent != null && parent.id == "divResult"){
                removeResult = false;
            }
        }
    
        //On ne pas ferme pas le div result si c'est un enfant 
        if(removeResult ){        
            divResult.parentNode.removeChild(divResult);
        }
    }
    
    if (typeof (e.srcElement.className.indexOf) == "function" && e.srcElement.className.indexOf("fa") < 0 &&  e.srcElement.parentNode.className != null && e.srcElement.parentNode.className.indexOf("clickableContext") < 0)
    {
        ContextMenu.Close();
    }

    var contextListBox = document.getElementById("contextListBox");

    if (e.srcElement.className.indexOf("optionEntityListBox") > -1) {
        check = e.srcElement.getElementsByTagName("input");
        if (check[0].checked) {
            check[0].checked = false;
        } else {
            check[0].checked = true;
        }
    }

    if (contextListBox != undefined && e.srcElement.parentNode.className.indexOf("selectFiterBox") < 0
            && e.srcElement.className.indexOf("selectFiterBox") < 0
            && e.srcElement.className.indexOf("entityListBoxIcon") < 0
            && e.srcElement.className.indexOf("selectMultipleBox") < 0
            && e.srcElement.className.indexOf("optionEntityListBox") < 0
            && e.srcElement.parentNode.className.indexOf("optionEntityListBox") < 0
            ) {
        document.body.removeChild(contextListBox);
    }
};

/***
 * Ferme le menu contextuel
 * @returns {undefined}
 */
Event.CloseContextMenu = function(){
     var contextMenu = document.getElementById("contextMenu");
     
     if(contextMenu != null){
         document.body.removeChild(contextMenu);
     }
};


/***
 * Ajout un event sur un control
 * @returns {undefined}
 */
Event.AddById = function (element, event, methode, app, balise)
{
    Dashboard.AddEventById(element, event, methode, app, balise);
};

/**
 * Ajout des évenent par class 
 * @param {type} className
 * @param {type} event
 * @param {type} callBack
 * @returns {undefined}
 */
Event.AddByClass = function (className, event, callBack)
{
    Dashboard.AddEventByClass(className, event, callBack);
};

Event.AddEvent = function (control, event, callBack) {
    Dashboard.AddEvent(control, event, callBack);
};

Event.AddBySelector = function (selector, event, callBack)
{
    Dashboard.AddEventBySelector(selector, event, callBack);
};


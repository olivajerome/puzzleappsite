<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Comunity\Entity;

use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Argument;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;
use Apps\Comunity\Entity\ComunityUser;
use Apps\Comunity\Entity\ComunityMessage;
use Core\Control\Image\Image;
use Core\Entity\User\User;
use Core\Utility\Date\Date;

class ComunityComunity extends Entity {

    //Constructeur
    function __construct($core) {
        //Version
        $this->Version = "2.0.0.0";

        //Nom de la table 
        $this->Core = $core;
        $this->TableName = "ComunityComunity";
        $this->Alias = "ComunityComunity";

        $this->Name = new Property("Name", "Name", TEXTBOX, true, $this->Alias);
        $this->Type = new Property("Type", "Type", NUMERICBOX, true, $this->Alias);
        $this->UserId = new Property("UserId", "UserId", NUMERICBOX, true, $this->Alias);
        $this->DateCreated = new Property("DateCreated", "DateCreated", DATEBOX, false, $this->Alias);
        $this->Title = new Property("Title", "Title", TEXTAREA, false, $this->Alias);
        $this->SubTitle = new Property("SubTitle", "SubTitle", TEXTAREA, false, $this->Alias);
        $this->Description = new Property("Description", "Description", TEXTAREA, false, $this->Alias);
        $this->Status = new Property("Status", "Status", NUMERICBOX, false, $this->Alias);
        $this->DateClotured = new Property("DateClotured", "DateClotured", DATEBOX, false, $this->Alias);
        $this->DateLastMessage = new Property("DateLastMessage", "DateLastMessage", DATETIMEBOX, false, $this->Alias);

        //Creation de l entité 
        $this->Create();
    }

    /**
     * Retourne les membres de la conversation
     */
    public function GetUserImage($limit = null) {
        $users = new ComunityUser($this->Core);
        $users->AddArgument(new Argument("Apps\Comunity\Entity\ComunityUser", "ComunityId", EQUAL, $this->IdEntite));

        $users = $users->GetByArg();

        $html = "";

        foreach ($users as $user) {
            if ($user->UserId->Value != $this->Core->User->IdEntite) {

                $member = $user->User->Value;
                $html .= "<div title='" . $member->GetPseudo() . "'  class='avatarmini-img avatarmini' style='cursor: pointer;'>
                                <img src=\"" . $member->GetImage() . "\" alt='images'>
                            </div>";
            }
        }

        return $html;
    }

    /**
     * Obtient le premiers message du projet
     */
    public function GetFirstMessage() {
        $discusMessage = new ComunityMessage($this->Core);
        $discusMessage->AddArgument(new Argument("Apps\Comunity\Entity\ComunityMessage", "ComunityId", EQUAL, $this->IdEntite));

        return $discusMessage->GetByArg();
    }

    /**
     * Url de la premiére image
     * @return type
     */
    public function GetFirstImageUrl() {
        return $this->GetImages(true, true);
    }

    /**
     * Obtient la premier image
     */
    public function GetFirstImage() {
        return $this->GetImages(true);
    }

    /**
     * Obtient les images
     */
    public function GetImages($first = false, $url = false) {
        if (file_exists("../Web/Data/Apps/Comunity/" . $this->IdEntite)) {
            $html = "";
            $mini = "<div class='mini'>";
            $i = 0;

            $dh = @opendir("../Web/Data/Apps/Comunity/" . $this->IdEntite);
            if ($dh) {
                while (($fname = readdir($dh)) !== false) {

                    if ($fname != "." && $fname != "..") {
                        $img = new Image($this->Core->GetPath("/Data/Apps/Comunity/" . $this->IdEntite . "/" . $fname));

                        if ($first) {

                            if ($url == true) {
                                return $this->Core->GetPath("/Data/Apps/Comunity/" . $this->IdEntite . "/" . $fname);
                            } else {
                                //  $img->AddStyle("width", "100%");
                                return $img->Show();
                            }
                        }
                        $img->AddStyle("width", "100px");
                        $mini .= $img->Show();

                        $i++;
                    }
                }

                $html .= $mini . "</div>";

                closedir($dh);
            }

            return $html;
        } else {
            if ($url) {
                return $this->Core->GetPath("/images/validation.png");
            } else {
                $img = new Image($this->Core->GetPath("/images/validation.png"));
                return $img->Show();
            }
        }
    }

    /**
     * Obtient l'id des membres
     */
    public function GetMembresIds() {
        $sql = "select GROUP_concat(distinct(UserId)) as ids FROM ComunityUser where ComunityId = " . $this->IdEntite;
        $result = $this->Core->Db->GetLine($sql);

        return $result["ids"];
    }

    /**
     * Retourne les memebre
     */
    public function GetMembres() {
        $sql = "select GROUP_concat(distinct(UserId)) as ids FROM ComunityMessage where ComunityId = " . $this->IdEntite;
        $result = $this->Core->Db->GetLine($sql);

        $users = new User($this->Core);
        $users = $users->Find("id in (" . $result["ids"] . ")");

        $html = "<p class='date'>" . count($users) . " Membres</p>";
        foreach ($users as $user) {
            $html .= "<div title='" . $user->GetPseudo() . "' class='avatarmini-img avatarmini'>";
            $html .= "<img src='" . $user->GetImageMini() . "' alt='images'>";
            $html .= "</div>";
        }

        return $html;
    }

    /**
     * Nombre de message
     */
    function GetNumberMessage() {
        $request = "SELECT count(Id) as nbMessage from  ComunityMessage WHERE ComunityId = " . $this->IdEntite;
        $result = $this->Core->Db->GetLine($request);

        return $result["nbMessage"];
    }

    public function GetLimitTitle() {
        return $this->GetTitle(7);
    }

    /*     * *
     * Selon le type de conversation ,il faut 
     */

    function GetTitle($limit = null) {
        //TODO Selon le type de conversation ile titre peut être différents

        $users = new ComunityUser($this->Core);
        $users->AddArgument(new Argument("Apps\Comunity\Entity\ComunityUser", "ComunityId", EQUAL, $this->IdEntite));

        if ($limit != null) {
            $users->SetLimit(1, $limit);
        }

        $users = $users->GetByArg();
        $html = "";
        $userImage = "<div class='userImage'>";
        $userPseudo = "";

        foreach ($users as $user) {
            if ($user->UserId->Value != $this->Core->User->IdEntite) {

                if ($userPseudo != "") {
                    $userPseudo .= ",";
                }

                $userPseudo .= $user->User->Value->GetPseudo();
                $userImage .= "<div  class='avatarmini-img avatarmini'>";
                $userImage .= "<img src='" . $user->User->Value->GetImage() . "' ></div>";
            }
        }

        //Comunityion depuis les offres
        if ($this->Type->Value == 3) {
            return $userImage . "</div><div class='userTitle'>" . $this->Title->Value . "</div>";
        }
        //Comunityion depuis les projet
        else if ($this->ProjetId->Value != "") {
            $projet = new ComunityComunity($this->Core);
            $projet->GetById($this->ProjetId->Value);

            return $userImage . "</div><div class='userTitle'>" . $projet->Title->Value . "</div>";
        }


        return $userImage . "</div><div class='userTitle'>" . $userPseudo . "</div>";
    }

    /**
     * Sous Titre
     */
    function GetSubTitle() {
        $dateModif = explode("/", $this->DateLastMessage->Value);

        $dateNow = new \DateTime("now");
        $dateModifStamp = new \DateTime($dateModif[0] . "-" . $dateModif[1] . "-" . $dateModif[2]);

        $heure = explode(" ", $dateModif[2]);

        $intervalle = $dateNow->diff($dateModifStamp);

        if ($intervalle->d == 0) {
            $infoDate = "Aujourd'hui (" . $heure[1] . ")";
        } else {
            $infoDate = $intervalle->d . " jour(s)";
        }


        return "<span>" . $this->SubTitle->Value . "</span><span class='day'>" . $infoDate . "</span>";
    }

    /*     * *
     * L'utilisateur à t'il lu la Comunityion
     */

    public function GetClassReaded() {
        $request = "SELECT Readed FROM ComunityUser WHERE ComunityId =  " . $this->IdEntite . " AND UserId = " . $this->Core->User->IdEntite;
        $result = $this->Core->Db->GetLine($request);

        if ($result["Readed"] == 1) {
            return 'NotRead';
        } else {
            return "Read";
        }
    }

    /*     * *
     * Retourne les membres qui opn rejoint le projet
     */

    public function GetProjetMember() {
        $discusUser = new ComunityUser($this->Core);
        return ($discusUser->find(" ComunityId = " . $this->IdEntite));
    }

    /**
     * Obtient les documents du projet
     */
    public function GetDocument() {
        $documentPath = "../Web/Data/Apps/Comunity/" . $this->IdEntite . "/Message";

        $photos = "<div class='projetImages'><h3>Photos</h3>";
        $documents = "<div class='projetDocument'><h3>Documents</h3><ul>";

        if (file_exists($documentPath)) {
            $dh = @opendir($documentPath);
            if ($dh) {
                //OUveture du repertoire
                while (($fname = readdir($dh)) !== false) {

                    if ($fname != "." && $fname != "..") {
                        $messagePath = $documentPath . "/" . $fname;

                        $df = @opendir($messagePath);
                        if ($df) { {
                                while (($fileName = readdir($df)) !== false) {
                                    if ($fileName != "." && $fileName != "..") {
                                        if ((strstr($fileName, ".txt") != false || strstr($fileName, ".TXT") != false ||
                                                strstr($fileName, ".pdf") || strstr($fileName, ".PDF") ||
                                                strstr($fileName, ".doc") || strstr($fileName, ".DOC"))) {
                                            $documents .= "<li><i class='fa fa-file fa-2x'>&nbsp;</i><a target='_blank' href='" . $this->Core->GetPath(str_replace("../Web", "", $messagePath . "/" . $fileName)) . "'>" . $fileName . "</a></li>";
                                        }
                                    }
                                    if ($fileName != "." && $fileName != "..") {
                                        if ((strstr($fileName, ".jpg") != false || strstr($fileName, ".JPG") != false ||
                                                strstr($fileName, ".jpeg") || strstr($fileName, ".JPEG") ||
                                                strstr($fileName, ".png") || strstr($fileName, ".PNG")
                                                )) {
                                            $photos .= "<a target='_blank' href='" . $this->Core->GetPath(str_replace("../Web", "", $messagePath . "/" . $fileName)) . "'><img src='" . $this->Core->GetPath(str_replace("../Web", "", $messagePath . "/" . $fileName)) . "' /></a>";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }


            return $photos . "</div>" . $documents . "</ul></div>";
        } else {
            return "Il n'y a pas de document";
        }
    }
}

?>
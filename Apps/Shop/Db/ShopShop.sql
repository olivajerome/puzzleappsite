CREATE TABLE IF NOT EXISTS `ShopShop` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`Name` TEXT  NULL ,
`Code` TEXT  NULL ,
`Description` TEXT  NULL ,
`UserId` int  NULL ,
PRIMARY KEY (`Id`),
CONSTRAINT `ee_user_ShopShop` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
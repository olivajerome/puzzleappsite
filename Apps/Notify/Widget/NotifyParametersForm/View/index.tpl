{{form->Open()}}
{{form->Error()}}
{{form->Success()}}

{{form->Render(Id)}}
    <div>
        <label>{{GetCode(Notify.Name)}}</label> 
        {{form->Render(Name)}}
    </div>

<div>
        <label>{{GetCode(Notify.Description)}}</label> 
        {{form->Render(Description)}}
    </div>

    <div>
        <label>{{GetCode(Notify.Sender)}}</label> 
        {{form->Render(Sender)}}
    </div>

    <div>
        <label>{{GetCode(Notify.ReplyTo)}}</label> 
        {{form->Render(ReplyTo)}}
    </div>
    <div class='center marginTop' >   
        {{form->Render(btnSaveParameter)}}
    </div>  

{{form->Close()}}
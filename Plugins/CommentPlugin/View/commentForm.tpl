{{form->Open()}}
{{form->Error()}}
{{form->Success()}}

{{form->Render(Id)}}
{{form->Render(EntityName)}}
{{form->Render(EntityId)}}

    <div>
        <label>{{GetCode(Base.YourName)}}</label> 
        {{form->Render(Name)}}
    </div>

    <div>
        <label>{{GetCode(Base.YourMessage)}}</label> 
        {{form->Render(Message)}}
    </div>
    <div class='center marginTop' >   
        {{form->Render(btnSendComment)}}
    </div>
   
{{form->Close()}}
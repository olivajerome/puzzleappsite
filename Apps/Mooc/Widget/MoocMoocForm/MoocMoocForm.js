var MoocMoocForm = function () {};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
MoocMoocForm.Init = function (callBack) {

    Event.AddById("btnSaveMooc", "click", MoocMoocForm.SaveMooc);
    Event.AddById("editParameter", "click", MoocMoocForm.EditParameter);
    Event.AddById("btnAddLesson", "click", () => {
        MoocMoocForm.EditLesson("")
    });

    MoocMoocForm.InitLesson();

    MoocMoocForm.callBack = callBack;
};

/***
 * Remove a lesson
 * @returns {undefined}
 */
MoocMoocForm.InitLesson = function () {

    Event.AddByClass("editLesson", "click", MoocMoocForm.EditLesson);
    Event.AddByClass("removeLesson", "click", MoocMoocForm.RemoveLesson);
};

/***
 * Obtient l'id de l'itin�raire courant 
 */
MoocMoocForm.GetId = function () {
    return Form.GetId("moocForm");
};

/*
 * Sauvegare l'itineraire
 */
MoocMoocForm.SaveMooc = function (e) {

    if (Form.IsValid("moocForm"))
    {

        let data = "Class=Mooc&Methode=SaveMooc&App=Mooc";
        data += Form.Serialize("moocForm");


        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if (data.statut == "Error") {
                Form.RenderError("moocForm", data.message);
            } else {

                Animation.Notify(Language.GetCode("Mooc.MoocSaved"));

                if (MoocMoocForm.callBack) {
                    MoocMoocForm.callBack(data);
                }
            }
        });
    }
};

/**
 * Edit the Parameter
 * @returns {undefined}
 */
MoocMoocForm.EditParameter = function () {
    Animation.Hide("lessonTool");
    Animation.Show("formTool");
};

/***
 * Edit a lesson
 * @returns {undefined}
 */
MoocMoocForm.EditLesson = function (e) {

    Animation.Hide("formTool");
    Animation.Show("lessonTool");

    let lessonId = "";
    let data = "Class=Mooc&Methode=EditLesson&App=Mooc";
    data += "&MoocId=" + MoocMoocForm.GetId();

    if (e != "") {
        lessonId = e.srcElement.id;
    }

    data += "&lessonId=" + lessonId;

    Request.Post("Ajax.php", data).then(data => {
        Animation.Load("lessonTool", data);

        MoocLessonForm.Init(MoocMoocForm.RefresLesson);
    });
};

/***
 * Rafresh lecon of a mooc
 * @param {type} data
 * @returns {undefined}
 */
MoocMoocForm.RefresLesson = function (data) {

    let lessons = data.data;
    let view = "";

    for (let i = 0; i < lessons.length; i++) {

        view += "<li class='lesson editLesson' id='" + lessons[i].IdEntite + "'>" + lessons[i].Name + "<i class='fa fa-trash removeLesson'></i></li>";
    }

    Animation.Load("listLesson", view);
    MoocMoocForm.InitLesson();
};

/***
 * Remove a lesson
 * @param {type} e
 * @returns {undefined}
 */
MoocMoocForm.RemoveLesson = function (e) {
    let lessonId = e.srcElement.parentNode.id;
    let container = e.srcElement.parentNode;

    Animation.Confirm(Language.GetCode("Mooc.ConfirmRemoveLesson"), function () {


        let data = "Class=Mooc&Methode=RemoveLesson&App=Mooc";
        data += "&lessonId=" + lessonId;

        Request.Post("Ajax.php", data).then(data => {
            container.parentNode.removeChild(container);
            
            MoocMoocForm.EditParameter();
        });
    });
};
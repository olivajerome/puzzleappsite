var WorkFlowTriggerActionForm = function(){};

/***
 * Init the form
 * @returns {undefined}
 */
WorkFlowTriggerActionForm.Init = function(){
    Event.AddById("btnSaveTriggerAction", "click", WorkFlowTriggerActionForm.SaveElement);
};

/***
* Get the Id of element 
*/
WorkFlowTriggerActionForm.GetId = function(){
    return Form.GetId("WorkFlowTriggerActionFormForm");
};

/*
* Save the Element
*/
WorkFlowTriggerActionForm.SaveElement = function(e){
   
    if(Form.IsValid("WorkFlowTriggerActionFormForm"))
    {
    let request = new Http("WorkFlow", "AddTriggerActions");
        request.SetData(Form.Serialize("WorkFlowTriggerActionFormForm"));

        request.Post(data => {

            data = JSON.parse(data);

            if(data.status == "error"){
                Form.RenderError("WorkFlowTriggerActionFormForm", data.data.message);
            } else{

                Form.SetId("WorkFlowTriggerActionFormForm", data.data.Id);
            }
        });
    }
};


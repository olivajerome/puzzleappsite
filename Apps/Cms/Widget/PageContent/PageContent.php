<?php

namespace Apps\Cms\Widget\PageContent;

use Core\View\View;
use Core\View\ElementView;
use Apps\Cms\Entity\CmsPage;
use Apps\Cms\Entity\CmsContainer;

class PageContent {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "") {
        $this->Core = $core;
    }

    /*     * *
     * Set the content
     */

    function Render($pageCode) {

        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);

        $page = new CmsPage($this->Core);
        $page = $page->GetByName($pageCode);
    
        if($page != null){
            $container = new CmsContainer($this->Core);

            $view->AddElement(new ElementView("Container", $container->Find("PageId=".$page->IdEntite . " ORDER By Position")));
            return $view->Render();
        }
    }
}

<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Plugins\VotePlugin\Entity;

use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class VoteVote extends Entity {

    //Constructeur
    function __construct($core) {
        //Version
        $this->Version = "2.0.0.0";

        //Nom de la table 
        $this->Core = $core;
        $this->TableName = "VoteVote";
        $this->Alias = "VoteVote";

        $this->UserId = new Property("UserId", "UserId", NUMERICBOX, false, $this->Alias);
        $this->EntityName = new Property("EntityName", "EntityName", TEXTBOX, false, $this->Alias);
        $this->EntityId = new Property("EntityId", "EntityId", NUMERICBOX, false, $this->Alias);
        $this->Vote = new Property("Vote", "Vote", TEXTBOX, false, $this->Alias);

        $this->UserName = new Property("UserName", "UserName", TEXTBOX, false, $this->Alias);
        $this->DateCreated = new Property("DateCreated", "DateCreated", DATEBOX, false, $this->Alias);
       
        //Creation de l entité 
        $this->Create();
    }
}

?>
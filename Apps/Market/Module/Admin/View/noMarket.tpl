
<section class='container'>
    <div class='row'>
        <h1>{{GetCode(Market.NoMarket)}}</h1>

        <div class='col-md-4'>
            <div class='info'><i class='fa fa-info'></i>
               {{GetCode(Market.NewMarketDescription    )}}
            </div>     
        </div>
        <div class='col-md-6'>
            {{marketForm}}
        </div>
    </div>
</section>  

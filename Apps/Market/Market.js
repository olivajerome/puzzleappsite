var Market = function () {};

/*
 * Add Event when the App loaded
 */
Market.Load = function (parameter)
{
    this.LoadEvent();
};

/*
 * Add event
 */
Market.LoadEvent = function ()
{
    Dashboard.AddEventAppMenu(Market.Execute, "", "Market");
    Dashboard.AddEventWindowsTool("Market");

    MarketForm.Init();
    Market.InitTabCategory();
    Market.InitTabCaracteristique();
    Market.InitTabProduct();
};

/**
 * Tab des catgéogrie
 * @returns {undefined}
 */
Market.InitTabCategory = function () {
    //Category
    Event.AddById("btnAddCategory", "click", () => {
        Market.ShowAddCategory("")
    });
    EntityGrid.Initialise('gdCategory');
};

/**
 * Dialogue d'ajout de catégorie
 */
Market.ShowAddCategory = function (categoryId) {

    Dialog.open('', {"title": Language.GetCode("Market.AddCategorie"),
        "app": "Market",
        "class": "DialogAdminMarket",
        "method": "AddCategory",
        "type": "left",
        "params": categoryId
    });
};

/****
 * Edite une catégorie
 * @param {type} categoryId
 * @returns {undefined}
 */
Market.EditCategory = function (categoryId) {
    Market.ShowAddCategory(categoryId);
};

/***
 * Supprime une catégorie
 * @param {type} categoryId
 * @returns {undefined}
 */
Market.DeleteCategory = function (categoryId) {

    Animation.Confirm(Language.GetCode("Market.ConfirmRemoveCategorie"), () => {

        let request = new Http("Market", "DeleteCategory");
        request.Add("categoryId", categoryId);

        request.Post(data => {

            data = JSON.parse(data);
            Market.ReloadCategory(data);
        });
    });
};

/***
 * Rafraichit la tab des catégorie
 * @returns {undefined}
 */
Market.ReloadCategory = function (data) {
    let gdCategory = Dom.GetById("gdCategory");
    gdCategory.parentNode.innerHTML = data.data;
    Market.InitTabCategory();
};

/**
 * Tab des caracteristique
 * @returns {undefined}
 */
Market.InitTabCaracteristique = function () {
    //Catégory
    Event.AddById("btnAddCaracteristique", "click", () => {
        Market.ShowAddCaracteristique("")
    });
    EntityGrid.Initialise('gdCaracteristique');
};

/**
 * Dialogue d'ajout de caractéristique
 */
Market.ShowAddCaracteristique = function (caracteristiqueId) {

    Dialog.open('', {"title": Language.GetCode("Market.AddCaracteristique"),
        "app": "Market",
        "class": "DialogAdminMarket",
        "method": "AddCaracteristique",
        "type": "left",
        "params": caracteristiqueId
    });
};

/****
 * Edite une caracteristique
 * @param {type} caracteristiqueId
 * @returns {undefined}
 */
Market.EditCaracteristique = function (caracteristiqueId) {
    Market.ShowAddCaracteristique(caracteristiqueId);
};


/***
 * Supprime une caractéristique
 * @param {type} categoryId
 * @returns {undefined}
 */
Market.DeleteCaracteristique = function (caracteristiqueId) {

    Animation.Confirm(Language.GetCode("Market.ConfirmRemoveCaracteristique"), () => {

        let data = "Class=Market&Methode=DeleteCaracteristique&App=Market";
        data += "&caracteristiqueId=" + caracteristiqueId;

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            Market.ReloadCaracteristique(data);
        });
    });
};


/***
 * Rafraichit la tab des caractéristiques
 * @returns {undefined}
 */
Market.ReloadCaracteristique = function (data) {
    let gdCaracteristique = Dom.GetById("gdCaracteristique");
    gdCaracteristique.parentNode.innerHTML = data.data;
    Market.InitTabCaracteristique();
};

/***
 * Init tab product
 * @returns {undefined}
 */
Market.InitTabProduct = function(){
    EntityGrid.Initialise('gdProduct');
}; 

/***
 * Start the Event for Member 
 */
Market.LoadMember = function () {
    
    Event.AddById("btnAddProduct", "click", function(){ Market.ShowAddProduct("")});
    Event.AddByClass("editProduct", "click", function(e){ 
        let productId = e.srcElement.id;
        Market.EditProduct(productId);
    });

    Market.InitEventOffer();

};

/***
 * Init Event on the Offer Tab
 */
Market.InitEventOffer = function(){

    Event.AddByClass("btnAcceptOffer", "click", Market.AcceptOffer);
    Event.AddByClass("btnRefuseOffer", "click", Market.RefuseOffer);
    Event.AddByClass("btnSendProduct", "click", Market.SendProduct);
    Event.AddByClass("btnConfirmRecept", "click", Market.ConfirmReceptProduct);
    Event.AddByClass("btnConfirmNoRecept", "click", Market.ConfirmNoReceptProduct);
};

/***
 * Get the Offer ID
 */
Market.GetOfferId = function(element){

    while(element.className.indexOf("blockOffer") < 1){
       element = element.parentNode; 
    }

    return element.id;
};

/***
 * Accept the offer
 */
Market.AcceptOffer = function(e){
    
    Animation.Confirm(Language.GetCode("Market.ConfirmAcceptThisOffer"),()=>{
   
        let offerId = Market.GetOfferId(e.srcElement);
        let request = new Http("Market","AcceptOffer");
        request.Add("OfferId", offerId);

        request.Post(function(data){
            Market.RefreshStatus(offerId);
        });
    });
};

/***
 * Refuse the offer
 * 
 */
Market.RefuseOffer = function(e){
    
    Animation.Confirm(Language.GetCode("Market.ConfirmRefuseThisOffer"),()=>{
        let offerId = Market.GetOfferId(e.srcElement);
        let request = new Http("Market","RefuseOffer");
            request.Add("OfferId", offerId);

            request.Post(function(data){
                Market.RefreshStatus(offerId, data);
            });
    });
};

/*
* Passe l'offre en status Produit envoyé 
*/
Market.SendProduct = function(e){

    let offerId = Market.GetOfferId(e.srcElement);
  
    Dialog.open('', {"title": Language.GetCode("Market.SendProduct"),
        "app": "Market",
        "class": "DialogMarket",
        "method": "SendProduct",
        "type": "right",
        "params": offerId
    });

};

/***
 * Confirm Recept product
 * @param {type} e
 * @returns {undefined}
 */
Market.ConfirmReceptProduct = function(e){
    
     Animation.Confirm(Language.GetCode("Market.ConfirmReceptProduct"),()=>{
        let offerId = Market.GetOfferId(e.srcElement);
        let request = new Http("Market","ConfirmReceptProduct");
            request.Add("OfferId", offerId);

            request.Post(function(data){
                Market.RefreshStatus(offerId, data);
            });
    });
};

/***
 * Confirm Recept product
 * @param {type} e
 * @returns {undefined}
 */
Market.ConfirmNoReceptProduct = function(e){
    
     Animation.Confirm(Language.GetCode("Market.ConfirmNoReceptProduct"),()=>{
        let offerId = Market.GetOfferId(e.srcElement);
        let request = new Http("Market","ConfirmNoReceptProduct");
            request.Add("OfferId", offerId);

            request.Post(function(data){
                Market.RefreshStatus(offerId, data);
            });
    });
};


/**
 * Refresh the statut/Action for the command
 */
Market.RefreshStatus = function(offerId, data){
    data = JSON.parse(data);
    let status = Dom.GetById("offerStatus-" + offerId);
    status.innerHTML = data.data.status;

    Market.InitEventOffer();
};

/***
 * Refresh 
 * @returns {undefined}
 */
Market.RefreshPlugin = function () {
    Dashboard.StartApp("", "Market", "");
};


/****
 * Add Product By User
 * @returns {undefined}
 */
Market.ShowAddProduct = function(productId){
    
     Dialog.open('', {"title": Language.GetCode("Market.AddProduct"),
        "app": "Market",
        "class": "DialogMarket",
        "method": "AddProduct",
        "type": "left",
        "params": productId
    });
};

/****
 * Add Product By User
 * @returns {undefined}
 */
Market.EditProduct = function(productId){
    
    Dialog.open('', {"title": Language.GetCode("Market.EditProduct"),
        "app": "Market",
        "class": "DialogMarket",
        "method": "EditProduct",
        "type": "left",
        "params": productId
    });
};

/*
 * Execute a function
 */
Market.Execute = function (e)
{
    //Call the function
    Dashboard.Execute(this, e, "Market");
    return false;
};

/***
 * Init the app front page
 */
Market.Init = function (app, method) {
    if (app == "Market") {
        switch (method) {
            case "Product":
                Market.InitProductPage();
                
                break;
            default :
                break;
        }
    }
};

/***
 * Init the Admin Widget
 */
Market.InitAdminWidget = function () {

};

Market.InitProductPage = function(){
  Event.AddById("btnMakeOffre", "click", Market.MakeOffer);

  //Init the plugin
  setTimeout(function(){
    VotePlugin.Init();
    RatingPlugin.Init();
  }, 1000);

};

/***
 * Make offer on a product
 * @returns {undefined}
 */
Market.MakeOffer = function(){
    
  Dialog.open('', {"title": Language.GetCode("Market.MakeOffer"),
        "app": "Market",
        "class": "DialogMarket",
        "method": "MakeOffer",
        "params": Dom.GetById("productId").value;
    });
};
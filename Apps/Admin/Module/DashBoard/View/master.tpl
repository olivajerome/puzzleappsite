<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Dashboard">
        <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

        <title>Admin</title>

        <!-- Bootstrap core CSS -->
        <link href="{{GetPath(/asset/bootstrap.css)}}" rel="stylesheet">
        <link href="{{GetPath(/asset/fontawesome.min.css)}}" rel="stylesheet">
        <!--external css-->

        <!-- Custom styles for this template -->
        <link href="{{GetPath(/css/style.css)}}" rel="stylesheet">
        <link href="{{GetPath(/asset/style.css)}}" rel="stylesheet">
        <link href="{{GetPath(/asset/style-responsive.css)}}" rel="stylesheet">
        <link href="{{GetPath(/asset/desktop.css)}}" rel="stylesheet">
        <link href="{{GetPath(/style.php?a=Admin)}}" rel="stylesheet">

        <script src='{{GetPath(/script.php)}}' ></script>
        <script src='{{GetPath(/script.php?a=Admin)}}' ></script>
        <script src='{{GetPath(/script.php?apps=all)}}' ></script>
         
    </head>
    <body>
        
        
        

        <section id="container" >
            <!-- **********************************************************************************************************************************************************
            TOP BAR CONTENT & NOTIFICATIONS
            *********************************************************************************************************************************************************** -->
            <!--header start-->
            <header class="header black-bg">
                <div class="sidebar-toggle-box">
                    <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
                </div>
                <!--logo start-->
                <a href="index.html" class="logo"><b>PuzzleApp</b></a>
                <!--logo end-->
                <div class="nav notify-row" id="top_men">
                    <!--  notification start -->
                    <ul class="nav top-menu">
                        <!-- App -->
                        <li class="dropdown">
                            <div id='tdApp' class='span8'>
                            </div>
                        </li>
                        <!-- App end -->
                        <!-- inbox dropdown end -->
                    </ul>
                    <!--  notification end -->
                </div>

                <div class="top-menu">
                    <ul class="nav pull-right top-menu">
                        <li><div id='tdApp' class='top-menu'>
                            </div>
                        </li>    
                        <li class='langSelector'>
                            <div style='margin-top: -10px'>
                            {{GetWidget(Lang,LangSelector,all)}}
                            </div>
                        </li>
                        
                        <li><a href="Disconnect" class='logout'><i class="fa fa-power-off"></i>&nbsp;{{GetCode(Base.Logout)}}</a></a></li>
                    </ul>
                </div>
            </header>
            <!--header end-->

            <!-- **********************************************************************************************************************************************************
            MAIN SIDEBAR MENU
            *********************************************************************************************************************************************************** -->
            <!--sidebar start-->
            <aside>
                <div id="sidebar"  class="nav-collapse ">
                    <!-- sidebar menu start-->
                    <ul class="sidebar-menu" id="nav-accordion">
                        <li class="sub-menu" id="btnStart">
                            <a id='btnDashBoard' >
                                <i class="fa fa-dashboard"></i>
                                <span>{{GetCode(Base.myDashboard)}}</span>
                            </a>
                        </li>
                        <li class="sub-menu">
                            <a href="javascript:;" >
                                <i class="fa fa-desktop"></i>
                                <span>{{GetCode(Base.myApplications)}}</span>
                                <i id='btnAddApp' class='fa fa-plus' style='float:right' title='{{GetCode(EeApp.AddApp)}}'></i>
                                
                            </a>
                            <ul class="subsss">
                                {{appUser}}
                            </ul>
                        </li>
                    </ul>
                    <!-- sidebar menu end-->
                </div>
            </aside>
            <!--sidebar end-->

            <!-- **********************************************************************************************************************************************************
            MAIN CONTENT
            *********************************************************************************************************************************************************** -->
            <!--main content start-->
            <section id="main-content">
                <section class="wrapper site-min-height">
                    <div class="row mt">
                        <div class="col-lg-12">
                            <div id='dvCenter'  class='Admin'>
                                <div class='App row-fluid'>
                                    {{content}}
                                </div>
                            </div>
                        </div>
                    </div>
                </section><! --/wrapper -->
            </section><!-- /MAIN CONTENT -->

            <!--main content end-->
            <!--footer start-->
            <footer class="site-footer">
                <div class="text-center">
                    PuzzleApp - PuzzeApp.com
                    <a href="blank.html#" class="go-top">
                        <i class="fa fa-angle-up"></i>
                    </a>
                </div>

                <div id='context'>
                </div>
            </footer>
            <!--footer end-->
        </section>
        !script
    </body>

    
    {{GetWidget(Admin,Tools,all)}}
   
</html>


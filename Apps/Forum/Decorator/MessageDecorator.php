<?php 


namespace Apps\Forum\Decorator  ;

use Apps\Forum\Helper\MessageHelper;

class MessageDecorator{


    static $votePlugin = null;
    
    public function __construct($core)
    {
        $this->Core = $core;
    }
    
    
    /***
     *  Obtient le html de la derni�re r�ponse
     **/
     public function GetLastReponse($entity){
    
        $reponse = MessageHelper::GetLastReponse($this->Core, $entity->IdEntite);
        
        if($reponse->Message->Value != "")
        {
            $content = str_replace('<p>&nbsp;</p>', '', $reponse->Message->Value ). "(".$reponse->DateCreated->Value.")";
        }
        else
        {
           return  $this->Core->GetCode("Forum.NoReponse");
        }
        
    
        $html ="<div class='col-md-2'>    
                <div class='avatarmini-img avatarmini' >
                    <img src='".$reponse->User->Value->GetImage()."' alt='images'> <br/>
                    <a title='Contacter ce membre en MP' href='#'>".$reponse->User->Value->GetPseudo()."</a>
                </div>
        </div>
        <div class='col-md-10'>
            $content
        </div>" ;
        
        
        return $html;
    
    }

    /**
     * Get Vote plugin 
     */
    function GetVoteReponsePlugin($reponse){

        // Mémorisation pour ne le récupérer que la premier fois 
        if(self::$votePlugin == null){
            $pluginVote = \Apps\EeApp\Helper\AppHelper::GetPluginType($this->Core, "Forum", "Vote");
            self::$votePlugin = $pluginVote;
        } 
       
        if(self::$votePlugin != ""){
            return self::$votePlugin->Render("ForumReponse", $reponse->IdEntite);
        }
       
        return "";
    }
} 


{{form->Open()}}
{{form->Error()}}
{{form->Success()}}

{{form->Render(Id)}}


    <div class='col-md-6'>
        <div class='block'>
            <h3>{{GetCode(Shop.DeliveryAddress)}}</h3>
            <div>
                <label>{{GetCode(Shop.Address)}}</label>
                {{form->Render(Address)}}
            </div>

            <div>
                <label>{{GetCode(Shop.ZipCode)}}</label>
                {{form->Render(ZipCode)}}
            </div>

            <div>
                <label>{{GetCode(Shop.City)}}</label>
                {{form->Render(City)}}
            </div>
        </div> 
    </div>

    <div class='col-md-6'>
        <div class='block'>
            <h3>{{GetCode(Shop.BillingAddress)}}</h3>
             <div>
                <label>{{GetCode(Shop.Address)}}</label>
                {{form->Render(AddressFacturation)}}
            </div>

            <div>
                <label>{{GetCode(Shop.ZipCode)}}</label>
                {{form->Render(ZipCodeFacturation)}}
            </div>

            <div>
                <label>{{GetCode(Shop.City)}}</label>
                {{form->Render(CityFacturation)}}
         </div>
        </div>  
    </div>


{{form->Close()}}
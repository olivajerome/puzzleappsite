<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Mooc\Entity;

use Core\Control\Image\Image;
use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Utility\Format\Format;
use Core\Core\Request;
use Apps\Mooc\Entity\MoocLesson;

class MoocMooc extends Entity {

    //Constructeur
    function __construct($core) {
        //Version
        $this->Version = "2.0.0.0";

        //Nom de la table 
        $this->Core = $core;
        $this->TableName = "MoocMooc";
        $this->Alias = "MoocMooc";

        $this->UserId = new Property("UserId", "UserId", NUMERICBOX, true, $this->Alias);
        $this->CategoryId = new Property("CategoryId", "CategoryId", NUMERICBOX, true, $this->Alias);
        $this->Name = new Property("Name", "Name", TEXTBOX, true, $this->Alias);
        $this->Code = new Property("Code", "Code", TEXTBOX, true, $this->Alias);
        $this->Description = new Property("Description", "Description", TEXTAREA, true, $this->Alias);
        $this->DateCreated = new Property("DateCreated", "DateCreated", DATEBOX, true, $this->Alias);

        //Repertoire de stockage des images    
        $this->DirectoryImage = "Data/Apps/Mooc/";

        //Creation de l entité 
        $this->Create();
    }

    /*
     * Get the code Formated for url
     */

    public function GetCode() {
        return Format::ReplaceForUrl($this->Name->Value);
    }

    /*
     * Get the Image og the article
     */

    function GetImage() {
        $fileName = "Data/Apps/Mooc/" . $this->IdEntite . "/thumb.png";

        if (file_exists($fileName)) {
            $image = new Image($this->Core->GetPath("/" . $fileName));
            $image->Title = $this->Name->Value;
        } else {
            $image = new Image($this->Core->GetPath("/images/nophoto.png"));
        }

        $image->Style = 'width:100px';

        return $image->Show();
    }

    /*
     * Get the Image of the article
     */

    function GetImagePath() {
        $fileName = "/Data/Apps/Mooc/" . $this->IdEntite . "/thumb.png";

        return $this->Core->GetPath($fileName);
    }

    /*
     * Get the Image og the article
     */

    function GetImagefullPath() {
        $fileName = "/Data/Apps/Mooc/" . $this->IdEntite . "/full.png";

        return $this->Core->GetPath($fileName);
    }

    /*     * *
     * Get the lessonn of the Mooc
     */

    public function GetLessons($array = false) {

        $lesson = new MoocLesson($this->Core);
        $lessons = $lesson->Find("MoocId = " . $this->IdEntite);

        return $array ? $lesson->ToAllArray($lessons) : $lessons;
    }

    /*     * *
     * Number User
     */

    public function GetNumberUser() {
        $request = "Select count(Id) as nbUser From MoocMoocUser Where MoocId=" . $this->IdEntite;
        $result = $this->Core->Db->GetLine($request);

        return $result["nbUser"];
    }

    /*     * *
     * Search un tutoriel
     */
    function SearchMooc() {
        $request = "select * From `MoocMooc` where Name like '" . Format::EscapeString(Request::GetPost("sourceControl")) . "%'";

        $results = $this->Core->Db->GetArray($request);

        $html = "<ul>";

        if (sizeof($results) > 0) {
            foreach ($results as $result) {
                
                $controlId =Request::GetPost("sourceControlId") ;
                $name=  $result["Name"] ;
                
                $html .= "<li onclick='AutoCompleteBox.SetResult(this,  \"$controlId\");' id='$name' >" . $name. "</li>";
            }
        } else {
            $html .= "<li>" . $this->Core->GetCode("NoResult") . "</li>";
        }

        $html .= "</ul>";
        echo $html;
    }
}

?>
<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Shop\Module\Member;

use Core\Control\Button\Button;
use Core\Controller\Controller;
use Core\View\View;
use Core\View\ElementView;
use Core\Control\TabStrip\TabStrip;
use Core\Control\EntityGrid\EntityGrid;
use Core\Control\EntityGrid\EntityColumn;
use Core\Control\EntityGrid\EntityIconColumn;
use Core\Control\EntityGrid\EntityFunctionColumn;
use Core\Entity\Entity\Argument;
use Apps\Shop\Helper\ShopHelper;
use Apps\Shop\Helper\CommandHelper;

use Apps\Shop\Widget\ShopForm\ShopForm;

/*
 * 
 */
 class MemberController extends Controller
 {
    /**
     * Constructeur
     */
    function _construct($core="")
    {
          $this->Core = $core;
    }

    /**Master Page */
    function Index(){

        $view = new View(__DIR__."/View/index.tpl", $this->Core);
      
        $view->AddElement(new ElementView("Commands", CommandHelper::GetByUser($this->Core)));


        return $view->Render();
     }
}
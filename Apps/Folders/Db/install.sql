CREATE TABLE IF NOT EXISTS `FolderFolder` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`UserId` INT  NOT NULL,
`Name` VARCHAR(200)  NOT NULL,
`Code` VARCHAR(200)  NOT NULL,
`Description`  VARCHAR(500)  NOT NULL,
`Url` TEXT  NOT NULL,
`Status` INT  NOT NULL,
`ParentId` INT  NULL ,
`AppName` VARCHAR(200)  NULL ,
`AppId` INT  NULL ,
`EntityName` VARCHAR(200)  NULL ,
`EntityId` INT  NULL ,
PRIMARY KEY (`Id`),
CONSTRAINT `ee_user_FolderFolder` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `FolderFiles` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`UserId` INT  NOT NULL,
`Name` TEXT  NOT NULL,
`Code` TEXT  NOT NULL,
`Url` TEXT  NOT NULL,
`Description` TEXT  NOT NULL,
`FolderId` INT  NULL,
`DateCreated` DATE  NOT NULL,
`DateUpdated` DATE  NOT NULL,
`Type` INT  NOT NULL,
PRIMARY KEY (`Id`),
CONSTRAINT `ee_user_FolderFiles` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`),
CONSTRAINT `FolderFolder_FolderFiles` FOREIGN KEY (`FolderId`) REFERENCES `FolderFolder`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `FolderFolderShared` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`FolderId` INT  NOT NULL,
`UserId` INT  NOT NULL,
PRIMARY KEY (`Id`),
CONSTRAINT `FolderFolder_FolderFolderShared` FOREIGN KEY (`FolderId`) REFERENCES `FolderFolder`(`Id`),
CONSTRAINT `ee_user_FolderFolderShared` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `FolderFileShared` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`FileId` INT  NOT NULL,
`UserId` INT  NOT NULL,
PRIMARY KEY (`Id`),
CONSTRAINT `FolderFile_FolderFileShared` FOREIGN KEY (`FileId`) REFERENCES `FolderFile`(`Id`),
CONSTRAINT `ee_user_FolderFileShared` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
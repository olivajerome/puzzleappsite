<section class="container">

    <input type='hidden' id='productId' value  ='{{Product->IdEntite}}' />
    
    <h1>{{GetCode(Shop.Product)}} :
        {{Product->Name->Value}}
    </h1>

    <div class='row'>
        <div class='col-md-6'>
            {{Apps\Shop\Decorator\ProductDecorator->GetProductImages()}}


            {{socialPlugin}}
        </div>

        <div class='col-md-6'>
        <h1>{{Product->Price->Value}}&euro;</h1>
        <h2>{{GetCode(Shop.Description)}}</h2>   
        <p>{{Product->Description->Value}}</p>
        <h2>{{GetCode(Shop.Caracteristiques)}}</h2>
            {{foreach Caracteristiques}}
            <div class='col-md-4'>
                <label>{{element->CaracteristiqueName->Value}}</label>
                <p>{{element->CaracteristiqueItemName->Value}}</p>
            </div>
            {{/foreach Caracteristiques}}
       
            <div class='col-md-12'>
                <div class='col-md-6'>
                    {{votePlugin}}
                </div>    
                <div class='col-md-6'>
                    {{ratingPlugin}}
                </div>    
            </div>

            <div class='col-md-12'>
        
                {{GetWidget(Shop,AddToCard,{{Product->IdEntite}})}}
                
                <div class='center'>
                    {{GetControl(Button,btnMakeOffre,{LangValue=Market.MakeAnOffer,Id=btnMakeOffre,CssClass=btn btn-secondary,})}}
                </div>
            </div>
        </div>

        <div class='col-md-12'>
            {{commentPlugin}}
        </div>
 </div>
</section>

{{votePluginScript}}
{{commentPluginScript}}
{{ratingPluginScript}}
           
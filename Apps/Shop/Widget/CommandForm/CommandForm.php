<?php

namespace Apps\Shop\Widget\CommandForm;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;
use Core\Core\Request;

use Apps\Shop\Widget\AdressForm\AdressForm;

class CommandForm extends Form {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "") {
        $this->Core = $core;
        $this->Data = $data;

        //On le fait ici pour avoir le formuaire � l'affichage mais aussi � la validation
        $this->Init($data);
    }

    /**
     * Initialisation 
     */
    function Init($data) {
        $this->form = new Form("CommandFormForm", $data);

        $this->form->SetView(__DIR__ . "/View/index.tpl", $this->Core);
        $this->form->SetAction("/View/index.tpl");

        $this->form->Add(array("Type" => "Hidden",
            "Id" => "Id",
        ));
        
        $this->form->Add(array("Type" => "Button",
            "Id" => "btnPrevious",
            "Value" => $this->Core->GetCode("Shop.Previous"),
            "CssClass" => "btn btn-primary"
        ));

        $this->form->Add(array("Type" => "Button",
            "Id" => "btnNext",
            "Value" => $this->Core->GetCode("Shop.Next"),
            "CssClass" => "btn btn-primary"
        ));
   

        $cart = json_decode(Request::GetSession("Cart"));
   
        if($cart == null){
            return $this->Core->GetCode("Shop.YouCartIsEmpty");
        }
         
         $total = 0;
         
         if(isset($cart->lines)){
             
             $html = "<table class='cartTable grid'>";
             $html .= "<tr><th>".$this->Core->GetCode("Shop.Product")."</th><th>".$this->Core->GetCode("Shop.Price")."</th></tr>";
             
             foreach($cart->lines as $line){
                 
                  $html .= "<tr><td>".$line->Name."<i  style='float:right'  id='".$line->IdEntite."' class='fa fa-trash removeCartLine'>&nbsp;</i></td><td>".$line->Price."&euro;</td></tr>";
                  
                  $total += (float)$line->Price;
             }
             
             $html .= "<tr><td><b>Total</b></td><td><b>".$total."&euro;</b></td></tr>";
             
             $html .= "</table>";
         }
         
         $this->form->AddElementView(new ElementView("table", $html));

         $addressForm = new AdressForm($this->Core);
         $this->form->AddElementView(new ElementView("adressForm", $addressForm->Render()));
         

        $pluginPayement = \Apps\EeApp\Helper\AppHelper::GetPluginType($this->Core, "Shop", "Paid");
        
        if($pluginPayement != ""){
            $this->form->AddElementView(new ElementView("PayPlugin", $pluginPayement->Render()));
        } else {
            $this->form->AddElementView(new ElementView("PayPlugin", $this->RenderNoPayPlugin()));
        }
    }

    /***
     * Render No Pay information
     */
    function RenderNoPayPlugin(){
        return $this->Core->GetCode("Shop.InformationNoPayPlugin");
    }
    
    /*     * *
     * G�n�rer les formulaire
     */

    function Render() {
        return $this->form->Render();
    }

    /*     * *
     * Valide les donn�es selon les diff�rents formulaires
     */

    function Validate($data = "") {
        $errors = $this->form->Validate($this->Data);

        return $errors;
    }

    /*     * *
     * Charge les controls avec les donn�es de l'entit�
     */

    function Load($entity) {
        $this->form->Load($entity);
    }

    /*     * *
     * Charge l'entit� avec les donn�es des diff�rents formulaire
     */

    function Populate($entity) {
        
    }

    /*     * *
     * Gestion des erreurs
     */

    function Error() {
        $errors[] = $this->form->errors;

        return json_encode(array("statut" => "Error", "message" => $errors));
    }

}

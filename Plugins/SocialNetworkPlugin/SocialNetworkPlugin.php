<?php

namespace Plugins\SocialNetworkPlugin;

use Core\View\View;
use Core\View\ElementView;

class SocialNetworkPlugin{

    function __construct($core){
        $this->Core = $core;
    }
    
    function GetType(){
        return "Social";
    }
    
    function Render(){
       
        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);
        return $view->Render();
    }

    function RenderConfigForm(){
        $view = new View(__DIR__ . "/View/config.tpl", $this->Core);
        return $view->Render();
    }
    
}
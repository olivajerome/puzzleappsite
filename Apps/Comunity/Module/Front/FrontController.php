<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Apps\Comunity\Module\Front;

use Core\Control\Button\Button;
use Core\Controller\Controller;
use Core\View\View;
use Core\View\ElementView;
use Core\Control\Upload\Upload;
use Core\Entity\Entity\Argument;
use Apps\Comunity\Widget\UserProjetWidget\UserProjetWidget;
use Apps\Comunity\Widget\ActionProjetWidget\ActionProjetWidget;
use Apps\Comunity\Widget\MemberProjetWidget\MemberProjetWidget;
use Apps\Comunity\Helper\ComunityHelper;
use Apps\Comunity\Entity\ComunityComunity;
use Apps\Comunity\Entity\ComunityFollow;

class FrontController extends Controller {

    /**
     * Constructeur
     */
    function __construct($core = "") {
        $this->Core = $core;
    }

    /**
     * Page d'accueil
     */
    function Index() {

        $comunity = ComunityHelper::GetDefault($this->Core);

        //Information Page
        $this->Core->MasterView->Set("Title", "La communauté");
        $this->Core->MasterView->Set("Description", $comunity->Description->Value);

        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);

        $comunity = ComunityHelper::GetDefault($this->Core);

        $view->AddElement(new ElementView("Comunity", $comunity));
        $view->AddElement(new ElementView("isConnected", $this->Core->IsConnected()));

        if ($this->Core->IsConnected()) {
            $view->AddElement(new ElementView("userImage", $this->Core->User->GetImageMini()));
        }

        $uploadDiscuss = new Upload("Discuss", "uploadDiscuss", "", "uploadDiscuss");
        $uploadDiscuss->RenderType = "Icone";
        $uploadDiscuss->Accept = "";

        $view->AddElement(new ElementView("uploadDiscuss", $uploadDiscuss->Show()));

        $view->AddElement(new ElementView("Messages", $this->GetMessages($comunity->IdEntite)));

        $view->AddElement(new ElementView("groupes", ComunityHelper::GetGroupes($this->Core)));

        return $view->Render();
    }

    /**
     * Info pour la page d'accueil
     */
    function LoadInfo($reseauCode) {
        $view = new View(__DIR__ . "/View/loadInfo.tpl", $this->Core);

        $view->AddElement(new ElementView("reseau", $reseauCode));

        $projets = DiscussHelper::GetLastRencontreByReseau($this->Core, $reseauCode);

        $view->AddElement(new ElementView("Projets", $projets));
        $view->AddElement(new ElementView("ProjetsImage", $projets));

        return $view->Render();
    }

    /**
     * Ajoute un message à la Comunityion courante
     * @param type $users
     * @param type $message
     * @param type $objet
     * @param type $ComunityId
     * @param type $images
     * @param type $type
     * @param type $reseauCode
     * @param type $documents
     * @param type $typeMessage
     */
    function AddMessage($users, $message, $objet, $ComunityId, $images = "", $type = 0, $reseauCode = "", $documents = "", $typeMessage = "") {
        //TODO DEPLACER CETTE METHOSE DANS UN HELPER
        if ($images != "") {
            $images = explode(";", $images);
            $messageImage = "";

            $messageId = ComunityHelper::AddMessage($this->Core, $ComunityId, $message);

            //On déplace les images dans le bon répertoire
            foreach ($images as $image) {
                $path = explode("/", $image);

                if (!file_exists("Data/Apps/Comunity/" . $ComunityId)) {
                    mkdir("Data/Apps/Comunity/" . $ComunityId);
                }
                if (!file_exists("Data/Apps/Comunity/" . $ComunityId . "/Message")) {
                    mkdir("Data/Apps/Comunity/" . $ComunityId . "/Message");
                }

                //TODO RECUPERE L'ID DE L4UTILISATEUR SI C'EST UNE INSCIRPTIon
                if (!file_exists("Data/Apps/Comunity/" . $ComunityId . "/Message/" . $messageId)) {
                    mkdir("Data/Apps/Comunity/" . $ComunityId . "/Message/" . $messageId);
                }

                $fileName = $path[count($path) - 1];

                rename("Data/Tmp/" . $fileName, "Data/Apps/Comunity/" . $ComunityId . "/Message/" . $messageId . "/" . $fileName);

                $messageImage .= "<img src='" . $this->Core->GetPath("/Data/Apps/Comunity/" . $ComunityId . "/Message/" . $messageId . "/" . $fileName) . "' >";
            }

            // C'est un fichier Type Image
            $message = "<p>" . $message . "</p>" . $messageImage;

            ComunityHelper::UpdateMessage($this->Core, $messageId, $message);
        } else if ($documents != "") {
            $documents = explode(";", $documents);
            $messageImage = "";

            $messageId = ComunityHelper::AddMessage($this->Core, $ComunityId, $message);

            //On déplace les images dans le bon répertoire
            foreach ($documents as $document) {
                $path = explode("/", $document);

                if (!file_exists("Data/Apps/Comunity/" . $ComunityId)) {
                    mkdir("Data/Apps/Comunity/" . $ComunityId);
                }

                if (!file_exists("Data/Apps/Comunity/" . $ComunityId . "/Message")) {
                    mkdir("Data/Apps/Comunity/" . $ComunityId . "/Message");
                }

                //TODO RECUPERE L'ID DE L4UTILISATEUR SI C'EST UNE INSCIRPTIon
                if (!file_exists("Data/Apps/Comunity/" . $ComunityId . "/Message/" . $messageId)) {
                    mkdir("Data/Apps/Comunity/" . $ComunityId . "/Message/" . $messageId);
                }

                $fileName = $path[count($path) - 1];

                rename("Data/Tmp/" . $fileName, "Data/Apps/Comunity/" . $ComunityId . "/Message/" . $messageId . "/" . $fileName);

                $messageDocument .= "<a  target='_blank' href='" . $this->Core->GetPath("/Data/Apps/Comunity/" . $ComunityId . "/Message/" . $messageId . "/" . $fileName) . "' ><i class='fa fa-file fa-2x' >&nbsp;</i>" . $fileName . "</a>";
            }

            // C'est un fichier Type Document
            $message = "<p>" . $message . "</p>" . $messageDocument;

            ComunityHelper::UpdateMessage($this->Core, $messageId, $message);
        } else if ($ComunityId == "") {
            $ComunityId = ComunityHelper::CreateConversation($this->Core, $users, $message, $objet);
            $messageId = $this->Core->Db->GetInsertedId();
        } else {

            $message = $message;

            $messageId = ComunityHelper::AddMessage($this->Core, $ComunityId, $message);
            ComunityHelper::UpdateLastMessage($this->Core, $ComunityId, $message);
        }

        return json_encode(array("discuId" => $ComunityId, "message" => $this->GetMessages($ComunityId, $messageId)));
    }

    /**
     * Détail d'un rencontre thématique
     */
    function Group($groupId, $completePage = true) {
        if (!$this->Core->IsConnected()) {
            $view = new View(__DIR__ . "/View/notConnected.tpl", $this->Core);

            return $view->Render();
        }

        $Comunity = new ComunityComunity($this->Core);

        $view = new View(__DIR__ . "/View/detailGroup.tpl", $this->Core);

        $view->AddElement(new ElementView('completePage', $completePage));
        $Comunity->GetById($groupId);

        $userProjetWidget = new UserProjetWidget($this->Core);
        $view->AddElement(new ElementView("userProjetWidget", $userProjetWidget->Render()));

        $actionProjetWidget = new ActionProjetWidget($this->Core, $groupId);
        $view->AddElement(new ElementView("ActionProjetWidget", $actionProjetWidget->Render()));

        $memberProjetWidget = new MemberProjetWidget($this->Core, $groupId, $Comunity);
        $view->AddElement(new ElementView("MemberProjetWidget", $memberProjetWidget->Render()));

        $view->AddElement(new ElementView("membres", $this->GeMembers($Comunity)));
        $view->AddElement(new ElementView("followers", $this->GetFollower($Comunity)));

        $view->AddElement(new ElementView("Comunity", $Comunity));

        if ($this->Core->IsConnected()) {
            $view->AddElement(new ElementView("userImage", $this->Core->User->GetImage()));
        }

        $view->AddElement(new ElementView("isConnected", $this->Core->IsConnected()));

        $messages = $Comunity->GetFirstMessage();

        $view->AddElement(new ElementView("Messages", $this->GetMessages($groupId, "", $reseauId)));
        $view->AddElement(new ElementView("nbCommentaire", count($messages)));

        //Upload Image
        // $uploadComunity = new Upload("Comunity", "uploadComunity", "", "uploadComunity");

        $uploadComunity = new Upload("Comunity", "uploadComunity", "", "uploadComunity");
        $uploadComunity->RenderType = "Icone";
        $uploadComunity->Accept = "";

        $view->AddElement(new ElementView("uploadComunity", $uploadComunity->Show()));

        return $view->Render();
    }

    /*     * *
     * Retourne tous les messages
     */

    function GetMessages($ComunityId, $messageId = "", $projetId = "", $page = "", $postId = "") {

        $view = new View(__DIR__ . "/View/detailMessage.tpl", $this->Core);

        $request = "SELECT message.Id as messageId, message.Message as text, message.DateCreated as dateCreated, 
                    user.Pseudo as pseudo, 
                    user.FirstName as FirstName, 
                    user.Id as UserId,

                    user.Image as image,
                    message.type as messageType,            

                    count(distinct(lk.id)) as nbLike,
                    count(distinct(comment.Id)) as nbComment,
                    GROUP_CONCAT(distinct(userLike.Id) SEPARATOR ':') as userLike ,
                    
                    comunity.type as discusType, comunity.Title as discussTitle,
                    
                    comunity.Id as projetId


                    FROM ComunityMessage as message 

                    JOIN ee_user as user on user.Id = message.UserId 
                    JOIN ComunityComunity as comunity on message.ComunityId = comunity.Id   

                    LEFT JOIN ComunityComment as comment on comment.MessageId = message.Id
                    LEFT JOIN ComunityLike as lk on lk.EntityId = message.Id and lk.EntityName = 'message'
                    LEFT JOIN ee_user as userLike on userLike.id = lk.UserId
                    ";

         if ($ComunityId != "") {
            $request .= " Where comunity.Id =  " . $ComunityId;
        }
        else if ($messageId != "") {
            $request .= " WHERE message.Id =  " . $messageId;
        } 
        
       

        if ($projetId != "") {
            $request .= " AND ComunityId in(" . $projetId . ")";
        }

        if ($postId != "") {
            $request .= " WHERE message.Id = " . $postId;
        }
        $request .= " GROUP BY message.Id Order By message.Id Desc ";

        if ($page == "") {
            $request .= " limit 0,5";
        } else {
            $request .= " limit " . ($page * 5) . ",5";
        }

        $results = $this->Core->Db->GetArray($request);

        foreach ($results as $message) {

            if ($message["messageType"] == 5) {
                $view = new View(__DIR__ . "/View/otherMessage.tpl", $this->Core);
            } else {
                $view = new View(__DIR__ . "/View/detailMessage.tpl", $this->Core);
            }

            $userId = $message["UserId"];

            if (file_exists("Data/Apps/Profil/User/" . $userId . "/thumb.png")) {
                $image = $this->Core->GetPath(str_replace("Data", "/Data", "Data/Apps/Profil/User/" . $userId . "/thumb.png"));
            } else {
                $image = "/images/noprofil.png";
            }

            $view->AddElement(new ElementView("image", $image));
            $view->AddElement(new ElementView("pseudo", $message["pseudo"] ? $message["pseudo"] : $message["FirstName"]));
            $view->AddElement(new ElementView("text", str_replace("&acute;", "'", $message["text"])));
            $view->AddElement(new ElementView("nbLike", $message["nbLike"]));
            $view->AddElement(new ElementView("nbComment", $message["nbComment"]));
            $view->AddElement(new ElementView("messageId", $message["messageId"]));

            if ($this->Core->IsConnected()) {
                $view->AddElement(new ElementView("userImage", $this->Core->User->GetImageMini()));
                $view->AddElement(new ElementView("moreAction", "<div class='moreAction' data-type='message'  style='float:right'>
                ...  
        </div>"));
            } else {
                $view->AddElement(new ElementView("moreAction", ""));
            }

            $view->AddElement(new ElementView("isConnected", $this->Core->IsConnected()));

            $view->AddElement(new ElementView("projet", ($message["discusType"] == 2 ) ? "<span class='fa fa-circle' style='font-size:6px;margin-left:-2px'></span><span class='projet'><a href='" . $this->Core->GetPath("/" . $reseau->Code->Value . "/ParlonsEn/Rencontre/" . $message["projetId"]) . "'>" . $message["discussTitle"] . "</a></span>" : ""));

            $dateNow = new \DateTime("now");
            $dateModif = new \DateTime($message["dateCreated"]);
            $intervalle = $dateNow->diff($dateModif);

            $view->AddElement(new ElementView("day", $intervalle->d . " jours"));

            if ($postId != "") {

                $discuss = DashBoardManager::GetApp("Discuss", $this->Core);
                $view->AddElement(new ElementView("comments", $discuss->GetComment("", $postId)));
            } else {
                $view->AddElement(new ElementView("comments", ""));
            }

            if ($message["userLike"] != "") {
                $users = explode(":", $message["userLike"]);
                $userLikeHtml = "";

                foreach ($users as $user) {

                    if (file_exists("Data/Apps/Profil/User/" . $user . "/thumb.png")) {
                        $image = $this->Core->GetPath(str_replace("Data", "/Data", "Data/Apps/Profil/User/" . $user . "/thumb.png"));
                    } else {
                        $image = "/images/noprofil.png";
                    }
                    $userLikeHtml .= '<span class="imgProfilSmall" style="background-image: url(' . $image . ')"></span>';
                }


                $view->AddElement(new ElementView("userLike", $userLikeHtml));
            } else {
                $view->AddElement(new ElementView("userLike", ""));
            }

            $html .= $view->Render();
        }

        return $html;
    }

    /*     * *
     * Retourne les photos des membres
     */

    function GeMembers($projet) {
        $members = $projet->GetProjetMember();

        $html = "";
        foreach ($members as $member) {
            $html .= '<div class="avatarmini-img avatarmini" style="cursor: pointer;">';
            $html .= '<img src="' . $member->User->Value->GetImage() . '" alt="images" /> ';
            $html .= ' </div>';
        }

        if (count($members) > 1) {
            $memberNumber = count($members) . " membres";
        } else {
            $memberNumber = count($members) . " membre";
        }

        return $html . "<span style='margin-left: 50px'>" . $memberNumber . "</span>";
    }

    /*     * *
     * Obitient le nombre de follower
     */

    function GetFollower($projet) {

        $follow = new ComunityFollow($this->Core);
        $follower = $follow->Find("ComunityId=" . $projet->IdEntite);

        if (count($follower) > 1) {
            return count($follower) . " abonnés";
        } else {
            return count($follower) . " abonné";
        }
    }
}

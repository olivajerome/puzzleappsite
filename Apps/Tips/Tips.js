var Tips = function() {};

	/*
	* Chargement de l'application
	*/
	Tips.Load = function(parameter)
	{
		this.LoadEvent();

		Tips.InitTabTips();
	};

	/*
	* Chargement des �venements
	*/
	Tips.LoadEvent = function()
	{
		Dashboard.AddEventAppMenu(Tips.Execute, "", "Tips");
		Dashboard.AddEventWindowsTool("Tips");
	};

   /*
	* Execute une fonction
	*/
	Tips.Execute = function(e)
	{
		//Appel de la fonction
		Dashboard.Execute(this, e, "Tips");
		return false;
	};

	/*
	*	Affichage de commentaire
	*/
	Tips.Comment = function()
	{
		Dashboard.Comment("Tips", "1");
	};

	/*
	*	Affichage de a propos
	*/
	Tips.About = function()
	{
		Dashboard.About("Tips");
	};

	/*
	*	Affichage de l'aide
	*/
	Tips.Help = function()
	{
		Dashboard.OpenBrowser("Tips","{$BaseUrl}/Help-App-Tips.html");
	};

   /*
	*	Affichage de report de bug
	*/
	Tips.ReportBug = function()
	{
		Dashboard.ReportBug("Tips");
	};

	/*
	* Fermeture
	*/
	Tips.Quit = function()
	{
		Dashboard.CloseApp("","Tips");
	};

	/***
	 * Init type app
	 */
	Tips.Init = function(){

	};
        
	/***
	 * Init the tab Tips
	 */
	Tips.InitTabTips = function () {
	
		Event.AddById("btnAddTips", "click", () => {
			Tips.ShowAddTips("");
		});

		EntityGrid.Initialise('gdTips');
	};

	/****
 * Edit a tips
 * @param {type} tipsId
 * @returns {undefined}
 */
Tips.EditTips = function (tipsId) {
    Tips.ShowAddTips(tipsId);
};

/**
 * Edit a Tips
 * @param {type} moocId
 * @returns {undefined}
 */
Tips.ShowAddTips = function (tipsId) {

    Dialog.open('', {"title": Dashboard.GetCode("Tips.AddTips"),
        "app": "Tips",
        "class": "DialogAdminTips",
        "method": "AddTips",
        "type": "right",
        "params": tipsId
    });
};

/***
 * Rafraichit la tab des tips
 * @returns {undefined}
 */
Tips.ReloadTips = function (data) {
    Dashboard.StartApp("", "Tips", "");
};

/***
 * Supprime un tips
 * @param {type} tips
 * @returns {undefined}
 */
Tips.DeleteTips = function (tipsId) {

    Animation.Confirm(Language.GetCode("Tips.ConfirmRemoveTips"), () => {

        let data = "Class=Tips&Methode=DeleteTips&App=Tips";
        data += "&tipsId=" + tipsId;

        Request.Post("Ajax.php", data).then(data => {
            Tips.ReloadTips(data);
        });
    });
};

	/***
	 * Affiche un dialogue de droite avec le derniers tips a affiché pour l'utilisateur
	 * @returns {undefined}
	 */
	Tips.Show = function(tips){
		
		if(tips == undefined){
			tips = "last";
		}
		
		Dialog.open('', {"title": Dashboard.GetCode("Tips.NewTips"),
				"app"   : "Tips",
				"class" : "DialogTips",
				"method" : "ShowTips",
				"params" : tips,
				"type"   :"right"    });
	};
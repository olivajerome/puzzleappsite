var WorkFlowActionForm = function(){};

/***
 * Init the form
 * @returns {undefined}
 */
WorkFlowActionForm.Init = function(){
    Event.AddById("btnSaveAction", "click", WorkFlowActionForm.SaveElement);
};

/***
* Get the Id of element 
*/
WorkFlowActionForm.GetId = function(){
    return Form.GetId("WorkFlowActionFormForm");
};

/*
* Save the Element
*/
WorkFlowActionForm.SaveElement = function(e){
   
    if(Form.IsValid("WorkFlowActionFormForm"))
    {
    let request = new Http("WorkFlow", "SaveAction");
        request.SetData(Form.Serialize("WorkFlowActionFormForm"));

        request.Post(data => {

            data = JSON.parse(data);

            if(data.status == "error"){
                Form.RenderError("WorkFlowActionFormForm", data.data.message);
            } else{

                Form.SetId("WorkFlowActionFormForm", data.data.Id);

                Dialog.Close();
            }
        });
    }
};


<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\WorkFlow;

use Core\Core\Core;
use Core\Core\Request;
use Core\Core\Response;
use Apps\Base\Base;


use Apps\WorkFlow\Module\Admin\AdminController;

class WorkFlow extends Base {

    /**
     * Author and version
     * */
    public $Author = 'Webemyos';
    public $Version = '1.0.0';

    /**
     * Constructeur
     * */
    function __construct() {
        $this->Core = Core::getInstance();
        parent::__construct($this->Core, "WorkFlow");
    }

    /**
     * Set public route
     */
    function GetRoute($routes = "") {
        $this->Route->SetPublic(array());

        return $this->Route;
    }

    /*     * *
     * Execute action after install
     */

    function PostInstall() {
        \Apps\EeApp\Helper\AppHelper::UpdateAppParams($this->Core,
                "WorkFlow",
                $this->Version,
                "", // Add Description of the app
                0, // Set 1 if the App have a AdminWidgetDashBoard
                0  // Set 1 if the App have a Member Module
        );
    }

    /**
     * Start Admin
     */
    function Run() {
        echo parent::RunApp($this->Core, "WorkFlow", "WorkFlow");
    }

    /***
     * Save the Workflow
     */
    function SaveWorkFlow(){
      
      $adminController = new AdminController($this->Core);
      $result = $adminController->SaveWorkFlow(Request::GetPosts());
 
      if ($result === true) {
        return Response::Success($adminController->GetTabWorkflow());
      } else {
        return Response::Error( $result);
     }
    }

    /***
     * Delete a workflow
     */
    function DeleteWorkFlow(){

      $adminController = new AdminController($this->Core);
      $result = $adminController->DeleteWorkFlow(Request::GetPost("WorkFlowId"));
 
      if ($result === true) {
        return Response::Success($adminController->GetTabWorkflow());
      } else {
        return Response::Error( $result);
     }
    }

    /**
     * Add Trigger and Action On a workflow 
     */
    function AddTriggerActions(){
      
      $adminController = new AdminController($this->Core);
      $result = $adminController->AddTriggerActions(Request::GetPosts());
 
      if ($result === true) {
        return Response::Success(array("Message" => $this->Core->GetCode("WorkFlow.WorkflowTriggerActionUpdated")));
      } else {
        return Response::Error( $result);
     }
    }

    /***
     * Save a action
     */
    function SaveAction(){

      $adminController = new AdminController($this->Core);
      $result = $adminController->SaveAction(Request::GetPosts());
 
      if ($result === true) {
        return Response::Success(array("Message" => $this->Core->GetCode("WorkFlow.WorkflowActionUpdated")));
      } else {
        return Response::Error( $result);
     }
    }

    /***
     * Remove a action
     */
    function RemoveAction(){
      $adminController = new AdminController($this->Core);
      $result = $adminController->RemoveAction(Request::GetPost("ActionId"));
 
      if ($result === true) {
        return Response::Success(array("Message" => $this->Core->GetCode("WorkFlow.WorkflowActionDeleted")));
      } else {
        return Response::Error( $result);
     }
    }

    /*     * *
     * Run member Controller
     * 
     * /
      /*function RunMember(){
      $memberController = new MemberController($this->Core);
      return $memberController->Index();
      } */

    /**
     * Get The siteMap 
     */
    /* public function GetSiteMap(){
      return SitemapHelper::GetSiteMap($this->Core);
      } */

    /*     * *
     * Get the widget
     */
    /* public function GetWidget($type = "", $params = "") {

      switch ($type) {
      case "AdminDashboard" :
      $widget = new \Apps\WorkFlow\Widget\AdminDashBoard\AdminDashBoard($this->Core);
      break;
      }

      return $widget->Render();
      } */

    /*     * *
     * Get Forum Addable widget
     */
    /* public function GetListWidget(){
      return array(array("Name" => "LastMessage",
      "Description" => $this->Core->GetCode("WorkFlow.DescriptionLastMessageWidget")),
      );
      } */
}

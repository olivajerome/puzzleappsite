<?php

namespace Apps\Mooc\Widget\LastMooc;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;
use Apps\Mooc\Helper\MoocHelper;

class LastMooc {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "") {
        $this->Core = $core;
    }

    /*     * *
     * render the last Mooc
     */

    function Render() {

        $mooc = MoocHelper::GetLast($this->Core);

        if ($mooc != null) {

            $view = new View(__DIR__ . "/View/index.tpl", $this->Core);
            $view->AddElement(new ElementView("Mooc", $mooc));
            return $view->Render();
        }

        return "";
    }
}

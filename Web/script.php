<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

include("../environment.php");
include("../autoload.php");

use Core\Core\Core;
use Core\Script\ScriptManager;
use Core\Core\Request;

$Core = Core::getInstance(GetEnvironnement(), false);
$Core->Init();

//Get a specific script
if (Request::Get("s")) {
    echo ScriptManager::Get(Request::Get("s"));

    return;
}

//Get a application script
if (Request::Get("a")) {
    echo ScriptManager::GetApp(Request::Get("a"), Request::Get("m"), "js");
    return;
}

 if (Request::Get("apps")) {
    echo ScriptManager::GetApps($Core, Request::Get("apps"), "js");
    return;
}

if (Request::Get("p")) {
    return ScriptManager::GetPlugin(Request::Get("p"), "js");
}

if (Request::Get("t")) {
    echo ScriptManager::GetTemplate($Core, Request::Get("t"), "js" );
    return;
}


if (Request::Get("w")) {
    echo ScriptManager::GetWidget(Request::Get("w"));
}
//Get all Script of the framework
else {
    echo ScriptManager::GetAllJs();
    
    //Create Js Variable for the Installed App
    echo ScriptManager::LoadApp();
}
<section>
      {{GetControl(Button,btnHide,{LangValue=TestEvent,CssClass=btn btn-primary,Id=btnTestEvent,)})}}
    
      
      <ul>
          <li class='item' id='1'>Item 1</li>
          <li class='item' id='2'>Item 2</li>
          <li class='item' id='3'>Item 3</li>
          
      </ul>
    
</section>
      
      
      <script>
          Event.AddById("btnTestEvent", "click", function(){
              Animation.Notify("You have clicked on the button");
          });
    
          Event.AddByClass("item", "click", function(e){
              let element = e.srcElement;
              Animation.Notify("You have clicked on the item  " + element.id);
          });
    
      </script>

<?php

namespace Apps\Blog\Widget\LastArticle;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;
use Apps\Blog\Helper\ArticleHelper;

class LastArticle {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "") {
        $this->Core = $core;
    }

    /*     * *
     * render the lAst Article
     */

    function Render() {

        $article = ArticleHelper::GetLast($this->Core);

        if ($article != null) {

            $view = new View(__DIR__ . "/View/index.tpl", $this->Core);
            $view->AddElement(new ElementView("Article", $article));
            return $view->Render();
        }

        return "";
    }
}

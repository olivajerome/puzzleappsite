<?php

namespace Apps\Downloader\Widget\DetailRessource;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;
use Apps\Downloader\Entity\DownloaderRessource;

class DetailRessource {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "") {
        $this->Core = $core;
        $this->Data = $data;
    }

    function Render($params) {

        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);

        if (is_numeric($params)) {
            $downloadRessource = new DownloaderRessource($this->Core);
            $downloadRessource->GetById($params);

            $view->AddElement(new ElementView("Ressource", $downloadRessource));
        } else {
            $ressource = new DownloaderRessource($this->Core);
            $ressource = $ressource->GetByCode($params);
            $view->AddElement(new ElementView("Ressource", $ressource));
        }

        return $view->Render();
    }
}

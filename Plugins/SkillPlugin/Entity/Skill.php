<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Plugins\SkillPlugin\Entity;

use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class Skill extends Entity {

    //Constructeur
    function __construct($core) {
        //Version
        $this->Version = "2.0.0.0";

        //Nom de la table 
        $this->Core = $core;
        $this->TableName = "Skill";
        $this->Alias = "Skill";

        $this->Name = new Property("Name", "Name", TEXTBOX, false, $this->Alias);
        $this->Description = new Property("Description", "Description", TEXTBOX, false, $this->Alias);
        $this->CategoryId = new Property("CategoryId", "CategoryId", NUMERICBOX, false, $this->Alias);
    
        //Creation de l entité 
        $this->Create();
    }
}

?>
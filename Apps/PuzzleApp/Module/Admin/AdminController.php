<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\PuzzleApp\Module\Admin;

use Core\Control\Button\Button;
use Core\Controller\Controller;
use Core\View\View;
use Core\View\ElementView;

use Core\Control\TabStrip\TabStrip;

use Apps\EeApp\Entity\EeAppApp;
use Apps\EeApp\Entity\EeAppPlugin;
use Apps\EeApp\Entity\EeAppTemplate;


/*
 * 
 */
 class AdminController extends Controller
 {
    /**
     * Constructeur
     */
    function _construct($core="")
    {
          $this->Core = $core;
    }

    /**
     * Creation
     */
    function Create()
    {
    }

    /**
     * Initialisation
     */
    function Init()
    {
    }

    /**
     * Affichage du module
     */
    function Show($all=true)
    {
         return $this->Index();
    }
    
    /*
    * Get the home page
    */
   function Index()
   {
       $view = new View(__DIR__."/View/index.tpl", $this->Core);

       $tabAdmin = new TabStrip("tabAdmin", "PuzzleApp");
       $tabAdmin->AddTab("Versioning", $this->GetTabVersionning());
       $tabAdmin->AddTab("Plugin Versioning", $this->GetTabPluginVersionning());
       $tabAdmin->AddTab("Template Versioning", $this->GetTabTemplateVersionning());
     
       $view->AddElement($tabAdmin);

       return $view->Render();
   }

   /***
    * Outil de versionning
    */
   function GetTabVersionning(){

     $view = new View(__DIR__."/View/versionning.tpl", $this->Core);

     $apps = new EeAppApp($this->Core);
     $view->AddElement(new ElementView("Apps", $apps->GetAll()));

     return  $view->Render();
   }

   /***
    * Outil de versionning
    */
   function GetTabPluginVersionning(){

     $view = new View(__DIR__."/View/versionningPlugin.tpl", $this->Core);

     $plugin = new EeAppPlugin($this->Core);
     $view->AddElement(new ElementView("Plugins", $plugin->GetAll()));

     return  $view->Render();
   }
   
   /***
    * Versionnind des template
    */
   function GetTabTemplateVersionning(){

     $view = new View(__DIR__."/View/versionningTemplate.tpl", $this->Core);

     $template = new EeAppTemplate($this->Core);
     $view->AddElement(new ElementView("Templates", $template->GetAll()));

     return  $view->Render();
   }
   
          
          /*action*/
 }?>
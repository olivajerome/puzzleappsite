<?php 
/*
* PuzzleApp
* Webemyos
* Jérôme Oliva
* GNU Licence
*/

namespace Apps\NewsLetter\Entity;
use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class NewsLetterEmail extends Entity  
{
	//Constructeur
	function __construct($core)
	{
		//Version
		$this->Version ="2.0.0.0"; 

		//Nom de la table 
		$this->Core=$core; 
		$this->TableName="NewsLetterEmail"; 
		$this->Alias = "NewsLetterEmail"; 

		$this->TypeId = new Property("TypeId", "TypeId", NUMERICBOX,  false, $this->Alias); 
		$this->Code = new Property("Code", "Code", TEXTBOX,  false, $this->Alias); 
		$this->Libelle = new Property("Libelle", "Libelle", TEXTBOX,  false, $this->Alias); 
		$this->Description = new Property("Description", "Description", TEXTAREA,  false, $this->Alias); 
		$this->Content = new Property("Content", "Content", TEXTAREA,  false, $this->Alias); 
		$this->DateCreated = new Property("DateCreated", "DateCreated", DATEBOX,  false, $this->Alias); 
		$this->Status = new Property("Status", "Status", NUMERICBOX,  false, $this->Alias); 

		//Creation de l entité 
		$this->Create(); 
	}
}
?>
/***
 * Class for plugin
 * @returns {undefined}
 */
Plugin = function () {};

/***
 * Define the default plugin to 
 * @param {type} pluginName
 * @param {type} pluginSource
 * @returns {undefined}
 */
Plugin.Register = function(pluginName, pluginSource, callBack){
    
    let script = document.createElement('script');
    script.setAttribute('type','text/javascript');
    script.setAttribute('src', "/script.php?p=" + pluginName);
    
    document.body.appendChild(script);
     
    script.onload = function() {
        //Set The PluginProperty to the correct plug    
        eval("Plugin." + pluginSource + "=" + pluginName + ";");
        
        if(callBack != undefined){
            callBack();
        }
     };
};

/***
 * Register plugin on Js Mode
 */
Plugin.RegisterJs = function(container){
    let pluginScript = document.getElementById(container);
    eval(pluginScript.innerHTML);
};

/***
 * SendRequest
 * @returns {undefined}
 */
Plugin.SendRequest = function(params, callBack){
  
     let data = "Class=DashBoardManager&Methode=RequestPlugin";
        data += "&Params="+JSON.stringify(params);
        
        Request.Post("Ajax.php", data).then(data => {
            callBack(data);
        });
};

<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Downloader\Helper;

use Apps\Downloader\Entity\DownloaderRessource;

class SitemapHelper {
    /*     * *
     * Obtient le site map du blog
     */

    public static function GetSiteMap($core, $returnUrl = false) {
        $urlBase = $core->GetPath("");
        $sitemap .= "<url><loc>$urlBase/Downloader</loc></url>";
        $urls = "";
       
        $ressource = new DownloaderRessource($core);
        $ressources = $ressource->GetAll();

        foreach ($ressources as $ressource) {
            $sitemap .= "<url><loc>$urlBase/Downloader/Documentation/" . $ressource->Code->Value . "</loc></url>";

            $url = $urlBase . "/Downloader/Documentation/" . $ressource->Code->Value;
            $urls .= '<br/><a target="__blank" href="' . $url . '">' . $url . '</a>';
        }

         if ($returnUrl == true) {
            return $urls;
        } else {
            return $sitemap;
        }
    }
}

<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */
include("../environment.php");
include("../autoload.php");

use Core\Script\ScriptManager;
use Core\Core\Request;
use Core\Core\Core;

$Core = Core::getInstance(GetEnvironnement(), true);
$Core->Init();

//Get a specific css
if(Request::Get("s"))
{
    echo ScriptManager::Get(Request::Get("s"));    
    return;
}
//Get a application css
if(Request::Get("a"))
{
    echo ScriptManager::GetApp(Request::Get("a"), "", "css"); 
    return;
}

if (Request::Get("t")) {
    echo ScriptManager::GetTemplate($Core, Request::Get("t"), "css" );
    return;
}

if (Request::Get("apps")) {
    echo ScriptManager::GetApps($Core, Request::Get("apps"), "css" );
}
//Get all css of the framework
else
{
    echo ScriptManager::GetAllJs();    
}

?>

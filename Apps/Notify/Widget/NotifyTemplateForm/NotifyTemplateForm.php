<?php

namespace Apps\Notify\Widget\NotifyTemplateForm;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;

class NotifyTemplateForm extends Form {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "") {
        $this->Core = $core;
        $this->Data = $data;

        $this->Init($data);
    }

    /**
     * Initialisation 
     */
    function Init($data) {
        $this->form = new Form("NotifyTemplateFormForm", $data);

        $this->form->SetView(__DIR__ . "/View/index.tpl", $this->Core);
        $this->form->SetAction("/View/index.tpl");

        $this->form->Add(array("Type" => "Hidden",
            "Id" => "Id",
        ));
        
        $Apps = \Apps\EeApp\Helper\AppHelper::GetAll($this->Core);
        $widgetsBlock = array();
        $descriptionWidget = "";
        $iWidget = 0;    
  
        foreach ($Apps as $app) {
            $appName = $app->Name->Value;
            $path = "\\Apps\\" . $appName . "\\" . $appName;
            $app = new $path($core);
            $listEmail = $app->GetListEmail();
            
            foreach($listEmail as $list ){
                $widgetsBlock[$appName . "-" .$list] = $appName . "-" . $list;
            }
        }

        $this->form->Add(array("Type" => "ListBox",
            "Id" => "Code",
            "Values" => $widgetsBlock, 
            "Validators" => ["Required"]
        ));


        $this->form->Add(array("Type" => "TextBox",
            "Id" => "Title",
            "Validators" => ["Required"]
        ));

        $this->form->Add(array("Type" => "TextBox",
            "Id" => "Description",
            "Validators" => ["Required"]
        ));
    
        $this->form->Add(array("Type" => "TextArea",
            "Id" => "Content",
            "Validators" => ["Required"]
        ));
        
        $this->form->Add(array("Type" => "Button",
            "Id" => "btnSaveTemplate",
            "Value" => $this->Core->GetCode("Notify.SaveTemplate"),
            "CssClass" => "btn btn-primary"
        ));
    }

    /*     * *
     * Render the html form
     */

    function Render() {
        return $this->form->Render();
    }

    /*     * *
     * Validate the data
     */

    function Validate($data = "") {
        $errors = $this->form->Validate($this->Data);

        return $errors;
    }

    /*     * *
     * Load control with the entity
     */

    function Load($entity) {
        $this->form->Load($entity);
    }

    /*     * *
     * Populate the entity with the form Data
     */

    function Populate($entity) {
        
    }

    /*     * *
     * Error message
     */

    function Error() {
        $errors[] = $this->form->errors;

        return json_encode(array("statut" => "Error", "message" => $errors));
    }

}

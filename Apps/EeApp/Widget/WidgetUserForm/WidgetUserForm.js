var WidgetUserForm = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
WidgetUserForm.Init = function(){
  Event.AddById("btnAdd", "click", WidgetUserForm.AddWidget);
};


WidgetUserForm.OnSuccess = function(callBack){

    WidgetUserForm.successHandler  = callBack;
};



/***
* Obtient l'id de l'itin�raire courant 
*/
WidgetUserForm.GetId = function(){
    return Form.GetId("WidgetUserFormForm");
};

/*
* Sauvegare l'itineraire
*/
WidgetUserForm.AddWidget = function(e){
    WidgetUserForm.successHandler("TOTI");

    if(Form.IsValid("WidgetUserFormForm"))
    {

    var data = "Class=Admin&Methode=AddWidgetUser&App=Admin";
        data +=  Form.Serialize("WidgetUserFormForm");

        Request.Post("Ajax.php", data).then(data => {

            Dialog.Close();
            Admin.LoadWidget();
        });
    }
};


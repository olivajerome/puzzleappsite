<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Downloader\Module\DialogAdminDownloader;

use Core\Control\Button\Button;
use Core\Controller\AdministratorController;
use Apps\Downloader\Widget\RessourceForm\RessourceForm;
use Apps\Downloader\Entity\DownloaderRessource;

class DialogAdminDownloaderController extends AdministratorController{

    /***
     * Add Ressource
     */
    function AddRessource($ressourceId){

         $ressourceForm = new RessourceForm($this->Core, $ressource);
    
        if($ressourceId != ""){
            $ressource = new DownloaderRessource($this->Core);
            $ressource->GetById($ressourceId);
           $ressourceForm->form->LoadImage($ressource);
        }
        
        $ressourceForm->Load($ressource);
      
        
        return $ressourceForm->Render();
    }
}
var Animation = function () {};

/**
 * Affiche un control
 * @param {*} controlId 
 */
Animation.Show = function (controlId)
{
    if (controlId.startsWith(".") || controlId.startsWith("#")) {

        var controls = document.querySelectorAll(controlId);

        for (var i = 0; i < controls.length; i++) {
            controls[i].style.display = "";
        }

    } else {
        var control = document.getElementById(controlId);
        control.style.display = "";
    }
};

/**
 * Hide à control
 * @param {*} controlId 
 * @param {*} speed 
 */
Animation.Hide = function (controlId, speed)
{
     if (controlId.startsWith(".") || controlId.startsWith("#")) {

        var controls = document.querySelectorAll(controlId);

        for (var i = 0; i < controls.length; i++) {
            controls[i].style.display = "none";
        }

    } else {
        var control = document.getElementById(controlId);
        control.style.display = "none";
    }
    
    /* var control = document.getElementById(controlId);
     
     var hide = new Hide();
     hide.control = control;
     hide.Run(speed); */
};

/**
 * Open à control
 * @param {*} controlId 
 * @param {*} speed 
 * @param {*} width 
 * @param {*} height 
 */
Animation.Open = function (controlId, speed, width, height)
{
    var control = document.getElementById(controlId);

    var open = new Open();
    open.control = control;
    open.Run(speed, width, height);
};

/**
 * FadeOut à control
 * @param {*} controlId 
 * @param {*} speed 
 */
Animation.FadeOut = function (controlId, speed, callBack)
{
    var control = document.getElementById(controlId);

    var fadeOut = new FadeOut();
    fadeOut.control = control;
    fadeOut.callBack = callBack;
    fadeOut.Run(speed);
};

/**
 * FadeIn à control
 * @param {*} controlId 
 * @param {*} speed 
 */
Animation.FadeIn = function (controlId, speed)
{
    var control = document.getElementById(controlId);

    var fadeIn = new FadeIn();
    fadeIn.control = control;
    fadeIn.Run(speed);
};

/**
 * Affiche une notification
 */
Animation.Notify = function (message, control, autoClose)
{
    var notification = new Notification();
    notification.AutoClose = autoClose;
    notification.Notify(message, control);
};

/**
 * SnavkBarr de confirmation
 * @param {*} message 
 * @param {*} callBack 
 */
Animation.Confirm = function (message, callBackYes, callBackNo) {

    var notification = new Notification();
    notification.Type = 'Confirm';
    notification.Notify(message, null, callBackYes, callBackNo);
};

/***
 * Notification de confirmation avec un champ text
 * @param {type} message
 * @param {type} callBackYes
 * @returns {undefined}
 */
Animation.Ask = function (message, callBackYes) {
    var notification = new Notification();
    notification.Type = 'Ask';
    notification.Notify(message, null, callBackYes);
};

/***
 * Ajout d'un viewer sur les images
 * @returns {undefined}
 */
Animation.AddViewer = function (control)
{
    var viewer = new Viewer();
    viewer.AddOnControl(control);
};

/**
 * Gestion des carroussel
 * @param {type} control
 * @returns {undefined}
 */
Animation.Caroussel = function (control)
{
    var caroussel = new Caroussel(control);

    if (typeof (caroussel.Start) == "undefined")
        return;

    caroussel.Start();

    return caroussel;
};

/**
 * Transforme des div en présenation vers des control
 * @param {*} contro 
 */
Animation.Tuto = function (control)
{
    var tuto = new Tuto(control);
    tuto.Start();
};

/***
 * Ajoute une class css
 * @param {type} control
 * @param {type} className
 * @returns {undefined}
 */
Animation.AddClass = function (controlId, className)
{
    var control = document.getElementById(controlId);
    control.className = control.className + " " + className;
};

/**
 * Supprimes une class Css
 * @param {*} controlId 
 * @param {*} className 
 */
Animation.RemoveClass = function (controlId, className)
{
    if (controlId.startsWith(".") || controlId.startsWith("#")) {

        var controls = document.querySelectorAll(controlId);

        for (var i = 0; i < controls.length; i++) {
            controls[i].className = controls[i].className.replace(className, "");
        }

    } else {
        var control = document.getElementById(controlId);
        control.className = control.className.replace(className, "");

        var control = document.getElementById(controlId);
        control.className = control.className.replace(className, "");
    }
};

/***
 * Ajoute/Supprime un class
 * @returns {undefined}
 */
Animation.ToogleClass = function (control, className) {

    if (control.className.indexOf(className) > -1) {
        control.className = control.className.replace(className, "");
    } else {
        control.className = control.className + " " + className;
    }
};

/**
 * Ajoute un loading sur un control
 * @param {*} controlId 
 */
Animation.AddLoading = function (controlId)
{
    var loading = new Loading();
    loading.ShowOn(controlId)
};

/**
 * Supprime un loading d'un control
 */
Animation.RemoveLoading = function (controlId)
{
    var loading = new Loading();
    loading.RemoveOn(controlId)
};

/***
 * Charge le html dans le control
 * @param {type} controlId
 * @param {type} html
 * @returns {undefined}
 */
Animation.Load = function (controlId, html) {
    var control = Dom.GetById(controlId);
    control.innerHTML = html;
};

/**
 * Ajoute un ToolTip sur un control
 * @param {type} controlId
 * @returns {undefined}
 */
Animation.ToolTip = function (controlId) {

    var toolTip = new ToolTip(controlId);

    Event.AddById(controlId, "mouseenter", function () {
        toolTip.Open();
    });

    Event.AddById(controlId, "mouseout", function () {
        toolTip.Close();
    });
};

/*
* Ajout le drag sur les elements selection par le selector
*/
Animation.AddDraggable = function(selector, dragStart, dragStop){
    
    var draggable = new Draggable();
        draggable.setDrag(selector, dragStart, dragStop);
};

/***
 * Set a element Editable
 * callBack the function wher blur the element
 */
Animation.SetEditable = function(control, callBack){

    let controls = document.querySelectorAll(control);

    for(let i = 0; i < controls.length; i++){
        var editable = new Editable(controls[i], callBack);
        editable.SetEditable();
    }
};
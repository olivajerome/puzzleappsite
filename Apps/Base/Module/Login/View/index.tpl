<section class='container'>
<h1>{{GetCode(Connexion)}}</h1>

<form method='post' action='Connect' class='block' >  
    {{error}} <br/>
    <label>{{GetCode(Login)}}</label>
    {{GetControl(EmailBox,login,{Required=true, PlaceHolder=VotreEmail})}} <br/>
    <label>{{GetCode(Password)}}</label>
    {{GetControl(Password,password,{Required=true, PlaceHolder=VotreEmail})}}<br/>

    <label></label>
    <div style='text-align:center'>
        {{GetControl(Submit, btnLogin,{LangValue=Base.Valid})}}<br/>
    </div>
</form>

<div style='text-align:right'>
    <a href='{{GetPath(/ForgetPassword)}}' >{{GetCode(Base.ForgetPassword)}}</a>
    <a href='{{GetPath(/Singup)}}' >{{GetCode(Base.Signup)}}</a>
</div>

</section>
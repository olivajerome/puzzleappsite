var Forum = function () {};

/*
 * Chargement de l'application
 */
Forum.Load = function (parameter)
{
    this.LoadEvent();
};


/***
 * Chargement de
 */
Forum.Load = function (parameter)
{
    this.LoadEvent();
};
/*
 * Chargement des �venements
 */
Forum.LoadEvent = function ()
{
    Dashboard.AddEventAppMenu(Forum.Execute, "", "Forum");
    Dashboard.AddEventWindowsTool("Forum");

    Forum.InitTabForum();
    Forum.InitTabCategory();
    Forum.InitTabMessage();
};

/***
 * Tab du forum
 */
Forum.InitTabForum = function () {
    ForumForm.Init();
};

/**
 * Tab des catgéogrie
 * @returns {undefined}
 */
Forum.InitTabCategory = function () {
    //Catégory
    Event.AddById("btnAddCategory", "click", () => {
        Forum.ShowAddCategory("")
    });
    EntityGrid.Initialise('gdCategory');
};


/**
 * Tab des catgéogrie
 * @returns {undefined}
 */
Forum.InitTabMessage = function () {
    EntityGrid.Initialise('gdMessage');
};


/*
 * Execute une fonction
 */
Forum.Execute = function (e)
{
    //Appel de la fonction
    Dashboard.Execute(this, e, "Forum");
    return false;
};

/*
 *	Affichage de commentaire
 */
Forum.Comment = function ()
{
    Dashboard.Comment("Forum", "1");
};

/*
 *	Affichage de a propos
 */
Forum.About = function ()
{
    Dashboard.About("Forum");
};

/*
 *	Affichage de l'aide
 */
Forum.Help = function ()
{
    Dashboard.OpenBrowser("Forum", "{$BaseUrl}/Help-App-Forum.html");
};

/*
 *	Affichage de report de bug
 */
Forum.ReportBug = function ()
{
    Dashboard.ReportBug("Forum");
};

/*
 * Fermeture
 */
Forum.Quit = function ()
{
    Dashboard.CloseApp("", "Forum");
};

/***
 * Refresh 
 * @returns {undefined}
 */
Forum.RefreshPlugin = function(){
    Dashboard.StartApp("", "Forum", "");
};

/**
 * Evenement
 */
ForumAction = function () {};

/**
 * Pop in d'ajout de forum
 * @returns {undefined}
 */
ForumAction.ShowAddForum = function (forumId)
{
    var param = Array();
    param['App'] = 'Forum';
    param['Title'] = 'Forum.ShowAddForum';

    if (forumId != undefined)
    {
        param['ForumId'] = forumId;
    }

    Dashboard.OpenPopUp('Forum', 'ShowAddForum', '', '', '', 'ForumAction.LoadAdmin()', serialization.Encode(param));
};

/**
 * Chage les forums de l'utilisateur
 * @returns {undefined}
 */
ForumAction.LoadMyForum = function ()
{
    var data = "Class=Forum&Methode=LoadMyForum&App=Forum";
    Dashboard.LoadControl("dvDesktop", data, "", "div", "Forum");
};

/**
 * Chage le forums de l'utilisateur
 * @returns {undefined}
 */
ForumAction.ShowDefaultForum = function (forum, categoryId, $messageId)
{
    var data = "Class=Forum&Methode=ShowDefaultForum&App=Forum";

    if (categoryId != undefined)
    {
        data += "&categoryId=" + categoryId;
    }

    if ($messageId != undefined)
    {
        data += "&messageId=" + $messageId;
    }

    Dashboard.LoadControl("dvDesktop", data, "", "div", "Forum");
};

/**
 * Charge un forum
 * @param {type} forumId
 * @returns {undefined}
 */
ForumAction.LoadForum = function (forumId)
{
    var data = "Class=Forum&Methode=LoadForum&App=Forum";
    data += "&forumId=" + forumId;
    Dashboard.LoadControl("dvDesktop", data, "", "div", "Forum");

    ForumAction.Tab = new Array();

    //Memorisation du forum
    ForumAction.ForumId = forumId;
};

/**
 * Dialogue d'ajout de catégorie
 */
Forum.ShowAddCategory = function (categoryId) {

    Dialog.open('', {"title": Dashboard.GetCode("Forum.AddCategorie"),
        "app": "Forum",
        "class": "DialogAdminForum",
        "method": "AddCategorie",
        "type": "left",
        "params": categoryId
    });
};

/****
 * Edite une catégorie
 * @param {type} categoryId
 * @returns {undefined}
 */
Forum.EditCategory = function (categoryId) {
    Forum.ShowAddCategory(categoryId);
};

/***
 * Supprime une catégorie
 * @param {type} categoryId
 * @returns {undefined}
 */
Forum.DeleteCategory = function (categoryId) {

    Animation.Confirm(Language.GetCode("Forum.ConfirmRemoveCategorie"), () => {

        let data = "Class=Forum&Methode=DeleteCategory&App=Forum";
        data += "&categoryId=" + categoryId;

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            Forum.ReloadCategory(data);
        });
    });
};

/***
 * Rafraichit la tab des catégorie
 * @returns {undefined}
 */
Forum.ReloadCategory = function (data) {
    let gdCategory = Dom.GetById("gdCategory");
    gdCategory.parentNode.innerHTML = data.data;
    Forum.InitTabCategory();
};

/**
 * Dialogue d'edition de message 
 */
Forum.EditMessage = function (messageId) {

    Dialog.open('', {"title": Dashboard.GetCode("Forum.EditMessage"),
        "app": "Forum",
        "class": "DialogAdminForum",
        "method": "AddMessage",
        "params": messageId,
        "type" : "left"
    });
};

/***
 * Rafraichit la tab des message
 * @returns {undefined}
 */
Forum.ReloadMessage = function (data) {
    
    let gdMessage = Dom.GetById("gdMessage");
  
    if(gdMessage != null){
        
        gdMessage.parentNode.innerHTML = data.data;
        Forum.InitTabMessage();
    }
};

/***
 * Supprime un message
 * @param {type} messageId
 * @returns {undefined}
 */
Forum.DeleteMessage = function (messageId) {

    Animation.Confirm(Language.GetCode("Forum.ConfirmRemoveMessage"), () => {

        let data = "Class=Forum&Methode=DeleteMessage&App=Forum";
        data += "&messageId=" + messageId;

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            Forum.ReloadMessage(data);
        });
    });
};

/**
 * Pop in de création de message
 * @param {type} categoryId
 * @returns {undefined}
 */
ForumAction.ShowAddSujet = function (categoryId)
{
    var param = Array();
    param['App'] = 'Forum';
    param['Title'] = 'Forum.ShowAddSujet';
    param['CategoryId'] = categoryId;

    Dashboard.OpenPopUp('Forum', 'ShowAddSujet', '', '', '', 'ForumAction.RefreshMessage(' + categoryId + ')', serialization.Encode(param));

    Dashboard.SetBasicAdvancedText("tbMessage");
};

/**
 * Rafrachit la liste des message de la categorie
 * @param {type} categoryId
 * @returns {undefined}
 */
ForumAction.RefreshMessage = function (categoryId)
{
    var dvMessage = document.getElementById("dvMessage");

    var JAjax = new ajax();
    JAjax.data = "Class=Forum&Methode=RefreshMessage&App=Forum";
    JAjax.data += "&categoryId=" + categoryId;

    dvMessage.innerHTML = JAjax.GetRequest("Ajax.php");
};

/**
 * Pop in d'ajout de reponse
 * @param {type} sujetId
 * @returns {undefined}ShowAddReponse
 */
ForumAction.ShowAddReponse = function (sujetId)
{
    var param = Array();
    param['App'] = 'Forum';
    param['Title'] = 'Forum.ShowAddReponse';
    param['SujetId'] = sujetId;

    Dashboard.OpenPopUp('Forum', 'ShowAddReponse', '', '', '', 'ForumAction.RefreshReponse(' + sujetId + ')', serialization.Encode(param));

    Dashboard.SetBasicAdvancedText("tbMessage");
};

/**
 * Rafrachit la liste des reponses d'un message
 * @param {type} categoryId
 * @returns {undefined}
 */
ForumAction.RefreshReponse = function (sujetId)
{
    var dvReponse = document.getElementById("dvReponse");

    var JAjax = new ajax();
    JAjax.data = "Class=Forum&Methode=RefreshReponse&App=Forum";
    JAjax.data += "&sujetId=" + sujetId;

    dvReponse.innerHTML = JAjax.GetRequest("Ajax.php");
};

/*
 * Charge la parti administration du forum
 */
ForumAction.LoadAdmin = function ()
{
    var data = "Class=Forum&Methode=LoadAdmin&App=Forum";
    Dashboard.LoadControl("dvDesktop", data, "", "div", "Forum");
};



/***
 * Start the Event for Member 
 */
Forum.LoadMember = function () {

    Event.AddByClass("editMessage", "click", Forum.EditMessageMember);
    Event.AddByClass("message", "blur", Forum.UpdateMessage);
    Event.AddByClass("deleteMessage", "click", Forum.DeleteMessageMember);

};

/***
 * Edit a message
 */
Forum.EditMessageMember = function(e){

    let container = e.srcElement.parentNode.parentNode;
    let message = container.getElementsByClassName("message");
    message[0].focus();
};

/**
* Update message
*/
Forum.UpdateMessage = function(e){
    let container = e.srcElement.parentNode;
    let message = e.srcElement;
    let title = container.getElementsByClassName("title");
 
    let data = "Class=Forum&Methode=UpdateMessage&App=Forum";
        data += "&Id=" + container.id;
        data += "&Title="+title[0].innerHTML;
        data += "&Message="+message.innerHTML;

        console.log(data);
        Request.Post("Ajax.php", data).then(data => {
            Animation.Notify(Language.GetCode("Forum.MessageUpdated"));
        });
};

/***
 * Remove message Member
 */
Forum.DeleteMessageMember = function(e){
    let container = e.srcElement.parentNode.parentNode;
   
    Animation.Confirm(Language.GetCode("Forum.ConfirmRemoveMessage"), () => {

        let data = "Class=Forum&Methode=DeleteMessage&App=Forum";
        data += "&messageId=" + container.id;

        Request.Post("Ajax.php", data).then(data => {
            container.parentNode.removeChild(container);
        });
    });
};

/***
 * Init the Forum App for current Page 
 */
Forum.Init = function(app, method){
    if(app == "Forum"){
        switch (method){
            case "Category":
                break; 
            case "NewDiscussion":
                Forum.InitNewDiscussionPage();
                break; 
            case "Sujet":
                Forum.InitSujetPage();
                break; 
            default :
            break;   
        }
    }
};

/*
 * Init  
 * @returns {undefined}
 */
Forum.InitNewDiscussionPage=function(){
    TextArea.ShowToolContentBlock(Dom.GetById("Message"));
};

/***
 * Init de la page d'un message
 */
Forum.InitSujetPage = function(){
 let txtEditor = new TextRichEditor(Dom.GetById("Message"));
    
    setTimeout(function(){
        if(Plugin.VotePlugin != undefined){
            Plugin.VotePlugin.Init();
        }
    },1000
  );
};

/***
 * Int the admin Widget
 * @returns {undefined}
 */
Forum.InitAdminWidget = function(){
    
    
    Event.AddByClass("editMessageForum", "click", (e)=>{
        Forum.EditMessage(e.srcElement.id);
        e.preventDefault();
        e.stopPropagation()
        
        
    });
};
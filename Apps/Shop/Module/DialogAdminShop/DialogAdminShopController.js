var DialogAdminShopController = function(){};

DialogAdminShopController.AddCategorie = function(){
    CategoryForm.Init(function(data){ Shop.ReloadCategory(data)});
};

DialogAdminShopController.AddCaracteristique = function(){
    CaracteristiqueForm.Init(function(data){ Shop.ReloadCaracteristique(data)});
};

DialogAdminShopController.AddProduct = function(){
    ProductForm.Init(Shop.ReloadProduct);
};

DialogAdminShopController.EditCommand = function(){
    DetailCommandForm.Init();
};

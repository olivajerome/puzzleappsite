<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

 namespace Apps\Downloader\Helper;

use Apps\Downloader\Entity\DownloaderRessource;
use Apps\Downloader\Entity\DownloaderRessourceContact;
use Core\Entity\Entity\Argument;
use Core\Utility\Format\Format;


class RessourceHelper
{

    /*
     * Return all ressource
     */
    public static function GetAll($core)
    {
        $ressources = new DownloaderRessource($core);
        return $ressources->GetAll();
    }
    
    /**
     * Obtient le nombre d'email
     * @param type $core
     * @param type $ressourceId
     */
    public static function GetNumberEmail($core, $ressourceId)
    {
        $contact = new DownloaderRessourceContact($core);
        $contact->AddArgument(new Argument("Apps\Downloader\Entity\DownloaderRessourceContact", "RessourceId", EQUAL, $ressourceId));

        return count($contact->GetByArg());
    }
    
    /*
     * Save the ressource
     */
    public static function SaveRessource($core, $ressourceId,  $name, $description, $url)
    {
        $ressource = new DownloaderRessource($core);
        
        if($ressourceId != "")
        {
            $ressource->GetById($ressourceId);
        }
        
        $ressource->UserId->Value = $core->User->IdEntite;
        $ressource->Name->Value = $name;
        $ressource->Description->Value = $description;
        $ressource->Url->Value = $url;
        $ressource->Code->Value = Format::ReplaceForUrl($name);
        
        $ressource->Save();
    }

    /***
     * Get Ressource By Category
     */
    public static function GetByCategory($core, $category){
        
        $ressource = new DownloaderRessource($core);
        $ressources = $ressource->Find("Category=".$category);

        $ressourceArray = $ressource->ToAllArray($ressources); 
        $ressourceArrayVersion = array();
        
        if($category == 1){
        
            foreach($ressourceArray as $ressource){

                $apName = $ressource["Name"];
                
                if($apName != "PuzzleApp"){
                 $path = "\\Apps\\".$apName ."\\".$apName;
                 $app = new $path($core->Core);
                 $ressource["Version"] = $app->Version;
                } 
                
                $ressourceArrayVersion[] =  $ressource ;  
            }
        
            return  $ressourceArrayVersion;
        }
        
        return $ressourceArray;
    }
}

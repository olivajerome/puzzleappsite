<?php 
/*
* PuzzleApp
* Webemyos
* Jérôme Oliva
* GNU Licence
*/

namespace Apps\Folders\Entity;
use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class FolderFolderShared extends Entity  
{
    protected $User;
    protected $Folder;
    
	//Constructeur
	function __construct($core)
	{
		//Version
		$this->Version ="2.0.0.0"; 

		//Nom de la table 
		$this->Core=$core; 
		$this->TableName="FolderFolderShared"; 
		$this->Alias = "FolderFolderShared"; 

		$this->FolderId = new Property("FolderId", "FolderId", NUMERICBOX,  false, $this->Alias); 
		$this->UserId = new Property("UserId", "UserId", NUMERICBOX,  false, $this->Alias); 
        $this->User = new EntityProperty("Core\Entity\User\User", "UserId"); 
        $this->Folder = new EntityProperty("Apps\Folders\Entity\FolderFolder", "FolderId"); 

		//Creation de l entité 
		$this->Create(); 
	}
}
?>
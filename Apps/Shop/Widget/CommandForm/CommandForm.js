var CommandForm = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
CommandForm.Init = function(){

    CommandForm.currentStep = 1;

    Event.AddById("btnNext", "click", CommandForm.NextStep);
    Event.AddById("btnPrevious", "click", CommandForm.PreviousStep);
};

/***
 * Render the next step form
 */
CommandForm.NextStep = function(){

    //Champ addresse non valide
    if(CommandForm.currentStep == 2 ){
        if(!Form.IsValid("AdressForm")){
            return;
        }
        
        if(Plugin.PaidPlugin != undefined){
            Plugin.PaidPlugin.Init();
        }
    } 
   
    if(CommandForm.currentStep == 3){
        
        if(Plugin.PaidPlugin != undefined){
            Plugin.PaidPlugin.Valid(function(){
                CommandForm.SaveCommand();
            });
        } else {
            CommandForm.SaveCommand();
        }
         
        return;   
    } 

    Animation.Hide("step" + CommandForm.currentStep);
    CommandForm.currentStep += 1;
    Animation.Show("step" + CommandForm.currentStep);

    CommandForm.UpdateBreadCrumb();    
};

/***
 * Set the Class Of Breab Crumb
 */
CommandForm.UpdateBreadCrumb = function(){
    let breadCrumb = document.querySelectorAll(".breadCrumb li");

    for(let i = 0; i < breadCrumb.length; i++){
        if(i + 1 == CommandForm.currentStep){
            breadCrumb[i].className = "active";
        } else{
            breadCrumb[i].className = "";
        }
    }
};

/***
 * Render the next step form
 */
CommandForm.PreviousStep = function(){

    if(CommandForm.currentStep > 1){
        Animation.Hide("step" + CommandForm.currentStep);
        CommandForm.currentStep -= 1;
        Animation.Show("step" + CommandForm.currentStep);
        
        CommandForm.UpdateBreadCrumb();    
    }
};


/***
* Obtient l'id de l'itin�raire courant 
*/
CommandForm.GetId = function(){
    return Form.GetId("CommandFormForm");
};

/***
 * Sauvegarde de la commande 
 */
CommandForm.SaveCommand = function(){

    Animation.Hide("buttonStep");
    Animation.Hide("step3");
    Animation.Show("step4");
    Animation.AddLoading("step4");

    //TODO VERIFIER QUE L4USER EST CONNECTE
    //OU PROPOSE LE DIALOGIUE DE CRATION DE COMPTE
    let data = "Class=Shop&Methode=CreateCommand&App=Shop";
    data +=  Form.Serialize("AdressForm");

    Request.Post("Ajax.php", data).then(data => {

        Animation.RemoveLoading("step4");

        CardWidget.UpdateCart(0);
        Animation.Load("step4", "data");
    });
};
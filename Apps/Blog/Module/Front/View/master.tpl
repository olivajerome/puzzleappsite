<section class='container'>
    <div class='row'> 
        <div class='col-md-9'>
            <div class='row'>
                {{content}}
            </div>
        </div>

        <div class='col-md-3 block-seco'>
            <div class=''>
                <h2>{{GetCode(Blog.Categorys)}}</h2>

                <ul>
                    {{foreach Category}}
                    <li>
                        <a href='{{GetPath(/Blog/Category/)}}{{element->GetUrlCode()}}'>
                            {{element->Name->Value}}
                        </a>
                    </li>
                    {{/foreach Category}}
                </ul>

                <h2>{{GetCode(Blog.LastArticles)}}</h2>
                <ul>
                    {{foreach Article}}
                    <li>
                        <a href='{{GetPath(/Blog/Article/)}}{{element->GetUrlCode()}}'>
                            {{element->Name->Value}}
                        </a>
                    </li>
                    {{/foreach Article}}
                </ul>

                <div id='dvNewsLetter' >
                    {{inscriptionNewletters}}
                </div>
            </div> 
        </div>
        <div class="clearfix"></div>
    </div>
</section>

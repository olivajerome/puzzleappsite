<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Mooc\Helper;

use Apps\Mooc\Entity\MoocCategory;
use Apps\Mooc\Entity\MoocMooc;
use Apps\Mooc\Entity\MoocLesson;

class SitemapHelper {
    /*     * *
     * Obtient le site map du Forum
     */

    public static function GetSiteMap($core, $returnUrl = false) {
        $urlBase = $core->GetPath("");
        $sitemap .= "<url><loc>$urlBase/Mooc</loc></url>";
        $urls = "";
       
        $category = new MoocCategory($core);
        $categorys = $category->GetAll();

        foreach ($categorys as $category) {
            $sitemap .= "<url><loc>$urlBase/Mooc/Category/" . $category->Code->Value . "</loc></url>";

            $url = $urlBase . "/Mooc/Category/" . $category->Code->Value;
            $urls .= '<br/><a target="__blank" href="' . $url . '">' . $url . '</a>';
        }

        $mooc = new MoocMooc($core);
        $moocs = $mooc->GetAll();

        foreach ($moocs as $mooc) {
            $sitemap .= "<url><loc>$urlBase/Mooc/Mooc/" . $mooc->Code->Value . "</loc></url>";

            $url = $urlBase . "/Mooc/Mooc/" . $mooc->Code->Value;
            $urls .= '<br/><a target="__blank" href="' . $url . '">' . $url . '</a>';
        }

        $moocLesson = new MoocLesson($core);
        $moocLessons = $moocLesson->GetAll();

        foreach ($moocLessons as $moocLesson) {
            $sitemap .= "<url><loc>$urlBase/Mooc/Lesson/" . $moocLesson->Code->Value . "</loc></url>";

            $url = $urlBase . "/Mooc/Lesson/" . $moocLesson->Code->Value;
            $urls .= '<br/><a target="__blank" href="' . $url . '">' . $url . '</a>';
        }

        if ($returnUrl == true) {
            return $urls;
        } else {
            return $sitemap;
        }
    }
}

<?php 
/*
* PuzzleApp
* Webemyos
* Jérôme Oliva
* GNU Licence
*/

namespace Apps\Folders\Entity;
use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class FolderFiles extends Entity  
{
	//Constructeur
	function __construct($core)
	{
		//Version
		$this->Version ="2.0.0.0"; 

		//Nom de la table 
		$this->Core=$core; 
		$this->TableName="FolderFiles"; 
		$this->Alias = "FolderFiles"; 

		$this->UserId = new Property("UserId", "UserId", NUMERICBOX,  false, $this->Alias); 
		$this->Name = new Property("Name", "Name", TEXTAREA,  false, $this->Alias); 
		$this->Code = new Property("Code", "Code", TEXTAREA,  false, $this->Alias); 
		$this->Url = new Property("Url", "Url", TEXTAREA,  false, $this->Alias); 
		$this->Description = new Property("Description", "Description", TEXTAREA,  false, $this->Alias); 
		$this->FolderId = new Property("FolderId", "FolderId", NUMERICBOX,  false, $this->Alias); 
		$this->DateCreated = new Property("DateCreated", "DateCreated", DATEBOX,  false, $this->Alias); 
		$this->DateUpdated = new Property("DateUpdated", "DateUpdated", DATEBOX,  false, $this->Alias); 
		$this->Type = new Property("Type", "Type", NUMERICBOX,  false, $this->Alias); 

		//Creation de l entité 
		$this->Create(); 
	}
}
?>
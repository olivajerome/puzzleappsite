<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Comunity\Module\DialogAdminComunity;

use Core\Control\Button\Button;
use Core\Controller\Controller;
use Apps\Comunity\Entity\ComunityComunity;
use Apps\Comunity\Widget\ComunityForm\ComunityForm;

class DialogAdminComunityController extends Controller{

    /***
     * Ajout d'une comunauté
     */
    function AddComunity($comunityId){
      
        $comunity = new ComunityComunity($this->Core);
        if($comunityId != ""){
            $comunity->GetById($comunityId);
        } 
        
        $ComunityForm = new ComunityForm($this->Core);
        $ComunityForm->Load($comunity);
        
        return $ComunityForm->Render();
    }
    
  
}

<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Market\Module\DialogMarket;

use Core\Control\Button\Button;
use Core\Controller\Controller;
use Core\View\View;

use Apps\Market\Widget\MarketProductForm\MarketProductForm;
use Apps\Market\Entity\MarketProduct;
use Apps\Market\Widget\MarketOfferForm\MarketOfferForm;
use Apps\Market\Entity\MarketProductOffer;
use Apps\Market\Widget\MarketProductShippingForm\MarketProductShippingForm;
use Apps\Market\Entity\MarketProductShipping;

/*
 * 
 */
 class DialogMarketController extends Controller
 {
    /**
     * Constructeur
     */
    function _construct($core="")
    {
          $this->Core = $core;
    }

    /**
     * Create
     */
    function Create()
    {
    }

    /**
     * Init
     */
    function Init()
    {
    }

    /**
     * Render
     */
    function Show($all=true)
    {
         return $this->Index();
    }
    
    /*
    * Get the home page
    */
    function Index()
    {
       $view = new View(__DIR__."/View/index.tpl", $this->Core);
       return $view->Render();
    }
   
   /***
    * Add Product By a user
    */
   function AddProduct($productId){
       
       $marketProductForm = new MarketProductForm($this->Core);
       
       return $marketProductForm->Render();
   }
   
   /***
    * Edite un produit
    */
   function EditProduct($productId){
       
       $product = new MarketProduct($this->Core);
       $product->GetById($productId);
      
       $marketProductForm = new MarketProductForm($this->Core, $product);
       $marketProductForm->Load($product);
       
       return $marketProductForm->Render();
   }
   
   /***
    * Make offer to a product
    */
   function MakeOffer($productId){
       
       if($this->Core->IsConnected()){
           
           $offerForm = new MarketOfferForm($this->Core);
     
           $offer = new MarketProductOffer($this->Core);
           $offer->ProductId->Value = $productId;
                  
           $offerForm->Load($offer);
           return $offerForm->Render();
       } 
       else {
           return $this->MustBeConnect();
       }
   }
   
   /***
    * Send the product 
    */
   function SendProduct($offerId){

        $offer = new MarketProductOffer($this->Core);
        $offer->GetById($offerId);

        $productShipping = new MarketProductShipping($this->Core);
        $productShipping->OfferId->Value = $offerId;
        $productShipping->ProductId->Value = $offer->ProductId->Value;

        $shippingForm = new MarketProductShippingForm($this->Core);
        $shippingForm->Load($productShipping);

        return $shippingForm->Render();
   }

   /***
    * Render Screen for connection
    */
   function MustBeConnect(){
       $view = new View(__DIR__."/View/notConnected.tpl", $this->Core);
       return $view->Render();
   }
          
          /*action*/
 }?>
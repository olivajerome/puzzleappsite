<?php

namespace Apps\Forum\Widget\MessageForm;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;
use Core\Control\EntityGrid\EntityGrid;
use Core\Control\EntityGrid\EntityColumn;
use Core\Control\EntityGrid\EntityIconColumn;
use Core\Control\EntityGrid\EntityFunctionColumn;
use Core\Entity\Entity\Argument;

class MessageForm extends Form {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "") {
        $this->Core = $core;
        $this->Data = $data;

        //On le fait ici pour avoir le formuaire � l'affichage mais aussi � la validation
        $this->Init($data);
    }

    /**
     * Initialisation 
     */
    function Init($data) {
        $this->form = new Form("MessageFormForm", $data);

        $this->form->SetView(__DIR__ . "/View/index.tpl", $this->Core);
        $this->form->SetAction("/View/index.tpl");

        $this->form->Add(array("Type" => "Hidden",
            "Id" => "Id",
        ));
        
        $this->form->Add(array("Type" => "TextBox",
            "Id" => "Title",
            "Validators" => ["Required"]
        ));
        
        
        $this->form->Add(array("Type" => "TextArea",
            "Id" => "Message",
            "Validators" => ["Required"]
        ));
        
        $this->form->Add(array("Type" => "Button",
            "Id" => "btnSaveMessage",
            "Value" => $this->Core->GetCode("Forum.UpdateMessage"),
            "CssClass" => "btn btn-primary"
        ));
        
        $this->form->AddElementView(new ElementView("gdReponse", $this->GetGridReponse($data["Id"])));
    }

     //Message of all Forum
     function GetGridReponse($messageId){
        $gdReponse = new EntityGrid("gdReponse", $this->Core);
        $gdReponse->Entity = "Apps\Forum\Entity\ForumReponse";
        $gdReponse->AddArgument(new Argument("Apps\Forum\Entity\ForumReponse", "MessageId", EQUAL, $messageId));   
        $gdReponse->App = "Forum";
        $gdReponse->Action = "GetTabMessage";
        $gdReponse->AddOrder("DateCreated desc");
  
        $gdReponse->AddColumn(new EntityColumn("DateCreated", "DateCreated"));
        $gdReponse->AddColumn(new EntityFunctionColumn("Message", "GetEditableMessage"));
       
        
        $gdReponse->AddColumn(new EntityIconColumn("Action", 
                                                  array(array("EditIcone", "Forum.EditReponse", "MessageForm.EditReponse"),
                                                        array("DeleteIcone", "Forum.DeleteReponse", "MessageForm.DeleteReponse"),
                                                  )    
                          ));
  
        return $gdReponse->Render();
     }
     
    
    
    /*     * *
     * G�n�rer les formulaire
     */

    function Render() {
        return $this->form->Render();
    }

    /*     * *
     * Valide les donn�es selon les diff�rents formulaires
     */

    function Validate($data = "") {
        $errors = $this->form->Validate($this->Data);

        return $errors;
    }

    /*     * *
     * Charge les controls avec les donn�es de l'entit�
     */

    function Load($entity) {
        $this->form->Load($entity);
    }

    /*     * *
     * Charge l'entit� avec les donn�es des diff�rents formulaire
     */

    function Populate($entity) {
        
    }

    /*     * *
     * Gestion des erreurs
     */

    function Error() {
        $errors[] = $this->form->errors;

        return json_encode(array("statut" => "Error", "message" => $errors));
    }

}

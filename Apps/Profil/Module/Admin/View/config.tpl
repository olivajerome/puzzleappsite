 <div class='marginTop borderTop'>
        <h2>{{GetCode(Profil.Plugin)}}</h2>
        <div class='right'>
            <button id='btnAddPlugin'  class='btn btn-primary' >{{GetCode(Profil.AddPlugin)}}</button>
        </div>

        <div id='lstPlugin' class='right'>
            {{foreach Plugins}}   
                <div class='chips'>

                    {{element->PluginName->Value}}

                    <i id='{{element->IdEntite}}' class='fa fa-trash removePlugin'></i>


                </div>
            {{/foreach Plugins}}   
        </div>    
    </div>
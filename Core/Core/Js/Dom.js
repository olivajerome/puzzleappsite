var Dom = function(){};

/***
 * Racourcit
 * @param {type} id
 * @returns {unresolved}
 */ 
Dom.GetById = function(id){
    var element = document.getElementById(id);
    return element;
};


Dom.GetParent = function(control, className){

   let parent = control.parentNode;
   let i = 0;

   while(parent.className.indexOf(className) < 0){
      parent = parent.parentNode;
      i++;

      if(i ==10){

         console.log("ON break");
         break;
      } 
   }

   return parent;
};

Dom.Remove = function(control){
   control.parentNode.removeChild(control);
};


/***
 * 
 * @param {type} className
 * @returns {undefined}
 */
Dom.GetByClassName = function(className){
    return document.getElementsByClassName(className);
};


/**
 * Vérify and execute Call Back when the document is Ready
 */
Dom.IsReady = function(callback) {
    if(callback && typeof callback === 'function'){
       if(document.attachEvent == undefined) {
          document.addEventListener("DOMContentLoaded", function() {
             if(Dom.IsLoaded == false){
               Dom.IsLoaded = true;
               return callback();
              }
          });
       } else {
          document.attachEvent("onreadystatechange", function() {
             if(document.readyState === "complete") {
                return callback();
             }
          });
       }
    } else {
       console.error('The callback is not a function!');
    }
 };

 /**
  * Appel les focntions lorsque le document est prêt
  */
 (function(document, window, domIsReady, undefined)
  {
      
    Dom.IsLoaded = false;
       
    Dom.IsReady(function() {

      //Script Nedeed Init
      Dialog.Init();

      Event.Init();
      
      //Init the App for All Page or The Selected Page
      App.Init();

    }
);
 })(document, window, Dom.IsReady);
 

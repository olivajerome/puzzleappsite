CREATE TABLE IF NOT EXISTS `MessageConversation` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`Type` INT  NULL ,
`UserId` INT  NULL ,
`DateCreated` DATE  NULL ,
`Title` TEXT  NULL ,
`SubTitle` TEXT  NULL ,
`Description` TEXT  NULL ,
`Status` INT  NULL ,
`DateClotured` DATE  NULL ,
`DateLastMessage` DATETIME  NULL ,


PRIMARY KEY (`Id`),
CONSTRAINT `ee_user_MessageConversation` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `MessageUser` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`ConversationId` INT  NULL ,
`UserId` INT  NULL ,
`Status` INT  NULL ,
`Readed` INT  NULL ,
PRIMARY KEY (`Id`),
CONSTRAINT `ee_user_MessageUser` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `MessageMessage` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`Type` INT  NULL ,
`UserId` INT  NULL ,
`Message` TEXT  NULL ,
`DateCreated` DATE  NULL ,
`ConversationId` INT  NULL ,
PRIMARY KEY (`Id`),
CONSTRAINT `ee_user_MessageMessage` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`),
CONSTRAINT `MessageMessage_MessageMessage` FOREIGN KEY (`ConversationId`) REFERENCES `MessageConversation`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `MessageComment` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`UserId` INT  NULL ,
`Message` TEXT  NULL ,
`DateCreated` DATE  NULL ,
`MessageId` INT  NULL ,
PRIMARY KEY (`Id`),
CONSTRAINT `MessageMessage_MessageComment` FOREIGN KEY (`MessageId`) REFERENCES `MessageMessage`(`Id`),
CONSTRAINT `ee_user_MessageComment` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `MessageLike` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`UserId` INT  NULL ,
`DateCreated` DATE  NULL ,
`AppName` VARCHAR(200)  NULL ,
`AppId` INT  NULL ,
`EntityName` VARCHAR(200)  NULL ,
`EntityId` INT  NULL ,
PRIMARY KEY (`Id`),
CONSTRAINT `ee_user_MessageLike` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `MessageFollow` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`MessageId` INT  NULL ,
`UserId` INT  NULL ,
PRIMARY KEY (`Id`),
CONSTRAINT `ee_user_MessageFollow` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`),
CONSTRAINT `MessageMessage_MessageFollow` FOREIGN KEY (`MessageId`) REFERENCES `MessageMessage`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\NewsLetter\Entity;

use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class NewsLetterEmailCampagne extends Entity {

    //Constructeur
    function __construct($core) {
        //Version
        $this->Version = "2.0.0.0";

        //Nom de la table 
        $this->Core = $core;
        $this->TableName = "NewsLetterEmailCampagne";
        $this->Alias = "NewsLetterEmailCampagne";

        $this->EmailId = new Property("EmailId", "EmailId", NUMERICBOX, false, $this->Alias);
        $this->Libelle = new Property("Libelle", "Libelle", TEXTBOX, false, $this->Alias);
        $this->Description = new Property("Description", "Description", TEXTBOX, false, $this->Alias);
        $this->DateCreated = new Property("DateCreated", "DateCreated", DATEBOX, false, $this->Alias);
        $this->DateSended = new Property("DateSended", "DateSended", DATEBOX, false, $this->Alias);
        $this->EmailSended = new Property("EmailSended", "EmailSended", NUMERICBOX, false, $this->Alias);

        //Creation de l entité 
        $this->Create();
    }
}

?>
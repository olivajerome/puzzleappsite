<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Carousel\Entity;

use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class CarouselItem extends Entity {

    //Constructeur
    function __construct($core) {
        //Version
        $this->Version = "2.0.0.0";

        //Nom de la table 
        $this->Core = $core;
        $this->TableName = "CarouselItem";
        $this->Alias = "CarouselItem";

        $this->CarouselId = new Property("CarouselId", "CarouselId", TEXTBOX, true, $this->Alias);
        $this->Name = new Property("Name", "Name", TEXTBOX, false, $this->Alias);
        $this->Content = new Property("Content", "Content", TEXTAREA, false, $this->Alias);

        //Creation de l entité 
        $this->Create();
    }
}

?>
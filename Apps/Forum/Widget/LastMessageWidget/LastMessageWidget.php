<?php

namespace Apps\Forum\Widget\LastMessageWidget;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;
use Apps\Forum\Entity\ForumMessage;
use Apps\Forum\Helper\MessageHelper;

class LastMessageWidget extends Form {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "") {
        $this->Core = $core;
        $this->Data = $data;
  }

    /*     * *
     * G�n�rer les formulaire
     */

    function Render() {
        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);
        
        $message = new ForumMessage($this->Core);
        $messages = MessageHelper::GetLastMessages($this->Core, 2);
        
        $view->AddElement(new ElementView("Messages", $messages));
        
        return $view->Render();
    }

}

var CommentPlugin = function () {};

/***
 * Init the plugin
 * @returns {undefined}
 */
CommentPlugin.Init = function () {
    
    //Front
    Event.AddById("btnSendComment", "click", CommentPlugin.SendComment);
    
    //Admin
    Event.AddBySelector(".publishComment", "click", CommentPlugin.PublishComment);
    Event.AddBySelector(".removeComment", "click", CommentPlugin.RemoveComment);
};

/***
 * Send a comment
 * @returns {undefined}
 */
CommentPlugin.SendComment = function () {

    if (Form.IsValid("CommentForm"))
    {
        Plugin.SendRequest({"plugin": "CommentPlugin",
            "method": "Comment",
            "EntityName": Form.GetValue("CommentForm", "EntityName"),
            "EntityId": Form.GetValue("CommentForm", "EntityId"),
            "Name": Form.GetValue("CommentForm", "Name"),
            "Message": Form.GetValue("CommentForm", "Message")
        }
        , (data) => {

            Form.Reset("CommentForm");

            let reponse = JSON.parse(data);
  
            Animation.Notify(reponse.data.Message);
        });
    }
};

//Publish a comment
CommentPlugin.PublishComment = function(e){
    
    let published = e.srcElement.classList.contains("published");
    
    Plugin.SendRequest({"plugin": "CommentPlugin",
            "method": "Publish",
            "EntityName": "BlogArticle",
            "EntityId": e.srcElement.id,
            "Status": published ? 1 : 2
        }
        , (data) => {
           e.srcElement.className =  "fa " + (published ? "fa-eye" : "fa-eye-slash published" )  ;
        });
};

/**
 * Delete a comment 
 * @param {type} e
 * @returns {undefined}
 */
CommentPlugin.RemoveComment = function (e) {

 let container = e.srcElement.parentNode.parentNode;
         
    Animation.Confirm(Language.GetCode("Base.ConfirmRemoveComment"), () => {

        Plugin.SendRequest({"plugin": "CommentPlugin",
            "method": "Remove",
            "EntityName": "BlogArticle",
            "EntityId": e.srcElement.id,
        }
        , (data) => {
           container.parentNode.removeChild(container);
        });
        }
    );
};

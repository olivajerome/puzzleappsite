<?php

namespace Apps\Comunity\Widget\LastMessage;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;
use Apps\Comunity\Helper\MessageHelper;

class LastMessage {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "") {
        $this->Core = $core;
    }

    /*     * *
     * render the last Mooc
     */

    function Render() {

        $messages = MessageHelper::GetLast($this->Core);

        if ($messages != null) {

            $view = new View(__DIR__ . "/View/index.tpl", $this->Core);
            $view->AddElement(new ElementView("Messages", $messages));
            return $view->Render();
        }

        return "";
    }
}

<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Cms\Helper;

use Apps\Cms\Entity\CmsPage;

class SitemapHelper {
    /*     * *
     * Obtient le site map du blog
     */

    public static function GetSiteMap($core, $returnUrl = false) {
       // $sitemap .= "<url><loc>$urlBase/Blog.html</loc></url>";
        $urls = "";
        $urlBase = $core->GetPath("");

        $page = new CmsPage($core);
        $pages = $page->GetAll();

        foreach ($pages as $page) {
            $sitemap .= "<url><loc>$urlBase/" . $page->Name->Value . "</loc></url>";

            $url = $urlBase . "/" . $page->Name->Value;
            $urls .= '<br/><a target="__blank" href="' . $url . '">' . $url . '</a>';
        }

        if ($returnUrl == true) {
            return $urls;
        } else {
            return $sitemap;
        }
    }
}

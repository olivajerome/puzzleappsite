<?php

namespace Plugins\CommentPlugin;

use Core\View\View;
use Core\View\ElementView;
use Core\Core\Response;
use Core\Control\Form\Form;
use Core\Utility\Date\Date;
use Plugins\CommentPlugin\Entity\CommentComment;

class CommentPlugin {

    public $Author = 'Webemyos';
    public $Version = '1.0.0.0';
    
    function __construct($core) {
        $this->Core = $core;
    }

    /*     * *
     * Comment Type
     */

    function GetType() {
        return "Comment";
    }

    /*     * *
     * Render the Js in the VotePlugin
     */

    function GetRegisterPlugin() {
        return "<script>document.addEventListener('DOMContentLoaded', function() { " . self::GetRegister() ."}) ;</script>";
    }
    
    /***
     * 
     */
    function GetRegister(){
       return "Plugin.Register('CommentPlugin', 'CommentPlugin', function(){ CommentPlugin.Init(); });" ;
    }
    
    /*     * *
     * Render the plugin
     */

    function Render($entityName, $entityId) {

        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);
        $view->AddElement(new ElementView("commentForm", $this->GetForm($entityName, $entityId)));

        $comment = new CommentComment($this->Core);
        $comments = $comment->Find("EntityName ='$entityName' AND EntityId ='$entityId' And Status = 2 ORDER By Id desc");
       
        $view->AddElement(new ElementView("Comments", $comments));
        
        return $view->Render();
    }

    /***
     * Render the Form
     */
    function GetForm($entityName, $entityId) {

        $this->form = new Form("CommentForm", $data);

        $this->form->SetView(__DIR__ . "/View/commentForm.tpl", $this->Core);
        $this->form->SetAction("/View/commentForm.tpl");

        $this->form->Add(array("Type" => "Hidden",
            "Id" => "Id",
        ));

        $this->form->Add(array("Type" => "Hidden",
            "Id" => "EntityName",
            "Value" => $entityName
        ));

        $this->form->Add(array("Type" => "Hidden",
            "Id" => "EntityName",
            "Value" => $entityName
        ));

        $this->form->Add(array("Type" => "Hidden",
            "Id" => "EntityId",
            "Value" => $entityId
        ));

        $this->form->Add(array("Type" => "TextBox",
            "Id" => "Name",
            "Validators" => ["Required"]
        ));

        $this->form->Add(array("Type" => "TextArea",
            "Id" => "Message",
            "Validators" => ["Required"]
        ));

        $this->form->Add(array("Type" => "Button",
            "Id" => "btnSendComment",
            "Value" => $this->Core->GetCode("Base.SendComment"),
            "CssClass" => "btn btn-primary"
        ));

        return $this->form->Render();
    }

    /*     * *
     * Add A comment
     */

    function Comment($params) {

        $comment = new CommentComment($this->Core);
        $comment->EntityName->Value = $params->EntityName;
        $comment->EntityId->Value = $params->EntityId;
        $comment->UserName->Value = $params->Name;
        $comment->Message->Value = $params->Message;
        $comment->DateCreated->Value = Date::Now();
        $comment->Status->Value = 1;

        if ($this->Core->IsConnected()) {
            $comment->UserId->Value = $this->Core->User->IdEntite;
        }

        $comment->Save();

        return Response::Success(array("Message" => $this->Core->GetCode("Base.CommentAdded")));
    }
    
    /***
     * Publish/UnPublish a comment
     */
    function Publish($params){
        $comment = new CommentComment($this->Core);
        $comment->GetById($params->EntityId);
        $comment->Status->Value = $params->Status;
        $comment->Save();
    }
    
    /***
     * Remove a comment
     */
    function Remove($params){
        $comment = new CommentComment($this->Core);
        $comment->GetById($params->EntityId);
        $comment->Delete();
    }
    
    /***
     * Get Comment of a entity
     */
    public function GetByEntity($core, $entityName, $entityId){
        $comment = new CommentComment($core);
        
        return $comment->Find("EntityName ='$entityName' AND EntityId ='$entityId' ORDER By Id desc");
    }
}

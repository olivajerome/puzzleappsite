var StyleEditor = function(){};

StyleEditor.Init = function(){
    Event.AddById("cssEditor", "blur", StyleEditor.SaveStyle);
};

/***
 * Sauvegarde le style
 */
StyleEditor.SaveStyle = function(){
    let css = Dom.GetById("cssEditor");

    var data = "Class=Cms&Methode=SaveStyle&App=Cms";
    data +=  "&css="+css.value;

    Request.Post("Ajax.php", data).then(data => {
    
    });
};
var LangSelector = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
LangSelector.Init = function(){
    Event.AddById("langs", "change", LangSelector.ChangeLang);
};

/***
 * Change the lang
 */
LangSelector.ChangeLang = function(e){

    let data = "Class=Lang&Methode=SetLanguage&App=Lang";
    data += "&langId=" + e.srcElement.value;

    Request.Post("Ajax.php", data).then(data => {
       document.location.reload();
    });

};
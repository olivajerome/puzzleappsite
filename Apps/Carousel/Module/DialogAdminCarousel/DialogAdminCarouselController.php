<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Carousel\Module\DialogAdminCarousel;

use Core\Control\Button\Button;
use Core\Controller\Controller;
use Core\View\View;
use Apps\Carousel\Entity\CarouselCarousel;
use Apps\Carousel\Widget\CarouselForm\CarouselForm;

/*
 * 
 */

class DialogAdminCarouselController extends Controller {

    /**
     * Constructeur
     */
    function _construct($core = "") {
        $this->Core = $core;
    }

    /**
     * Create
     */
    function Create() {
        
    }

    /**
     * Init
     */
    function Init() {
        
    }

    /**
     * Render
     */
    function Show($all = true) {
        return $this->Index();
    }

    /*
     * Get the home page
     */

    function Index() {
        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);
        return $view->Render();
    }

    /**
     * Add Edit a corousel
     */
    function AddCarousel($carouselId) {
        $carousel = new CarouselCarousel($this->Core);

        if ($carouselId != "") {
            $carousel->GetById($carouselId);
        }

        $carouselForm = new CarouselForm($this->Core, $carousel);
        $carouselForm->Load($carousel);

        return $carouselForm->Render();
    }

    /* action */
}

?>
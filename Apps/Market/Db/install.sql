CREATE TABLE IF NOT EXISTS `MarketMarket` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`Name` TEXT  NOT NULL,
`Description` TEXT  NOT NULL,
`AppName` VARCHAR(200)  NULL ,
`AppId` INT  NULL ,
`EntityName` VARCHAR(200)  NULL ,
`EntityId` INT  NULL ,
PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `MarketCategory` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`MarketId` INT  NOT NULL,
`Code` VARCHAR(200)  NOT NULL,
`Name` VARCHAR(200)  NOT NULL,
`Description` TEXT  NOT NULL,
PRIMARY KEY (`Id`),
CONSTRAINT `MarketMarket_MarketCategory` FOREIGN KEY (`MarketId`) REFERENCES `MarketMarket`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `MarketCaracteristique` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`MarketId` INT  NOT NULL,
`Name` VARCHAR(200)  NOT NULL,
`Description` TEXT  NOT NULL,
PRIMARY KEY (`Id`),
CONSTRAINT `MarketMarket_MarketCaracteristique` FOREIGN KEY (`MarketId`) REFERENCES `MarketMarket`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `MarketCaracteristiqueItem` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`CaracteristiqueId` INT  NOT NULL,
`Label` TEXT  NOT NULL,
PRIMARY KEY (`Id`),
CONSTRAINT `MarketCaracteristique_MarketCaracteristiqueItem` FOREIGN KEY (`CaracteristiqueId`) REFERENCES `MarketCaracteristique`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `MarketProduct` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`Title` VARCHAR(200)  NOT NULL,
`Code` VARCHAR(200)  NOT NULL,
`Description` TEXT  NOT NULL,
`Status` INT  NOT NULL,
`Price` TEXT  NOT NULL,
`MarketId` INT  NOT NULL,
`UserId` INT  NOT NULL,
PRIMARY KEY (`Id`),
CONSTRAINT `MarketMarket_MarketProduct` FOREIGN KEY (`MarketId`) REFERENCES `MarketMarket`(`Id`),
CONSTRAINT `ee_user_MarketProduct` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `MarketProductCategory` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`ProductId` INT  NOT NULL,
`CategoryId` INT  NOT NULL,
PRIMARY KEY (`Id`),
CONSTRAINT `MarketProduct_MarketProductCategory` FOREIGN KEY (`ProductId`) REFERENCES `MarketProduct`(`Id`),
CONSTRAINT `MarketCategory_MarketProductCategory` FOREIGN KEY (`CategoryId`) REFERENCES `MarketCategory`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `MarketProductCaracteristique` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`ProductId` INT  NOT NULL,
`CaracteristiqueId` INT  NOT NULL,
`ItemId` INT  NOT NULL,
PRIMARY KEY (`Id`),
CONSTRAINT `MarketProduct_MarketProductCaracteristique` FOREIGN KEY (`ProductId`) REFERENCES `MarketProduct`(`Id`),
CONSTRAINT `MarketCaracteristique_MarketProductCaracteristique` FOREIGN KEY (`CaracteristiqueId`) REFERENCES `MarketCaracteristique`(`Id`),
CONSTRAINT `MarketCaracteristiqueItem_MarketProductCaracteristique` FOREIGN KEY (`ItemId`) REFERENCES `MarketCaracteristiqueItem`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `MarketProductOffer` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`ProductId` INT  NOT NULL,
`UserId` INT  NOT NULL,
`Price` VARCHAR(200)  NOT NULL,
`DateCreated` DATE  NOT NULL,
`Status` INT  NOT NULL,
PRIMARY KEY (`Id`),
CONSTRAINT `MarketProduct_MarketProductOffer` FOREIGN KEY (`ProductId`) REFERENCES `MarketProduct`(`Id`),
CONSTRAINT `ee_user_MarketProductOffer` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `MarketProductShipping` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`OfferId` INT  NOT NULL,
`ProductId` INT  NOT NULL,
`TrackingNumber` TEXT  NOT NULL,
`TrackingUrl` TEXT  NOT NULL,
`DateSended` DATE  NOT NULL,
`Status` INT  NOT NULL,
PRIMARY KEY (`Id`),
CONSTRAINT `MarketProduct_MarketProductShipping` FOREIGN KEY (`ProductId`) REFERENCES `MarketProduct`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
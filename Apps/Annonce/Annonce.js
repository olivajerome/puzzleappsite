var Annonce = function () {};

/*
 * Add Event when the App loaded
 */
Annonce.Load = function (parameter)
{
    this.LoadEvent();
    
    Annonce.InitTabCategory();
    Annonce.InitTabAnnonce();
    Annonce.InitTabPlugin();
};

/*
 * Add event
 */
Annonce.LoadEvent = function ()
{
    Dashboard.AddEventAppMenu(Annonce.Execute, "", "Annonce");
    Dashboard.AddEventWindowsTool("Annonce");
};

/***
 * Start the Event for Member 
 */
Annonce.LoadMember = function () {

    Animation.SetEditable(".annonceMember .title", function(control){
        let annonceId = Dom.GetParent(control, "annonceMember").id;
        let value = control.innerHTML;

        Annonce.UpdateAnnonce(annonceId, value, "Title");
    });

    Animation.SetEditable(".annonceMember .description", function(control){
        let annonceId = Dom.GetParent(control, "annonceMember").id;
        let value = control.innerHTML;
   
        Annonce.UpdateAnnonce(annonceId, value, "Description");
    });

    Event.AddByClass("removeAnnonce", "click", Annonce.DeleteAnnonce);

    //If user install Message 
    if(ContactMember != undefined ){
        ContactMember.Init();
    }

};

/**
 * Update the title
 * @param {} annonceId 
 * @param {*} value 
 */
Annonce.UpdateAnnonce = function(annonceId, value, property){    
     let request = new Http("Annonce", "UpdateAnnonce");

        request.Add("AnnonceId", annonceId)
               .Add("Property", property)
               .Add("Value", value)
               .Post(function(data){
        });
};

/**
 * Remove a annonce
 * @param {*} e 
 */
Annonce.DeleteAnnonce = function(e){

    Animation.Confirm(Language.GetCode("Annonce.ConfirmRemoveAnnonce"), function(){
        let annonceId = e.srcElement.id;
        let container = Dom.GetParent(e.srcElement, "annonceMember");

        Annonce.UpdateAnnonce(annonceId, 99, "Status");
        Dom.Remove(container);
    });

 };

/*
 * Execute a function
 */
Annonce.Execute = function (e)
{
    //Call the function
    Dashboard.Execute(this, e, "Annonce");
    return false;
};

/***
 * Init the app front page
 */
Annonce.Init = function (app, method) {
    if (app == "Annonce") {
        switch (method) {
            case "Category" : 
               Annonce.InitCategoryPage();
                break;
            case "Annonce" :
                Annonce.InitAnnoncePage();
                break;
            default :
                break;
        }
    }
};

/***
 * Init the Admin Widget
 */
Annonce.InitAdminWidget = function () {

};

/*
 * Init the Event for tabCategory 
 */
Annonce.InitTabCategory = function(){
     //Catégory
    Event.AddById("btnAddCategory", "click", () => {
        Annonce.ShowAddCategory("");
    });
    EntityGrid.Initialise('gdCategory');
    
};

/**
 * Edit category
 * @param {type} categoryId
 * @returns {undefined}
 */
Annonce.EditCategory = function (categoryId) {
    Annonce.ShowAddCategory(categoryId);
};

/**
 * Dialogue add a categorie
 */
Annonce.ShowAddCategory = function (categoryId) {

    Dialog.open('', {"title": Dashboard.GetCode("Annonce.AddCategorie"),
        "app": "Annonce",
        "class": "DialogAdminAnnonce",
        "method": "AddCategory",
        "type": "left",
        "params": categoryId
    });
};

//Reload the category grid
Annonce.ReloadCategory = function(html){
    let categoryContainer = Dom.GetById("gdCategory").parentNode;
    categoryContainer.innerHTML = html;
    Annonce.InitTabCategory();
};

//Delete a category
Annonce.DeleteCategory = function(categoryId){
      Animation.Confirm(Language.GetCode("Annonce.ConfirmRemoveCategorie"), () => {

        let data = "Class=Annonce&Methode=DeleteCategory&App=Annonce";
        data += "&categoryId=" + categoryId;

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            Annonce.ReloadCategory(data.data);
        });
    });
};


Annonce.InitTabAnnonce = function(){
    EntityGrid.Initialise('gdAnnonce');
};

/***
 * Init the category page
 */
Annonce.InitCategoryPage = function(){
    Event.AddById("btnAddAnnonce", "click", function(){
        Annonce.ShowAnnonce("");
    });
   
}; 

/**
 * Dialogue add a categorie
 */
Annonce.ShowAnnonce = function (categoryId) {
     Dialog.open('', {"title": Dashboard.GetCode("Annonce.AddCategorie"),
        "app": "Annonce",
        "class": "DialogAnnonce",
        "method": "AddAnnonce",
        "params": ""
    });
};


/**
 * Edit category
 * @param {type} categoryId
 * @returns {undefined}
 */
Annonce.EditAnnonce = function (annonceId) {
  
    Dialog.open('', {"title": Dashboard.GetCode("Annonce.EditAnnonce"),
        "app": "Annonce",
        "class": "DialogAdminAnnonce",
        "method": "EditAnnonce",
        "type": "left",
        "params": annonceId
    });
};

/***
 * Init the Annonce page
 * @returns {undefined}
 */
Annonce.InitAnnoncePage = function(){
    
    Animation.AddViewer("imageContainer");
    AnnonceResponseForm.Init();

  setTimeout(function(){
        if(Plugin.VotePlugin != undefined){
            Plugin.VotePlugin.Init();
        }
    },1000);

};

/***
 * Init extend plugin
 * @returns {undefined}
 */
Annonce.InitTabPlugin = function(){
    Event.AddById("btnAddPlugin", "click", Annonce.AddPlugin);
    Event.AddByClass("removePlugin", "click" , Annonce.RemovePlugin);
};

/***
 * Ajout d'un plugin
 */
Annonce.AddPlugin = function(e){
  
    e.preventDefault();
    
     Dialog.open('', {"title": Dashboard.GetCode("Annonce.AddPlugin"),
     "app": "EeApp",
     "class": "DialogEeApp",
     "method": "AddPluginApp",
     "params": "Annonce",
     "type" : "right"
     });
 };

Annonce.RefreshPlugin = function(){
    let pluginAnnonceContainer = Dom.GetById("pluginAnnonceContainer").parentNode;
    let request = new Http("Annonce", "RefreshPlugin");
   
    request.Post(function(data){
        pluginAnnonceContainer.innerHTML = data;
        Annonce.InitTabPlugin();
    });
};

 /***
  * Remove plugin to the shop 
  * @returns {undefined}
  */
 Annonce.RemovePlugin = function(e){
     
     Animation.Confirm(Language.GetCode("Annonce.RemovePlugin"), function(){
         let container = e.srcElement.parentNode;
         
         let  data = "Class=EeApp&Methode=RemovePluginApp&App=EeApp";
              data += "&pluginId="+e.srcElement.id;
              
         Request.Post("Ajax.php", data).then(data => {
             container.parentNode.removeChild(container);
         });
     });
 };
 
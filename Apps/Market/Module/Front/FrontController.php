<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Market\Module\Front;

use Core\Control\Button\Button;
use Core\Controller\Controller;
use Core\View\View;
use Core\View\ElementView;
use Apps\Market\Helper\MarketHelper;
use Apps\Market\Entity\MarketCategory;
use Apps\Market\Helper\ProductHelper;

/*
 * 
 */

class FrontController extends Controller {

    /**
     * Constructeur
     */
    function _construct($core = "") {
        $this->Core = $core;
    }

    /**
     * Create
     */
    function Create() {
        
    }

    /**
     * Init
     */
    function Init() {
        
    }

    /**
     * Render
     */
    function Show($all = true) {
        return $this->Index();
    }

    /*
     * Get the home page
     */

    function Index() {
        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);

        $market = MarketHelper::GetMarket($this->Core);
        $view->AddElement(new ElementView("Market", $market[0]));

        //Category 
        $category = new MarketCategory($this->Core);
        $view->AddElement(new ElementView("Categories", $category->Find("MarketId=" . $market[0]->IdEntite)));

        return $view->Render();
    }

    /**
     * Category page
     * @param type $code
     * @return type
     */
    function Category($category) {

        $view = new View(__DIR__ . "/View/category.tpl", $this->Core);

        $view->AddElement(new ElementView("Category", $category));
        $view->AddElement(new ElementView("Products", ProductHelper::GetByCategorie($this->Core, $category->IdEntite)));

        return $view->Render();
    }

    /*     * **
     * Product Page 
     */

    public function Product($product) {

        $view = new View(__DIR__ . "/View/product.tpl", $this->Core);

        $view->AddElement(new ElementView("Product", $product));

        $caracteristiques = ProductHelper::GetCaracteristiqueByProduct($this->Core, $product->IdEntite, "entity");
        $view->AddElement(new ElementView("Caracteristiques", $caracteristiques));

        //Plugins 
        $socialPlugin = \Apps\EeApp\Helper\AppHelper::GetPluginType($this->Core, "Shop", "Social");
        $view->AddElement(new ElementView("socialPlugin",  $socialPlugin ? $socialPlugin->Render() : ""));
    
        $votePlugin = \Apps\EeApp\Helper\AppHelper::GetPluginType($this->Core, "Market", "Vote");
        $view->AddElement(new ElementView("votePluginScript",  $votePlugin ? $votePlugin->GetRegisterPlugin() : ""));
        $view->AddElement(new ElementView("votePlugin",  $votePlugin ? $votePlugin->Render("MarketProduct", $product->IdEntite) : ""));
    
        $ratingPlugin = \Apps\EeApp\Helper\AppHelper::GetPluginType($this->Core, "Market", "Rate");
        $view->AddElement(new ElementView("ratingPluginScript",  $ratingPlugin ? $ratingPlugin->GetRegisterPlugin() : ""));
        $view->AddElement(new ElementView("ratingPlugin",  $ratingPlugin ? $ratingPlugin->Render("MarketProduct", $product->IdEntite) : ""));
    
        $commentPlugin = \Apps\EeApp\Helper\AppHelper::GetPluginType($this->Core, "Market", "Comment");
        $view->AddElement(new ElementView("commentPluginScript",  $commentPlugin ? $commentPlugin->GetRegisterPlugin() : ""));
        $view->AddElement(new ElementView("commentPlugin",  $commentPlugin ? $commentPlugin->Render("MarketProduct", $product->IdEntite) : ""));
    


        return $view->Render();
    }

    /* action */
}

?>
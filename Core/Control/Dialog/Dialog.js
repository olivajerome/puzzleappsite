var Dialog = function () {};


Dialog.Type = {"Left" : "left", "Right" : "right" , "Full" : "full"};

/**
 * Add Event on The DialogButton
 */
Dialog.Init = function ()
{
    var dialogs = document.getElementsByClassName("dialogButton");

    for (var i = 0; i < dialogs.length; i++)
    {
        Dashboard.AddEvent(dialogs[i], "click", Dialog.open);
    }
};

/**
 * Open Dialog
 * And load Content From the App/Module
 * @param {*} e 
 */
Dialog.open = function (e, data)
{
    if (e != '')
    {
        e.preventDefault;
        Dialog.data = e.srcElement.parentNode.dataset;
    } else
    {
        Dialog.data = data;
    }

    if (data.height != undefined)
    {
        var height = data.height;
    } else
    {
        height = null;
    }
    if (Dialog.data.type != undefined)
    {
        var dialog = document.getElementById(Dialog.data.type + "Dialog");

        if (dialog == undefined)
        {
            var dialog = document.createElement('div');
            dialog.id = Dialog.data.type + "Dialog";
            var created = false;
        } else
        {
            var created = true;
        }

        dialog.className = Dialog.data.type;
        //dialog.style.width = "0px";
        dialog.style.padding = "10px";
    } else
    {
        var dialog = document.createElement('div');
        var created = false;

        dialog.zIndex = 9999;
        dialog.style.opacity = 0;

        dialog.id = 'dialog';

        if (data.height != undefined)
        {
            dialog.style.height = data.height;
        }
    }

    // Ajout d'un div Grise
    var backGround = document.createElement('div');

    backGround.style.height = document.body.parentNode.scrollHeight + "px";
    backGround.id = "back";
    backGround.zIndex = "9";

    document.body.appendChild(backGround);


    var html = "<div id='DialogTitle'>" + Dialog.data.title;
    html += "<span style='float:right' class='DialogClose'  id='DialogClose'><i class='fa fa-remove fa-times'>&nbsp</i></span> ";
    html += "</div>";

    if (Dialog.data.type != undefined)
    {
        html += "<div id='DialogRighContent' class='dialogContent' ><img src='images/loading/load.gif' alt='Loading'/></div>";
        dialog.innerHTML = html;
    } else {

        html += "<div id='DialogContent' class='dialogContent'><img src='images/loading/load.gif' alt='Loading'/></div>";
        dialog.innerHTML = html;
    }

    if (created == false) {
        document.body.appendChild(dialog);
    }

    //On determine si on est sur un réseau 
    var urls = document.location.href.split("/");

    if (Dialog.data.type != undefined)
    {
        var DialogContent = document.getElementById("DialogRighContent");
    } else
    {
        var DialogContent = document.getElementById("DialogContent");
        dragElement(dialog);
    }

    if (Dialog.data.html)
    {
        DialogContent.innerHTML = Dialog.data.html;

        Dashboard.AddEventByClass("DialogClose", "click", Dialog.close);
        setTimeout(() => {
            dialog.style.width = "40%";
            dialog.style.opacity = 100;
        }, 100);
    } else {

        //Pour atteindre Ajax.php de la racine
        var ajaxFile = urls[0] + "/" + urls[1] + "/" + urls[2] + "/Ajax.php";
        var currentContent = dialog.getElementsByClassName("dialogContent");
        Dialog.currentContent = currentContent[0];

        //Chargement
        Request.Post(ajaxFile, "App=" + Dialog.data.app + "&Class=" + Dialog.data.class + "&Methode=" + Dialog.data.method + "&Params=" + Dialog.data.params).then(data => {

            Dashboard.AddEventByClass("DialogClose", "click", Dialog.close);

            var DialogContent = Dialog.currentContent;// document.getElementById("DialogContent");
            DialogContent.innerHTML = data;

            var controller = Dialog.data.class + "Controller";
            var methode = controller + "." + Dialog.data.method + "('" + JSON.stringify(Dialog.data) + "')";
            dialog.style.opacity = 100;

            if (Dialog.data.type != "right" && Dialog.data.type != "left" && Dialog.data.type != "full") {
                var width = (document.body.clientWidth);
                var left = (width / 2) - (dialog.getBoundingClientRect().width / 2);

                dialog.style.left = left + 'px';

                var bodyHeight = (screen.height);
                var top = (bodyHeight / 2) - (dialog.getBoundingClientRect().height / 1.5);

                dialog.style.left = left + 'px';

                //On positionne en haut si négatif
                if (top < 0 || (bodyHeight, dialog.getBoundingClientRect().height, top) < 130) {
                    top = 0;
                }

                dialog.style.top = top + "px";
            } 
            //Appel de la méthode Js 
            // Ne pas oublier d'inclure le scrip Js du module
            eval(methode);

            Dialog.resize(dialog, height);
            
            Dialog.Reposition();
        });
    }
};

/**
 * Close the dialog
 */
Dialog.close = function ()
{

    var dialog = document.getElementById("rightDialog");

    if (dialog != undefined)
    {
        dialog.style.width = "0px";
        dialog.style.padding = "0px";

        dialog.parentNode.removeChild(dialog);

        var backGround = document.getElementById('back');
        document.body.removeChild(backGround);

        return;
    }

    var dialog = document.getElementById("leftDialog");

    if (dialog != undefined)
    {
        dialog.style.width = "0px";
        dialog.style.padding = "0px";

        dialog.parentNode.removeChild(dialog);

        var backGround = document.getElementById('back');
        document.body.removeChild(backGround);

        return;
    }

var dialog = document.getElementById("fullDialog");

    if (dialog != undefined)
    {
        dialog.style.width = "0px";
        dialog.style.padding = "0px";

        dialog.parentNode.removeChild(dialog);

        var backGround = document.getElementById('back');
        document.body.removeChild(backGround);

        return;
    }
    
    var dialog = document.getElementById("dialog");


    if (dialog != undefined)
    {
        Animation.FadeOut("dialog", 50, function () {
            var dialog = document.getElementById("dialog");

            document.body.removeChild(dialog);
        }
        );
    }

    var backGround = document.getElementById('back');
    document.body.removeChild(backGround);

};

Dialog.Close = function () {
    Dialog.close();
};

/**
 *  Positionne correctemnet le dialog
 **/
Dialog.resize = function (dialog, height)
{
    var dialogContent = document.getElementById("DialogContent");
    var dialog = document.getElementById("dialog");

    if (height != null)
    {
        //dialog.style.height = height + "px";
        //dialog.style.top = '100px';
    } else
    {
        /* var width = (document.body.clientWidth);
         
         var  left = (width / 2)  - (dialog.offsetWidth / 2 ) ;
         dialog.style.left = left;
         
         var height = (document.body.clientHeight);
         
         var dialogContainer = document.getElementById("dialogContainer");
         
         
         if(dialogContainer.offsetHeight < ( height - 100))
         {                
         dialogContent.style.height = dialogContainer.offsetHeight + 250;
         var top = (height / 2)  - (dialogContent.style.height ) ;
         dialog.style.top = top;
         } 
         else
         {        
         dialogContent.style.height =   height - 100;
         dialog.style.top = 50;
         
         } */
    }
};

/***
 * Repositionne le dialog 
 * @returns {undefined}
 */
Dialog.Reposition = function () {

    let dialog = document.getElementById("dialog");
    
    if(dialog == undefined){
        return;
    }
    
    let dialogContent = document.getElementById("DialogContent");
    let dialogRect = dialogContent.getClientRects()[0];
    let width = (window.innerWidth - dialogRect.width);
    let left = width / 2;
    let height = (window.innerHeight - dialogRect.height);
    let top = height / 2;

    dialog.style.left = left + "px";
    dialog.style.top = ( top - 50 )+ "px";

};

function dragElement(elmnt) {
    var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    //console.log(elmnt.id);

    if (document.getElementById("DialogTitle" /*elmnt.id + "header"*/)) {
        // if present, the header is where you move the DIV from:
        document.getElementById("DialogTitle" /*elmnt.id + "header" */).onmousedown = dragMouseDown;
    } else {
        // otherwise, move the DIV from anywhere inside the DIV:
        elmnt.onmousedown = dragMouseDown;
    }

    function dragMouseDown(e) {
        e = e || window.event;
        e.preventDefault();
        // get the mouse cursor position at startup:
        pos3 = e.clientX;
        pos4 = e.clientY;
        document.onmouseup = closeDragElement;
        // call a function whenever the cursor moves:
        document.onmousemove = elementDrag;
    }

    function elementDrag(e) {
        e = e || window.event;
        e.preventDefault();
        // calculate the new cursor position:
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        // set the element's new position:
        elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
        elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
    }

    function closeDragElement() {
        // stop moving when mouse button is released:
        document.onmouseup = null;
        document.onmousemove = null;
    }
}



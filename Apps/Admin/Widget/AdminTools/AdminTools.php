<?php

namespace Apps\Admin\Widget\AdminTools; 

use Core\View\View;
use Core\View\ElementView;

use Core\Dashboard\DashBoardManager;

class AdminTools {

    /**
     * Constructeur
     */
    function __construct($core = "") {
        $this->Core = $core;
    }
    
    /***
     * Icone a mettre dans  un menu  
     */
    function Index($params){
        
        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);
     
                
        return $view->Render();
    }
}
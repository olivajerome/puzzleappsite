var CategoryForm = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
CategoryForm.Init = function(callBack){

    Event.AddById("btnSaveCategory", "click", CategoryForm.SaveCategory);
    CategoryForm.callBack = callBack;
};

/***
* Obtient l'id de l'itin�raire courant 
*/
CategoryForm.GetId = function(){
    return Form.GetId("categoryForm");
};

/*
* Sauvegare l'itineraire
*/
CategoryForm.SaveCategory = function(e){
   
    if(Form.IsValid("categoryForm"))
    {

    var data = "Class=Forum&Methode=SaveCategory&App=Forum";
        data +=  Form.Serialize("categoryForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);
         
            if(data.statut == "Error"){
                Form.RenderError("categoryForm", data.message);
            } else {

                Dialog.Close();
                
                if(CategoryForm.callBack){
                    CategoryForm.callBack(data);
                }
            }
        });
    }
};


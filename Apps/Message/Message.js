var Message = function () {};

Message.Init = function () {

};

Message.LoadMember = function (parameter)
{
    this.LoadEvent();
};

/*
 * Chargement de l'application
 */
Message.Load = function (parameter)
{
    this.LoadEvent();
};

/*
 * Chargement des �venements
 */
Message.LoadEvent = function ()
{
    Dashboard.AddEventAppMenu(Message.Execute, "", "Message");
    Dashboard.AddEventWindowsTool("Message");
    Dashboard.AddEventByClass('discussPreview', "click", MessageAction.LoadMessage);
    Dashboard.AddEventByClass('fa-archive', "click", MessageAction.RemoveMessage);
    Dashboard.AddEventById("btnSendMessage", "click", MessageAction.AddMessage);
    Dashboard.AddEventById("tbSearchConversation", "keyup", MessageAction.SerchConversation);
    Dashboard.AddEventById("resetFilter", "click", MessageAction.ResetFilter);
    Dashboard.AddEventById("btnHomeMessage", "click", function () {
        MessageAction.ShowHideLeftColumn(true);
    });

    Dashboard.AddEventById("btnNewConversation", "click", MessageAction.OpenNewConversation);

    MessageAction.LoadFirst();

    //Touche entrée
    window.addEventListener("keypress", function (event) {
        if (event.keyCode == 13) {
            MessageAction.AddMessage();
        }
    });
};

/*
 * Execute une fonction
 */
Message.Execute = function (e)
{
    //Appel de la fonction
    Dashboard.Execute(this, e, "Message");
    return false;
};

/*
 *	Affichage de commentaire
 */
Message.Comment = function ()
{
    Dashboard.Comment("Message", "1");
};

/*
 *	Affichage de a propos
 */
Message.About = function ()
{
    Dashboard.About("Message");
};

/*
 *	Affichage de l'aide
 */
Message.Help = function ()
{
    Dashboard.OpenBrowser("Message", "{$BaseUrl}/Help-App-Message.html");
};

/*
 *	Affichage de report de bug
 */
Message.ReportBug = function ()
{
    Dashboard.ReportBug("Message");
};

/*
 * Fermeture
 */
Message.Quit = function ()
{
    Dashboard.CloseApp("", "Message");
};

MessageAction = function () {};


/**
 * Envoi le message
 * @returns {undefined}
 */
MessageAction.AddMessage = function (refresh) {

    let dashboardDiscus = document.getElementById("dashboardDiscus");
    let tbMessage = document.getElementById("tbMessage");
    let hdUserId = document.getElementById("hdUserId");
    let hdMessageId = document.getElementById("hdMessageId");
    let uploadImages = document.getElementById("uploadImages-fileToUpload");
    let images = new Array();
    let documents = new Array();

    let data = "App=Message&Methode=StartMessage";

    if (hdUserId != undefined)
    {
        data += "&users=" + hdUserId.value;
    }

    data += "&message=" + tbMessage.value;
    data += "&discussId=" + hdMessageId.value;

    let imgs = uploadImages.getElementsByTagName("img");

    for (let i = 0; i < imgs.length; i++)
    {
        images.push(imgs[i].src);
    }
    data += "&images=" + images.join(";");

    //Document
    let span = uploadImages.getElementsByTagName("span");
    for (let i = 0; i < span.length; i++)
    {
        documents.push(span[i].innerHTML);
    }
    data += "&documents=" + documents.join(";");

    if (tbMessage.value.trim() == "" && images.length == 0 && documents.length == 0) {

        Animation.Notify("Veuillez saisir un texte, sélectionner une image ou un document !");
        return;
    }

    MessageAction.ShowNotify("Envoi..");

    let url = window.location.origin + "/Ajax.php";

    Request.Post(url, data).then(data => {
        data = JSON.parse(data);

        let hdMessageId = document.getElementById('hdMessageId');
        hdMessageId.value = data.discuId;

        tbMessage.value = "";
        dashboardDiscus.innerHTML += data.message;
        MessageAction.HideNotify();

        MessageAction.AddEvent();

        let fileToUpload = document.getElementById("uploadImages-fileToUpload");
        fileToUpload.innerHTML = "";

        let messageContainer = document.getElementById("messageContainer");
        messageContainer.style.display = "";

        let imageButton = document.getElementsByClassName("fa-image");
        imageButton[0].style.display = "";

        //Nouvelle conversation
        if (MessageAction.NewConversation == true) {
            //On met à jour lma colonne de gauche 
            MessageAction.RefreshMessage();
        }
        MessageAction.ScrollBottom();
    });
};

/**
 * Scroll En base
 * @returns {undefined}
 */
MessageAction.ScrollBottom = function ()
{
    let dashboardDiscus = document.getElementById("dashboardDiscus");
    dashboardDiscus.scrollTo(0, dashboardDiscus.scrollHeight);
};

/**
 * Affiche la barre de notification
 * @param {type} message
 * @returns {undefined}
 */
MessageAction.ShowNotify = function (message)
{
    let notify = document.getElementById("notify");
    notify.innerHTML = message + "...";
    notify.style.display = "block";
};

/***
 * Reinitialise la tchatbox
 * @returns {undefined}
 */
MessageAction.ReinitTchatBox = function ()
{
    let tbMessage = document.getElementById("tbMessage");
    tbMessage.value = "";
};

/**
 * Ferme la notification
 * @returns {undefined}
 */
MessageAction.HideNotify = function ()
{
    let notify = document.getElementById("notify");
    notify.style.display = "none";
};

/***
 * Affiche ou cache la colonne de gauche
 * @param {type} state
 * @returns {undefined}
 */
MessageAction.ShowHideLeftColumn = function (state)
{
    let userSearch = document.getElementsByClassName("userSearch");
    let actualMessageUser = document.getElementById("actualMessageUser");
    let btnNewConversation = document.getElementById("btnNewConversation");
    let btnHomeMessage = document.getElementById("btnHomeMessage");

    if (state == false) {

        btnHomeMessage.style.display = "block";
        userSearch[0].style.display = 'none';
        actualMessageUser.style.display = 'none';
        btnNewConversation.style.display = "none";
    } else {
        btnHomeMessage.style.display = "none";
        userSearch[0].style.display = 'block';
        actualMessageUser.style.display = 'block';
        btnNewConversation.style.display = "block";
    }
};

/***
 * Charge les messages d'une conversation
 * @param {type} e
 * @returns {undefined}
 */
MessageAction.LoadMessage = function (e, discussId)
{
    //Mode téléphone
    if (document.body.clientWidth < 1000) {

        MessageAction.ShowHideLeftColumn(false);
    }

    //Si on a cliquer sur le bouton Archive 
    if (e.srcElement.className == "fa fa-archive") {
        return;
    }

    if (e != null)
    {
        MessageAction.NewConversation = false;
        container = e.srcElement;

        MessageAction.ReinitTchatBox();

        while (!(container.className.indexOf("discussPreview") > -1)) {
            container = container.parentNode;
        }

        var discussId = container.id;

        //On supprime le I
        let info = container.getElementsByClassName("fa-info");

        if (info.length > 0) {
            container.removeChild(info[0]);
        }

        let disccus = document.getElementsByClassName("discussPreview");

        for (let i = 0; i < disccus.length; i++)
        {
            disccus[i].className = "discussPreview";
        }
        container.className = "discussPreview active";
    }

    //Sauvegarde de la discussion courante
    MessageAction.discussId = discussId;

    let tchat = document.getElementById("messageBlock");
    let tbObjet = document.getElementById("tbObjet");

    if (discussId != "")
    {
        let dashboardDiscus = document.getElementById("dashboardDiscus");
        dashboardDiscus.innerHTML = "<img src='" + document.location.origin + "/images/loading/load.gif' alt='Loading'/>";

        let url = window.location.origin + "/Ajax.php";
        let data = "App=Message&Methode=LoadMessage";
        data += "&discussId=" + discussId;

        Request.Post(url, data).then(data => {

            dashboardDiscus.innerHTML = data;

            messageBlock.style.display = "block";
            tbObjet.style.display = "none";

            MessageAction.AddEvent();
            MessageAction.UpdateNumberMessageRead();
            MessageAction.ScrollBottom();
        });
    }
};

/**
 * Ajoute les évenements sur les Icones
 * @returns {undefined}
 */
MessageAction.AddEvent = function () {

    Dashboard.AddEventById("btnSend", "click", MessageAction.AddMessage);
    Dashboard.AddEventByClass("removeMessage", "click", MessageAction.RemoveMessage);
    Dashboard.AddEventByClass("fa-edit", "click", MessageAction.EditMessage);
    Dashboard.AddEventByClass("moreActionMessage", "click", MessageAction.OpenMenuMessage);
};

/**
 * Supprime un message
 * @param {type} e
 * @returns {undefined}
 */
MessageAction.RemoveMessage = function (e) {

    let container = e.srcElement.parentNode;

    Animation.Confirm(Language.GetCode("Message.ConfirmRemoveMessage"), function ()
    {
        let url = window.location.origin + "/Ajax.php";
        let data = "App=Message&Methode=RemoveMessage";
        data += "&messageId=" + container.id;

        Request.Post(url, data).then(data => {
            container.parentNode.parentNode.removeChild(container.parentNode);
        });
    })
            ;
};

/**
 * Edit un message 
 * @param {type} e
 * @returns {undefined}
 */
MessageAction.EditMessage = function (e) {

    let container = e.srcElement.parentNode.parentNode;
    let message = container.getElementsByTagName("span");
    let messageId = e.srcElement.id;

    let img = message[0].getElementsByTagName('img');
    let text = message[0].getElementsByTagName('b');
    let imgs = "";

    if (img.length > 0) {
        let messageOrigin = text[0].innerHTML;

        for (let i = 0; i < img.length; i++) {
            imgs += "<img src='" + img[i].src + "' /> ";
        }
    } else {
        let messageOrigin = message[0].innerHTML;
    }

    message[0].style.display = "none";

    container.innerHTML += "<textarea class='form-control'>" + messageOrigin + "</textarea><i id = 'btnSendModif' class='fa fa-plane' title='envoyer'></i>";

    Dashboard.AddEventById("btnSendModif", "click", function (e) {

        let newMessage = container.getElementsByTagName("textarea");

        let url = window.location.origin + "/Ajax.php";
        let data = "App=Message&Methode=UpdateMessage";
        data += "&messageId=" + messageId;
        data += "&newMessage=" + imgs + newMessage[0].value;

        Request.Post(url, data).then(data => {

            message[0].innerHTML = imgs + newMessage[0].value;
            message[0].style.display = "block";
            container.removeChild(newMessage[0]);
            container.removeChild(e.srcElement);

            MessageAction.AddEvent();
        });
    });
};

/**
 * Cloture une conversation
 * @returns {undefined}
 */
MessageAction.RemoveConversation = function (e) {

    MessageAction.CloseActionMenu();

    e.preventDefault();
    e.stopPropagation();

    let discussId = document.getElementById('hdMessageId');

    Animation.Confirm(Language.GetCode("Message.ConfirmRemoveConversation"), function () {
        let url = window.location.origin + "/Ajax.php";
        let data = "App=Message&Methode=RemoveConversation";
        data += "&discussId=" + discussId.value;

        Request.Post(url, data).then(data => {
            MessageAction.RefreshMessage();

            let dashboardDiscus = document.getElementById("dashboardDiscus");
            dashboardDiscus.innerHTML = "";
        });
    });
};

/**
 * Show Overlay
 * @param {type} e
 * @returns {undefined}
 */
MessageAction.ShowOverlay = function ()
{
    let dialog = document.createElement('div');
    dialog.id = "overlay",
            dialog.className = "overlay";

    document.body.appendChild(dialog);
};

/***
 * cache l'overlay
 * @returns {undefined}
 */
MessageAction.HideOverlay = function ()
{
    let overlay = document.getElementById("overlay");
    document.body.removeChild(overlay);
};

/**
 * Pop in d'ajout de conversation
 * @returns {undefined}
 */
MessageAction.OpenNewConversation = function (e)
{
    MessageAction.ShowOverlay();

    MessageAction.discussId = null;

    let dialog = document.createElement('div');
    dialog.className = "dialogNew";
    dialog.id = "dialogNew";
    dialog.style.position = 'absolute';
    dialog.style.width = "300px";
    dialog.style.height = "450px";
    dialog.style.overflow = "auto";
    dialog.style.border = '1px solid grey';
    dialog.style.padding = "5px";
    dialog.style.zIndex = 10000;
    dialog.style.left = (e.clientX - 300) + "px";
    dialog.style.top = e.clientY + "px";
    dialog.style.backgroundColor = "white";

    let title = "";
    title = "<div style='text-align:left;width:100%' class='title'><i class='fa fa-times' alt='' title='" + Language.GetCode("Message.Close") + "' onclick='MessageAction.CloseNewConversation();MessageAction.HideOverlay();'></i>";
    title += "<div>" + Language.GetCode("Message.NewMessage") + "<span  title='" + Language.GetCode("Message.StartConversation") + "' style='float:right' >";
    title += "<i id='btnStartDisscus'style='display:none' class='fa fa-arrow-right' >&nbsp;</i></span></div></div>";
    title += "</div>";
    title += "<div class='search'>&Agrave; : <input type='text' class='form-control' id='tbSearchUser' placeholder='" + Language.GetCode("Message.Search") + "' onkeyup='MessageAction.SearchUser(this);' ></div>";
    title += "<div id='resultSeachUser'  ></div>";

    dialog.innerHTML = title;

    document.body.appendChild(dialog);

    let tbSearchUser = document.getElementById("tbSearchUser");
    tbSearchUser.focus();

    Dashboard.AddEventById("btnStartDisscus", "click", MessageAction.StartMessage);
};

/**
 * Rechercher les utilisateurs correspondant à la recheche
 * @returns {undefined}
 */
MessageAction.SearchUser = function (e)
{
    let searchValue = e.value;
    let resultSeachUser = document.getElementById('resultSeachUser');

    resultSeachUser.innerHTML = Language.GetCode("Message.Loading");

    let url = window.location.origin + "/Ajax.php";
    let data = "App=Message&Methode=SearchUser";
    data += "&search=" + searchValue;

    Request.Post(url, data).then(data => {

        resultSeachUser.innerHTML = data;

        Dashboard.AddEventByClass("cbUser", "click", MessageAction.ShowStarConversation);
    });
};

/***
 * Affiche ou masque la flèche pour démarrer une conversation
 * @returns {undefined}
 */
MessageAction.ShowStarConversation = function ()
{
    let resultSeachUser = document.getElementById('resultSeachUser');
    let checkbox = resultSeachUser.getElementsByClassName("cbUser");
    let btnStartDisscus = document.getElementById('btnStartDisscus');
    let OneSelect = false;

    for (let i = 0; i < checkbox.length; i++)
    {
        if (checkbox[i].checked) {
            OneSelect = true;
        }
    }

    if (OneSelect == true)
    {
        btnStartDisscus.style.display = "block";
    } else {
        btnStartDisscus.style.display = "none";
    }
};

/**
 * Démarre une conversation Multiple
 * @returns {undefined}
 */
MessageAction.StartMessage = function ()
{
    MessageAction.HideOverlay();

    let resultSeachUser = document.getElementById('resultSeachUser');
    let cbUser = resultSeachUser.getElementsByClassName("cbUser");
    let users = Array();

    for (let i = 0; i < cbUser.length; i++)
    {
        if (cbUser[i].type == "checkbox" && cbUser[i].checked)
        {
            users.push(cbUser[i].id);
        }
    }

    MessageAction.CloseNewConversation();

    let data = "App=Message&Methode=CreateMessage";
    data += "&user=" + users.join();

    let dashboardDiscus = document.getElementById("dashboardDiscus");
    dashboardDiscus.innerHTML = "<img src='" + document.location.origin + "/images/loading/load.gif' alt='Loading'/>";

    let url = window.location.origin + "/Ajax.php";

    Request.Post(url, data).then(data => {

        dashboardDiscus.innerHTML = data;

        let messageBlock = document.getElementById("messageBlock");
        messageBlock.style.display = "block";
        tbObjet.style.display = "none";
    });

    MessageAction.NewConversation = true;
};

/**
 * 
 * @returns {undefined}
 */
MessageAction.CloseNewConversation = function ()
{
    let dialogNew = document.getElementById("dialogNew");
    dialogNew.parentNode.removeChild(dialogNew);
};

/***
 * Rafrachit les conversation du memebre
 * @returns {undefined}
 */
MessageAction.RefreshMessage = function ()
{
    let actualMessageUser = document.getElementById("actualMessageUser");

    let data = "App=Message&Methode=RefreshMessage";
    let url = window.location.origin + "/Ajax.php";

    Request.Post(url, data).then(data => {

        actualMessageUser.innerHTML = data;

        Dashboard.AddEventByClass('discussPreview', "click", MessageAction.LoadMessage);
    });
};

/**
 * Envoi une piéce jointe
 * @returns {undefined}
 */
MessageAction.SendAttachment = function () {

    let messageContainer = document.getElementById("messageContainer");
    messageContainer.style.display = "none";

    let imageButton = document.getElementsByClassName("fa-image");
    imageButton[0].style.display = "none";

    Dashboard.AddEventByClass("fa-remove", "click", function () {
        messageContainer.style.display = "";
        imageButton[0].style.display = "";
    });

};

/**
 * Remet à jour les filtres
 * @returns {undefined}
 */
MessageAction.ResetFilter = function ()
{
    let tbSearchConversation = document.getElementById("tbSearchConversation");
    tbSearchConversation.value = "";

    MessageAction.SerchConversation();
};

/**
 * 
 * @returns {undefined}
 */
MessageAction.SerchConversation = function ()
{
    let tbSearchConversation = document.getElementById("tbSearchConversation");
    let resetFilter = document.getElementById("resetFilter");
    let iconeSearch = document.getElementById("iconeSearch");

    if (tbSearchConversation.value != "")
    {
        resetFilter.style.display = "inline-block";
        iconeSearch.style.display = "none";
    } else {
        resetFilter.style.display = "none";
        iconeSearch.style.display = "inline-block";
    }

    let discussPreview = document.getElementsByClassName("discussPreview");

    for (let i = 0; i < discussPreview.length; i++) {

        let title = discussPreview[i].getElementsByClassName("title");

        if (!title[0].innerHTML.includes(tbSearchConversation.value))
        {
            discussPreview[i].style.display = "none";
        } else {
            discussPreview[i].style.display = "block";
        }
    }
};

/**
 * Tout marqué comme vu
 * @returns {undefined}
 */
MessageAction.SetAllView = function ()
{
    MessageAction.CloseActionMenu();

    let url = window.location.origin + "/Ajax.php";
    let data = "App=Message&Methode=SetAllView";

    Request.Post(url, data).then(data => {

        MessageAction.UpdateNumberMessageRead();
    });
};

/**
 * Met à jour le nombre de message
 * @returns {undefined}
 */
MessageAction.UpdateNumberMessageRead = function () {

    let nbInfo = document.getElementsByClassName("nbInfo");

    if (nbInfo.length > 0) {
        let data = "App=Message&Methode=GetNumberNotRead";
        let url = window.location.origin + "/Ajax.php";

        Request.Post(url, data).then(data => {
            nbInfo[0].innerHTML = data;
        });
    }
};

/***
 * Menu pour supprimer un message
 * @returns {undefined}
 */
MessageAction.OpenMenuMessage = function (e)
{
    this.core = document.createElement('div');
    this.core.id = "actionMenu";
    this.core.style.position = 'absolute';
    this.core.style.width = "200px";
    this.core.style.height = "150px";
    this.core.style.overflow = "auto";
    this.core.style.border = '1px solid grey';
    this.core.style.padding = "5px";
    this.core.style.left = e.clientX - 150 + "px";
    this.core.style.top = e.pageY + "px";
    this.core.style.backgroundColor = "white";
    this.core.innerHTML = "<div style='text-align:right;width:100%'><i class='fa fa-remove' alt='' title='Fermer' onclick='CloseTool(this)'></i></div>";
    this.core.innerHTML += "<span>Chargement...</span>";

    let data = "App=Message&Methode=GetMoreActionMessage";
    data += "&messageId=" + e.srcElement.parentNode.id;

    let url = window.location.origin + "/Ajax.php";

    Request.Post(url, data).then(data => {

        let content = this.core.getElementsByTagName("span");
        content[0].innerHTML = data;

        Dashboard.AddEventById("removeActionMessage", "click", MessageAction.RemoveConversation);
        Dashboard.AddEventById("setAllView", "click", MessageAction.SetAllView);
    });

    document.body.appendChild(this.core);
};

/**
 * Ferme le menu d'aide
 */
MessageAction.CloseActionMenu = function ()
{
    let actionMenu = document.getElementById("actionMenu");

    if (actionMenu) {
        actionMenu.parentNode.removeChild(actionMenu);
    }
};

/***
 * Click sur la premiere conversation
 */
MessageAction.LoadFirst = function () {

    let MessagePreview = document.getElementsByClassName("discussPreview");

    if (MessagePreview.length > 0 && document.body.clientWidth > 375) {
        MessagePreview[0].click();
    }
};
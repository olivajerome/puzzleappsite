<div style='display:block; width:100%; height: 450px; background-size: cover;background-image: url({{Article->GetImageFullPath()}})'>

</div>

<div class='centerBlock'>
    <h1>{{Article->Name->Value}}</h1>

    {{Article->Content->Value}}
</div>

<div>
    <div id='dvAddComment' >
        {{commentPlugin}}
    </div>
</div>


<div class="centerBlock">
    <div class='row'>
        <h3>{{GetCode(Blog.YouCanLike)}}</h3>

        {{foreach Articles}}
        <div class='col-md-4'>
            <div class='block'> 
                <div class='col-md-12' >
                    {{element->GetImage()}}
                </div>
                <div class='col-md-12'>
                    <h4>{{element->Name->Value}}</h4>
                    {{element->GetSmallDescription()}}
                </div>
                <div  class='col-md-12'>
                    <a href='{{GetPath(/Blog/Article/)}}{{element->GetUrlCode()}}' >{{GetCode(Blog.ReadThisArticle)}} </a>
                </div>
            </div>
        </div>
        {{/foreach Articles}}
    </div>
</div>


{{commentPluginScript}}
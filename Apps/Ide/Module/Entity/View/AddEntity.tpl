<div class='marginTop'>
	<div class='col_9 alignCenter'>
            <div>
                <label>{{GetCode(Ide.EntityName)}} </label>
                {{tbNameEntity}}
            </div>
            <div class='marginTop'>
                <label>{{GetCode(Ide.Shared)}} </label>
                {{cbShared}}
            </div>
        </div>
        <div id='dvField' class='marginTop'>
            <h3 >{{GetCode(Ide.Fields)}}</h3>
            <b class='fa fa-plus' onclick='IdeElement.AddField("taField")'>&nbsp</b>
            <br/>
            <table id='taField' class='grid'>
                <tr>
                    <th>{{GetCode(Ide.Name)}}</th>
                    <th>{{GetCode(Ide.Type)}}</th>
                    <th>{{GetCode(Ide.Null)}}</th>
                </tr>
            </table>
        </div>
        <div id='dvKey' class='marginTop'>
            <h3>{{GetCode(Ide.Key)}}</h3>
             <b class='fa fa-plus' onclick='IdeElement.AddKey("taKey")'>&nbsp</b><br/>
            <table id='taKey' class='grid'>
                <tr>
                    <th>{{GetCode(Ide.BaseEntite)}}</th>
                    <th>{{GetCode(Ide.BaseField)}}</th>
                    <th>{{GetCode(Ide.Field)}}</th>
                </tr>
            </table>
        </div>
        <div class='alignCenter' id='dvResultEntity'>
        </div>
        <div class='marginTop center'>
        {{btnCreate}}
        </div>
</div>    


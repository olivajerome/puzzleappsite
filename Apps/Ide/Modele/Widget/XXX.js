var XXX = function(){};

/***
 * Init the form
 * @returns {undefined}
 */
XXX.Init = function(){
    Event.AddById("btnSave", "click", XXX.SaveElement);
};

/***
* Get the Id of element 
*/
XXX.GetId = function(){
    return Form.GetId("XXXForm");
};

/*
* Save the Element
*/
XXX.SaveElement = function(e){
   
    if(Form.IsValid("XXXForm"))
    {
    let request = new Http("XXX", "SaveElement");
        request.SetData(Form.Serialize("XXXForm"));

        request.Post(data => {

            data = JSON.parse(data);

            if(data.status == "error"){
                Form.RenderError("XXXForm", data.data.message);
            } else{

                Form.SetId("XXXForm", data.data.Id);
            }
        });
    }
};


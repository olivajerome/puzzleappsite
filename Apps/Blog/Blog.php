<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Blog;

use Core\App\Application;
use Core\Core\Core;
use Core\Core\Request;
use Core\Core\Response;
use Core\Utility\File\File;
use Core\Utility\ImageHelper\ImageHelper;
use Apps\Blog\Entity\BlogArticle;
use Apps\Blog\Entity\BlogBlog;
use Apps\Blog\Helper\ArticleHelper;
use Apps\Blog\Helper\BlogHelper;
use Apps\Blog\Helper\CategoryHelper;
use Apps\Blog\Helper\CommentHelper;
use Apps\Blog\Helper\SitemapHelper;
use Apps\Blog\Module\Article\ArticleController;
use Apps\Blog\Module\Blog\BlogController;
use Apps\Blog\Module\Category\CategoryController;
use Apps\Blog\Module\Front\FrontController;
use Apps\Blog\Module\Admin\AdminController;
use Apps\Blog\Module\Api\ApiController;

class Blog extends Application {

    /**
     * Auteur et version
     * */
    public $Author = 'Webemyos';
    public $Version = '2.1.0.1';
    public static $Directory = "../Apps/Blog";

    /**
     * Constructeur
     * */
    function __construct($core = "") {
        parent::__construct($core, "Blog");
        $this->Core = Core::getInstance();
    }

    /**
     * Execution de l'application
     */
    function Run() {
        echo parent::RunApp($this->Core, "Blog", "Blog");
    }

    /**
     * Set the Public Routes
     */
    public function GetRoute() {
        $this->Route->SetPublic(array("Category", "Article", "Subscribe"));

        return $this->Route;
    }

    /*     * *
     * Execute action after install
     */

    function PostInstall() {
        \Apps\EeApp\Helper\AppHelper::UpdateAppParams($this->Core,
                "Blog",
                $this->Version,
                "Gestionnaire du contenu du blog",
                1,
                0
        );

        \Apps\EeApp\Helper\AppHelper::CreateDataDir("Blog");
    }

    /*
     * Home Page Blog
     */

    function Index() {
        $frontController = new FrontController($this->Core);
        return $frontController->Index();
    }

    /*
     * Get The Categorie
     */

    function Category($params) {
        $frontController = new FrontController($this->Core);
        return $frontController->Category($params);
    }

    /*
     * Get The Categorie
     */

    function Article($params) {
        $frontController = new FrontController($this->Core);
        return $frontController->Article($params);
    }

    /*
     * Insription newsletters
     */

    function Subscribe() {
        if (BlogHelper::SaveUserNewLetter($this->Core, Request::GetPost("Email"))) {
            return Response::Success(array("Message" => $this->Core->GetCode("Blog.SubscribeSuccess")));
        }
    }

    /**
     * Save the Blog
     */
    function SaveBlog() {

        $adminController = new AdminController($this->Core);
        return $adminController->SaveBlog($this->Core, Request::GetPosts());
    }

    /**
     * Save a catégorie
     */
    function SaveCategory() {
        $adminController = new AdminController($this->Core);

        if ($adminController->SaveCategory($this->Core, Request::GetPosts())) {

            $blog = BlogHelper::GetFirst($this->Core);

            return Response::Success($adminController->GetTabCategory($blog));
        } else {
            return Response::Error();
        }
    }

    /*     * *
     * Delete a catégorie
     */

    function DeleteCategory() {

        $adminController = new AdminController($this->Core);

        if ($adminController->DeleteCategory($this->Core, Request::GetPost("categoryId"))) {

            $blog = BlogHelper::GetFirst($this->Core);
            return Response::Success($adminController->GetTabCategory($blog));
        } else {
            return Response::Error();
        }
    }

    /**
     * Save article
     */
    function SaveArticle() {

        $adminController = new AdminController($this->Core);

        if ($adminController->SaveArticle($this->Core, Request::GetPosts())) {

            $blog = BlogHelper::GetFirst($this->Core);

            return Response::Success($adminController->GetTabArticle($blog));
        } else {
            return Response::Error();
        }
    }

    /*     * *
     * Supprime un article
     */

    function DeleteArticle() {
        $adminController = new AdminController($this->Core);

        if ($adminController->DeleteArticle($this->Core, Request::GetPost("articleId"))) {

            $blog = BlogHelper::GetFirst($this->Core);
            $adminController = new AdminController($this->Core);
            return Response::Success($adminController->GetTabArticle($blog));
        } else {
            return Response::Error();
        }
    }

    /**
     * Met à jour le contenu
     */
    function UpdateContent() {
        $adminController = new AdminController($this->Core);
        $adminController->UpdateContent($this->Core, Request::GetPost("Id"), Request::GetPost("Content"));
        return Response::Success(array("Message" => "ArticleContentUpdated"));
    }

    /**
     * Sauvegare les images de presentation
     */
    function DoUploadFile($idElement, $tmpFileName, $fileName, $action) {
        //Ajout de l'image dans le repertoire correspondant
        $directory = "Data/Apps/Blog/";

        File::CreateDirectory($directory);

        File::CreateDirectory($directory . $idElement);

        switch ($action) {

            case "SaveImageArticle" :
                $directory = "Data/Tmp";

                move_uploaded_file($tmpFileName, $directory . "/" . $idElement . ".jpg");

                echo Response::Success(array("type:" => "IMAGE", "Message" => "OK"));

                break;
            default :

                if ($action == "UploadImport") {

                    $fileTmp = $directory . $folder . "import.csv";

                    move_uploaded_file($tmpFileName, $fileTmp);

                    echo Response::Success(array("type:" => "DOCUMENT", "file" => $fileTmp));
                } else {

                    //Sauvegarde
                    move_uploaded_file($tmpFileName, $directory . $idElement . "/" . $fileName);

                    //renommage du fichier
                    $fileNameMini = str_replace(".png", "", $fileName);
                    $fileNameMini = str_replace(".jpg", "", $fileNameMini);
                    $fileNameMini = str_replace(".jpeg", "", $fileNameMini);
                    $fileNameMini = str_replace(".ico", "", $fileNameMini);

                    //Crée un miniature
                    $image = new ImageHelper();
                    $image->load($directory . $idElement . "/" . $fileName);
                    $image->fctredimimage(48, 0, $directory . $idElement . "/" . $fileNameMini . "_96.png");

                    echo Response::Success(array("type:" => "IMAGE", "Message" => "OK"));
                }

                break;
        }
    }

    /**
     * Obtien l'image de l'article
     */
    function GetImageArticle() {
        $article = new BlogArticle($this->Core);
        $article->GetById(Request::GetPost("ArticleId"));

        $ArticleController = new ArticleController($this->Core);
        return $ArticleController->GetImageArticle($article->BlogId->Value, $article->IdEntite);
    }

    /**
     * Obtient les images du blogs
     * format niormal et mini
     */
    function GetImages() {
        return BlogHelper::GetImages($this->Core, Request::GetPost("BlogId"));
    }

    /**
     * Obtient les blgo lié a une App
     */
    function GetByApp($appName, $entityName, $entityId) {
        return BlogHelper::GetByApp($this->Core, $appName, $entityName, $entityId);
    }

    /**
     * Affiche le blog en front office
     * @param type $name
     */
    function Display($name = "") {
        $this->ShowBlog($name);
    }

    /**
     * Ajout un Email à la newsletter du blog
     */
    function AddEmailNews() {
        $email = Request::GetPost("tbEmailNews");
        $blogId = Request::GetPost("BlogId");

        if ($email != "") {
            echo "<h4>" . $this->Core->GetCode("Blog.UserNewsLetterSaved") . "</h4>";

            BlogHelper::SaveUserNewLetter($this->Core, $email, $blogId);
        } else {
            echo "<span class='error'>" . $this->Core->GetCode("Blog.ErrorUserNews") . "</span>";
            $blogController = new BlogController($this->Core);

            echo $blogController->GetNewLetterBlock($blogId);
        }
    }

    /**
     * Synchronise le blog avec le blgo distant
     */
    function Synchronise() {
        echo BlogHelper::Synchronise($this->Core, Request::GetPost("BlogId"));
    }

    /**
     * Get The siteMap 
     */
    public function GetSiteMap($url = false) {
        return SitemapHelper::GetSiteMap($this->Core, $url);
    }

    /*     * *
     * Get the widget
     */

    public function GetWidget($type = "", $params = "") {

        switch ($type) {
            case "AdminDashboard" :
                $widget = new \Apps\Blog\Widget\AdminDashBoard\AdminDashBoard($this->Core);
                break;
            case "LastArticle" :
                $widget = new \Apps\Blog\Widget\LastArticle\LastArticle($this->Core);
                break;
        }

        return $widget->Render();
    }

    /*     * **
     * List of AddAblbe Widget on CMS
     */

    public function GetListWidget() {
        return array(array("Name" => "LastArticle",
                "Description" => $this->Core->GetCode("Blog.DescriptionLastArticle")),
        );
    }

    /*     * *
     * Export category and article
     */

    function Export() {
        $AdminController = new AdminController($this->Core);
        return $AdminController->Export();
    }
    
    /***
     * Import the blog
     */
    function Import() {
        $AdminController = new AdminController($this->Core);
        return Response::Success($AdminController->Import(Request::GetPosts()));
    }
    
    /***
     * Get category
     */
    function GetListCategory(){
        $apiController = new ApiController($this->Core);
        return Response::Success($apiController->GetListCategory());
    }
    
    /*     * *
     * Get List article of category
     */

    public function GetListArticles($code) {
        $apiController = new ApiController($this->Core);
        return Response::Success($apiController->GetListArticles($code));
    }
    
    /***
     * Get Detail Of Article
     */
    public function GetArticle($code) {
        $apiController = new ApiController($this->Core);
        return Response::Success($apiController->GetArticle($code));
    }
    
}

?>

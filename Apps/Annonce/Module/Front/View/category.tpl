<section class='container'>
    
    {{GetControl(Hidden,annonce,{Value={{Category->IdEntite}},Id=categoryId,)})}}
     
    
    <h1>{{GetCode(Annonce.AnnonceOfCategory)}}
        {{Category->Name->Value}}
    </h1>
    <p>{{Category->Description->Value}}</p>

    
    <div class='col-md-12'>
        
       {{foreach Annonces}}
    
        <a href='{{GetPath(/Annonce/Annonce/{{element->Code->Value}})}}'>
            <div class='col-md-3'>
                <div class='block' style='height:350px'>
                    <span style='display:block; width:100%; height: 100px; background-size: cover;background-image: url({{element->GetImagePath()}})'>
                    </span>

                    <h3 style="min-height:60px"> {{element->Title->Value}}
                    </h3>
                   <div style='height:130px;margin'>
                       <p>
                       {{element->Description->Value}}
                       </p>
                   </div>
                </div>
            </div>
        </a>
        {{/foreach Annonces}}
    
    </div>
    
    <div class='col-md-12 center marginTop'>
    
        {{if ShowAddAnnonce == true}}
                {{GetControl(Button,btnAddAnnonce,{LangValue=Annonce.AddAnnonce,CssClass=btn btn-primary,Id=btnAddAnnonce,)})}}
        {{/if ShowAddAnnonce == true}}
        

        {{if ShowAddAnnonce == false}}
         <i class='fa fa-star'>  {{GetCode(Annonce.ConnectOrSignupForCreateAnnonce)}}</i>
        {{/if ShowAddAnnonce == false}}
    </div> 
    
    
    
    
</section>

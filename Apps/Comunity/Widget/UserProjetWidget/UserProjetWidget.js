

var UserProjetWidget = function(){};

/***
 * Initialisation du widget
 * @returns {undefined}
 */
UserProjetWidget.Init = function(){
    
   //Les différents conteneur 
   var wallProjet = document.getElementById('wallProjet'); 
   var dashboardDiscus = document.getElementById("dashboardDiscus");
   var lstProjet  = document.getElementById("lstProjet");
   
   
   var reseauCode  = document.getElementById("reseauCode");
   
   Dashboard.AddEventByClass("stateProjet", "click", UserProjetWidget.loadTypeProjet);
   Dashboard.AddEventByClass("linkProjet", "click", UserProjetWidget.loadProjet);
};

/**
 * Charge les projets selon leur état
 * Actis/En création/ Terminé
 * @returns {undefined}
 */
UserProjetWidget.loadTypeProjet = function(e){
    
    var data = "App=ParlonsEn&Methode=LoadTypeProjet";
        data += "&statut=" + e.srcElement.id; 
        data += "&reseauCode=" +reseauCode.value; 

     var url = window.location.origin + "/Ajax.php";
     var userProjetWidget = document.getElementById("userProjetWidget");

     Request.Post(url, data).then(data=>{
          userProjetWidget.innerHTML = data;
          
          UserProjetWidget.Init();
    });
};

/***
 * Charge les discsusions du projet
 * @param {type} e
 * @returns {undefined}
 */
UserProjetWidget.loadProjet = function(e){
    
    if(e.srcElement.id == "")
    {
        projetId =  e.srcElement.parentNode.id ?  e.srcElement.parentNode.id : e.srcElement.parentNode.parentNode.id;
    } 
    else
    {
         projetId = e.srcElement.id;
    }
    
     var data = "App=ParlonsEn&Methode=LoadProjet";
        data += "&projetId=" + projetId; 

     var hdDiscussId = document.getElementById("hdDiscussId");
         hdDiscussId.value = projetId;
                 
     var url = window.location.origin + "/Ajax.php";

     Request.Post(url, data).then(data=>{
         
           wallProjet.innerHTML = data;
           
           if(wallProjet.className == "col-md-6"){
               //Sin on est sur tous les projets
               wallProjet.className = "col-md-9";
               
               var AboutProjet = document.getElementById("AboutProjet");
               AboutProjet.style.display = "none";
           }
           ParlonsEn.Init();
           ActionProjetWidget.Init();
           MemberProjetWidget.Init();
    });
};

/**
 * Rafraichit les projets de l'utilisateur 
 * @returns nothing
 */
UserProjetWidget.LoadUserProjet = function()
{
    var data = "App=ParlonsEn&Methode=LoadUserProjet";
        data += "&reseauCode=" +  reseauCode.value;
     var url = window.location.origin + "/Ajax.php";

     Request.Post(url, data).then(data=>{
         
            lstProjet.parentNode.parentNode.innerHTML = data;
            
            UserProjetWidget.Init();
    });
};
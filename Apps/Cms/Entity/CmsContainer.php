<?php 
/*
* PuzzleApp
* Webemyos
* Jérôme Oliva
* GNU Licence
*/

namespace Apps\Cms\Entity;
use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;
use Apps\Cms\Entity\CmsBlock;

class CmsContainer extends Entity  
{
	//Constructeur
	function __construct($core)
	{
		//Version
		$this->Version ="2.0.0.0"; 

		//Nom de la table 
		$this->Core=$core; 
		$this->TableName="CmsContainer"; 
		$this->Alias = "CmsContainer"; 

		$this->Name = new Property("Name", "Name", TEXTAREA,  false, $this->Alias); 
		$this->Code = new Property("Code", "Code", TEXTAREA,  false, $this->Alias); 
		$this->Position = new Property("Position", "Position", NUMERICBOX,  false, $this->Alias); 
		$this->PageId = new Property("PageId", "PageId", NUMERICBOX,  false, $this->Alias); 
		$this->Style = new Property("Style", "Style", TEXTBOX,  false, $this->Alias); 
		$this->CssClass = new Property("CssClass", "CssClass", TEXTBOX,  false, $this->Alias); 

		//Creation de l entité 
		$this->Create(); 
	}

	/***
	 * Get The block Rendering
	 */
	function RenderBlocks($editor = true){

		$cmsBlock = new CmsBlock($this->Core);
		$blocks = $cmsBlock->Find("ParentId=".$this->IdEntite . " ORDER BY Position" );

		$editorDecorator = new \Apps\Cms\Decorator\Editor\EditorDecorator($this->Core);
		
		$view = "";

		foreach($blocks as $block){
			if($editor){
			
			$view .= "<div id='".$block->IdEntite."'  class='contentBlock col-md-".$block->Size->Value." subBlock'>";
			$view .= " <div class='tool'>
							<i class='fa fa-edit btnEditBlock'></i>
							<i class='fa fa-remove btnRemoveBlock'></i>
						</div>";
			$view .= $editorDecorator->GetBlock($block);
			$view .= "</div>";
			} else {
				$view .= $editorDecorator->GetFrontBlock($block);
			}
		}

		return $view;
	}

	/***
	 * Get The Front block Rendering
	 */
	function RenderFrontBlocks(){
		return $this->RenderBlocks(false);
	}

}
?>
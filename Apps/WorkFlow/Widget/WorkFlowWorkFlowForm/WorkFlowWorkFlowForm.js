var WorkFlowWorkFlowForm = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
WorkFlowWorkFlowForm.Init = function(callBack){
    Event.AddById("btnSaveWorkFlow", "click", WorkFlowWorkFlowForm.SaveElement);
};

/***
* Obtient l'id de l'itin�raire courant 
*/
WorkFlowWorkFlowForm.GetId = function(){
    return Form.GetId("WorkFlowWorkFlowFormForm");
};

/*
* Sauvegare l'itineraire
*/
WorkFlowWorkFlowForm.SaveElement = function(e){
   
    if(Form.IsValid("WorkFlowWorkFlowForm"))
    {

        let request = new Http("WorkFlow", "SaveWorkFlow");
        request.SetData(Form.Serialize("WorkFlowWorkFlowForm"));

        request.Post(function(data){

            data = JSON.parse(data);

            if(data.statut == "Error"){
                Form.RenderError("WorkFlowWorkFlowForm", data.message);
            } else {

                Form.SetId("WorkFlowWorkFlowForm", data.data.Id);

                Dialog.Close();

                WorkFlow.RefreshWorkflow(data);
            }
        });
    }
};


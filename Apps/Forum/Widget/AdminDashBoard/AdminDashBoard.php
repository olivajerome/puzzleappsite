<?php

namespace Apps\Forum\Widget\AdminDashBoard;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;
use Core\Entity\Entity\Argument;
use Apps\Forum\Helper\MessageHelper;

class AdminDashBoard {
    
    public function __construct($core) {
        $this->Core = $core;
    }
    
    /***
     * Render the widget
     */
    public function Render(){
        $view = new View(__DIR__ . "/View/adminDashboard.tpl", $this->Core);
       
        $messages = MessageHelper::GetLastMessages($this->Core);
        
        $view->AddElement(new ElementView("Messages", $messages));
        
        return $view->Render();
    }
}

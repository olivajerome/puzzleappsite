CREATE TABLE IF NOT EXISTS `EeAppCategory` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`Name` VARCHAR(200)  NULL ,
PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `EeAppApp` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`CategoryId` INT  NULL ,
`Name` VARCHAR(200)  NULL ,
`Description` TEXT  NULL ,
`Version` TEXT  NULL ,
`Widget` INT  NULL ,
`Member` INT  NULL ,
`Actif` INT NULL,
`AppName` VARCHAR(200)  NULL ,
`AppId` INT  NULL ,
`EntityName` VARCHAR(200)  NULL ,
`EntityId` INT  NULL ,
PRIMARY KEY (`Id`),
CONSTRAINT `EeAppCategory_EeAppApp` FOREIGN KEY (`CategoryId`) REFERENCES `EeAppCategory`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 


CREATE TABLE IF NOT EXISTS `EeAppUser` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`UserId` INT  NULL ,
`AppId` INT  NULL ,
PRIMARY KEY (`Id`),
CONSTRAINT `ee_user_EeAppUser` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`),
CONSTRAINT `EeApp_EeAppUser` FOREIGN KEY (`AppId`) REFERENCES `EeAppApp`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 


CREATE TABLE IF NOT EXISTS `EeAppAdmin` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`AppId` INT  NULL ,
`UserId` INT  NULL ,
PRIMARY KEY (`Id`),
CONSTRAINT `EeAppApp_EeAppAdmin` FOREIGN KEY (`AppId`) REFERENCES `EeAppApp`(`Id`),
CONSTRAINT `ee_user_EeAppAdmin` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

INSERT INTO EeAppCategory (`Name`) VALUE ('Administration');

INSERT INTO EeAppApp(`CategoryId`, `Name`, `Description`, `Widget`) VALUE( 1, "Cms", "Gestionnaire du contenu", 1);
INSERT INTO EeAppApp(`CategoryId`, `Name`, `Description`, `Widget`) VALUE( 1, "EeApp", "Gestionnaires des applications", 1);
INSERT INTO EeAppAdmin(`UserId`, `AppId`) VALUE( 1, 2);
INSERT INTO EeAppApp(`CategoryId`, `Name`, `Description`, `Widget`) VALUE( 1, "Lang", "Gestionnaire des traductions", 1);
INSERT INTO EeAppApp(`CategoryId`, `Name`, `Description`) VALUE( 1, "Ide", "Application de developpement");

INSERT INTO EeAppUser(`UserId`, `AppId`) VALUE( 1, 1);
INSERT INTO EeAppUser(`UserId`, `AppId`) VALUE( 1, 2);
INSERT INTO EeAppUser(`UserId`, `AppId`) VALUE( 1, 3);
INSERT INTO EeAppUser(`UserId`, `AppId`) VALUE( 1, 4);

CREATE TABLE IF NOT EXISTS `EeWidgetUser` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`UserId` INT  NULL ,
`AppId` INT  NULL ,
`Ordre` INT  NULL ,
PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `EeAppPlugin` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`Name` VARCHAR(200)  NULL ,
`Code` VARCHAR(200)  NULL ,
`Description` TEXT  NULL ,
PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `EeAppPluginApp` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`AppId` INT  NULL ,
`PluginId` INT  NULL ,
PRIMARY KEY (`Id`),
CONSTRAINT `EeAppApp_EeAppPluginApp` FOREIGN KEY (`AppId`) REFERENCES `EeAppApp`(`Id`),
CONSTRAINT `EeAppPlugin_EeAppPluginApp` FOREIGN KEY (`PluginId`) REFERENCES `EeAppPlugin`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `EeAppTemplate` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`Name` TEXT  NOT NULL,
`Code` TEXT  NOT NULL,
`Description` VARCHAR(200)  NOT NULL,
`Actif` int  NULL,
PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 


INSERT INTO EeAppTemplate(`Name`, `Code`, `Description` , `Actif`) VALUE( "Défaut", "Defaullt","Théme par default", 1);

<h1>{{GetCode(Blog.NoBlog)}}</h1>

<div class='container'>
    <div class='row'>
        <div class='col-md-12'>
            <div class='info'><i class='fa fa-info'></i>
               {{GetCode(Blog.NewBlogDescription)}}
            </div>
            
              {{blogForm}}
        </div>
       
    </div>
</div>
        

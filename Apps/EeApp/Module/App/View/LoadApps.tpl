<div class="content-panel">
     <div class='alignLeft'>
          <div class="info"><i class="fa fa-info"></i>
         {{GetCode(EeApp.AppsDescription)}}
        </div>
    </div>
        <table class="table">
            <thead>
                <tr>
                  <th>{{GetCode(Category)}}</th>
                  <th>{{GetCode(Name)}}</th>
                  <th>{{GetCode(Description)}}</th>
                  <th>{{GetCode(Version)}}</th>
                </tr>
            </thead>
            <tbody>
                {{foreach}}
                    <tr>
                        <td>{{element->Category->Value->Name->Value}}</td>
                        <td>{{element->Name->Value}}</td>
                        <td>{{element->Description->Value}}</td>
                        <td>{{element->GetVersion()}}</td>
                        <td>
                            <input type='button' class='btn btn-warning' onclick='EeAppAction.UpdateApp("{{element->Name->Value}}", this)' value='{{GetCode(EeApp.UpdateApp)}}' />
                            <input type='button' class='btn btn-success' onclick='EeAppAction.Add({{element->IdEntite}}, this)' value='{{GetCode(EeApp.AddToMenu)}}' />
                        </td>
                        </td>
                    </tr>
                {{/foreach}}            
                </tbody>
            </table>
        </div>
</div>


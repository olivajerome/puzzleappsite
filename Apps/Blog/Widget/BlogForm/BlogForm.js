var BlogForm = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
BlogForm.Init = function(){
    Event.AddById("btnSave", "click", BlogForm.SaveElement);
    Event.AddById("btnAddPlugin", "click", BlogForm.AddPlugin);
    Event.AddByClass("removePlugin", "click" , BlogForm.RemovePlugin);
    
    BlogForm.RegisterPlugin();
};

/***
 * Registert the plugin
 * @returns {undefined}
 */
BlogForm.RegisterPlugin = function(){
    let pluginScript = Dom.GetById("pluginScript");
    
    if(pluginScript){
        eval(pluginScript.innerHTML);
    }
};

/***
* Obtient l'id de l'itin�raire courant 
*/
BlogForm.GetId = function(){
    return Form.GetId("BlogFormForm");
};

/*
* Sauvegare l'itineraire
*/
BlogForm.SaveElement = function(e){
   
    if(Form.IsValid("BlogFormForm"))
    {

    let data = "Class=Blog&Methode=SaveBlog&App=Blog";
        data +=  Form.Serialize("BlogFormForm");

        Request.Post("Ajax.php", data).then(data => {
            
            if(Form.GetValue("BlogFormForm", "Id") == ""){
                Dashboard.StartApp('', 'Blog', '');
            } else {
               Animation.Notify(Language.GetCode("Blog.BlogSaved"));
           }
        });
    }
};

/***
 * Ajout d'un plugin
 */
BlogForm.AddPlugin = function(e){
  
    e.preventDefault();
    
     Dialog.open('', {"title": Dashboard.GetCode("Blog.AddPlugin"),
     "app": "EeApp",
     "class": "DialogEeApp",
     "method": "AddPluginApp",
     "params": "Blog",
     "type" : "right"
     });
 };
 
 /***
  * Remove plugin to the shop 
  * @returns {undefined}
  */
 BlogForm.RemovePlugin = function(e){
     
     Animation.Confirm(Language.GetCode("Blog.ConfirmRemovePlugin"), function(){
         let container = e.srcElement.parentNode;
         
         let  data = "Class=EeApp&Methode=RemovePluginApp&App=EeApp";
              data += "&pluginId="+e.srcElement.id;
              
         Request.Post("Ajax.php", data).then(data => {
             container.parentNode.removeChild(container);
         });
     });
 };
 
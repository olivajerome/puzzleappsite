<?php

/**
 * Module d'accueil
 * */

namespace Apps\Cms\Module\Api;

use Core\Control\Button\Button;
use Core\Controller\Controller;
use Core\View\View;
use Core\View\ElementView;
use Apps\Cms\Entity\CmsPage;
use Apps\Cms\Widget\PageContent\PageContent;

class ApiController extends Controller {

    /***
     * Get data and content HTML of a page
     */
    public function GetPage($name){

        $page = new CmsPage($this->Core);
        $page = $page->GetByName($name);

        $widget = new PageContent($this->Core);

        $pageArray = $page->ToArray();
        $pageArray["html"] = $widget->Render($name);

        return $pageArray; 
    }

    /***
     * Get All Page available
     */
    public function GetListPage(){
        $page = new CmsPage($this->Core);
        $pages = $page->GetAll();
        
        return $page->ToAllArray($pages); 
    }

}
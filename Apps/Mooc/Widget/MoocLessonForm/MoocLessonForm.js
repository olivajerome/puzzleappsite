var MoocLessonForm = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
MoocLessonForm.Init = function(callBack){

    Event.AddById("btnSaveLesson", "click", MoocLessonForm.SaveLesson);
   
    let content = document.querySelector("#lessonForm #Content");
    
    MoocLessonForm.txtEditor = new TextRichEditor(content,
            {tools: ["ImageTool", "SourceCode" , "HtmlToCode"],
                events: [{"tool": "ImageTool", "events": [{"type": "mousedown", "handler": MoocLessonForm.OpenImageLibrary}]}],
            }
    );
 
    MoocLessonForm.callBack = callBack;
};

MoocLessonForm.OpenImageLibrary = function(editor, node){
     
     if (node.tagName != "DIV") {
        Animation.Notify(Language.GetCode("Base.TextRichEditorCantAddImageAtThisEmplacement"));
        return;
    }

    editor.setSelectedNode(node);
    Dashboard.currentEditor = editor;

    let blockId = editor.sourceControl.parentNode.id;

    Dialog.open('', {"title": Dashboard.GetCode("Cms.Addmage"),
        "app": "Cms",
        "class": "DialogCms",
        "method": "AddImage",
        "type": "left",
        "params": blockId
    });
};

/***
* Obtient l'id de l'itin�raire courant 
*/
MoocLessonForm.GetId = function(){
    return Form.GetId("categoryForm");
};

/*
* Sauvegare l'itineraire
*/
MoocLessonForm.SaveLesson = function(e){
   
    if(Form.IsValid("lessonForm"))
    {

    var data = "Class=Mooc&Methode=SaveLesson&App=Mooc";
        data +=   Form.Serialize("lessonForm");
        data += "&Content=" + MoocLessonForm.txtEditor.getCleanContent(); 
        
        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);
         
            if(data.statut == "Error"){
                Form.RenderError("articleForm", data.message);
            } else {
   
                if(MoocLessonForm.callBack){
                    MoocLessonForm.callBack(data);
                }
            }
        });
    }
};


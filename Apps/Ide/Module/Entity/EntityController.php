<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Ide\Module\Entity;

use Core\Control\Button\Button;
use Core\Control\CheckBox\CheckBox;
use Core\Control\ListEditTemplate\ListEditTemplate;
use Core\Control\TextBox\TextBox;
use Core\Controller\AdministratorController;
use Core\Core\Request;
use Core\Dashboard\DashBoardManager;
use Core\Control\EntityGrid\EntityGrid;
use Core\Control\EntityGrid\EntityColumn;
use Core\Control\EntityGrid\EntityIconColumn;
use Core\Control\EntityGrid\EntityFunctionColumn;
use Core\Entity\Entity\Argument;
use Core\View\View;
use Core\View\ElementView;

class EntityController extends AdministratorController {

    /**
     * Pop in d'ajout d'entite
     */
    function ShowAddEntity() {
        $view = new View(__DIR__ . "/View/AddEntity.tpl", $this->Core);

        //Nom 
        $tbNameEntity = new TextBox("tbNameEntity");
        $tbNameEntity->PlaceHolder = $this->Core->GetCode("Name");
        $view->AddElement($tbNameEntity);

        //Entité paretragé entre application
        $cbShared = new CheckBox("cbShared");
        $view->AddElement($cbShared);

        //Bouton de creation
        $btnCreate = new Button(BUTTON, "btnCreate");
        $btnCreate->CssClass = "btn btn-success";
        $btnCreate->Value = $this->Core->GetCode("Ide.CreateTheEntity");
        $btnCreate->OnClick = "IdeElement.CreateEntity();";

        $view->AddElement($btnCreate);

        return $view->Render();
    }

    /**
     * Affiche les donnée de l'entité
     */
    function ShowDataEntity() {

        $EntityName = Request::GetPost("Entity");
        $projetName = Request::GetPost("Projet");
        $EntityPath = "Apps\\" . $projetName . "\Entity\\" . $EntityName;

        $gdCategory = new EntityGrid("gdCategory", $this->Core);
        $gdCategory->Entity = $EntityPath;
        $gdCategory->App = "Mooc";
        $gdCategory->Action = "GetTabCategory";

        $btnAdd = new Button(BUTTON, "btnAddCategory");
        $btnAdd->Value = $this->Core->GetCode("Mooc.AddCategory");

        $gdCategory->AddButton($btnAdd);

        $entity = new $EntityPath($this->Core);

        foreach ($entity->GetProperty() as $property) {
            $gdCategory->AddColumn(new EntityColumn($property->Name, $property->Name));
        }


        //$gdCategory->AddColumn(new EntityColumn("Name", "Name"));

        $gdCategory->AddColumn(new EntityIconColumn("Action",
                        array(array("EditIcone", "Mooc.EditCategory", "Mooc.EditCategory"),
                    array("DeleteIcone", "Mooc.DeleteCategory", "Mooc.DeleteCategory"),
                        )
        ));

        return $gdCategory->Render();
    }
}

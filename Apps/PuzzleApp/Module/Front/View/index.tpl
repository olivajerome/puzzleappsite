<section id='hero'>
    <div class='hero-container container'>
        <h1>The hybride Framework</h1>
        <h2>All the tools to build great websites</h2>
        <p>Quickly develop your websites without an external librarian</p>
        
    </div>
</section>

<section id="testimonials" class="padd-section text-center wow fadeInUp">
    <div class="container">
        <div class="row justify-content-center">

            <div class="col-md-12">

                <div class="testimonials-content row">
                    
                    <div class='col-md-5'>
        <h3 class='fa fa-puzzle-piece fa-2x'>&nbsp;Un framework simple </h3>
            <ul style='text-align:left'>
                <li>Modèle MVC avec une surchage simple des templates. 
                   <i class='mention fa fa-check'>&nbsp;Pour adapter rapidement votre design.</i>
                </li>
                <li>Modèle MVC coté client. 
                   <i class='mention fa fa-check'>&nbsp;Pour disposer d'interface UX au top.</i>
                </li>
                
                <li>Du javascript integré et compilé à la volée. 
                    <i class='mention fa fa-check'>&nbsp;Pas besoin d'integrér des bibliothèques tierces.</i>
                </li>
                <li>Des contrôles qui font presque tout sauf le café. 
                    <i class='mention fa fa-check'>&nbsp;Champ email, date, datagrid, onglet, slider ... </i>
                </li>
                <li>Un ORM simple d'utilisation. 
                    <i class='mention fa fa-check'>&nbsp;Une fonction GetByModele pour définir ce que vous souhaitez obtenir.</i>
                </li>
                <li>Un dashboard tout prêt.
                    <i class='mention fa fa-check'>&nbsp;Pour administrer rapidement votre site.</i>
                </li>
                <li> ...</li>
                
            </ul>    
    </div>
    <div class='col-md-1' >
        <br/><br/>
        <i class='fa fa-plus fa-3x'></i>
    </div>
    
    <div class='col-md-5'>
        <h3 class='fa fa-desktop fa-2x'>&nbsp;Des applications pour toutes les situations.</h3>
        <ul style='text-align:left'>
            <li>Pour gérer vos pages via le cms.</li>
            <li>Pour blogger.</li>
            <li>Pour envoyer et suivre les emails via les newsletters.</li>
            <li>Pour gérer vos documents.</li>
            <li>Pour collaborer.</li>
            <li>Pour obtenir des statistiques</li>
            <li>Pour faires des enquêtes.</li>
          
            <li>...</li>   
        </ul>
    </div>
                    
                    
                </div>
            </div>

        </div>
    </div>
</section>



<section id="features" class="padd-section text-center wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">

    <div class="container">
      <div class="section-title text-center">
        <h2>Pourquoi utilisez PuzzleApp ? </h2>
        <p class="separator">Un seul framework pour tout faire.</p>
      </div>
    </div>

    <div class="container">
      <div class="row">

           <div class="col-md-6 col-lg-3">
          <div class="feature-block">
            <i class='fa fa-desktop fa-2x' ></i>
            <h4>Un framework PHP/JS</h4>
            <h5>Pas besoin d'ajouter des librairies tierces</h5>
            <p>Offrez à vos utilisateurs des interface UX intuitive. Ne cherchez pas à inclure des librairies tierces complexes</p>
          </div>
        </div>

          
        <div class="col-md-6 col-lg-3">
          <div class="feature-block">
            <i class='fa fa-desktop fa-2x' ></i>
            <h4>Une architecture claire</h4>
            <h5>Découpage Des Fonctionnalitées Par Application</h5>
            <p>Une Application Contient La Gestion Du Front, L'administration, Son Modéle De Données Et Ces Connexions Pour Interargir Avec Les Autres.</p>
          </div>
        </div>

        <div class="col-md-6 col-lg-3">
          <div class="feature-block">
            <i class='fa fa-desktop fa-2x' ></i>
            <h4>Adaptable à vos besoins</h4>
            <h5>Surchager tous</h5>
            <p>L'app blog vous convient mais il manque des fonctionnalités, surchargez la et implémentez les votres</p>
          </div>
        </div>
        <div class="col-md-6 col-lg-3">
          <div class="feature-block">
            <i class='fa fa-desktop fa-2x' ></i>
            <h4>Un IDE intégré</h4>
            <h5>Créé vos applications, vos formulaire, vos entité dirctement</h5>
            <p>Il écrira pour vous un partie du code, comme les applicartions, les entités, les modules ..</p>
          </div>
        </div>
    </div>
  </section>

<section id="video" class="text-center wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
    <div class="overlay">
      <div class="container ">
          <h1>Exemple d'application</h1>
          <div class='row' style='pdding-top:20px'>
            <div class="col-md-4 col-lg-3">
                 <div>
                     <h4>blog</h4>
                    <img  src='{{GetPath(\images\maquette\blog.png)}}' /> 
                 </div>
            </div>
             <div class="col-md-4 col-lg-3">
                 <div>
                    <h4>Réseau social</h4>
                    <img  src='{{GetPath(\images\maquette\ReseauSocial.png)}}' /> 
                 </div>
            </div>
            <div class="col-md-4 col-lg-3">
                 <div>
                     <h4>RoadMap</h4>
                     <img  src='{{GetPath(\images\maquette\tdb.png)}}' /> 
                 </div>
            </div>
             <div class="col-md-4 col-lg-3">
                  <div>
                    <h4>Espace membre</h4>
                    <img  src='{{GetPath(\images\maquette\landing.png)}}' /> 
                 </div>
            </div>
          </div>
      </div>
    </div>
  </section>


<section id="pricing" class="padd-section text-center wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">

    <div class="container">
      <div class="section-title text-center">

        <h2>Démarrez tout de suite</h2>
        <p class="separator">Simple et facile à installez.</p>

      </div>
    </div>

    <div class="container">
      <div class="row">

        <div class="col-md-6 col-lg-3">
          <div class="block-pricing" style='height:600px'>
            <div class="table">
              <h4>1. Téléchargez la solution</h4>
              <ul class="list-unstyled">
                <li>Télechargez la solution et déposez la sur votre serveur.</li>
                <li>Lancez l'installation en configuant la base de données et l'administrateur</li>
                </li>Votre site est prêt, vous pouvez commencer à publier des articles.</li>
              </ul>
              <div class="table_btn">
                <a href="{{GetPath(/Downloader/Download/puzzleApp)}}" class="btn"><i class="fa fa-download"></i>Je télécharge</a>
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-6 col-lg-3">
          <div class="block-pricing" style='height:600px'>
            <div class="table">
              <h4>2.Installez les applications.</h4>
              <ul class="list-unstyled">
                <li>Installez simplement des applications</li>
                <li>Blog, Cms, réseau social, RoadMap, Partage de document</li>
                <li>Appliquez vos propres templates sur les applications</li>
                <li>Etendez les méthodes du coeur à votre guise</li>
              </ul>
              <div class="table_btn">
                <a href="{{GetPath(/Store)}}" class="btn"><i class="fa fa-shopping-cart"></i>J'accède au store</a>
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-6 col-lg-3">
          <div class="block-pricing" style='height:600px'>
            <div class="table">
              <h4>2.Codez vos fonctionnalités</h4>
              <ul class="list-unstyled">
                <li>Suivez le tutoriel pour vous former.</li>
                <li>Travailler sur les fonctionnalités clés.</li>
                <li>L'équipe développement peut intégrer rapidement les fonctionnalités clés.</li>
                <li>L'équipe design peut mettre en place rapidment tout le design du site</li>
              </ul>
              <div class="table_btn">
                <a href="{{GetPath(/Mooc)}}" class="btn"><i class="fa fa-book"></i> Les tutoriels</a>
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-6 col-lg-3">
          <div class="block-pricing" style='height:600px'>
            <div class="table">
              <h4>4.Rejoignez la communautée</h4>
              <ul class="list-unstyled">
                <li>Participez aux évolutions du coeur et des applications</li>
                <li>Posez vos question sur <br/><a href='{{GetPath(/Forum)}}'>le forum</a></li>
                <li>Donnez vos avis, ou suggestions sur <br/><a href='{{GetPath(/Comunity)}}'>Le réseau social</a></li>
                <li>Suivez l'évolution prévue sur <br/><a href='{{GetPath(/Task)}}'>La roadmap</a> </li>
              </ul>
           <div class="table_btn">
                <a href="{{GetPath(/Singup)}}" class="btn"><i class="fa fa-user"></i>Je m'inscris</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>




<section id="features" class="padd-section text-center wow fadeInUp" style='display:none'>

    <div class="container">
        <div class="section-title text-center">
            <h2>Pourquoi utilisez PuzzleApp</h2>
            <p class="separator">Integer cursus bibendum augue ac cursus .</p>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class='col-md-5'>
        <h3 class='fa fa-gears fa-2x'>&nbsp;Une architecture claire </h3>
            <ul>
                <li>Découpage des fonctionnalitées par application 
                   <i class='mention fa fa-check'>&nbsp;Une application contient la gestion du front, l'administration, son modéle de données et ces connexions pour interargir avec les autres.</i>
                </li>
                <li>Des évolutions simple à mettre en ouvre
                    <i class='mention fa fa-check'>L'app blog vous convient mais il manque des fonctionnalités, surchargez la et implémentez les votres</i>
                </li>
                <li>Une gestion poussé des templates
                    <i class='mention fa fa-check'>Vous pouvez adapter les templates de base et ainsi répartir rapidement les taches entre développeur et intégrateur </i>
                </li>
                <li>Un framework php et un framework javascript tout en un
                    <i class='mention fa fa-check'>Offrez à vos utilisateurs des interface UX intuitive. Ne cherchez pas à inclure des librairies tierces complexes</i>
                </li>
                <li>Un IDE intégré pour acceler vos développement
                      <i class='mention fa fa-check'>Il écrira pour vous un partie du code, comme les applicartions, les entités, les modules ..</i>
                </li>
            </ul>    
    </div>
    <div class='col-md-1' >
        <br/><br/>
        <i class='fa fa-plus fa-3x'></i>
    </div>
    
    <div class='col-md-5'>
        <h3 class='fa fa-book fa-2x'>&nbsp;Les ressources.</h3>
        <ul>
            <li>
                <a href='{{GetPath(/Mooc/Mooc/Installer-puzzleapp)}}' >Installer et utilisez PuzzleApp</a>
                <p>Découvrez comment installer et utiliser le framework simplement</p>
            </li>
            <li>
                <a href='{{GetPath(/Mooc/Mooc/Creer-votre-premier-projet)}}' >Créer votre premier projet</a>
                <p>Dans ce tutoriel vous apprendrez à créer un projet en découvrant les bases du framework</p>
            </li>
            <li>
                <a href='{{GetPath(/Mooc/Mooc/Creer-votre-premiere-application)}}' >Créer votre premiere application</a>
                <p>Ce tutoriel plus poussé vous découvrirez comment développer une application compléte avec gestion, du front, du back, des entités ...</p>
            </li>
            <li>
                <a href='{{GetPath(/Mooc/Mooc/Le-framework-puzzleapp)}}' >Le framework PuzzleApp</a>
                <p>Cet index présente les entrailles du framework</p>
            </li>

            
        </ul>
        </ul>
    </div>

        </div>
    </div>
</section>


<div class='row'  style='display:none'>
    <div class='col-md-12 borderTop'>
        <div> 
        <h2 class='fa fa-question fa-2x borderBottom' >&nbsp;Pourquoi utiliser Puzzle App</h2>
        </div>

    </div>
</div>

<div class='row' style='display:none'>

    <div class='col-md-12 borderTop'>
        <div> 
        <h2 class='fa fa-play fa-2x borderBottom' >&nbsp;Démarrez tout de suite</h2>
        </div>
      <div class='col-md-4'>
          <h3 class='fa fa-download fa-2x'>&nbsp;1.Téléchargez la solution</h3>
          <p>Télechargez la solution et déposez la sur votre serveur.</p>
          <p>C'est simple.</p>
          <a href='{{GetPath(/Downloader/Download/puzzleApp)}}' class='btn btn-info'>Je télécharge</a>
      </div>
      <div class='col-md-4'>
          <h3 class='fa fa-desktop fa-2x'>&nbsp;2.Installez les applications.</h3>
          <p>Installer et configurez les applications dont vous avez besoin pour lancer votre site.
              Un Cms, Un Blog, une landing page pour recueillir des avis.</p>
             
           <p>  Vous lancez une startup ? L'équipe marketing peut commencer à écrire des articles, montrer la vie du site.
               
               Pendant que les développeurs développent les fonctionnalités.
          </p>
           <a href='{{GetPath(/Store)}}' class='btn btn-warning'>Je découvre le store</a>
         </div>
      <div class='col-md-4'>
          <h3 class='fa fa-group fa-2x'>&nbsp;3.Développez vos fonctionnalités.</h3>
          <p>Travailler sur les fonctionnalités clés de votre métier.
          L'équipe développement peut intégrer facilement et rapidement les fonctionnalités clés de votre application.
          En intégrant des statistiques vous saurez rapidement ce qu'il faut faire évoluer ou abandonner.
          </p>
           <a href='{{GetPath(/Mooc)}}' class='btn btn-success'>Les tutoriels</a>
      </div>
    </div>
    <div class='col-md-12'  style='border-bottom:1px solid grey; width:100%; background-color: #bebebe'>
        <div class='col-md-4'>
            <h3 class='fa fa-newspaper-o fa-2x'>&nbsp;Les news</h3>
            <ul>
                {{foreach Articles}}
                <li><a href='{{GetPath(/Blog/Article/{{element->Code->Value}})}}'>
                    {{element->Name->Value}}
                    </a>
                </li>
                {{/foreach Articles}}
            </ul>
        </div>
        <div class='col-md-4'>
            <h3 class='fa fa-flash fa-2x' >We need You</h3>
            <p>Rejoins la communauté et viens donner un peu de temps, ton génie, ton savoir faire, tes idées...</p>
            <a class='btn btn-primary' href='{{GetPath(/Singup)}}' >Rejoins nous</a><br/>
        </div>
        <div class='col-md-4'>
            <h3 class='fa fa-share fa-2x'>Suis nous</h3>
            <p>Laisse ton email</p>
            {{GetForm(/Blog/Subscribe)}}
                {{GetControl(EmailBox,Email,{Required=true,PlaceHolder=Email})}}
            
                {{GetControl(Submit,Send,{Value=Inscription,CssClass=btn btn-primary})}}
            {{CloseForm()}}
        </div>
            
    </div>
</div>
</div>
<div class='clear'></div>


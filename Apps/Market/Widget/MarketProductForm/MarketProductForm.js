var MarketProductForm = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
MarketProductForm.Init = function(callBack){
    Event.AddById("btnSaveProduct", "click", MarketProductForm.SaveProduct);
    MarketProductForm.callBack = callBack;

    //Catégorie
    Event.AddById("btnAddCategoryProduct", "click", MarketProductForm.AddCategoryProduct);
    
    //Caracteristiques
    Event.AddById("lstCaracteristique", "change", MarketProductForm.GetCaracteristiqueItem);
    Event.AddById("btnAddCaracteristiqueProduct", "click", MarketProductForm.AddCaracteristiqueProduct);

    if(Form.GetId("MarketProductForm") != "") {

        MarketProductForm.RenderCategory();
        MarketProductForm.RenderCaracteristique();
    }
};

/***
* Get The Id Of The Form
*/
MarketProductForm.GetId = function(){
    return Form.GetId("MarketProductForm");
};

/*
* Sauvegare l'itineraire
*/
MarketProductForm.SaveProduct = function(e){
   
    if(Form.IsValid("MarketProductForm"))
    {

    var data = "Class=Market&Methode=SaveProduct&App=Market";
        data +=  Form.Serialize("MarketProductForm");

        Request.Post("Ajax.php", data).then(data => {

            if(data.statut == "Error"){
                Form.RenderError("MarketProductForm", data.message);
            } else {

                Dialog.Close();

                if(MarketProductForm.callBack){
                    MarketProductForm.callBack(data);
                }
            }
        });
    }
};

/***
 * Ajoute une catégorie
 */
MarketProductForm.AddCategoryProduct = function(e){
    let productId = Form.GetId("MarketProductForm");
    let categoryId = Dom.GetById("lstCategorie").value;

    var data = "Class=Market&Methode=AddCategoryProduct&App=Market";
        data += "&productId=" + productId;
        data += "&categoryId=" + categoryId;
    
    Request.Post("Ajax.php", data).then(data => {
        Dom.GetById("hdCategory").value = data;
    
        MarketProductForm.RenderCategory();
    });
};

/***
 * Genre le html des categorie a=à partir du json
 */
MarketProductForm.RenderCategory = function(){

    let category = Dom.GetById("hdCategory").value;
        category = JSON.parse(category);

    let view = "";

    for(let i=0; i < category.length; i++){
        view += "<div id='"+ category[i].IdEntite +"' class='chips removeCategorie'>" +  category[i].CategorieName + "<i class='fa fa-trash'></i></div>";
    }

    Dom.GetById("lstProductCategory").innerHTML = view;

    Event.AddByClass("removeCategorie", "click", MarketProductForm.RemoveCategorie);
};

/***
 * Supprime une catégorie
 */
MarketProductForm.RemoveCategorie = function(e){

    let categoryProductId = e.srcElement.id;
    let container  = e.srcElement.parentNode;

    Animation.Confirm(Language.GetCode("Market.ConfirmRemoveCategoryProduct"), ()=>{

        let data = "Class=Market&Methode=RemoveCategorieProduct&App=Market";
            data += "&Id=" + categoryProductId;
       
        Request.Post("Ajax.php", data).then(data => {
            container.parentNode.removeChild(container);
        });
    });
};

/***
 * Charge les items d'une caractéristique
 */
MarketProductForm.GetCaracteristiqueItem = function(e){

    let caracteristiqueId = e.srcElement.value;
    let lstCaracteristiqueItem = Dom.GetById("lstCaracteristiqueItem");

    let data = "Class=Market&Methode=GetCaracteristiqueItem&App=Market";
    data += "&CaracteristiqueId=" + caracteristiqueId;

    Request.Post("Ajax.php", data).then(data => {
    
        lstCaracteristiqueItem.innerHTML = "";
        data = JSON.parse(data);
    
        for(let i =0; i < data.length; i++){

            lstCaracteristiqueItem.innerHTML += "<option value='"+data[i].IdEntite+"' >" + data[i].Label  +"</option>"
        }
    });
};

/***
 * Ajoute une caracteristuqie à un produit
 */
MarketProductForm.AddCaracteristiqueProduct = function(){

    let productId = Form.GetId("MarketProductForm");
    let caracteristiqueId = Dom.GetById("lstCaracteristique").value;
    let caracteristiqueItemId = Dom.GetById("lstCaracteristiqueItem").value;
   
    if(caracteristiqueItemId != ""){
        let data = "Class=Market&Methode=AddCaracteristiqueProduct&App=Market";
            data += "&productId=" + productId;
            data += "&caracteristiqueId=" + caracteristiqueId;
            data += "&itemId=" + caracteristiqueItemId;

        Request.Post("Ajax.php", data).then(data => {
            Dom.GetById("hdCaracteristique").value = data;

            MarketProductForm.RenderCaracteristique();
        });
    } else {
        Animation.Notify(Language.GetCode("Market.YouNeedSelectCaracteristiqueAndItem"))
    }
};

/***
 * Genre le html des categorie a=à partir du json
 */
MarketProductForm.RenderCaracteristique = function(){

    let caracteristique = Dom.GetById("hdCaracteristique").value;
        caracteristique = JSON.parse(caracteristique);

    let view = "";

    for(let i=0; i < caracteristique.length; i++){
        view += "<div id='"+ caracteristique[i].IdEntite +"' class='chips removeCaracteristique'>" +  caracteristique[i].CaracteristiqueName + "-" + caracteristique[i].CaracteristiqueItemName +  "<i class='fa fa-trash'></i></div>";
    }

    Dom.GetById("lstProductCaracteristique").innerHTML = view;

    Event.AddByClass("removeCaracteristique", "click", MarketProductForm.RemoveCaracteristique);
};

/***
 * Supprime une caracteristique
 */
MarketProductForm.RemoveCaracteristique = function(){
    let categoryProductId = e.srcElement.id;
    let container  = e.srcElement.parentNode;

    Animation.Confirm(Language.GetCode("Market.ConfirmRemoveCaracteristiqueProduct"), ()=>{

        let data = "Class=Market&Methode=RemoveCaracteristiqueProduct&App=Market";
            data += "&Id=" + categoryProductId;
       
        Request.Post("Ajax.php", data).then(data => {
            container.parentNode.removeChild(container);
        });
    });
};


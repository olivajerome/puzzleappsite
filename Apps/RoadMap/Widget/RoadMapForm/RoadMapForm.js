var RoadMapForm = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
RoadMapForm.Init = function(){
    Event.AddById("btnSaveRoadMap", "click", RoadMapForm.SaveRoadMap);
};

/***
* Obtient l'id de l'itin�raire courant 
*/
RoadMapForm.GetId = function(){
    return Form.GetId("itineraireForm");
};

/*
* Sauvegare l'itineraire
*/
RoadMapForm.SaveRoadMap = function(e){
   
    if(Form.IsValid("roadMapForm"))
    {

    var data = "Class=RoadMap&Methode=SaveRoadMap&App=RoadMap";
        data +=  Form.Serialize("roadMapForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.statut == "Error"){
                Form.RenderError("roadMapForm", data.message);
            } else{

                Form.SetId("roadMapForm", data.data.Id);
                
                RoadMap.LoadRoadMaps();
                Dialog.Close();
            }
        });
    }
};


CREATE TABLE IF NOT EXISTS `ShopCommand` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`UserId` INT  NOT NULL,
`Name` VARCHAR(200)  NOT NULL,
`CartId` VARCHAR(200)  NOT NULL,
`Status` INT  NOT NULL,
`Total` float  NOT NULL,
`DateCreated` DATE  NOT NULL,
`DateClotured` DATE NULL,

`Adress` VARCHAR(500)  NOT NULL,
`AdressFacturation` VARCHAR(500)  NOT NULL,

PRIMARY KEY (`Id`),
CONSTRAINT `ee_user_ShopCommand` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
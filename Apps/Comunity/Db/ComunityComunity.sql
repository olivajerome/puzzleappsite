CREATE TABLE IF NOT EXISTS `ComunityComunity` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`Name` varchar(200)  NULL ,
`Type` INT  NULL ,
`UserId` INT  NULL ,
`DateCreated` DATE  NULL ,
`Title` TEXT  NULL ,
`SubTitle` TEXT  NULL ,
`Description` TEXT  NULL ,
`Status` INT  NULL ,
`DateClotured` DATE  NULL ,
`DateLastMessage` DATETIME  NULL ,

PRIMARY KEY (`Id`),
CONSTRAINT `ee_user_ComunityComunity` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
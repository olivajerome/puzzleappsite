{{form->Open()}}
{{form->Error()}}
{{form->Success()}}

{{form->Render(Id)}}

{{form->Render(MoocId)}}

    <div>
        <label>{{GetCode(Mooc.Name)}}</label> 
        {{form->Render(Name)}}
    </div>
   
    <div>
        <label>{{GetCode(Mooc.Description)}}</label> 
        {{form->Render(Description)}}
    </div>
   
    <div>
        <label>{{GetCode(Mooc.Content)}}</label> 
        {{form->Render(Content)}}
    </div>
    
    <div class='center marginTop' >   
        {{form->Render(btnSaveLesson)}}
    </div>  

{{form->Close()}}
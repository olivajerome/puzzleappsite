<div class='col-md-10 centerBlock'>
    <h1>{{GetCode(Forum.YourMessage)}}</h1>

    {{foreach Messages}}
        <div class='messageForum' id='{{element->IdEntite}}'> 
            <div class='tools'>
                <i class='fa fa-edit editMessage' title='{{GetCode(Forum.EditMessage)}}'></i>
                <i class='fa fa-trash deleteMessage' title='{{GertCofe(Forum.RemoveMessage)}}'></i>
            </div>
            <div class='info' >
                {{element->DateCreated->Value}}

                <h3 class='title'>
                    {{element->Title->Value}}
                </h3>
            </div>
            <div class='message' contenteditable>
                {{element->Message->Value}}
            </div>
            <div class='lastReponse'>
                <div class='info'>
                       Last Reponse : 22/01/2022   numberReponse : 10 
                </div>   
                 
                {{element->GetLastReponse()}}
            </div>

        </div>
    {{/foreach Messages}}

</div>
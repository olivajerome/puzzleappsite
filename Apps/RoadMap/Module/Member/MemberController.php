<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\RoadMap\Module\Member;

use Core\Control\Button\Button;
use Core\Controller\Controller;
use Core\View\View;
use Core\View\ElementView;
use Core\Entity\Entity\Argument;

use Apps\RoadMap\Module\RoadMap\RoadMapController;

/*
 * 
 */

class MemberController extends Controller {

    /**
     * Constructeur
     */
    function _construct($core = "") {
        $this->Core = $core;
    }

    /***
     * list the Moooc Of the User 
     */
    function Index() {
  
        $roadMapController = new  RoadMapController($this->Core);
        return $roadMapController->Load(1);
    }
}

var IdeHelperForm = function(){};

/***
 * Init the form
 * @returns {undefined}
 */
IdeHelperForm.Init = function(){
    Event.AddById("btnSave", "click", IdeHelperForm.SaveHelper);
};

/***
* Get the Id of element 
*/
IdeHelperForm.GetId = function(){
    return Form.GetId("IdeHelperFormForm");
};

/*
* Save the Element
*/
IdeHelperForm.SaveHelper = function(e){
   
    if(Form.IsValid("IdeHelperFormForm"))
    {

    let data = "Class=Ide&Methode=AddHelper&App=Ide";
        data +=  Form.Serialize("IdeHelperFormForm");
        data += "&Projet=" + Ide.Projet;

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);
   
            if(data.status == "error"){
                Form.RenderError("IdeHelperFormForm", data.data.message);
            } else{

                Form.SetId("IdeHelperFormForm", data.data.Id);
                IdeElement.LoadRefreshHelper();
                Dialog.Close();
            }
        });
    }
};


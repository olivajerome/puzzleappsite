<div class="content-panel">
     <div class='alignLeft'>
        <div class="info"><i class="fa fa-info"></i>
          {{GetCode(EeApp.PluginDashboardDescription)}}
        </div>
    </div>
        <table class="table">
            <thead>
                <tr>
                  <th>{{GetCode(Name)}}</th>
                  <th>{{GetCode(Description)}}</th>
                </tr>
            </thead>
            <tbody>
                {{foreach}}
                    <tr>
                        <td>{{element->Name->Value}}</td>
                        <td>{{element->Description->Value}}</td>
                        <td><i title='{{GetCode(EeApp.EditPlugin)}}' id='{{element->IdEntite}}' class='fa fa-edit btnEditPlugin'>&nbsp;</i></td>


                    </tr>
                {{/foreach}}            
                </tbody>
            </table>
        </div>
</div>


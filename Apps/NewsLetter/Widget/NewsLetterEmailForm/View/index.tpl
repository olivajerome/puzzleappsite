<section style='height:90vh;overflow:auto' >

{{form->Open()}}
{{form->Error()}}
{{form->Success()}}

{{form->Render(Id)}}
    <div>
        <label>{{GetCode(NewsLetter.Type)}}</label> 
        {{form->Render(TypeId)}}
    </div>

    <div>
        <label>{{GetCode(NewsLetter.App)}}</label> 
        {{form->Render(AppId)}}
    </div>
    <div id='lstEmailApp'>
    
    </div>


    <div>
        <label>{{GetCode(NewsLetter.Libelle)}}</label> 
        {{form->Render(Libelle)}}
    </div>

    <div>
        <label>{{GetCode(NewsLetter.Code)}}</label> 
        {{form->Render(Code)}}
    </div>

    <div>
        <label>{{GetCode(NewsLetter.Description)}}</label> 
        {{form->Render(Description)}}
    </div>

    <div>
        <label>{{GetCode(NewsLetter.Content)}}</label> 
        <div>
            {{form->Render(Content)}}
        </div>
    </div>

    <div class='center marginTop' >   
        {{form->Render(btnSaveEmail)}}
    </div>  

{{form->Close()}}


</section>
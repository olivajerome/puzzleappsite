
<section class='container'>
    <div class='row'>
        <h1 class='borderBottom' >Les applications</h1>

        {{foreach}}
        <div class='col-md-4'>
            <div class='block height300 centerButton'>
                <h2>{{element->Name->Value}}</h2>
                <p>{{element->Description->Value}}</p>
                  <a class='btn btn-primary' href='{{GetPath(/Downloader/Download/{{element->Code->Value}})}}'>Télécharger</a>
            </div>  
        </div>
        {{/foreach}}

    </div>
</section>


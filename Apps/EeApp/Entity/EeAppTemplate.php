<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\EeApp\Entity;

use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class EeAppTemplate extends Entity {

    //Constructeur
    function __construct($core) {
        //Version
        $this->Version = "2.0.0.0";

        //Nom de la table 
        $this->Core = $core;
        $this->TableName = "EeAppTemplate";
        $this->Alias = "EeAppTemplate";

        $this->Name = new Property("Name", "Name", TEXTAREA, false, $this->Alias);
        $this->Code = new Property("Code", "Code", TEXTAREA, false, $this->Alias);
        $this->Actif = new Property("Actif", "Actif", NUMERICBOX, false, $this->Alias);
        $this->Description = new Property("Description", "Description", TEXTBOX, false, $this->Alias);

        //Creation de l entité 
        $this->Create();
    }
    
    /***
     * Check picto if actif
     */
    function GetActifPicto(){
        
        if($this->Actif->Value == 1){
            return "<i class='fa fa-check'>&nbsp;</i>";
        }
        
        return "";
    }
}

?>
<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Forum\Module\DialogAdminForum;

use Core\Control\Button\Button;
use Core\Controller\AdministratorController;
use Apps\Forum\Entity\ForumCategory;
use Apps\Forum\Entity\ForumMessage;
use Apps\Forum\Widget\CategoryForm\CategoryForm;
use Apps\Forum\Widget\MessageForm\MessageForm;

class DialogAdminForumController extends AdministratorController{

    /***
     * Ajout d'une catégorie
     */
    function AddCategorie($categoryId){
      
        $category = new ForumCategory($this->Core);
        if($categoryId != ""){
            $category->GetById($categoryId);
        }
        
        $categoryForm = new CategoryForm($this->Core);
        $categoryForm->Load($category);
        
        return $categoryForm->Render();
    }
    
    /**
     * Edit a message
     * @param type $messageId
     * @return type
     */
    function AddMessage($messageId){
      
        $message = new ForumMessage($this->Core);
        $message->GetById($messageId);
        
        $messageForm = new MessageForm($this->Core, array("Id" => $messageId));
        $messageForm->Load($message);
        
        return $messageForm->Render();
    }
}
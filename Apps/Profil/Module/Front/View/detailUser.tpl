<div class='centerContainer detailProduct' >
    <div class='row centerBlock'>

        <div class='col-md-2'>
            <h2>{{User->GetPseudo()}}</h2>
            <div class='imgProfil' style='background-image: url({{User->GetImageMini()}}) '>
            </div>
        </div>
            <div class='col-md-2'>
                <div>
                    <label>{{GetCode(Profil.Name)}} : </label>
                    {{User->Name->Value}}
                </div>
                <div>
                    <label>{{GetCode(Profil.FirstName)}} : </label>
                    {{User->FirstName->Value}}
                </div>
                <div>
                    <label>{{GetCode(Profil.BirthDate)}} : </label>
                    {{User->BirthDate->Value}}
                </div>
            </div>
        </div>    
    </div>
</div>   


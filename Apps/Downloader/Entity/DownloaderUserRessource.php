<?php 
/*
* PuzzleApp
* Webemyos
* Jérôme Oliva
* GNU Licence
*/

namespace Apps\Downloader\Entity;
use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class DownloaderUserRessource extends Entity  
{
	//Constructeur
	function __construct($core)
	{
		//Version
		$this->Version ="2.0.0.0"; 

		//Nom de la table 
		$this->Core=$core; 
		$this->TableName="DownloaderUserRessource"; 
		$this->Alias = "DownloaderUserRessource"; 

		$this->Name = new Property("Name", "Name", TEXTBOX,  false, $this->Alias); 
		$this->Code = new Property("Code", "Code", TEXTBOX,  false, $this->Alias); 
		$this->Description = new Property("Description", "Description", TEXTAREA,  false, $this->Alias); 
		$this->UserId = new Property("UserId", "UserId", NUMERICBOX,  false, $this->Alias); 

		//Repertoire de stockage des ressources    
        $this->DirectoryImage = "Data/Apps/Downloader/UserRessource/";


		//Creation de l entité 
		$this->Create(); 
	}

	function GetUrl(){
		$documents = $this->GetImages();

		return $documents[0];
	}
}
?>
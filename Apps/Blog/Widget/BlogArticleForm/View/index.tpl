{{form->Open()}}
{{form->Error()}}
{{form->Success()}}

{{form->Render(Id)}}
    <div>
        <label>{{GetCode(Blog.ArticleName)}}</label> 
        {{form->Render(Name)}}
    </div>
    <div>
        <label>{{GetCode(Blog.ArticleDescription)}}</label> 
        {{form->Render(Description)}}
    </div>
    
    <div>
        <label>{{GetCode(Blog.ArticleCategory)}}</label> 
        {{form->Render(CategoryId)}}
    </div>
    
    <div>
        <label>{{GetCode(Blog.ArticleImage)}}</label> 
        {{form->Render(Upload)}}
    </div>
    
    <div>
       {{form->RenderImage()}}
    </div>
    
    <div class='center marginTop' >   
        {{form->Render(btnSaveArticle)}}
    </div>  

{{form->Close()}}
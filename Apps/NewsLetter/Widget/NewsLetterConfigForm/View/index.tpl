{{form->Open()}}
{{form->Error()}}
{{form->Success()}}

{{form->Render(Id)}}


    <div>
        <label>{{GetCode(NewsLetter.From)}}</label> 
        {{form->Render(EmailFrom)}}
    </div>

     <div>
        <label>{{GetCode(NewsLetter.ReplyTo)}}</label> 
        {{form->Render(EmailReplyTo)}}
    </div>

    <div class='center marginTop' >   
        {{form->Render(btnSaveNewsLetterConfig)}}
    </div>  

{{form->Close()}}
CREATE TABLE IF NOT EXISTS `MessageFollow` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`ConversationId` INT  NULL ,
`UserId` INT  NULL ,
PRIMARY KEY (`Id`),
CONSTRAINT `ee_user_MessageFollow` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`),
CONSTRAINT `MessageConversation_MessageFollow` FOREIGN KEY (`ConversationId`) REFERENCES `MessageConversation`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
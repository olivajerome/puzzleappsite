<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Annonce\Entity;

use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;
use Core\Entity\User\User;

class AnnonceResponse extends Entity {

    //Constructeur
    function __construct($core) {
        //Version
        $this->Version = "2.0.0.0";

        //Nom de la table 
        $this->Core = $core;
        $this->TableName = "AnnonceResponse";
        $this->Alias = "AnnonceResponse";

        $this->AnnonceId = new Property("AnnonceId", "AnnonceId", NUMERICBOX, false, $this->Alias);
        $this->UserId = new Property("UserId", "UserId", NUMERICBOX, false, $this->Alias);
        
        $this->Response = new Property("Response", "Response", TEXTAREA, false, $this->Alias);
        $this->Status = new Property("Status", "Status", NUMERICBOX, false, $this->Alias);

        //Creation de l entité 
        $this->Create();
    }
    
    /***
     * Get The user Response
     */
    function GetUser(){
        $user = new User($this->Core);
        $user->GetById($this->UserId->Value);
        
        $view ="<div>";
        $view .= $user->RenderImageMini();
        $view .= "<br/>".$user->GetPseudo(); 
        $view .= "</div>";
        return $view;
    }
}

?>
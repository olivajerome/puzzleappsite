<?php

namespace Apps\Forms\Decorator;
use Core\Core\Core;

class FormDecorator{


    public function __construct($core){
            $this->Core = $core;
    }
    /**
     * Render The Form in Admin Mode
     */
    public function GetAdminQuestion($entity){

        $view = "<label>" . $entity->Libelle->Value."</label>";
        $view .= "<div class='tools'><i title='".$this->Core->GetCode("Form.AddQuestion")."' class='fa fa-edit btnEditQuestion'></i><i title='".$this->Core->GetCode("Form.DeleteQuestion")."' class='fa fa-trash btnDeleteQuestion'></i></div>";

        $view .= "<br/>";
        switch($entity->Type->Value){
             case 0 :
                $view .= $this->renderTypeSimple();
             break;
             case 1 :
                $view .= $this->renderTypeParagraphe();
             break;
             case 2 :
                $view .= "<i id='" . $entity->IdEntite . "' class='fa fa-plus btnAddReponse' title='".$this->Core->GetCode("Form.AddReponse")."' '></i><br/>"; 
                $view .= $this->renderTypeChoiceMultiple($entity);
             break;
             case 3 :
                $view .= "<i id='" . $entity->IdEntite . "' class='fa fa-plus btnAddReponse' title='".$this->Core->GetCode("Form.AddReponse")."' '></i><br/>"; 
                $view .= $this->renderTypeChoiceUnique($entity);
             break;
             case 4 :
                $view .= "<i id='" . $entity->IdEntite . "' class='fa fa-plus btnAddReponse' title='".$this->Core->GetCode("Form.AddReponse")."' '></i><br/>"; 
                $view .= $this->renderTypeList($entity);
             break;
        }

        return $view;
    }

    /**
     * Render type Input
     */
    public function renderTypeSimple(){
        return "<input class='form-control'  disabled />";
    }
    
    /***
     * Render Type TextArea
     */
    public function renderTypeParagraphe(){
        return "<textearea style='height:50px;' class='form-control'  disabled ></textarea>";
    }
    
    /***
     * Render Type CheckBox
     */
    public function renderTypeChoiceMultiple($entity){
       $reponses =  $entity->GetReponses();
       
       $view ="";
       foreach($reponses as $reponse){
           $view .= $this->GetAdminResponse($reponse, $entity->Type->Value);
       }
       
       return $view;
    }
    
    /**
     * Render Type Radio Button
     */
    public function renderTypeChoiceUnique($entity){
       $reponses =  $entity->GetReponses();
       
       $view ="";
       foreach($reponses as $reponse){
           $view .= $this->GetAdminResponse($reponse, $entity->Type->Value);
       }
       
       return $view;
    }
    
    /***
     * Render type Select
     */
    public function renderTypeList($entity){
       $reponses =  $entity->GetReponses();
       $list = new \Core\Control\ListBox\ListBox("");
       
       $view ="";
       foreach($reponses as $reponse){
          $list->Add($reponse->Value->Value, $reponse->Value->Value);
          $view .= $this->GetAdminResponse($reponse, $entity->Type->Value);
       }
       
       return $view;
    }
    
    /***
     * Get Admin Response
     */
    public function  GetAdminResponse($entity, $type){
        
        $view ="<div id='".$entity->IdEntite."'>";
        switch($type)
        {
            case 2 : 
                $view.= "<input type ='checkBox' disabled />" ;
       
            break;
            case 3 : 
                $view.= "<input type ='radio' disabled />" ;
                break;
        }
        
        $view .= "<label>" . $entity->Value->Value . "</label>";
        $view .= "<div class='tools'><i class='fa fa-edit btnEditReponse'></i><i class='fa fa-trash btnDeleteReponse'></i></div></div>";
 
        return $view;
    }
}
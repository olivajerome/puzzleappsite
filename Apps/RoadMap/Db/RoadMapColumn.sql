CREATE TABLE IF NOT EXISTS `RoadMapColumn` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`RoadMapId` INT  NULL ,
`Name` VARCHAR(200)  NULL ,
`Code` VARCHAR(200)  NULL ,
`Description` TEXT  NULL ,
PRIMARY KEY (`Id`),
CONSTRAINT `RoadMapRoadMap_RoadMapColumn` FOREIGN KEY (`RoadMapId`) REFERENCES `RoadMapRoadMap`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Core\Router;

/**
 * Description of Router
 *
 * @author jerome
 */
class Router 
{
    /*
     * Extract the route
     */
    public static function ExtractRoute()
    {
        //Get The Complete Uri
        $url = explode("/", $_SERVER["REQUEST_URI"]);
        
        if($_SERVER["HTTP_HOST"] == "localhost")
        {
            $route = new Route($url[3], isset($url[4])?$url[4]:"", isset($url[5])?$url[5]:"");
        }
        else
        {
            $route = new Route(self::cleanUrl($url[1]), self::cleanUrl($url[2]),  self::cleanUrl($url[3]));
        }
        
        return $route;
    }
    
    
    /**
     * Néttoie 'lurtl
     **/
    public static function cleanUrl($url){
         
         //On recherche les ?
         $sign = strpos($url, "?");
         if($sign > -1){
            return substr($url , 0, $sign); 
         }  else {
            return $url;
         } 
    
    }
}

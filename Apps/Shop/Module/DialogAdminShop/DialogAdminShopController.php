<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Shop\Module\DialogAdminShop;

use Core\Control\Button\Button;
use Core\Controller\Controller;
use Apps\Shop\Entity\ShopCategory;
use Apps\Shop\Entity\ShopCaracteristique;
use Apps\Shop\Entity\ShopProduct;
use Apps\Shop\Entity\ShopCommand;
use Apps\Shop\Widget\CategoryForm\CategoryForm;
use Apps\Shop\Widget\CaracteristiqueForm\CaracteristiqueForm;
use Apps\Shop\Widget\ProductForm\ProductForm;
use Apps\Shop\Widget\DetailCommandForm\DetailCommandForm;

class DialogAdminShopController extends Controller{

    /***
     * Ajout d'une catégorie
     */
    function AddCategorie($categoryId){
      
        $category = new ShopCategory($this->Core);
        if($categoryId != ""){
            $category->GetById($categoryId);
        }
        
        $categoryForm = new CategoryForm($this->Core);
        $categoryForm->Load($category);
        $categoryForm->form->LoadImage($category);

        
        return $categoryForm->Render();
    }

    /***
     * Ajout d'une caractéristique
     */
    function AddCaracteristique($caracteristiqueId){
       
        $caracteristique = new ShopCaracteristique($this->Core);
 
        if($caracteristiqueId != ""){
            $caracteristique->GetById($caracteristiqueId);
        }
    
       $data["Id"] = $caracteristiqueId;
       $caracteristiqueForm = new CaracteristiqueForm($this->Core, $data);
       $caracteristiqueForm->Load($caracteristique);
     
       return $caracteristiqueForm->Render();
    }

    /**
     * Ajout d'un produit
     */
    function AddProduct($productId){

        $product = new ShopProduct($this->Core);
    
        if($productId != ""){
            $product->GetById($productId);
        }
        
        $productForm = new ProductForm($this->Core, $product);
        $productForm->Load($product);
        $productForm->form->LoadImage($product);

        return $productForm->Render();
    }

    /***
     * Edition d'une commande
     */
    function EditCommand($commandId){
        $command = new ShopCommand($this->Core);
        $command->GetById($commandId);
        
        $commandForm = new DetailCommandForm($this->Core, $command);
        $commandForm->Load($command);
        
        return $commandForm->Render();
    }
}
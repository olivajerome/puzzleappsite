<?php

namespace Plugins\RatingPlugin;

use Core\View\View;
use Core\View\ElementView;
use Core\Core\Response;
use Core\Control\NoteBox\NoteBox;
use Core\Utility\Date\Date;

use Plugins\RatingPlugin\Entity\RatingNote;

class RatingPlugin {

    public $Author = 'Webemyos';
    public $Version = '1.0.0.0';

    function __construct($core) {
        $this->Core = $core;
    }

    function GetType() {
        return "Rate";
    }

     /*     * *
     * Render the plugin
     */

     function Render($entityName, $entityId) {

        if($entityName != "" && $entityId != ""){
        
            $view = new View(__DIR__ . "/View/index.tpl", $this->Core);
            $view->AddElement(new ElementView("EntityName", $entityName));
            $view->AddElement(new ElementView("EntityId", $entityId));

            $note = self::GetByEntity($this->Core, $entityName, $entityId, $this->Core->User->IdEntite);

            $noteBox = new NoteBox("noteBox", count($note) > 0 ? $note[0]->Note->Value : '');
            $view->AddElement($noteBox);

            return $view->Render();
        } 
        
        return "";
    }

    /*     * *
     * Render the Js in the VotePlugin
     */

    function GetRegisterPlugin() {
        return '<script>document.addEventListener("DOMContentLoaded", function() {  
                Plugin.Register("RatingPlugin", "RatingPlugin"); }) ;</script>';
    }

    /**
     * Add A note to  element
     */
    function Note($params){
        
        if (!$this->Core->IsConnected()) {
            return Response::Error(array("Message" => "NotConnect"));
        }

        $RatingNote = new RatingNote($this->Core);
        $userVote = $RatingNote->Find("UserId= " . $this->Core->User->IdEntite . " and EntityName = '" . $params->EntityName . "' and EntityId=" . $params->EntityId);

        if (count($userVote) > 0) {
            $RatingNote = $userVote[0];
        } else {
            $RatingNote = new RatingNote($this->Core);
            $RatingNote->UserId->Value = $this->Core->User->IdEntite;
            $RatingNote->EntityName->Value = $params->EntityName;
            $RatingNote->EntityId->Value = $params->EntityId;
        }

        $RatingNote->UserName->Value = $this->Core->User->GetPseudo();
        $RatingNote->DateCreated->Value = Date::Now();

        $RatingNote->Note->Value = $params->Note;
        $RatingNote->Save();

        return Response::Success(array());
    }

    /***
     * Get Note of a entity
     */
    public function GetByEntity($core, $entityName, $entityId, $userId = ""){
        $note = new RatingNote($core);
        
        if($userId != ""){
            return $note->Find("EntityName ='$entityName' AND EntityId ='$entityId' and UserId = ".$userId." ORDER By Id desc");
        } else{
            return $note->Find("EntityName ='$entityName' AND EntityId ='$entityId' ORDER By Id desc");
        }
    }
}
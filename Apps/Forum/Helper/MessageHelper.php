<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Forum\Helper;

use Core\Entity\Entity\Argument;
use Core\Utility\Date\Date;

use Apps\Forum\Entity\ForumMessage;
use Apps\Forum\Entity\ForumReponse;
use Apps\Forum\Widget\MessageForm\MessageForm;



class MessageHelper
{
    /***
     * Obtient un message et ses réponses
     */
    public static function GetMessage($core, $messageId){
        $message = new ForumMessage($core);
        $message->GetById($messageId);
        
        $reponse = new ForumReponse($core);
        $reponses = self::GetReponse($core, $messageId);
        
        return array("message" => $message->ToArray(),
                     "reponses" => $reponse->ToAllArray($reponses)
                );
        
    }
    
    /**
     * Obtient tous les messages d'une categories
     * @param type $core
     * @param type $categoryId
     */
    public static function GetByCategory($core, $categoryId)
    {
        $messages = new ForumMessage($core);
        $messages->AddArgument(new Argument("Apps\Forum\Entity\ForumMessage", "CategoryId",EQUAL, $categoryId ));
        $messages->AddOrder("Id desc");
        
        return $messages->GetByArg();
    }
    
    /**
     * Enregistre une discussion
     * @param type $core
     * @param type $title
     * @param type $message
     */
    public static function Save($core, $categoryId, $title, $text)
    {
        $message = new ForumMessage($core);
        $message->Title->Value = $title;
        $message->Message->Value = $text;
        $message->UserId->Value = $core->User->IdEntite;
        $message->CategoryId->Value = $categoryId;
        $message->DateCreated->Value = Date::Now();
        
        $message->Save();
    }
    
     /**
     * Enregistre une réponse
     * @param type $core
     * @param type $title
     * @param type $message
     */
    public static function SaveReponse($core, $sujetId, $title, $text)
    {
        $message = new ForumReponse($core);
        $message->Message->Value = $text;
        $message->UserId->Value = $core->User->IdEntite;
        $message->MessageId->Value = $sujetId;
        $message->DateCreated->Value = Date::Now();
        
        $message->Save();
    }
    
    /**
     * Obtient le nombre de reponse d'un message
     */
    public static function GetNumberReponse($core, $messageId)
    {
        return count(self::GetReponse($core, $messageId));
    }
    
    /**
     * Obtient les réponses d'un message
     * @param type $core
     * @param type $messageId
     */
    public static function GetReponse($core, $messageId)
    {
        $reponses = new ForumReponse($core);
        $reponses->AddArgument(new Argument("Apps\Forum\Entity\ForumReponse", "MessageId", EQUAL, $messageId ));
        
        $reponses->AddOrder("Id");
        
        return $reponses->GetByArg();
    }
    
    /**
     * Obtient le dernier message de ma catégorie
     * @param type $core
     * @param type $categoryId
     */
    public static function GetLastMessage($core, $categoryId)
    {
        $message = new ForumMessage($core);
        $message->AddArgument(new Argument("Apps\Forum\Entity\ForumMessage", "CategoryId", EQUAL, $categoryId));
        $message->AddOrder("Id desc");
        
        $messages = $message->GetByArg();
        
        if(count($messages) > 0)
        {
            return $messages[0];
        }
        else
        {
            return null;
        }
    }
    
    /**
     * Obtient la derniere réponse d'un message
     * @param type $core
     * @param type $messageId
     */
    public static function GetLastReponse($core, $messageId)
    {
        $reponse = new ForumReponse($core);
        $reponse->AddArgument(new Argument("Apps\Forum\Entity\ForumReponse", "MessageId", EQUAL, $messageId));
    
        $reponse->AddOrder("Id");
        
        $reponses = $reponse->GetByArg();
        
        if(count($reponses) > 0)
        {
            return $reponses[0];
        }
        else
        {
            return null;
        }
    }
    
    /***
     * update a message
     */
    public static function UpdateMessage($core, $data){

        $messageForm = new MessageForm($core, $data);

        if($messageForm->Validate($data)){

            $message = new ForumMessage($core);
            
            $message->GetById($data["Id"]);
           
            $messageForm->Populate($message);
            $message->Save();

            return true;
        }

        return false;
    }
    
    /***
     * Delete a message
     */
    public static function DeleteMessage($core, $messageId){
        
        $reponse = new ForumReponse($core);
        $responses = $reponse->Find("MessageId=" .$messageId);
        
        foreach($responses as $response){
            $response->Delete();
        }
        
        $message = new ForumMessage($core);
        $message->GetById($messageId);
        $message->Delete();
        
        return true;
    }

    /***
     * Delete a reponse
     */
    public static function DeleteReponse($core, $reponseId){
        $reponse = new ForumReponse($core);
        $reponse->GetById($reponseId);
        $reponse->Delete();
    }

    /**
     * Update a reponse
     */
    public static function UpdateReponse($core, $data){
      
        $reponse = new ForumReponse($core);
        $reponse->GetById($data['reponseId']);
        $reponse->Message->Value = $data["reponse"];
        $reponse->Save();
    }

    /***
     * Get the message of User
     */
    public static function GetByUser($core, $userId){

        $message = new ForumMessage($core);

        return $message->Find("UserId=" . $userId . " ORDER BY DateCreated desc");
    }
    
    /***
     * Obtient les derniers messages poste
     */
    public static function GetLastMessages($core, $limit = 5){
        
        $message = new ForumMessage($core);
        $messages = $message->Find(" Id > 0 order by id desc limit 0," .$limit);
                
        return $messages;      
    }
 }



var IdeModuleForm = function(){};

/***
 * Init the form
 * @returns {undefined}
 */
IdeModuleForm.Init = function(){
    Event.AddById("btnSave", "click", IdeModuleForm.SaveElement);
};

/***
* Get the Id of element 
*/
IdeModuleForm.GetId = function(){
    return Form.GetId("IdeModuleFormForm");
};

/*
* Save the Element
*/
IdeModuleForm.SaveElement = function(e){
   
    if(Form.IsValid("IdeModuleFormForm"))
    {

    let data = "Class=Ide&Methode=AddModule&App=Ide";
        data +=  Form.Serialize("IdeModuleFormForm");
        data += "&Projet=" + Ide.Projet;

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.status == "error"){
                Form.RenderError("IdeModuleFormForm", data.data.message);
            } else{
                IdeElement.LoadRefreshModule();
                Dialog.Close();
            }
        });
    }
};


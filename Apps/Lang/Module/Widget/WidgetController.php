<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Lang\Module\Widget;

use Core\Control\Button\Button;
use Core\Controller\Controller;
use Core\View\View;
use Core\View\ElementView;
use Apps\Blog\Helper\BlogHelper;
use Apps\Blog\Helper\CategoryHelper;

/*
 * 
 */

class WidgetController extends Controller {

    /**
     * Constructeur
     */
    function _construct($core = "") {
        $this->Core = $core;
    }

    /**
     * Creation
     */
    function Create() {
        
    }

    /**
     * Initialisation
     */
    function Init() {
        
    }

    /**
     * Affichage du module
     */
    function Show($type, $params) {
        switch ($type) {
            case "AdminDashboard" :
                return $this->AdminDashboard($params);
                break;
        }
    }

    /*
     * Get the home page
     */

    function Index() {
        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);
        return $view->Render();
    }

    /*     * *
     * Widget de base
     */

    function AdminDashboard($params) {
        $view = new View(__DIR__ . "/View/adminDashboard.tpl", $this->Core);
        return $view->Render();
    }

    /* action */
}

?>
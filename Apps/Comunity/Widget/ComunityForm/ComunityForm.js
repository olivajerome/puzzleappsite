var ComunityForm = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
ComunityForm.Init = function(){
    Event.AddById("btnSaveComunity", "click", ComunityForm.SaveElement);
};

/***
* Obtient l'id de l'itin�raire courant 
*/
ComunityForm.GetId = function(){
    return Form.GetId("ComunityFormForm");
};

/*
* Sauvegare l'itineraire
*/
ComunityForm.SaveElement = function(e){
   
    if(Form.IsValid("ComunityFormForm"))
    {

    let data = "Class=Comunity&Methode=SaveComunity&App=Comunity";
        data +=  Form.Serialize("ComunityFormForm");

        Request.Post("Ajax.php", data).then(data => {
            
            Dialog.Close();
            Dashboard.StartApp("", "Comunity", "");
        });
    }
};


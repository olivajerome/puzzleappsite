<?php 

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\EeApp\Entity;

use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

use Core\Control\Image\Image;


class EeAppApp extends Entity  
{
    protected $Category;

    //Constructeur
    function __construct($core)
    {
        //Version
        $this->Version ="2.0.0.0"; 

        //Nom de la table 
        $this->Core=$core; 
        $this->TableName="EeAppApp"; 
        $this->Alias = "EeAppApp"; 

        $this->CategoryId = new Property("CategoryId", "CategoryId", NUMERICBOX,  true, $this->Alias); 
        $this->Category = new EntityProperty("Apps\EeApp\Entity\EeAppCategory", "CategoryId");

        $this->Name = new Property("Name", "Name", TEXTBOX,  true, $this->Alias); 
        $this->Description = new Property("Description", "Description", TEXTAREA,  false, $this->Alias); 
        $this->Version = new Property("Version", "Version", TEXTAREA,  false, $this->Alias); 

        $this->Actif = new Property("Actif", "Actif", CHECKBOX,  false, $this->Alias); 
        $this->Widget = new Property("Widget", "Widget", CHECKBOX,  false, $this->Alias); 
        $this->Member = new Property("Member", "Member", CHECKBOX,  false, $this->Alias); 

        //Partage entre application 
        $this->AddSharedProperty();

        //Creation de l entité 
        $this->Create(); 
    }

    /*
     * Retourne l'image
     */
    function GetImage()
    {
       $fileName = "../Apps/".$this->Name->Value."/Images/logo.png";

        if(!file_exists($fileName))
        {
            $fileName = "../Images/noimages.png";
        }

        $image = new Image($fileName);
        $image->AddStyle("width", "35px");
        $image->Title = "Presentation";

        return $image->Show();
    }
    
    /***
     * Obtient la version d'une application
     */
    function GetVersion(){
        $appName = $this->Name->Value;
        
        $path = "\\Apps\\".$appName ."\\".$appName;
        $app = new $path($this->Core);
    
        return $app->Version;
    }
}
?>
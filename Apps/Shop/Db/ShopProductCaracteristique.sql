CREATE TABLE IF NOT EXISTS `ShopProductCaracteristique` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`ProductId` INT  NULL ,
`CaracteristiqueId` INT  NULL ,
`ItemId` INT  NULL ,
PRIMARY KEY (`Id`),
CONSTRAINT `ShopProduct_ShopProductCaracteristique` FOREIGN KEY (`ProductId`) REFERENCES `ShopProduct`(`Id`),
CONSTRAINT `ShopCaracteristique_ShopProductCaracteristique` FOREIGN KEY (`CaracteristiqueId`) REFERENCES `ShopCaracteristique`(`Id`),
CONSTRAINT `ShopCaracteristiqueItem_ShopProductCaracteristique` FOREIGN KEY (`ItemId`) REFERENCES `ShopCaracteristiqueItem`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
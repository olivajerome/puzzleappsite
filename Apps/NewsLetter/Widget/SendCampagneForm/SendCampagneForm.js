var SendCampagneForm = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
SendCampagneForm.Init = function(){
    Event.AddById("btnSend", "click", SendCampagneForm.SendCampagne);
};

/***
* Obtient l'id de l'itin�raire courant 
*/
SendCampagneForm.GetId = function(){
    return Form.GetId("SendCampagneFormForm");
};

/*
* Send Campage to the list of email
*/
SendCampagneForm.SendCampagne = function(e){
   
    if(Form.IsValid("SendCampagneFormForm"))
    {

    var data = "Class=NewsLetter&Methode=SendCampagne&App=NewsLetter";
        data +=  Form.Serialize("SendCampagneFormForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.statut == "Error"){
                Form.RenderError("SendCampagneFormForm", data.message);
            } else{

                Form.SetId("SendCampagneFormForm", data.data.Id);
            }
        });
    }
};


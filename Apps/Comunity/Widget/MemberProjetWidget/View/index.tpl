<div>
<div id='lstMembre'>
    <h3>Les membres de ce projet</h3>
    
    <ul>
    {{foreach Users}}
        <li>
            <div class="avatarmini-img avatarmini" style="cursor: pointer;"> 
               <img src="{{element->User->Value->GetImageMini()}}" alt="images"> 
            </div>
            <b>{{element->User->Value->GetPseudo()}}</b>
            
            {{element->GetSendMessage()}}
            
            
            
        </li>
    
    {{/foreach Users}}
    </ul>
</div>
</div>    
    
<script>
    MemberProjetWidget.Init();
</script>
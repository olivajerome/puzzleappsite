<?php

namespace Apps\NewsLetter\Helper;

use Core\Utility\Date\Date;
use Apps\NewsLetter\Widget\NewsLetterCampagneForm\NewsLetterCampagneForm;

use Apps\NewsLetter\Entity\NewsLetterEmailCampagne;
use Apps\NewsLetter\Entity\NewsLetterListeEmail;
use Apps\NewsLetter\Entity\NewsLetterEmailCampagneSend;


class CampagneHelper {

    /*     * *
     * Save Campagne
     */

     public static function SaveCampagne($core, $data) {

        $campagneForm = new NewsLetterCampagneForm($core, $data);

        if ($campagneForm->Validate($data)) {

            $newsLetterCampagne = new NewsLetterEmailCampagne($core);

            if ($data["Id"] != "") {
                $newsLetterCampagne->GetById($data["Id"]);
            } else{
                $newsLetterCampagne->DateCreated->Value = Date::Now();
            }

            $campagneForm->Populate($newsLetterCampagne);
            $newsLetterCampagne->Save();

            return $campagneForm->Success(array("Message" => $core->GetCode("NewsLetter.EmailSaved"), "Id" => $core->Db->GetInsertedId()));
        } else {

            return $campagneForm->Error();
        }
    }

    /***
     * Send a campagne to a liste of Email
     */
    public static function SendCampagne($core, $data){

        $campagneId = $data["CampagneId"];
        $listId = $data["ListId"];

        $campagne = new NewsLetterEmailCampagne($core);
        $campagne->GetById($data["CampagneId"]);

        $listEmail = new NewsLetterListeEmail($core);
        $listEmail->GetById($data["ListId"]);

        echo $campagne->Libelle->Value . " => ".  $listEmail->Emails->Value;
   
   
        $campagneSend = new NewsLetterEmailCampagneSend($core);
        $campagneSend->CampagneId->Value = $campagne->IdEntite;
        $campagneSend->Emails->Value = $listEmail->Emails->Value;
        $campagneSend->DateSended->Value = Date::Now();
        $campagneSend->Save();
    } 
}
<?php 

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Notify\Entity;

use Core\Entity\Entity\Entity;
use Core\Entity\Entity\EntityProperty;
use Core\Entity\Entity\Property;


class NotifyNotify extends Entity  
{
    //Entity Property
    protected $User;

    //Constructeur
    function __construct($core)
    {
        //Version
        $this->Version ="2.0.0.0"; 

        //Nom de la table 
        $this->Core=$core; 
        $this->TableName="NotifyNotify"; 
        $this->Alias = "NotifyNotify"; 

        $this->UserId = new Property("UserId", "UserId", NUMERICBOX,  false, $this->Alias); 
        $this->User = new EntityProperty("User", "UserId"); 

        $this->Code = new Property("Code", "Code", TEXTBOX,  true, $this->Alias); 
        $this->Description = new Property("Description", "Description", TEXTAREA,  false, $this->Alias); 
        $this->DateCreate = new Property("DateCreate", "DateCreate", DATEBOX,  true, $this->Alias); 
        $this->DestinataireId = new Property("DestinataireId", "DestinataireId", NUMERICBOX,  false, $this->Alias); 
        $this->View = new Property("View", "View", NUMERICBOX,  false, $this->Alias); 

        //Partage entre application 
        $this->AddSharedProperty();

        //Creation de l entité 
        $this->Create(); 
    }
    
    /****
     * Get user whos send the notify
     */
    function GetUser(){
        $user = new \Core\Entity\User\User($this->Core);
        $user->GetById($this->UserId->Value);
        
        return   $user->GetPseudo();
    }
    
    /***
     * Get the notify
     */
    function GetMessage(){
        $app = \Core\Dashboard\DashBoardManager::GetApp($this->AppName->Value, $this->Core);
        return $app->GetDetailNotify($this);
    }
}
?>
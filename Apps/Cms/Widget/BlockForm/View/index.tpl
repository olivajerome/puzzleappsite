  {{form->Open()}}
  {{form->Error()}}
  {{form->Success()}}

  {{form->Render(Id)}}
  {{form->Render(ParentId)}}
 
  <div class='form-group'>
      <label>{{GetCode(Cms.BlockType)}}</label>
      {{form->Render(TypeId)}}
  </div>  
  
  <div class='form-group'>
      <div class='col-md-6'>
        <label>{{GetCode(Cms.Size)}}</label>
        {{form->Render(Size)}}
      </div>
      <div class='col-md-6'>
      
        <label>{{GetCode(Cms.Position)}}</label>
      {{form->Render(Position)}}
      </div>
  </div>  
  
  <div class='form-group'>
      <label>{{GetCode(Cms.CssClass)}}</label>
      {{form->Render(CssClass)}}
  </div>  
  
  <div class='form-group'>
      <label>{{GetCode(Cms.Style)}}</label>
      {{form->Render(Style)}}
  </div>
  
  
   <div class='form-group' id='additionnalData'>
   </div>
   
   <div class='center form-group ' >   
       {{form->Render(btnSaveBlock)}}
   </div>  
  {{form->Close()}}

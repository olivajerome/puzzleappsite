CREATE TABLE IF NOT EXISTS `MessageConversation` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`Type` INT  NULL ,
`UserId` INT  NULL ,
`DateCreated` DATE  NULL ,
`Title` TEXT  NULL ,
`SubTitle` TEXT  NULL ,
`Description` TEXT  NULL ,
`Status` INT  NULL ,
`DateClotured` DATE  NULL ,
`DateLastMessage` DATETIME  NULL ,


PRIMARY KEY (`Id`),
CONSTRAINT `ee_user_MessageConversation` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
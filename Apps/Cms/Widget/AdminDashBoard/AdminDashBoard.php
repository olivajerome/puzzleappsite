<?php

namespace Apps\Cms\Widget\AdminDashBoard;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;
use Core\Entity\Entity\Argument;
use Apps\Cms\Entity\CmsPage;

class AdminDashBoard {
    
    public function __construct($core) {
        $this->Core = $core;
    }
    
    /***
     * Render the widget
     */
    public function Render(){
        $view = new View(__DIR__ . "/View/adminDashboard.tpl", $this->Core);
        
        $pages = new CmsPage($this->Core);
        
        $view->AddElement(new ElementView("Pages",$pages->Find("CmsId = 1")));
        
        return $view->Render();
    }
}

<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Shop\Entity;

use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class ShopCategory extends Entity {

    //Constructeur
    function __construct($core) {
        //Version
        $this->Version = "2.0.0.0";

        //Nom de la table 
        $this->Core = $core;
        $this->TableName = "ShopCategory";
        $this->Alias = "ShopCategory";

        $this->ShopId = new Property("ShopId", "ShopId", NUMERICBOX, true, $this->Alias);
        $this->Name = new Property("Name", "Name", TEXTAREA, true, $this->Alias);
        $this->Code = new Property("Code", "Code", TEXTAREA, true, $this->Alias);
        $this->Description = new Property("Description", "Description", TEXTAREA, true, $this->Alias);

        //Repertoire de stockage des images    
        $this->DirectoryImage = "Data/Apps/Shop/Category/";

        //Creation de l entité 
        $this->Create();
    }
    
     /***
     * Get Image Path
     */
    function GetImagePath(){
        
        if(file_exists($this->DirectoryImage . $this->IdEntite ."/full.png")){
             return $this->DirectoryImage . $this->IdEntite ."/full.png" ;
        } 
        
        return $this->Core->GetPath("/images/nophoto.png");
    }
}

?>
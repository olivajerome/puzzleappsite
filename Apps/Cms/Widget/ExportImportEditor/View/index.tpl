<div class='info'><i class='fa fa-info'></i>
    {{GetCode(Cms.DescriptionImportExport)}}
</div>
<div class="marginTop">
    <button id='btnExportPage' class='btn btn-primary' >{{GetCode(Cms.ExportPages)}}</button>
    <button id='btnImportPage' class='btn btn-primary' >{{GetCode(Cms.ImportPages)}}</button>
</div>

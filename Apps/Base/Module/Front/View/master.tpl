<html>
   <head>
    <title>{{Title}}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="{{Description}}">
    <meta name="author" content="Webemyos.com">
    <link rel="icon" href="Images/favicon-tnp.ico">
    <link href="{{GetPath(/asset/bootstrap.css)}}" rel="stylesheet" >
    <!--  link href="{{GetPath(/asset/global.css)}}" rel="stylesheet" -->
    <link href="{{GetPath(/asset/boostrap/css/bootstrap.min.css)}}" rel="stylesheet">
    <link href="{{GetPath(/asset/fontawersome/css/all.css)}}" rel="stylesheet">
    <!-- Main Stylesheet File -->
    <link href="{{GetPath(/css/style.css)}}" rel="stylesheet">
    <link href="{{GetPath(/css/ui.css)}}" rel="stylesheet">
    <link href="{{GetPath(/style.php?apps=all)}}" rel="stylesheet">
    <script src='{{GetPath(/script.php)}}' ></script>
    <script src='{{GetPath(/script.php?apps=all)}}' ></script>
  
    
  </head>
  
    <body>
      <header id="header" class="header header-hide scroll-header">
        <div class='row'>
          {{GetWidget(Cms,BlockContent,Header)}}
        </div>
      </header>


      <header style='display:none' id="header" class="header header-hide scroll-header">
        <div class="">
          <nav id="nav-menu-container">
            <ul class="nav-menu">
              <li><a  class="last-item-menu" href="{{GetPath(/index)}}">{{GetCode(Base.Home)}}</a></li>
                    <li><a  class="last-item-menu" href="{{GetPath(/Contact)}}">{{GetCode(Base.Contact)}}</a></li>
                    <li>
                        {{if connected == true}}
                            <a class="last-item-menu"  href='{{GetPath(/Membre)}}' >{{GetCode(Base.Member)}}</a>
                        {{/if connected == true}}
                        
                        {{if connected == false}}
                            <a class="last-item-menu" href='{{GetPath(/Login)}}' >{{GetCode(Base.Login)}}</a>
                        {{/if connected == false}}
                    </li>
            </ul>
          </nav><!-- #nav-menu-container -->
        
        <div id="logo" class="pull-left">
            <h1><a href="#body" class="scrollto"><span>Puzzle</span>App</a></h1>
            <!-- Uncomment below if you prefer to use an image logo -->
            <!-- <a href="#body"><img src="img/logo.png" alt="" title="" /></a>-->
          </div>

        </div>
      </header>
   <div class="navbar-wrapper" style='display:none'>
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
          <div class="navbar-header">
            <a class="navbar-brand" href="index.php" alt="PuzzleApp" title="Plus qu'un framework, une solution compléte pour tous les types de site.">
               <h2>PuzzleApp</h2>
            </a>
             
          </div>
          <div id="navbar" class="navbar-collapse collapse">

            <ul class="nav navbar-nav navbar-right">
                <li> <div id='tdApp' class='span8'>
                        </div>  </li>
                <li><a  class="last-item-menu" href="{{GetPath(/index)}}">{{GetCode(Base.Home)}}</a></li>
               
                <li>
                    {{if connected == true}}
                        <a class="last-item-menu"  href='{{GetPath(/Membre)}}'>{{GetCode(Base.Membre)}}</a>
                    {{/if connected == true}}
                    
                     {{if connected == false}}
                        <a class="last-item-menu" href='{{GetPath(/Login)}}' >{{GetCode(Base.Login)}}</a>
                    {{/if connected == false}}
                    
                    
                </li>
            </ul>
          </div>
        </div>
      </nav>
    </div>

    <!-- Header Home
    ================================================== -->

    <div class="content mainContent">
      <div id="dvCente">
        <div id="appRunLePupitreDigital" class="App row-fluid">
          <div id='appCenter'>
                  {{content}}
          </div>
        </div>
      </div>
    </div>

    <!-- Footer
    ================================================== -->
    <footer class='footer'>
      <div class="container">
        <div class='row'>
          {{GetWidget(Cms,BlockContent,Footer)}}
        </div>
      </div>
    </footer>

</html>
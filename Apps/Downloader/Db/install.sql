CREATE TABLE IF NOT EXISTS `DownloaderRessource` (
`Id` int(11) NOT NULL AUTO_INCREMENT,
`UserId` INT  NULL ,
`Name` TEXT  NULL ,
`Code` TEXT  NULL ,
`Category` INT  NULL ,
`Description` TEXT  NULL ,
`LongDescription` TEXT  NULL ,
`Url` TEXT  NULL ,
`AppName` VARCHAR(200)  NULL ,
`AppId` INT  NULL ,
`EntityName` VARCHAR(200)  NULL ,
`EntityId` INT  NULL ,
PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`;

CREATE TABLE IF NOT EXISTS `DownloaderRessourceContact` (
`Id` int(11) NOT NULL AUTO_INCREMENT,
`RessourceId` INT  NULL ,
`UserId` INT  NULL ,
`Email` VARCHAR(200)  NULL ,
PRIMARY KEY (`Id`),
CONSTRAINT `EeDownloaderRessource_EeDownloaderRessourceContact` FOREIGN KEY (`RessourceId`) REFERENCES `DownloaderRessource`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`;

Update DownloaderRessource Set Url ="Data/Apps/Downloader/1/Base.zip" where Name = "Base";
Update DownloaderRessource Set Url ="Data/Apps/Downloader/1/Lang.zip" where Name = "Lang";

CREATE TABLE IF NOT EXISTS `DownloaderRessource` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`Name` VARCHAR(200)  NOT NULL,
`Code` VARCHAR(200)  NOT NULL,
`Description` TEXT  NOT NULL,
`UserId` INT  NOT NULL,
PRIMARY KEY (`Id`),
CONSTRAINT `ee_user_DownloaderRessource` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `DownloaderUserRessource` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`Name` VARCHAR(200)  NOT NULL,
`Code` VARCHAR(200)  NOT NULL,
`Description` TEXT  NOT NULL,
`UserId` INT  NOT NULL,
PRIMARY KEY (`Id`),
CONSTRAINT `ee_user_DownloaderUserRessource` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
var ContactForm = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
ContactForm.Init = function(){
    Event.AddById("btnSend", "click", ContactForm.SendMessage);
};

/***
* Obtient l'id de l'itin�raire courant 
*/
ContactForm.GetId = function(){
    return Form.GetId("ContactFormForm");
};

/*
* Envoie le message
*/
ContactForm.SendMessage = function(e){
   
    if(Form.IsValid("ContactFormForm"))
    {
      console.log("TODO");  
        
      return ;  

    let data = "Class=ContactForm&Methode=SaveElement&App=ContactForm";
        data +=  Form.Serialize("ContactFormForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.statut == "Error"){
                Form.RenderError("ContactFormForm", data.message);
            } else{

                Form.SetId("ContactFormForm", data.data.Id);
            }
        });
    }
};


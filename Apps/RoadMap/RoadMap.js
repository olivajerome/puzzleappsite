var RoadMap = function() {};

	/*
	* Chargement de l'application
	*/
	RoadMap.Load = function(parameter)
	{
		this.LoadEvent();
	};

	/*
	* Chargement des �venements
	*/
	RoadMap.LoadEvent = function()
	{
            Event.AddById("btnCreateRoadMap","click", RoadMap.ShowAddRoadMap);
            Event.AddByClass("btnRoadMap", "click", RoadMap.LoadRoadMap) ;
  	};

   /*
	* Execute une fonction
	*/
	RoadMap.Execute = function(e)
	{
		//Appel de la fonction
		Dashboard.Execute(this, e, "RoadMap");
		return false;
	};

	/*
	*	Affichage de commentaire
	*/
	RoadMap.Comment = function()
	{
		Dashboard.Comment("RoadMap", "1");
	};

	/*
	*	Affichage de a propos
	*/
	RoadMap.About = function()
	{
		Dashboard.About("RoadMap");
	};

	/*
	*	Affichage de l'aide
	*/
	RoadMap.Help = function()
	{
		Dashboard.OpenBrowser("RoadMap","{$BaseUrl}/Help-App-RoadMap.html");
	};

   /*
	*	Affichage de report de bug
	*/
	RoadMap.ReportBug = function()
	{
		Dashboard.ReportBug("RoadMap");
	};

	/*
	* Fermeture
	*/
	RoadMap.Quit = function()
	{
		Dashboard.CloseApp("","RoadMap");
	};
    
    /***
     *  Dialog d'ajout de RoadMap 
     **/
    RoadMap.ShowAddRoadMap = function(){
           
          Dialog.open('', {"title": Dashboard.GetCode("RoadMap.AddRoadMap"),
          "app": "RoadMap",
          "class": "DialogRoadMap",
          "method": "ShowAddRoadMap",
          "type": "left",
          "params" : ""
          });
    };
    
    /*
    ** Recharge la liste des roadMaps de l'utilisateir
    */
    RoadMap.LoadRoadMaps = function(){
    
        let roadMaps = Dom.GetById("roadMaps");
        
        var data = "Class=RoadMap&Methode=LoadRoadMaps&App=RoadMap";
      
        Request.Post("Ajax.php", data).then(data => {
            roadMaps.innerHTML = data; 
            
            Event.AddByClass("btnRoadMap", "click", RoadMap.LoadRoadMap) ;
        });
        
    };
    
    /***
     * Charge la roadMap d'un projet
     **/
    RoadMap.LoadRoadMap = function(e, roadMapId){
        
        if(e == undefined){
            RoadMapWidget.Load(roadMapId);
       }
        else{
            RoadMapWidget.Load(e.srcElement.id);
        }
    };

	RoadMap.Init = function(){
	};

	/*
	* RoadMap de Shared with the User
	*/
	RoadMap.LoadMember = function(){		
		RoadMapWidget.AddEventRoadMap();
	};


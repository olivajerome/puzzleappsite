var DialogAdminIdeController = function(){};


DialogAdminIdeController.AddProjet = function(){
    IdeProjetForm.Init();
};

DialogAdminIdeController.ShowAddModule = function(){
    IdeModuleForm.Init();
};

DialogAdminIdeController.ShowAddHelper = function(){
    IdeHelperForm.Init();
};

DialogAdminIdeController.ShowAddWidget = function(){
    IdeWidgetForm.Init();
};

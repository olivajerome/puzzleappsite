CREATE TABLE IF NOT EXISTS `MarketProductCategory` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`ProductId` INT  NOT NULL,
`CategoryId` INT  NOT NULL,
PRIMARY KEY (`Id`),
CONSTRAINT `MarketProduct_MarketProductCategory` FOREIGN KEY (`ProductId`) REFERENCES `MarketProduct`(`Id`),
CONSTRAINT `MarketCategory_MarketProductCategory` FOREIGN KEY (`CategoryId`) REFERENCES `MarketCategory`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
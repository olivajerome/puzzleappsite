CREATE TABLE IF NOT EXISTS `ComunityFollow` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`ComunityId` INT  NULL ,
`UserId` INT  NULL ,
PRIMARY KEY (`Id`),
CONSTRAINT `ee_user_ComunityFollow` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`),
CONSTRAINT `ComunityComunity_ComunityFollow` FOREIGN KEY (`ComunityId`) REFERENCES `ComunityComunity`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
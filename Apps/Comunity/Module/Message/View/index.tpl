<div class='tchatTitle'>
   <div class="avatarmini-img avatarmini" style="cursor: pointer;"> 
      <img src="{{memberImage}}" alt="images"> 
    </div>
    <div>
        {{memberPseudo}}
    </div>
</div>
<div class='messageContent' id='messageContent'  >
    {{Messages}}
</div>
<div class='center'>
    <textarea id='tbTchatMessage' class='form-control' placeHolder='Ecrivez votre message' ></textarea>
    
    <button id='btnSendTchatMessage' class='btn btn-primary'>Envoyer</button>
    
</div>

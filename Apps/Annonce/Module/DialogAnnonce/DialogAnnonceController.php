<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Annonce\Module\DialogAnnonce;

use Core\Control\Button\Button;
use Core\Controller\Controller;
use Core\View\View;

use Apps\Annonce\Widget\AnnonceAnnonceForm\AnnonceAnnonceForm;

/*
 * 
 */
 class DialogAnnonceController extends Controller
 {
    /**
     * Constructeur
     */
    function _construct($core="")
    {
          $this->Core = $core;
    }

    /**
     * Create
     */
    function Create()
    {
    }

    /**
     * Init
     */
    function Init()
    {
    }

    /**
     * Render
     */
    function Show($all=true)
    {
         return $this->Index();
    }
    
    /*
    * Get the home page
    */
   function Index()
   {
       $view = new View(__DIR__."/View/index.tpl", $this->Core);
       return $view->Render();
   }
    
   /*
    * Add Annonce
    */
   function AddAnnonce()
   {
      $annonceForm = new AnnonceAnnonceForm($this->Core);
      return $annonceForm->Render();
    }
    
  
          /*action*/
 }?>
<?php 
/*
* PuzzleApp
* Webemyos
* Jérôme Oliva
* GNU Licence
*/

namespace Apps\Shop\Entity;
use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class ShopCaracteristiqueItem extends Entity  
{
	//Constructeur
	function __construct($core)
	{
		//Version
		$this->Version ="2.0.0.0"; 

		//Nom de la table 
		$this->Core=$core; 
		$this->TableName="ShopCaracteristiqueItem"; 
		$this->Alias = "ShopCaracteristiqueItem"; 

		$this->CaracteristiqueId = new Property("CaracteristiqueId", "CaracteristiqueId", NUMERICBOX,  true, $this->Alias); 
		$this->Label = new Property("Label", "Label", TEXTBOX,  true, $this->Alias); 

		//Creation de l entité 
		$this->Create(); 
	}
}
?>
<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\EeApp\Module\Home;

use Apps\EeApp\EeApp;
use Core\Control\Button\Button;
use Core\Control\Text\Text;
use Core\Controller\Controller;
use Core\View\View;
use Core\View\ElementView;

/**
 * Module d'accueil
 * */
 class HomeController extends Controller 
 {
    /**
     * Constructeur
     */
    function __construct($core="")
    {
        $this->Core = $core;
    }

    /**
     * Creation
     */
    function Create()
    {
    }

    /**
     * Initialisation
     */
    function Init()
    {
    }

    /**
     * Affichage du module
     */
    function Show($all=true)
    {
        $view = new View(__DIR__ . "/View/Home.tpl", $this->Core);

        //Bouton mes applications
        $btnMyApp = new Button(BUTTON, "btnMyApp");
        $btnMyApp->Value = $this->Core->GetCode("EeApp.MyApp");
        $btnMyApp->CssClass = "btn btn-info";
        $btnMyApp->OnClick = "EeAppAction.LoadMyApp();";
        $view->AddElement($btnMyApp);

        //Fichier Partage
        $btnShowApp= new Button(BUTTON, "btnShowApp");
        $btnShowApp->Value = $this->Core->GetCode("EeApp.Apps");
        $btnShowApp->CssClass = "btn btn-success";
        $btnShowApp->OnClick = "EeAppAction.LoadApps();";
        $view->AddElement($btnShowApp);
        
        //Template disponiblme
        $btnShowApp= new Button(BUTTON, "btnShowTemplate");
        $btnShowApp->Value = $this->Core->GetCode("EeApp.AvailableTemplates");
        $btnShowApp->CssClass = "btn btn-success";
        $btnShowApp->OnClick = "EeAppAction.LoadTemplates();";
        $view->AddElement($btnShowApp);
        
        $btnShowPlugin = new Button(BUTTON, "btnShowPlugin");
        $btnShowPlugin->Value = $this->Core->GetCode("EeApp.AvailablePlugin");
        $btnShowPlugin->CssClass = "btn btn-success";
        $btnShowPlugin->OnClick = "EeAppAction.LoadPlugins();";
        $view->AddElement($btnShowPlugin);
        
        //Le store
        $btnStore= new Button(BUTTON, "btnStore");
        $btnStore->Value = $this->Core->GetCode("EeApp.Store");
        $btnStore->CssClass = "btn btn-warning";
        $btnStore->OnClick = "EeAppAction.LoadStore();";
        $view->AddElement($btnStore);
   
        $view->AddElement(new ElementView("frameworkVersion", $this->Core->GetVersion()));

        
        if(EeApp::IsAdmin($this->Core, "EeApp", $this->Core->User->IdEntite))
        {
            $btnAdmin = new Button(BUTTON, "btnAdmin");
            $btnAdmin->Value = $this->Core->GetCode("EeApp.Admin");
            $btnAdmin->CssClass = "btn btn-danger";
            $btnAdmin->OnClick = "EeAppAction.LoadAdmin();";
            $view->AddElement($btnAdmin);
        }
        else
        {
            $view->AddElement(new Text("btnAdmin"));
        }
   
        return $view->Render();
    }
 }?>
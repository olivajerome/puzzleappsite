<input type='hidden' id='hdComunityId' value='{{Comunity->IdEntite}}'/> 


<div class="container" id='appRunComunity' style='position: relative; top: 0px;'>
   
    {{if completePage == true}}
    
        <input type='hidden' id='reseauCode' value='{{reseauCode}}'/> 
	    <div class='col-md-3 leftColumn ' style='position: sticky; top: 80px; z-index: 999; height: 0;'>
		<div class='navigation'>
			<a class='linkAllbleu' href='{{GetPath(/{{reseauCode}})}}'><i class="fa fa-arrow-left"></i> Revenir à l'accueil</a>
			<a href={{GetPath(/{{reseauCode}}/ParlonsEn/NewComunityion)}} class='btn btn-blue' ><i class='fa fa-plus'>&nbsp;</i>Créer un projet</a>
		</div>

		{{userProjetWidget}}

	</div>
        
    <div id='wallProjet' class='col-md-9'>        
            
    {{/if completePage == true}}
    
        <div class='col-md-12' id='dashboardDiscus'>
			<div class='col-md-12 projetTitle' style='background-image: url("{{Comunity->GetFirstImageUrl()}}");'>
				<div class='projetAction'>
					<div class='col-md-4' >
						<h1 style='font-size: 24px; font-weight: bold;'>{{Comunity->Title->Value}}</h1>
						<div class='followers'>
							{{membres}} . {{followers}} 
						</div>
					</div>
					   {{ActionProjetWidget}}
				</div>   
			 </div>  
        </div>
        <div class='col-md-9'>
             
        <div id='dashboardDiscusMessage'>
            
            {{if isConnected == true}}
            
            <div class='messageBlock row'>
                <div class='col-md-2'>
                    <div title="Jean391" class="avatarmini-img avatarmini" style="cursor: pointer;">
                        <img src="{{userImage}}" alt="images">
                    </div>
                </div>
                <div class='col-md-10'>
                   <input id="tbMessage" type="text" class="form-control" placeholder="Publier quelque chose">
                   
                   
                </div>
                <div class='col-md-1'>
                   
                </div>    
                <div class='col-md-12 center imagesButton'>
                  {{uploadComunity}} 
                </div>
            </div>   
           
            {{/if isConnected == true}}
            
                  
            
            <div class='messages' id='messages' >  
                {{Messages}}
            </div>
       </div> 
        </div>
        <div class='col-md-3'>
      
            <div class='projet'>
	        <h3>&Agrave; propos de ce projet</h3>
        
                <span style="font-size: 12px;">Actif depuis le <b class='orange'>{{Comunity->DateCreated->Value}}</b></span>
        
                <p style="font-size: 12px;">
                    {{Comunity->Description->Value}}
                </p>
            </div>  
                
             {{MemberProjetWidget}}   
                
            <div class='projet' style='display:none'>
                <h3>Les membres de ce projet</h3>
               {{Comunity->GetMembres()}}
            </div>
        </div>
                
    
    </div>
</div>
    
</div>


          
         
                        
<script>
    Comunity.Init();
</script>
                        
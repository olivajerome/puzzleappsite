var NotifyMenu = function(){};

/***
 * Init the Form
 * @returns {undefined}
 */
NotifyMenu.Init = function(){
    Event.AddById("notifyMenu", "click", NotifyMenu.Open);
};

/***
 * Open the contexte notify
 */
NotifyMenu.Open = function(e){
     
   var notifyMenu = new ContextMenu(e);
          
         if(notifyMenu.IsOpen()){
             ContextMenu.Close();
         }
         else{
 
             notifyMenu.App = "Notify";
             notifyMenu.Methode = "GetInfo";
             notifyMenu.Params = "";
             notifyMenu.right = "0";
             notifyMenu.top = "50";
             notifyMenu.height = "100";
             notifyMenu.position = "fixed";
             
             notifyMenu.onLoaded = function(){
 
               let nbInfoNotify = Dom.GetById("nbInfoNotify");
              
                   if(nbInfoNotify != null){
                       nbInfoNotify.innerHTML = 0;
                   }
                   
                   
                   Event.AddById("#btnLoadMoreNotify", "click", function(){
                      console.log("Load More nNotify"); 
                   });
             };
 
             notifyMenu.Show();
         }
};
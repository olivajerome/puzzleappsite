class Editable{

    constructor(control, callBack){

        this.control = control;
        this.callBack = callBack;
    }

    SetEditable()
    {
        let instance = this;
        this.control.style.cursor = "pointer";
        Event.AddEvent(this.control, "click", function(){instance.EditElement();});
    }

    Blur(){
        this.control.innerHTML = this.editElement.value;
        this.control.style.display ="";

        this.callBack(this.control);
        this.editElement.parentNode.removeChild(this.editElement);
    }

    EditElement(){
        let instance = this;    
        let container = this.control.parentNode;
        this.editElement = document.createElement("input");
        this.editElement.type = "text";
        this.editElement.value = this.control.innerHTML;
        this.editElement.className = "form form-control";
        container.prepend(this.editElement);
        this.control.style.display = "none";

        Event.AddEvent(this.editElement, "blur", function(){instance.Blur();});
    }
};

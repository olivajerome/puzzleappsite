<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Downloader\Entity ;

use Core\Entity\Entity\Argument;
use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;

class DownloaderRessource extends Entity
{
    //Constructeur
    function __construct($core)
    {
        //Version
        $this->Version ="2.0.0.0";

        //Nom de la table
        $this->Core=$core;
        $this->TableName="DownloaderRessource";
        $this->Alias = "DownloaderRessource";

        $this->UserId = new Property("UserId", "UserId", NUMERICBOX,  true, $this->Alias);
        $this->Url = new Property("Url", "Url", TEXTBOX,  false, $this->Alias);
        $this->Name = new Property("Name", "Name", TEXTBOX,  true, $this->Alias);
        $this->Code = new Property("Code", "Code", TEXTBOX,  true, $this->Alias);
        $this->Category = new Property("Category", "Category", NUMERICBOX,  true, $this->Alias);
        $this->Description = new Property("Description", "Description", TEXTAREA,  false, $this->Alias);
        $this->LongDescription = new Property("LongDescription", "LongDescription", TEXTAREA,  false, $this->Alias);
        
        //Partage entre application
        $this->AddSharedProperty();

        //Repertoire de stockage des ressources    
        $this->DirectoryImage = "Data/Apps/Downloader/";

        //Creation de l entité
        $this->Create();
    }
    
    /*
     * Get Number of Download
     */
    function GetNumberEmail()
    {
        $contact = new DownloaderRessourceContact($this->Core);
        $contact->AddArgument(new Argument("Apps\Downloader\Entity\DownloaderRessourceContact", "RessourceId", EQUAL, $this->IdEntite));

        return count($contact->GetByArg());
    }
    
    /***
     * Return link url 
     */
    function GetPath(){
        
        $document = $this->GetImages();
      
        return $document[0];
    }
    
    /****
     * Retur the firls file path
     */
    function GetDocPath(){
    
        $documents = $this->GetDocuments();
        
        return $documents[0];
    }
    
    /****
     * 
     */
    function GetImageFullPath(){
        if (file_exists($this->DirectoryImage . $this->IdEntite)) {
            
            return  $this->Core->GetPath("/" . $this->DirectoryImage . $this->IdEntite . "/full.png");
        }
    }
    
    /***
     * Path to download
     */
    function GetDownloadPath(){
        return $this->Core->GetPath("/Downloader/Download/".$this->Code->Value );  
    }

    /***
     * Path to the Documentation
     */
    function GetDoocPath(){
     return $this->Core->GetPath("/Downloader/Documentation/".$this->Code->Value );  
    }
    
     /*     * *
     * Obtient les Images
     */

    function GetImages() {

        $images = array();

        if ($this->IdEntite != "") {

            $this->DirectoryImage . $this->IdEntite;

            if (file_exists($this->DirectoryImage . $this->IdEntite)) {

                $fd = dir($this->DirectoryImage . $this->IdEntite);
                $html = "";

                while ($file = $fd->read()) {
                    if (($file != ".") && ($file != "..") && /* $file != "thumb.png" &&*/ strpos($file, '.zip') === false) {

                        $images[] = $this->Core->GetPath("/" . $this->DirectoryImage . $this->IdEntite . "/" . $file);
                    }
                }
                $fd->close();
            }
        }

        return $images;
    }
    
    /***
     * Get Document of the ressource
     */
    function GetDocuments(){
        $documents = array();

        if ($this->IdEntite != "") {

            $this->DirectoryImage . $this->IdEntite;

            if (file_exists($this->DirectoryImage . $this->IdEntite)) {

                $fd = dir($this->DirectoryImage . $this->IdEntite);
                $html = "";

                while ($file = $fd->read()) {
                    if (($file != ".") && ($file != "..") && strpos($file, '.png') === false && strpos($file, '.jpg') === false  ) {

                        $documents[] = $this->Core->GetPath("/" . $this->DirectoryImage . $this->IdEntite . "/" . $file);
                    }
                }
                $fd->close();
            }
        }

        return $documents;
    }
    
    
}
?>

var Notes = function () {};

/*
 * Add Event when the App loaded
 */
Notes.Load = function (parameter)
{
    this.LoadEvent();
    Notes.InitTabNote();
};

/*
 * Add event
 */
Notes.LoadEvent = function ()
{
    Dashboard.AddEventAppMenu(Notes.Execute, "", "Notes");
    Dashboard.AddEventWindowsTool("Notes");
};

/***
 * Start the Event for Member 
 */
Notes.LoadMember = function () {
};

/*
 * Execute a function
 */
Notes.Execute = function (e)
{
    //Call the function
    Dashboard.Execute(this, e, "Notes");
    return false;
};

/***
 * Init the app front page
 */
Notes.Init = function (app, method) {
    if (app == "Notes") {
        switch (method) {
            default :
                break;
        }
    }
};

/***
 * Init the Admin Widget
 */
Notes.InitAdminWidget = function () {

};


/**
* Tab des catgéogrie
    * @returns {
        undefined
        }
*/
    
Notes.InitTabNote = function () {
            //Catégory
    Event.AddById("btnAddNote", "click", () => {
        Notes.ShowAddNote("");
    });
    
    Event.AddByClass("editNote","click", function(e){
        Notes.EditNote(e.srcElement.id);
    });
    
    EntityGrid.Initialise('gdNote');
};

/**
 * Edit category
 * @param {type} categoryId
 * @returns {undefined}
 */
Notes.EditNote = function (noteId) {
    Notes.ShowAddNote(noteId);
};

/**
 * Dialogue d'ajout de catégorie
 */
Notes.ShowAddNote = function (noteId) {

    Dialog.open('', {"title": Dashboard.GetCode("Notes.AddNote"),
        "app": "Notes",
        "class": "DialogAdminNotes",
        "method": "ShowAddNote",
        "type": "left",
        "params": noteId
    });
};

/***
 * Rafraichit la tab des catégorie
 * @returns {undefined}
 */
Notes.ReloadNotes = function (data) {
    let gdNote = Dom.GetById("gdNote");
    
    gdNote.parentNode.innerHTML = data.data.tabGrid;
    Notes.InitTabNote();

    let tabPostIt = Dom.GetById("tabPostIt");
    tabPostIt.innerHTML =  data.data.tabPostIt;
    
};

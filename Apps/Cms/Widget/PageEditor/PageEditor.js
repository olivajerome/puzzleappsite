var PageEditor = function(){};

PageEditor.Init = function(){
    
  Event.AddById("btnAddContainer", "click", PageEditor.ShowAddContainer);
  Event.AddByClass("btnEditContainer", "click", PageEditor.EditContainer);
  Event.AddByClass("btnRemoveContainer", "click", PageEditor.RemoveContainer);
  Event.AddByClass("btnAddBlock", "click", PageEditor.ShowAddBlock);
  Event.AddByClass("btnEditBlock", "click", LayoutEditor.EditBlock);
  Event.AddByClass("btnRemoveBlock", "click", LayoutEditor.RemoveBlock);
  Event.AddByClass("blockContent", "click", LayoutEditor.ShowToolContentBlock);
  Event.AddByClass("linkBlock", "click", LayoutEditor.EditItemMenu);
  Event.AddByClass("btnAddItemMenu", "click", LayoutEditor.AddItemMenu);
  Event.AddByClass("bntEditImage", "click", LayoutEditor.EditImage);
  Event.AddByClass("btnEditSource", "click", LayoutEditor.btnEditSource);

  //Event.AddById("pageName", "blur", PageEditor.UpdatePageName);
  //Event.AddById("pageTitle", "blur", PageEditor.UpdatePageTitle);
  //Event.AddById("pageDescription", "blur", PageEditor.UpdatePageDescription);
};

/***
 * Ad a container
 * @returns {undefined}
 */
PageEditor.ShowAddContainer = function(){
    
    Dialog.open('', {"title": Dashboard.GetCode("Cms.AddContainer"),
          "app": "Cms",
          "class": "DialogCms",
          "method": "ShowAddContainer",
          "type": "left",
          "params" : Dom.GetById("pageId").value
          });
};

/**
 * Edit a container
 * @param {} e 
 */
PageEditor.EditContainer = function(e){
    
  Dialog.open('', {"title": Dashboard.GetCode("Cms.EditContainer"),
        "app": "Cms",
        "class": "DialogCms",
        "method": "EditContainer",
        "type": "left",
        "params" : e.srcElement.parentNode.parentNode.id
        });
};

/***
 * Delete a container
 */
PageEditor.RemoveContainer = function(e){

  let container = e.srcElement.parentNode.parentNode;
  
  Animation.Confirm(Language.GetCode("Cms.RemoveContainer"), function(){

    let data = "Class=Cms&Methode=RemoveContainer&App=Cms";
         data +=  "&ContainerId=" + container.id;
         
            Request.Post("Ajax.php", data).then(data => {
                Cms.RefreshTool();
            });   
  });
};

/***
 * Dialog d'ajout d'un block
 * @param {type} e
 * @returns {undefined}
 */
PageEditor.ShowAddBlock = function(e){
  let block = e.srcElement;
  let container = block.parentNode.parentNode.id;
  
  Dialog.open('', {"title": Dashboard.GetCode("Cms.AddBlock"),
        "app": "Cms",
        "class": "DialogCms",
        "method": "ShowAddBlockContainer",
        "type": "left",
        "params" : container
        });
};

/***
 * Update the Name Page
 */
PageEditor.UpdatePageName = function(e){

  let data = "Class=Cms&Methode=UpdatePageName&App=Cms";
      data +=  "&PageId=" + Dom.GetById("PageId").value;
      data +=  "&Name=" + e.srcElement.innerHTML;

          Request.Post("Ajax.php", data).then(data => {
              Cms.RefreshTool();
          });   
};

/***
 * Update the Title Page
 */
PageEditor.UpdatePageTitle = function(e){

  let data = "Class=Cms&Methode=UpdatePageTitle&App=Cms";
      data +=  "&PageId=" + Dom.GetById("PageId").value;
      data +=  "&Title=" + e.srcElement.innerHTML;

          Request.Post("Ajax.php", data).then(data => {
              Cms.RefreshTool();
          });   
};

/***
 * Updtae de description page
 */
PageEditor.UpdatePageDescription = function(e){

  let data = "Class=Cms&Methode=UpdatePageDescription&App=Cms";
      data +=  "&PageId=" + Dom.GetById("PageId").value;
      data +=  "&Description=" + e.srcElement.innerHTML;

          Request.Post("Ajax.php", data).then(data => {
              Cms.RefreshTool();
          });   
};




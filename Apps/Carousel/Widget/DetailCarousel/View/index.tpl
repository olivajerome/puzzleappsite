<div>
     <div id='container{{Carousel->IdEntite}}' data-parameters='{{Carousel->GetParameters()}}' >
         
         {{foreach Items}}
            <div class='item' style='display:none'>
               {{element->Content->Value}}     
            </div>
         {{/foreach Items}}
    </div>
</div>

<script>
    Animation.Caroussel("container{{Carousel->IdEntite}}");
</script>
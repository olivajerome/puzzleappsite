<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */


 namespace Apps\Annonce\Helper;

 use Apps\Annonce\Entity\AnnonceAnnonce;
 use Apps\Annonce\Entity\AnnonceResponse;
 
class AnnonceHelper
{
    
    /***
     * Get Annonce in Status 2 for a category
     */
    public static function GetPublishedByCategory($core, $categoryId){
        
        $annonce = new AnnonceAnnonce($core);
        return $annonce->Find("CategoryId= " . $categoryId . " and Status = 2");
    }
    
    /***
     * Get Annonce of a user
     */
    public static function GetByUser($core, $userId){
        $annonce = new AnnonceAnnonce($core);
        return $annonce->Find("UserId= " . $userId . " and Status <> 99 order by id desc");
    }
    
    /***
     * Get All Response af annonce
     */
    public static function GetResponses($core, $annonceId){
        
        $response = new AnnonceResponse($core);
        return $response->Find("AnnonceId=" . $annonceId);
    }
}
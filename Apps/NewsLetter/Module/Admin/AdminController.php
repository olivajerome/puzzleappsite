<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\NewsLetter\Module\Admin;

use Core\Control\Button\Button;
use Core\Controller\Controller;
use Core\View\View;
use Core\View\ElementView;
use Core\Control\TabStrip\TabStrip;
use Core\Control\EntityGrid\EntityGrid;
use Core\Control\EntityGrid\EntityColumn;
use Core\Control\EntityGrid\EntityIconColumn;
use Core\Control\EntityGrid\EntityFunctionColumn;
use Core\Entity\Entity\Argument;
use Apps\NewsLetter\Entity\NewsLetterConfig;
use Apps\NewsLetter\Widget\NewsLetterConfigForm\NewsLetterConfigForm;

/*
 * 
 */

class AdminController extends Controller {

    /**
     * Constructeur
     */
    function _construct($core = "") {
        $this->Core = $core;
    }

    /**
     * Creation
     */
    function Create() {
        
    }

    /**
     * Initialisation
     */
    function Init() {
        
    }

    /**
     * Affichage du module
     */
    function Show($all = true) {
        return $this->Index();
    }

    /*
     * Get the home page
     */

    function Index() {
        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);

        $tabNewsLetter = new TabStrip("tabNewsLetter", "NewsLetter");
        $tabNewsLetter->AddTab($this->Core->GetCode("NewsLetter.Config"), $this->GetTabConfig());
        $tabNewsLetter->AddTab($this->Core->GetCode("NewsLetter.EmailBase"), $this->GetTabEmailBase());
        $tabNewsLetter->AddTab($this->Core->GetCode("NewsLetter.EmailProgramme"), $this->GetTabEmailProgramme());
        $tabNewsLetter->AddTab($this->Core->GetCode("NewsLetter.EmailEvent"), $this->GetTabEmailEvent());
        $tabNewsLetter->AddTab($this->Core->GetCode("NewsLetter.ListEmail"), $this->GetTabListEmail());
        $tabNewsLetter->AddTab($this->Core->GetCode("NewsLetter.Campagne"), $this->GetTabCampagne());
        $tabNewsLetter->AddTab($this->Core->GetCode("NewsLetter.CampagneSended"), $this->GetTabCampagneSended());
       
        $view->AddElement(new ElementView("tabNewsLetter", $tabNewsLetter->Render()));

        return $view->Render();
    }

    /*     * *
     * Configuration des emails
     */

    function GetTabConfig() {

        $view = new View(__DIR__ . "/View/tabConfig.tpl", $this->Core);

        $newsLetterConfigForm = new NewsLetterConfigForm($this->Core);

        $newLetterConfig = new NewsLetterConfig($this->Core);
        $newLetterConfig = $newLetterConfig->Find("id > 0");
        
        if(count($newLetterConfig)> 0){
            $newLetterConfig = $newLetterConfig[0];
        }
        
        $newsLetterConfigForm->Load($newLetterConfig);

        $view->AddElement(new ElementView("newsLetterConfigForm", $newsLetterConfigForm->Render()));

        $plugins = \Apps\EeApp\Helper\AppHelper::GetPluginApp($this->Core, "NewsLetter");
        
        $view->AddElement(new ElementView("Plugins", $plugins));

        return $view->Render();

    }

    /*     * *
     * Email Sended By the Site, After Inscription, On contact
     */

    function GetTabEmailBase() {

        $gdEmailBase = new EntityGrid("gdEmailBase", $this->Core);
        $gdEmailBase->Entity = "Apps\NewsLetter\Entity\NewsLetterEmail";
        $gdEmailBase->App = "NewsLetter";
        $gdEmailBase->Action = "GetTabEmailBase";
        
        $gdEmailBase->AddArgument(new Argument("Apps\NewsLetter\Entity\NewsLetterEmail", "TypeId", EQUAL, 1));
        
        $gdEmailBase->AddColumn(new EntityColumn("Libelle", "Libelle"));
        $gdEmailBase->AddColumn(new EntityColumn("Code", "Code"));
        $gdEmailBase->AddColumn(new EntityColumn("Description", "Description"));

        $gdEmailBase->AddColumn(new EntityIconColumn("Action",
                        array(array("EditIcone", "NewsLetter.EditEmail", "NewsLetter.EditEmail"),
                    array("DeleteIcone", "NewsLetter.DeleteEmail", "NewsLetter.DeleteEmail"),
                        )
        ));
        
        $infoEmailBase = "  <div class='info'><i class='fa fa-info'></i>".
                         $this->Core->GetCode("NewsLetter.EmailBaseDescription") .
                         " </div>";
        

        return $infoEmailBase.$gdEmailBase->Render();
    }

    /***
     * Email Programme
     */
    function GetTabEmailProgramme() {

        $gdEmailProgram = new EntityGrid("gdEmailProgram", $this->Core);
        $gdEmailProgram->Entity = "Apps\NewsLetter\Entity\NewsLetterEmail";
        $gdEmailProgram->App = "NewsLetter";
        $gdEmailProgram->Action = "GetTabEmailProgram";
        
        $gdEmailProgram->AddArgument(new Argument("Apps\NewsLetter\Entity\NewsLetterEmail", "TypeId", EQUAL, 2));
        
        $gdEmailProgram->AddColumn(new EntityColumn("Libelle", "Libelle"));
        $gdEmailProgram->AddColumn(new EntityColumn("Code", "Code"));
        $gdEmailProgram->AddColumn(new EntityColumn("Description", "Description"));

        $gdEmailProgram->AddColumn(new EntityIconColumn("Action",
                        array(array("EditIcone", "NewsLetter.EditEmail", "NewsLetter.EditEmail"),
                    array("DeleteIcone", "NewsLetter.DeleteEmail", "NewsLetter.DeleteEmail"),
                        )
        ));
        
        $infoEmailProgramm = "  <div class='info'><i class='fa fa-info'></i>".
                            $this->Core->GetCode("NewsLetter.EmailProgrammeDescription") .
                        "</div>";

        return $infoEmailProgramm.$gdEmailProgram->Render();
    }
    
    /***
     * Email Event
     */
    function GetTabEmailEvent() {

        $gdEmailEvent = new EntityGrid("gdEmailEvent", $this->Core);
        $gdEmailEvent->Entity = "Apps\NewsLetter\Entity\NewsLetterEmail";
        $gdEmailEvent->App = "NewsLetter";
        $gdEmailEvent->Action = "GetTabEmailProgram";
        
        $gdEmailEvent->AddArgument(new Argument("Apps\NewsLetter\Entity\NewsLetterEmail", "TypeId", EQUAL, 3));
        
        $gdEmailEvent->AddColumn(new EntityColumn("Libelle", "Libelle"));
        $gdEmailEvent->AddColumn(new EntityColumn("Code", "Code"));
        $gdEmailEvent->AddColumn(new EntityColumn("Description", "Description"));

        $gdEmailEvent->AddColumn(new EntityIconColumn("Action",
                        array(array("EditIcone", "NewsLetter.EditEmail", "NewsLetter.EditEmail"),
                    array("DeleteIcone", "NewsLetter.DeleteEmail", "NewsLetter.DeleteEmail"),
                        )
        ));

        $infoEmailEvent = "  <div class='info'><i class='fa fa-info'></i>".
             $this->Core->GetCode("NewsLetter.EmailEventDescription") .
            "</div>";

        
        return $infoEmailEvent.$gdEmailEvent->Render();
    }
    
    /***
     * Liste d'email
     */
    function GetTabListEmail(){
        
        $gdListEmail = new EntityGrid("gdListEmail", $this->Core);
        $gdListEmail->Entity = "Apps\NewsLetter\Entity\NewsLetterListeEmail";
        $gdListEmail->App = "NewsLetter";
        $gdListEmail->Action = "GetTabListEmail";
        
        
        $gdListEmail->AddColumn(new EntityColumn("Libelle", "Libelle"));
        $gdListEmail->AddColumn(new EntityColumn("Description", "Description"));

        $gdListEmail->AddColumn(new EntityIconColumn("Action",
                        array(array("EditIcone", "NewsLetter.EditLisEmail", "NewsLetter.EditListEmail"),
                    array("DeleteIcone", "NewsLetter.DeleteListEmail", "NewsLetter.DeleteListEmail"),
                        )
        ));

        $infoListEmail = "  <div class='info'><i class='fa fa-info'></i>".
                            $this->Core->GetCode("NewsLetter.ListEmailDescription") ."</div>";

        
        return $infoListEmail.$gdListEmail->Render();
    }
    
    /***
     * Campagne
     */
    function GetTabCampagne(){
         
        $gdCampagne = new EntityGrid("gdCampagne", $this->Core);
        $gdCampagne->Entity = "Apps\NewsLetter\Entity\NewsLetterEmailCampagne";
        $gdCampagne->App = "NewsLetter";
        $gdCampagne->Action = "GetTabCampagne";
        
        $gdCampagne->AddColumn(new EntityColumn("Libelle", "Libelle"));
        $gdCampagne->AddColumn(new EntityColumn("Description", "Description"));

        $gdCampagne->AddColumn(new EntityIconColumn("Action",
                        array(array("EditIcone", "NewsLetter.EditCampagne", "NewsLetter.EditCampagne"),
                              array("ShareIcone", "NewsLetter.SendCampagne", "NewsLetter.SendCampagne"),
                              array("DeleteIcone", "NewsLetter.DeleteCampagne", "NewsLetter.DeleteCampagne"),
                        )
        ));

        $infoCampagne = "  <div class='info'><i class='fa fa-info'></i>".
                            $this->Core->GetCode("NewsLetter.CampagneDescription") ."</div>";

        
        return $infoCampagne.$gdCampagne->Render();
    }

    /**
     * Sended Campagne
     */
    function GetTabCampagneSended(){
         
        $gdCampagneSended = new EntityGrid("gdCampagneSended", $this->Core);
        $gdCampagneSended->Entity = "Apps\NewsLetter\Entity\NewsLetterEmailCampagneSend";
        $gdCampagneSended->App = "NewsLetter";
        $gdCampagneSended->Action = "GetTabCampagne";
        
        $gdCampagneSended->AddColumn(new EntityColumn("CampagneId", "CampagneId"));
        $gdCampagneSended->AddColumn(new EntityColumn("DateSended", "DateSended"));
        $gdCampagneSended->AddColumn(new EntityColumn("Emails", "Emails"));
        
        $infoCampagneSended = "  <div class='info'><i class='fa fa-info'></i>".
                            $this->Core->GetCode("NewsLetter.CampagneSendedDescription") ."</div>";

        
        return $infoCampagneSended.$gdCampagneSended->Render();
    }
    
    
    /* action */
}

?>
var DialogCmsController = function(){};


DialogCmsController.ShowAddPage = function(){
  PageForm.Init();
};

/***
 * Ajout d'un block
 */
DialogCmsController.ShowAddBlock = function(){
  BlockForm.Init();
};

/**
 * Add Item to a menu
 */
DialogCmsController.AddItemMenu = function(){
  BlockItemForm.Init();
};

/***
 * Edit item Menu
 */
DialogCmsController.EditItemMenu = function(){
  BlockItemForm.Init();
};

/***
 * Edit/Add image to a block
 */
DialogCmsController.EditImage = function(){
  BlockImageForm.Init();
};

/***
 * Edit/Add image to a block
 */
DialogCmsController.AddImage = function(){
  BlockImageForm.InitAdd();
};

/**
 * Add Container
 */
DialogCmsController.ShowAddContainer = function(){
  ContainerForm.Init();
};

/**
 * Edit a container
 */
DialogCmsController.EditContainer = function(){
  ContainerForm.Init();
};

/***
 * Add Block to a item
 */
DialogCmsController.ShowAddBlockContainer = function(params){
  BlockForm.Init();
};

DialogCmsController.ShowImportPage = function(){
  ImportForm.Init();
};

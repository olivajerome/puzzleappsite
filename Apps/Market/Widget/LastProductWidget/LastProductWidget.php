<?php

namespace Apps\Market\Widget\LastProductWidget;

use Core\View\View;
use Core\View\ElementView;

use Apps\Market\Helper\ProductHelper;


class LastProductWidget {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "") {
        $this->Core = $core;
        $this->Data = $data;
    }

     /**
     * Caroussel des derniers produit
     */
    function Render(){

        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);

        $view->AddElement(new ElementView("Product", ProductHelper::GetLast($this->Core, true)));

        return $view->Render();
    }

}
<?php

/*
 * PuzzleApp
 * Webemyos
 * J�r�me Oliva
 * GNU Licence
 */

namespace Apps\RoadMap\Module\RoadMap;

use Core\Control\Button\Button;
use Core\Controller\Controller;
use Core\View\View;
use Core\View\ElementView;

use Apps\RoadMap\Helper\RoadMapHelper;


/*
 * 
 */
 class RoadMapController extends Controller
 {
    /**
     * Constructeur
     */
    function _construct($core="")
    {
          $this->Core = $core;
    }

    /**
     * Creation
     */
    function Create()
    {
    }

    /**
     * Initialisation
     */
    function Init()
    {
    }

    /**
     * Affichage du module
     */
    function Show($all=true)
    {
         return $this->Index();
    }
    
    /**
     * Charge les colonnes et les tickets d'une RoadMap
     **/
    function Load($roadMapId){
    
       $view = new View(__DIR__."/View/load.tpl", $this->Core);
       
       $view->AddElement(new ElementView("roadMapId", $roadMapId));
       $view->AddElement(new ElementView("columns", RoadMapHelper::GetColumns($this->Core, $roadMapId)));
       
       return $view->Render();
    }
    
    /**
     * Charge les roadmap de l'utilisateur
     */
    function LoadRoadMaps(){
       
       $view = new View(__DIR__."/View/roadmaps.tpl", $this->Core);
       
       $roadMaps = RoadMapHelper::GetByUser($this->Core, $this->Core->User->IdEntite, $projetId);
       
       $view->AddElement(new ElementView("RoadMaps", $roadMaps));
       return $view->Render();
    }
    
    /***
     * Menu du paramétrage
     */
    function GetParameterMenu(){
     
        $menu = "<ul>";
        $menu .= "<li><i class='fa fa-edit btnEditRoadMap'>".$this->Core->GetCode("RoadMap.EditRoadMap")."</i></li>";
        $menu .= "<li><i class='fa fa-trash btnDeleteRoadMap'>".$this->Core->GetCode("RoadMap.DeleteRoadMap")."</i></li>";
        $menu .= "</ul>";
        
        return $menu;
    }
}
<?php

namespace Apps\Blog\Widget\ImportBlogForm;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;

class ImportBlogForm extends Form {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "") {
        $this->Core = $core;
        $this->Data = $data;

        //On le fait ici pour avoir le formuaire l'affichage mais auss la validation
        $this->Init($data);
    }

    /**
     * Initialisation 
     */
    function Init($data) {
        $this->form = new Form("ImportBlogForm", $data);

        $this->form->SetView(__DIR__ . "/View/index.tpl", $this->Core);
        $this->form->SetAction("/View/index.tpl");

        $this->form->Add(array("Type" => "Upload",
            "Id" => "Upload",
            "App" => "Blog",
            "Method" => "UploadImport",
            "Multiple" => false,
            "Validators" => ["Required"],
            "Accept"    => "json"
        ));

        $this->form->Add(array("Type" => "Button",
            "Id" => "btnImportBlog",
            "Value" => $this->Core->GetCode("Blog.Import"),
            "CssClass" => "btn btn-primary"
        ));
    }

    /*     * *
     * Render the Form
     */

    function Render() {
        return $this->form->Render();
    }

    /*     * *
     * Validate the form
     */

    function Validate($data = "") {
        $errors = $this->form->Validate($this->Data);

        return $errors;
    }

    /*     * *
     * Load controls with the data l'entity
     */

    function Load($entity) {
        $this->form->Load($entity);
    }

    /*     * *
     * Load entity with the 
     *      */

    function Populate($entity) {
        
    }

    /*     * *
     * Gestion des erreurs
     */

    function Error() {
        $errors[] = $this->form->errors;

        return json_encode(array("statut" => "Error", "message" => $errors));
    }

}

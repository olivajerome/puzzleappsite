<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Carousel;

use Core\Core\Core;
use Core\Core\Request;
use Core\Core\Response;
use Apps\Base\Base;
use Apps\Carousel\Module\Admin\AdminController;
use Apps\Carousel\Entity\CarouselCarousel;

class Carousel extends Base {

    /**
     * Author and version
     * */
    public $Author = 'Webemyos';
    public $Version = '1.0.0.0';

    /**
     * Constructeur
     * */
    function __construct() {
        $this->Core = Core::getInstance();
        parent::__construct($this->Core, "Carousel");
    }

    /**
     * Set public route
     */
    function GetRoute($routes = "") {
        $this->Route->SetPublic(array());

        return $this->Route;
    }

    /*     * *
     * Execute action after install
     */

    function PostInstall() {
        \Apps\EeApp\Helper\AppHelper::UpdateAppParams($this->Core,
                "Carousel",
                $this->Version,
                "", // Add Description of the app
                0, // Set 1 if the App have a AdminWidgetDashBoard
                0  // Set 1 if the App have a Member Module
        );
    }

    /**
     * Start Admin
     */
    function Run() {
        echo parent::RunApp($this->Core, "Carousel", "Carousel");
    }

    /*
     * Sauvegarde un mooc
     */

    public function SaveCarousel() {
        $adminController = new AdminController($this->Core);
        $carouselId = $adminController->SaveCarousel(Request::GetPosts());

        if ($carouselId !== false) {
            return Response::Success(array("Id" => $carouselId, "html" => $adminController->GetTabCarousel()));
        } else {
            return Response::Error();
        }
    }

    /*     * *
     * Delete a Mooc
     */

    function DeleteCarousel() {
        $adminController = new AdminController($this->Core);

        if ($adminController->DeleteCarousel(Request::GetPost("CarouselId"))) {
            return Response::Success($adminController->GetTabCarousel());
        } else {
            return Response::Error();
        }
    }

    /*     * *
     * Edit a item
     */

    public function EditItem() {
        $adminController = new AdminController($this->Core);
        return $adminController->EditItem(Request::GetPost("itemId"), Request::GetPost("CarouselId"));
    }

    /*
     * Sauvegarde un mooc
     */

    public function SaveItem() {
        $adminController = new AdminController($this->Core);
        $itemId = $adminController->SaveItem(Request::GetPosts());

        if ($itemId !== false) {

            $Carousel = new CarouselCarousel($this->Core);
            $Carousel->GetById(Request::GetPost("CarouselId"));
            $items = $Carousel->GetItems(true);

            return Response::Success(array("Id" => $itemId, "items" => $items));
        } else {
            return Response::Error();
        }
    }

    /*     * *
     * Remove a item
     */

    public function RemoveItem() {
        $adminController = new AdminController($this->Core);

        if ($adminController->RemoveItem(Request::GetPost("itemId"))) {
            return Response::Success($adminController->GetTabCarousel());
        } else {
            return Response::Error();
        }
    }

    /*     * *
     * Get the widget
     */

    public function GetWidget($type = "", $params = "") {

        switch ($type) {
            case "DetailCarousel" :
                $widget = new \Apps\Carousel\Widget\DetailCarousel\DetailCarousel($this->Core);
                break;
        }

        return $widget->Render($params);
    }

    /*     * *
     * Get Forum Addable widget
     */

    public function GetListWidget() {
        return array(array("Name" => "DetailCarousel",
                "Description" => $this->Core->GetCode("Carousel.DescriptionDetailCarousel"),
                "Parameters" => $this->GetListCarousel()
            ),
        );
    }

    /*     * *
     * List of carousel
     */

    public function GetListCarousel() {
        $list = new \Core\Control\EntityListBox\EntityListBox("Data", $this->Core);
        $list->Entity = "\Apps\Carousel\Entity\CarouselCarousel";
        $list->AddField("Name");
        return $list->Render();
    }
}

var MarketOfferForm = function(){};

/***
 * Init the form
 * @returns {undefined}
 */
MarketOfferForm.Init = function(){
    Event.AddById("btnSendOffer", "click", MarketOfferForm.SendOffer);
};

/***
* Get the Id of element 
*/
MarketOfferForm.GetId = function(){
    return Form.GetId("MarketOfferFormForm");
};

/*
* Save the Element
*/
MarketOfferForm.SendOffer = function(e){
   
    if(Form.IsValid("MarketOfferFormForm"))
    {
    let request = new Http("Market", "SendOffer");
        request.SetData(Form.Serialize("MarketOfferFormForm"));

        request.Post(data => {

            data = JSON.parse(data);

            if(data.status == "error"){
                Form.RenderError("MarketOfferFormForm", data.data.message);
            } else{

              Dialog.Close();
              Animation.Notify(Language.GetCode("Market.OfferSended"));
            }
        });
    }
};


<?php
/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */


namespace Apps\Forum\Module\Admin;

use Core\Controller\AdministratorController;
use Core\View\View;
use Core\View\ElementView;
use Core\Control\TabStrip\TabStrip;
use Core\Control\EntityGrid\EntityGrid;
use Core\Control\EntityGrid\EntityColumn;
use Core\Control\EntityGrid\EntityIconColumn;
use Core\Control\EntityGrid\EntityFunctionColumn;
use Core\Entity\Entity\Argument;
use Core\Control\Button\Button;

use Apps\Forum\Entity\ForumForum;
use Apps\Forum\Helper\ForumHelper;
use Apps\Forum\Helper\CategoryHelper;
use Apps\Forum\Widget\ForumForm\ForumForm;

class AdminController extends AdministratorController
{
    /**
     * Constructeur
     */
    function __construct($core="")
    {
       $this->Core = $core;
    }
    
    /**
     * Affichage du module
     */
    function Show($all=true)
    {
         return $this->Index();
    }
    
    /*
    * Get the home page
    */
   function Index()
   {
      $forum = ForumHelper::GetFirst($this->Core);

       if($forum){
         
         $view = new View(__DIR__."/View/index.tpl", $this->Core);
        
         $tabForum = new TabStrip("tabForum", "Forum"); 
         $tabForum->AddTab($this->Core->GetCode("Forum.Forum"), $this->GetTabForum($forum));
         $tabForum->AddTab($this->Core->GetCode("Forum.Categories"), $this->GetTabCategory($forum));
         $tabForum->AddTab($this->Core->GetCode("Forum.Messages"), $this->GetTabMessage($forum));
         
         $view->AddElement(new ElementView("tabForum", $tabForum->Render($forum)));

        } else {
            
         $view = new View(__DIR__."/View/noForum.tpl", $this->Core);

         $forumForm = new ForumForm($this->Core);
         $view->AddElement(new ElementView("forumForm", $forumForm->Render($forum)));
       }

       return $view->Render();
   }

   /****
    * Tab Forum
    */
   function GetTabForum($forum){

        $forumForm = new ForumForm($this->Core, $forum);
        $forumForm->Load($forum);
        return $forumForm->Render($forum);   
   }

   /***
    * Categorie of the Forum
    */
    function GetTabCategory($forum){
     
        $gdCategory = new EntityGrid("gdCategory", $this->Core);
        $gdCategory->Entity = "Apps\Forum\Entity\ForumCategory";
        $gdCategory->AddArgument(new Argument("Apps\Forum\Entity\ForumCategory", "ForumId", EQUAL, $forum->IdEntite));
        $gdCategory->App = "Forum";
        $gdCategory->Action = "GetTabCategory";
  
        $btnAdd = new Button(BUTTON, "btnAddCategory");
        $btnAdd->Value = $this->Core->GetCode("Forum.AddCategory");
  
        $gdCategory->AddButton($btnAdd);
  
        $gdCategory->AddColumn(new EntityColumn("Name", "Name"));
        
        $gdCategory->AddColumn(new EntityIconColumn("Action", 
                                                  array(array("EditIcone", "Forum.EditCategory", "Forum.EditCategory"),
                                                        array("DeleteIcone", "Forum.DeleteCategory", "Forum.DeleteCategory"),
                                                  )    
                          ));
  
        return $gdCategory->Render();
     }


     //Message of all Forum
     function GetTabMessage(){
        $gdMessage = new EntityGrid("gdMessage", $this->Core);
        $gdMessage->Entity = "Apps\Forum\Entity\ForumMessage";
        //$gdMessage->AddArgument(new Argument("Apps\Forum\Entity\ForumCategory", "ForumId", EQUAL, $forum->IdEntite));
        $gdMessage->App = "Forum";
        $gdMessage->Action = "GetTabMessage";
        $gdMessage->AddOrder("DateCreated desc");
  
        $gdMessage->AddColumn(new EntityColumn("DateCreated", "DateCreated"));
        $gdMessage->AddColumn(new EntityColumn("Title", "Title"));
        $gdMessage->AddColumn(new EntityColumn("Message", "Message"));
       
        
        $gdMessage->AddColumn(new EntityIconColumn("Action", 
                                                  array(array("EditIcone", "Forum.EditMessage", "Forum.EditMessage"),
                                                        array("DeleteIcone", "Forum.DeleteMessage", "Forum.DeleteMessage"),
                                                  )    
                          ));
  
        return $gdMessage->Render();
     }
     
     /**
      * Save/Update the forum
      * @param type $data
      * @return type
      */
     function SaveForum($data){
        return ForumHelper::Save($this->Core, $data);
     }
     
     /***
      * Save/Update the categiry
      */
     function SaveCategory($data){
         return CategoryHelper::SaveCategory($this->Core, $data);
     }
     
     /***
      * Delete a category
      */
     function DeleteCategory($categoryId){
        return CategoryHelper::DeleteCategory($this->Core, $categoryId);
    }     
}

<?php

namespace Apps\Seo\Widget\PageSeoForm;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;

class PageSeoForm extends Form {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "") {
        $this->Core = $core;
        $this->Data = $data;

        //On le fait ici pour avoir le formuaire � l'affichage mais aussi � la validation
        $this->Init($data);
    }

    /**
     * Initialisation 
     */
    function Init($data) {
        $this->form = new Form("pageSeoForm", $data);

        $this->form->SetView(__DIR__ . "/View/index.tpl", $this->Core);
        $this->form->SetAction("/View/index.tpl");

        $this->form->Add(array("Type" => "Hidden",
            "Id" => "Id",
        ));
        
        $this->form->Add(array("Type" => "TextBox",
            "Id" => "Url",
        ));
        
        $this->form->Add(array("Type" => "TextBox",
            "Id" => "Title",
            "Validators" => ["Required"]
        ));
        
        $this->form->Add(array("Type" => "TextBox",
            "Id" => "Description",
            "Validators" => ["Required"]
        ));
        
      /*  $this->form->Add(array("Type" => "TextArea",
            "Id" => "Content",
            "Validators" => ["Required"]
        ));*/
        
        
        $this->form->Add(array("Type" => "Button",
            "Id" => "btnSavePage",
            "Value" => "Save",
            "CssClass" => "btn btn-primary"
        ));
    }

    /*     * *
     * G�n�rer les formulaire
     */

    function Render() {
        return $this->form->Render();
    }

    /*     * *
     * Valide les donn�es selon les diff�rents formulaires
     */

    function Validate($data = "") {
        $errors = $this->form->Validate($this->Data);

        return $errors;
    }

    /*     * *
     * Charge les controls avec les donn�es de l'entit�
     */

    function Load($itineraire) {
        $this->form->Load($itineraire);
    }

    /*     * *
     * Charge l'entit� avec les donn�es des diff�rents formulaire
     */

    function Populate($product) {
        
    }

    /*     * *
     * Gestion des erreurs
     */

    function Error() {
        $errors[] = $this->form->errors;

        return json_encode(array("statut" => "Error", "message" => $errors));
    }

}

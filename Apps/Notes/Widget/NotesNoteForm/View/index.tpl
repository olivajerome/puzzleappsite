{{form->Open()}}
{{form->Error()}}
{{form->Success()}}

{{form->Render(Id)}}


    <div>
        <label>{{GetCode(Notes.Text)}}</label> 
        {{form->Render(Text)}}
    </div>

    <div>
        <label>{{GetCode(Notes.Visible)}}</label> 
        {{form->Render(Visible)}}
    </div>

    
    <div class='center marginTop' >   
        {{form->Render(btnSave)}}
    </div>  

{{form->Close()}}
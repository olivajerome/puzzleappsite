<?php

namespace Apps\Forms\Widget\DetailForm;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;

use Apps\Forms\Module\Front\FrontController;

class DetailForm extends Form {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "") {

        $this->Core = $core;
        $this->Data = $data;

        $this->Init($data);
    }
    
    function Init(){
    }

    function Render($params =""){
       
        $frontController = new FrontController($this->Core);
        return $frontController->Index($params);
    }

}

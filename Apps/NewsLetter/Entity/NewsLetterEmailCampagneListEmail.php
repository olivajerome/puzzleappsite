<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\NewsLetter\Entity;

use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class NewsLetterEmailCampagneListEmail extends Entity {

    //Constructeur
    function __construct($core) {
        //Version
        $this->Version = "2.0.0.0";

        //Nom de la table 
        $this->Core = $core;
        $this->TableName = "NewsLetterEmailCampagneListEmail";
        $this->Alias = "NewsLetterEmailCampagneListEmail";

        $this->CampagneId = new Property("CampagneId", "CampagneId", NUMERICBOX, false, $this->Alias);
        $this->ListEmailId = new Property("ListEmailId", "ListEmailId", NUMERICBOX, false, $this->Alias);

        //Creation de l entité 
        $this->Create();
    }
}

?>
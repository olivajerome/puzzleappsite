class Paragraph extends BaseTool {
    getIcone() {
        return "P";
    }

    getName() {
        return Language.GetCode("Base.TextRichEditorParagraph");
    }
    execute(e) {
        e.preventDefault();
        this.applyStyle("p");
        super.execute();
    }
}
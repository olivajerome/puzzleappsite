<input type='hidden' id='reseauCode' value='{{reseauCode}}'/> 


        
    <div class="row">
            <div class="col-md-3 leftColumn"  style="position: sticky; top: 80px; z-index: 999; height: 0;">
               <div class='navigation'>
                <a class='linkAllbleu' href='{{GetPath(/{{reseauCode}})}}'><i class="fa fa-arrow-left"></i> Revenir à l'accueil</a>
                <a href={{GetPath(/{{reseauCode}}/ParlonsEn/NewDiscussion)}} class='btn btn-blue' ><i class='fa fa-plus'>&nbsp;</i>Créer un projet</a>
                </div>
                <div class='projets'>
                    {{userProjetWidget}}
                </div>
            </div>
            <div id='wallProjet' class='col-md-6'>
                <h1>L'activité de vos projets</h1>
                {{HomeWall}}
				
            </div>
            <div class='col-md-3'>
                                    <div class='AboutProjet' id='AboutProjet'>
                                            <h2>A propos des projets</h2>
                                            <p>

                                                Retrouver ici tous les projets de votre réseau et leur actualités, le but est de pouvoir faciliter le lien et l’entraide par le biais de la création de projets collectifs, alors  n'hésitez surtout pas à créer des projets ou bien participer à ceux déjà disponibles. 
Si vous avez des idées, il est tout à fait possible de la soumettre en créant le projet directement, ou bien en proposant  un thème dans le fil d’actualité générale situé dans la page d'accueil de votre réseau.  

                                            </p>
                                    </div>
                            </div>
            
    </div>



<script>
    ParlonsEn.Init();
</script>
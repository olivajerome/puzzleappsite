/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function Caroussel(control) {

    this.container = document.getElementById(control);
    this.parameters = JSON.parse(this.container.dataset.parameters);
    
    if (this.container == null) {
        return;
    }

    if (this.container.className == "carrousselInline")
    {
        this.type = "inline";

        // Ajout des fléches gauche/Droite
        var arrowLeft = document.createElement("span");
        arrowLeft.id = "carrouselLeft";
        arrowLeft.className = "arrowLeft";
        arrowLeft.innerHTML = "<i  class='fa fa-caret-left fa-3x'></i></i>";
        this.container.append(arrowLeft);

        // Ajout des fléches gauche/Droite
        var arrowRight = document.createElement("span");
        arrowRight.className = "arrowRight";
        arrowRight.id = "carrouselRight";
        arrowRight.innerHTML = "<i  class='fa fa-caret-right fa-3x'></i>";
        this.container.append(arrowRight);

        var carrousel = this;

        Event.AddEvent(arrowLeft, "click", function () {
            carrousel.NaviguateInline(-1);
        });

        Event.AddEvent(arrowRight, "click", function () {
            carrousel.NaviguateInline(1);
        });
    } else
    {
        this.type = "block";
        this.items = this.container.getElementsByClassName("item");
        this.currentIndex = 0;

        var carrousel = this;

        for (var i = 1; i < this.items.length; i++)
        {
            this.items[i].style.display = "none";
        }

        // Ajout des fléches gauche/Droite
        var arrowLeft = document.createElement("span");
        arrowLeft.id = "carrouselLeft";
        arrowLeft.className = "arrowLeft";
        arrowLeft.innerHTML = "<i  class='class='fa-solid fa-caret-left fa-3x'></i>";
        this.container.append(arrowLeft);

        // Ajout des fléches gauche/Droite
        var arrowRight = document.createElement("span");
        arrowRight.className = "arrowRight";
        arrowRight.id = "carrouselRight";
        arrowRight.innerHTML = "<i  class='class='fa-solid fa-caret-right fa-3x'>&nbsp;</i>";
        this.container.append(arrowRight);

        Event.AddById("carrouselLeft", "click", function () {
            if (carrousel.currentIndex > 0) {
                carrousel.Naviguate(-1);
            }
        });

        Event.AddById("carrouselRight", "click", function () {
            if (carrousel.currentIndex < carrousel.items.length - 1) {
                carrousel.Naviguate(1);
            }
        });


        //Ajout du pager en bas au centre
        var pager = document.createElement("div");
        pager.id = "carrouselPager";
        pager.className = "carrouselPager";

        for (var i = 0; i < this.items.length; i++) {
            itemClass = (i == 0) ? "active" : "";

            pager.innerHTML += "<span id='circle" + i + "' class='fa fa-circle fa-2x " + itemClass + " '>&nbsp;</span>";
        }

        this.container.append(pager);
    }

    /**
     * Navigue entre les sections
     */
    this.Naviguate = function (step) {

        //Ajustement mobile
        if (window.innerWidth < 500) {
            var stepWidth = 350;
        } else {
            var stepWidth = 350;
        }

        if (this.type == "inline")
        {

            var items = this.container.getElementsByClassName("item");

            if (this.currentIndex == (items.length - 1) || this.currentIndex == -1) {
                this.currentIndex = 0;
            } else {
                this.currentIndex += step;
            }

            for (var i = 0; i < items.length; i++)
            {
                items[i].style.left = "-" + (this.currentIndex * stepWidth) + "px";
            }
        } else
        {
            this.items[carrousel.currentIndex].style.display = "none";
            this.currentIndex += step;
            this.items[carrousel.currentIndex].style.display = "";

            var itemPager = this.container.getElementsByClassName("fa-circle");

            for (var i = 0; i < itemPager.length; i++) {
                if (i == carrousel.currentIndex) {
                    Animation.AddClass(itemPager[i].id, "active");
                } else {
                    Animation.RemoveClass(itemPager[i].id, "active");
                }
            }
        }
    };

    this.NaviguateInline = function (index) {
        clearInterval(carrousel.interval);
        carrousel.Naviguate(index);
    };

    /**
     * Lance le timer
     */
    this.Start = function ()
    {
        var carrousel = this;

        carrousel.currentIndex = 1;
        carrousel.Naviguate(-1);


        if(carrousel.parameters.speed != undefined){
            intervall = carrousel.parameters.speed;
        }
        else if (carrousel.type == "inline") {
            intervall = 2000;
        } else {
            intervall = 5000;
        }


        carrousel.interval = setInterval(function () {

            if (carrousel.type == "inline")
            {
                carrousel.Naviguate(1);
            } else
            {
                if (carrousel.currentIndex == carrousel.items.length - 1)
                {

                    for (var i = 1; i < carrousel.items.length; i++)
                    {
                        carrousel.items[i].style.display = "none";
                    }

                    carrousel.currentIndex = 0;
                    carrousel.Naviguate(0);
                } else
                {
                    carrousel.Naviguate(1);
                }
            }

        }, intervall);
    }
}
;
<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Plugins\RatingPlugin\Entity;

use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class RatingNote extends Entity {

    //Constructeur
    function __construct($core) {
        //Version
        $this->Version = "2.0.0.0";

        //Nom de la table 
        $this->Core = $core;
        $this->TableName = "RatingNote";
        $this->Alias = "RatingNote";

        $this->UserId = new Property("UserId", "UserId", NUMERICBOX, false, $this->Alias);
        $this->EntityName = new Property("EntityName", "EntityName", TEXTBOX, false, $this->Alias);
        $this->EntityId = new Property("EntityId", "EntityId", NUMERICBOX, false, $this->Alias);
        $this->Note = new Property("Note", "Note", TEXTBOX, false, $this->Alias);

        $this->UserName = new Property("UserName", "UserName", TEXTBOX, false, $this->Alias);
        $this->DateCreated = new Property("DateCreated", "DateCreated", DATEBOX, false, $this->Alias);
       
        //Creation de l'entité 
        $this->Create();
    }
}

?>
<?php

namespace Apps\Cms\Widget\ExportImportEditor;

use Core\View\View;
use Core\View\ElementView;

class ExportImportEditor{
    
    public function __construct($core) {
        $this->Core = $core;
    }
    
    /***
     * Render the widget
     */
    public function Render(){
        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);
        return $view->Render();
    }
}
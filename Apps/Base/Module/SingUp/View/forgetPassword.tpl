<section class='container'>

<h1>{{GetCode(Base.ForgetPassword)}}</h1>


{{if Success == false}}

<form method='post' action='' class='block' >  
 <label>{{GetCode(Email)}}</label>
    {{GetControl(EmailBox,email,{Required=true, PlaceHolder=VotreEmail})}} <br/>


   <div class='center marginTop'>
        <button type='submit' class='btn btn-primary'>{{GetCode(Base.Valid)}}</button>
    </div>

</form>

{{/if Success == false}}

{{if Success == true}}

    {{Message}}
    
{{/if Success == true}}

</section>
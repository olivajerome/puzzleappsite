var CaracteristiqueForm = function(){
   
};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
CaracteristiqueForm.Init = function(callBack){
    Event.AddById("btnSaveCaracteristique", "click", CaracteristiqueForm.SaveElement);
    Event.AddById("btnAddItem", "click", CaracteristiqueForm.AddItem);
    CaracteristiqueForm.callBack = callBack;
};

/***
* Obtient l'id de l'itin�raire courant 
*/
CaracteristiqueForm.GetId = function(){
    return Form.GetId("CaracteristiqueForm");
};

/*
* Sauvegare l'itineraire
*/
CaracteristiqueForm.SaveElement = function(e){
   
    if(Form.IsValid("CaracteristiqueForm"))
    {

    var data = "Class=Shop&Methode=SaveCaracteristique&App=Shop";
        data +=  Form.Serialize("CaracteristiqueForm");


        let items = document.querySelectorAll("#caracteristiqueItem input");
        let itemsData = Array();

        for(var i = 0; i < items.length ; i ++){
            itemsData.push({id:items[i].id, label : items[i].value });
        }

        data += "&items="+ JSON.stringify(itemsData);

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.statut == "Error"){
                Form.RenderError("CaracteristiqueForm", data.message);
            } else{

                Form.SetId("CaracteristiqueForm", data.data.Id);
                Dialog.Close();
                
                 
                if(CaracteristiqueForm.callBack){
                    CaracteristiqueForm.callBack(data);
                }
            }
        });
    }
};

/***
 * Ajout un item
 */
CaracteristiqueForm.AddItem = function(e){
    let addItem = "<input type='text' id='0' style='width:90%;margin-right:5px' class='form-control'/>";
        addItem += "<i class='fa fa-trash removeCaracteristique' onclick='CaracteristiqueForm.RemoveItem(this)' ></i>";

    let addItemContainer = document.createElement("div");
    addItemContainer.innerHTML = addItem;
    addItemContainer.style.display = "flex";
    addItemContainer.style.marginTop = "5px";

    let caracteristiqueItem = document.getElementById("caracteristiqueItem");
    caracteristiqueItem.appendChild(addItemContainer);
};

/***
 * Remove a itme
 */
CaracteristiqueForm.RemoveItem = function(control) {

    let itemId = control.parentNode.querySelector("input").id;

    if(itemId != 0){
        let data = "Class=Shop&Methode=RemoveCaracteristiqueItem&App=Shop";
            data += "&itemId=" + itemId;    

            Request.Post("Ajax.php", data).then(data => {
                let container = control.parentNode;
                container.parentNode.removeChild(container);    
                        
            });
    }

};

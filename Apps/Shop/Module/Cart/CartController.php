<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Shop\Module\Cart;

use Core\Control\Button\Button;
use Core\Controller\Controller;
use Core\View\View;
use Core\View\ElementView;

use Core\Core\Request;

/*
 * 
 */
 class CartController extends Controller
 {
    /**
     * Constructeur
     */
    function _construct($core="")
    {
          $this->Core = $core;
    }

    /**
     * Creation
     */
    function Create()
    {
    }

    /**
     * Initialisation
     */
    function Init()
    {
    }

    /**
     * Affichage du module
     */
    function Show($all=true)
    {
         return $this->Index();
    }
    
    /*
    * Get the home page
    */
   function Index()
   {
       $view = new View(__DIR__."/View/index.tpl", $this->Core);
       
       $cart = json_decode(Request::GetSession("Cart"));
   
      if($cart == null){
          return $this->Core->GetCode("Shop.YouCartIsEmpty");
      }
       
       $total = 0;
       
       if(isset($cart->lines)){
           
           $html = "<table class='cartTable grid'>";
           $html .= "<tr><th>".$this->Core->GetCode("Shop.Product")."</th><th>".$this->Core->GetCode("Shop.Price")."</th></tr>";
           
           foreach($cart->lines as $line){
               
                $html .= "<tr><td>".$line->Name."<i style='float:right' id='".$line->IdEntite."' class='fa fa-trash removeCartLine'>&nbsp;</i></td><td style='text-align:right'>".$line->Price."&euro;</td></tr>";
                
                $total += (float)$line->Price;
           }
           
           $html .= "<tr><td><b>Total</b></td><td style='text-align:right'><b>".$total."&euro;</b></tr>";
           
           $html .= "</table>";
       }
       
       $html .= "<div class='center marginTop'><a href='".$this->Core->GetPath("/Shop/Cart")."' class='btn btn-primary width100'>".$this->Core->GetCode("Shop.ValidateMyCart")."</a></div>";
       
       $view->AddElement(new ElementView("table", $html));
               
       return $view->Render();
   }
          
          /*action*/
 }?>
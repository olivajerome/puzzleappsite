<?php

namespace Apps\Mooc\Widget\AdminDashBoard;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;
use Core\Entity\Entity\Argument;
use Apps\Mooc\Helper\MessageHelper;
use Apps\Mooc\Entity\MoocMooc;

class AdminDashBoard {
    
    public function __construct($core) {
        $this->Core = $core;
    }
    
    /***
     * Render the widget
     */
    public function Render(){
        $view = new View(__DIR__ . "/View/adminDashboard.tpl", $this->Core);
       
        $mooc = new MoocMooc($this->Core);
        
        $view->AddElement(new ElementView("Mooc", $mooc->Find( " Id > 0 Order by Id desc limit 0,10 " )));
        
        return $view->Render();
    }
}

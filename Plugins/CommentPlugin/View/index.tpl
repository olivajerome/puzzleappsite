<h1>{{GetCode(Base.AddComment)}}</h1>
{{commentForm}}

<h1>{{GetCode(Base.Comments)}}</h1>
{{foreach Comments}}
    <div style='text-align: left'>
        <div>
            <b>{{element->DateCreated->Value}} </b>
            {{element->UserName->Value}}
        </div>
        <p>
            {{element->Message->Value}}
        </p>
    </div>

{{/foreach Comments}}


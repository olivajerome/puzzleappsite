<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Notify\Module\DialogAdminNotify;

use Core\Control\Button\Button;
use Core\Controller\AdministratorController;

use Core\View\View;

use Apps\Notify\Widget\NotifyParametersForm\NotifyParametersForm;
use Apps\Notify\Entity\NotifyParameters;

use Apps\Notify\Widget\NotifyTemplateForm\NotifyTemplateForm;
use Apps\Notify\Entity\NotifyTemplate;

/*
 * 
 */
 class DialogAdminNotifyController extends AdministratorController
 {
    /**
     * Constructeur
     */
    function _construct($core="")
    {
          $this->Core = $core;
    }

    /**
     * Create
     */
    function Create()
    {
    }

    /**
     * Init
     */
    function Init()
    {
    }

    /**
     * Render
     */
    function Show($all=true)
    {
         return $this->Index();
    }
    
    /*
    * Get the home page
    */
   function Index()
   {
       $view = new View(__DIR__."/View/index.tpl", $this->Core);
       return $view->Render();
   }

   /**
    * Add/Edit a parameter
    */
   function ShowAddParameter($parameterId){

    $parametersForm = new NotifyParametersForm($this->Core);

    if($parameterId != ""){
        $parameters = new NotifyParameters($this->Core);
        $parameters->GetById($parameterId);
        $parametersForm->Load($parameters);
    }

    return $parametersForm->Render();

   }

   /***
    * Add template for a spécifique email 
    */
  function ShowAddTemplate($templateId){

    $templateForm = new NotifyTemplateForm($this->Core);

    if($templateId != ""){
        $template = new NotifyTemplate($this->Core);
        $template->GetById($templateId);
        $templateForm->Load($template);
    }

    return $templateForm->Render();

  }


          
          /*action*/
 }?>
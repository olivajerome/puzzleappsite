<section>
    <div class='col-md-3'>
        <label>Simple Dialog</label><br>
        {{GetControl(Button,btnHide,{Value=Open dialog,CssClass=btn btn-primary,Id=btnOpenSimple,)})}}
    </div>
    <div class='col-md-3'>
        <label>Left Dialog</label><br>
        {{GetControl(Button,btnHide,{Value=Open left dialog,CssClass=btn btn-primary,Id=btnOpenLeft,)})}}
    </div> 
    
    <div class='col-md-3'>
        <label>Right Dialog</label><br>
        {{GetControl(Button,btnHide,{Value=Open right dialog,CssClass=btn btn-primary,Id=btnOpenRight,)})}}
    </div> 
    
    <div class='col-md-3'>
        <label>Full Dialog</label><br>
        {{GetControl(Button,btnHide,{Value=Open full dialog,CssClass=btn btn-primary,Id=btnOpenFull,)})}}
    </div> 
    
    
</section>

    
    <script>
    
    Event.AddById("btnOpenSimple", "click", function(){
    
     Dialog.open('', {"title": "Simple Dialog",
        "app": "PuzzleApp",
        "class": "DialogPuzzle",
        "method": "GetSimpleContent",
        "params": ""
     });
            
    });
    
    Event.AddById("btnOpenLeft", "click", function(){
    
     Dialog.open('', {"title": "Simple Dialog",
        "app": "PuzzleApp",
        "class": "DialogPuzzle",
        "method": "GetSimpleContent",
        "type" : Dialog.Type.Left,
        "params": ""
     });
            
    });
    
    Event.AddById("btnOpenRight", "click", function(){
    
     Dialog.open('', {"title": "Simple Dialog",
        "app": "PuzzleApp",
        "class": "DialogPuzzle",
        "method": "GetSimpleContent",
        "params": "",
        "type" : Dialog.Type.Right,
     });
            
    });
    
    Event.AddById("btnOpenFull", "click", function(){
    
     Dialog.open('', {"title": "Simple Dialog",
        "app": "PuzzleApp",
        "class": "DialogPuzzle",
        "method": "GetSimpleContent",
        "params": "",
        "type" : Dialog.Type.Full,
     });
            
    });
    
    </script>
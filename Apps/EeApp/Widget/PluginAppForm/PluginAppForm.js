var PluginAppForm = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
PluginAppForm.Init = function(){
    Event.AddById("btnAddPluginToApp", "click", PluginAppForm.SaveElement);
};

/***
* Obtient l'id de l'itin�raire courant 
*/
PluginAppForm.GetId = function(){
    return Form.GetId("PluginAppFormForm");
};

/*
* Sauvegare l'itineraire
*/
PluginAppForm.SaveElement = function(e){
   
    if(Form.IsValid("PluginAppFormForm"))
    {

    var data = "Class=EeApp&Methode=AddPluginToApp&App=EeApp";
        data +=  Form.Serialize("PluginAppFormForm");

        
        Request.Post("Ajax.php", data).then(data => {
            
            let App = Form.GetValue("PluginAppFormForm", "AppName");
            
            Dialog.Close();
            
            eval(App + ".RefreshPlugin()");
        });
    }
};


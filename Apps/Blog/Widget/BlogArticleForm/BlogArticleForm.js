var BlogArticleForm = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
BlogArticleForm.Init = function(callBack){

    Event.AddById("btnSaveArticle", "click", BlogArticleForm.SaveArticle);
    BlogArticleForm.callBack = callBack;
};

/***
* Obtient l'id de l'itin�raire courant 
*/
BlogArticleForm.GetId = function(){
    return Form.GetId("articleForm");
};

/*
* Sauvegare l'itineraire
*/
BlogArticleForm.SaveArticle = function(e){
   
    if(Form.IsValid("articleForm"))
    {

    let data = "Class=Blog&Methode=SaveArticle&App=Blog";
        data +=  Form.Serialize("articleForm");
      
        
        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);
         
            if(data.statut == "Error"){
                Form.RenderError("articleForm", data.message);
            } else {

                Dialog.Close();
                
                if(BlogArticleForm.callBack){
                    BlogArticleForm.callBack(data);
                }
            }
        });
    }
};


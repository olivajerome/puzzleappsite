<?php

/*
 * PuzzleApp
 * Webemyos
 * J�r�me Oliva
 * GNU Licence
 */

namespace Apps\RoadMap\Module\Home;

use Core\Control\Button\Button;
use Core\Controller\Controller;
use Core\View\View;
use Core\View\ElementView;


use Apps\RoadMap\Helper\RoadMapHelper;


/*
 * 
 */
 class HomeController extends Controller
 {
    /**
     * Constructeur
     */
    function _construct($core="")
    {
          $this->Core = $core;
    }

    /**
     * Creation
     */
    function Create()
    {
    }

    /**
     * Initialisation
     */
    function Init()
    {
    }

    /**
     * Affichage du module
     */
    function Show($all=true)
    {
         return $this->Index();
    }
    
    /*
    * Get the home page
    */
   function Index($projetId = "")
   {
       $view = new View(__DIR__."/View/index.tpl", $this->Core);
       
       $roadMaps = RoadMapHelper::GetByUser($this->Core, $this->Core->User->IdEntite, $projetId);
       
       $view->AddElement(new ElementView("RoadMaps", $roadMaps));
       return $view->Render();
   }
          
          /*action*/
 }?>
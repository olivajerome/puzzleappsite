var Lang = function () {};

Lang.Load = function (parameter)
{
    Lang.LoadEvent();
};

Lang.LoadEvent = function ()
{
    Dashboard.AddEventAppMenu(Lang.Execute, "", "Lang");
    Dashboard.AddEventWindowsTool("Lang");

    LangAction.AddEvent();

    Event.AddById("btnAddLang", "click", () => {
        Animation.Ask(Language.GetCode("Lang.LangName"), function (name) {

            let data = "Class=Lang&Methode=AddLang&App=Lang";
            data += "&langName=" + name;

            Request.Post("Ajax.php", data).then(data => {
                Dashboard.StartApp("", "Lang", "");
            });
        });
    });
};

Lang.Execute = function (e)
{
    //Appel de la fonction
    Dashboard.Execute(this, e, "Lang");
    return false;
};


Lang.Comment = function ()
{
    Dashboard.Comment("Lang", "1");
};

/*
 *	Affichage de a propos
 */
Lang.About = function ()
{
    Dashboard.About("Lang");
};

/*
 *	Affichage de l'aide
 */
Lang.Help = function ()
{
    Dashboard.OpenBrowser("Lang", "{$BaseUrl}/Help-App-Lang.html");
};

/*
 *	Affichage de report de bug
 */
Lang.ReportBug = function ()
{
    Dashboard.ReportBug("Lang");
};

/*
 * Fermeture
 */
Lang.Quit = function ()
{
    Dashboard.CloseApp("", "Lang");
};

/*
 * Evenements
 */
LangAction = function ()
{

};

/*
 * Charge les Elements courantes
 */
LangAction.LoadElement = function (page, kewWord, keyValue)
{
    var data = "Class=Lang&Methode=LoadElement&App=Lang";
    data += "&page=" + page;

    if (kewWord != undefined && kewWord != "")
    {
        data += "&keyWord=" + kewWord;
    }

    if (keyValue != undefined && keyValue != "")
    {
        data += "&keyValue=" + keyValue;
    }

    Dashboard.LoadControl("elementContainer", data, "60vh", "div", "Lang");


    LangAction.AddEvent();



    //Ajout des evenement de sauvegarde et de suppression
    /* $("#Id tr .icon-edit ").click(function(){LangAction.EditElement(this) });
     $("#Id tr .icon-save ").click(function(){LangAction.SaveElement(this) });
     
     $("#Id tr .fa-remove").click(function(){LangAction.RemoveElement(this) });
     $("#Id tr input").blur(function(){LangAction.UpdateElement(this) });*/
};

LangAction.AddEvent = function () {
    var desktop = document.getElementById("appRunLang");
    var input = desktop.getElementsByTagName("input");

    for (i = 0; i < input.length; i++)
    {
        Dashboard.AddEvent(input[i], "blur", LangAction.UpdateElement);
    }

};

LangAction.EditElement = function (e)
{
    var container = e.parentNode.parentNode;
    var control = container.getElementsByTagName("input");


    //MOde POP up
    var param = Array();
    param['App'] = 'Lang';
    param['Title'] = 'Lang.EditElement';
    param['elementId'] = control[0].id;

    Dashboard.OpenPopUp('Lang', 'EditElement', '', '', '', 'LangAction.RefreshElement()', serialization.Encode(param));

    Dashboard.SetBasicAdvancedText("tbLibelle");
};

/*
 * Enregistre l'element
 * @param {type} e
 * @returns {undefined}
 */
LangAction.SaveElement = function (e)
{
    var container = e.parentNode.parentNode;
    var control = container.getElementsByTagName("input");
};

/*
 * Delete a element
 */
LangAction.RemoveElement = function (e)
{
    var idElement = e.parentNode.parentNode.childNodes[0].nextSibling.innerHTML;

    var JAjax = new ajax();
    JAjax.data = "App=Lang&Methode=RemoveElement";
    JAjax.data += "&idElement=" + idElement;

    JAjax.GetRequest("Ajax.php");

    e.parentNode.parentNode.parentNode.removeChild(e.parentNode.parentNode);
};

/***
 * Delete a element
 */
LangAction.UpdateElement = function (e)
{

    if (e.srcElement.id != "tbSearch" && e.srcElement.id != "btnSearch" && e.srcElement.id != "tbSearchLangue" && e.srcElement.id != "btnSearchLangue")
    {
        var idElement = e.srcElement.parentNode.parentNode.childNodes[0].nextSibling.innerHTML;

        if (idElement != undefined)
        {
            var JAjax = new ajax();
            JAjax.data = "App=Lang&Methode=UpdateElement";
            JAjax.data += "&idElement=" + idElement;
            JAjax.data += "&value=" + e.srcElement.value;

            JAjax.GetRequest("Ajax.php");

            Animation.Notify(Language.GetCode("Lang.TextUpdated"));
        }
    }
};

/***
 * Recherche un �lement par son code ou son llibelle
 * @returns {undefined}
 */
LangAction.Search = function () {
    var tbSearch = document.getElementById("tbSearch");
    LangAction.LoadElement(0, tbSearch.value);
};

/***
 * Recherche un �lement par son code ou son llibelle
 * @returns {undefined}
 */
LangAction.SearchLangue = function () {
    var tbSearch = document.getElementById("tbSearchLangue");


    LangAction.LoadElement(0, "", tbSearchLangue.value);
};

/***
 * Initialisation du front widget
 * @returns {undefined}
 */
Lang.Init = function () {

};


Lang.InitAdminWidget = function () {

    var tbSearchLang = document.getElementById("tbSearchLang");

    Event.AddById("tbSearchLang", "change", function () {

        var data = "Class=Lang&Methode=LoadElement&App=Lang";
        data += "&page=0";
        data += "&all=0";

        data += "&keyWord=" + tbSearchLang.value;

        Request.Post("Ajax.php", data).then(data => {

            var resultSearchLang = document.getElementById("resultSearchLang");
            resultSearchLang.innerHTML = data;

            var desktop = document.getElementById("resultSearchLang");
            var input = desktop.getElementsByTagName("input");

            for (i = 0; i < input.length; i++)
            {
                Dashboard.AddEvent(input[i], "blur", LangAction.UpdateElement);
            }
        });
    });
};
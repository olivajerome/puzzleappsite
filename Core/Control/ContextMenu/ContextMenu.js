
/**
 * Menu contextuel
 * @param {*} control 
 */
function ContextMenu(event) {

    this.event = event;

    this.Show = function () {

        var contextMenu = document.getElementById("contextMenu");

        //On ne réouvre pas un deuxième menu contextuel
        if (contextMenu != null) {
            //return;
        }

        menu = document.createElement('div');
        menu.id = "contextMenu";

        if (this.position != undefined) {
            menu.style.position = this.position;

        } else {
            menu.style.position = 'absolute';
        }
        
        menu.style.minWidth = "200px";
        menu.style.width = "250px";

        if (this.height) {
            menu.style.height = "calc(" + this.height + "%)";
        } else {
            menu.style.minHeight = "150px";
            menu.style.maxHeight = "350px";
        }

        menu.style.overflow = "auto";
        menu.style.zIndex = 9999999;
        menu.style.padding = "5px";

        if (this.right != undefined) {
            menu.style.right = this.right + "px";
        } else {
            menu.style.left = (this.event.clientX - 200) + "px";
        }

        if (this.top != undefined) {
            menu.style.top = this.top + "px";
        } else {
            menu.style.top = this.event.pageY + "px";
        }

        menu.style.backgroundColor = "white";
        menu.innerHTML = "<div style='text-align:right;width:100%'><i id='btnCloseContext' class='fa fa-remove' alt='' title='Fermer' >&nbsp;</i></div>";

        menu.innerHTML += "<span>Chargement...</span>";

        var data = "App=" + this.App + "&Methode=" + this.Methode;
        data += this.Params;

        var url = window.location.origin + "/Ajax.php";

        Request.Post(url, data).then(data => {

            var content = menu.getElementsByTagName("span");
            content[0].innerHTML = data;


            this.onLoaded();

        });

        document.body.appendChild(menu);

        Event.AddById("btnCloseContext", "click", this.Close);
    };

    /*
     * Ferme le tool tip
     */
    this.Close = function () {
        var contextMenu = document.getElementById("contextMenu");
        document.body.removeChild(contextMenu);
    };

    /*
     * Vérifie si un context menu est ouvert
     */
    this.IsOpen = function () {
        var contextMenu = document.getElementById("contextMenu");
        return (contextMenu != null)
    };
};

ContextMenu.Close = function(){
     var contextMenu = document.getElementById("contextMenu");
     
     if(contextMenu != null){
        document.body.removeChild(contextMenu);
    }
};
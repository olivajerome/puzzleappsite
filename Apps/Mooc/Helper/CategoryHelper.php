<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

 namespace Apps\Mooc\Helper;

use Apps\Mooc\Entity\MoocCategory;
use Apps\Mooc\Widget\MoocCategoryForm\MoocCategoryForm;
 
class CategoryHelper
{   
    /**
     * Sauvagarde une catégorie
     */
    public static function Save($core, $name, $description, $categoryId)
    {
        $category = new MoocCategory($core);
        
        if($categoryId != "")
        {
            $category->GetById($categoryId);
        }
        
        $category->Name->Value = $name;
        $category->Description->Value = $description;
        $category->Save();   
    }

    /*     * *
     * Sauvegarde une catégorie
     */

     public static function SaveCategory($core, $data) {

        $categoryForm = new MoocCategoryForm($core, $data);

        if ($categoryForm->Validate($data)) {

            $category = new MoocCategory($core);

            if ($data["Id"] != "") {
                $category->GetById($data["Id"]);
            } 

            $categoryForm->Populate($category);
            $category->Save();

            return true;
        }

        return false;
    }

    /*     * *
     * Sauvegarde une catégorie
     */

     public static function DeleteCategory($core, $categoryId) {
   
        //TODO DELETE THE MOOC BEFORE
        $category = new MoocCategory($core);
        $category->GetById($categoryId);
        $category->Delete();
        return true;
    }
}


?>

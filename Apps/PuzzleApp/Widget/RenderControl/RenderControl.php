<?php

namespace Apps\PuzzleApp\Widget\RenderControl;

use Core\Core\Core;
use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;
use Core\Control\AutoCompleteBox\AutoCompleteBox;
use Core\Control\Button\Button;
use Core\Control\Captcha\Captcha;
use Core\Control\CheckBox\CheckBox;
use Core\Control\ListCheckBox\ListCheckBox;
use Core\Control\DateBox\DateBox;
use Core\Control\DateTimeBox\DateTimeBox;
use Core\Control\EmailBox\EmailBox;
use Core\Control\NumericBox\NumericBox;
use Core\Control\RadioButton\RadioButton;
use Core\Control\ListRadio\ListRadio;
use Core\Control\TextBox\TextBox;
use Core\Control\NoteBox\NoteBox;
use Core\Control\PassWord\PassWord;
use Core\Control\TextArea\TextArea;
use Core\Control\ListBox\ListBox;
use Core\Control\EntityListBox\EntityListBox;
use Core\Control\EntityGrid\EntityGrid;
use Core\Control\EntityGrid\EntityColumn;
use Core\Control\EntityGrid\EntityIconColumn;
use Core\Control\EntityGrid\EntityFunctionColumn;
use Core\Entity\Entity\Argument;
use Core\Control\TabStrip\TabStrip;
use Core\Control\VTab\VTab;
use Core\Control\Libelle\Libelle;
use Core\Control\RangeBox\RangeBox;
use Core\Control\Upload\Upload;
use Core\Control\Link\Link;

class RenderControl extends Form {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "") {
        $this->Core = Core::getInstance();
    }

    /**     *
     * Render the html form
     */
    function Render($params = "") {

        switch ($params) {
            case "TextBox" :
                return $this->RenderTextBox();
                break;
            case "TextArea" :
                return $this->RenderTextArea();
                break;
            case "PassWord" :
                return $this->RenderPassWord();
                break;
            case "NoteBox" :
                return $this->RenderNoteBox();
                break;
            case "Link" :
                return $this->RenderLink();
                break;

            case "CheckBox" :
                return $this->RenderCheckBox();
                break;
            case "AutoCompleteBox" :
                return $this->RenderAutoCompleteBox();
                break;
            case "Button" :
                return $this->RenderButton();
                break;
            case "Captcha" :
                return $this->RenderCaptcha();
                break;
            case "CheckBox" :
                return $this->RenderCheckBox();
                break;
            case "ListCheckBox" :
                return $this->RenderListCheckBox();
                break;
            case "RadioButton" :
                return $this->RenderRadioButton();
                break;
            case "ListRadio" :
                return $this->RenderListRadio();
                break;
            case "DateBox" :
                return $this->RenderDateBox();
                break;
            case "EmailBox" :
                return $this->RenderEmailBox();
                break;
            case "NumericBox" :
                return $this->RenderNumericBox();
                break;
            case "ListBox" :
                return $this->RenderListBox();
                break;
            case "EntityListBox" :
                return $this->RenderEntityListBox();
                break;
            case "EntityGrid" :
                return $this->RenderEntityGrid();
                break;
            case "TabStrip" :
                return $this->RenderTabStrip();
                break;
            case "VTab" :
                return $this->RenderVTab();
                break;
            case "Dialog" :
                return $this->RenderDialog();
                break;
            case "RangeBox":
                return $this->RenderRangeBox();
                break;
            case "Upload":
                return $this->RenderUpload();
                break;
            case "RichText":
                return $this->RenderRichText();
                break;
        }
    }

    function RenderTextBox() {
        $textBox = new TextBox("tb");
        $textBox->PlaceHolder = "Enter your text";
        return $textBox->Render();
    }

    function RenderPassWord() {
        $textBox = new PassWord("tb");
        return $textBox->Render();
    }

    function RenderNoteBox() {
        $textBox = new NoteBox("nb");

        $script = "<script>NoteBox.Init('nb')</script>";

        return $textBox->Render() . $script;
    }

    function RenderTextArea() {
        $textBox = new TextArea("tb");
        $textBox->PlaceHolder = "Enter your text";
        return $textBox->Render();
    }

    function RenderAutoCompleteBox() {
        $entityBox = new AutoCompleteBox("autoComplete", $this->Core);
        $entityBox->Entity = "Apps/Mooc/Entity/MoocMooc";
        $entityBox->Methode = "SearchMooc";
        $entityBox->PlaceHolder = "Enter your search";
        return $entityBox->Render();
    }

    function RenderButton() {
        $button = new Button();
        $button->CssClass = "btn btn-primary";
        $button->Value = $this->Core->GetCode("Base.Save");

        $buttonInfo = new Button();
        $buttonInfo->CssClass = "btn btn-info";
        $buttonInfo->Value = $this->Core->GetCode("Base.Save");

        $buttonWarning = new Button();
        $buttonWarning->CssClass = "btn btn-warning";
        $buttonWarning->Value = $this->Core->GetCode("Base.Save");

        return $button->Render() . $buttonInfo->Render() . $buttonWarning->Render();
    }

    function RenderCaptcha() {
        $Captcha = new Captcha();
        return $Captcha->Render();
    }

    function RenderCheckBox() {
        $checkBox = new CheckBox("cb");
        $checkBox->Value = "TOTO";

        $checkBoxWidthLibelle = new CheckBox("cb2");
        $checkBoxWidthLibelle->Libelle = "My label";

        return $checkBox->Render() . "<br/>" . $checkBoxWidthLibelle->Render();
    }

    function RenderListCheckBox() {
        $listCb = new ListCheckBox("lstChBox");
        $listCb->Add("Apple", 1);
        $listCb->Add("Orange", 2);
        $listCb->Add("Banana", 2);

        return $listCb->Render();
    }

    function RenderRadioButton() {
        $checkBox = new RadioButton("cb");
        $checkBox->Value = "TOTO";

        $checkBoxWidthLibelle = new RadioButton("cb2");
        $checkBoxWidthLibelle->Libelle = "My label";

        return $checkBox->Render() . "<br/>" . $checkBoxWidthLibelle->Render();
    }

    function RenderListRadio() {
        $listCb = new ListRadio("lstChBox");
        $listCb->Add("Apple", 1);
        $listCb->Add("Orange", 2);
        $listCb->Add("Banana", 2);

        return $listCb->Render();
    }

    function RenderListBox() {
        $listCb = new ListBox("lstBox");
        $listCb->Add("Apple", 1);
        $listCb->Add("Orange", 2);
        $listCb->Add("Banana", 2);

        return $listCb->Render();
    }

    function RenderDateBox() {
        $dateBox = new DateBox("db");
        $dateBox->Style = "Width:40%;display:inline-block;";

        $dateTimeBox = new DateTimeBox("db");
        $dateTimeBox->Style = "Width:40%;display:inline-block;";

        return $dateBox->Render() . "" . $dateTimeBox->Render();
    }

    function RenderEmailBox() {
        $emailBox = new EmailBox("db");
        return $emailBox->Render();
    }

    function RenderNumericBox() {
        $emailBox = new NumericBox("db");
        return $emailBox->Render();
    }

    function RenderRangeBox() {
        $emailBox = new RangeBox("db");
        return $emailBox->Render();
    }

    function RenderLink(){
        $emailBox = new Link("the link", "/Home");
        return $emailBox->Render();
    }
    
    function RenderUpload() {
        $emailBox = new Upload("db");
        return $emailBox->Render();
    }
    
    function RenderEntityListBox() {
        $view = "";

        $lstMooc = new EntityListBox("lstMooc", $this->Core);
        $lstMooc->Entity = "\Apps\Mooc\Entity\MoocMooc";
        $lstMooc->AddField("Name");

        $view .= "<br/><label>Simple EntityListBox</label>";
        $view .= $lstMooc->Render();

        $lstMoocFilter = new EntityListBox("lstFilterMooc", $this->Core);
        $lstMoocFilter->Entity = "\Apps\Mooc\Entity\MoocMooc";
        $lstMoocFilter->Filter = true;
        $lstMoocFilter->AddField("Name");

        $view .= "<br/><label>Search EntityListBox</label>";
        $view .= $lstMoocFilter->Render();

        $lstMoocMulti = new EntityListBox("lstMoocMulti", $this->Core);
        $lstMoocMulti->Entity = "\Apps\Mooc\Entity\MoocMooc";
        $lstMoocMulti->Multiple = true;
        $lstMoocMulti->AddField("Name");

        $view .= "<br/><label>Multipl EntityListBox</label>";
        $view .= $lstMoocMulti->Render();

        return $view;
    }

    function RenderEntityGrid() {
        $gdMooc = new EntityGrid("gdMooc", $this->Core);
        $gdMooc->Entity = "Apps\Mooc\Entity\MoocMooc";
        //$gdMooc->AddArgument(new Argument("Apps\Mooc\Entity\MoocMooc", "MoocId", EQUAL, $Mooc->IdEntite));
        $gdMooc->AddOrder("Id Desc");

        $gdMooc->App = "Mooc";
        $gdMooc->Action = "GetTabMooc";

        $btnAdd = new Button(BUTTON, "btnAddMooc");
        $btnAdd->Value = $this->Core->GetCode("Mooc.AddMooc");

        $gdMooc->AddButton($btnAdd);

        $gdMooc->AddColumn(new EntityFunctionColumn("Image", "GetImage"));
        $gdMooc->AddColumn(new EntityColumn("Name", "Name"));

        $commentPlugin = \Apps\EeApp\Helper\AppHelper::GetPluginType($this->Core, "Mooc", "Comment");

        if ($commentPlugin != null) {
            $gdMooc->AddColumn(new EntityFunctionColumn("Comments", "GetComments"));
        }

        $gdMooc->AddColumn(new EntityIconColumn("Action",
                        array(array("EditIcone", "Mooc.EditMooc", "testEdit"),
                    array("DeleteIcone", "Mooc.DeleteMooc", "testDelete"),
                        )
        ));

        $view = $gdMooc->Render();
        $view .= "<script> EntityGrid.Initialise('gdMooc');";
        $view .= "Event.AddById('btnAddMooc', 'click',  testEdit);";
        $view .= "function testEdit(moocId){Animation.Notify(\"You have selecter the tutoriel width id \" +moocId );}";
        $view .= "function testDelete(moocId){Animation.Confirm(\"Do you want to delete the mooc id ? \" +moocId, function(){Animation.Notify(\"Its a demo\");} );}";

        $view .= "</script>";

        return $view;
    }

    function RenderTabStrip() {

        $tabStrip = new TabStrip("tb");
        $tabStrip->AddTab("Item 1", "Content of Item 1");
        $tabStrip->AddTab("Item 2", "Content of item 2, Lorme Ipsus ");
        $tabStrip->AddTab("Item 3", "Content of item 3");

        return $tabStrip->Render();
    }

    function RenderVTab() {

        $tabStrip = new VTab("tb");
        $tabStrip->AddTab("Item 1", new Libelle("<h1 style='border:none'>Content of Item 1</h1><p> Lorme Ipsus</p>"));
        $tabStrip->AddTab("Item 2", new Libelle("<h1 style='border:none'>Content of Item 2</h1><p>Content of Item 2, Lorme Ipsus </p>"));
        $tabStrip->AddTab("Item 3", new Libelle("<h1 style='border:none'>Content of item 3</h1>"));

        return $tabStrip->Render();
    }

    function RenderDialog() {
        $view = new View(__DIR__ . "/View/dialog.tpl", $this->Core);

        return $view->Render();
    }

    function RenderRichText() {
          $view = new View(__DIR__ . "/View/richText.tpl", $this->Core);

        return $view->Render();
    }
}

class Underline extends BaseTool {
    getIcone() {
        return "<u>u</u>";
    }

    getName() {
        return Language.GetCode("Base.TextRichEditorUndeline");
    }
    execute(e) {
        e.preventDefault();
        this.applyStyle("u");
        super.execute();
    }
}


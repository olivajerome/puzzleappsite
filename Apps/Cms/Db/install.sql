CREATE TABLE IF NOT EXISTS `CmsCms` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`Name` VARCHAR(200)  NULL ,
`Description` TEXT  NULL ,
`UserId` INT  NULL ,
`Style` TEXT  NULL ,
`AppName` VARCHAR(200)  NULL ,
`AppId` INT  NULL ,
`EntityName` VARCHAR(200)  NULL ,
`EntityId` INT  NULL ,
PRIMARY KEY (`Id`),
CONSTRAINT `ee_user_CmsCms` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `CmsPage` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`CmsId` INT  NULL ,
`Name` VARCHAR(200)  NULL ,
`Title` VARCHAR(200)  NULL ,
`Description` TEXT  NULL ,
`Content` TEXT  NULL ,
PRIMARY KEY (`Id`),
CONSTRAINT `CmsCms_CmsPage` FOREIGN KEY (`CmsId`) REFERENCES `CmsCms`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

INSERT INTO CmsCms(`UserId`, `Name`, `Description`) VALUE( 1, "Premier Cms", "Gestion des pages de base du site");
INSERT INTO CmsPage(`CmsId`, `Name`, `Title`, `Description`, `Content` ) VALUE( 1, "Home", "Home","Home", "Home");

CREATE TABLE IF NOT EXISTS `CmsContainer` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`Name` TEXT  NOT NULL,
`Code` TEXT  NOT NULL,
`Position` INT  NOT NULL,
`PageId` INT NULL,
`Style` TEXT  NULL,
`CssClass` TEXT  NULL,
PRIMARY KEY (`Id`),
CONSTRAINT `CmsPage_CmsContainer` FOREIGN KEY (`PageId`) REFERENCES `CmsPage`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `CmsBlockType` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`Name` TEXT  NOT NULL,
`Code` TEXT  NOT NULL,
PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`;

CREATE TABLE IF NOT EXISTS `CmsBlock` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`TypeId` INT  NOT NULL,
`ParentId` INT  NOT NULL,
`Position` INT  NOT NULL,
`Size` INT  NOT NULL,
`Style` TEXT  NULL,
`CssClass` TEXT  NULL,
`Content` TEXT  NULL,
`Widget` TEXT  NULL,
PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`;

INSERT INTO CmsContainer(`Name`, `Code`, `Position`) VALUE( "Header", "Header", "1");
INSERT INTO CmsContainer(`Name`, `Code`,`Position`) VALUE( "Body","Body", "2");
INSERT INTO CmsContainer(`Name`, `Code`,`Position`) VALUE( "Footer","Footer", "3");

INSERT INTO CmsBlockType(`Name`, `Code`) VALUE( "Text","Text");
INSERT INTO CmsBlockType(`Name`, `Code`) VALUE( "Image","Image");
INSERT INTO CmsBlockType(`Name`, `Code`) VALUE( "Link","Link");
INSERT INTO CmsBlockType(`Name`, `Code`) VALUE( "Menu","Menu");
INSERT INTO CmsBlockType(`Name`, `Code`) VALUE( "Widget","Widget");

CREATE TABLE IF NOT EXISTS `CmsBlockItem` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`BlockId` INT  NULL ,
`Name` TEXT  NULL ,
`Url` TEXT  NULL ,
PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 



 
<?php

/*
 * PuzzleApp
 * Webemyos
 * J�r�me Oliva
 * GNU Licence
 */

namespace Apps\Ide\Helper ;

use Apps\Ide\Ide;
use Core\Utility\File\File;


class WidgetHelper
{
    /**
     * Cr�e une entity
     * Fichier php et fichier sql
     * 
     * @param type $core
     */
    public static function CreateWidget($core, $name, $projet)
    {
    
      //Creation du repertoire du projet
      $destination = Ide::$Destination."/".$projet."/Widget/".$name;
     
      File::CreateDirectory(Ide::$Destination."/".$projet."/Widget");
                
        if(!file_exists($destination))
        {
            //Creation du repertoire
             File::CreateDirectory($destination);
     
             File::CreateDirectory($destination . "/View");
            
            //Copie des fichiers
            copy(Ide::$Directory. '/Modele/Widget/XXX.php', $destination."/".$name.".php");
            copy(Ide::$Directory. '/Modele/Widget/XXX.js', $destination."/".$name.".js");
            copy(Ide::$Directory. '/Modele/Widget/View/index.tpl', $destination."/View/index.tpl");
            
            
            //Remplace les noms
            self::ReplaceNameBlock($destination."/".$name.".php", $name, $projet);
            self::ReplaceNameBlock($destination."/".$name.".js", $name, $projet);
            
            //Enregistre le module dans l'application
            //self::RegisterBlock($core, $name, $projet);
            
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
    * Remplace touts les noms des applications
    * */
    public static function ReplaceNameBlock($file, $name, $projet)
    {
           $content = File::GetFileContent($file);
           $content = str_replace("XXX", $name."", $content);
           $content = str_replace("YYY", $projet, $content);
           

           //Enregistrement
           File::SetFileContent($file, $content);
    }
}

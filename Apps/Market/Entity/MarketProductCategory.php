<?php 
/*
* PuzzleApp
* Webemyos
* Jérôme Oliva
* GNU Licence
*/

namespace Apps\Market\Entity;
use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class MarketProductCategory extends Entity  
{
	//Constructeur
	function __construct($core)
	{
		//Version
		$this->Version ="2.0.0.0"; 

		//Nom de la table 
		$this->Core=$core; 
		$this->TableName="MarketProductCategory"; 
		$this->Alias = "MarketProductCategory"; 

		$this->ProductId = new Property("ProductId", "ProductId", NUMERICBOX,  false, $this->Alias); 
		$this->CategoryId = new Property("CategoryId", "CategoryId", NUMERICBOX,  false, $this->Alias); 

		//Creation de l entité 
		$this->Create(); 
	}
}
?>
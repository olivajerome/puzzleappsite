<?php

namespace Apps\Cms\Widget\Library;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;

class Library extends Form {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "") {
        $this->Core = $core;
        $this->Data = $data;

        $this->Init($data);
    }

    /***
     * Get the Images
     */
    function GetImages($apps){
        $images = array();
        $directory = "Data/Apps/Cms/" ;
       
            if (file_exists($directory)) {

                $fd = dir($directory);
                $html = "";

                while ($file = $fd->read()) {
                    if (($file != ".") && ($file != "..")) {

                        $images[] = $this->Core->GetPath("/" .$directory . "/" . $file);
                    }
                }
                $fd->close();
            }
        
        return $images;
    }
    
    /***
     * Render a container whose contains all image of the CMS
     */
    function GetImagesBlock($selectedImage, $apps = "Cms") {

        $images = $this->GetImages($apps);
        $view = "<div class='cmsLibrary row'>";

        foreach ($images as $image) {

            if (self::isSameImage($image, $selectedImage)) {
                $class = "selected";
            } else {
                $class = "";
            }

            $view .= "<div class='imgContainer col-md-3 $class'>";
            $view .= "<img class='imgLib'  src='" . $image . "' />";
            $view .= "</div>";
        }

        $view .= "</div>";

        return $view;
    }

    /**
     * Compare image
     */
    function isSameImage($imageSource, $imageSearch){
       
        $imgSource = explode('/', $imageSource);
        $imgSourceName = $imgSource[count($imgSource) - 1];
        
        $imgSearch = explode('/', $imageSearch);
        $imgSearchName = $imgSearch[count($imgSearch) - 1];
        
        return ($imgSourceName ==  $imgSearchName);
    }
    
    /**
     * Initialisation 
     */
    function Init($data) {
        $this->form = new Form("LibraryForm", $data);

        $this->form->SetView(__DIR__ . "/View/index.tpl", $this->Core);
        $this->form->SetAction("/View/index.tpl");

        $this->form->Add(array("Type" => "Hidden",
            "Id" => "Id",
        ));
        
        $this->form->Add(array("Type" => "TextBox",
            "Id" => "Name",
            "Validators" => ["Required"]
        ));
        
        $this->form->Add(array("Type" => "Button",
            "Id" => "btnSave",
            "Value" => "Save",
            "CssClass" => "btn btn-primary"
        ));
    }

    /*     * *
      Render the Html
     */

    function Render() {
        return $this->form->Render();
    }

    /*     * *
     * Validate the data
     */

    function Validate($data = "") {
        $errors = $this->form->Validate($this->Data);

        return $errors;
    }

    /*     * *
     * Load Control with the Entity
     */

    function Load($entity) {
        $this->form->Load($entity);
    }

    /*     * *
     * Load The Entity
     */

    function Populate($entity) {
        
    }

    /*     * *
     * Gestion des erreurs
     */

    function Error() {
        $errors[] = $this->form->errors;

        return json_encode(array("statut" => "Error", "message" => $errors));
    }
}

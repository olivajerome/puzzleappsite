<section class="container">

    <h1>{{GetCode(Shop.Category)}} :
        {{Category->Name->Value}}
    </h1>
    
    {{foreach Products}}
        
        <div class='col-md-4'>
            <div class='block'>
                <a href='{{GetPath(/Shop/Product/{{element->Code->Value}})}}'>
                    <div class='bordered clickable' >

                       <div class='col-md-12' style='height:350px;background-size: cover;background-repeat: no-repeat;background-position: center;background-image:url({{element->GetImageMiniPath()}})'>   
                       </div> 
                       <h2>{{element->Name->Value}}</h2>
                       <div class='center'>
                        <h3>{{element->Price->Value}} &euro;</h3>
                       </div>

                       <p>{{element->Description->Value}}</p>
                    </div>
                </a>
            </div> 
        </div>
    
    {{/foreach Products}}
    
</section>

var AnnonceResponseForm = function(){};

/***
 * Init the form
 * @returns {undefined}
 */
AnnonceResponseForm.Init = function(){
    Event.AddById("btnSave", "click", AnnonceResponseForm.SaveElement);
};

/***
* Get the Id of element 
*/
AnnonceResponseForm.GetId = function(){
    return Form.GetId("AnnonceResponseFormForm");
};

/*
* Save the Element
*/
AnnonceResponseForm.SaveElement = function(e){
   
    if(Form.IsValid("AnnonceResponseFormForm"))
    {

    let data = "Class=Annonce&Methode=AddResponse&App=Annonce";
        data +=  Form.Serialize("AnnonceResponseFormForm");
        data +=  "&AnnonceId=" + Dom.GetById("annonceId").value;

        Animation.AddLoading("AnnonceResponseFormForm");
        Animation.Hide("AnnonceResponseFormForm");
  
        Request.Post("Ajax.php", data).then(data => {

        Animation.Show("AnnonceResponseFormForm");
        Animation.RemoveLoading("AnnonceResponseFormForm");
       
            data = JSON.parse(data);

            if(data.status == "error"){
         
                Form.RenderError("AnnonceResponseFormForm", data.data.message);
            } else{

               Animation.Load("AnnonceResponseFormForm" , data.data );
            }
        });
    }
};


<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Notes;

use Core\Core\Core;
use Core\Core\Request;
use Core\Core\Response;

use Apps\Base\Base;
use Apps\Notes\Module\Admin\AdminController;

class Notes extends Base {

    /**
     * Author and version
     * */
    public $Author = 'Webemyos';
    public $Version = '1.0.0.0';

    /**
     * Constructeur
     * */
    function __construct() {
        $this->Core = Core::getInstance();
        parent::__construct($this->Core, "Notes");
    }

    /**
     * Set public route
     */
    function GetRoute($routes = "") {
        $this->Route->SetPublic(array());

        return $this->Route;
    }

    /*     * *
     * Execute action after install
     */

    function PostInstall() {
        \Apps\EeApp\Helper\AppHelper::UpdateAppParams($this->Core,
                "Notes",
                $this->Version,
                "", // Add Description of the app
                0, // Set 1 if the App have a AdminWidgetDashBoard
                0  // Set 1 if the App have a Member Module
        );
    }

    /**
     * Start Admin
     */
    function Run() {
        echo parent::RunApp($this->Core, "Notes", "Notes");
    }

    /***
     * Add/Update a note
     */
    function SaveNote() {
        $adminController = new AdminController($this->Core);
        $result = $adminController->SaveNote(Request::GetPosts());
        
        if($result === true){
            return Response::Success(array("tabPostIt" => $adminController->GetTabTabNote(), "tabGrid"=>$adminController->GetTabNote()));
        } else {
            return Response::Error(array("message" => $result));
        }
    }

    /*     * *
     * Run member Controller
     * 
     * /
      /*function RunMember(){
      $memberController = new MemberController($this->Core);
      return $memberController->Index();
      } */

    /**
     * Get The siteMap 
     */
    /* public function GetSiteMap(){
      return SitemapHelper::GetSiteMap($this->Core);
      } */

    /*     * *
     * Get the widget
     */
     public function GetWidget($type = "", $params = "") {

        switch ($type) {
            case "LastNote" :
            $widget = new \Apps\Notes\Widget\LastNotes\LastNotes($this->Core);
            break;
      }

      return $widget->Render();
      } 

    /*     * *
     * Get Forum Addable widget
     */
     public function GetListWidget(){
      return array(array("Name" => "LastNote",
      "Description" => $this->Core->GetCode("Notes.DescriptionLastNote")),
      );
      } 
}

class Stroke extends BaseTool {
    getIcone() {
        return "<s>S</s>";
    }

    getName() {
        return Language.GetCode("Base.TextRichEditorStroke");
    }
    execute(e) {
        e.preventDefault();
        this.applyStyle("s");
        super.execute();
    }
}


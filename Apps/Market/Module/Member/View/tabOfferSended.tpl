<section>
    {{foreach Offers}}
    
    <div class='block blockOffer' id='{{element->IdEntite}}' >
        <div class='col-md-4'>
            {{Apps\Market\Decorator\OfferDecorator->RenderImageProduct(element)}}
        </div>
        <div class='col-md-8'>
            <div>
                <label>{{GetCode(Market.DateOffer)}}</label>
                {{element->DateCreated->Value}}
            </div>
            <div>
                <label>{{GetCode(Market.Price)}}</label>
                {{element->Price->Value}} &nbsp;
            </div>
        </div>  
    
        <div class='col-md-12 center' id='offerStatus-{{element->IdEntite}}'>
            {{Apps\Market\Decorator\OfferDecorator->RenderButtonStatut(element)}}
        </div>
    </div>
  
    {{/foreach Offers}}
    
</section>

var AnnonceAnnonceForm = function(){};

/***
 * Init the form
 * @returns {undefined}
 */
AnnonceAnnonceForm.Init = function(){
    Event.AddById("btnSave", "click", AnnonceAnnonceForm.SaveElement);
};

/***
* Get the Id of element 
*/
AnnonceAnnonceForm.GetId = function(){
    return Form.GetId("AnnonceAnnonceFormForm");
};

/*
* Save the Element
*/
AnnonceAnnonceForm.SaveElement = function(e){
   
    if(Form.IsValid("AnnonceAnnonceFormForm"))
    {

    let data = "Class=Annonce&Methode=SaveAnnonce&App=Annonce";
        data +=  Form.Serialize("AnnonceAnnonceFormForm");
        data += "&CategoryId=" + Dom.GetById("categoryId").value;
  
        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.status == "error"){
                Form.RenderError("AnnonceAnnonceFormForm", data.data.message);
            } else{
                Dialog.Close();
                Animation.Notify(Language.GetCode("Annonce.SavedWaitPublish"));
                Form.SetId("AnnonceAnnonceFormForm", data.data.Id);
            }
        });
    }
};


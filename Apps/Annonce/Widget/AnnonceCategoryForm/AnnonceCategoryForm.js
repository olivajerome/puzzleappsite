var AnnonceCategoryForm = function(){};

/***
 * Init the form
 * @returns {undefined}
 */
AnnonceCategoryForm.Init = function(){
    Event.AddById("btnSave", "click", AnnonceCategoryForm.SaveElement);
};

/***
* Get the Id of element 
*/
AnnonceCategoryForm.GetId = function(){
    return Form.GetId("AnnonceCategoryFormForm");
};

/*
* Save the Element
*/
AnnonceCategoryForm.SaveElement = function(e){
   
    if(Form.IsValid("AnnonceCategoryFormForm"))
    {

    let data = "Class=Annonce&Methode=SaveCategory&App=Annonce";
        data +=  Form.Serialize("AnnonceCategoryFormForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.status == "error"){
                Form.RenderError("AnnonceCategoryFormForm", data.data.message);
            } else{

                Form.SetId("AnnonceCategoryFormForm", data.data.Id);
                Annonce.ReloadCategory(data.data);    
                Dialog.Close();
            }
        });
    }
};


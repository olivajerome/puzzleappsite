<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\EeApp\Helper;

use Core\Core\Core;
use Core\Utility\File\File;
use Apps\EeApp\Entity\EeAppApp;
use Apps\EeApp\Helper\UploadHelper;

class VersionningHelper {
   
     /*     * *
     * Met à jour le framework
     */

    public static function UpdateFramework($core) {
        return UploadHelper::DoUpdateFramework($core);
    }
   
    /****
     * Update à App
     */
    public static function UpdateApp($core, $app){
        return UploadHelper::DoUpdateApp($core, $app);
    }

    /***
     * Obtien tle store des applications
     */
    public static function GetAppStore($core){
        $url = $core->Config->GetKey("puzzleAppUrl") . "Api/GetAppStore";
        return  json_decode(file_get_contents($url));
    }
}

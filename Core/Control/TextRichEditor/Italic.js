class Italic extends BaseTool {
    getIcone() {
        return "I";
    }

    getName() {
        return Language.GetCode("Base.TextRichEditorBoldItalic");
    }
    
    execute(e) {
        e.preventDefault();
        this.applyStyle("i");
        super.execute();
    }
}

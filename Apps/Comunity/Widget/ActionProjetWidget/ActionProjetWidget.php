<?php

namespace Apps\Comunity\Widget\ActionProjetWidget;

use Core\View\View;
use Core\View\ElementView;
use Apps\Comunity\Helper\ComunityHelper;

class ActionProjetWidget
{
    
    /***
     * 
     */
    function __construct($core, $projetId)
    {
        $this->Core= $core;
        $this->ProjetId = $projetId;
    }
  
    /*
     * Affiche le widget
     */
    function Render()
    {
        $view = new View(__DIR__."/View/index.tpl", $this->Core);

        if(ComunityHelper::UserFollow($this->Core, $this->ProjetId ))
        {
            $view->AddElement(new ElementView("state", "Disabled"));
            $view->AddElement(new ElementView("btnFollowText", "Vous suivez"));
        }
        else
        {
            $view->AddElement(new ElementView("state", ""));
            $view->AddElement(new ElementView("btnFollowText", "Suivre"));
        }
        
        if(ComunityHelper::UserParticipate($this->Core, $this->ProjetId ))
        {
            $view->AddElement(new ElementView("stateParticipate", "Disabled"));
            $view->AddElement(new ElementView("btnParticipateText", "Vous participez"));
        }
        else
        {
            $view->AddElement(new ElementView("stateParticipate", ""));
            $view->AddElement(new ElementView("btnParticipateText", "Participer"));
        }
        return $view->Render();
    }
    
    /***
     * Plmus d'action sur un projet
     */
    function GetMoreAction($projetId)
    {
        $html ="<ul>";
        
        if(ComunityHelper::UserFollow($this->Core, $projetId ))
        {
            $html .= "<li id='unFollow' ><i class='fa fa-sign-out' ></i>Ne plus suivre</li>";
        }
        
        if(ComunityHelper::UserParticipate($this->Core, $projetId ))
        {
            $html .= "<li id='unParticipate' ><i class='fa fa-sign-in'></i>Ne plus participer</li>";
        }
        
        //TODO RAJOUTER LES DROITS AMBASSADEUR OU CREATEUR DU PROJET
        $html .= "<li id='modifyProjet' ><i class='fa fa-edit'>&nbsp;</i>Modifier</li>";
        $html .= "<li id='deleteProjet' ><i class='fa fa-trash'>&nbsp;</i>Supprimer</li>";
        $html .= "<li id='archiveProjet' ><i class='fa fa-archive'>&nbsp;</i>Archiver</li>";
        $html .= "<li id='pinProjet' ><i class='fa fa-bookmark'>&nbsp;</i> Epingler à la page d'accueil</li>";
        
        $html .= "</ul>";
        
        return $html;
        
        
    }
    
}
<div>
    
    <div class='col-md-2'>
        <ul>
            <li id='editParameter'><i class='fa fa-gears' ></i>{{GetCode(Carousel.Parameters)}}</li> 
            <li><i class='fa fa-list'></i>{{GetCode(Carousel.Item)}}
                <i class='fa fa-plus' id='btnAddItem' title='{{GetCode(Carousel.AddItem)}}'></i>
            
            </li>
            
            <div id='listItem'>
            {{foreach Items}}
              <li class='Item editItem' id='{{element->IdEntite}}'>
                  {{element->Name->Value}}
                  <i class='fa fa-trash removeItem'></i>
              </li>
            {{/foreach Items}}
            </div>
            
            
        </ul>
    </div>
    
    <div class='col-md-8'>
        <div id='formTool'>
            
            {{form->Open()}}
            {{form->Error()}}
            {{form->Success()}}

            {{form->Render(Id)}}
               
                <div>
                    <label>{{GetCode(Carousel.Name)}}</label> 
                    {{form->Render(Name)}}
                </div>
                <div>
                    <label>{{GetCode(Carousel.Type)}}</label> 
                    {{form->Render(Type)}}
                </div>
                <div>
                    <label>{{GetCode(Carousel.Entity)}}</label> 
                    {{form->Render(Entity)}}
                </div>
                <div>
                    <label>{{GetCode(Carousel.Parameters)}}</label> 
                    {{form->Render(Parameters)}}
                </div>
                <div>
                    <label>{{GetCode(Carousel.Description)}}</label> 
                    {{form->Render(Description)}}
                </div>
               
                <div class='center marginTop' >   
                    {{form->Render(btnSaveCarousel)}}
                </div>  

            {{form->Close()}}
        </div>
        
        <div id='itemTool'></div>
        
    </div>
</div>
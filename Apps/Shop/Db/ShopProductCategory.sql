CREATE TABLE IF NOT EXISTS `ShopProductCategory` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`ProductId` INT  NULL ,
`CategoryId` INT  NULL ,
PRIMARY KEY (`Id`),
CONSTRAINT `ShopCategory_ShopProductCategory` FOREIGN KEY (`CategoryId`) REFERENCES `ShopCategory`(`Id`),
CONSTRAINT `ShopProduct_ShopProductCategory` FOREIGN KEY (`ProductId`) REFERENCES `ShopProduct`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
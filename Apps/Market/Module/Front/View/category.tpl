<section class='container'>
    <h1>{{GetCode(Market.ProductOfCategory)}}
          {{Category->Name->Value}}  
    </h1>

    
    <div>
        {{foreach Products}}

        <div class='col-md-4'>
                <div class='block' id='{{element->IdEntite}}'>
                    <span style="display:block; width:100%; height: 150px; background-size: cover;background-image: url({{element->GetImagePath()}})">
                    </span>
                    <h2>{{element->Title->Value}}</h2>
                    <p>{{element->Description->Value}}</p>

                    <div class='center'>
                     <a href='{{GetPath(/Market/Product/{{element->Code->Value}})}}' class='btn btn-primary discovert' >{{GetCode(Market.Discover)}}</a>
                    </div>
                </div>
        </div>

        {{/foreach Products}}
    </div>
</section>
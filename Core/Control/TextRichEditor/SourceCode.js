class SourceCode extends BaseTool {

    constructor(editor){
        super(editor);
        this.activated = false;
        this.name = "SourceCode";
    }

    getIcone() {
        return "<i class='fa fa-edit'></i>";
    }

    getName() {
        return Language.GetCode("Base.TextRichEditorShowSourceCode");
    }
    execute(e) {
        
        let instance = this;
        let inputArea = instance.editor.inputArea;
        let elements = inputArea.querySelectorAll("div,h1,h2,h3, p, img");

        if(instance.activated){
            
            instance.activated = false;
            let codeEditor  = document.getElementById("codeEditor");

            inputArea.innerHTML = codeEditor.value.replace(/<code>([\s\S]*?)<\/code>/g, function(match, p1) {
                   // Remplacer les caractères < et > dans le contenu trouvé, puis ajouter les <br> pour les sauts de ligne
                   let transformed = p1
                       .replace(/\n/g, "<br>"); // Remplacer les sauts de ligne par <br>

                   // Retourner le contenu modifié avec les balises <codeHtml></codeHtml>
                   return "<code>" + transformed + "</code>";
                   });;
          
            inputArea.style.display = "";
            inputArea.parentNode.removeChild(codeEditor);

        } else {
            instance.activated = true;

            let codeEditor = document.createElement("textarea");
            codeEditor.id = "codeEditor";
            codeEditor.style.width = "100%";
            codeEditor.style.height = "auto";
            codeEditor.style.minHeight = "250px";
          
            codeEditor.value = inputArea.innerHTML.replace(/<code>([\s\S]*?)<\/code>/g, function(match, p1) {
                   // Remplacer les caractères < et > dans le contenu trouvé, puis ajouter les <br> pour les sauts de ligne
                   let transformed = p1
                       .replace(/<br>/g, "\n"); // Remplacer les sauts de ligne par <br>

                   // Retourner le contenu modifié avec les balises <codeHtml></codeHtml>
                   return "<code>" + transformed + "</code>";
                   });
            
            inputArea.parentNode.appendChild(codeEditor);
            inputArea.style.display = "none";
        }   

      super.execute();
    }
}
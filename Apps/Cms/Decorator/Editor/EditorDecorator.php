<?php 

namespace Apps\Cms\Decorator\Editor;

use Apps\Cms\Entity\CmsBlock;


class EditorDecorator{


    public function __construct($core){
        $this->Core = $core;
    }

    /***
     * Get Editor type block
     */
    public function GetBlock($block){

        switch( $block->TypeId->Value){
            case CmsBlock::TYPE_TEXT : 
                $content = $block->Content->Value != "" ? $block->Content->Value : $this->Core->GetCode("Cms.ClickHereToEnterYourText");      

                return "<div  class='blockContent'>
                       ".$content."  
                         </div>";
            break;
            case CmsBlock::TYPE_IMAGE: 
                $view = "<div class='floatRight' >";
                $view .= "<i class='fa fa-edit bntEditImage' title='".$this->Core->GetCode("Cms.EditImage")."' ></i>";
                $view .= "<i class='fa fa-list btnEditSource' title='".$this->Core->GetCode("Cms.EditSource")."' ></i>";
                $view .= "</div>";
                $view .= "<div class='editContent'>".$block->Content->Value. "</div>"; 
                
                return $view;
            break;
            case CmsBlock::TYPE_LINK :
            case CmsBlock::TYPE_MENU :
                $view = "<div class='".($block->TypeId->Value == 4 ? "menu" : "links")."' id='".$block->IdEntite."'><i class='fa fa-plus btnAddItemMenu floatRight' title='".$this->Core->GetCode("Cms.AddItemMenu")."'></i>" ;
                
                $items = $block->GetItems();

                if(count($items) > 0){
                    foreach($items as $item){
                        $view .= "<span class='linkItem'><a id='".$item->IdEntite."'  class='linkBlock' href='".$this->Core->GetPath("/".$item->Url->Value). "'>" .$item->Name->Value. "</a><i id='".$item->IdEntite."'  class='fa fa-remove btnRemoveLinkItem'></i></span>";
                    }    
                }     

                $view .= "</div>";
                return $view;
                break;
            case CmsBlock::TYPE_WIDGET : 
                return $block->Widget->Value;  
                break;    
        }
    }

    /**
     * Render the block in the front
     */
    public function GetFrontBlock($block){

        $style= $block->Style->Value != "" ? "style='".$block->Style->Value."'" : "";

        switch($block->TypeId->Value){
            
            case CmsBlock::TYPE_TEXT:
            case CmsBlock::TYPE_IMAGE:
                return "<div class='col-md-".$block->Size->Value . " ". $block->CssClass->Value . "' ". $style." >
                <div>".$block->Content->Value."</div>
                </div>"; 
            break;
            case CmsBlock::TYPE_LINK :
            case CmsBlock::TYPE_MENU :
                $view = "<div class='".($block->TypeId->Value == 4 ? "menu" : "links")." ". $block->CssClass->Value . "' ". $style." id='".$block->IdEntite."'" ;
                
                $items = $block->GetItems();

                if(count($items) > 0){
                    foreach($items as $item){
                        //$view .= "<a href='".$this->Core->GetPath("/".  $item->Url->Value). "'>" .$item->Name->Value. "</a>";
                    }    
                }     

                $view .= "</div>";
                return $view;
            case CmsBlock::TYPE_WIDGET :
            
                $widget = explode("-",  $block->Widget->Value);

                if($block->Content->Value != ""){
                    $params = $block->Content->Value;
                } else{
                    $params = "All";
                }
                
                $view .= "\n\r<div class='col-md-" . $block->Size->Value . "  ".$block->CssClass->Value. "   ' style='" . $block->Style->Value . "' >";
                $view .= "{{GetWidget(".$widget[0].",".$widget[1].",".$params.")}}";
                $view .= "</div>";

                return $view;
            break; 

        }
    }
}

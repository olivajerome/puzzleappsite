var UploaderRessource = function(){};

/***
 * Init the form
 * @returns {undefined}
 */
UploaderRessource.Init = function(){
    Event.AddById("btnProposeRessouce", "click", UploaderRessource.ShowAddUserRessource);
};

/***
 * Dialog ad ressource user
 */
UploaderRessource.ShowAddUserRessource = function(){

    Dialog.open('', {"title": Dashboard.GetCode("Downloader.NewRessource"),
    "app": "Downloader",
    "class": "DialogDownloader",
    "method": "ShowAddRessource",
    "type": "left",
    "params" : ""
    });
};
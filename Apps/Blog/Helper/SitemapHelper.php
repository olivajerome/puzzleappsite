<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Blog\Helper;

use Apps\Blog\Entity\BlogCategory;
use Apps\Blog\Entity\BlogArticle;

class SitemapHelper {
    /*     * *
     * Obtient le site map du blog
     */

    public static function GetSiteMap($core, $returnUrl = false) {
        $urlBase = $core->GetPath("");
        $sitemap .= "<url><loc>$urlBase/Blog</loc></url>";
        $urls = "";
      
        $category = new BlogCategory($core);
        $categorys = $category->GetAll();

        foreach ($categorys as $category) {
            $sitemap .= "<url><loc>$urlBase/Blog/Category/" . $category->Code->Value . "</loc></url>";

            $url = $urlBase . "/Blog/Category/" . $category->Code->Value;
            $urls .= '<br/><a target="__blank" href="' . $url . '">' . $url . '</a>';
        }

        $article = new BlogArticle($core);
        $articles = $article->GetAll();

        foreach ($articles as $article) {
            $sitemap .= "<url><loc>$urlBase/Blog/Article/" . $article->Code->Value . "</loc></url>";

            $url = $urlBase . "/Blog/Article/" . $article->Code->Value;
            $urls .= '<br/><a target="__blank" href="' . $url . '">' . $url . '</a>';
        }

        if ($returnUrl == true) {
            return $urls;
        } else {
            return $sitemap;
        }
    }
}

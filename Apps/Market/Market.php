<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Market;

use Core\Core\Core;
use Apps\Base\Base;
use Core\Core\Request;
use Core\Core\Response;
use Apps\Market\Module\Admin\AdminController;
use Apps\Market\Module\Member\MemberController;
use Apps\Market\Module\Front\FrontController;
use Apps\Market\Helper\ProductHelper;
use Apps\Market\Helper\CaracteristiqueHelper;
use Apps\Market\Helper\NotifyHelper;

use Apps\Market\Entity\MarketCategory;
use Apps\Market\Entity\MarketProduct;

class Market extends Base {

    /**
     * Author and version
     * */
    public $Author = 'Webemyos';
    public $Version = '1.0.0.0';

    /**
     * Constructeur
     * */
    function __construct() {
        $this->Core = Core::getInstance();
        parent::__construct($this->Core, "Market");
    }

    /**
     * Set public route
     */
    function GetRoute($routes = "") {
        $this->Route->SetPublic(array("Category", "Product", "MakeOffer"));

        return $this->Route;
    }

    /*     * *
     * Execute action after install
     */

    function PostInstall() {
        \Apps\EeApp\Helper\AppHelper::UpdateAppParams($this->Core,
                "Market",
                $this->Version,
                "Market place between user", // Add Description of the app
                1, // Set 1 if the App have a AdminWidgetDashBoard
                1  // Set 1 if the App have a Member Module
        );

        \Apps\EeApp\Helper\AppHelper::CreateDataDir("Market");
    }

    /*     * *
     * Home page Market
     */

    function Index() {

        $this->Core->MasterView->Set("Title", $this->Core->GetCode("Market.HomePageTitle"));

        $frontController = new FrontController($this->Core);
        return $frontController->Index();
    }

    /**
     * Détail of a category
     */
    function Category($params) {

        $category = new MarketCategory($this->Core);
        $category = $category->GetByCode($params);

        $this->Core->MasterView->Set("Title", $this->Core->GetCode("Market.CategoryPageTitle") . " - " . $category->Name->Value);

        $frontController = new FrontController($this->Core);
        return $frontController->Category($category);
    }

    /*     * *
     * Détail of a product
     */

    function Product($params) {

        $product = new MarketProduct($this->Core);
        $product = $product->GetByCode($params);
        
        $this->Core->MasterView->Set("Title", $this->Core->GetCode("Market.ProductPageTitle") . "- " . $product->Title->Value);

        $frontController = new FrontController($this->Core);
        return $frontController->Product($product);
    }

    /**
     * Start Admin
     */
    function Run() {
        echo parent::RunApp($this->Core, "Market", "Market");
    }

    /*     * *
     * Save market 
     */

    function SaveMarket() {
        $adminController = new AdminController($this->Core);

        if ($adminController->SaveMarket(Request::GetPosts())) {
            return Response::Success(array());
        } else {
            return Response::Error();
        }
    }

    /*     * *
     * Sauvegarde la catégorie
     */

    function SaveCategory() {
        $adminController = new AdminController($this->Core);

        if ($adminController->SaveCategory($this->Core, Request::GetPosts())) {
            $adminController = new AdminController($this->Core);
            return Response::Success($adminController->GetTabCategory());
        } else {
            return Response::Error();
        }
    }

    /*     * *
     * Delete a catégorie
     */

    function DeleteCategory() {
        $adminController = new AdminController($this->Core);

        if ($adminController->DeleteCategory($this->Core, Request::GetPost("categoryId"))) {
            $adminController = new AdminController($this->Core);
            return Response::Success($adminController->GetTabCategory());
        } else {
            return Response::Error();
        }
    }

    /**
     * Sauvegarde la caractéristique
     */
    function SaveCaracteristique() {

        $adminController = new AdminController($this->Core);

        if ($adminController->SaveCaracteristique($this->Core, Request::GetPosts())) {
            $adminController = new AdminController($this->Core);
            return Response::Success($adminController->GetTabCaracteristique());
        } else {
            return Response::Error();
        }
    }

    /*     * *
     * Supprime une caracteristiques
     */

    function DeleteCaracteristique() {

        $adminController = new AdminController($this->Core);

        if ($adminController->DeleteCaracteristique($this->Core, Request::GetPost("caracteristiqueId"))) {

            $adminController = new AdminController($this->Core);
            return Response::Success($adminController->GetTabCaracteristique());
        } else {
            return Response::Error();
        }
    }

    /*     * *
     * Supprime un item d'une caractétistique
     */

    function RemoveCaracteristiqueItem() {
        $adminController = new AdminController($this->Core);

        return $adminController->RemoveCaracteristiqueItem($this->Core, Request::GetPost("itemId"));
    }

    /*     * *
     * Transfert les fichiers dans le Tmp
     */

    function DoUploadFile($idElement, $tmpFileName, $fileName, $action) {

        $directory = "Data/Tmp";

        move_uploaded_file($tmpFileName, $directory . "/" . $idElement . ".jpg");

        echo Response::Success(array("type:" => "IMAGE", "Message" => "OK"));
    }

    /*     * *
     * Run member Controller
     * 
     */

    function RunMember() {
        $memberController = new MemberController($this->Core);
        return $memberController->Index();
    }

    /*     * **
     * Save a product
     */

    function SaveProduct() {
        $memberController = new MemberController($this->Core);

        $result = $memberController->SaveProduct(Request::GetPosts());

        if ($result === true) {
            return Response::Success(array());
        } else {
            return Response::Error($result);
        }
    }
    
    /*     * *
     * Add category to a product
     */

    function AddCategoryProduct() {
        return json_encode(ProductHelper::AddCategoryProduct($this->Core, Request::GetPosts()));
    }

    /*
     * Delete a catégory to a product
     */

    function RemoveCategorieProduct() {
        return ProductHelper::RemoveCategorieProduct($this->Core, Request::GetPost("Id"));
    }

    /*     * *
     * Get the items of  a caracteristiques
     */

    function GetCaracteristiqueItem() {
        return json_encode(CaracteristiqueHelper::GetItemByCaracteristique($this->Core, Request::GetPost("CaracteristiqueId")));
    }

    /*     * *
     * Add a cracteristique to a product
     */

    function AddCaracteristiqueProduct() {
        return json_encode(ProductHelper::AddCaracteristiqueProduct($this->Core, Request::GetPosts()));
    }

    /*     * *
     * Delete a caracteristique product
     */

    function RemoveCaracteristiqueProduct() {
        return ProductHelper::RemoveCaracteristiqueProduct($this->Core, Request::GetPost("Id"));
    }

    /**
     * User send Offer on a Product
     */
    function SendOffer(){
        $memberController = new MemberController($this->Core);

        $result = $memberController->SendOffer(Request::GetPosts());

        if ($result === true) {
            return Response::Success(array());
        } else {
            return Response::Error($result);
        }
    }

    /***
     * Accept the Offer
     */
    function AcceptOffer(){
        
        $memberController = new MemberController($this->Core);
        $offerId = Request::GetPost("OfferId");

        $result = $memberController->AcceptOffer($offerId);

        if ($result === true) {
            return Response::Success(array("status" => $memberController->GetStatus($offerId)));
        } else {
            return Response::Error($result);
        }
    }

    /***
     * Refuse the Offer
     */
    function RefuseOffer(){
        
        $memberController = new MemberController($this->Core);
        $offerId = Request::GetPost("OfferId");

        $result = $memberController->RefuseOffer($offerId);

        if ($result === true) {
            return Response::Success(array("status" => $memberController->GetStatus($offerId)));
        } else {
            return Response::Error($result);
        }
    }    

    /***
     * Save Track Numbe, Track Shipping on product
     */
    function SaveProductShipping(){

        $memberController = new MemberController($this->Core);
        
        $result = $memberController->SaveProductShipping(Request::GetPost());

        if ($result === true) {
            return Response::Success(array("status" => $memberController->GetStatus($offerId)));
        } else {
            return Response::Error($result);
        }
    }

    /***
     * Confirm recept the product
     */
    function ConfirmReceptProduct(){
        $memberController = new MemberController($this->Core);
        
        $offerId = Request::GetPost("OfferId");
         
        $result = $memberController->ConfirmReceptProduct($offerId);

        if ($result === true) {
            return Response::Success(array("status" => $memberController->GetStatus($offerId)));
        } else {
            return Response::Error($result);
        }
    }
    
    /***
     * Confirm no recept the product
     */
    function ConfirmNoReceptProduct(){
        $memberController = new MemberController($this->Core);
        
        $offerId = Request::GetPost("OfferId");
        
        $result = $memberController->ConfirmNoReceptProduct($offerId);

        if ($result === true) {
            return Response::Success(array("status" => $memberController->GetStatus($offerId)));
        } else {
            return Response::Error($result);
        }
    }
    
    /***
     * Detail d'une notification
     */
    public function GetDetailNotify($notify){
       return NotifyHelper::GetDetailNotify($this->Core, $notify);
    }
    
    /**
     * Get The siteMap 
     */
    /* public function GetSiteMap(){
      return SitemapHelper::GetSiteMap($this->Core);
      } */

    /*     * *
     * Get the widget
     */
    public function GetWidget($type = "", $params = "") {

      switch ($type) {
        case "AdminDashboard" :
            $widget = new \Apps\Market\Widget\AdminDashBoard\AdminDashBoard($this->Core);
        break;
        case "LastProduct" :
           $widget = new \Apps\Market\Widget\LastProductWidget\LastProductWidget($this->Core);
         break;
      }

      return $widget->Render();
    }

    /*     * *
     * Get Market Addable widget
     */
    public function GetListWidget(){
      return array(array("Name" => "LastProduct",
      "Description" => $this->Core->GetCode("Market.DescriptionLastProduct")),
      );
      } 
}

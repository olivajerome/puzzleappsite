{{form->Open()}}
{{form->Error()}}
{{form->Success()}}

{{form->Render(Id)}}
    <div>
        <label>{{GetCode(Shop.Code)}}</label> 
        {{form->Render(Code)}}
    </div>

    <div>
        <label>{{GetCode(Shop.Name)}}</label> 
        {{form->Render(Name)}}
    </div>

    <div>
        <label>{{GetCode(Shop.Description)}}</label> 
        {{form->Render(Description)}}
    </div>

    <div>
        <label>{{GetCode(Shop.Price)}}</label> 
        {{form->Render(Price)}}
    </div>
    
    <div>
        {{form->Render(Upload)}}
    </div>

    <div>
       {{form->RenderImage()}}
    </div>
    
    <div class='center marginTop' >   
        {{form->Render(btnSaveProduct)}}
    </div>  


    {{tabProduct}}

{{form->Close()}}
<div class='col-md-4'>
    <div class='block'>
        <i class='fa fa-trash removeWidget' data-app='Shop'></i>
        <i class='fa fa-credit-card title'>&nbsp;{{GetCode(Shop.Shop)}}</i>
        <ul>
            <li><b>{{newCommand}}</b> {{GetCode(Shop.NewCommandToSend)}}</li>
            <li><b>{{toSend}}</b> {{GetCode(Shop.CommandToSend)}}</li>
            <li><b>{{sended}}</b> {{GetCode(Shop.CommandSended)}}</li>
        </ul>
    </div>
</div>

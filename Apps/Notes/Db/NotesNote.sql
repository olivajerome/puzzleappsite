CREATE TABLE IF NOT EXISTS `NotesNote` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`DateCreated` DATE  NOT NULL,
`Text` TEXT  NOT NULL,
`UserId` INT  NOT NULL,
`Visible` INT NULL,
`Status` INT  NOT NULL,
PRIMARY KEY (`Id`),
CONSTRAINT `ee_user_NotesNote` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
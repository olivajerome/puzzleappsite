var Folders = function() {};

/*
* Chargement de l'application
*/
Folders.Load = function(parameter)
{
    Folders.CurrentFolderId = "";
    this.LoadEvent();
};

/*
* Chargement des evenements
*/
Folders.LoadEvent = function()
{
    Dashboard.AddEventAppMenu(Folders.Execute, "", "Folders");
    Dashboard.AddEventWindowsTool("Folders");
    
    Event.AddById("btnAddFolder", "click", function(e){
  
            menuNew = new ContextMenu(e);
            menuNew.App = "Folders";
            menuNew.Methode = "GetNewActionMenu";
            menuNew.Params = "";
            
            menuNew.onLoaded = function () {

                Dashboard.AddEventById("mnAddFolder", "click", function(){
                    ContextMenu.Close();
                    Folders.ShowAddFolder("");
                });
                
                Event.AddById("mnAddFile", "click", function(){
                    ContextMenu.Close();
                    Folders.ShowAddFile("");
                });
                
                Dashboard.AddEventById("setAllView", "click", DiscussAction.SetAllView);
            };

            menuNew.Show();
    
    });
    
    Event.AddByClass("btnDeleteFolder", "click", function(e){
        
        Animation.Confirm(Language.GetCode("Folder.ConfirmDeleteFolder"), function(){
            let container = e.srcElement.parentNode.parentNode;
            let folderId = container.id;
            
               var data = "Class=Folders&Methode=DeleteFolder&App=Folders";
                data += "&folderId=" + folderId;

                Request.Post("Ajax.php", data).then(data => {
                    container.parentNode.removeChild(container); 
                });
        });
    });
    
    Event.AddByClass("btnEditFolder", "click", function(e){
       Folders.ShowAddFolder(e.srcElement.parentNode.parentNode.id);
    });
    
    Event.AddByClass("btnShareFolder", "click", function(e){
        console.log(e.srcElement.parentNode.parentNode.id);
        
          var  menuShare = new ContextMenu(e);
            menuShare.App = "Folders";
            menuShare.Methode = "GetShareMenu";
            menuShare.Params = "&folderId="+ e.srcElement.parentNode.parentNode.id;
         
            menuShare.onLoaded = function () {

                Dashboard.AddEventById("mnAddFolder", "click", function(){
                    ContextMenu.Close();
                    Folders.ShowAddFolder("");
                });
                
                Dashboard.AddEventById("setAllView", "click", DiscussAction.SetAllView);
                Event.AddByClass("deleteSharedFolder", "click", Folders.DeleteUserFolder);
            };

            menuShare.Show();
    });
    
     Event.AddByClass("fa-folder", "click", function(e){
       Folders.CurrentFolderId = e.srcElement.parentNode.id;
       Folders.GetDirectory(e.srcElement.parentNode.id);
    });
    
    Event.AddByClass("previousFolder", "click", function(e){
        Folders.CurrentFolderId = e.srcElement.id;
        Folders.GetDirectory(e.srcElement.id);
    });
    
    
    Event.AddByClass("btnShareFile", "click", function(e){
        
          var  menuShare = new ContextMenu(e);
            menuShare.App = "Folders";
            menuShare.Methode = "GetShareMenuFile";
            menuShare.Params = "&fileId="+ e.srcElement.parentNode.parentNode.id;
         
            menuShare.onLoaded = function () {

                Dashboard.AddEventById("mnAddFile", "click", function(){
                    ContextMenu.Close();
                    Folders.ShowAddFolder("");
                });
                
                Dashboard.AddEventById("setAllView", "click", DiscussAction.SetAllView);
                Event.AddByClass("deleteSharedFolder", "click", Folders.DeleteUserFile);
            };

            menuShare.Show();
    });
    
    
    Event.AddByClass("btnDeleteFile", "click", function(e){
        let fileId = e.srcElement.parentNode.parentNode.id;
    
        Animation.Confirm(Language.GetCode("Folder.ConfirmDeleteFile"), function(){
            
             var data = "Class=Folders&Methode=DeleteFile&App=Folders";
                data += "&fileId=" + fileId;

                Request.Post("Ajax.php", data).then(data => {
                    e.srcElement.parentNode.parentNode.parentNode.removeChild(e.srcElement.parentNode.parentNode);
                 });
            });
        });
        
        
    Event.AddById("btnSharedMe", "click", Folders.LoadSharedWithMe);    
};

/*
* Execute une fonction
*/
Folders.Execute = function(e)
{
    //Appel de la fonction
    Dashboard.Execute(this, e, "Folders");
    return false;
};

/*
*	Affichage de commentaire
*/
Folders.Comment = function()
{
    Dashboard.Comment("Folders", "1");
};

/*
*	Affichage de a propos
*/
Folders.About = function()
{   
    Dashboard.About("Folders");
};

/*
*	Affichage de l'aide
*/
Folders.Help = function()
{
    Dashboard.OpenBrowser("Folders","{$BaseUrl}/Help-App-Folders.html");
};

/*
*	Affichage de report de bug
*/
Folders.ReportBug = function()
{   
    Dashboard.ReportBug("Folders");
};

/*
* Fermeture
*/
Folders.Quit = function()
{
    Dashboard.CloseApp("","Folders");
};

/*
** Ajouter un dossier
*/
Folders.ShowAddFolder = function(folderId){

Dialog.open('', {"title": Dashboard.GetCode("Folders.NewFolder"),
        "app": "Folders",
        "class": "DialogFolder",
        "method": "ShowAddFolder",
        "params": folderId + "&ParentId="+Folders.CurrentFolderId ,
        "type": "right"});
};

/*
* Ajoute un fichier
*/

Folders.ShowAddFile = function(fileId){

    Dialog.open('', {"title": Dashboard.GetCode("Folders.NewFile"),
        "app": "Folders",
        "class": "DialogFolder",
        "method": "ShowAddFile",
        "params": fileId+"&FolderId="+Folders.CurrentFolderId,
        "type": "right"});
};

/*
* Récupére les dossiers et fichiers d'un repertoire
*/
Folders.GetDirectory = function(folderId){
    var data = "Class=Folders&Methode=GetDirectory&App=Folders";
        data += "&folderId=" + folderId;

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);
            Folders.LoadDirectory(data.data.folders, data.folderId, data.data.files );
         });
};


/*
* Charge le repertoire courant avec les dossiers et fichiers
*/
Folders.LoadDirectory = function(folders, folderId, files){

   let root = Dom.GetById("root");
   let html ="";

   html += "<div class='left'><i class='fa fa-arrow-left previousFolder' id ='"+folderId+"'>&nbsp;</i></div>";
    for(var i = 0 ; i < folders.length; i++){

        html += "<div class='col-md-1 folder' id='"+folders[i].IdEntite+"'"+ 
                "title='" + folders[i].Description + "'>"+ 
                " <i class='fa fa-folder fa-2x'>&nbsp;</i>"+ 
                " <div class='button' >"+ 
                "     <i class='fa fa-edit btnEditFolder'>&nbsp;</i>"+ 
                "     <i class='fa fa-user btnShareFolder'>&nbsp;</i>"+ 
                "     <i class='fa fa-trash btnDeleteFolder'>&nbsp;</i>"+ 
                " </div> "+ 
                " <h5>" + folders[i].Name + "</h5> "+
                "  </div>";
    }
    
    for(var i = 0 ; i < files.length; i++){

        html += "<div class='col-md-1 folder' id='"+files[i].IdEntite+"'"+ 
                "title='" + files[i].Description + "'>"+ 
                " <i class='fa fa-file fa-2x'>&nbsp;</i>"+ 
                " <div class='button' >"+ 
                "     <i class='fa fa-edit btnEditFile'>&nbsp;</i>"+ 
                "     <i class='fa fa-user btnShareFile'>&nbsp;</i>"+ 
                "     <i class='fa fa-trash btnDeleteFile'>&nbsp;</i>"+ 
                " </div> "+ 
                " <h5>" + files[i].Name + "</h5> "+
                "  </div>";
    }
    
    root.innerHTML = html;
    
    Folders.LoadEvent();
};

/*
** Selection des utilisateurs
*/
Folders.UserSelected = function(){

    let folderShareId = Dom.GetById("folderShareId");
    let userSelected = document.querySelectorAll("#divResult input[type=checkbox]:checked");
    let users = Array();
    
    for(var i =0; i < userSelected.length; i++){
        users.push(userSelected[i].id);
    }
    
    var data = "Class=Folders&Methode=ShareFolder&App=Folders";
        data += "&folderId=" + folderShareId.value;
        data += "&users=" + users.join(",");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);
            Folders.LoadDirectory(data.data.folders, data.folderId);
         });
         
    Dom.CloseSearch();
    ContextMenu.Close();
};


Folders.UserFileSelected  = function(){

    let folderShareId = Dom.GetById("folderShareId");
    let userSelected = document.querySelectorAll("#divResult input[type=checkbox]:checked");
    let users = Array();
    
    for(var i =0; i < userSelected.length; i++){
        users.push(userSelected[i].value);
    }
    
    var data = "Class=Folders&Methode=ShareFile&App=Folders";
        data += "&fileId=" + folderShareId.value;
        data += "&users=" + users.join(",");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);
            Folders.LoadDirectory(data.data.folders, data.folderId);
         });
         
    Dom.CloseSearch();
    ContextMenu.Close();
};

/*
** Supprime un utilisateur d'ou dossier partagé
*/
Folders.DeleteUserFolder = function(e){
 
    let container = e.srcElement.parentNode;

    var data = "Class=Folders&Methode=DeleteUserFolder&App=Folders";
        data += "&userFolderId=" + container.id;
     
        Request.Post("Ajax.php", data).then(data => {

            container.parentNode.removeChild(e.srcElement.parentNode);
         });
 };
  
/*
** Supprime un utilisateur d'ou fichier partagé
*/
Folders.DeleteUserFile = function(e){
 
    let container = e.srcElement.parentNode;

    var data = "Class=Folders&Methode=DeleteUserFile&App=Folders";
        data += "&userFolderId=" + container.id;
     
        Request.Post("Ajax.php", data).then(data => {

            container.parentNode.removeChild(e.srcElement.parentNode);
         });
 };
 
 /*
 ** Charge les dossiers et fichiers partagé avec moi
 */
 Folders.LoadSharedWithMe = function(){
 
    var data = "Class=Folders&Methode=LoadSharedWithMe&App=Folders";
       
        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);
            Folders.LoadDirectory(data.folders, false, data.files);
    });
 };
 
 Folders.Init = function(){
     
 };
<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Seo;

use Core\Core\Core;
use Apps\Base\Base;
use Apps\Seo\Entity\SeoPage;
use Apps\Seo\Module\Widget\WidgetController;
use Apps\Seo\Helper\PageHelper;
use Core\Core\Request;

class Seo extends Base {

    /**
     * Auteur et version
     * */
    public $Author = 'Webemyos';
    public $Version = '1.0.0';

    /**
     * Constructeur
     * */
    function __construct($core) {
        $this->Core = Core::getInstance();
        parent::__construct($this->Core, "Seo");
    }

    /**
     * Execution de l'application
     */
    function Run() {
        echo parent::RunApp($this->Core, "Seo", "Seo");
    }

    /**
     * Définie les routes publiques
     */
    function GetRoute($routes = "") {
        parent::GetRoute(array());
        return $this->Route;
    }

    /*     * *
     * Fait le lien entre le pages et 
     */

    function SetTitleAndDescription($app, $action) {

        $SeoPage = new SeoPage($this->Core);

        if ($action == "") {
            $pages = $SeoPage->Find("Url = '" . $app . "'");
        } else {

            $pages = $SeoPage->Find("Url = '" . $app . "/" . $action . "'");
        }

        if (count($pages) > 0) {

            $page = $pages[0];

            $this->Core->MasterView->Set("Title", $page->Title->Value);
            $this->Core->MasterView->Set("Description", $page->Description->Value);
        }
    }

    /*     * *
     * Obtient les widget
     */

    public function GetWidget($type, $params) {

        $widget = new WidgetController($this->Core);
        return $widget->Show($type, $params);
    }
    
    /**
     * Sauvegare une page avec le page Form 
     */
    public function SavePageSeoForm() {
        return PageHelper::SavePageSeoForm($this->Core, Request::GetPosts());
    }

    /***
     * Supprime une page
     */
    public function DeletePage() {
        return PageHelper::DeletePage($this->Core, Request::GetPost("PageId"));
    }


}

?>
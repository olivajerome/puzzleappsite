<?php 
/*
* PuzzleApp
* Webemyos
* Jérôme Oliva
* GNU Licence
*/

namespace Apps\Comunity\Entity;
use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

use Core\Entity\User\User;

class ComunityMessage extends Entity  
{
    
    protected $User;
	
        //C eonstructeur
	function __construct($core)
	{
		//Version
		$this->Version ="2.0.0.0"; 

		//Nom de la table 
		$this->Core=$core; 
		$this->TableName="ComunityMessage"; 
		$this->Alias = "ComunityMessage"; 

		$this->Type = new Property("Type", "Type", NUMERICBOX,  true, $this->Alias); 
		$this->UserId = new Property("UserId", "UserId", NUMERICBOX,  true, $this->Alias); 
                $this->User = new EntityProperty("Core\Entity\User\User", "UserId"); 
                   
		$this->Message = new Property("Message", "Message", TEXTAREA,  false, $this->Alias); 
		$this->DateCreated = new Property("DateCreated", "DateCreated", DATEBOX,  true, $this->Alias); 
		$this->ComunityId = new Property("ComunityId", "ComunityId", NUMERICBOX,  true, $this->Alias); 

		//Creation de l entité 
		$this->Create(); 
	}
        
        function GetTitleUser()
        {
           $user = new User($this->Core);
           $user->GetById($this->UserId->Value);
            
           $html = "<div title='".$user->GetPseudo()."' class='avatarmini-img avatarmini' > ";
           $html .= "<img src='".$user->GetImageMini()."' alt='images'>";
           $html .= "</div>";
           
           $html .="<div style='width:80%' ><h2>".$user->GetPseudo()." .<i>2j</i></h2>" . "</div>" ;
           
           
           return $html;
           
        }
        
        /**
         * Retourne le pseudo de l'utilisateur
         */
        function ImageUser()
        {
            $user = new User($this->Core);
            $user->GetById($this->UserId->Value);
            
            $html = "<span title='".$user->GetPseudo()."' class='avatarmini-img avatarmini' > ";
            $html .= "<img src='".$user->GetImageMini()."' alt='images'>";
            $html .= "</span>";
            
            return $html;
            
        }
        
        /**
         * Pseuo et ville
         */
        function PseudoUser()
        {
            $user = new User($this->Core);
            $user->GetById($this->UserId->Value);
            
           return  "<p class='membresTitre2  text-left user'>".$user->GetPseudo()."
					<br>
					<span class='membreville '>".$user->City->Value."</span>
				</p>"; 
        }
        
        /**
         * Obtient le nombre de commentaire
         * TODO PASSSER CETTE PROPIETE DANS UN SQL PROPERTY
         * @return string
         */
        function GetNumberComment()
        {
            $request = "SELECT count(Id) as nbComment FROM ComunityComment where MessageId = " . $this->IdEntite;
            $result = $this->Core->Db->GetLine($request);
            
            return $result["nbComment"];
        }
}
?>
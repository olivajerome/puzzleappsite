<section class='container'>
    <div class='row'>
        <div>
            <h1 class=''>
                <a class='noHover' href='{{GetPath(/Mooc)}}'>
                    <i class ='fa fa-home  breadCrumbIcon'></i> 
                </a>
                
                {{GetCode(Mooc.Tutorial)}}
                <i class ='fa fa-angle-right breadCrumbIcon'></i>  
                {{Mooc->Name->Value}}
            
            <i class ='fa fa-angle-right breadCrumbIcon'></i>  
            <span class='finalBreadCrumb'>{{Intro->Name->Value}}</span>
            
            </h1>   
            <div class='col-md-2'></div>
        </div>
        <div class='col-md-2'>
            <div class='block'>   
           <h2 class='borderBottom'>{{GetCode(Mooc.Lessons)}}</h2>

                <ul>
                    {{foreach Lessons}}
                    <li><a href='{{GetPath(/Mooc/Lesson/)}}{{element->Code->Value}}'>
                            {{element->Name->Value}}
                        </a>
                    </li>
                    {{/foreach Lessons}}
                </ul>
            </div>
        </div>
        <div class='col-md-10'>
            <div class='centerBlock'>
                <h2></h2>
                <p>{{Intro->GetContent()}}</p>
            </div>    
        </div>
    </div>
</section>

CREATE TABLE IF NOT EXISTS `MessageMessage` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`Type` INT  NULL ,
`UserId` INT  NULL ,
`Message` TEXT  NULL ,
`DateCreated` DATE  NULL ,
`ConversationId` INT  NULL ,
PRIMARY KEY (`Id`),
CONSTRAINT `ee_user_MessageMessage` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`),
CONSTRAINT `MessageMessage_MessageMessage` FOREIGN KEY (`ConversationId`) REFERENCES `MessageConversation`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
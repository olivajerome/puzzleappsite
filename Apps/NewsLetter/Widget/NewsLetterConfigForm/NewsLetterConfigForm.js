var NewsLetterConfigForm = function () {};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
NewsLetterConfigForm.Init = function () {
    Event.AddById("btnSaveNewsLetterConfig", "click", NewsLetterConfigForm.SaveElement);
    Event.AddById("btnAddPlugin", "click", NewsLetterConfigForm.AddPlugin);
    Event.AddByClass("removePlugin", "click" , NewsLetterConfigForm.RemovePlugin);
};

/***
 * Obtient l'id de l'itin�raire courant 
 */
NewsLetterConfigForm.GetId = function () {
    return Form.GetId("NewsLetterConfigFormForm");
};

/*
 * Save the configuration
 */
NewsLetterConfigForm.SaveElement = function (e) {

    if (Form.IsValid("NewsLetterConfigFormForm"))
    {

        var data = "Class=NewsLetter&Methode=SaveConfig&App=NewsLetter";
        data += Form.Serialize("NewsLetterConfigFormForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if (data.statut == "Error") {
                Form.RenderError("NewsLetterConfigFormForm", data.message);
            } else {

                Form.SetId("NewsLetterConfigFormForm", data.data.Id);
            }

            Animation.Notify(Language.GetCode("NewsLetter.ConfigUpdated"));

        });
    }
};

/***
 * Ajout d'un plugin
 */
NewsLetterConfigForm.AddPlugin = function(e){
  
    e.preventDefault();
    
     Dialog.open('', {"title": Dashboard.GetCode("NewsLetter.AddPlugin"),
     "app": "EeApp",
     "class": "DialogEeApp",
     "method": "AddPluginApp",
     "params": "NewsLetter",
     "type" : "right"
     });
 };
 
 /***
  * Remove plugin to the shop 
  * @returns {undefined}
  */
 NewsLetterConfigForm.RemovePlugin = function(e){
     
     Animation.Confirm(Language.GetCode("NewsLetter.RemovePlugin"), function(){
         let container = e.srcElement.parentNode;
         
         let  data = "Class=EeApp&Methode=RemovePluginApp&App=EeApp";
              data += "&pluginId="+e.srcElement.id;
              
         Request.Post("Ajax.php", data).then(data => {
             container.parentNode.removeChild(container);
         });
     });
 };
 


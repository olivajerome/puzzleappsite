CREATE TABLE IF NOT EXISTS `WorkFlowTrigger` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`WorkFlowId` INT  NOT NULL,
`Event` TEXT  NOT NULL,
`DateExecution` TEXT NULL,
PRIMARY KEY (`Id`),
CONSTRAINT `WorkFlowWorkFlow_WorkFlowTrigger` FOREIGN KEY (`WorkFlowId`) REFERENCES `WorkFlowWorkFlow`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Core\Control\ListRadio;

use Core\Control\IControl;
use Core\Control\Control;

class ListRadio extends Control {
    /*
     * List of Element   
     */

    public $Elements;

    /*
     * Group Name
     */
    public $Name;

    //Ajout d'un element à la liste
    function Add($key, $value) {
        $this->Elements[$key] = $value;
    }

    function Show() {
        $html = "<ul " . $this->getProperties() . " >";

        if (is_array($this->Elements)) {

            foreach ($this->Elements as $key => $value) {
                $html .= "<li><input name='" . $this->Name ."'  type='radio' value='$value'><label>&nbsp;$key</label></input></li>";
            }
        } else {

            $Elements = explode(";", $this->Elements);

            foreach ($Elements as $element) {
                $data = explode(":", $element);

                $html .= "<li><input type='radio' name = '" . $this->Name . "'  value='$data[0]'>$data[1]</input></li>";
            }
        }
        $html .= "</ul>";

        return $html;
    }
}

<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Carousel\Module\Admin;

use Core\Control\Button\Button;
use Core\Controller\AdministratorController;
use Core\View\View;
use Core\View\ElementView;
use Core\Control\TabStrip\TabStrip;
use Core\Control\EntityGrid\EntityGrid;
use Core\Control\EntityGrid\EntityColumn;
use Core\Control\EntityGrid\EntityIconColumn;
use Core\Control\EntityGrid\EntityFunctionColumn;
use Core\Utility\Format\Format;
use Apps\Carousel\Widget\CarouselForm\CarouselForm;
use Apps\Carousel\Widget\CarouselItemForm\CarouselItemForm;
use Apps\Carousel\Entity\CarouselCarousel;
use Apps\Carousel\Entity\CarouselItem;

/*
 * 
 */

class AdminController extends AdministratorController {

    /**
     * Constructeur
     */
    function _construct($core = "") {
        $this->Core = $core;
    }

    /**
     * Create
     */
    function Create() {
        
    }

    /**
     * Init
     */
    function Init() {
        
    }

    /**
     * Render
     */
    function Show($all = true) {
        return $this->Index();
    }

    /*
     * Get the home page
     */

    function Index() {
        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);

        $tabCarousel = new TabStrip("tabCarousel", "Carousel");
        $tabCarousel->AddTab($this->Core->GetCode("Carousel.Carousels"), $this->GetTabCarousel());

        $view->AddElement(new ElementView("tabCarousel", $tabCarousel->Render()));

        return $view->Render();
    }

    /*     * *
     * Carousel
     */

    function GetTabCarousel() {

        $gdCarousel = new EntityGrid("gdCarousel", $this->Core);
        $gdCarousel->Entity = "Apps\Carousel\Entity\CarouselCarousel";
        $gdCarousel->App = "Carousel";
        $gdCarousel->Action = "GetTabCarousel";

        $btnAdd = new Button(BUTTON, "btnAddCarousel");
        $btnAdd->Value = $this->Core->GetCode("Carousel.AddCarousel");
        $gdCarousel->AddButton($btnAdd);

        $gdCarousel->AddColumn(new EntityColumn($this->Core->GetCode("Carousel.Name"), "Name"));
        $gdCarousel->AddColumn(new EntityColumn($this->Core->GetCode("Carousel.Description"), "Description"));

        $gdCarousel->AddColumn(new EntityIconColumn("Action",
                        array(array("EditIcone", "Carousel.EditCarousel", "Carousel.EditCarousel"),
                    array("DeleteIcone", "Carousel.DeleteCarousel", "Carousel.DeleteCarousel"),
                        )
        ));

        return $gdCarousel->Render();
    }

    /*     * *
     * Save a Carousel
     */

    function SaveCarousel($data) {

        $carouselForm = new CarouselForm($this->Core, $data);

        if ($carouselForm->Validate($data)) {

            $carousel = new CarouselCarousel($this->Core);

            if ($data["Id"] != "") {
                $carousel->GetById($data["Id"]);
                $carouseId = $data["Id"];
            }
            $carousel->Code->Value = Format::ReplaceForUrl($data["Name"]);

            $carouselForm->Populate($carousel);
            $carousel->Save();

            if ($data["Id"] == "") {
                $carouseId = $this->Core->Db->GetInsertedId();
            }

            return $carouseId;
        }

        return false;
    }

    /*     * *
     * Delte the carousel and this items
     */

    function DeleteCarousel($carouselId) {

        $item = new CarouselItem($this->Core);
        $items = $item->Find("CarouselId=" . $carouselId);

        foreach ($items as $item) {
            $item->Delete();
        }

        $carousel = new CarouselCarousel($this->Core);
        $carousel->GetById($carouselId);
        $carousel->Delete();

        return true;
    }

    /*     * *
     * Edit a item
     */

    function EditItem($itemId, $carrouselId) {

        $item = new CarouselItem($this->Core);

        if ($itemId != "") {
            $item->GetById($itemId);
        }

        $item->CarouselId->Value = $carrouselId;

        $carouselItemForm = new CarouselItemForm($this->Core, $item, $carrouselId);
        $carouselItemForm->Load($item);

        return $carouselItemForm->Render();
    }

    /*     * *
     * Save a item
     */

    public function SaveItem($data) {

        $carouselItemForm = new CarouselItemForm($this->Core, $data);

        if ($carouselItemForm->Validate($data)) {

            $item = new CarouselItem($this->Core);

            if ($data["Id"] != "") {
                $item->GetById($data["Id"]);
                $itemId = $data["Id"];
            } else {
                $item->CarouselId->Value = $data["CarouselId"];
            }

            $carouselItemForm->Populate($item);
            $item->Save();

            if ($data["Id"] == "") {
                $itemId = $this->Core->Db->GetInsertedId();
            }
            return $itemId;
        }

        return $false;
    }

    /****
     * Remove a item
     */
    function RemoveItem($itemId){
        
       $item = new CarouselItem($this->Core);
       $item->GetById($itemId);
        $item->Delete();
    }
    
    /* action */
}

?>
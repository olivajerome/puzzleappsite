<div class='tchatTitle'>
   <div class="avatarmini-img avatarmini" style="cursor: pointer;"> 
      <img src="{{memberImage}}" alt="images"> 
    </div>
    <div>
        {{memberPseudo}}
    </div>
    
    <input type='hidden' id='hdMessageId' value='{{MessageId}}' />
    <input type='hidden' id='hdUserId' value='{{UserId}}' />
    
</div>
<div class='messageContent' id='dashboardDiscus'  >
    {{Messages}}
</div>
<div class='center tchatBlock'>
    
    <div id='notify'></div>
    
    <textarea id='tbMessage' class='form-control' placeHolder='Ecrivez votre message' ></textarea>
    
    {{uploadMessage}}
    
    <button id='btnSendTchatMessage' class='btn btn-primary'>Envoyer</button>
    
</div>

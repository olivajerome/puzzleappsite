<div class='col-md-4'>
    <div class='block widgetDashboard'>
        <i class='fa fa-trash removeWidget' data-app='Mooc' title='{{GetCode(EeApp.RemoveWidgetDahboard)}}'></i>
        <i class='fa fa-archive title'>&nbsp;{{GetCode(Mooc.Mooc)}}</i>
            
        <ul>
            {{foreach Mooc}}
                <li>  
                   {{element->Name->Value}} 
                   
                   <div class='picto'>
                       <b>    
                         {{element->GetNumberUser()}} - 
                         {{GetCode(Mooc.Student)}}
                     </b> 
                   
                   </div> 
                </li>
            {{/foreach Mooc}}
        </ul>


    </div>
</div>

var UserRessourceForm = function(){};

/***
 * Init the form
 * @returns {undefined}
 */
UserRessourceForm.Init = function(){
    Event.AddById("btnSendRessource", "click", UserRessourceForm.SendRessource);
};

/***
* Get the Id of element 
*/
UserRessourceForm.GetId = function(){
    return Form.GetId("UserRessourceFormForm");
};

/*
* Save the Element
*/
UserRessourceForm.SendRessource = function(e){
   
    if(Form.IsValid("UserRessourceFormForm"))
    {

    let data = "Class=Downloader&Methode=SaveUserRessource&App=Downloader";
        data +=  Form.Serialize("UserRessourceFormForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.status == "error"){
                Form.RenderError("UserRessourceFormForm", data.data.message);
            } else {

               Dialog.Close();
               Animation.Notify(Language.GetCode("Downloader.ThankYouForYouContribution"));
            }
        });
    }
};


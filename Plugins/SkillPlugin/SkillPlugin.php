<?php

namespace Plugins\SkillPlugin;

use Core\View\View;
use Core\View\ElementView;
use Core\Core\Response;
use Core\Control\Form\Form;
use Core\Utility\Date\Date;
use Core\Control\TextBox\TextBox;
use Core\Control\Button\Button;
use Core\Control\EntityGrid\EntityGrid;
use Core\Control\EntityGrid\EntityColumn;
use Core\Control\EntityGrid\EntityIconColumn;
use Core\Control\EntityGrid\EntityFunctionColumn;
use Core\Control\EntityListBox\EntityListBox;
use Core\Entity\Entity\Argument;

use Plugins\SkillPlugin\Entity\SkillCategory;
use Plugins\SkillPlugin\Entity\Skill;
use Plugins\SkillPlugin\Entity\SkillEntity;


class SkillPlugin {

    
    public $Author = 'Webemyos';
    public $Version = '1.0.0.0';
    
    function __construct($core) {
        $this->Core = $core;
    }

    /*     * *
     * Comment Type
     */

    function GetType() {
        return "Skill";
    }

    /*     * *
     * Render the Js in the VotePlugin
     */

    function GetRegisterPlugin() {
        return "<script>document.addEventListener('DOMContentLoaded', function() { " . self::GetRegister() ."}) ;</script>";
    }
    
    /***
     * 
     */
    function GetRegister(){
       return "Plugin.Register('SkillPlugin', 'SkillPlugin', function(){ SkillPlugin.Init(); });" ;
    }
    
    /***
     * Render for Admin
     */
    function RenderAdmin($appName){

        $view = new View(__DIR__ . "/View/admin.tpl", $this->Core);         

        $view->AddElement(new ElementView("AppName", $appName));
        
        $gdCategory = new EntityGrid("gdCategory", $this->Core);
        $gdCategory->Entity = "Plugins\SkillPlugin\Entity\SkillCategory";
        $gdCategory->App = "Shop";
        $gdCategory->Action = "GetTabCategory";
  
        $gdCategory->AddArgument(new Argument("Plugins\SkillPlugin\Entity\SkillCategory", "AppName", EQUAL, $appName));
        
        $tbNew = new TextBox("newSkillCategoryName");
        $tbNew->Style= 'width:60%;display:inline-block';
        $tbNew->PlaceHolder = $this->Core->GetCode("Base.EnterCategory");
        $view->AddElement($tbNew);

        $buttonAdd = new Button("btnAdd");
        $buttonAdd->Id = "btnAddCategory";
        $buttonAdd->Value = $this->Core->GetCode("Base.AddCategory");

        $view->AddElement($buttonAdd);

        $gdCategory->AddColumn(new EntityColumn("Name", "Name"));
        $gdCategory->AddColumn(new EntityFunctionColumn("Skill", "GetSkills"));
        
        $gdCategory->AddColumn(new EntityIconColumn("Action", 
                                                  array(array("AddIcone", "SkillPlugin.AddSkill", "SkillPlugin.AddSkill"),
                                                        array("DeleteIcone", "SkillPlugin.DeleteCategory", "SkillPlugin.DeleteCategory"),
                                                  )    
                          ));
        $view->AddElement($gdCategory);

        $view->AddElement(new ElementView("pluginScript", $this->GetRegister()));

        return $view->Render();
    }

    /***
     *  Render Member Skill
     */
    function RenderMember($appName, $entityId){

        $view = new View(__DIR__ . "/View/member.tpl", $this->Core);         

        $view->AddElement(new ElementView("AppName", $appName));
        $view->AddElement(new ElementView("EntityId", $entityId));
        
        $lstSkillCategory = new EntityListBox("lstSkillCategory", $this->Core);
        $lstSkillCategory->Entity = "Plugins\SkillPlugin\Entity\SkillCategory";
        $lstSkillCategory->AddArgument(new Argument("Plugins\SkillPlugin\Entity\SkillCategory", "AppName", EQUAL, $appName));
        
        $lstSkillCategory->ListBox->Add($this->Core->GetCode("Base.SelectACategory"), "");
        
        $view->AddElement(new ElementView("lstSkillCategory", $lstSkillCategory->Render()));
    
        $SkillEntity = new SkillEntity($this->Core);
        $SkillEntity->Select("Skill.Name", "SkillName");
        $SkillEntity->Join("Skill", "Skill", "left", "SkillEntity.SkillId = Skill.Id");
        $Skills = $SkillEntity->Find("EntityId=" . $entityId);

        $view->AddElement(new ElementView("Skills", $Skills));
        
        $view->AddElement(new ElementView("pluginScript", $this->GetRegister()));
           
        return $view->Render();
    }

    /***
     * Add Categorie of compétences
     */
    function AddCategory($params){

        $category = new SkillCategory($this->Core);
        $category->Name->Value = $params->name;
        $category->AppName->Value = $params->appName;
        $category->Save();

        return $this->RenderAdmin($params->appName);
    }

    /***
     * AddSkill to a category
     */
    function AddSkill($params){

        $Skill = new Skill($this->Core);
        $Skill->Name->Value = $params->name;
        $Skill->CategoryId->Value = $params->categoryId;
        $Skill->Save();

        $category = new SkillCategory($this->Core);
        $category->GetById($params->categoryId);
        return $category->GetSkills();
    }

    /**
     * Delete a Skill
     */
    function RemoveSkill($params){

        $Skill = new Skill($this->Core);
        $Skill->GetById($params->id);

        $this->DeleteSkillEntity($Skill->IdEntite);
          
        $Skill->Delete();
    }

    /**
     * Remove a category
     */
    function DeleteCategory($params){

        $category = new SkillCategory($this->Core);
        $category->GetById($params->id);

        $Skill = new Skill($this->Core);
        $Skills =  $Skill->Find("CategoryId=" .$params->id );

        foreach($Skills as $Skill){

            $this->DeleteSkillEntity($Skill->IdEntite);
            $Skill->Delete();
        }

        $category->Delete();

        return $this->RenderAdmin($params->appName);
    }

    /**
     * Delete user Skill
     */
    function DeleteSkillEntity($SkillId){

        $SkillEntity = new SkillEntity($this->Core);
        $Skills = $SkillEntity->Find("SkillId=" .$SkillId );

        foreach($Skills as $Skill){
            $Skill->Delete();
        }
    }
    
    /***
     * Get All Skill to a category
     */
    function GetSkillsByCategory($params){
      $category = new SkillCategory($this->Core);
      $category->GetById($params->id);
      
      return $category->GetSkills(true, false);
    }
    
    /***
     * Add A skill to a entity
     */
    function AddToEntity($params){
        
        $SkillEntity = new SkillEntity($this->Core);
        $SkillEntity->SkillId->Value = $params->id;
        $SkillEntity->EntityName->Value = $params->appName;
        $SkillEntity->EntityId->Value = $params->entityId;
        
        $SkillEntity->Save();
        
                
        $SkillEntity = new SkillEntity($this->Core);
        $SkillEntity->Select("Skill.Name", "SkillName");
        $SkillEntity->Join("Skill", "Skill", "left", "SkillEntity.SkillId = Skill.Id");
        $Skills = $SkillEntity->Find("EntityId=" . $params->entityId);
        
        $view = "";
        
        foreach($Skills as $skill){
        
            $view .= "<div class='chips'>
                ".$skill->SkillName->Value."
                <i class='fa fa-trash removeSkillEntity' id='".$skill->IdEntite."'  ></i>
            </div>";
        }
        
        return  $view;
    }
    
    /****
     * Remove Skill to a entity
     */
    function RemoveToEntity($params){
        
        $SkillEntity = new SkillEntity($this->Core);
        $SkillEntity->GetById($params->id);
        $SkillEntity->Delete();
    }
}
{{form->Open()}}
{{form->Error()}}
{{form->Success()}}

{{form->Render(Id)}}

    <div>
        <label>{{GetCode(EeApp.Code)}}</label> 
        {{form->Render(Code)}}
    </div>
    
    <div>
        <label>{{GetCode(EeApp.Name)}}</label> 
        {{form->Render(Name)}}
    </div>
    
    <div>
        <label>{{GetCode(EeApp.Description)}}</label> 
        {{form->Render(Description)}}
    </div>

    <div class='center marginTop' >   
        {{form->Render(btnSave)}}
    </div>  

{{form->Close()}}
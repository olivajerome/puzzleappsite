<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Mooc\Module\DialogAdminMooc;

use Core\Control\Button\Button;
use Core\Controller\AdministratorController;
use Apps\Mooc\Entity\MoocCategory;
use Apps\Mooc\Entity\MoocMooc;
use Apps\Mooc\Widget\MoocCategoryForm\MoocCategoryForm;
use Apps\Mooc\Widget\MoocMoocForm\MoocMoocForm;

class DialogAdminMoocController extends AdministratorController{

    /***
     * Ajout d'une catégorie
     */
    function AddCategorie($categoryId){
      
        $category = new MoocCategory($this->Core);
        if($categoryId != ""){
            $category->GetById($categoryId);
        }
        
        $moocCategoryForm = new MoocCategoryForm($this->Core);
        $moocCategoryForm->Load($category);
        
        return $moocCategoryForm->Render();
    }
    
    
    /***
     * Ajout d'un Mooc
     */
    function AddMooc($moocId){
      
        $mooc = new MoocMooc($this->Core);
        if($moocId != ""){
            $mooc->GetById($moocId);
        }
        
        $moocMoocForm = new MoocMoocForm($this->Core, $mooc);
        $moocMoocForm->Load($mooc);
        $moocMoocForm->form->LoadImage($mooc);
        
        return $moocMoocForm->Render();
    }
}
<?php

namespace Apps\Shop\Helper;

use Core\Core\Request;

use Apps\Shop\Entity\Command;
use Apps\Shop\Entity\CommandLine;
use Apps\Shop\Entity\ShopProduct;


class CartHelper{
    
    
    /***
     * Ajoute un produit au panier
     */
    public static function AddToCart($core, $product){
        
       // Request::ClearSession("Cart");
        
        self::CreateCart();
       
       return self::AddLine($core, $product);
    }
    
    /***
     * Ajout du panier en sessions si il' n'existe pas encore
     */
    public static function CreateCart(){
        
        if(!Request::GetSession("Cart")){
  
            Request::SetSession("Cart", json_encode(array("id" => uniqid(), "lines" => array())));
        }
    }
    
    /***
     * Ajotue une line si l'id entite n'est pas déjà présent
     */
    public static function AddLine($core, $productId){
        
        $cart = json_decode(Request::GetSession("Cart"));
        
        $cart->lines = (array)$cart->lines;
        
        $exist = false;
        
        foreach($cart->lines as $key => $value ){
            if($value->entityId == $productId){
               $exist = true;
            }
        }
     
        if(!$exist){

            $product = new ShopProduct($core);
            $product->GetById($productId);

            $cart->lines[$productId] =  $product->ToArray();
       }
       
       Request::SetSession("Cart", json_encode($cart));

       return count($cart->lines);
    }
    
    /***
     * Suppression de la ligne du produit
     */
    public static function RemoveToCart($core, $productId){
  
        $cart = json_decode(Request::GetSession("Cart"));
        
        $cart->lines = (array)$cart->lines;
        
        $lines = array();
       
        foreach($cart->lines as $key => $value ){
            if($value->IdEntite != $productId){
                $lines[$key] =  $value;
            }
        }
       
       $cart->lines = $lines;
       Request::SetSession("Cart", json_encode($cart));
       
       return self::GetNumberElement();
    }
    
    /***
     * Obtient le nombre d'élement dans le panier
     */
    public static function GetNumberElement(){
        
        if(Request::GetSession("Cart")){
            $cart = json_decode(Request::GetSession("Cart"), true);
            
            return count($cart["lines"]);
        } else{
            return 0; 
        }
    }
    
    /***
     * Obtient le Panier
     */
    public static function GetCart(){
        return json_decode(Request::GetSession("Cart"));
    }
    
    /***
     * Obtient le montant total du panier
     */
    public static function GetTotalCart(){
       $cart = self::GetCart();
       
       $total = 0;
       
       
        if(isset($cart->lines)){
           
           foreach($cart->lines as $line){
                $total += (float)$line->price;
           }
       }
       
       return $total; 
    }
    
    
    
    /***
     * Vie le panier
     */
    public static function ClearCart(){
        Request::ClearSession("Cart");
    }
    
    /***
     * Récupere un panier sauvegardé
     */
    public static function GetSavedCart($core, $cartId){
        
        $commande = new Command($core);
        $commande = $commande->Find("CartId='" . $cartId."'");
        
        $cart = array();
        $cart["status"] = $commande[0]->Status->Value;
        $cart["lines"] = array();
        
        if(count($commande) > 0){
      
            $lines = new CommandLine($core);
            $lines = $lines->Find("CommandId=" .$commande[0]->IdEntite);
            
            foreach($lines as $line){
                $cart["lines"][] = array("entityId" => $line->EntityId->Value);
            }
        }
    
        return json_encode($cart);
   }
}

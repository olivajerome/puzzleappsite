<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Plugins\SkillPlugin\Entity;

use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class SkillEntity extends Entity {

    //Constructeur
    function __construct($core) {
        //Version
        $this->Version = "2.0.0.0";

        //Nom de la table 
        $this->Core = $core;
        $this->TableName = "SkillEntity";
        $this->Alias = "SkillEntity";

        $this->EntityName = new Property("EntityName", "EntityName", TEXTBOX, false, $this->Alias);
        $this->EntityId = new Property("EntityId", "EntityId", TEXTBOX, false, $this->Alias);
        $this->SkillId = new Property("SkillId", "SkillId", TEXTBOX, false, $this->Alias);
        $this->Description = new Property("Description", "Description", TEXTBOX, false, $this->Alias);
    
        //Creation de l entité 
        $this->Create();
    }
}

?>
<?php
/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */


namespace Apps\Blog\Module\Admin;

use Core\Controller\AdministratorController;
use Core\Entity\Entity\Argument;
use Core\Utility\Format\Format;
use Core\View\ElementView;
use Core\View\View;
use Core\Control\TabStrip\TabStrip;
use Core\Control\EntityGrid\EntityGrid;
use Core\Control\EntityGrid\EntityColumn;
use Core\Control\EntityGrid\EntityIconColumn;
use Core\Control\EntityGrid\EntityFunctionColumn;
use Core\Control\Button\Button;

use Apps\Blog\Helper\BlogHelper;
use Apps\Blog\Helper\CategoryHelper;
use Apps\Blog\Helper\ArticleHelper;
use Apps\Blog\Widget\BlogForm\BlogForm;
use Apps\Blog\Helper\ImportHelper;


use Apps\Blog\Entity\BlogCategory;
use Apps\Blog\Entity\BlogArticle;

class AdminController extends AdministratorController
{
    /**
     * Affichage du module
     */
    function Show($all=true)
    {
         return $this->Index();
    }
    
    /*
    * Get the home page
    */
   function Index()
   {
      $blog = BlogHelper::GetFirst($this->Core);

       if($blog){
         
         $view = new View(__DIR__."/View/index.tpl", $this->Core);
        
         $tabBlog = new TabStrip("tabBlog", "Blog"); 
         $tabBlog->AddTab($this->Core->GetCode("Blog.Blog"), $this->GetTabBlog($blog));
         $tabBlog->AddTab($this->Core->GetCode("Blog.Categorys"), $this->GetTabCategory($blog));
         $tabBlog->AddTab($this->Core->GetCode("Blog.Articles"), $this->GetTabArticle($blog));
         $tabBlog->AddTab($this->Core->GetCode("Blog.UserNewsLetter"), $this->GetTabUserNewsLetter($blog));
         $tabBlog->AddTab($this->Core->GetCode("Blog.ExportImport"), $this->GetTabExportImport($blog));
         
         $view->AddElement(new ElementView("tabBlog", $tabBlog->Render($blog)));

        } else {
            
         $view = new View(__DIR__."/View/noBlog.tpl", $this->Core);

         $blogForm = new BlogForm($this->Core);
         $view->AddElement(new ElementView("blogForm", $blogForm->Render($Blog)));
       }

       return $view->Render();
   }

   /***
    * Detail of the blog
    */
   function GetTabBlog($blog){

      $blogForm = new BlogForm($this->Core, $blog);
      $blogForm->Load($blog);
      return $blogForm->Render($blog);   
 }

 /***
    * Categorie dof the Forum
    */
    function GetTabCategory($Blog){
     
      $gdCategory = new EntityGrid("gdCategory", $this->Core);
      $gdCategory->Entity = "Apps\Blog\Entity\BlogCategory";
      $gdCategory->AddArgument(new Argument("Apps\Blog\Entity\BlogCategory", "BlogId", EQUAL, $Blog->IdEntite));
      $gdCategory->App = "Blog";
      $gdCategory->Action = "GetTabCategory";

      $btnAdd = new Button(BUTTON, "btnAddCategory");
      $btnAdd->Value = $this->Core->GetCode("Blog.AddCategory");

      $gdCategory->AddButton($btnAdd);
      $gdCategory->AddColumn(new EntityColumn("Name", "Name"));
      
      $gdCategory->AddColumn(new EntityIconColumn("Action", 
                                                array(array("EditIcone", "Blog.EditCategory", "Blog.EditCategory"),
                                                      array("DeleteIcone", "Blog.DeleteCategory", "Blog.DeleteCategory"),
                                                )    
                        ));

      return $gdCategory->Render();
   }
   
   /***
    * Artile of the Blog
    */
    function GetTabArticle($Blog){
     
      $gdArticle = new EntityGrid("gdArticle", $this->Core);
      $gdArticle->Entity = "Apps\Blog\Entity\BlogArticle";
      $gdArticle->AddOrder("Id Desc");
      
      $gdArticle->AddArgument(new Argument("Apps\Blog\Entity\BlogArticle", "BlogId", EQUAL, $Blog->IdEntite));
      $gdArticle->App = "Blog";
      $gdArticle->Action = "GetTabArticle";

      $btnAdd = new Button(BUTTON, "btnAddArticle");
      $btnAdd->Value = $this->Core->GetCode("Blog.AddArticle");

      $gdArticle->AddButton($btnAdd);
      
      $gdArticle->AddColumn(new EntityFunctionColumn("Image", "GetImage"));
      $gdArticle->AddColumn(new EntityColumn("Name", "Name"));
      $gdArticle->AddColumn(new EntityColumn("DateCreated", "DateCreated"));
      $gdArticle->AddPluginColumn("Blog", "Comment", "Comments", "GetComments");
      
      $gdArticle->AddColumn(new EntityIconColumn("Action", 
                                                array(array("ParameterIcone", "Blog.EditArticle", "Blog.EditArticle"),
                                                      array("EditIcone", "Blog.WriteArticle", "Blog.WriteArticle"),
                                                      array("DeleteIcone", "Blog.DeleteArticle", "Blog.DeleteArticle"),
                                                )    
                        ));

      return $gdArticle->Render();
   }
   
   /***
    * User newsletters
    */
   function GetTabUserNewsLetter(){
      $gdUser = new EntityGrid("gdUser", $this->Core);
      $gdUser->Entity = "Apps\Blog\Entity\BlogUserNewLetter";
      $gdUser->AddArgument(new Argument("Apps\Blog\Entity\BlogUserNewLetter", "BlogId", EQUAL, $Blog->IdEntite));
      $gdUser->App = "Blog";
      $gdUser->Action = "GetTabUserNewsLetter";
      $gdUser->AddColumn(new EntityColumn("Email", "Email"));
      
      return $gdUser->Render();
   }

   /****
    * Export/Import tools
    */
   function GetTabExportImport(){

      $view = new View(__DIR__."/View/exportImport.tpl", $this->Core);
       
      return $view->Render();
   }

   /***
    * Save The Blog
    */
   function SaveBlog($core, $data){
      return BlogHelper::Save($core, $data);
   }

   /**
    * Save a catégory
    */
   function SaveCategory($core, $data){
      return CategoryHelper::SaveCategory($core, $data);
   }

   /**
    * Delete a category
    */
   function DeleteCategory($core, $categoryId){
      return CategoryHelper::DeleteCategory($core, $categoryId);
   }

   /***
    * Save Article
    */
   function SaveArticle($core, $data){
      return ArticleHelper::Save($core, $data);
   }

   /***
    * DeleteArticle
    */
   function DeleteArticle($core, $articleId){
      return ArticleHelper::DeleteArticle($core, $articleId);
   }

   /***
    * Update Content of a article
    */
   function UpdateContent($core, $articleId, $content){

     $content = json_decode($content);
     return ArticleHelper::SaveContent($core, $articleId, $content->content);
   }

   /***
    * Export all category and Article
    */
   function Export(){

      $category = new BlogCategory($this->Core);
      $categorys = $category->GetAll();
      $categorysArray = array();

      foreach($categorys as $category){

         $currentCategory = $category->ToArray();

         $article = new BlogArticle($this->Core);
         $articles = $article->Find("CategoryId=" . $category->IdEntite);

         foreach($articles as $article){
            $currentCategory["Articles"][] = $article->ToArray();
         }

         $categorysArray[] = $currentCategory;
      }


      //Force download
      header('Content-Type: application/octet-stream');
      header("Content-Transfer-Encoding: Binary"); 
      header("Content-disposition: attachment; filename=ExportBlog.json"); 

      return json_encode($categorysArray);

   }
   
    /**
     * Import all categorie and Article
     *  
     */
    function Import($data){
        
       $file = $data["dvUpload-UploadfileToUpload"];
       $data = json_decode(file_get_contents($file));
       
       $message ="Debut du traitement";
         
       foreach($data as $category){
       
           $message .= "<br>--------------"; 
           $message .= ImportHelper::ImportCategory($this->Core, $category);
       }
    }
}
<?php 
/*
* PuzzleApp
* Webemyos
* Jérôme Oliva
* GNU Licence
*/

namespace Apps\Market\Entity;
use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class MarketProductShipping extends Entity  
{
	//Constructeur
	function __construct($core)
	{
		//Version
		$this->Version ="2.0.0.0"; 

		//Nom de la table 
		$this->Core=$core; 
		$this->TableName="MarketProductShipping"; 
		$this->Alias = "MarketProductShipping"; 

		$this->OfferId = new Property("OfferId", "OfferId", NUMERICBOX,  false, $this->Alias); 
		$this->ProductId = new Property("ProductId", "ProductId", NUMERICBOX,  false, $this->Alias); 
		$this->TrackingNumber = new Property("TrackingNumber", "TrackingNumber", TEXTAREA,  false, $this->Alias); 
		$this->TrackingUrl = new Property("TrackingUrl", "TrackingUrl", TEXTAREA,  false, $this->Alias); 
		$this->DateSended = new Property("DateSended", "DateSended", DATEBOX,  false, $this->Alias); 
		$this->Status = new Property("Status", "Status", NUMERICBOX,  false, $this->Alias); 

		//Creation de l entité 
		$this->Create(); 
	}
}
?>
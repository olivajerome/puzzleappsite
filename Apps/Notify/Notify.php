<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Notify;

use Core\Core\Core;
use Core\App\Application;
use Core\Core\Request;
use Core\Core\Response;
use Apps\Notify\Module\Member\MemberController;
use Apps\Notify\Module\Admin\AdminController;
use Apps\Notify\Helper\NotifyHelper;

class Notify extends Application {

    /**
     * Auteur et version
     * */
    public $Author = 'Eemmys';
    public $Version = '2.1.0.0';
    public static $Directory = "../Apps/Notify";

    /**
     * Constructeur
     * */
    function __construct() {
        $this->Core = Core::getInstance();
        parent::__construct($this->Core, "Notify");
    }

    /**
     * Execution de l'application
     */
    function Run() {
        echo parent::RunApp($this->Core, "Notify", "Notify");
    }

    /**     *
     * Run member Controller
     */
    function RunMember() {
        $memberController = new MemberController($this->Core);
        return $memberController->Index();
    }

    /**
     * Ajoute une notification
     * 
     * @param type $userId
     * @param type $DestinataireId
     * @param type $AppName
     * @param type $EntityId
     * @param type $Code
     */
    public function AddNotify($userId, $code, $destinataireId = "", $AppName = "", $EntityId = "", $emailSubjet = "", $emailMessage = "") {
        NotifyHelper::AddNotify($this->Core, $userId, $code, $destinataireId, $AppName, $EntityId, $emailSubjet, $emailMessage);
    }

    /**
     * Obtient les notifications des applications
     * 
     * @param type $appName
     * @param type $EntityId
     */
    public function GetNotifyApp($appName, $EntityId) {
        return NotifyHelper::GetNotify($this->Core, $appName, $EntityId);
    }

    /**
     * Affiche les dernières notifications
     * 
     * @return string
     */
    public function GetInfo() {
        $view = "";

        //Obtient les dernière notifications
        $notifications = NotifyHelper::GetLastByUser($this->Core, $this->Core->User->IdEntite);

        if (count($notifications) > 0) {

            foreach ($notifications as $notification) {
                $app = \Core\Dashboard\DashBoardManager::GetApp($notification->AppName->Value, $this->Core);
               
                $view .= "<div class='notify'>";
                $view .= $app->GetDetailNotify($notification);

                $view .= "</div>";
            }

            return $view;
        } else {
            return $this->Core->GetCode("Notify.NoNotify");
        }
    }

    /**
     * Retourne le nombre de notification
     */
    function GetCount($recent = false) {
        $notify = NotifyHelper::GetByUser($this->Core, $this->Core->User->IdEntite, "", true);

        return count($notify);
    }

    /*
     * VUe par le membre
     */

    function ViewNotify() {
        $notify = NotifyHelper::ViewByUser($this->Core, $this->Core->User->IdEntite, "", true);
        echo $this->GetCount(true);
    }

    /**
     * Save the parameter
     */
    function SaveParameter() {

        $adminController = new AdminController($this->Core);
        $result = $adminController->SaveParameter(Request::GetPosts());

        if ($result === true) {
            return Response::Success($adminController->GetTabParameters());
        } else {
            return Response::Error(array("message" => $result));
        }
    }

    /*     * *
     * Delete a parameters
     */

    function DeleteParameter() {

        $adminController = new AdminController($this->Core);
        $result = $adminController->DeleteParameter(Request::GetPost("parameterId"));

        if ($result === true) {
            return Response::Success($adminController->GetTabParameters());
        } else {
            return Response::Error(array("message" => $result));
        }
    }

    /*     * *
     * Rafraichit la liste des plugin
     */

    function RefreshPlugin() {
        $adminController = new AdminController($this->Core);
        return $adminController->GetTabExtension();
    }

    /*     * *
     * Save the Template
     */

    function SaveTemplate() {

        $adminController = new AdminController($this->Core);
        $result = $adminController->SaveTemplate(Request::GetPosts());

        if ($result === true) {
            return Response::Success($adminController->GetTabTemplate());
        } else {
            return Response::Error(array("message" => $result));
        }
    }

    /*     * *
     * Delete a template
     */

    function DeleteTemplate() {

        $adminController = new AdminController($this->Core);
        $result = $adminController->DeleteTemplate(Request::GetPost("templateId"));

        if ($result === true) {
            return Response::Success($adminController->GetTabTemplate());
        } else {
            return Response::Error(array("message" => $result));
        }
    }

    /****
     * Test a template
     */
    function TestTemplate(){
        $adminController = new AdminController($this->Core);
       $result = $adminController->TestTemplate(Request::GetPosts());
    }
    
    /*     * *
     * Obtient les widget
     */

    public function GetWidget($type = "", $params = "") {
        switch ($type) {
            case "NotifyMenu" :
                $widget = new \Apps\Notify\Widget\NotifyMenu\NotifyMenu($this->Core);
                break;
        }

        return $widget->Render($params);
    }

    /**     *
     * Get cms Addable widget
     */
    public function GetListWidget() {
        return array(array("Name" => "NotifyMenu",
                "Description" => $this->Core->GetCode("Notify.DescriptionNotifyMenu")),
        );
    }
    
    
    public function GetLast(){
        
    }
    
}

?>
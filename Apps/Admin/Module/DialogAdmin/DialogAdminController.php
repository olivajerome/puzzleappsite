<?php

/*
 * PuzzleApp
 * Webemyos
 * J�r�me Oliva
 * GNU Licence
 */

namespace Apps\Admin\Module\DialogAdmin;

use Core\Controller\Controller;
use Core\View\View;
use Core\View\ElementView;
use Core\Core\Request;

use Apps\EeApp\Widget\WidgetUserForm\WidgetUserForm;

/*
 * 
 */
 class DialogAdminController extends Controller
 {
    /**
     * Constructeur
     */
    function _construct($core="")
    {
          $this->Core = $core;
    }

    /**
     * Creation
     */
    function Create()
    {
    }

    /**
     * Initialisation
     */
    function Init()
    {
    }

    /***
     * Dialog d'ajout d'app au tableau de bord
     */
    function ShowAddAppDasboard(){
        $view = new View(__DIR__."/View/addAppDashboard.tpl", $this->Core);

        $WidgetUserForm = new WidgetUserForm($this->Core);

        $view->AddElement(new ElementView("widgetUserForm", $WidgetUserForm->Render()));


        return $view->Render();
    }

          /*action*/
 }?>
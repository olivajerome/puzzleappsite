<div class='row-fluid span12'>
    <div class='span11' id='Tools'>
        {{btnMyApp}}
        {{btnShowApp}}
        {{btnShowTemplate}}
        {{btnShowPlugin}}
        {{btnStore}}
        
        {{btnAdmin}}
        
    <br/>
    <div class='marginTop'>
        {{GetCode(Base.YourVersionSysteme)}} : {{frameworkVersion}}

        {{GetControl(Button,btnUpdateFramework,{LangValue=EeApp.UpdateFramework,CssClass=btn btn-warning,Id=btnUpdateFramework})}}</p>
    </div>
    
    </div>
    
    
    <div class='col-mid-12 alignCenter' id='dvDesktop'>
          <h4 class='blueOne'>{{GetCode(EeApp.TitleHome)}}</h4>
            <p>{{GetCode(EeApp.MessageHome)}}</p>
    </div>
</div>

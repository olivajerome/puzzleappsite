CREATE TABLE IF NOT EXISTS `ComunityMessage` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`Type` INT  NULL ,
`UserId` INT  NULL ,
`Message` TEXT  NULL ,
`DateCreated` DATE  NULL ,
`ComunityId` INT  NULL ,
PRIMARY KEY (`Id`),
CONSTRAINT `ee_user_ComunityMessage` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`),
CONSTRAINT `ComunityComunity_ComunityMessage` FOREIGN KEY (`ComunityId`) REFERENCES `ComunityComunity`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
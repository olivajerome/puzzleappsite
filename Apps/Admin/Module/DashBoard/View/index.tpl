<div class='dashboard'>

    <div class='alignRight'>
        <i id='btnAddWidget'  class='fa fa-plus' title ="{{GetCode(Admin.AddWidget)}}"></i>
    </div>

    <div id='widgets'></div>
</div>

<script>
    Admin.Init();
    Admin.LoadWidget();
</script>




CREATE TABLE IF NOT EXISTS `AnnonceResponse` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`AnnonceId` INT  NOT NULL,
`UserId` INT  NOT NULL,
`Response` TEXT  NOT NULL,
`Status` INT  NOT NULL,
PRIMARY KEY (`Id`),
CONSTRAINT `AnnonceAnnonce_AnnonceResponse` FOREIGN KEY (`AnnonceId`) REFERENCES `AnnonceAnnonce`(`Id`),
CONSTRAINT `ee_user_AnnonceResponse` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
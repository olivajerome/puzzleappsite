<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\RoadMap;

 
 use Core\Core\Core;
 use Core\Core\Request;
 use Apps\Base\Base;
 
 
 use Apps\RoadMap\Module\RoadMap\RoadMapController;
 use Apps\RoadMap\Module\Member\MemberController;
 use Apps\RoadMap\Helper\RoadMapHelper;
 
 
class RoadMap extends Base
{
	/**
	 * Auteur et version
	 * */
	public $Author = 'Webemyos';
	public $Version = '2.1.0.0';
   
	/**
	 * Constructeur
	 * */
	function __construct($core)
	{
        $this->Core = Core::getInstance();  
		parent::__construct($this->Core, "RoadMap");
	}

	 /**
	  * Execution de l'application
	  */
	 function Run()
	 {
		echo parent::RunApp($this->Core, "RoadMap", "RoadMap");
	 }
     
     /***
      * Run Member app
      */
     function RunMember(){
        
        $memberController = new  MemberController($this->Core);
        return $memberController->Index();
     }

     /**
	 * Définie les routes publiques
	 */
	function GetRoute($routes ="")
	{
		parent::GetRoute(array());
		return $this->Route;
  	}
    
    /***
     * Sauvegarde une RoadMap  
     **/
    function SaveRoadMap(){
        return RoadMapHelper::SaveRoadMap($this->Core, Request::GetPosts());
    }
    
    /***
     * Supprime une RoadMap
     */
    function DeleteRoadMap(){
        return RoadMapHelper::DeleteRoadMap($this->Core, Request::GetPost("roadMapId"));
    }
    
    /***
     * Charge les colonnes et les tickets d'une RoadMap
     **/
    function Load(){
        
        $roadMapController = new  RoadMapController($this->Core);
        return $roadMapController->Load(Request::GetPost("roadMapId"));
    }
    
    
    /***
     * Sauvegarde une Colonne  
     **/
    function SaveColumn(){
    
        return RoadMapHelper::SaveColumn($this->Core, Request::GetPosts());
    }
    
     /***
     * Sauvegarde un Ticket  
     **/
    function SaveTicket(){
    
        return RoadMapHelper::SaveTicket($this->Core, Request::GetPosts());
    }
    
    /***
     * Changhe un ticket de colonne
     */
    function ChangeColumn(){
        return RoadMapHelper::ChangeColumn($this->Core, Request::GetPosts());
    }
    
    /***
     * Supprime une colone
     */
    function DeleteColumn(){
        return RoadMapHelper::DeleteColumn($this->Core, Request::GetPost("columnId"));
    }
    
    /***
     * Selectionne et affiche le bon widget
     */
    function GetWidget($type, $params){
        
        switch($type){
            case "GetShared" : 
                $widget = new \Apps\RoadMap\Widget\GetShared\GetShared($this->Core);
                break;
        }
        
        return $widget->Index($params);
    }
    
    /***
     * Charges les roadMaps de l'utilisateur
     */
    function LoadRoadMaps(){
        $roadMapController = new  RoadMapController($this->Core);
        return $roadMapController->LoadRoadMaps();
    }
    
    /***
     * Parametrage
     */
    function GetParameterMenu(){
        $roadMapController = new  RoadMapController($this->Core);
        return $roadMapController->GetParameterMenu();
    }
    
    /***
     * Supprime un ticket
     */
    function DeleteTicket(){
          return RoadMapHelper::DeleteTicket($this->Core, 
                                             Request::GetPost("ticketId"));
    }
    
    
}
?>
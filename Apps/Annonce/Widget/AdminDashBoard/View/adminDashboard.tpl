<div class='col-md-4'>
    <div class='block widgetDashboard'>
        <i class='fa fa-trash removeWidget' data-app='Annonce' title='{{GetCode(EeApp.RemoveWidgetDahboard)}}'></i>
        <i class='fa fa-list title'>&nbsp;{{GetCode(Annonce.Annonce)}}</i>
            
        <ul>
            {{foreach Annonces}}
                <li>  
                   {{element->Title->Value}} 
                    <a target='_blank' href='{{GetPath(/Annonce/Annonce/{{element->Code->Value}})}}'
                    <i class='fa fa-link'></i>
                    </a>
                </li>
            {{/foreach Annonces}}
        </ul>
    </div>
</div>

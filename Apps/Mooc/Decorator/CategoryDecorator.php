<?php 

namespace Apps\Mooc\Decorator;

use Core\View\ElementView;
use Core\View\View;

class CategoryDecorator{

    public function __construct($core){
        $this->Core = $core;
    }

    /***
     * Render category and is lesson
     */
    public function RenderCategory($category){

        $view = new View(__DIR__."/View/category.tpl", $this->Core);
        $view->AddElement(new ElementView("Category", $category));
        $view->AddElement(new ElementView("Mooc", $category->GetMooc()));
        return $view->Render();
    }
}
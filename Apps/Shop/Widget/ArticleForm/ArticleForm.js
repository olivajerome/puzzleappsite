var ArticleForm = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
ArticleForm.Init = function(){
    Event.AddById("btnSave", "click", ArticleForm.SaveElement);
};

/***
* Obtient l'id de l'itin�raire courant 
*/
ArticleForm.GetId = function(){
    return Form.GetId("ArticleFormForm");
};

/*
* Sauvegare l'itineraire
*/
ArticleForm.SaveElement = function(e){
   
    if(Form.IsValid("ArticleFormForm"))
    {

    var data = "Class=ArticleForm&Methode=SaveElement&App=ArticleForm";
        data +=  Form.Serialize("ArticleFormForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.statut == "Error"){
                Form.RenderError("ArticleFormForm", data.message);
            } else{

                Form.SetId("ArticleFormForm", data.data.Id);
            }
        });
    }
};


<section class='container' >
    <div class='row'>
        <h1 class='borderBottom' >{{GetCode(Mooc.TheTutorials)}}</h1>

        {{foreach Mooc}}

        <div class='col-md-3'>

            <div class='mooc'>
                <span style='display:block; width:100%; height: 150px; background-size: cover;background-image: url({{element->GetImageFullPath()}})'>
                </span>

                <h3>{{element->Name->Value}}
                    <p style='font-size:8px'>
                        {{element->DateCreated->Value}}
                    </p>
                </h3>

                        <p>{{element->Description->Value}}</p>

                        <div class='center'>
                        <a href ='{{GetPath(/Mooc/Mooc/{{element->Code->Value}})}}' >
                            {{GetCode(Mooc.StartThisMooc)}}
                        </a>
                        </div>
            </div>
        </div>

        {{/foreach Mooc}}

    </div>
</section>
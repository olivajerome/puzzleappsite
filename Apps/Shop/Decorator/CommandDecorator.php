<?php

namespace Apps\Shop\Decorator;

use Apps\Shop\Entity\ShopCommandLine;

class CommandDecorator{

    /***
     * Render the Address of command
     */
    public static function GetAddress($command){
        
        $address = json_decode($command->Adress->Value);
        
        $view .= "<h3>".$command->Core->GetCode("Shop.DeliveryAddress")."</h3>";
        
        $view .= "<div><label>".$command->Core->GetCode("Shop.Address")." : </label>";
        $view .= $address->address . "</div>";
        
        $view .= "<div><label>".$command->Core->GetCode("Shop.ZipCode")." : </label>";
        $view .= $address->zipCode . "</div>";
        
        $view .= "<div><label>".$command->Core->GetCode("Shop.City")." : </label>";
        $view .= $address->city . "</div>";
      
        $address = json_decode($command->AdressFacturation->Value);
      
        
        $view .= "<h3>".$command->Core->GetCode("Shop.BillingAddress")."</h3>";
        
        $view .= "<div><label>".$command->Core->GetCode("Shop.Address")." : </label>";
        $view .= $address->address . "</div>";
        
        $view .= "<div><label>".$command->Core->GetCode("Shop.ZipCode")." : </label>";
        $view .= $address->zipCode . "</div>";
        
        $view .= "<div><label>".$command->Core->GetCode("Shop.City")." : </label>";
        $view .= $address->city . "</div>";
       
        return $view;   
    }

    /***
     * Get the product And the prices
     */
    public static function GetProducts($command){

        $commandLine = new ShopCommandLine($command->Core);
        $lines =  $commandLine->Find("CommandId=" . $command->IdEntite);    

        if(count($lines)> 0){
           
            $view = "<table class='cartTable grid'>";
            $view .= "<tr><th>".$command->Core->GetCode("Shop.Product")."</th><th>".$command->Core->GetCode("Shop.Price")."</th></tr>";
            
            foreach($lines as $line){
                
                
                 switch($line->EntityName->Value){
                     
                     case "ShopProduct" :
                         $product = new \Apps\Shop\Entity\ShopProduct($command->Core);
                         $product->GetById($line->EntityId->Value);
                         break;
                 }   
                
                 $view .= "<tr><td>".$product->Name->Value."</td><td style='text-align:right'>".$line->Price->Value." &euro;</td></tr>";
                 
                 $total += (float)$line->Price->Value;
            }
            
            $view .= "<tr><td><b>Total</b></td><td style='text-align:right'><b>".$total."&euro; </b></td></td></tr>";
            
            $view .= "</table>";
        }

        return $view;
    }

    public static function GetImageProduct(){

    }
}
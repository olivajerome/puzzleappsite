CREATE TABLE IF NOT EXISTS `FolderFolderShared` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`FolderId` INT  NOT NULL,
`UserId` INT  NOT NULL,
PRIMARY KEY (`Id`),
CONSTRAINT `FolderFolder_FolderFolderShared` FOREIGN KEY (`FolderId`) REFERENCES `FolderFolder`(`Id`),
CONSTRAINT `ee_user_FolderFolderShared` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
<?php


namespace Apps\RoadMap\Decorator;

use Apps\RoadMap\Entity\RoadMapTicket;
use Core\Utility\Format\Format;

class RoadMapDecorator{

    public function GetTickets($column){
    
        $ticket = new RoadMapTicket($column->Core);
        $html ="";
        
        foreach($ticket->Find("ColumnId=" . $column->IdEntite) as $ticket)  {
              $html .= "<div id='".$ticket->IdEntite."' class='ticket'>";
              $html .= "<h3>".$ticket->Name->Value."</h3>";
              $html .= "<p>".Format::Tronquer($ticket->Description->Value, 50)."</p>";
            
              $html .= "<i class='fa fa-edit editTicket'>&nbsp;</i>";
              $html .= "<i class='fa fa-trash deleteTicket'>&nbsp;</i>";
            
              $html .= "</div>";
        }
    
       return $html;
    }
}
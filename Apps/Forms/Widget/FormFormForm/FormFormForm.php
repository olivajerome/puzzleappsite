<?php

namespace Apps\Forms\Widget\FormFormForm;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;

class FormFormForm extends Form {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "") {
        $this->Core = $core;
        $this->Data = $data;

        //On le fait ici pour avoir le formuaire � l'affichage mais aussi � la validation
        $this->Init($data);
    }

    /**
     * Initialisation 
     */
    function Init($data) {
        $this->form = new Form("FormFormFormForm", $data);

        $this->form->SetView(__DIR__ . "/View/index.tpl", $this->Core);
        $this->form->SetAction("/View/index.tpl");

        $this->form->Add(array("Type" => "Hidden",
            "Id" => "Id",
        ));
        
        $this->form->Add(array("Type" => "TextBox",
            "Id" => "Libelle",
            "Validators" => ["Required"]
        ));
        
        $this->form->Add(array("Type" => "TextArea",
            "Id" => "Commentaire",
            "Validators" => ["Required"]
        ));

        $this->form->Add(array("Type" => "Button",
            "Id" => "btnSaveForm",
            "Value" => $this->Core->GetCode("Save"),
            "CssClass" => "btn btn-primary"
        ));
        
        $this->form->Add(array("Type" => "ListBox",
            "Id" => "questionType",
            "Values" => array($this->Core->GetCode("Forms.QuestionTypeText") => 0 , 
                              $this->Core->GetCode("Forms.Paragraphe") => 1,
                              $this->Core->GetCode("Forms.CheckBox") => 2,
                              $this->Core->GetCode("Forms.Radio") => 3,
                              $this->Core->GetCode("Forms.List")=> 4,
                
                )
            ));
        
            if($data->IdEntite != ""){
                $this->form->AddElementView(new ElementView("Questions",  $data->GetQuestions()));
            } else{
                $this->form->AddElementView(new ElementView("Questions",  array()));
           
            }
    }

    /*     * *
     * G�n�rer les formulaire
     */

    function Render() {
        return $this->form->Render();
    }

    /*     * *
     * Valide les donn�es selon les diff�rents formulaires
     */

    function Validate($data = "") {
        $errors = $this->form->Validate($this->Data);

        return $errors;
    }

    /*     * *
     * Charge les controls avec les donn�es de l'entit�
     */

    function Load($entity) {
        $this->form->Load($entity);
    }

    /*     * *
     * Charge l'entit� avec les donn�es des diff�rents formulaire
     */

    function Populate($entity) {
        
    }

    /*     * *
     * Gestion des erreurs
     */

    function Error() {
        $errors[] = $this->form->errors;

        return json_encode(array("statut" => "Error", "message" => $errors));
    }

}

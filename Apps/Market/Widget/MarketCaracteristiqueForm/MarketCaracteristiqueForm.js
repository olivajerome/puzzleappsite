var MarketCaracteristiqueForm = function(){
   
};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
MarketCaracteristiqueForm.Init = function(callBack){
    Event.AddById("btnSaveCaracteristique", "click", MarketCaracteristiqueForm.SaveElement);
    Event.AddById("btnAddItem", "click", MarketCaracteristiqueForm.AddItem);
    MarketCaracteristiqueForm.callBack = callBack;
};

/***
* Obtient l'id de l'itin�raire courant 
*/
MarketCaracteristiqueForm.GetId = function(){
    return Form.GetId("MarketCaracteristiqueForm");
};

/*
* Sauvegare l'itineraire
*/
MarketCaracteristiqueForm.SaveElement = function(e){
   
    if(Form.IsValid("MarketCaracteristiqueForm"))
    {

    var data = "Class=Market&Methode=SaveCaracteristique&App=Market";
        data +=  Form.Serialize("MarketCaracteristiqueForm");


        let items = document.querySelectorAll("#caracteristiqueItem input");
        let itemsData = Array();

        for(var i = 0; i < items.length ; i ++){
            itemsData.push({id:items[i].id, label : items[i].value });
        }

        data += "&items="+ JSON.stringify(itemsData);

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.statut == "Error"){
                Form.RenderError("MarketCaracteristiqueForm", data.message);
            } else{

                Form.SetId("MarketCaracteristiqueForm", data.data.Id);
                Dialog.Close();
                
                 
                if(MarketCaracteristiqueForm.callBack){
                    MarketCaracteristiqueForm.callBack(data);
                }
            }
        });
    }
};

/***
 * Ajout un item
 */
MarketCaracteristiqueForm.AddItem = function(e){
    let addItem = "<input type='text' id='0' style='width:90%;margin-right:5px' class='form-control'/>";
        addItem += "<i class='fa fa-trash removeCaracteristique' onclick='MarketCaracteristiqueForm.RemoveItem(this)' ></i>";

    let addItemContainer = document.createElement("div");
    addItemContainer.innerHTML = addItem;
    addItemContainer.style.display = "flex";
    addItemContainer.style.marginTop = "5px";

    let caracteristiqueItem = document.getElementById("caracteristiqueItem");
    caracteristiqueItem.appendChild(addItemContainer);
};

/***
 * Remove a itme
 */
MarketCaracteristiqueForm.RemoveItem = function(control) {

    let itemId = control.parentNode.querySelector("input").id;

    if(itemId != 0){
        let data = "Class=Market&Methode=RemoveCaracteristiqueItem&App=Market";
            data += "&itemId=" + itemId;    

            Request.Post("Ajax.php", data).then(data => {
                let container = control.parentNode;
                container.parentNode.removeChild(container);    
                        
            });
    }

};

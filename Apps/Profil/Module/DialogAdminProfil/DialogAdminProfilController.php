<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Profil\Module\DialogAdminProfil;

use Core\Controller\Controller;
use Core\Entity\User\User;

use Apps\Profil\Widget\ProfilForm\ProfilForm;

class DialogAdminProfilController extends Controller{

    /***
     * Edit a user
     */
    function EditUser($userId){
      
        $user = new User($this->Core);
        $user->GetById($userId);
        
        $profilForm = new ProfilForm($this->Core);
        $profilForm->Load($user);
        $profilForm->form->LoadImage($user);
        
        return $profilForm->Render();
    }
}
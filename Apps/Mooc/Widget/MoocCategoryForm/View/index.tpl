{{form->Open()}}
{{form->Error()}}
{{form->Success()}}

{{form->Render(Id)}}
    <div>
        <label>{{GetCode(Mooc.CategoryName)}}</label> 
        {{form->Render(Name)}}
    </div>
    <div>
        <label>{{GetCode(Mooc.CategoryDescription)}}</label> 
        {{form->Render(Description)}}
    </div>
    <div class='center marginTop' >   
        {{form->Render(btnSaveCategory)}}
    </div>  

{{form->Close()}}
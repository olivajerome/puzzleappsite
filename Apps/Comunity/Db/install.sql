CREATE TABLE IF NOT EXISTS `ComunityComunity` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`Name` varchar(200)  NULL ,
`Type` INT  NULL ,
`UserId` INT  NULL ,
`DateCreated` DATE  NULL ,
`Title` TEXT  NULL ,
`SubTitle` TEXT  NULL ,
`Description` TEXT  NULL ,
`Status` INT  NULL ,
`DateClotured` DATE  NULL ,
`DateLastMessage` DATETIME  NULL ,
PRIMARY KEY (`Id`),
CONSTRAINT `ee_user_ComunityComunity` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `ComunityUser` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`ComunityId` INT  NULL ,
`UserId` INT  NULL ,
`Status` INT  NULL ,
`Readed` INT  NULL ,
PRIMARY KEY (`Id`),
CONSTRAINT `ee_user_ComunityUser` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `ComunityMessage` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`Type` INT  NULL ,
`UserId` INT  NULL ,
`Message` TEXT  NULL ,
`DateCreated` DATE  NULL ,
`ComunityId` INT  NULL ,
PRIMARY KEY (`Id`),
CONSTRAINT `ee_user_ComunityMessage` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`),
CONSTRAINT `ComunityComunity_ComunityMessage` FOREIGN KEY (`ComunityId`) REFERENCES `ComunityComunity`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `ComunityComment` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`UserId` INT  NULL ,
`Message` TEXT  NULL ,
`DateCreated` DATE  NULL ,
`MessageId` INT  NULL ,
`ParentId` INT  NULL ,
`ParentType` INT  NULL ,
PRIMARY KEY (`Id`),
CONSTRAINT `ComunityMessage_ComunityComment` FOREIGN KEY (`MessageId`) REFERENCES `ComunityMessage`(`Id`),
CONSTRAINT `ee_user_ComunityComment` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `ComunityLike` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`UserId` INT  NULL ,
`DateCreated` DATE  NULL ,
`AppName` VARCHAR(200)  NULL ,
`AppId` INT  NULL ,
`EntityName` VARCHAR(200)  NULL ,
`EntityId` INT  NULL ,
PRIMARY KEY (`Id`),
CONSTRAINT `ee_user_ComunityLike` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `ComunityFollow` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`ComunityId` INT  NULL ,
`UserId` INT  NULL ,
PRIMARY KEY (`Id`),
CONSTRAINT `ee_user_ComunityFollow` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`),
CONSTRAINT `ComunityComunity_ComunityFollow` FOREIGN KEY (`ComunityId`) REFERENCES `ComunityComunity`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
<?php

namespace Apps\WorkFlow\Widget\WorkFlowTriggerActionForm;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;

use Apps\WorkFlow\Entity\WorkFlowTrigger;

class WorkFlowTriggerActionForm extends Form {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "") {
        $this->Core = $core;
        $this->Data = $data;

        $this->Init($data);
    }

    /**
     * Initialisation 
     */
    function Init($data) {
        $this->form = new Form("WorkFlowTriggerActionFormForm", $data);

        $this->form->SetView(__DIR__ . "/View/index.tpl", $this->Core);
        $this->form->SetAction("/View/index.tpl");

        $this->form->Add(array("Type" => "Hidden",
            "Id" => "Id",
        ));
      
        $this->form->Add(array("Type" => "Hidden",
            "Id" => "WorkFlowId",
        ));
  
        $this->form->Add(array("Type" => "TextBox",
            "Id" => "Name",
            "Validators" => ["Required"]
        ));
        
        $Apps = \Apps\EeApp\Helper\AppHelper::GetAll($this->Core);
        $events = array();

        foreach($Apps as $app){
        
            $appName = $app->Name->Value;
            $path = "\\Apps\\" . $appName . "\\" . $appName;
            $app = new $path($core);
            $listEvent = $app->GetListEvent();
           
            foreach($listEvent as $event){
                $events[$appName . "-" .$event["Name"]] = $appName . "-" . $event["Name"];
            }
        }

        $this->form->Add(array("Type" => "ListBox",
            "Id" => "Event",
            "Values" => $events,
            "Validators" => ["Required"]
        ));

        $this->form->Add(array("Type" => "Button",
            "Id" => "btnSaveTriggerAction",
            "Value" => $this->Core->GetCode("WorkFlow.SaveTriggerAction"),
            "CssClass" => "btn btn-primary"
        ));
    }

    /*     * *
     * Render the html form
     */

    function Render() {
        return $this->form->Render();
    }

    /*     * *
     * Validate the data
     */

    function Validate($data = "") {
        $errors = $this->form->Validate($this->Data);

        return $errors;
    }

    /*     * *
     * Load control with the entity
     */

    function Load($entity) {
        $this->form->Load($entity);
    }

    /*     * *
     * Populate the entity with the form Data
     */

    function Populate($entity) {
        
    }

    /*     * *
     * Error message
     */

    function Error() {
        $errors[] = $this->form->errors;

        return json_encode(array("statut" => "Error", "message" => $errors));
    }

}

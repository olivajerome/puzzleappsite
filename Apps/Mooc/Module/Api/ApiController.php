<?php
/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Mooc\Module\Api;

use Core\Controller\Controller;

use Apps\Mooc\Entity\MoocLesson;
use Apps\Mooc\Entity\MoocMooc;
use Apps\Mooc\Helper\MoocHelper;

 class ApiController extends Controller
 {
    /**
     * Constructeur
     */
    function __construct($core="")
    {
            $this->Core = $core;
    }

    /***
     * GetList Of Mooc
     */
    function GetListMooc(){
     
        $Mooc = new MoocMooc($this->Core);
        $Moocs  = $Mooc->GetAll();

        return $Mooc->ToAllArray($Moocs); 
    }
}
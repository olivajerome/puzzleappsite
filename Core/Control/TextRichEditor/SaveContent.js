class SaveContent extends BaseTool {
    getIcone() {
        return "<i class='fa fa-check'></i>";
    }

    getName() {
        return Language.GetCode("Base.TextRichEditorSaveContent");
    }
    execute(e) {

        this.editor.clearBeforeSave();

        for (let event in this.events) {
            if (this.events[event]["type"] == e.type) {
                this.events[event]['handler'](this.editor);
            }
        }
    }
}

<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\PuzzleApp\Module\DialogPuzzle;

use Core\Control\Button\Button;
use Core\Controller\Controller;
use Core\View\View;

/*
 * 
 */
 class DialogPuzzleController extends Controller
 {
    /**
     * Constructeur
     */
    function _construct($core="")
    {
          $this->Core = $core;
    }

    /**
     * Create
     */
    function Create()
    {
    }

    /**
     * Init
     */
    function Init()
    {
    }

    /**
     * Render
     */
    function Show($all=true)
    {
         return $this->Index();
    }
    
    /*
    * Get the home page
    */
   function Index()
   {
       $view = new View(__DIR__."/View/index.tpl", $this->Core);
       return $view->Render();
   }
       
    function GetSimpleContent(){
        return "<h1>Simple Content</h1><p>Lorem Isum</p>";
    }
          /*action*/
 }?>
CREATE TABLE IF NOT EXISTS `FolderFiles` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`UserId` INT  NOT NULL,
`Name` TEXT  NOT NULL,
`Code` TEXT  NOT NULL,
`Url` TEXT  NOT NULL,
`Description` TEXT  NOT NULL,
`FolderId` INT NULL,
`DateCreated` DATE  NOT NULL,
`DateUpdated` DATE  NOT NULL,
`Type` INT  NOT NULL,
PRIMARY KEY (`Id`),
CONSTRAINT `ee_user_FolderFiles` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`),
CONSTRAINT `FolderFolder_FolderFiles` FOREIGN KEY (`FolderId`) REFERENCES `FolderFolder`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
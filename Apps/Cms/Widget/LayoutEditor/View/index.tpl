 <div class='editBlock' data-block="header">
    <label>{{GetCode(Cms.Header)}}</label>
    <div class='floatRight'>
        <i class='fa fa-plus btnAddBlock' title='{{GetCode(Cms.AddBlock)}}'></i>
    </div>
    <div class='row subBlockContainer'>
        {{foreach HeaderBlock}}
            <div class='contentBlock col-md-{{element->Size->Value}} subBlock' id='{{element->IdEntite}}'>
                <div class='tool'>
                    <i class='fa fa-edit btnEditBlock' title='{{GetCode(Cms.EditBlock)}}' ></i>
                    <i class='fa fa-remove btnRemoveBlock' title='{{GetCode(Cms.RemoveBlock)}}' ></i>
                </div>        
                {{Apps\Cms\Decorator\Editor\EditorDecorator->GetBlock(element)}}
            </div>
        {{/foreach HeaderBlock}}
    </div>

</div>
<div class='editBlock'>
    <label>{{GetCode(Cms.Body)}}</label>
</div>
<div class='editBlock' data-block="footer" >
    <label>{{GetCode(Cms.Footer)}}</label>
    <div class='floatRight'>
        <i class='fa fa-plus btnAddBlock' title='{{GetCode(Cms.AddBlock)}}'></i>
    </div>
    <div class='row subBlockContainer'>
     {{foreach FooterBlock}}
        <div class='contentBlock col-md-{{element->Size->Value}} subBlock' id='{{element->IdEntite}}'>
            <div class='tool'>
                    <i class='fa fa-edit btnEditBlock' title)='{{GetCode(Cms.EditBlock)}}' ></i>
                    <i class='fa fa-remove btnRemoveBlock' title='{{GetCode(Cms.RemoveBlock)}}' ></i>
                </div>        
                {{Apps\Cms\Decorator\Editor\EditorDecorator->GetBlock(element)}}
        </div>
    {{/foreach FooterBlock}}
    </div>
</div>

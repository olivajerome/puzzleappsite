<?php

namespace Apps\Shop\Widget\AdminDashBoard;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;
use Core\Entity\Entity\Argument;
use Apps\Shop\Entity\ShopCommand;

class AdminDashBoard {
    
    public function __construct($core) {
        $this->Core = $core;
    }
    
    /***
     * Render the widget
     */
    public function Render(){
        $view = new View(__DIR__ . "/View/adminDashboard.tpl", $this->Core);
       
        $command = new ShopCommand($this->Core);
        $commands =  $command->Find(" Status > 0");

        $newCommand = 0;
        $toSend = 0;
        $sended = 0;
        
        
        foreach($commands as $command) {

            switch($command->Status->Value){
                case 1 : $newCommand ++ ;break;
                case 2 : $toSend ++ ; break;
                case 3 : $sended ++ ; break;
            }
        }

        $view->AddElement(new ElementView("newCommand", $newCommand ));
        $view->AddElement(new ElementView("toSend", $toSend ));
        $view->AddElement(new ElementView("sended", $sended ));

        return $view->Render();
    }
}

<?php 
/*
* PuzzleApp
* Webemyos
* Jérôme Oliva
* GNU Licence
*/

namespace Apps\EeApp\Entity;
use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class EeAppPluginApp extends Entity  
{
	//Constructeur
	function __construct($core)
	{
		//Version
		$this->Version ="2.0.0.0"; 

		//Nom de la table 
		$this->Core=$core; 
		$this->TableName="EeAppPluginApp"; 
		$this->Alias = "EeAppPluginApp"; 

		$this->AppId = new Property("AppId", "AppId", NUMERICBOX,  true, $this->Alias); 
		$this->PluginId = new Property("PluginId", "PluginId", NUMERICBOX,  true, $this->Alias); 

		//Creation de l entité 
		$this->Create(); 
	}
}
?>
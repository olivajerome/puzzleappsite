var NewsLetterListEmailForm = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
NewsLetterListEmailForm.Init = function(callBack){

    NewsLetterListEmailForm.CallBack = callBack;

    Event.AddById("btnSaveListEmail", "click", NewsLetterListEmailForm.SaveElement);
    Event.AddById("btnAddEmail", "click", NewsLetterListEmailForm.AddEmailToList);
    
    let Emails = Dom.GetById("Emails").value.split(";");
    
    for(var i = 0; i < Emails.length; i++){
        NewsLetterListEmailForm.AddEmailInList(Emails[i]);
    }
    
    Animation.Hide("Emails");
};

/***
* Obtient l'id de l'itin�raire courant 
*/
NewsLetterListEmailForm.GetId = function(){
    return Form.GetId("NewsLetterListEmailFormForm");
};

/***
 * Add valid email to the list
 * @param {type} email
 * @returns {undefined}
 */
NewsLetterListEmailForm.AddEmailInList = function(email){
    
      let listEmail = Dom.GetById("listEmail");
       listEmail.innerHTML += "<li><span>" +email+ "</span><i class='fa fa-trash' onclick='NewsLetterListEmailForm.RemoveToList(this)'></i> </li>";
};

/**
 * Delete email to the list
 * @param {type} element
 * @returns {undefined}
 */
NewsLetterListEmailForm.RemoveToList = function(element){
    let container = element.parentNode;
    container.parentNode.removeChild(container);
};

/***
 * Add Email to the list
 * @returns {undefined}
 */
NewsLetterListEmailForm.AddEmailToList = function(){
    
    let emailToAdd = Dom.GetById("emailToAdd").value;
   
    if(EmailBox.Verify(emailToAdd)){
    
        NewsLetterListEmailForm.AddEmailInList(emailToAdd);
    
    } else { 
        Animation.Notify(Language.GetCode("NewsLetter.EmailInvalid"));
    }
};


/*
* Sauvegare l'itineraire
*/
NewsLetterListEmailForm.SaveElement = function(e){
   
   let Emails = Dom.GetById("Emails");
       Emails.value = "";
            
   let newEmails = document.querySelectorAll("#listEmail li span");  
   let emails = Array(); 
    
    for(let i= 0; i < newEmails.length;i++){
        emails.push(newEmails[i].innerHTML);
    }
    
    Emails.value = emails.join(";");
    
    if(Form.IsValid("NewsLetterListEmailFormForm"))
    {

          
        

    var data = "Class=NewsLetter&Methode=SaveListEmail&App=NewsLetter";
        data +=  Form.Serialize("NewsLetterListEmailFormForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.statut == "Error"){
                Form.RenderError("NewsLetterListEmailFormForm", data.message);
            } else{

                Form.SetId("NewsLetterListEmailFormForm", data.data.Id);
            }
            
            Dialog.Close();

            if (NewsLetterListEmailForm.CallBack) {
                NewsLetterListEmailForm.CallBack(data);
            }
        });
    }
};




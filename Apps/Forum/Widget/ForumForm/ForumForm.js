var ForumForm = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
ForumForm.Init = function(){
    Event.AddById("btnSave", "click", ForumForm.SaveElement);
    Event.AddById("btnAddPlugin", "click", ForumForm.AddPlugin);
    Event.AddByClass("removePlugin", "click" , ForumForm.RemovePlugin);
};

/***
* Obtient l'id de l'itin�raire courant 
*/
ForumForm.GetId = function(){
    return Form.GetId("ForumFormForm");
};

/*
* Sauvegare l'itineraire
*/
ForumForm.SaveElement = function(e){
   
    if(Form.IsValid("ForumFormForm"))
    {

    let data = "Class=Forum&Methode=SaveForum&App=Forum";
        data +=  Form.Serialize("ForumFormForm");

        Request.Post("Ajax.php", data).then(data => {
            
            if(Form.GetValue("ForumFormForm", "Id") == ""){
                Dashboard.StartApp('', 'Forum', '');
            } else {
               Animation.Notify(Language.GetCode("Forum.ForumSaved"));
           }
        });
    }
};

/***
 * Ajout d'un plugin
 */
ForumForm.AddPlugin = function(e){
  
    e.preventDefault();
    
     Dialog.open('', {"title": Dashboard.GetCode("Forum.AddPlugin"),
     "app": "EeApp",
     "class": "DialogEeApp",
     "method": "AddPluginApp",
     "params": "Forum",
     "type" : "right"
     });
 };
 
 /***
  * Remove plugin to the shop 
  * @returns {undefined}
  */
 ForumForm.RemovePlugin = function(e){
     
     Animation.Confirm(Language.GetCode("Forum.RemovePlugin"), function(){
         let container = e.srcElement.parentNode;
         
         let  data = "Class=EeApp&Methode=RemovePluginApp&App=EeApp";
              data += "&pluginId="+e.srcElement.id;
              
         Request.Post("Ajax.php", data).then(data => {
             container.parentNode.removeChild(container);
         });
     });
 };
 
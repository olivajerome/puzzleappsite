<?php
/**
 * Outil pour sauvegarder l'application
 */
class PinnerMenu extends Tools
{
    //Affiche le control
    public function Render()
    {
        $this->Icone = "fa fa-list";
        $this->Title = $this->Core->GetCode("Ide.AddToMenu");
        $this->OnClick = "IdeTool.AddToMenu();";
        
        return parent::Render();
    }
}

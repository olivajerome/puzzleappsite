<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Forms\Module\DialogAdminForm;

use Core\Control\Button\Button;
use Core\Controller\AdministratorController;
use Apps\Forms\Entity\FormForm;
use Apps\Forms\Widget\FormFormForm\FormFormForm;

class DialogAdminFormController extends AdministratorController{

    /***
     * Ajout d'un Mooc
     */
    function AddForm($formId){
      
        $form = new FormForm($this->Core);
        if($formId != ""){
            $form->GetById($formId);
        }
        
        $formForm = new FormFormForm($this->Core, $form);
        $formForm->Load($form);
        //$moocMoocForm->form->LoadImage($form);
        
        return $formForm->Render();
    }
}
<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Annonce\Module\DialogAdminAnnonce;

use Core\Control\Button\Button;
use Core\Controller\AdministratorController;
use Core\View\View;

use Apps\Annonce\Widget\AnnonceCategoryForm\AnnonceCategoryForm;
use Apps\Annonce\Entity\AnnonceCategory;

use Apps\Annonce\Widget\AnnonceAdminForm\AnnonceAdminForm;
use Apps\Annonce\Entity\AnnonceAnnonce;

/*
 * 
 */
 class DialogAdminAnnonceController extends AdministratorController
 {
    /**
     * Constructeur
     */
    function _construct($core="")
    {
          $this->Core = $core;
    }

    /**
     * Create
     */
    function Create()
    {
    }

    /**
     * Init
     */
    function Init()
    {
    }

    /**
     * Render
     */
    function Show($all=true)
    {
         return $this->Index();
    }
    
    /*
    * Get the home page
    */
   function Index()
   {
       $view = new View(__DIR__."/View/index.tpl", $this->Core);
       return $view->Render();
   }
   
   /***
    * Add a category
    */
   function AddCategory($categoryId){
       $annonceCategoryForm = new AnnonceCategoryForm($this->Core);
       
        if($categoryId != ""){
           $category = new AnnonceCategory($this->Core);
           $category->GetById($categoryId);
           $annonceCategoryForm->Load($category);
           $annonceCategoryForm->form->LoadImage($category);
       }
       
       return $annonceCategoryForm->Render();
   }
   
     /**
     * Edite annonce
     */
    function EditAnnonce($annonceId){
 
        $annonceAdminForm = new AnnonceAdminForm($this->Core);
           $annonce = new AnnonceAnnonce($this->Core);
           $annonce->GetById($annonceId);
           $annonceAdminForm->Load($annonce);
           $annonceAdminForm->form->LoadImage($annonce);
       
       return $annonceAdminForm->Render();
 
    }
    
          
          /*action*/
 }?>
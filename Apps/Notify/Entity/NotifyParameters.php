<?php 
/*
* PuzzleApp
* Webemyos
* Jérôme Oliva
* GNU Licence
*/

namespace Apps\Notify\Entity;
use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class NotifyParameters extends Entity  
{
	//Constructeur
	function __construct($core)
	{
		//Version
		$this->Version ="2.0.0.0"; 

		//Nom de la table 
		$this->Core=$core; 
		$this->TableName="NotifyParameters"; 
		$this->Alias = "NotifyParameters"; 

		$this->Name = new Property("Name", "Name", TEXTBOX,  false, $this->Alias); 
		$this->Description = new Property("Description", "Description", TEXTAREA,  false, $this->Alias); 
		$this->Sender = new Property("Sender", "Sender", TEXTBOX,  false, $this->Alias); 
		$this->ReplyTo = new Property("ReplyTo", "ReplyTo", TEXTBOX,  false, $this->Alias); 

		//Creation de l entité 
		$this->Create(); 
	}
}
?>
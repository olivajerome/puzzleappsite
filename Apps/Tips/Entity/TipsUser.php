<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Tips\Entity;

use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class TipsUser extends Entity {

    //Constructeur
    function __construct($core) {
        //Version
        $this->Version = "2.0.0.0";

        //Nom de la table 
        $this->Core = $core;
        $this->TableName = "TipsUser";
        $this->Alias = "TipsUser";

        $this->TipsId = new Property("TipsId", "TipsId", NUMERICBOX, true, $this->Alias);
        $this->UserId = new Property("UserId", "UserId", NUMERICBOX, true, $this->Alias);

        //Creation de l entité 
        $this->Create();
    }

    function GetUser(){
        $user = new \Core\Entity\User\User($this->Core);
        $user->GetById($this->UserId->Value);
        return $user->GetPseudo();
    }

    function GetTips(){
        $user = new \Apps\Tips\Entity\TipsTips($this->Core);
        $user->GetById($this->TipsId->Value);
        return $user->Name->Value;
    }

}

?>
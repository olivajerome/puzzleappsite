var ImportBlogForm = function(){};

/***
 * Init the form
 * @returns {undefined}
 */
ImportBlogForm.Init = function(){
    Event.AddById("btnImportBlog", "click", ImportBlogForm.Import);
};

/*
* Import
*/
ImportBlogForm.Import = function(e){
   
    if(Form.IsValid("ImportBlogForm"))
    {

    var data = "Class=Blog&Methode=Import&App=Blog";
        data +=  Form.Serialize("ImportBlogForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.statut == "Error"){
                Form.RenderError("ImportBlogForm", data.message);
            } else{
                 Dialog.Close();  
                 
                 Dashboard.StartApp('', 'Blog', '');
            }
        });
    }
};


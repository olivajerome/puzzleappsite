<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Ide\Module\Module;

use Apps\Ide\Helper\ModuleHelper;
use Core\Action\AjaxAction\AjaxAction;
use Core\Block\Block;
use Core\Control\Button\Button;
use Core\Control\CheckBox\CheckBox;
use Core\Control\TextBox\TextBox;
use Core\Controller\AdministratorController;
use Core\Core\Request;

class ModuleController extends AdministratorController {

    /**
     * Add module to the selected projet
     */
    function AddModule($data) {
        //Creation du projet
        if (ModuleHelper::CreateModule($this->Core, $data["Projet"], $data["Name"])) {
            return true;
        } else {
            return $this->Core->GetCode("Ide.ModuleExist");
        }
    }

    /**
     * Permet d'ajouter une action à un module
     */
    function ShowAddActionModule() {
        //Recuperation des variables
        $projet = Request::GetPost("Projet");
        $block = Request::GetPost("Block");

        //Creation du bloc
        $jbAction = new Block($this->Core, "jbAction");

        //Nom de l'action
        $tbNameAction = new TextBox("tbNameAction");
        $tbNameAction->PlaceHolder = $this->Core->GetCode("Name");
        $jbAction->AddNew($tbNameAction);

        //Ajout d'un template
        $cbTemplate = new CheckBox("cbTemplate");
        $cbTemplate->Libelle = $this->Core->GetCode("Ide.AddTemplate");
        $jbAction->AddNew($cbTemplate);

        //Action   
        $action = new AjaxAction("Ide", "AddActionModule");
        $action->AddArgument("App", "Ide");
        $action->AddArgument("Projet", $projet);
        $action->AddArgument("Block", $block);

        $action->ChangedControl = "jbAction";
        $action->AddControl($tbNameAction->Id);
        $action->AddControl($cbTemplate->Id);

        //Bouton de sauvegarde
        $btnCreate = new Button(BUTTON);
        $btnCreate->CssClass = "button orange";
        $btnCreate->Value = $this->Core->GetCode("Save");
        $btnCreate->OnClick = $action;
        $jbAction->AddNew($btnCreate);

        return $jbAction->Show();
    }

    /**
     * Ajoute une action à un module
     */
    function AddActionModule() {
        //Recuperation des données
        $projet = Request::GetPost("Projet");
        $block = Request::GetPost("Block");
        $nameAction = Request::GetPost("tbNameAction");
        $addTemplate = (Request::GetPost("cbTemplate") == 1);

        if ($nameAction == "") {
            return "<span class='FormUserError'>" . $this->Core->GetCode("NameObligatory") . "</span>" . self::ShowAddModule();
        } else {
            ModuleHelper::AddActionModule($projet, $block, $nameAction, $addTemplate);
            return "<span class='FormUserValid'>" . $this->Core->GetCode("SaveOk") . "</span>";
        }
    }
}

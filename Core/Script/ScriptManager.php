<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Core\Script;

use Core\Core\Core;
use Core\Utility\File\File;
use Core\Utility\Packer\Packer;

/**
 * Description of ScriptManager
 *
 * @author jerome
 */
class ScriptManager
{
    /*
     * Get à compressed script
     */
    public static function Get($s)
    {
        $script = CacheManager::Find($s ."js");

        if($script == null)
        {
            switch($s)
            {
               case "Dashboard":
                  $script =  ScriptManager::GetDashBoard();
                  $script = ScriptManager::Minify($script);
               break;
            }
        }

        return $script;
    }

    /*
     * Obtain the dashboard script
     */
    public static function GetDashBoard()
    {
        $scripts  = File::GetFileContent(dirname(__DIR__)."/Dashboard/Dashboard.js");
        $scripts  .= File::GetFileContent(dirname(__DIR__)."/Dashboard/DashboardApp.js");
        $scripts  .= File::GetFileContent(dirname(__DIR__)."/Dashboard/DashboardModule.js");
        $scripts  .= File::GetFileContent(dirname(__DIR__)."/Dashboard/DashboardWidget.js");
        $scripts  .= File::GetFileContent(dirname(__DIR__)."/Core/Js/Request.js");
        $scripts  .= File::GetFileContent(dirname(__DIR__)."/Core/Js/Event.js");
        $scripts  .= File::GetFileContent(dirname(__DIR__)."/Core/Js/App.js");
        $scripts  .= File::GetFileContent(dirname(__DIR__)."/Core/Js/Dom.js");
        $scripts  .= File::GetFileContent(dirname(__DIR__)."/Core/Js/Http.js");
        $scripts  .= File::GetFileContent(dirname(__DIR__)."/Core/Js/View.js");
        $scripts  .= File::GetFileContent(dirname(__DIR__)."/Core/Js/Language.js");
        $scripts  .= File::GetFileContent(dirname(__DIR__)."/Core/Js/Plugin.js");

        return $scripts;
    }

    /*
     * Get Application Script
     */
    public static function GetApp($a, $m, $type)
    {
        $core = Core::GetInstance(GetEnvironnement());
        
        if($m != "")
        {
           $directory = dirname(dirname(__DIR__)). "/Apps/".$a. "/Module/".$m."/";

           $directory.$m."Controller.".$type;
           $script  = File::GetFileContent( $directory.$m."Controller.".$type);

            //Ajout des vue dans un tableau js
            $ViewsArray = $a . $m . "View";
            $Views = "var ".$ViewsArray . " = new Array();";

            if($dossier = opendir($directory . "View"))
            {
                while(false !== ($fichier = readdir($dossier)))
                {
                    if($fichier != '.' && $fichier != '..' && $fichier != 'index.php')
                    {
                        if (stripos($fichier, '.jtpl') !== FALSE) {
                            
                            $content = File::GetFileContent($directory ."View/" .$fichier);

                            $Views .= $ViewsArray.".push(new Array('" .$fichier. "', '".$content."'));"; 
                        }
                        
                    }
                }

                $script .= "\n\r".  $Views;
            }
        }
        else
        {
            $script  = File::GetFileContent(dirname(dirname(__DIR__)). "/Apps/".$a. "/".$a.".".$type);
            
            //Ajout du dialog
            $script  .= self::GetApp($a, "Dialog".$a , $type);
        }

        //Add Style Cms saved in bdd
        if($type == "css" && $a == "Cms")
        {
            $cms = new \Apps\Cms\Entity\CmsCms($core);
            $cms->GetById(1);
            $script .= $cms->Style->Value;
        }

        if($type == "js" && $core->Debug === false)
        {
            $script .= self::GetWidget($a);
            $script .= self::GetModule($a);
            $script = ScriptManager::Minify($script);
        }
        else if($type == "css")
        {
             header('content-type: text/css');
        }

        return $script;
    }

    /***
     * Concatene tous les scripts de toutes les applications
     */
    public static function GetApps($core, $apps, $type) {

        if ($apps == "all") {
            $apps = \Apps\EeApp\Helper\AppHelper::GetAll($core);

            foreach ($apps as $app) {
                $scripts .= self::GetApp($app->Name->Value, "", $type);
            }
        } else {
            foreach (explode("-", $apps) as $app) {
                $scripts .= self::GetApp($app, "", $type);
            }
        }

        return $scripts;
    }

    /**
     * Obitent lme js compilé des widget d'une app
     * @param type $app
     */
    public static function GetWidget($app)
    {

       $directory = dirname(dirname(__DIR__)). "/Apps/".$app. "/Widget";
       $scripts ="" ;
       if($dossier = opendir($directory))
         {
                while(false !== ($fichier = readdir($dossier)))
                {

                    if($fichier != "." && $fichier != ".." && is_dir($directory."/".$fichier))
                    {
                        if($child = opendir($directory."/".$fichier))
                        {
                            
                           while(false !== ($script = readdir($child)))
                            {
                               if( strpos($script, ".js") !== false)
                               {
                                 $scripts  .= File::GetFileContent($directory."/".$fichier . "/". $script);
                               }
                            }
                        }
                    }
                    
                    echo  ScriptManager::Minify($scripts);
                }
            }
    }
    
    
    public static function GetTemplate($core, $template, $type) {
        if($type == "css")
        {
             header('content-type: text/css');
        }
        
        $directory = dirname(dirname(__DIR__)) . "/Templates/" . $template . "/" . $type;
        $scripts = "";

        if ($dossier = opendir($directory)) {
            while (false !== ($fichier = readdir($dossier))) {
                $scripts .= File::GetFileContent($directory . "/" . $fichier);
            }
        
            echo $scripts;
        }
    }

    /***
     * Register plugin of a app
     */
    public static function GetPluginApp($core, $app){
        $plugins = \Apps\EeApp\Helper\AppHelper::GetPluginApp($core, $app);
        $script = "";
        
        foreach($plugins as $plugin){
             $script .= File::GetFileContent(dirname(dirname(__DIR__)). "/Plugins/".$plugin->PluginName->Value. "/".$plugin->PluginName->Value.".js");
        }
        
        return $script;
    }
    
    
    public static function GetPlugin($p, $type){
        $script  = File::GetFileContent(dirname(dirname(__DIR__)). "/Plugins/".$p. "/".$p.".".$type);
        echo ScriptManager::Minify($script);
    }
    
     /**
     * Obitent le js compilé des mopdule d'une app
     * @param type $app
     */
    public static function GetModule($app)
    {

       $directory = dirname(dirname(__DIR__)). "/Apps/".$app. "/Module";
       $scripts ="" ;
       if($dossier = opendir($directory))
         {
                while(false !== ($fichier = readdir($dossier)))
                {

                    if($fichier != "." && $fichier != ".." && is_dir($directory."/".$fichier))
                    {
                        if($child = opendir($directory."/".$fichier))
                        {
                            
                           while(false !== ($script = readdir($child)))
                            {
                               if( strpos($script, ".js") !== false)
                               {
                                 $scripts  .= File::GetFileContent($directory."/".$fichier . "/". $script);
                               }
                            }
                        }
                    }
                    
                    echo  ScriptManager::Minify($scripts);
                }
            }
    }
    
    /*
     * Get Compressed file for the Js
     */
    public static function GetAllJs()
    {
        $script = CacheManager::Find("script.js");

        if($script == null)
        {
            //List all Js in the Core
            $script = ScriptManager::GetAll();
            $script .= ScriptManager::GetDashBoard();
            $script = ScriptManager::Minify($script);
           
            $script .= self::GetEditor();
        }

        return $script;
    }
    
    /***
     * Get The Editor Js class
     */
    public static function GetEditor(){
        $script = File::GetFileContent(dirname(__DIR__)."/Control/TextRichEditor/TextRichEditor.js");
        $script .= File::GetFileContent(dirname(__DIR__)."/Control/TextRichEditor/BaseTool.js");
        $script .= File::GetFileContent(dirname(__DIR__)."/Control/TextRichEditor/Bold.js");
        $script .= File::GetFileContent(dirname(__DIR__)."/Control/TextRichEditor/Italic.js");
        $script .= File::GetFileContent(dirname(__DIR__)."/Control/TextRichEditor/H.js");
        $script .= File::GetFileContent(dirname(__DIR__)."/Control/TextRichEditor/Underline.js");
        $script .= File::GetFileContent(dirname(__DIR__)."/Control/TextRichEditor/Stroke.js");
        $script .= File::GetFileContent(dirname(__DIR__)."/Control/TextRichEditor/List.js");
        $script .= File::GetFileContent(dirname(__DIR__)."/Control/TextRichEditor/Font.js");
        $script .= File::GetFileContent(dirname(__DIR__)."/Control/TextRichEditor/Align.js");
        $script .= File::GetFileContent(dirname(__DIR__)."/Control/TextRichEditor/ImageTool.js");
        $script .= File::GetFileContent(dirname(__DIR__)."/Control/TextRichEditor/SaveContent.js");
        $script .= File::GetFileContent(dirname(__DIR__)."/Control/TextRichEditor/ShowBlock.js");
        $script .= File::GetFileContent(dirname(__DIR__)."/Control/TextRichEditor/Link.js");
        $script .= File::GetFileContent(dirname(__DIR__)."/Control/TextRichEditor/SourceCode.js");
        $script .= File::GetFileContent(dirname(__DIR__)."/Control/TextRichEditor/TableTool.js");
        $script .= File::GetFileContent(dirname(__DIR__)."/Control/TextRichEditor/Paragraph.js");
        $script .= File::GetFileContent(dirname(__DIR__)."/Control/TextRichEditor/HtmlToCode.js");
        
        return $script;
    }

    /*
     * Find all js in the Core
     */
    public static function GetAll()
    {
        $script ="";

        $folders = array("Action", "Control", "Utility", "Dashboard");
        $scripts = "";

        foreach($folders as $folder)
        {
           if($dossier = opendir(dirname(__DIR__)."/".$folder))
            {
             
               
                while(false !== ($fichier = readdir($dossier)))
                {
                    
                    if($fichier == "TextRichEditor") continue;
                      
                    if($fichier != "." && $fichier != ".." && is_dir(dirname(__DIR__)."/".$folder . "/" .$fichier))
                    {
                        if($child = opendir(dirname(__DIR__)."/".$folder."/".$fichier))
                        {
                           while(false !== ($script = readdir($child)))
                            {
                               if( strpos($script, ".js") !== false)
                               {
                                     $scripts  .= File::GetFileContent(dirname(__DIR__)."/".$folder."/".$fichier . "/". $script);
                               }
                            }
                        }
                    }
                }
            }
        }

        return $scripts;
    }

    /*
     * Minify the Js
     */
    public static function Minify($content)
    {
        return Packer::Compress($content);
    }

    /***
     * Get the installed App
     */
    public static function LoadApp(){

        $core = Core::GetInstance(GetEnvironnement());
        $app = \Apps\EeApp\Helper\AppHelper::GetAll($core);
        
        foreach($app as $app){
            $appsName .= "'".$app->Name->Value."',";
         }
        return "var Apps = [".$appsName."];";
    }

}

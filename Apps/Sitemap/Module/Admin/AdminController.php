<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Sitemap\Module\Admin;

use Core\Control\Button\Button;
use Core\Controller\AdministratorController;
use Core\View\View;
use Core\View\ElementView;

use Core\Control\TabStrip\TabStrip;
use Core\Control\VTab\VTab;
use Core\Control\Libelle\Libelle;
use Apps\EeApp\EeApp;


/*
 * 
 */
 class AdminController extends AdministratorController
 {
    /**
     * Constructeur
     */
    function _construct($core="")
    {
          $this->Core = $core;
    }

    /**
     * Creation
     */
    function Create()
    {
    }

    /**
     * Initialisation
     */
    function Init()
    {
    }

    /**
     * Affichage du module
     */
    function Show($all=true)
    {
         return $this->Index();
    }
    
    /*
    * Get the home page
    */
   function Index()
   {
       $view = new View(__DIR__."/View/index.tpl", $this->Core);
       
       $tabSiteMap = new TabStrip("tabSiteMap", "Sitemap");
       $tabSiteMap->AddTab($this->Core->GetCode("Sitemap.Url"), $this->GetTabSiteMap());

       $view->AddElement(new ElementView("tabSiteMap", $tabSiteMap->Render()));
       return $view->Render();
   }
   
   /***
    * Get all SiteMap
    */
   function GetTabSiteMap(){
       
        $tabApp = new VTab("tabApp");
        $eapp = new EeApp();
        $apps = $eapp->GetAll();

        foreach ($apps as $app) {
            $appPath = "\\Apps\\" . $app->Name->Value . "\\" . $app->Name->Value;
            $ap = new $appPath($this->Core);

            $content = $ap->GetSiteMap(true);
           
           if($content != ""){
            $tabApp->AddTab($app->Name->Value, new Libelle($content));
           }
        }
        
        return $tabApp;
   }
          
          /*action*/
 }?>
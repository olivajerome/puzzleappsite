<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Annonce\Module\Api;

use Core\Controller\Controller;

use Core\View\View;
use Core\View\ElementView;
use Apps\Annonce\Entity\AnnonceCategory;
use Apps\Annonce\Entity\AnnonceAnnonce;


/*
 * 
 */

class ApiController extends Controller {

    /***
     * Get All Categorie
     */
    function GetCategories(){
        
        $categorie = new AnnonceCategory($this->Core);
        $categories =  $categorie->GetAll();
        
        return $categorie->ToAllArray($categories);
    }
    
    /***
     * Get the categorie and the annonce
     */
    function GetCategorie($categoryId){
        
          $categorie = new AnnonceCategory($this->Core);
          $categorie->GetById($categoryId);
          
          $annonce = new AnnonceAnnonce($this->Core);
          $annonces = $annonce->Find("CategoryId=" . $categoryId);
          
          
          return array("category" => $categorie->ToArray(), "annonces" =>  $annonce->ToAllArray($annonces) );
    }
    
    /***
     * Get detail of a annonce
     */
    function GetAnnonce($annonceId){
        $annonce = new AnnonceAnnonce($this->Core);
        $annonce->GetById($annonceId);
        
        return $annonce->ToArray(); 
    }
    
}
    

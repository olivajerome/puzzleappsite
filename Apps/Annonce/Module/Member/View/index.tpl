<section>
    <h1>{{GetCode(Annonce.YourAnnonce)}}</h1>
    
   {{foreach Annonces}}
    
    <div class='block annonceMember' id='{{element->IdEntite}}'>
        <div class='{{ {{element->GetNumberImage()}}  == 0 ? hidden : col-md-4 }}'>
             {{Apps\Annonce\Decorator\AnnonceDecorator->RenderImages(element)}}
        </div>
        <div class='{{ {{element->GetNumberImage()}}  == 0 ? col-m-12 : col-md-8}}'>
            <div>
                <h2 class='title'>{{element->Title->Value}}</h2>
            </div>
            <div>
                <p class='description'>{{element->Description->Value}}</p>
            </div>
        </div>
    
        <div id='responseContainer' class='col-md-12'>
            {{Apps\Annonce\Decorator\AnnonceDecorator->RenderResponses(element)}}
        </div>
 
        <div class='tool col-md-12'>
            <i class='fa fa-trash removeAnnonce' id='{{element->IdEntite}}' title='{{GetCode(Annonce.Delete)}}'></i>
        </div>
    </div>

    {{/foreach Annonces}}
  
</section>
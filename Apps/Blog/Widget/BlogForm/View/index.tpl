{{form->Open()}}
{{form->Error()}}
{{form->Success()}}

{{form->Render(Id)}}

    <h2>{{GetCode(Blog.Configuration)}}</h2>
    
    <div>
        <label>{{GetCode(Blog.Name)}}</label> 
        {{form->Render(Name)}}
    </div>

    <div>
        <label>{{GetCode(Blog.Description)}}</label> 
        {{form->Render(Description)}}
    </div>
    <div class='center marginTop' >   
        {{form->Render(btnSave)}}
    </div>
    
    {{if ShowPlugin == true}}
    
    <div class='marginTop borderTop'>
        <h2>{{GetCode(Blog.Plugin)}}</h2>
        <div class='right'>
            <button id='btnAddPlugin'  class='btn btn-primary' >{{GetCode(Blog.AddPlugin)}}</button>
        </div>

        <div id='lstPlugin' class='right'>
            {{foreach Plugins}}   
                <div class='chips'>

                    {{element->PluginName->Value}}

                    <i id='{{element->IdEntite}}' class='fa fa-trash removePlugin'></i>


                </div>
            {{/foreach Plugins}}   
        </div>    
    </div>
        
    {{/if ShowPlugin == true}}

{{form->Close()}}


{{PluginScript}}
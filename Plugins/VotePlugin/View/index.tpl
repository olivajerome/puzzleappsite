<div class='votePlugin' data-entity='{{EntityName}}' style='text-align:right'
     data-id='{{EntityId}}' >
    <span>
        <i style='cursor:pointer' class='fa fa-thumbs-up addPlus'></i>
        <b class='plus'>{{plus}}</b>
    </span>
    <span>
        <i style='cursor:pointer' class='fa fa-thumbs-down addMinus'></i>
        <b class='minus'>{{minus}}</b>
   </span>
</div>
CREATE TABLE IF NOT EXISTS `CmsBlock` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`TypeId` INT  NOT NULL,
`ParentId` INT  NOT NULL,
`Position` INT  NOT NULL,
`Size` INT  NOT NULL,
`Style` TEXT  NULL,
`CssClass` TEXT NULL,
`Content` TEXT  NULL,
`Widget` TEXT  NULL,
PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
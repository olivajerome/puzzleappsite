function Show()
{
    this.query = "";

    this.Run = function()
    {
         var controls = document.querySelectorAll(this.query);
    
         for(var i = 0 ; i < controls.length; i++ )
         {
             controls[i].style.display = "";
         }
    };

    this.Tick = function()
    {
        this.control.style.width = this.control.style.width.replace("px", "")  - 10  + "px";
        this.control.style.height = this.control.style.height.replace("px", "")  - 10  + "px";
    
        if(this.control.style.width.replace("px","") <= 0 && this.control.style.height.replace("px","") <= 0)
        {
            window.clearInterval(this.interval);
            this.control.style.display = "none";
        }
    };
};
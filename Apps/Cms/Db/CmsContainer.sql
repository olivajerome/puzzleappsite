CREATE TABLE IF NOT EXISTS `CmsContainer` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`Name` TEXT  NOT NULL,
`Code` TEXT  NOT NULL,
`Position` INT  NOT NULL,
`PageId` INT NULL,
`Style` TEXT  NULL,
`CssClass` TEXT  NULL,
PRIMARY KEY (`Id`),
CONSTRAINT `CmsPage_CmsContainer` FOREIGN KEY (`PageId`) REFERENCES `CmsPage`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
var DialogMarketController = function(){};

DialogMarketController.AddProduct = function(){
    MarketProductForm.Init();
};

DialogMarketController.EditProduct = function(){
    MarketProductForm.Init();
};

DialogMarketController.MakeOffer = function(){
    MarketOfferForm.Init();
};

DialogMarketController.SendProduct = function(){
    MarketProductShippingForm.Init();
};


<section class='container'>
    <h1><img style='height:150px' src='{{Ressource->GetImageFullPath()}}' /> 
        {{Ressource->Name->Value}}</h1>
    
    <p>{{Ressource->LongDescription->Value}}</p>
 
    <div class='row' id='viewerDocumentation' style='cursor :pointer'>
       {{Apps\Downloader\Decorator\RessourceDecorator->RenderImages()}}
    </div>
 
    
    <div class='center'>
         <a class='btn btn-primary' href='{{Ressource->GetDownloadPath()}}'>
            {{GetCode(Downloader.Download)}}
        </a>
    </div>
    
</section>

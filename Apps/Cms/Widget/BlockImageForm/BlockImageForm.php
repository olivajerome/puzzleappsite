<?php

namespace Apps\Cms\Widget\BlockImageForm;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;

use Apps\Cms\Widget\Library\Library;

use Apps\Cms\Entity\CmsBlock;

class BlockImageForm extends Form {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "") {
        $this->Core = $core;
        $this->Data = $data;

        //On le fait ici pour avoir le formuaire � l'affichage mais aussi � la validation
        $this->Init($data);
    }

    /**
     * Initialisation 
     */
    function Init($data) {
        $this->form = new Form("BlockImageForm", $data);

        $this->form->SetView(__DIR__ . "/View/index.tpl", $this->Core);
        $this->form->SetAction("/View/index.tpl");

        $this->form->Add(array("Type" => "Hidden",
            "Id" => "BlockId",
            "Value" => $data["Id"]
        ));

        $this->form->Add(array("Type" => "Hidden",
            "Id" => "imgSrc",
        ));
        
        $this->form->Add(array("Type" => "Upload", 
                                        "Id" => "uploadImageCms",
                                        "App" => "Cms",
                                        "Method" => "UploadImage",
                                        "Multiple" => false, 
                ));

        
        $this->form->Add(array("Type" => "NumericBox",
            "Id" => "Width",
            "Value" => 100
        ));
          
        $this->form->Add(array("Type" => "Button",
            "Id" => "btnSaveBlockImage",
            "Value" => $this->Core->GetCode("Cms.SaveBlockImage"),
            "CssClass" => "btn btn-primary"
        ));
        
        $libray = new Library($this->Core);
        
        if($data["Id"] != ""){
            $block = new CmsBlock($this->Core);
            $block->GetById($data["Id"]);
           
            //Recuperation de l'image
             preg_match('/Data(.*?jpg)/', $block->Content->Value, $matches);
        
            $currentImage = $matches[1];
        }
        
        $this->form->AddElementView(new ElementView("Library", $libray->GetImagesBlock($currentImage, $data["Apps"])));
        
        
    }

    /*     * *
     * G�n�rer les formulaire
     */

    function Render() {
        return $this->form->Render();
    }

    /*     * *
     * Valide les donn�es selon les diff�rents formulaires
     */

    function Validate($data = "") {
        $errors = $this->form->Validate($this->Data);

        return $errors;
    }

    /*     * *
     * Charge les controls avec les donn�es de l'entit�
     */

    function Load($block) {
        $this->form->Load($block);
    }

    /*     * *
     * Charge l'entit� avec les donn�es des diff�rents formulaire
     */

    function Populate($product) {
    }

    /*     * *
     * Gestion des erreurs
     */

    function Error() {
        $errors[] = $this->form->errors;

        return json_encode(array("statut" => "Error", "message" => $errors));
    }

}

var LastNotes = function(){};

/***
 * Init the form
 * @returns {undefined}
 */
LastNotes.Init = function(){
    Event.AddById("btnSave", "click", LastNotes.SaveElement);
};

/***
* Get the Id of element 
*/
LastNotes.GetId = function(){
    return Form.GetId("LastNotesForm");
};

/*
* Save the Element
*/
LastNotes.SaveElement = function(e){
   
    if(Form.IsValid("LastNotesForm"))
    {

    let data = "Class=LastNotes&Methode=SaveElement&App=LastNotes";
        data +=  Form.Serialize("LastNotesForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.status == "error"){
                Form.RenderError("LastNotesForm", data.data.message);
            } else{

                Form.SetId("LastNotesForm", data.data.Id);
            }
        });
    }
};


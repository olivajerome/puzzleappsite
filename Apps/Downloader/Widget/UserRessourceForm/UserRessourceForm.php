<?php

namespace Apps\Downloader\Widget\UserRessourceForm;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;

class UserRessourceForm extends Form {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "") {
        $this->Core = $core;
        $this->Data = $data;

        $this->Init($data);
    }

    /**
     * Initialisation 
     */
    function Init($data) {
        $this->form = new Form("UserRessourceFormForm", $data);

        $this->form->SetView(__DIR__ . "/View/index.tpl", $this->Core);
        $this->form->SetAction("/View/index.tpl");

        $this->form->Add(array("Type" => "Hidden",
            "Id" => "Id",
        ));
        
        $this->form->Add(array("Type" => "TextBox",
            "Id" => "Name",
            "Validators" => ["Required"]
        ));
        
        $this->form->Add(array("Type" => "TextArea",
            "Id" => "Description",
            "Validators" => ["Required"]
        ));

        $this->form->Add(array("Type" => "Upload",
        "Id" => "Upload",
        "App" => "Downloader",
        "Method" => "SaveRessource",
        "Accept" => "All",
        "Multiple" => false
    ));

        $this->form->Add(array("Type" => "Button",
            "Id" => "btnSendRessource",
            "Value" => $this->Core->GetCode("Downloader.Send"),
            "CssClass" => "btn btn-primary"
        ));
    }

    /*     * *
     * Render the html form
     */

    function Render() {
        return $this->form->Render();
    }

    /*     * *
     * Validate the data
     */

    function Validate($data = "") {
        $errors = $this->form->Validate($this->Data);

        return $errors;
    }

    /*     * *
     * Load control with the entity
     */

    function Load($entity) {
        $this->form->Load($entity);
    }

    /*     * *
     * Populate the entity with the form Data
     */

    function Populate($entity) {
        
    }

    /*     * *
     * Error message
     */

    function Error() {
        $errors[] = $this->form->errors;

        return json_encode(array("statut" => "Error", "message" => $errors));
    }

}

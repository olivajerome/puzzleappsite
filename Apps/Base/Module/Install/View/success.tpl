<section class='container'>
    <h1>Installation complete</h1>

    <p> 
        The installation of your puzzleApp site is complete. You may now proceed to install and configure applications through the administration interface. Your login link is:
        
        <br/><a href='Login'>My admin dashboard</a>
    </p>
</section>    
function Draggable(){

    this.selectedElement = null;

    /*
    * Ajout des events sur le selector
    */
    this.setDrag = function(selector, dragStart, dragStop){
        
        Draggable.selector = selector;
        Draggable.dragStart = dragStart;
        Draggable.dragStop = dragStop;
        
        Event.AddByClass(selector, "mousedown", this.startDrag);
        Event.AddByClass(selector, "mousemove", this.drag);
        Event.AddByClass(selector, "mouseup", this.stopDrag);
    };
    
    /*
    * Démarrage du drag
    */
    this.startDrag = function(e){
    
        // Pour l'édition
        if(e.srcElement.className.indexOf("fa") > -1)
        {
            return true;
        }
       
        this.selectedElement = e.srcElement;
    
        e.preventDefault();
        e.stopPropagation();
     
       pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
      
        
        while(this.selectedElement.className.indexOf(Draggable.selector) < 0){
            this.selectedElement = this.selectedElement.parentNode; 
        }
       
        // get the mouse cursor position at startup:
        pos3 = e.clientX;
        pos4 = e.clientY;
        
        Draggable.dragStart();
        
        return true;
    };
    
    /*
    * Drag the element
    */
    this.drag = function(e){

    if(this.selectedElement != null){
            
            e.preventDefault();
           e.stopPropagation();
    
            elmnt = this.selectedElement;
            
            elmnt.style.position = "fixed";
            elmnt.style.width = "250px";
            e.srcElement.style.display = "block";
            e.preventDefault();
            
            // calculate the new cursor position:
            pos1 = pos3 - e.clientX;
            pos2 = pos4 - e.clientY;
            pos3 = e.clientX;
            pos4 = e.clientY;
            //console.log()
            
            // set the element's new position:
            elmnt.style.top = (e.clientY - 50 )+ "px";
            elmnt.style.left =  (e.clientX -50 )+ "px";
    
        }

        return;
    };
    
    /*
    * Fin du drag
    */
    this.stopDrag = function(e){
    
    if( this.selectedElement != null){
        e.preventDefault();
        e.stopPropagation();
        
        //Call Back
        Draggable.dragStop(e.clientX, e.clientY, this.selectedElement);
        
        this.selectedElement.style.position ="";
        this.selectedElement.style.width = "";
        this.selectedElement = null;
        }
       
        return;
        
    };
};

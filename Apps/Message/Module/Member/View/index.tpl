<section class='col-md-10 centerBlock'>
    <div class='row'>
        <div class="col-md-4 leftColumn">
            <div id='btnHomeMessage' title='retour' class='fa fa-arrow-left' style='display:none'>&nbsp;</div>
            <div class='btn btn-primary' id='btnNewConversation' >{{GetCode(Message.NewConversation)}}</div>

            <div class='userSearch'>
                <i id='resetFilter' class='fa fa-arrow-left' title ='Effacer le filtre' style='display:none' >&nbsp;</i>
                <input class="form-control" id='tbSearchConversation' placeHolder ="Rechercher" ></input>
            </div>

            <input type='hidden' id='userPseudo'  value='{{userPseudo}}' />

            <div class="MessagePreview" id='actualMessageUser'>
                <ul>
                    {{foreach Conversations}}
                    <li class='discussPreview {{element->GetClassReaded()}}'
                        id='{{element->IdEntite}}'>
                        <div class='row'>
                            <div class='col-md-10'>
                                <div class='title'>
                                    {{element->GetLimitTitle()}} 
                                </div>
                                <div class='subTitle'>
                                    {{element->GetSubTitle()}} 
                                </div>  
                            </div>
                        </div>
                    </li>
                    {{/foreach Conversations}}

                </ul>
            </div>                  

        </div>
        <div class="col-md-8 rightColumn" >
            <div >
                <div id='dashboardDiscus' style='height:auto;min-height:200px'>

                </div>

                <div id='notify' style='display:none'>Chargement</div>
                <div class='tchat' id='messageBlock' style='display:none'>

                    <div class='text' id='messageContainer'>
                        <input type='text' placeHolder ='Saisissez un objet' class='form-control' id='tbObjet'/>
                        <input id='tbMessage' type='text' class='form-control' placeholder="Tapez votre message" />
                    </div>
                    <div class='attachement'>
                        {{uploadMessage}}
                    </div>
                    <div >
                        <button class='btn btn-primary' id='btnSendMessage' >Envoyer</button>
                    </div>
                </div> 
            </div>
        </div>

    </div>              
    <div class='clear'></div>

</section>


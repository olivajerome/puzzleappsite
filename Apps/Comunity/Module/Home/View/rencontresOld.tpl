<div class="container">
	
	<div class="row">
		<div class="col-md-12">
			<h1>Rencontres thématiques</h1>
		</div>
	</div>	
		<div class="cadredotted">
	<div class="row">
		<div class="col-md-12">
		<p style="padding-top:5px; font-weight: bold; color: #f39a40;">Projets en cours</p>
		</div>
		
			<ul>
		 	
			{{foreach Discuss}}
                            <div class="col-md-4">
				<li>
					<a href="{{Path}}/ParlonsEn/Rencontre/{{element->IdEntite}}">
                                            
                                            
					   {{element->Title->Value}}
                                           
                                          ({{element->GetNumberMessage()}})
					</a>
				</li>
			</div>
			
                        {{/foreach Discuss}}
                        
			
			
			</ul>
		 <div class="col-md-12 center">
			<a class="actionButton btn btn-primary" style="width: 80%; margin: 20px 0 10px 0;" href="{{Path}}/ParlonsEn/NewDiscussion">
				<i class="fa fa-plus">&nbsp;</i>Nouveau projet
			</a>
		</div>
	</div>
			</div>
</div>

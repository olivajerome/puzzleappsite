<div class='col-md-4'>
    <div class='block widgetDashboard'>
        <i class='fa fa-trash removeWidget' data-app='Seo' title='{{GetCode(EeApp.RemoveWidgetDahboard)}}'></i>
        <i class='fa fa-book title'>&nbsp;{{GetCode(Seo.Seo)}}</i>
       
        <input type='hidden' data-sourceName='pagesSeo' id='pagesSeo' value='{{pages}}' />
           <ul id='pagesSeoContainer' data-source='pagesSeo'>
                <li class='model page ' style='display:none'  >
                    <input type='hidden' data-property = 'IdEntite'  />
                    <input type='hidden' data-property = 'Code'  />
                    <b data-property = 'Url' ></b> : <b data-property = 'Title' ></b> 
                    <div class='icones'>
                        <i class='fa fa-gear editSeoPage' ></i>
                        <i class='fa fa-trash deleteSeoPage' ></i>
                    </div>        
                </li>
            </ul>

        <div class='buttons'>
            <button id='btnAddSeoPageAdminWidget' onclick="" class='btn btn-primary'>{{GetCode(Seo.NewPage)}}</button>
        </div>

    </div>
</div>

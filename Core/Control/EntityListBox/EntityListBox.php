<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Core\Control\EntityListBox;

use Core\Control\IControl;
use Core\Control\Control;
use Core\Control\ListBox\ListBox;

 class EntityListBox extends Control implements IControl
 {
    //Propriete
    private $Core;
    private $Entity;
    private $ListBox;
    private $Selected;
    private $SelectedValue;
    private $Argument = array();
    private $Fields = array();

    //Constructeur
    public function __construct($name, $core="")
    {
       //Version
       $this->Version ="2.0.0.0";

       $this->Name = $name;
       $this->Core = $core;
       $this->Id = $name ;
       $this->EmptyVisible =false;
       $this->Asc=true;

       $this->ListBox = new ListBox($name);
       $this->ListBox->Libelle = $this->Libelle;
    }

   //Ajout d'un parametre
   public function AddArgument($argument)
   {
     $this->Argument[] = $argument;
   }

  //Ajout d'un champ
  public function AddField($field)
  {
  	$this->Fields[] = $field;
  }

   //Chargement
   private function Load()
   {

	//Creation de l'entit�
    $Entity = new $this->Entity($this->Core);

   //Recuperation des entit�s
    if(sizeof($this->Argument) > 0)
    {
      foreach($this->Argument as $argument)
      {
        $Entity->AddArgument($argument);
      }

      $Entites = $Entity->GetByArg();
    }
    else
    {
      $Entites = $Entity->GetAll();
    }
    if(sizeof($Entites)> 0)
    {
        //Chargement
        foreach($Entites as $entity)
        {
                if(sizeof($this->Fields)>0)
                {
                    $Value ="";
                    foreach($this->Fields as $fields)
                    {
                        $field = $fields;
                        $Value .= " ".$entity->$field->Value;
                    }

                    if($this->Selected &&  $this->Selected == $entity->IdEntite){
                        $this->SelectedValue = $Value;
                    } 
    
                    if(isset($this->Data)){
                      $data = $this->Data;
                      $dataValue = $entity->$data->Value;
                    }  

                   $this->ListBox->Add($Value,$entity->IdEntite, $dataValue);

        
                }
                else
                  $this->ListBox->Add($entity->Name->Value,$entity->IdEntite);
        }
    }
  }

    //Affichage
  public function Show()
  {
  	//Chargement
    $this->Load();

    if($this->Selected)
		$this->ListBox->Selected = $this->Selected;

    if($this->Validators){
      $this->ListBox->Validators = $this->Validators ;
    }

    if($this->Filter === true){
        
        $html = "<div><div class='selectFiterBox' onclick='EntityListBox.Open(this);'>";
        $html .= "<span>" .$this->SelectedValue . "</span>" ;
        $html .= "<i class ='entityListBoxIcon  fa fa-angle-down' ></i> ";
        $html .= "</div>";
        
        $this->ListBox->Style = "display:none";
        $html .= $this->ListBox->Show();
        
        $html .= "</div>";
        
        return $html;
    }
    else if($this->Multiple === true){
        
        if($this->ListBox->Selected != ""){
            
            $entities = new $this->Entity($this->Core);
            $entities = $entities->Find("id in (".$this->ListBox->Selected.")");
            $values ="";
            
            foreach($entities as $entitie){
                if($values != ""){
                    $values .= ",";
                }
                $values .= $entitie->Libelle->Value;
            }
        }
        
        $html = "<div><div class='selectMultipleBox' onclick='EntityListBox.OpenMultiple(this);'>";
        $html .= "<span>" .$values . "</span>" ;
        $html .= "<i class ='entityListBoxIcon  fa fa-angle-down' ></i> ";
        $html .= "</div>";
        
        $this->ListBox->Multiple = true;
        $this->ListBox->Style = "display:none";
        $html .= $this->ListBox->Show();
        
        $html .= "</div>";
        
        return $html;
    } 
    
    else {
        return $this->ListBox->Show();
    }
    
  }

  //asseceurs
  public function __get($name)
  {
    return $this->$name;
  }

  public function __set($name,$value)
  {
    $this->$name=$value;
  }

 }
?>

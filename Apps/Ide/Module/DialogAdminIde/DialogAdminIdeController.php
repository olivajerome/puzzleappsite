<?php 
/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Ide\Module\DialogAdminIde;

use Core\Controller\AdministratorController;
use Apps\Ide\Widget\IdeProjetForm\IdeProjetForm;
use Apps\Ide\Widget\IdeModuleForm\IdeModuleForm;
use Apps\Ide\Widget\IdeHelperForm\IdeHelperForm;
use Apps\Ide\Widget\IdeWidgetForm\IdeWidgetForm;


class DialogAdminIdeController extends AdministratorController{

    /***
     * Add a Project 
     */
    public function AddProjet(){
        $ideProjetForm = new IdeProjetForm($this->Core);
        return $ideProjetForm->Render();
    }

    /***
     * Add a module
     */
    public function ShowAddModule(){
        $ideModuleForm = new IdeModuleForm($this->Core);
        return $ideModuleForm->Render();
    }
    
    /***
     * Add a helper
     */
    public function ShowAddHelper(){
         $ideHelperForm = new IdeHelperForm($this->Core);
        return $ideHelperForm->Render();
    }
    
    /***
     * Add A widget
     */
    public function ShowAddWidget(){
         $ideHelperForm = new IdeWidgetForm($this->Core);
        return $ideHelperForm->Render();
    }
}
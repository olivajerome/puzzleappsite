var RatingPlugin = function () {};

RatingPlugin.Init = function (e) {

    NoteBox.Init("noteBox", function(note, e){

        let container = e.srcElement.parentNode.parentNode;
    
        Plugin.SendRequest({"plugin": "RatingPlugin",
        "method": "Note",
        "EntityName": container.dataset.entity,
        "EntityId": container.dataset.id,
        "Note": note}, (data) => {
            let reponse = JSON.parse(data);
        });
    });
};
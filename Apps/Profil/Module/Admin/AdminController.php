<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Profil\Module\Admin;

use Apps\Profil\Modele\CompetenceModele;
use Core\Control\TabStrip\TabStrip;
use Core\Controller\AdministratorController;
use Core\View\ElementView;
use Core\View\View;
use Core\Control\EntityGrid\EntityGrid;
use Core\Control\EntityGrid\EntityColumn;
use Core\Control\EntityGrid\EntityIconColumn;
use Core\Control\EntityGrid\EntityFunctionColumn;

/**
 * Description of AdminController
 *
 * @author OLIVA
 */
class AdminController extends AdministratorController
{
    /**
     * Constructeur
     */
    function __construct($core="")
    {
        $this->Core = $core;
    }
    /*
     * Get the home page
     */

    function Show() {
        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);

        $tabStrip = new TabStrip("tabUsers");
        $tabStrip->AddTab($this->Core->GetCode("Profil.Configuration"), $this->GetTabConfig());
        $tabStrip->AddTab($this->Core->GetCode("Profil.Sections"), $this->GetTabSection());
        $tabStrip->AddTab($this->Core->GetCode("Profil.Groups"), $this->GetTabGroupe());
        $tabStrip->AddTab($this->Core->GetCode("Profil.Users"), $this->GetTabUser());

        $skillplugin = \Apps\EeApp\Helper\AppHelper::GetPluginType($this->Core, "Profil" , "Skill");
        
        if($skillplugin != null){
            $tabStrip->AddTab($this->Core->GetCode("Profil.Competence"), $skillplugin->RenderAdmin("Profil"));
        }

        $view->AddElement($tabStrip);
        return $view->Render();
    }

    /*
    * Configuration
    */
    function GetTabConfig() {
    
        $view = new View(__DIR__ . "/View/config.tpl", $this->Core);

        $plugins = \Apps\EeApp\Helper\AppHelper::GetPluginApp($this->Core, "Profil");
        
        $view->AddElement(new ElementView("Plugins", $plugins));

        return $view->Render();
    }

    /*     * *
     * Grille des section
     */

    function GetTabSection() {
        
        $view = new View(__DIR__ . "/View/loadSection.tpl", $this->Core);

        $gdSection = new EntityGrid("gdSection", $this->Core);
        $gdSection->Entity = "Core\Entity\Section\Section";
        $gdSection->App = "Users";
        $gdSection->Action = "GetTabSection";
        $gdSection->ClassName = "AdminController";
        

        $gdSection->AddColumn(new EntityColumn("Name", "Name"));

        $gdSection->AddColumn(new EntityIconColumn("", array(array("EditIcone", "Users.EditUser", "Users.EditUser"),
            )
        ));

        $view->AddElement($gdSection);

        return $view->Render();
    }
    
    /*     * *
     * Grille des groupe
     */

    function GetTabGroupe() {
        
        $view = new View(__DIR__ . "/View/loadGroupe.tpl", $this->Core);

        $gdGroup = new EntityGrid("gdGroup", $this->Core);
        $gdGroup->Entity = "Core\Entity\Group\Group";
        $gdGroup->App = "Users";
        $gdGroup->Action = "GetTabGroup";
        $gdGroup->ClassName = "AdminController";
        

        $gdGroup->AddColumn(new EntityColumn("Name", "Name"));

        $gdGroup->AddColumn(new EntityIconColumn("", array(array("EditIcone", "Users.EditGroup", "Users.EditGroup"),
            )
        ));

        $view->AddElement($gdGroup);

        return $view->Render();
    }
    
     /*     * *
     * Grille des utilisateurs
     */

    function GetTabUser() {
        
        $view = new View(__DIR__ . "/View/loadUser.tpl", $this->Core);

        $gdUser = new EntityGrid("gdUser", $this->Core);
        $gdUser->Entity = "Core\Entity\User\User";
        $gdUser->App = "Users";
        $gdUser->Action = "GetTabUser";
        $gdUser->ClassName = "AdminController";
   
        $gdUser->AddOrder("Id desc");
        $gdUser->AddColumn(new EntityColumn($this->Core->GetCode("Profil.Email"), "Email"));
        $gdUser->AddColumn(new EntityColumn($this->Core->GetCode("Profil.FirstName"), "FirstName"));
        $gdUser->AddColumn(new EntityColumn($this->Core->GetCode("Profil.Name"), "Name"));
        $gdUser->AddColumn(new EntityColumn($this->Core->GetCode("Profil.DateCreate"), "DateCreate"));
        $gdUser->AddColumn(new EntityColumn($this->Core->GetCode("Profil.DateConnect"), "DateConnect"));
        $gdUser->AddColumn(new EntityFunctionColumn($this->Core->GetCode("Profil.Group"), "GetGroup"));
        
        $gdUser->AddColumn(new EntityIconColumn("", 
                array(array("EditIcone", "Profil.EditUser", "Profil.EditUser"),
            )
        ));
        
        $view->AddElement($gdUser);

        return $view->Render();
    }
    /*
     * Show Admin
     */
    function OLD()
    {
        $tbAdmin = new TabStrip("tbAdmin", "Profil");
        $tbAdmin->AddTab($this->Core->GetCode("Profil.Users"), $this->GetTabUsers());
        //  $tbAdmin->AddTab($this->Core->GetCode("Profil.CompetenceCategory"), $this->GetTabCategory());
      //  $tbAdmin->AddTab($this->Core->GetCode("Profil.Competence"), $this->GetTabCompetence());
        
        return $tbAdmin->Show();
    }
    
    /*
     * Get The Category of Compétence
     */
    function GetTabCategory()
    {  
        $view = new View(__DIR__."/View/ShowCategory.tpl", $this->Core);
        
        $category = new ProfilCompetenceCategory($this->Core);
        $view->AddElement(new ElementView("Category", $category->GetAll()));
        
        return $view->Render();
    }
        
    /*
     * Pop in Add Compétence
     */
    function ShowAddCategory($entityId)
    {
        $view = new View(__DIR__."/View/ShowAddCategory.tpl", $this->Core);
        
        //Add Message Modele
        $modele = new CompetenceCategoryModele($this->Core, $entityId);
                 
        // Set modele vith Ajax
        $view->SetModel($modele, true);
        $view->SetApp("Profil");
        $view->SetAction("ShowAddCategory");
        
        return $view->Render();
    }
    
     /*
     * Get The Compétence
     */
    function GetTabCompetence()
    {  
        $view = new View(__DIR__."/View/ShowCompetence.tpl", $this->Core);
        
        $competence = new ProfilCompetence($this->Core);
        $view->AddElement(new ElementView("Competence", $competence->GetAll()));
        
        return $view->Render();
    }
    
    /*
     * Pop in Add Compétence
     */
    function ShowAddCompetence($entityId)
    {
        $view = new View(__DIR__."/View/ShowAddCompetence.tpl", $this->Core);
        
        //Add Message Modele
        $modele = new CompetenceModele($this->Core, $entityId);
                 
        // Set modele vith Ajax
        $view->SetModel($modele, true);
        $view->SetApp("Profil");
        $view->SetAction("ShowAddCompetence");
        
        return $view->Render();
    }
}

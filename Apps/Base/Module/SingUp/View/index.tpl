<section class='container'>
<h1>{{GetCode(Connexion)}}</h1>



{{if Success == true}}

    {{GetCode(Base.CompteCreated)}}
    
{{/if Success == true}}

{{if Success == false}}

<form method='post' action='' class='block'>  
    {{error}} 
    
    <label>{{GetCode(Base.Identifiant)}}</label>
    {{GetControl(EmailBox,login,{Required=true, PlaceHolder=VotreEmail})}} <br/>
   
    <label>{{GetCode(Base.Password)}}</label>
    {{GetControl(PassWord,password,{Required=true, PlaceHolder=VotrePass})}} <br/>
    
    <label>{{GetCode(Base.VerifPassword)}}</label>
    {{GetControl(PassWord,verif,{Required=true, PlaceHolder=Verification})}} <br/>
    
    {{Captcha}}
    
    <div class='center marginTop'>
        <button type='submit' class='btn btn-primary'>{{GetCode(Base.Valid)}}</button>
    </div>
 
</form>
{{/if Success == false}}

</section>
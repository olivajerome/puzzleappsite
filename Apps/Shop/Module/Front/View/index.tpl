
<section class="container">

    <h1>{{GetCode(Shop.WelcomeOnShop)}}</h1>

    <p>{{Shop->Description->Value}}</p>


    <h2>{{GetCode(Shop.Categories)}}</h2>

    {{foreach Categories}}
    <div class='col-md-4'>
        <div class='block'  style='height:350px'>
            <a href ='{{GetPath(/Shop/Category/{{element->Code->Value}})}}'>
                <span style="display:block; width:100%; height: 150px; background-size: cover;background-image: url({{element->GetImagePath()}})">
                </span>
                <div class='bordered clickable'>
                    <h2>{{element->Name->Value}}</h2>
                    <p>{{element->Description->Value}}</p>
                </div>    
            </a>
        </div>
    </div>
    {{/foreach Categories}}

</section>
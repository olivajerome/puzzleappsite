<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\EeXXX;

use Core\Core\Core;
use Apps\Base\Base;

class EeXXX extends Base {

    /**
     * Author and version
     * */
    public $Author = 'Webemyos';
    public $Version = '1.0.0';

    /**
     * Constructeur
     * */
    function __construct() {
        $this->Core = Core::getInstance();
        parent::__construct($this->Core, "EeXXX");
    }

    /**
     * Set public route
     */
    function GetRoute($routes = "") {
        $this->Route->SetPublic(array());

        return $this->Route;
    }

    /*     * *
     * Execute action after install
     */

    function PostInstall() {
        \Apps\EeApp\Helper\AppHelper::UpdateAppParams($this->Core,
                "EeXXX",
                $this->Version,
                "", // Add Description of the app
                0, // Set 1 if the App have a AdminWidgetDashBoard
                0  // Set 1 if the App have a Member Module
        );
    }

    /**
     * Start Admin
     */
    function Run() {
        echo parent::RunApp($this->Core, "EeXXX", "EeXXX");
    }

    /*     * *
     * Run member Controller
     * 
     * /
      /*function RunMember(){
      $memberController = new MemberController($this->Core);
      return $memberController->Index();
      } */

    /**
     * Get The siteMap 
     */
    /* public function GetSiteMap(){
      return SitemapHelper::GetSiteMap($this->Core);
      } */

    /*     * *
     * Get the widget
     */
    /* public function GetWidget($type = "", $params = "") {

      switch ($type) {
      case "AdminDashboard" :
      $widget = new \Apps\EeXXX\Widget\AdminDashBoard\AdminDashBoard($this->Core);
      break;
      }

      return $widget->Render();
      } */

    /*     * *
     * Get Forum Addable widget
     */
    /* public function GetListWidget(){
      return array(array("Name" => "LastMessage",
      "Description" => $this->Core->GetCode("EeXXX.DescriptionLastMessageWidget")),
      );
      } */
}

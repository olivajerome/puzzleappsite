<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Cms\Helper;

use Apps\Cms\Entity\CmsPage;
use Apps\Cms\Entity\CmsContainer;
use Apps\Cms\Entity\CmsBlock;
use Apps\Cms\Entity\CmsBlockItem;
use Core\Entity\Entity\Argument;
use Apps\Cms\Helper;
use Apps\Cms\Helper\BlockHelper;

class ImportHelper {
    /*     * *
     * Import à page to the CMS
     */

    public static function ImportPage($core, $page) {

        $message = "Traitement de la page " . $page->Name;

        if ($page->IdEntite != "") {

            $currentPage = new CmsPage($core);
            $currentPage = $currentPage->GetByName($page->Name);

            if ($currentPage != null) {

                $message .= "<br>------Suppression de la page " . $page->Name;
                //Delete the page, all container and block
                CmsHelper::RemovePage($core, $currentPage->IdEntite);
            }

            //Create Page 
            $newPage = new CmsPage($core);
            $newPage->CmsId->Value = 1;
            $newPage->Name->Value = $page->Name;
            $newPage->Title->Value = $page->Title;
            $newPage->Description->Value = $page->Description;
            $newPage->Content->Value = $page->Content;

            $newPage->Save();
            $newPageId = $core->Db->GetInsertedId();

            foreach ($page->Containers as $container) {
                $message .= self::ImportContainer($core, $container, $newPageId);
            }
        } else {

            foreach ($page->Containers as $container) {
                $message .= self::ImportContainer($core, $container, null, $container->Name);
            }
        }

        return $message;
    }

    /*     * *
     * Import the container
     */

    public static function ImportContainer($core, $container, $newPageId, $containerName = null) {
        $message = "<br/>Traitement du container  " . $container->Name . " pour la page avec l'id ." . $newPageId;

        $newContainer = new CmsContainer($core);

        if ($containerName != "") {

            $newContainer = $newContainer->GetByName($containerName);
            BlockHelper::RemoveContainer($core, $newContainer->IdEntite, false);
        } else {
            $newContainer->PageId->Value = $newPageId;
        }

        $newContainer->Name->Value = $container->Name;
        $newContainer->Code->Value = $container->Code;
        $newContainer->Position->Value = $container->Position;
        $newContainer->Style->Value = $container->Style;
        $newContainer->CssClass->Value = $container->CssClass;

        $newContainer->Save();

        $newContainerId = $core->Db->GetInsertedId();

        foreach ($container->Blocks as $block) {
            $message .= self::ImportBlock($core, $block, ($containerName == null ? $newContainerId : $newContainer->IdEntite));
        }

        return $message;
    }

    /**
     * Import the block
     * @param type $block
     * @return string
     */
    public static function ImportBlock($core, $block, $newContainerId) {
        $message = "<br/>----Traitement du block  " . $block->IdEntite;

        $newBlock = new CmsBlock($core);
        $newBlock->ParentId->Value = $newContainerId;
        $newBlock->Position->Value = $block->Position;
        $newBlock->TypeId->Value = $block->TypeId;
        $newBlock->Size->Value = $block->Size;
        $newBlock->Style->Value = $block->Style;
        $newBlock->CssClass->Value = $block->CssClass;
        $newBlock->Content->Value = $block->Content;
        $newBlock->Widget->Value = $block->Widget;
        $newBlock->Save();

        $newBlockId = $core->Db->GetInsertedId();

        foreach ($block->Items as $item) {

            $newItem = new CmsBlockItem($core);
            $newItem->BlockId->Value = $newBlockId;
            $newItem->Name->Value = $item->Name;
            $newItem->Url->Value = $item->Url;
            $newItem->Save();
        }

        return $message;
    }
}

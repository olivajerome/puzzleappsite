var MarketForm = function(){};

/***
 * Init the form
 * @returns {undefined}
 */
MarketForm.Init = function(){
    Event.AddById("btnSaveMarket", "click", MarketForm.SaveElement);
    Event.AddById("btnAddPlugin", "click", MarketForm.AddPlugin);
    Event.AddByClass("removePlugin", "click" , MarketForm.RemovePlugin);

    //Comment Plugin Script
    Plugin.RegisterJs("pluginCommentScript");
};

/***
* Get the Id of element 
*/
MarketForm.GetId = function(){
    return Form.GetId("MarketFormForm");
};

/*
* Save the Element
*/
MarketForm.SaveElement = function(e){
   
    if(Form.IsValid("MarketFormForm"))
    {
    let request = new Http("Market", "SaveMarket");
        request.SetData(Form.Serialize("MarketFormForm"));

        request.Post(data => {

            data = JSON.parse(data);

            if(data.status == "error"){
                Form.RenderError("MarketFormForm", data.data.message);
            } else{

                Form.SetId("MarketFormForm", data.data.Id);
         
                Animation.Notify(Language.GetCode("Market.MarketSaved"));
                
                if(Dom.GetById("gdCategory") == null){
                     Dashboard.StartApp("", "Market", "");
                }
            }
        });
    }
};

/***
 * Ajout d'un plugin
 */
MarketForm.AddPlugin = function(e){
  
    e.preventDefault();
    
     Dialog.open('', {"title": Dashboard.GetCode("Market.AddPlugin"),
     "app": "EeApp",
     "class": "DialogEeApp",
     "method": "AddPluginApp",
     "params": "Market",
     "type" : "right"
     });
 };
 
 /***
  * Remove plugin to the shop 
  * @returns {undefined}
  */
 MarketForm.RemovePlugin = function(e){
     
     Animation.Confirm(Language.GetCode("Market.ConfirmRemovePlugin"), function(){
         let container = e.srcElement.parentNode;
         
         let  data = "Class=EeApp&Methode=RemovePluginApp&App=EeApp";
              data += "&pluginId="+e.srcElement.id;
              
         Request.Post("Ajax.php", data).then(data => {
             container.parentNode.removeChild(container);
         });
     });
 };
 

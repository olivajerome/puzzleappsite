<?php

namespace Apps\Membre\Widget\Menu;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;

class Menu extends Form {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "") {
        $this->Core = $core;
        $this->Data = $data;
    }

    function Render(){

        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);

        $apps = \Apps\EeApp\Helper\AppHelper::GetForMember($this->Core);
        $view->AddElement(new ElementView("Apps", $apps));
       
        return $view->Render();
    }
}
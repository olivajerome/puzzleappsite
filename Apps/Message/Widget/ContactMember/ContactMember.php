<?php 

namespace Apps\Message\Widget\ContactMember;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;
use Apps\Forum\Entity\ForumMessage;
use Apps\Forum\Helper\MessageHelper;

class ContactMember {
    
    public function __construct($core){
        $this->Core = $core;
    }
    
    public function Render($parameter){
        
        $parameters = explode("-", $parameter);
        $entity = $parameters[0];
        $idEntite = $parameters[1];
        
        $request = "Select UserId from " . $entity. " where Id= ".$idEntite;
        $result = $this->Core->Db->GetLine($request);
        
        
        return "<i id='".$result["UserId"]."' class='fa fa-comment contactMember'></i>";
    }
    
}



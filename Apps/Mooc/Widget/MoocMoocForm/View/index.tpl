<div>
    
    <div class='col-md-2'>
        <ul>
            <li id='editParameter'><i class='fa fa-gears' ></i>{{GetCode(Mooc.Parameters)}}</li> 
            <li><i class='fa fa-list'></i>{{GetCode(Mooc.Lesson)}}
                <i class='fa fa-plus' id='btnAddLesson' title='{{GetCode(Mooc.AddLesson)}}'></i>
            
            </li>
            
            <div id='listLesson'>
            {{foreach Lessons}}
              <li class='lesson editLesson' id='{{element->IdEntite}}'>
                  {{element->Name->Value}}
                  <i class='fa fa-trash removeLesson'></i>
              </li>
            {{/foreach Lessons}}
            </div>
            
            
        </ul>
    </div>
    
    <div class='col-md-8'>
        <div id='formTool'>
            
            {{form->Open()}}
            {{form->Error()}}
            {{form->Success()}}

            {{form->Render(Id)}}
                <div>
                    <label>{{GetCode(Mooc.MoocCategory)}}</label> 
                    {{form->Render(CategoryId)}}
                </div>    
                <div>
                    <label>{{GetCode(Mooc.MoocName)}}</label> 
                    {{form->Render(Name)}}
                </div>
                <div>
                    <label>{{GetCode(Mooc.MoocDescription)}}</label> 
                    {{form->Render(Description)}}
                </div>

               

                <div>
                    <label>{{GetCode(Mooc.MoocImage)}}</label> 
                    {{form->Render(Upload)}}
                </div>

                <div>
                   {{form->RenderImage()}}
                </div>

                <div class='center marginTop' >   
                    {{form->Render(btnSaveMooc)}}
                </div>  

            {{form->Close()}}
        </div>
        
        <div id='lessonTool'></div>
        
    </div>
</div>


var ProductForm = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
ProductForm.Init = function(callBack){
    Event.AddById("btnSaveProduct", "click", ProductForm.SaveProduct);
    ProductForm.callBack = callBack;

    //Catégorie
    Event.AddById("btnAddCategoryProduct", "click", ProductForm.AddCategoryProduct);
    
    //Caracteristiques
    Event.AddById("lstCaracteristique", "change", ProductForm.GetCaracteristiqueItem);
    Event.AddById("btnAddCaracteristiqueProduct", "click", ProductForm.AddCaracteristiqueProduct);

    ProductForm.RenderCategory();
    ProductForm.RenderCaracteristique();
};

/***
* Obtient l'id de l'itin�raire courant 
*/
ProductForm.GetId = function(){
    return Form.GetId("ProductForm");
};

/*
* Sauvegare l'itineraire
*/
ProductForm.SaveProduct = function(e){
   
    if(Form.IsValid("ProductForm"))
    {

    var data = "Class=Shop&Methode=SaveProduct&App=Shop";
        data +=  Form.Serialize("ProductForm");

        Request.Post("Ajax.php", data).then(data => {

            if(data.statut == "Error"){
                Form.RenderError("ProductForm", data.message);
            } else {

                Dialog.Close();

                if(ProductForm.callBack){
                    ProductForm.callBack(data);
                }
            }
        });
    }
};

/***
 * Ajoute une catégorie
 */
ProductForm.AddCategoryProduct = function(e){
    let productId = Form.GetId("ProductForm");
    let categoryId = Dom.GetById("lstCategorie").value;

    var data = "Class=Shop&Methode=AddCategoryProduct&App=Shop";
        data += "&productId=" + productId;
        data += "&categoryId=" + categoryId;
    
    Request.Post("Ajax.php", data).then(data => {
        Dom.GetById("hdCategory").value = data;
    
        ProductForm.RenderCategory();
    });
};

/***
 * Genre le html des categorie a=à partir du json
 */
ProductForm.RenderCategory = function(){

    let category = Dom.GetById("hdCategory").value;
        category = JSON.parse(category);

    let view = "";

    for(let i=0; i < category.length; i++){
        view += "<div id='"+ category[i].IdEntite +"' class='chips removeCategorie'>" +  category[i].CategorieName + "<i class='fa fa-trash'></i></div>";
    }

    Dom.GetById("lstProductCategory").innerHTML = view;

    Event.AddByClass("removeCategorie", "click", ProductForm.RemoveCategorie);
};

/***
 * Supprime une catégorie
 */
ProductForm.RemoveCategorie = function(e){

    let categoryProductId = e.srcElement.id;
    let container  = e.srcElement.parentNode;

    Animation.Confirm(Language.GetCode("Shop.ConfirmRemoveCategoryProduct"), ()=>{

        let data = "Class=Shop&Methode=RemoveCategorieProduct&App=Shop";
            data += "&Id=" + categoryProductId;
       
        Request.Post("Ajax.php", data).then(data => {
            container.parentNode.removeChild(container);
        });
    });
};

/***
 * Charge les items d'une caractéristique
 */
ProductForm.GetCaracteristiqueItem = function(e){

    let caracteristiqueId = e.srcElement.value;
    let lstCaracteristiqueItem = Dom.GetById("lstCaracteristiqueItem");

    let data = "Class=Shop&Methode=GetCaracteristiqueItem&App=Shop";
    data += "&CaracteristiqueId=" + caracteristiqueId;

    Request.Post("Ajax.php", data).then(data => {
    
        lstCaracteristiqueItem.innerHTML = "";
        data = JSON.parse(data);
    
        for(let i =0; i < data.length; i++){

            lstCaracteristiqueItem.innerHTML += "<option value='"+data[i].IdEntite+"' >" + data[i].Label  +"</option>"
        }
    });
};

/***
 * Ajoute une caracteristuqie à un produit
 */
ProductForm.AddCaracteristiqueProduct = function(){

    let productId = Form.GetId("ProductForm");
    let caracteristiqueId = Dom.GetById("lstCaracteristique").value;
    let caracteristiqueItemId = Dom.GetById("lstCaracteristiqueItem").value;
   
    if(caracteristiqueItemId != ""){
        let data = "Class=Shop&Methode=AddCaracteristiqueProduct&App=Shop";
            data += "&productId=" + productId;
            data += "&caracteristiqueId=" + caracteristiqueId;
            data += "&itemId=" + caracteristiqueItemId;

        Request.Post("Ajax.php", data).then(data => {
            Dom.GetById("hdCaracteristique").value = data;

            ProductForm.RenderCaracteristique();
        });
    } else {
        Animation.Notify(Language.GetCode("Shop.YouNeedSelectCaracteristiqueAndItem"))
    }
};

/***
 * Genre le html des categorie a=à partir du json
 */
ProductForm.RenderCaracteristique = function(){

    let caracteristique = Dom.GetById("hdCaracteristique").value;
        caracteristique = JSON.parse(caracteristique);

    let view = "";

    for(let i=0; i < caracteristique.length; i++){
        view += "<div id='"+ caracteristique[i].IdEntite +"' class='chips removeCaracteristique'>" +  caracteristique[i].CaracteristiqueName + "-" + caracteristique[i].CaracteristiqueItemName +  "<i class='fa fa-trash'></i></div>";
    }

    Dom.GetById("lstProductCaracteristique").innerHTML = view;

    Event.AddByClass("removeCaracteristique", "click", ProductForm.RemoveCaracteristique);
};

/***
 * Supprime une caracteristique
 */
ProductForm.RemoveCaracteristique = function(){
    let categoryProductId = e.srcElement.id;
    let container  = e.srcElement.parentNode;

    Animation.Confirm(Language.GetCode("Shop.ConfirmRemoveCaracteristiqueProduct"), ()=>{

        let data = "Class=Shop&Methode=RemoveCaracteristiqueProduct&App=Shop";
            data += "&Id=" + categoryProductId;
       
        Request.Post("Ajax.php", data).then(data => {
            container.parentNode.removeChild(container);
        });
    });
};


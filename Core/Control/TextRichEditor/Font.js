class FontSize extends BaseTool {
    getIcone() {
        return "<select><option>Font-size</option></option><option value='10'>10px</option><option value='12'>12px</option><option value='24'>24px</option></select>";
    }

    getName() {
       return Language.GetCode("Base.TextRichEditorFonSize");
    }
    execute(e) {
        let instance = this;

        this.select = this.container.querySelector("select");
        this.select.addEventListener("change", (e) => {
            let value = e.srcElement.value;
            instance.addStyle("fontSize", value + "px");
            super.execute();
        });
    }
}

class FontColor extends BaseTool {
    getIcone() {
        return "<select><option>Color</option></option><option value='red'>red</option><option value='blue'>blue</option><option value='black'>black</option></select>";
    }

    getName() {
        return Language.GetCode("Base.TextRichEditorFontColor");
    }
    execute(e) {
        let instance = this;

        this.select = this.container.querySelector("select");
        this.select.addEventListener("change", (e) => {
            let value = e.srcElement.value;
            instance.addStyle("color", value);
            super.execute();
        });
    }
}


class BackgroundColor extends BaseTool {
    getIcone() {
        return "<select><option>Back ground color</option></option><option value='black'>black</option><option value='white'>white</option><option value='red'>red</option><option value='blue'>blue</option></select>";
    }

    getName() {
        return Language.GetCode("Base.TextRichEditorBackGroundColor");
    }
    execute(e) {
        let instance = this;

        this.select = this.container.querySelector("select");
        this.select.addEventListener("change", (e) => {
            let value = e.srcElement.value;
            instance.addStyle("backgroundColor", value);
            super.execute();
        });
    }
}






<?php 
/*
* PuzzleApp
* Webemyos
* Jérôme Oliva
* GNU Licence
*/

namespace Apps\WorkFlow\Entity;
use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class WorkFlowAction extends Entity  
{
	//Constructeur
	function __construct($core)
	{
		//Version
		$this->Version ="2.0.0.0"; 

		//Nom de la table 
		$this->Core=$core; 
		$this->TableName="WorkFlowAction"; 
		$this->Alias = "WorkFlowAction"; 

		$this->WorkFlowId = new Property("WorkFlowId", "WorkFlowId", NUMERICBOX,  false, $this->Alias); 
		$this->Name = new Property("Name", "Name", TEXTAREA,  false, $this->Alias); 
		$this->Action = new Property("Action", "Action", TEXTAREA,  false, $this->Alias); 
		$this->DateExec = new Property("DateExec", "DateExec", TEXTAREA,  false, $this->Alias); 

		//Creation de l entité 
		$this->Create(); 
	}
}
?>
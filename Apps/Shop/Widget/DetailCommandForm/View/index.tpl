{{form->Open()}}
{{form->Error()}}
{{form->Success()}}

{{form->Render(Id)}}

    <div>
        <label>{{GetCode(Shop.Ref)}}</label> 
        {{form->Render(Name)}}
    </div>
    <div>
        <label>{{GetCode(Shop.Status)}}</label> 
        {{form->Render(Status)}}
    </div>

    <div>
        <label>{{GetCode(Shop.DateCreated)}}</label> 
        {{form->Render(DateCreated)}}
    </div>

    <div>
        {{address}}
    </div>
    
    
    <div>
        {{products}}
    </div>
    

    <div class='center marginTop' >   
        {{form->Render(btnUpdateCommand)}}
    </div>  

{{form->Close()}}
<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Admin;

use Apps\Admin\Module\DashBoard\DashBoardController;
use Core\Core\Core;
use Core\App\Application;
use Core\Core\Request;

use Apps\Admin\Helper\UserWidgetHelper;
use Apps\Admin\Module\Log\LogController;

/**
 * Description of Admin
 *
 * @author OLIVA
 */
class Admin extends Application
{
    
    public $Author = 'Webemyos';
    public $Version = '2.1.0.0';
    
     /*
     * Créate de app Base
     */
    public function __construct()
    {
        $this->Core = Core::getInstance();
        parent::__construct($this->Core, "Admin");
    }
    
     /**
     * Execution de l'application
     */
    function Run() {
        echo parent::RunApp($this->Core, "Admin", "Admin");
    }
    
    /**
     * Set Admin Public
     */
    public function GetRoute()
    {
        $this->Route->SetPublic(array("Admin"));
        return $this->Route;
    }

    /*
     * Get the master View
     */
    public function GetMasterView()
    {
        $dashBoardController = new DashBoardController();
        return $dashBoardController->GetMasterView();
    }
    
    /**
     * Obteint les derniers sujets
     */
    function GetWidget($type, $params) {
       
        switch($type){
            case 'Tools' :
                $widget = new \Apps\Admin\Widget\AdminTools\AdminTools($this->Core);
            break;
        }
        
        return $widget->Index($params);
    }
    
    /***
     * Retourne les logs
     */
    function GetLog(){
        $logController = new LogController($this->Core);
        return $logController->GetLog();
    }

    /***
     * Vide la session de debuggage
     */
    function CleanDebugger(){
        Trace::CleanDebugger();
    }
    
    /*
     * Home page 
     */
    public function Index()
    {
    }

    /***
     * Charge le tableau de bord Admin
     */
    public function LoadDashboard(){
        $dashboardController = new DashBoardController($this->Core);
        return $dashboardController->Index();
    }


    /***
     * Ajoute un widget au tableau de bord de l'utilisateur
     */
    public function AddWidgetUser(){
        return UserWidgetHelper::AddWidget($this->Core, Request::GetPosts());
    }

    /***
     * Supprime un widget du tableau de bord utilisateur
     */
    public function RemoveWidgetUser(){
        return UserWidgetHelper::RemoveWidget($this->Core, Request::GetPosts());
    }
}

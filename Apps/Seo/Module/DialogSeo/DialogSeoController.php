<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Seo\Module\DialogSeo;

use Core\Control\Button\Button;
use Core\Controller\Controller;
use Core\View\View;
use Core\View\ElementView;

use Apps\Seo\Widget\PageSeoForm\PageSeoForm;
use Apps\Seo\Entity\SeoPage;

use Core\Control\TextArea\TextArea;

/*
 * 
 */

class DialogSeoController extends Controller {

    /**
     * Constructeur
     */
    function _construct($core = "") {
        $this->Core = $core;
    }

    /**
     * Creation
     */
    function Create() {
        
    }

    /**
     * Initialisation
     */
    function Init() {
        
    }

    /**
     * Affichage du module
     */
    function Show($all = true) {
        return $this->Index();
    }

    /*
     * Get the home page
     */

    function Index() {
        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);
        return $view->Render();
    }

    /**
     * Ajout d'une page
     */
    function ShowAddPage($id){
        
        $pageForm = new PageSeoForm($this->Core);
        
        if($id != ""){
            
            $page = new SeoPage($this->Core);
            $page->GetById($id);
            $pageForm->Load($page);
        }
        
        return $pageForm->Render();
    }
   
    /* action */
}

?>
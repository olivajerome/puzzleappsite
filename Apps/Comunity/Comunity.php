<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Comunity;

use Apps\Coopere\Coopere;
use Core\Core\Core;
use Core\Core\Request;
use Core\Core\Response;
use Apps\Forum\Forum;
use Core\App\Application;
use Apps\Forum\Module\Forum\ForumController;
use Apps\Forum\Entity\ForumForum;
use Apps\Comunity\Entity\ComunityComunity;
use Apps\Comunity\Entity\ComunityMessage;
use Apps\Forum\Helper\MessageHelper;
use Apps\Comunity\Helper\ComunityHelper;
use Apps\Comunity\Module\Home\HomeController;
use Apps\Comunity\Module\Front\FrontController;
use Apps\Comunity\Module\Message\MessageController;
use Apps\Comunity\Module\Wall\WallController;

use Apps\Comunity\Widget\UserProjetWidget\UserProjetWidget;
use Apps\Comunity\Widget\ActionProjetWidget\ActionProjetWidget;
use Apps\Comunity\Widget\MemberProjetWidget\MemberProjetWidget;
use Core\Dashboard\DashBoardManager;
use Core\Entity\User\User;

use Apps\Comunity\Module\Member\MemberController;

class Comunity extends Application {

    /**
     * Auteur et version
     * */
    public $Author = 'Eemmys';
    public $Version = '2.0.0';
    public static $Directory = "../Apps/Comunity";


    function __construct($core) {
        $this->Core = Core::getInstance();
        parent::__construct($this->Core, "Comunity");
    }

    /***
      * Execute action after install
      */
     function PostInstall() {
        \Apps\EeApp\Helper\AppHelper::UpdateAppParams($this->Core,
                "Comunity",
                $this->Version,
                "Application de forum.",
                1,
                1
        );
        
        \Apps\EeApp\Helper\AppHelper::CreateDataDir("Comunity");
    }
    
    /**
     * Execution de l'application
     */
    function Run() {
        echo parent::RunApp($this->Core, "Comunity", "Comunity");
    }
    
     /***
     * Run member Controller
     */
    function RunMember(){
        $memberController = new MemberController($this->Core);
        return $memberController->Index();
    }
    
    /**
     * Définie les routes publiques
     */
    function GetRoute($routes = "") {
        $this->Route->SetPublic(array("Group"));
        return $this->Route;
    }

    /**
     * Obteint les derniers sujets
     */
    function GetWidget($type = "", $params = "") {
        return $this->GetLastInfo();
    }

    /**
     * Page d'accueil avec les derniers post
     */
    function Index() {
        $frontController = new FrontController($this->Core);
        return $frontController->Index();
    }

    /**
     * Nouvelle mise en pages des projet de la page d'accueil
     */
    function GetLastInfo() {
        return "";
        $frontController = new FrontController($this->Core);
        return $frontController->LoadInfo();
    }

    /**
     * Obtient les dérniers messages
     * @return string
     */
    function GetLastInfoOld() {
        $messages = DiscussHelper::GetLastRencontreByReseau($this->Core, Coopere::$reseauName);

        $html .= '<div class="">
            <a href="' . $this->Core->GetPath("/" . Coopere::$reseauName . "/Comunity/NewDiscussion") . '"
            style="width: 100%;" type="button" class="btn btn-primary">Nouveau sujet</a>
            </div>';

        $html .= "<ul style='margin-top:10px'>";

        foreach ($messages as $message) {
            $html .= "<li style='text-align: left;margin-top:5px'>";
            $html .= "<i class='fa fa-angle-right' aria-hidden='true'></i><a href='" . $this->Core->GetPath("/" . Coopere::$reseauName . "/Comunity/Rencontre/" . $message->IdEntite . "/" . $message->Title->Value) . "' >" . $message->Title->Value . "</a>";

            $html .= "</li>";
        }

        $html .= "</ul>";

        $html .= '<div class="col-md-12 col-xs-12 section-padding">
		<a href="' . $this->Core->GetPath("/" . Coopere::$reseauName . "/Comunity/Rencontres") . '" style="width: 100%;" type="button" class="btn btn-light">Tous les sujets</a>
		</div>';

        return $html;
    }

    /***
     * Créate new Comunity
     */
    public function SaveComunity(){
        
        if(ComunityHelper::SaveComunity($this->Core, Request::GetPosts())){        
            return Response::Success(array("Message" => $this->Core->GetCode("Comunity.ComunityCreated")));
        }
    }
    
    
    /**
     * Nouveau système des rencontres thématiques 
     * Création d'une nouvelle rencontres
     * @return string
     */
    function NewDiscussion() {
        $this->Core->MasterView->Set("Title", "Nouveau projet");

        $homeController = new HomeController($this->Core);
        return $homeController->NewDiscussion();
    }

    /**
     * Toutes les rencontres
     */
    function Rencontres() {
        $this->Core->MasterView->Set("Title", "Rencontres thématiques");

        $homeController = new HomeController($this->Core);
        return $homeController->Rencontres(Coopere::$reseauName);
    }

    /**
     * Détail d'une rencontre
     * */
    function Group($groupId) {
        $this->Core->MasterView->Set("Title", "Groupe");

        $frontController = new FrontController($this->Core);
        return $frontController->Group($groupId);
    }

    /*
     * Suivi d'une rencontre thématique
     * 
     */

    function ParticipateProjet() {
        DiscussHelper::ParticipateProjet($this->Core, Request::GetPost("projetId"));
        return "OK";
    }

    /**
     * Ne plus participer
     */
    function UnParticipate() {
        DiscussHelper::UnParticipateProjet($this->Core, Request::GetPost("projetId"));
    }

    /**
     * Ajout un message à la conversation courante
     */
    function AddMessage() {
        $homeController = new FrontController($this->Core);
        return $homeController->AddMessage(Request::GetPost("users"),
                        Request::GetPost("message"),
                        Request::GetPost("objet"),
                        Request::GetPost("comunityId"),
                        Request::GetPost("images"),
                        0,
                        "",
                        Request::GetPost("documents"),
                        Request::GetPost("type")
        );
    }

    /**
     * Ajotu d'un like sur une dissus ou un message
     */
    public function AddLike() {
        ComunityHelper::AddLike($this->Core,
                Request::GetPost("messageId"),
                Request::GetPost("type")
        );

        $request = "Select GROUP_CONCAT(userLike.id SEPARATOR ':') as userLike 
                         FROM ComunityLike as lk 
                         JOIN ee_user as userLike on userLike.Id = lk.UserId
 
                         WHERE lk.EntityId = " . Request::GetPost("messageId") . " and lk.EntityName = 'message' ";

        $result = $this->Core->Db->GetLine($request);

        if ($result["userLike"] != "") {
            $users = explode(":", $result["userLike"]);
            $userLikeHtml = "";

            foreach ($users as $user) {
                $userLikeHtml .= "<div class='avatarmini-img avatarmini'>";

                if(file_exists("Data/Apps/Profil/User/". $user . "/thumb.png")){
                    $image = $this->Core->GetPath(str_replace("Data" , "/Data", "Data/Apps/Profil/User/". $user . "/thumb.png")) ; 
                } else{
                    $image = "/images/noprofil.png";
                }
                $userLikeHtml .=   '<span class="imgProfilSmall" style="background-image: url('.$image.')"></span>';
              
            }
            $userLikeHtml .= "<div style='width:65px !important;margin-left:50px'> " . count($users) . " j'aime(s)</div>";

            return $userLikeHtml;
        } else {
            return "0 j'aime";
        }
    }

    /**
     * Ajotu un commentaire sur un message
     */
    public function AddComment() {
        $commentId = ComunityHelper::AddComment($this->Core,
                        Request::GetPost("messageId"),
                        Request::GetPost("message"),
                        Request::GetPost("images")
        );

        return $this->GetComment();
    }

    /**
     * Obtient les commentaires d'un message
     */
    public function GetComment($commentId = "", $messageId = "") {
        if ($messageId != "") {
            $comments = ComunityHelper::GetComment($this->Core, $messageId);
        } else if ($commentId == "") {
            $comments = ComunityHelper::GetComment($this->Core, Request::GetPost("messageId"));
        } else {
            $comments = ComunityHelper::GetComment($this->Core, "", $commentId);
        }
        $html = "";

        if (count($comments) > 0) {

            foreach ($comments as $comment) {
                $user = new User($this->Core);
                $user->GetById($comment->UserId->Value);

                $html .= "<section> ";
                $html .= "<div style='width: 70px; text-align: center; zoom: 0.7; top: -10px;position: relative;' title='" . $user->GetPseudo() . "' class='avatarmini-img avatarmini'>";
                $html .= "<span class='imgProfilSmall' style='background-image: url(".$user->GetImageMini().") '></span></div>";
                  
                
                $html .= "<div class='commentMessageUser'  id='" . $comment->IdEntite . "'>";

                $html .= "<div class='moreAction' id='" . $comment->IdEntite . "' data-type ='comment' style='float:right'>...</div>";

                $html .= "<div><h6>" . $user->GetPseudo() . "</h6><p>" . $comment->Message->Value . "</p></div>";
                $html .= "</div>";

                if ($commentId == "") {


                    $html .= "<div class='actionComment' data-comment='" . $comment->IdEntite . "'><div class='likeComment' id='" . $comment->IdEntite . "' data-type ='comment' style=''><b class='nbLikeComment'>" . ComunityHelper::GetNumberLike($this->Core, $comment->IdEntite) . "</b>&nbsp;<i class='fa fa-thumbs-o-up '>&nbsp;</i></div>";

                    $html .= "<b class='likeComent'> J'aime(s)</b><b class='commentComent' > - répondre</b></div>";
                    $html .= "<div class='lstComment'>" . $this->GetComment($comment->IdEntite) . "</div>";
                }

                $html .= "</section>";
            }
        }

        return $html;
    }

    /**
     * Retourne les actions supplémentaitre
     */
    public function GetMoreAction() {
        $html = "<ul>";

        if (ComunityHelper::IsUserOrAdmin($this->Core, Request::GetPost("messageId"), Request::GetPost("type"))) {

            if (Request::GetPost("type") == "message") {
                $message = new ComunityMessage($this->Core);
                $message->GetById(Request::GetPost("messageId"));

                if ($message->Type->Value != 5) {
                    $html .= "<li ><i class='fa fa-edit' id='editAction' >&nbsp;Modifier</i></li>";
                }
            } else {
                $html .= "<li ><i class='fa fa-edit' id='editAction' >&nbsp;Modifier</i></li>";
            }

            $html .= "<li><i class='fa fa-trash' id='removeAction' >&nbsp;Supprimer</i></li>";
        }

        $html .= "<li><i class='fa fa-share' id='signalAction' >&nbsp;Signaler</i></li>";
        $html .= "</ul>";

        return $html;
    }

    /**
     * Met à jour un message 
     */
    public function UpdateMessage() {
        return ComunityHelper::UpdateMessage($this->Core,
                        Request::GetPost("messageId"),
                        Request::GetPost("newMessage"),
        );
    }

    /**
     * Met à jour un commentaire 
     */
    public function UpdateComment() {
        return ComunityHelper::UpdateComment($this->Core,
                        Request::GetPost("messageId"),
                        Request::GetPost("newMessage"),
        );
    }

    /**
     * Supprime un message et tous les commentaires associés
     */
    public function DeleteMessage() {
        return ComunityHelper::DeleteMessage($this->Core,
                        Request::GetPost("messageId"));
    }

    /*     * *
     * Supprime un commentaire
     */

    public function DeleteComment() {
        return ComunityHelper::DeleteComment($this->Core,
                        Request::GetPost("messageId"));
    }

    /**
     * Ajout d'un like sur un comment
     */
    public function LikeComment() {
        ComunityHelper::AddLike($this->Core,
                Request::GetPost("commentId"),
                "comment"
        );

        return ComunityHelper::GetNumberLike($this->Core, Request::GetPost("commentId"));
    }

    /*     * *
     * Ajoute un message sur un commentaire
     */

    public function AddCommentComment() {
        $commentId = ComunityHelper::AddComment($this->Core,
                        Request::GetPost("commentId"),
                        Request::GetPost("message"),
                        "",
                        "comment"
        );

        return $this->GetComment(Request::GetPost("commentId"));
    }

    /**
     * Charge mes projets selon leur Etat
     */
    function LoadTypeProjet() {
        $userProjetWidget = new UserProjetWidget($this->Core, Request::GetPost("reseauCode"), Request::GetPost("statut"));
        return $userProjetWidget->Render();
    }

    /**
     * Charge unprojet
     */
    function LoadProjet() {
        $homeController = new HomeController($this->Core);
        return $homeController->DetailRencontre(Request::GetPost("projetId"), "", false);
    }

    /**
     * Ajout d'un suivie de projet
     */
    function FollowProjet() {
        DiscussHelper::AddFollow($this->Core, Request::GetPost("projetId"));
    }

    /**
     * Ajout d'un suivie de projet
     */
    function UnFollowProjet() {
        DiscussHelper::UnFollowProjet($this->Core, Request::GetPost("projetId"));
    }

    /**
     * Charge les Projet de l'utilisateur
     */
    function LoadUserProjet() {
        $userProjetWidget = new UserProjetWidget($this->Core, Request::GetPost("reseauCode"));
        return $userProjetWidget->Render();
    }

    /*
     * Plus d'actions des Projets
     */

    function GetMoreActionProjet() {
        $actionProjetWidget = new ActionProjetWidget($this->Core, Request::GetPost("projetId"));
        return $actionProjetWidget->GetMoreAction(Request::GetPost("projetId"));
    }

    /**
     * Charge les membre de l'utilisateur
     */
    function LoadMemberProjet() {
        $projet = new DiscussDiscuss($this->Core);
        $projet->GetById(Request::GetPost("projetId"));

        $memberProjetWidget = new MemberProjetWidget($this->Core, Request::GetPost("projetId"), $projet);
        return $memberProjetWidget->Render();
    }

    /*     * *
     * On supprime complétement le projet
     */

    function DeleteProjet() {
        DiscussHelper::DeleteDiscuss($this->Core, Request::GetPost("projetId"));
    }

    /**
     * Archiver le projet
     */
    function ArchiveProjet() {
        DiscussHelper::ArchiveProjet($this->Core, Request::GetPost("projetId"));
    }

    /*     * *
     * Charge les documents du projet
     */

    function LoadDocument() {
        $homeController = new HomeController($this->Core);
        return $homeController->LoadDocument(Request::GetPost("projetId"));
    }

    /*     * *
     * Pop in de discussion avec un memebre
     */

    function GetTchatMessage() {
        $messageController = new MessageController($this->Core);
        return $messageController->Index(Request::GetPost("userId"));
    }

    /*     * *
     * Charge la page suivante
     */

    function LoadNextPage() {
        $homeController = new HomeController($this->Core);
        return $homeController->GetMessages(Request::GetPost("projetId"), "", "", "", Request::GetPost("page"));
    }

    /**
     * Charge les disccussion privé entre les membres du projet
     */
    function LoadDiscuss() {
        $homeController = new HomeController($this->Core);
        return $homeController->LoadDiscussProjet(Request::GetPost("projetId"));
    }

    /**
     * Envoi une inviation à découvrir le projet
     */
    function SendNofication() {
        $user = Request::GetPost("user");
        $projet = new DiscussDiscuss($this->Core);
        $projet->GetById(Request::GetPost("projetId"));

        $message = "Bonjour, " . $this->Core->User->GetPseudo() . " vous invite à découvir le projet";
        $message .= "<br/><a style='background:#2bb4ac;border:15px solid #2bb4ac;padding:0px;color:#ffffff;font-family:sans-serif;font-size:15px;line-height:1.1;text-align:center;text-decoration:none;display:block;border-radius:2px;font-weight:normal'  href='" . $this->Core->GetPath("/" . Request::GetPost("reseauCode") . "/Comunity/Rencontre/" . $projet->IdEntite) . "'>" . $projet->Title->Value . "</a>";

        $notify = DashBoardManager::GetApp("Notify", $this->Core);

        foreach (explode(",", $user) as $userId) {

            $notify->AddNotify($this->Core->User->IdEntite, "InvitationProjet", $userId, "Projet", $projet->IdEntite, "Venez découvrir ce projet", $message);
        }
    }

    /*     * *
     * Ajout un projet sur la page d'accueil
     */

    function PinProjet() {
        $request = "Update DiscussDiscuss Set HomePage = 1 Where Id = " . Request::GetPost("projetId");
        $this->Core->Db->Execute($request);
    }
    
    /****
     * Get all message of the 
     */
    function GetMessages(){
        return Response::Success(\Apps\Comunity\Helper\ComunityHelper::GetMessagesWithComment($this->Core, 1));
    }
    
    /**
     * Getr A detail of a message
     * @return type
     */
    function GetDetail(){
        
        $message = new \Apps\Comunity\Entity\ComunityMessage($this->Core);
        $message->GetById(33);
        
        $comment = new \Apps\Comunity\Entity\ComunityComment($this->Core);
        $comments = $comment->Find("MessageId=" . $message->IdEntite);
        
        return Response::Success(array("Message" => $message->ToArray(),
                                        "Comments" => $comment->ToAllArray($comments))
            );
    }
    
     /**
     * Charge le mur de l'utilisateur
     */
    public function LoadMyWall($show = true)
    {
        $wallController = new WallController($this->Core);
        return $wallController->LoadMyWall(Request::GetPost("ComunityId"));
    }

    /*     * *
     * Get cms Addable widget
     */

     public function GetListWidget() {
        return array(array("Name" => "LastMessage",
                "Description" => $this->Core->GetCode("Comunity.LastMessage")),
        );
    }

    public function GetDetailNotify(){
        
    }

}

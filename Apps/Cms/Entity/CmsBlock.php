<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Cms\Entity;

use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;
use Apps\Cms\Entity\CmsBlockItem;

class CmsBlock extends Entity {

    //TYPE OF BLOCK
    const TYPE_TEXT   = 1;
    const TYPE_IMAGE  = 2;
    const TYPE_LINK   = 3;
    const TYPE_MENU   = 4;
    const TYPE_WIDGET = 5;


    //Constructeur
    function __construct($core) {
        //Version
        $this->Version = "2.0.0.0";

        //Nom de la table 
        $this->Core = $core;
        $this->TableName = "CmsBlock";
        $this->Alias = "CmsBlock";

        $this->ParentId = new Property("ParentId", "ParentId", NUMERICBOX, false, $this->Alias);
        $this->Position = new Property("Position", "Position", NUMERICBOX, false, $this->Alias);
        $this->TypeId = new Property("TypeId", "TypeId", NUMERICBOX, false, $this->Alias);
        $this->Size = new Property("Size", "Size", NUMERICBOX, false, $this->Alias);
        $this->Style = new Property("Style", "Style", TEXTAREA, false, $this->Alias);
        $this->CssClass = new Property("CssClass", "CssClass", TEXTBOX,  false, $this->Alias); 
        $this->Content = new Property("Content", "Content", TEXTAREA, false, $this->Alias);
        $this->Widget = new Property("Widget", "Widget", TEXTAREA, false, $this->Alias);

        //Creation de l entité 
        $this->Create();
    }

    /**
     * Get the items of the block
     */
    function GetItems(){

        $blockItems = new CmsBlockItem($this->Core);
        return $blockItems->Find('BlockId= ' . $this->IdEntite);
    }
}

?>
<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Market\Module\Admin;

use Core\Control\Button\Button;
use Core\Controller\Controller;
use Core\View\View;
use Core\View\ElementView;
use Core\Control\TabStrip\TabStrip;
use Core\Control\EntityGrid\EntityGrid;
use Core\Control\EntityGrid\EntityColumn;
use Core\Control\EntityGrid\EntityIconColumn;
use Core\Control\EntityGrid\EntityFunctionColumn;
use Core\Entity\Entity\Argument;
use Core\Utility\Format\Format;

use Apps\Market\Helper\MarketHelper;
use Apps\Market\Widget\MarketForm\MarketForm;
use Apps\Market\Entity\MarketMarket;
use Apps\Market\Widget\MarketCategoryForm\MarketCategoryForm;
use Apps\Market\Widget\MarketCaracteristiqueForm\MarketCaracteristiqueForm;

use Apps\Market\Entity\MarketCategory;
use Apps\Market\Entity\MarketCaracteristique;
use Apps\Market\Entity\MarketCaracteristiqueItem;


/*
 * 
 */
 class AdminController extends Controller
 {
    /**
     * Constructeur
     */
    function _construct($core="")
    {
          $this->Core = $core;
    }

    /**
     * Create
     */
    function Create()
    {
    }

    /**
     * Init
     */
    function Init()
    {
    }

    /**
     * Render
     */
    function Show($all=true)
    {
         return $this->Index();
    }
    
    /*
    * Get the home page
    */
   function Index()
   {
    $market = MarketHelper::GetMarket($this->Core);

    if(count($market) > 0){
      
      $market = $market[0];
      $view = new View(__DIR__."/View/index.tpl", $this->Core);
     
      $tabMarket = new TabStrip("tabMarket", "Market"); 
      $tabMarket->AddTab($this->Core->GetCode("market.market"), $this->GetTabMarket($market));
      $tabMarket->AddTab($this->Core->GetCode("market.Categories"), $this->GetTabCategory());
      $tabMarket->AddTab($this->Core->GetCode("market.Caracteristiques"), $this->GetTabCaracteristique());
      $tabMarket->AddTab($this->Core->GetCode("market.Product"), $this->GetTabProduct());
     // $tabMarket->AddTab($this->Core->GetCode("market.Command"), $this->GetTabCommand());
      
      $view->AddElement(new ElementView("tabMarket", $tabMarket->Render()));

     } else {
      $view = new View(__DIR__."/View/noMarket.tpl", $this->Core);

      $marketForm = new MarketForm($this->Core);
      $view->AddElement(new ElementView("marketForm", $marketForm->Render()));
    }

    return $view->Render();
   }

   /***
    * Get te market Property
    */
   function GetTabMarket($market){

        $marketForm = new MarketForm($this->Core, $market);
        $marketForm->Load($market);
        
        return $marketForm->Render(); 
   }

   /***
    * Save the market Place
    */
   function SaveMarket($data){
    
        $marketForm = new MarketForm($this->Core, $data);

        if($marketForm->Validate($data)){

            $market = new MarketMarket($this->Core);

            if($data["Id"] != ""){
                $market->GetById($data["Id"]);
            }

            $marketForm->Populate($market);
            $market->Save();

            return true;
        }
         
        return $marketForm->errors;
   }

   /***
    * Categorie de la shop
    */
    function GetTabCategory(){

     $gdCategory = new EntityGrid("gdCategory", $this->Core);
     $gdCategory->Entity = "Apps\Market\Entity\MarketCategory";
     $gdCategory->App = "Market";
     $gdCategory->Action = "GetTabCategory";

     $btnAdd = new Button(BUTTON, "btnAddCategory");
     $btnAdd->Value = $this->Core->GetCode("Market.AddMarket");

     $gdCategory->AddButton($btnAdd);

     $gdCategory->AddColumn(new EntityColumn("Name", "Name"));
     
     $gdCategory->AddColumn(new EntityIconColumn("Action", 
                                               array(array("EditIcone", "Market.EditCategory", "Market.EditCategory"),
                                                     array("DeleteIcone", "Market.DeleteCategory", "Market.DeleteCategory"),
                                               )    
                       ));

     return $gdCategory->Render();
  }

  /***
   * Sauvegarde une catégorie
  */
  public static function SaveCategory($core, $data){

     $categoryForm = new MarketCategoryForm($core, $data);

     if($categoryForm->Validate($data)){

         $category = new MarketCategory($core);
         
         if($data["Id"] != "" ){
             $category->GetById($data["Id"]);
         } else {
             $Market = MarketHelper::GetMarket($core);
             $category->MarketId->Value = $Market[0]->IdEntite;
         }

         $category->Code->Value = Format::ReplaceForUrl($data["Name"]);
      
         $categoryForm->Populate($category);
         $category->Save();

         if($data["Id"] == ""){
             $category->IdEntite = $core->Db->GetInsertedId();
         }
         
         $category->SaveImage($data["dvUpload-UploadfileToUpload"], true);
         
         return true;
     }

     return false;
 }
 
 /***
  * Supprime une catégorie
  */
 public static function DeleteCategory($core, $categoryId){

    //Suppression dans les produit
    //TODO REACTIVER
    // $productCategory = new MarketProductCategory($core);
    // $productCategory->AddArgument(new Argument("Apps\Market\Entity\MarketProductCategory" , "CategoryId", EQUAL, $categoryId));
    // $productCategory->DeleteByArg();
     
     $category = new MarketCategory($core);
     $category->GetById($categoryId);
     $category->Delete();
     
     return true;
 }
 
 
  /***
    * Caracteristiques des produits
    */
   function GetTabCaracteristique(){
       
      $gdCaracteristique = new EntityGrid("gdCaracteristique", $this->Core);
      $gdCaracteristique->Entity = "Apps\Market\Entity\MarketCaracteristique";
      $gdCaracteristique->App = "Market";
      $gdCaracteristique->Action = "GetTabCaracteristique";

      $btnAdd = new Button(BUTTON, "btnAddCaracteristique");
      $btnAdd->Value = $this->Core->GetCode("Market.AddCaracteristique");

      $gdCaracteristique->AddButton($btnAdd);

      $gdCaracteristique->AddColumn(new EntityColumn("Name", "Name"));
      
      $gdCaracteristique->AddColumn(new EntityIconColumn("Action", 
                                                array(array("EditIcone", "Market.EditCaracteristique", "Market.EditCaracteristique"),
                                                      array("DeleteIcone", "Market.DeleteCaracteristique", "Market.DeleteCaracteristique"),
                                                )    
                        ));

      return $gdCaracteristique->Render();
   }
 
       /**
     * Sauvegarde une caractériqtiques et ses valeurs
     */
    public static function SaveCaracteristique($core, $data) {
        
        $caracteristiqueForm = new MarketCaracteristiqueForm($core, $data);

        if ($caracteristiqueForm->Validate($data)) {

            $caracteristique = new MarketCaracteristique($core);

            if ($data["Id"] != "") {
                $caracteristique->GetById($data["Id"]);
            } else {
                $market = MarketHelper::GetMarket($core);
                $caracteristique->MarketId->Value = $market[0]->IdEntite;
            }

            $caracteristiqueForm->Populate($caracteristique);
            $caracteristique->Save();

            if ($data["Id"] == "") {
                 $data["Id"] = $core->Db->GetInsertedId();
            }
            
            //Sauvegarde des items
            foreach (json_decode($data["items"]) as $caracteristiqueItem) {

                $item = new MarketCaracteristiqueItem($core);
                if ($caracteristiqueItem->id != 0) {
                    $item->GetById($caracteristiqueItem->id);
                }
                $item->CaracteristiqueId->Value = $data["Id"];
                $item->Label->Value = $caracteristiqueItem->label;
                $item->Save();
            }

            return true;
        }

        return false;
    }

    /*     * *
     * Remove a caractristic item
     */

    public static function RemoveCaracteristiqueItem($core, $itemId) {

        $item = new MarketCaracteristiqueItem($core);
        $item->GetById($itemId);
        $item->Delete();
    }

    /*     * *
     * Get the items of a caracteristique
     */

    public static function GetItemByCaracteristique($core, $caracteristiqueId) {
        $item = new MarketCaracteristiqueItem($core);
        $items = $item->Find("CaracteristiqueId=" . $caracteristiqueId);

        return $item->ToAllArray($items);
    }

    /*     * *
     * Delete a caracteristiques 
     */

    public static function DeleteCaracteristique($core, $caracteristiqueId) {

        $items = new MarketCaracteristiqueItem($core);
        $items->AddArgument(new Argument("Apps\Shop\Entity\ShopCaracteristiqueItem", "CaracteristiqueId", EQUAL, $caracteristiqueId));
        $items->DeleteByArg();

        $caracteristique = new MarketCaracteristique($core);

        $caracteristique->GetById($caracteristiqueId);
        $caracteristique->Delete();
        
        return true;
    } 
   
     /**
    * Produit
    */
   public function GetTabProduct(){
  
      $gdProduct = new EntityGrid("gdProduct", $this->Core);
      $gdProduct->Entity = "Apps\Market\Entity\MarketProduct";
      $gdProduct->App = "Market";
      $gdProduct->Action = "GetTabProduct";

      $gdProduct->AddColumn(new EntityColumn("Title", "Title"));
      $gdProduct->AddColumn(new EntityColumn("Price", "Price"));

      //Plugin Column
      $gdProduct->AddPluginColumn("Market", "Comment", "Comments", "GetComments");
      $gdProduct->AddPluginColumn("Market", "Vote", "Vote", "GetVotes");
      $gdProduct->AddPluginColumn("Market", "Rate", "Notes", "GetNotes");
     
      $gdProduct->AddColumn(new EntityIconColumn("Action", 
                                                array(array("EditIcone", "Market.EditProduct", "Market.EditProduct"),
                                                )    
                        ));
      
      
      return $gdProduct->Render();
   }
   
          /*action*/
 }?>
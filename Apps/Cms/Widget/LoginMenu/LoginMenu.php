<?php

namespace Apps\Cms\Widget\LoginMenu;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;

class LoginMenu extends Form {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "") {
        $this->Core = $core;
        $this->Data = $data;

        //On le fait ici pour avoir le formuaire à l'affichage mais aussi à la validation
        $this->Init($data);
    }

    /**
     * Initialisation 
     */
    function Init($data) {
    }

    /*     * *
     * Genere les formulaire
     */

    function Render() {
        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);
        $view->AddElement(new ElementView("connected", $this->Core->isConnected()));
        
        return $view->Render();
    }
}

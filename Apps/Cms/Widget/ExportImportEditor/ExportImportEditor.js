var ExportImportEditor = function () {};

ExportImportEditor.Init = function(){
    Event.AddById("btnExportPage", "click", ExportImportEditor.ExportPages);
    Event.AddById("btnImportPage", "click", ExportImportEditor.ShowImportPage);
};

/***
 * Export all Page
 */
ExportImportEditor.ExportPages = function(){
   window.open("Api/Cms/ExportPages", "_blank");
};  

/***
 * Import Pages and container
 */
ExportImportEditor.ShowImportPage = function(){

    Dialog.open('', {"title": Dashboard.GetCode("Cms.ImportPages"),
    "app": "Cms",
    "class": "DialogCms",
    "method": "ShowImportPage",
    "type": "left",
    });
};
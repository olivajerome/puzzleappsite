<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Mooc;

use Core\Core\Core;
use Core\App\Application;
use Core\Control\Image\Image;
use Core\Core\Request;
use Core\Core\Response;
use Core\Dashboard\DashBoard;
use Core\Utility\File\File;
use Apps\Mooc\Helper\CategoryHelper;
use Apps\Mooc\Helper\MoocHelper;
use Apps\Mooc\Helper\SitemapHelper;
use Apps\Mooc\Module\Admin\AdminController;
use Apps\Mooc\Module\Mooc\MoocController;
use Apps\Mooc\Module\Search\SearchController;
use Apps\Mooc\Module\Front\FrontController;
use Apps\Mooc\Module\Member\MemberController;
use Apps\Mooc\Module\Api\ApiController;
use Apps\Mooc\Entity\MoocMooc;
use Apps\Mooc\Entity\MoocLesson;

/**
 * Application de gestion des Cours 
 * */
class Mooc extends Application {

    /**
     * Auteur et version
     * */
    public $Author = 'Eemmys';
    public $Version = '2.1.1.0';
    public static $Directory = "../Apps/Mooc";

    /**
     * Constructeur
     * */
    function __construct($core = "") {
        parent::__construct($core, "Mooc");
        $this->Core = Core::getInstance();
    }

    /**
     * Execution de l'application
     */
    function Run() {
        echo parent::RunApp($this->Core, "Mooc", "Mooc");
    }

    /*     * *
     * Run member Controller
     */

    function RunMember() {
        $memberController = new MemberController($this->Core);
        return $memberController->Index();
    }

    /**
     * Set the Public Routes
     */
    public function GetRoute() {
        $this->Route->SetPublic(array("Mooc", "Lesson"));

        return $this->Route;
    }

    /*     * *
     * Execute action after install
     */

    function PostInstall() {
        \Apps\EeApp\Helper\AppHelper::UpdateAppParams($this->Core,
                "Mooc",
                $this->Version,
                "Gestionnaire des Tutoriel",
                1,
                1
        );

        \Apps\EeApp\Helper\AppHelper::CreateDataDir("Mooc");
    }

    /*
     * Show Home Page
     */

    public function Index() {
        $this->Core->MasterView->Set("Title", "Tutoriel");

        $frontConroller = new FrontController($this->Core);
        return $frontConroller->Index();
    }

    /*
     * Show one Mooc
     */

    public function Mooc($params) {
        $Mooc = new MoocMooc($this->Core);
        $Mooc = $Mooc->GetByCode($params);

        $this->Core->MasterView->Set("Title", $Mooc->Name->Value);
        $this->Core->MasterView->Set("Description", $Mooc->Description->Value);

        $frontConroller = new FrontController($this->Core);
        return $frontConroller->Mooc($params);
    }

    /**
     * Load one leson
     * @param type $params
     */
    public function Lesson($params) {
        $MoocLesson = new MoocLesson($this->Core);
        $MoocLesson = $MoocLesson->GetByCode($params);

        $this->Core->MasterView->Set("Title", $MoocLesson->Name->Value);
        $this->Core->MasterView->Set("Description", $MoocLesson->Description->Value);

   
        $frontConroller = new FrontController($this->Core);
        return $frontConroller->Lesson($params);
    }

    /*
     * Charge la partie administration
     */

    public function LoadAdmin() {
        $adminController = new AdminController($this->Core);
        echo $adminController->Show();
    }

    /**
     * Sauvegarde une catégorie
     */
    function SaveCategory() {
        $adminController = new AdminController($this->Core);

        if ($adminController->SaveCategory($this->Core, Request::GetPosts())) {

            return Response::Success($adminController->GetTabCategory());
        } else {
            return Response::Error();
        }
    }

    /**
     * Delete a catégorie
     * @return type
     */
    function DeleteCategory() {

        $adminController = new AdminController($this->Core);

        if ($adminController->DeleteCategory($this->Core, Request::GetPost("categoryId"))) {

            $adminController = new AdminController($this->Core);
            return Response::Success($adminController->GetTabCategory());
        } else {
            return Response::Error();
        }
    }

    /**
     * Rafraichit les catégories
     */
    function RefreshCategory() {
        $categoryController = new AdminController($this->Core);
        echo $categoryController->GetTabCategory()->Show();
    }

    /*
     * Sauvegarde un mooc
     */

    public function SaveMooc() {
        $adminController = new AdminController($this->Core);

        if ($adminController->SaveMooc($this->Core, Request::GetPosts())) {

            return Response::Success($adminController->GetTabMooc());
        } else {
            return Response::Error();
        }
    }

    /*     * *
     * Delete a Mooc
     */

    function DeleteMooc() {
        $adminController = new AdminController($this->Core);

        if ($adminController->DeleteMooc($this->Core, Request::GetPost("moocId"))) {
            return Response::Success($adminController->GetTabMooc());
        } else {
            return Response::Error();
        }
    }

    /*
     * Edite les lessons d'un Mooc
     */

    function EditMooc() {
        $moocController = new MoocController($this->Core);
        echo $moocController->EditMooc(Request::GetPost("moocId"));
    }

    /*
     * Sauvegarde une lesson
     */

    public function SaveLesson() {

        $adminController = new AdminController($this->Core);

        if ($adminController->SaveLesson($this->Core, Request::GetPosts())) {

            $Mooc = new MoocMooc($this->Core);
            $Mooc->GetById(Request::GetPost("MoocId"));
            $lessons = $Mooc->GetLessons(true);

            return Response::Success($lessons);
        } else {
            return Response::Error();
        }
    }

    /*     * *
     * Remove lesson
     */

    public function RemoveLesson() {
        $adminController = new AdminController($this->Core);

        if ($adminController->RemoveLesson($this->Core, Request::GetPost("lessonId"))) {
            return Response::Success(array("Message" => $this->Core->GetCode("Mooc.LessonRemoved")));
        } else {
            return Response::Error();
        }
    }

    /*
     * Rafraichit les elements
     */

    function RefreshElement() {
        $moocController = new MoocController($this->Core);
        echo $moocController->GetElements(Request::GetPost("lessonId"), false);
    }

    /*
     * Charge l'écran de recherche
     */

    public function LoadSearch() {
        $searchController = new SearchController($this->Core);
        echo $searchController->Show();
    }

    /*
     * Rechercher les mooc 
     */

    public function Search() {
        $searchController = new SearchController($this->Core);
        echo $searchController->Search(Request::GetPost("categoryId"));
    }

    /*
     * Lance un Mooc
     */

    public function StartMooc() {
        //Memorise le Mooc pour l'utilisateur
        MoocHelper::Memorise($this->Core, $this->Core->User->IdEntite, Request::GetPost("moocId"));

        $moocController = new MoocController($this->Core);
        echo $moocController->StartMooc(Request::GetPost("moocId"));
    }

    /*
     * Charge une lecon
     */

    public function LoadLesson() {
        $moocController = new MoocController($this->Core);
        echo $moocController->LoadLesson(Request::GetPost("lessonId"));
    }

    /*     * *
     * Edit a lesson
     */

    public function EditLesson() {
        $adminController = new AdminController($this->Core);
        return $adminController->EditLesson(Request::GetPost("lessonId"), Request::GetPost("moocId"));
    }

    /**
     * Charge un quiz
     */
    function LoadQuiz() {
        $eform = DashBoard::GetApp("Form", $this->Core);
        $eform->IdEntity = Request::GetPost("quizId");

        echo $eform->Display();
    }

    /*
     * Charge les Mooc de l'utilisateur
     */

    function LoadMyLesson() {
        $searchController = new SearchController($this->Core);
        echo $searchController->LoadMyLesson();
    }

    function Display() {
        $html = "<div id='appRunMooc'>";
        $html .= "<div id='dvLesson'>";

        $moockController = new MoocController($this->Core);
        $html .= $moockController->StartMooc(Request::Get("Id"), true);

        $html .= "</div></div>";

        return $html;
    }

    /*     * *
     * UnFollow a Mooc 
     */

    function UnfollowMooc() {
        MoocHelper::RemoveUser($this->Core, Request::GetPost("userMoocId"));
    }

    /**
     * Obtient les images du blogs
     * format niormal et mini
     */
    function GetImages() {
        echo MoocHelper::GetImages($this->Core, Request::GetPost("moocId"));
    }

    /**
     * Sauvegare les images de presentation
     */
    function DoUploadFile($idElement, $tmpFileName, $fileName, $action) {
        $directory = "Data/Tmp";

        move_uploaded_file($tmpFileName, $directory . "/" . $idElement . ".jpg");

        echo Response::Success(array("type:" => "IMAGE", "Message" => "OK"));
    }

    /*     * *
     * Obtient les mooc Actif
     */

    public function GetActive() {
        return Response::Success(MoocHelper::GetAll($this->Core, true));
    }

    /*     * *
     * Obtient un mooc avec lses lessons
     */

    public function GetMooc() {
        return Response::Success(MoocHelper::GetDetail($this->Core, Request::GetPost("moocId")));
    }

    /**
     * Get The siteMap 
     */
    public function GetSiteMap($url = false) {
        return SitemapHelper::GetSiteMap($this->Core, $url);
    }

    /**
     * Obtient le widget spécifique
     */
    public function GetWidget($type = "", $params = "") {

        switch ($type) {
            case "ListMooc" :
                $widget = new \Apps\Mooc\Widget\ListMooc\ListMooc($this->Core);
                break;
            case "AdminDashboard" :
                $widget = new \Apps\Mooc\Widget\AdminDashBoard\AdminDashBoard($this->Core);
                break;
            case "LastMooc" :
                $widget = new \Apps\Mooc\Widget\LastMooc\LastMooc($this->Core);
                break;
        }

        return $widget->Render($params);
    }

    /*     * **
     * List of AddAblbe Widget on CMS
     */

    public function GetListWidget() {
        return array(array("Name" => "LastMooc",
                "Description" => $this->Core->GetCode("Mooc.DescriptionLastMooc")),
        );
    }

    /***
     * Get List Of Mooc
     */
    public function GetListMooc() {

        $apiController = new ApiController($this->Core);
        return Response::Success($apiController->GetListMooc());
    }
}

?>
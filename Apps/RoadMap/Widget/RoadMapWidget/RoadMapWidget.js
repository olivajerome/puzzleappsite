var RoadMapWidget = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
RoadMapWidget.Init = function(){
    Event.AddById("btnSave", "click", ItineraireForm.SaveItineraire);
};

/***
* Charge un tableau avec les colonnes et les tickets
*/
RoadMapWidget.Load = function(RoadMapId){
    
        let currentDashBoard = document.getElementById("currentDashBoard");
          
        let data = "Class=RoadMap&Methode=Load&App=RoadMap";
            data += "&roadMapId=" +RoadMapId;

        Request.Post("Ajax.php", data).then(data => {
                 currentDashBoard.innerHTML = data;
                 
                 RoadMapWidget.AddEventRoadMap();
        });
};

/***
 * Ajout les event sur la roadMap charg�
 **/
RoadMapWidget.AddEventRoadMap = function(){

    Event.AddById("btnNewColumn", "click", RoadMapWidget.ShowAddColumn);
    Event.AddByClass("bntEditColumn", "click", RoadMapWidget.EditColumn);
    Event.AddByClass("bntDeleteColumn", "click", RoadMapWidget.DeleteColumn);

    Event.AddById("btnParameters", "click", RoadMapWidget.OpenParameterMenu);
    
    Event.AddByClass("btnNewTicket", "click", RoadMapWidget.ShowAddTicket);
    Event.AddByClass("editTicket", "click", RoadMapWidget.EditTicket);
    Event.AddByClass("deleteTicket", "click", RoadMapWidget.DeleteTicket);
    
    Animation.AddDraggable("ticket", function(){
    },  
    function(x, y, element){
    
        var columns = document.getElementsByClassName("column");
        
        for(var i = 0; i < columns.length; i++){
            
            let coordonneeColumn = columns[i].getClientRects();
            
            if(x > coordonneeColumn[0].x && x < ( coordonneeColumn[0].x  + 250)  ){
                columns[i].append(element);
                
                RoadMapWidget.ChangeColumn(element.id, columns[i].id);
            }
         }
    }
    );
};

/*
* Obtient l'id de la roadMap en cours 
*/
RoadMapWidget.GetCurrentId = function(){
 let hdRoadMapId = document.getElementById("hdRoadMapId");
 return hdRoadMapId.value;
};

/*
* Menu contextuel des paramétres
*/
RoadMapWidget.OpenParameterMenu = function(e){

 let hdRoadMapId = document.getElementById("hdRoadMapId");
 
    menuDiscuss = new ContextMenu(e);
    menuDiscuss.App = "RoadMap";
    menuDiscuss.Methode = "GetParameterMenu";
    menuDiscuss.Params = "&roadMapId=" + hdRoadMapId.value;
     
    menuDiscuss.onLoaded = function () {

        Event.AddByClass("btnEditRoadMap", "click", RoadMapWidget.EditRoadMap);
        Event.AddByClass("btnDeleteRoadMap", "click", RoadMapWidget.DeleteRoadMap);
    };

    menuDiscuss.Show();
    
};

/*
* Edite une roadMap
*/
RoadMapWidget.EditRoadMap = function(){

 ContextMenu.Close();

 Dialog.open('', {"title": Dashboard.GetCode("RoadMap.AddRoadMap"),
          "app": "RoadMap",
          "class": "DialogRoadMap",
          "method": "ShowAddRoadMap",
          "type": "left",
          "params" : RoadMapWidget.GetCurrentId()
          });
};

/*
** Supprime une RoadMap
*/
RoadMapWidget.DeleteRoadMap = function(){
    
    ContextMenu.Close();
 
    Animation.Confirm(Language.GetCode("RoadMap.ConfirmDeleteRoadMap"), function(){
        
       let data = "Class=RoadMap&Methode=DeleteRoadMap&App=RoadMap";
            data += "&roadMapId=" +RoadMapWidget.GetCurrentId();
            Request.Post("Ajax.php", data).then(data => {
            
              RoadMap.LoadRoadMaps();
              RoadMapWidget.Clear();
        });
        
    });
};

/*
* Vide la roadMap
*/
RoadMapWidget.Clear = function(){
    let columns = document.querySelector(".columns");
    columns.innerHTML = "";
};

/*
* Sauvegarde le changement de colonne
*/
RoadMapWidget.ChangeColumn = function(ticketId, columnId){

 let data = "Class=RoadMap&Methode=ChangeColumn&App=RoadMap";
            data += "&ticketId=" +ticketId;
            data += "&columnId=" +columnId;
       
            Request.Post("Ajax.php", data).then(data => {
        });
};



/***
 * Dialogue d'ajout de colonne
 **/
RoadMapWidget.ShowAddColumn = function(){

  let hdRoadMapId = document.getElementById("hdRoadMapId");

  Dialog.open('', {"title": Dashboard.GetCode("RoadMap.AddColumn"),
          "app": "RoadMap",
          "class": "DialogRoadMap",
          "method": "ShowAddColumn",
          "params": "right&RoadMapId=" + hdRoadMapId.value ,
          "type"  : "left"
          });
          
} ;

/*
* Edit une colonne
*/
RoadMapWidget.EditColumn = function(e){

   let columnId = e.srcElement.parentNode.parentNode.id;
   
   Dialog.open('', {"title": Dashboard.GetCode("RoadMap.AddColumn"),
          "app": "RoadMap",
          "class": "DialogRoadMap",
          "method": "ShowAddColumn",
          "params": "columnId=" + columnId,
          "type"  : "left"
          });
          
};

/*
* Supprime une colonne
*/
RoadMapWidget.DeleteColumn = function(e){
 
let column = e.srcElement.parentNode.parentNode;
  
    Animation.Confirm(Language.GetCode("RoadMap.ConfirmDeleteColumn"),function(){
    
    
    let data = "Class=RoadMap&Methode=DeleteColumn&App=RoadMap";
            data += "&columnId=" +column.id;
       
            Request.Post("Ajax.php", data).then(data => {
                column.parentNode.removeChild(column);
            });
    });
};

/***
 * Dialogue d'ajout de ticket
 **/
RoadMapWidget.ShowAddTicket = function(e){

   let columnId = e.srcElement.parentNode.parentNode.id;

  Dialog.open('', {"title": Dashboard.GetCode("RoadMap.AddTicket"),
          "app": "RoadMap",
          "class": "DialogRoadMap",
          "method": "ShowAddTicket",
          "params": "new&ColumnId=" + columnId ,
          "type" : "right"
          });
};

/*
** Ouvre un ticket
*/
RoadMapWidget.EditTicket = function(e){

e.preventDefault();
        e.stopPropagation();
        
let element = e.srcElement;

while(element.className.indexOf("ticket") < 0){
    element = element.parentNode;
}

 Dialog.open('', {"title": Dashboard.GetCode("RoadMap.EditTicket"),
          "app": "RoadMap",
          "class": "DialogRoadMap",
          "method": "ShowAddTicket",
          "params": element.id,
          "type" : "right"
          });
};

/***
* Supprime un ticket
*/
RoadMapWidget.DeleteTicket = function(e){

    let element = e.srcElement;

    while(element.className.indexOf("ticket") < 0){
        element = element.parentNode;
    }
   
    Animation.Confirm(Language.GetCode("RoadMap.ConfirmDeleteTicket"), function(){
    
    let data = "Class=RoadMap&Methode=DeleteTicket&App=RoadMap";
            data += "&ticketId=" +element.id;
       
            Request.Post("Ajax.php", data).then(data => {
            
            element.parentNode.removeChild(element);
            
        });
    
    });
};

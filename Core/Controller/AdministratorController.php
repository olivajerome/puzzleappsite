<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Core\Controller;

use Core\Controller\Controller;

/**
 * Description of Controller
 *
 * @author oliva
 */
class AdministratorController extends Controller {

    function __construct($core = "") {

        // Extend to admin controller
        $this->Core = $core;
    
        if($this->Core->User->GroupeId->Value != 1){
            throw new \Exception("Not Allowed");
        }
    }
}
<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Forms\Module\Front;

use Core\Controller\Controller;
use Core\Core\Request;
use Core\View\ElementView;
use Core\View\View;

use Apps\Forms\Entity\FormForm;
use Apps\Forms\Helper\FormHelper;
use Apps\Forms\Helper\QuestionHelper;


class FrontController extends Controller
{
    
    function __construct($core = "")
    {
        parent::__construct($core);
    }
    
    /*
     * Show All Mooc
     */
    function Index($params)
    {
        if(Request::GetPost() != null)
        {   
            FormHelper::SaveReponseUser($this->Core, Request::GetPost());
            
            $view = new View(__DIR__."/View/valid.tpl", $this->Core);
        }
        else
        {
            $view = new View(__DIR__."/View/index.tpl", $this->Core);

            //Get the Form
            $form = new FormForm($this->Core);
            
            if(is_numeric($params)){
               $form->GetById($params);
            } else{
                $form = $form->GetByCode($params);
            }
            
            
            $view->AddElement(new ElementView("Form", $form));

            //Get the question
            $view->AddElement(new ElementView("Questions", QuestionHelper::GetByForm($this->Core, $form)));
        }
        
        return $view->Render();

    }
}

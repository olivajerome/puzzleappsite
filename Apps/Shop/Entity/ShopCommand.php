<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Shop\Entity;

use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class ShopCommand extends Entity {

    //Constructeur
    function __construct($core) {
        //Version
        $this->Version = "2.0.0.0";

        //Nom de la table 
        $this->Core = $core;
        $this->TableName = "ShopCommand";
        $this->Alias = "ShopCommand";

        // Identifiant unique du panier en session
        $this->CartId = new Property("CartId", "CartId", TEXTBOX, false, $this->Alias);

        $this->UserId = new Property("UserId", "UserId", NUMERICBOX, false, $this->Alias);
        $this->Status = new Property("Status", "Status", NUMERICBOX, false, $this->Alias);
        $this->Name = new Property("Name", "Name", TEXTBOX, false, $this->Alias);
        $this->Total = new Property("Total", "Total", TEXTBOX, false, $this->Alias);
        $this->DateCreated = new Property("DateCreated", "DateCreated", DATEBOX, false, $this->Alias);
        $this->DateClotured = new Property("DateClotured", "DateClotured", DATEBOX, false, $this->Alias);
        $this->Adress = new Property("Adress", "Adress", TEXTBOX, false, $this->Alias);
        $this->AdressFacturation = new Property("AdressFacturation", "AdressFacturation", TEXTBOX, false, $this->Alias);

        //Creation de l entité 
        $this->Create();
    }

    /*     * *
     * Affiche le status de la commande
     */

    function GetStatut() {

        switch ($this->Status->Value) {
            case 1:
                return $this->Core->GetCode("Shop.StateEncours");
                break;
            case 2:
                return $this->Core->GetCode("Shop.StateValidated");
                break;
        }
    }

    /*     * *
     * Calcul le total de la commande
     */

    function Total() {

        $request = "Select Sum(Price) as total From CommandLine Where CommandId = " . $this->IdEntite;
        $result = $this->Core->Db->GetLine($request);

        return round($result["total"], 2);
    }

    /**
     * Obtient le lien de génération du Pdf
     */
    function GetLinkPdf() {

         if (strstr($this->Name->Value, "Forfait")) {
            return "<a title='".$this->Core->GetCode("Shop.DownloadFacture")."' target='blank' href='" . $this->Core->GetPath("/Trip/GenerateFacture/" . base64_encode($this->IdEntite)) . "'  ><i class='fa fa-file'></i></a>";
 
         } else {
            $html = "<a title='".$this->Core->GetCode("Shop.DownloadCarnet")."'   target='blank' href='" . $this->Core->GetPath("/Trip/Generate/" . base64_encode($this->IdEntite)) . "'  ><i class='fa fa-book downloadCarnet'></i></a>";
            $html .= "<a title='".$this->Core->GetCode("Shop.DownloadGpx")."'   target='blank' href='" . $this->Core->GetPath("/Trip/GenerateGpx/" . base64_encode($this->IdEntite)) . "'  ><i class='fa fa-road'></i></a>";
            $html .= "<a title='".$this->Core->GetCode("Shop.ShowCarnet")."'   target='blank' href='" . $this->Core->GetPath("/Trip/Cart/".$this->CartId->Value)."'><i class='fa fa-eye'></i></a>";
         
            return $html;
         }
    }
    
    /*     * *
     * Type de comande
     */

    function GetType() {

        if (strstr($this->Name->Value, "Forfait")) {
            return $this->Name->Value;
        } else {
            return $this->Core->GetCode("Shop.CommandeTypeVoyage");
        }
    }

    /***
     * Unite Euro ou KM
     */
    function GetUnite(){
         if (strstr($this->Name->Value, "Forfait")) {
            return "&euro;";
        } else {
            return "Km";
        }
    }
    
    /***
     * Unite Euro ou KM
     */
    function GetUniteLibelle(){
         if (strstr($this->Name->Value, "Forfait")) {
            return $this->Core->GetCode("Shop.Price");
        } else {
            return $this->Core->GetCode("Cdv.Distance");
        }
    }
    
    /*     * *
     * Total
     */

    function GetTotal() {
        
        $request = "Select SUM(Price) as total FROM CommandLine Where CommandId=" .$this->IdEntite;
        $result = $this->Core->Db->GetLine($request);
        
        $total = $result["total"];
        
        if (strstr($this->Name->Value, "Forfait")) {
            return round($total,2) . "&euro;";
        } else {
            return  round($total,2)  . "Km";
        }
    }

}

?>
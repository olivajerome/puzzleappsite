<?php

/**
 * PuzzleApp 
 * PuzzleApp.org - The Hybride Framework.
 * Oliva Jérôme
 * GNU Licence
 * */
//DTD Type
define("STRICT", "strict");
define("TRANSI", " transitional");
define("FRAME", "frameset");

//Button Type
define("SUBMIT", "submit");
define("BUTTON", "button");

//Request Type
define("GET", "get");
define("POST", "post");

//Control Type
define("TEXTBOX", "Core\Control\TextBox\TextBox");
define("PASSWORD", "Core\Control\PassWord\PassWord");
define("NUMERICBOX", "Core\Control\NumericBox\NumericBox");
define("TEXTAREA", "Core\Control\TextArea\TextArea");
define("HIDDEN", "Core\Control\Hidden\Hidden");
define("EMAILBOX", "Core\Control\EmailBox\EmailBox");
define("TELBOX", "Core\Control\TelBox\TelBox");
define("CHECKBOX", "Core\Control\CheckBox\CheckBox");
define("DATEBOX", "Core\Control\DateBox\DateBox");
define("DATETIMEBOX", "Core\Control\DateTimeBox\DateTimeBox");
define("IMAGE", "Core\Control\Image\Image");
define("UPLOAD", "Core\Control\Upload\Upload");

//SQL Operator
define("EQUAL", "Equal");
define("NOTEQUAL", "NotEqual");
define("IN", "In");
define("NOTIN", "NotIn");
define("BETWEEN", "Between");
define("LESS", "Less");
define("MORE", "More");
define("LIKE", "Like");
define("ALL", "All");
define("ISNULL", "IsNull");
define("ISNOTNULL", "IsNotNull");

//SQL Join Type
define("LEFTJOIN", "left");
define("INNERJOIN", "inner");
define("RIGHTJOIN", "right");

//Event Type
define("OPENWIN", "OpenWin");
define("LOADWIN", "LoadWin");
define("CLOSEWIN", "CloseWin");
define("CLOSELOAD", "CloseLoad");

//Entite Type
define("BLOC", "Bloc");
define("PAGE", "Page");
define("CONTROL", "Control");

//Log Type
define("CORE", "Core");
define("DB", "Db");
define("EN", "En");

//Log level
define("All", "");
define("ERR", "ERR");
define("INFO", "INFO");
define("LogLEVEL", "ERR");
define("ROOT", 1);

//Log type
define("CORE", "Core");
define("DB", "Db");
define("EN", "En");
define("RUNNER", "Runner");
define("AJAX", "Ajax");

//Type de base de donnee
define("XML", "xml");

//Erreur code
define("ACTION", "Action");
define("RESULT", "Result");
define("OK", "000");
define("KO", "999");
define("ERROR", "Err");
define("ERR_PASSWORD", "Password invalide");
define("ERR_USER_NOT_EXIST", "User existe pas");
define("ERR_USER_EXIST", "User existe");
define("ERR_ADD_USER", "Erreur ajout user");
define("ERR_USER_NOT_CONNECT", "User non connecte");
define("ERR_FILE_NOT_DEFINED", "File non definie");
define("ERR_USER_FILE_NOT_FOUND", "File or user non trouve");
define("ERR_ADD_COMMENT", "Erreur ajout commentaire");
define("ERR_USER_NOT_IMAGE", "User n'a pas d'image");

//Erreur File Code
define("ERR_NO_FILE", "Fichier de taille nulle"); // Fichier de taille nulle
define("ERR_INI_SIZE", "Fichier trop lourd pour le serveur"); // Fichier trop lourd limite par le serveur
define("ERR_FORM_SIZE", "Fichier trop lourd pour le formulaire"); //Fichier trop lourd limit par le formulaire
define("ERR_PARTIAL", "Problème de transfert"); // Problème de transfert

//@deprecated
define("ALIGNRIGHT","style='text-align:right;'");
define("ALIGNLEFT","style='text-align:left;'");
define("ALIGNCENTER","style='text-align:center;'");
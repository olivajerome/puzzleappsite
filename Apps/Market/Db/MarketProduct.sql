CREATE TABLE IF NOT EXISTS `MarketProduct` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`Title` VARCHAR(200)  NOT NULL,
`Code` VARCHAR(200)  NOT NULL,
`Description` TEXT  NOT NULL,
`Status` INT  NOT NULL,
`Price` TEXT  NOT NULL,
`MarketId` INT  NOT NULL,
`UserId` INT  NOT NULL,
PRIMARY KEY (`Id`),
CONSTRAINT `MarketMarket_MarketProduct` FOREIGN KEY (`MarketId`) REFERENCES `MarketMarket`(`Id`),
CONSTRAINT `ee_user_MarketProduct` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
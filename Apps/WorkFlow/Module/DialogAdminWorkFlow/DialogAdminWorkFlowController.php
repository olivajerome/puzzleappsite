<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\WorkFlow\Module\DialogAdminWorkFlow;

use Core\Control\Button\Button;
use Core\Controller\Controller;
use Core\View\View;

use Apps\WorkFlow\Widget\WorkFlowWorkFlowForm\WorkFlowWorkFlowForm;
use Apps\WorkFlow\Entity\WorkFlowWorkFlow;

use Apps\WorkFlow\Widget\WorkFlowTriggerActionForm\WorkFlowTriggerActionForm;
use Apps\WorkFlow\Entity\WorkFlowTrigger;

use Apps\WorkFlow\Widget\WorkFlowActionForm\WorkFlowActionForm;
use Apps\WorkFlow\Entity\WorkFlowAction;

/*
 * 
 */
 class DialogAdminWorkFlowController extends Controller
 {
    /**
     * Constructeur
     */
    function _construct($core="")
    {
          $this->Core = $core;
    }

    /**
     * Create
     */
    function Create()
    {
    }

    /**
     * Init
     */
    function Init()
    {
    }

    /**
     * Render
     */
    function Show($all=true)
    {
         return $this->Index();
    }
    
    /*
    * Get the home page
    */
   function Index()
   {
       $view = new View(__DIR__."/View/index.tpl", $this->Core);
       return $view->Render();
   }

   /***
    * Add/Edit a workFlow
    */
   function AddWorkFlow($params ){

        $workFlowForm = new WorkFlowWorkFlowForm($this->Core);

        if($params != ""){
          $workFlow = new WorkFlowWorkFlow($this->Core);
          $workFlow->GetById($params);
          $workFlowForm->Load($workFlow);
        }

        return $workFlowForm->Render();
   }

   /***
    * Add Trigger and Task to a workflow
    */
   function AddTaskWorkFlow($workFlowId){

    $data = array();
    $data["WorkFlowId"] = $workFlowId;

    $workTriggerActionForm = new WorkFlowTriggerActionForm($this->Core, $data);

    $trigger = new WorkFlowTrigger($this->Core);
    $trigger->WorkFlowId->Value = $workFlowId;

    $triggers = $trigger->Find("WorkFlowId=".$workFlowId);

    if(count($triggers) > 0){
        $trigger = $triggers[0];
        $data["Event"] =  $trigger->Event->Value;
    }  

    $workTriggerActionForm->Load($data);

     return $workTriggerActionForm->Render();
   }

   /**
    * Add Action to A WorkFlow
    */
   function AddActionWorkFlow($workFlowId){

    $workActionForm = new WorkFlowActionForm($this->Core, $data);

    $action = new WorkFlowAction($this->Core);
    $action->WorkFlowId->Value = $workFlowId;

    $workActionForm->Load($action);

     return $workActionForm->Render();
   }
          
          /*action*/
 }?>
<?php 
/*
* PuzzleApp
* Webemyos
* Jérôme Oliva
* GNU Licence
*/

namespace Apps\NewsLetter\Entity;
use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class NewsLetterConfig extends Entity  
{
	//Constructeur
	function __construct($core)
	{
		//Version
		$this->Version ="2.0.0.0"; 

		//Nom de la table 
		$this->Core=$core; 
		$this->TableName="NewsLetterConfig"; 
		$this->Alias = "NewsLetterConfig"; 

		$this->EmailFrom = new Property("EmailFrom", "EmailFrom", TEXTBOX,  false, $this->Alias); 
		$this->EmailReplyTo = new Property("EmailReplyTo", "EmailReplyTo", TEXTBOX,  false, $this->Alias); 

		//Creation de l entité 
		$this->Create(); 
	}
}
?>
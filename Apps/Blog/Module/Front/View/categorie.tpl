<h1>{{GetCode(Blog.ArticlesOfCategory)}} :
    {{Category->Name->Value}} </h1>

<h2>{{Category->Description->Value}}</h2>

{{foreach Article}}
<div class='col-md-3'>

    <div class='block' style='height:350px'>
        <span style='display:block; width:100%; height: 100px; background-size: cover;background-image: url({{element->GetImagePath()}})'>
        </span>

        <h3 style="min-height:60px"> {{element->Name->Value}}
            <p style='font-size:8px'>
                {{element->DateCreated->Value}}
            </p>
        </h3>


        <div style='height:130px;margin'>{{element->Description->Value}}</div>

        <a href ='{{GetPath(/Blog/Article/{{element->Code->Value}})}}' >

            {{GetCode(Blog.ReadThisArticle)}}
        </a>
    </div>
</div>

{{/foreach Article}}
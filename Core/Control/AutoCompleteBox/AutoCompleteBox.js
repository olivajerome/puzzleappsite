var AutoCompleteBox = new AutoCompleteBox();

function AutoCompleteBox()
{
    this.Search = function(control, classe, methode, startAt, sourceControl, parameter)
    {
        var position = control.getBoundingClientRect();
        
        var span = document.getElementById('sp'+control.id);

        if(control.value.length >= startAt)
        {
        //determine les positions du control par rapport a ces parent
        var obj = span;
       
        //Definir la position en dessous de la textBox
        var divResult = document.getElementById("divResult");

        if(divResult == null)
        {
                //Creation de la div de resultat.
            var divResult = document.createElement('div');
            divResult.id = "divResult";
            divResult.style.left = position.x  + "px";

            divResult.style.top = position.y + 40 + "px";
            divResult.style.width = "300px";

            divResult.innerHTML = "<center><br /><br /><img src='../Images/loading/load.gif' alt='Wait' title='wait' /></center>";

            document.body.appendChild(divResult);
        }

        //Appele de la fonction ajax D'autocompletion
         var JAjax = new ajax();
         JAjax.data = "Class="+classe+"&Methode="+methode;
         JAjax.data +="&sourceControl="+control.value;
         JAjax.data +="&sourceControlId="+control.id;
         JAjax.data +="&"+parameter;

         //
         if(sourceControl != "")
         {
            var Control = document.getElementById(sourceControl);

            if(Control != null)
             JAjax.data +="&source="+Control.value;
            else
             JAjax.data +="&source="+sourceControl;
         }

         //JAjax.mode = "A";
         //JAjax.Control = divResult;

          result =   JAjax.GetRequest("Ajax.php");
          if(result != ""){
            divResult.innerHTML = result;
          } else{
              document.body.removeChild(divResult);
          }
        }
        else
        {
            //fermeture de la div
            var divResult = document.getElementById("divResult");
            if(divResult != null)
            {
                    document.body.removeChild(divResult);
            }
        }
    };

    this.SetResult = function(control, controlId)
    {
        //Recuperation de la textBox
        var ctr = document.getElementById(controlId);
        ctr.value = control.id;

        //Recuperation de la div
        var divResult = document.getElementById("divResult");

        if(divResult != null)
        {
                document.body.removeChild(divResult);
        }
        else
        {
          ClosePopUp();
        }
    };
};
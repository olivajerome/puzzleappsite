<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Blog\Module\DialogAdminBlog;

use Core\Control\Button\Button;
use Core\Controller\AdministratorController;
use Apps\Blog\Entity\BlogCategory;
use Apps\Blog\Entity\BlogArticle;
use Apps\Blog\Widget\BlogCategoryForm\BlogCategoryForm;
use Apps\Blog\Widget\BlogArticleForm\BlogArticleForm;
use Apps\Blog\Widget\BlogArticleEditor\BlogArticleEditor;
use Apps\Blog\Widget\ImportBlogForm\ImportBlogForm;

class DialogAdminBlogController extends AdministratorController{

    /***
     * Ajout d'une catégorie
     */
    function AddCategorie($categoryId){
      
        $category = new BlogCategory($this->Core);
        if($categoryId != ""){
            $category->GetById($categoryId);
        }
        
        $categoryForm = new BlogCategoryForm($this->Core);
        $categoryForm->Load($category);
        
        return $categoryForm->Render();
    }
    
    /**
     * Edit a article
     * @param type $articleId
     * @return type
     */
    function AddArticle($articleId) {

        $article = new BlogArticle($this->Core);
        if($articleId != "") {
            $article->GetById($articleId);
        }

        $articleForm = new BlogArticleForm($this->Core, array("Id" => $articleId));
        $articleForm->form->LoadImage($article);
        $articleForm->Load($article);

        return $articleForm->Render();
    }
    
    /***
     * Editeur de l'article
     */
    function WriteArticle($articleId){
        
        $article = new BlogArticle($this->Core);
         $article->GetById($articleId);
        $articleForm = new BlogArticleEditor($this->Core, array("Id" => $articleId));
        $articleForm->Load($article);

        return $articleForm->Render();
    }

    /***
     * Show Import Page
     */
    function ShowImport(){
        $importBlogForm = new ImportBlogForm($this->Core);
        return $importBlogForm->Render();
    }
}

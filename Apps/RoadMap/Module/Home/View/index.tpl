<section>
    <h1>{{GetCode(RoadMap.RoadMap)}}</h1>
    <div class='tools'>
        <a id='btnCreateRoadMap'><i class='fa fa-plus'></i>{{GetCode(RoadMap.AddRoadMap)}}</a>
    </div>
    <div id='roadMaps' class='dashboards'>
        {{foreach RoadMaps}}
            <button class='btn btn-secondary btnRoadMap' id={{element->IdEntite}}>
                   {{element->Name->Value}}
            </button>
        {{/foreach RoadMaps}}
    </div>
</section>

<div id='currentDashBoard'>
</div>
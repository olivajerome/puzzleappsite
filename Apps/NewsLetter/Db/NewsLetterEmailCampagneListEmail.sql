CREATE TABLE IF NOT EXISTS `NewsLetterEmailCampagneListEmail` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`CampagneId` INT  NOT NULL,
`ListEmailId` INT  NOT NULL,
PRIMARY KEY (`Id`),
CONSTRAINT `NewsLetterEmailCampagne_NewsLetterEmailCampagneListEmail` FOREIGN KEY (`CampagneId`) REFERENCES `NewsLetterEmailCampagne`(`Id`),
CONSTRAINT `NewsLetterListEmail_NewsLetterEmailCampagneListEmail` FOREIGN KEY (`ListEmailId`) REFERENCES `NewsLetterListEmail`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
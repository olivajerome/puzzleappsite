<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Forms\Module\Admin;

use Core\Controller\AdministratorController;
use Core\View\View;
use Core\View\ElementView;
use Core\Core\Request;
use Core\Control\TabStrip\TabStrip;
use Core\Control\EntityGrid\EntityGrid;
use Core\Control\EntityGrid\EntityColumn;
use Core\Control\EntityGrid\EntityIconColumn;
use Core\Control\EntityGrid\EntityFunctionColumn;
use Core\Control\EntityListBox\EntityListBox;
use Core\Entity\Entity\Argument;
use Core\Control\Button\Button;
use Apps\Forms\Entity\FormQuestion;
use Apps\Forms\Entity\FormResponse;
use Apps\Forms\Decorator\FormDecorator;
use Apps\Forms\Helper\FormHelper;


class AdminController extends AdministratorController {

    /**
     * Constructeur
     */
    function __construct($core = "") {
        $this->Core = $core;
    }

    /**
     * Affichage du module
     */
    function Show($all = true) {
        return $this->Index();
    }

    /*
     * Get the home page
     */

    function Index() {

        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);

        $tabForm = new TabStrip("tabForm", "Forms");
        $tabForm->AddTab($this->Core->GetCode("Forms.Forms"), $this->GetTabForms());
        $tabForm->AddTab($this->Core->GetCode("Forms.Responses"), $this->GetTabResponse());
       
        $view->AddElement(new ElementView("tabForm", $tabForm->Render()));
        return $view->Render();
    }

    /*     * *
     * Categorie dof the Forum
     */

    function GetTabForms() {

        $gdForm = new EntityGrid("gdForm", $this->Core);
        $gdForm->Entity = "Apps\Forms\Entity\FormForm";
        $gdForm->App = "FormForm";
        $gdForm->Action = "GetTabForms";

        $btnAdd = new Button(BUTTON, "btnAddForm");
        $btnAdd->Value = $this->Core->GetCode("Forms.AddForm");

        $gdForm->AddButton($btnAdd);

        $gdForm->AddColumn(new EntityColumn("Libelle", "Libelle"));

        $gdForm->AddColumn(new EntityIconColumn("Action",
                        array(array("EditIcone", "Forms.EditForm", "Forms.EditForm"),
                    array("DeleteIcone", "Forms.DeleteForm", "Forms.DeleteForm"),
                        )
        ));

        return $gdForm->Render();
    }
    
    /***
     * Response of the Form
     */
    function GetTabResponse(){
        $view = new View(__DIR__ . "/View/tabResponse.tpl", $this->Core);

        $lstForms = new EntityListBox("lstForm", $this->Core);
        $lstForms->Entity = "Apps\Forms\Entity\FormForm";
        $lstForms->ListBox->Add($this->Core->GetCode("Forms.SelectTheForm"),"");
        $lstForms->AddField("Libelle");
        $view->AddElement($lstForms);

        return $view->Render();    
    }

    /***
     * Save a form
     */
    function SaveForm($data){
       return FormHelper::SaveForm($this->Core, $data);
    }

    /***
     * Delete a form
     */
    function DeleteForm($formId){
       return  FormHelper::DeleteForm($this->Core, $formId);
    }
    /***
     * Add Question 
     */
    function AddQuestion(){
        
        $formQuestion = new FormQuestion($this->Core);
        $formQuestion->FormId->Value = Request::GetPost("FormId");
        $formQuestion->Type->Value = Request::GetPost("Type");
        $formQuestion->Libelle->Value = Request::GetPost("Libelle");
        $formQuestion->Save();
        
        $questionId = $this->Core->Db->GetInsertedId();
        
        $newQuestion = new FormQuestion($this->Core);
        $newQuestion->GetById($questionId);
        
        $decorator = new FormDecorator($this->Core);
        return $decorator->GetAdminQuestion($newQuestion);
    }
   
    /**
     * Update Libelle of a question
     */
    function UpdateQuestion(){

        $formQuestion = new FormQuestion($this->Core);
        $formQuestion->GetById(Request::GetPost("QuestionId"));
        $formQuestion->Libelle->Value = Request::GetPost("Libelle");
        $formQuestion->Save();
    }

    /**
     * delete a question
     */
    function DeleteQuestion(){
        
        $formQuestion = new FormQuestion($this->Core);
        $formQuestion->GetById(Request::GetPost("QuestionId"));

        foreach($formQuestion->GetReponses() as $response){
            $response->Delete();
        }

        $formQuestion->Delete();
    }

    /***
     * Add Reponse to a question
     */
    function AddReponse(){
        
        $formReponse = new FormResponse($this->Core);
        $formReponse->QuestionId->Value = Request::GetPost("QuestionId");
        $formReponse->Value->Value = Request::GetPost("Libelle");
        $formReponse->Save();
        
        $responseId = $this->Core->Db->GetInsertedId();
        
        $newResponse = new FormResponse($this->Core);
        $newResponse->GetById($responseId);
        
        $formQuestion = new FormQuestion($this->Core);
        $formQuestion->GetById(Request::GetPost("QuestionId"));
        
        $decorator = new FormDecorator($this->Core);
        return $decorator->GetAdminResponse($newResponse, $formQuestion->Type->Value);
    }
   
    /**
     * Update Libelle of a reponse
     */
    function UpdateReponse(){

        $formResponse = new FormResponse($this->Core);
        $formResponse->GetById(Request::GetPost("ReponseId"));
        $formResponse->Value->Value = Request::GetPost("Libelle");
        $formResponse->Save();
    }

    /**
     * delete a reponse
     */
    function DeleteReponse(){
        
        $formReponse = new FormResponse($this->Core);
        $formReponse->GetById(Request::GetPost("ReponseId"));
        $formReponse->Delete();
    }
}

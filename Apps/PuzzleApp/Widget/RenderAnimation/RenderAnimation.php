<?php

namespace Apps\PuzzleApp\Widget\RenderAnimation;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;

class RenderAnimation extends Form {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "") {
        $this->Core = $core;
    }

   /* Render the html form
     */

    function Render($params = "") {

        switch($params){
            case "Hide" :
              $view = new View(__DIR__."/View/hide.tpl", $this->Core);
             break;
           case "Fade" :
              $view = new View(__DIR__."/View/fade.tpl", $this->Core);
             break;
           case "Loading" :
              $view = new View(__DIR__."/View/loading.tpl", $this->Core);
             break;
           case "Load" :
              $view = new View(__DIR__."/View/load.tpl", $this->Core);
             break;
           case "Notify" :
              $view = new View(__DIR__."/View/notify.tpl", $this->Core);
             break;
           case "Viewer" :
              $view = new View(__DIR__."/View/viewer.tpl", $this->Core);
             break;
          case "Caroussel" :
              $view = new View(__DIR__."/View/caroussel.tpl", $this->Core);
             break;
         case "ContextMenu" :
              $view = new View(__DIR__."/View/contextMenu.tpl", $this->Core);
             break;
         case "ToolTip" :
              $view = new View(__DIR__."/View/toolTip.tpl", $this->Core);
             break;
         case "Request" :
              $view = new View(__DIR__."/View/request.tpl", $this->Core);
             break;
         case "Event" :
              $view = new View(__DIR__."/View/event.tpl", $this->Core);
             break;
        }
        
        return  $view->Render();
    }
    
}

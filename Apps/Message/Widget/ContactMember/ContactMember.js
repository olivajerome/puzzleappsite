var ContactMember = function(){};

ContactMember.Init = function(){
    Event.AddByClass("contactMember", "click", ContactMember.OpenConversation);
};

ContactMember.OpenConversation = function(e){
    
    let userId = e.srcElement.id ;
    
      Dialog.open('', {"title": Dashboard.GetCode("Membre.ContactMember"),
        "app": "Message",
        "class": "DialogMessage",
        "method": "OpenConversation",
        "type": Dialog.Type.Right ,
        "params": userId
    });
};
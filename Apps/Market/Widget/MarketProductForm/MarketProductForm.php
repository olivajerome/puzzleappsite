<?php

namespace Apps\Market\Widget\MarketProductForm;

use Core\Entity\Entity\Argument;
use Core\Control\Form\Form;
use Core\Control\Hidden\Hidden;
use Core\View\View;
use Core\View\ElementView;
use Core\Control\VTab\VTab;
use Core\Control\TabStrip\TabStrip;


use Core\Control\EntityListBox\EntityListBox;

use Apps\Market\Helper\ProductHelper;

class MarketProductForm extends Form {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "") {
        $this->Core = $core;
        $this->Data = $data;

        //On le fait ici pour avoir le formuaire � l'affichage mais aussi � la validation
        $this->Init($data);
    }

    /**
     * Initialisation 
     */
    function Init($data) {
        $this->form = new Form("MarketProductForm", $data);

        $this->form->SetView(__DIR__ . "/View/index.tpl", $this->Core);
        $this->form->SetAction("/View/index.tpl");

        $this->form->Add(array("Type" => "Hidden",
            "Id" => "Id",
        ));
      
        $this->form->Add(array("Type" => "TextBox",
            "Id" => "Title",
            "Validators" => ["Required"]
        ));

        $this->form->Add(array("Type" => "TextArea",
            "Id" => "Description",
            "Validators" => ["Required"]
        ));

        $this->form->Add(array("Type" => "NumericBox",
            "Id" => "Price",
            "Validators" => ["Required"]
        ));
        
        $this->form->Add(array("Type" => "Upload",
            "Id"  => "Upload",
            "App" => "Market",
            "Method" => "SaveImageProduct",
        ));
        
        $this->form->Add(array("Type" => "Button",
            "Id" => "btnSaveProduct",
            "Value" => $this->Core->GetCode("Save"),
            "CssClass" => "btn btn-primary"
        ));

        if($this->Data->IdEntite != ""){ 

            $tabProduct = new VTab("tabProduct");
            $tabProduct->AddTab($this->Core->GetCode("Market.Categorie"), $this->GetTabCategorie($this->Data));
            $tabProduct->AddTab($this->Core->GetCode("Market.Caracteristique"), $this->GetTabCaracteristique($this->Data));
            
            $this->form->AddElementView(new ElementView("tabProduct" , $tabProduct->Render()));
        } else {

            $emptyLabel = new \Core\Control\Libelle\Libelle("");

            $this->form->AddElementView(new ElementView("tabProduct" , $emptyLabel->Render() ));
        }
    }

    /***
     * Categorie du produit
     */
    function GetTabCategorie($product){

        $view = new View(__DIR__ . "/View/categories.tpl", $this->Core);

        $lstCategorie = new EntityListBox('lstCategorie', $this->Core);
        $lstCategorie->Entity = "Apps\Market\Entity\MarketCategory";

       //TODO PRENDRE CEUX PAR SHOP 

        $view->AddElement($lstCategorie);

        $categories = ProductHelper::GetCategoryByProduct($this->Core, $product->IdEntite);

        $hdCategory = new Hidden("hdCategory");
        $hdCategory->Value = json_encode($categories);

        $view->AddElement($hdCategory);

        return $view;
    }

    /***
     * Categorie du produit
     */
    function GetTabCaracteristique($product){

        $view = new View(__DIR__ . "/View/caracteristiques.tpl", $this->Core);

        //TODO PRENDRE CEUX PAR SHOP 
        $lstCaracteristique = new EntityListBox('lstCaracteristique', $this->Core);
        $lstCaracteristique->Entity = "Apps\Market\Entity\MarketCaracteristique";
        $lstCaracteristique->ListBox->Add($this->Core->GetCode("Market.ChooseCaracteristique"), "");
      
        $view->AddElement($lstCaracteristique);

        //TODO PRENDRE CEUX PAR SHOP 

        $lstCaracteristiqueItem = new EntityListBox('lstCaracteristiqueItem', $this->Core);
        $lstCaracteristiqueItem->Entity = "Apps\Market\Entity\MarketCaracteristiqueItem";
        $lstCaracteristiqueItem->AddField("Label");
      
        $lstCaracteristiqueItem->AddArgument(new Argument("Apps\Market\Entity\MarketCaracteristiqueItem", "CaracteristiqueId", EQUAL, 1 ));
        $view->AddElement($lstCaracteristiqueItem);

        $caracteristiques = ProductHelper::GetCaracteristiqueByProduct($this->Core, $product->IdEntite);

        $hdCaracteristique = new Hidden("hdCaracteristique");
        $hdCaracteristique->Value = json_encode($caracteristiques);

        $view->AddElement($hdCaracteristique);


        
        return $view;
    }

    /*     * *
     * G�n�rer les formulaire
     */

    function Render() {
        return $this->form->Render();
    }

    /*     * *
     * Valide les donn�es selon les diff�rents formulaires
     */

    function Validate($data = "") {
        $errors = $this->form->Validate($this->Data);

        return $errors;
    }

    /*     * *
     * Charge les controls avec les donn�es de l'entit�
     */

    function Load($entity) {
        $this->form->Load($entity);
    }

    /*     * *
     * Charge l'entit� avec les donn�es des diff�rents formulaire
     */

    function Populate($entity) {
        
    }

    /*     * *
     * Gestion des erreurs
     */

    function Error() {
        $errors[] = $this->form->errors;

        return json_encode(array("statut" => "Error", "message" => $errors));
    }

}

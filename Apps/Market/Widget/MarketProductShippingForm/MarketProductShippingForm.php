<?php

namespace Apps\Market\Widget\MarketProductShippingForm;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;

class MarketProductShippingForm extends Form {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "") {
        $this->Core = $core;
        $this->Data = $data;

        $this->Init($data);
    }

    /**
     * Initialisation 
     */
    function Init($data) {
        $this->form = new Form("MarketProductShippingFormForm", $data);

        $this->form->SetView(__DIR__ . "/View/index.tpl", $this->Core);
        $this->form->SetAction("/View/index.tpl");

        $this->form->Add(array("Type" => "Hidden",
            "Id" => "Id",
        ));
        
        $this->form->Add(array("Type" => "Hidden",
            "Id" => "OfferId",
        ));

        $this->form->Add(array("Type" => "Hidden",
           "Id" => "ProductId",
        ));
 
        $this->form->Add(array("Type" => "TextBox",
            "Id" => "TrackingNumber",
        ));
        
        $this->form->Add(array("Type" => "TextBox",
            "Id" => "TrackingUrl",
        ));


        $this->form->Add(array("Type" => "Button",
            "Id" => "btnSaveShipping",
            "Value" => $this->Core->GetCode("Market.SaveShipping"),
            "CssClass" => "btn btn-primary"
        ));
    }

    /*     * *
     * Render the html form
     */

    function Render() {
        return $this->form->Render();
    }

    /*     * *
     * Validate the data
     */

    function Validate($data = "") {
        $errors = $this->form->Validate($this->Data);

        return $errors;
    }

    /*     * *
     * Load control with the entity
     */

    function Load($entity) {
        $this->form->Load($entity);
    }

    /*     * *
     * Populate the entity with the form Data
     */

    function Populate($entity) {
        
    }

    /*     * *
     * Error message
     */

    function Error() {
        $errors[] = $this->form->errors;

        return json_encode(array("statut" => "Error", "message" => $errors));
    }

}

<?php 
/*
* PuzzleApp
* Webemyos
* Jérôme Oliva
* GNU Licence
*/

namespace Apps\Folders\Entity;
use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;


class FolderFileShared extends Entity  
{
    
    protected $User;
    protected $File;
    
	//Constructeur
	function __construct($core)
	{
		//Version
		$this->Version ="2.0.0.0"; 

		//Nom de la table 
		$this->Core=$core; 
		$this->TableName="FolderFileShared"; 
		$this->Alias = "FolderFileShared"; 

		$this->FileId = new Property("FileId", "FileId", NUMERICBOX,  false, $this->Alias); 
		$this->UserId = new Property("UserId", "UserId", NUMERICBOX,  false, $this->Alias); 
        $this->User = new EntityProperty("Core\Entity\User\User", "UserId"); 
        $this->File = new EntityProperty("Apps\Folders\Entity\FolderFiles", "FolderId"); 

        //Creation de l entité 
		$this->Create(); 
	}
    
    function GetPseudo(){
        return $this->User->Value->GetPseudo();
    }
}
?>
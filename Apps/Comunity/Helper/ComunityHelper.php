<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Apps\Comunity\Helper;

use Apps\Comunity\Entity\ComunityComunity;
use Apps\Comunity\Entity\ComunityMessage;
use Apps\Comunity\Entity\ComunityUser;
use Apps\Comunity\Entity\ComunityComment;
use Apps\Comunity\Entity\ComunityLike;

use Core\Utility\Date\Date;
use Core\Entity\Entity\Argument;
     
use Core\Utility\File\File;
use Apps\Comunity\Entity\ComunityFollow;
use Core\Dashboard\DashBoardManager;

use Apps\Annonce\Entity\AnnonceAnnonce;

use Apps\Comunity\Widget\ComunityForm\ComunityForm;


/***
 * Class utilitaire 
 */
class ComunityHelper
{
    
    public static function GetUser()
    {}

    /***
     * Retourne la communauté par défaut
     * Celle qui a le type 1 
     * */    
    public static function GetDefault($core)
    {
        $comunity = new ComunityComunity($core);
        $comunity->AddArgument(new Argument("Apps\Comunity\Entity\ComunityComunity", "Type", EQUAL , "1"));
     //   $comunity->AddArgument(new Argument("Apps\Comunity\Entity\ComunityComunity", "Status", NOTEQUAL , "99"));
      
        $comunitys = $comunity->GetByArg();
        
        if(count($comunitys) > 0 ){
            return $comunitys[0];
        } 
        
        return null;
    }

     /**
     * Obtient une requete permettant d'obtenir les communauté de l'utilisateur
     */
    public static function GetRequestByUser($core)
    {
        $request = "SELECT ComunityId FROM ComunityUser WHERE UserId = ".$core->User->IdEntite;
      //   $request .= " union select 1 as ComunityId "; 
        return $request;
    }


    /**
     * Créer une conversation
     * @param type $core
     * @param type $title
     */
    public static function CreateConversation($core, $users, $message, $title ="" , $type= 1 , $reseauCode = "" , $projetId = "" )
    {
        //Conversation a plusieurs
        if($users != ""){
            
            $request = "SELECT GROUP_CONCAT(Pseudo) as pseudo  FROM ee_user WHERE ID in(".$users.")" ;
            $result = $core->Db->GetLine($request);

            if($title == ""){
              $title = $result["pseudo"];  
            }
        }
        
        $Comunity = new ComunityComunity($core);
        $Comunity->UserId->Value = $core->User->IdEntite;
        $Comunity->Title->Value = $title; 
        $Comunity->SubTitle->Value = $message; 
        $Comunity->DateCreated->Value = Date::Now();
        $Comunity->DateLastMessage->Value = Date::Now(true);
        $Comunity->Type->Value = $type;
        
        if($type == 2) {
        
          $reseau = new CoopereReseau($core);
          $reseau = $reseau->GetByCode($reseauCode);
          
          $Comunity->ReseauId->Value =   $reseau->IdEntite;
        }
      
        //Comunityion sur un projet
        if($projetId != ""){
            
            $Comunity->ProjetId->Value = $projetId;
            $baseComunity = new ComunityComunity($core);
            $baseComunity->GetById($projetId);
            
            //TODO RAJOUTE LE MEMEBRE DANS CE PROJET LORSQU'IL rentre dans la conversation ,
            $users = $baseComunity->GetMembresIds();
        } 
        
        //Status 0 pour dire qu'il est en création temsp que personne ne participe
        if($reseauCode != "")
        {
            $Comunity->Status->Value = "0";
        }
        else
        {
            $Comunity->Status->Value = 1;
        }
        $Comunity->Save();
        $ComunityId = $core->Db->GetInsertedId();
          
        //Pour les rencontres thématiques et les Actualités on n'enregistre pas de membres
        if($type == 2) {
            return $ComunityId;
        }
        
        //Ajoute les utilisateurs
        $ComunityMember = new ComunityUser($core);
        $ComunityMember->ComunityId->Value = $ComunityId;
        $ComunityMember->UserId->Value = $core->User->IdEntite;
        $ComunityMember->Status->Value = 1;
         $ComunityMember->Readed->Value = 2;
        $ComunityMember->Save();
        
        if($users != "")
        {
            foreach(explode(",", $users)  as $user )
            {
                $ComunityMember = new ComunityUser($core);
                $ComunityMember->ComunityId->Value = $ComunityId;
                $ComunityMember->UserId->Value = $user;
                $ComunityMember->Status->Value = 1;
                $ComunityMember->Readed->Value = 1;
                $ComunityMember->Save();
            }
        }
        
        //Création du premier message
        $discusMessage = new ComunityMessage($core);
        $discusMessage->UserId->Value =  $core->User->IdEntite;
        $discusMessage->DateCreated->Value = Date::Now();
        $discusMessage->Message->Value = $message;
        $discusMessage->Type->Value = 1;
        $discusMessage->ComunityId->Value = $ComunityId;
        $discusMessage->Save();
        
        return $ComunityId;
    }
    
    /**
     * Ajoute un message à la disccusion 
     */
    public static function AddMessage($core, $ComunityId, $message, $type = 1)
    {
        $discusMessage = new ComunityMessage($core);
        $discusMessage->UserId->Value =  $core->User->IdEntite;
        $discusMessage->DateCreated->Value = Date::Now();
        $discusMessage->Message->Value = $message;
        $discusMessage->Type->Value = $type;
        $discusMessage->ComunityId->Value = $ComunityId;
        //$discusMessage->Status->Value = 1;
       
        $discusMessage->Save();
        
        return $core->Db->GetInsertedId();
    }
    
    /**
     * Obitient les conversation active de l'utilisateur
     * @param type $core
     */
    public static function GetActive($core)
    {
        $request = "SELECT group_concat(ComunityId) as ComunityIds FROM ComunityUser";
        $request .= " JOIN ComunityComunity as Comunity on Comunity.Id = ComunityUser.ComunityId  WHERE ComunityUser.Status = 1 AND ComunityUser.UserId = ".$core->User->IdEntite ;
        $request .= " AND Comunity.ReseauId is null ";
       
        $result = $core->Db->GetLine($request);
        
        $Comunity = new ComunityComunity($core);
        
        return $Comunity->Find("Id in (".$result["ComunityIds"].") Order By DateLastMessage desc ");
     }
     
     /***
      * Obitent les Comunityions actives
      */
     public static function GetRencontreActive($core, $reseauCode)
     {
        $reseau = new CoopereReseau($core);
        $reseau = $reseau->GetByCode($reseauCode);
          
          
        $request = "SELECT group_concat(ComunityId) as ComunityIds FROM ComunityUser";
        $request .= " JOIN ComunityComunity as Comunity on Comunity.Id = ComunityUser.ComunityId  WHERE ComunityUser.Status = 1 AND ComunityUser.UserId = ".$core->User->IdEntite ;
        $request .= " AND Comunity.ReseauId = " .$reseau->IdEntite;
        
        $result = $core->Db->GetLine($request);
        
        $Comunity = new ComunityComunity($core);
        
        return $Comunity->Find("Id in (".$result["ComunityIds"].") Order By Id desc ");
     }
     
     /**
      * Obtient les message de la conversation
      * @param type $core
      * @param type $ComunityId
      */
     public static function GetMessages($core, $ComunityId)
     {
       $discusMessage = new ComunityMessage($core);
       $discusMessage->AddArgument(new Argument("Apps\Comunity\Entity\ComunityMessage", "ComunityId", EQUAL, $ComunityId));
       
       return $discusMessage->GetByArg();
     }
     
     /**
      * Supprime un message 
      * @param type $core
      * @param type $messageId
      */
     public static function RemoveMessage($core, $messageId)
     {
           $discusMessage = new ComunityMessage($core);
           $discusMessage->GetById($messageId);
           $discusMessage->Delete();
     }
     
     /**
      * Met à jour un message
      */
     public static function UpdateMessage($core, $messageId, $newMessage)
     {
        $discusMessage = new ComunityMessage($core);
        $discusMessage->GetById($messageId);
        $discusMessage->Message->Value = $newMessage;
        
        $discusMessage->Save();
     } 
     
     /**
      * Met à jour un message
      */
     public static function UpdateComment($core, $messageId, $newMessage)
     {
        $discusMessage = new ComunityComment($core);
        $discusMessage->GetById($messageId);
        $discusMessage->Message->Value = $newMessage;
        $discusMessage->Save();
     } 
     
     /**
      * Cloture la conversation
      */
     public static function RemoveComunity($core, $ComunityId)
     {
        $Comunity = new ComunityComunity($core);
        $Comunity->GetById($ComunityId);
        
        if($Comunity->UserId->Value == $core->User->IdEntite)
        {
           $Comunity->Status->Value =  2;
           $Comunity->DateClotured->Value = Date::Now(); 
           $Comunity->Save();
           
            $request = " UPDATE ComunityUser Set Status = 2 WHERE ComunityId =   " .$ComunityId;
          
            $core->Db->Execute($request);
        }
        //On clot que pour Moi 
        else
        {
           echo $request = " UPDATE ComunityUser Set Status = 2 WHERE ComunityId =   " .$ComunityId . " and UserId = " . $core->User->IdEntite ;
            $core->Db->Execute($request);
        }
     }
     
     /**
      * Obtient les rencontres thématiques d'un reseau
      */
     public static function GetLastRencontreByReseau($core, $reseauCode, $status = "", $limit = true)
     {
        $reseau = new CoopereReseau($core);
        $reseau = $reseau->GetByCode($reseauCode);
      
        $Comunity = new ComunityComunity($core);
        $Comunity->AddArgument(new Argument("Apps\Comunity\Entity\ComunityComunity", "ReseauId", EQUAL, $reseau->IdEntite)); 
        $Comunity->AddArgument(new Argument("Apps\Comunity\Entity\ComunityComunity", "Type", NOTEQUAL, 4)); 
        
        if($status == "")
        {
            $Comunity->AddArgument(new Argument("Apps\Comunity\Entity\ComunityComunity", "Status", EQUAL, 1)); 
        }
        else
        {
            $Comunity->AddArgument(new Argument("Apps\Comunity\Entity\ComunityComunity", "Status", EQUAL, $status)); 
        }
        
        $Comunity->AddOrder("Id desc");
        
        if($limit){
            $Comunity->SetLimit(1,3);
        }
        
        return $Comunity->GetByArg();
     }

     /**
      * Permet de suivres les conversations de rencontres thématiques
      * @param type $core
      * @param type $disccusId
      */
     public static function ParticipateProjet($core, $disccusId)
     {
         if(!self::Exist($core,$disccusId ))
         {
           $ComunityMember = new ComunityUser($core);
           $ComunityMember->UserId->Value = $core->User->IdEntite;
           $ComunityMember->ComunityId->Value = $disccusId;
           $ComunityMember->Status->Value= 1;
           $ComunityMember->Readed->Value = 2; 
           $ComunityMember->Save();
         }
         
         $Comunity = new ComunityComunity($core);
         $Comunity->GetById($disccusId);
         
         //Un membre rejoint le projet il passe donc en Actifs
         if($Comunity->Status->Value == 0)
         {
            $request = "Update ComunityComunity SET Status =  1 WHERE Id = " .  $disccusId;
            $core->Db->Execute($request);      
         }
         
          //Il faut aussi que supprimer le memebre de la disccussion
         $Comunity = new ComunityComunity($core);
         $Comunity->AddArgument(new Argument("Apps\Comunity\Entity\ComunityComunity" ,"ProjetId", EQUAL, $disccusId));
         $Comunity =  $Comunity->GetByArg();
         
         if(count($Comunity) > 0)
         {
            $ComunityMember = new ComunityUser($core);
            $ComunityMember->UserId->Value = $core->User->IdEntite;
            $ComunityMember->ComunityId->Value =  $Comunity[0]->IdEntite;
            $ComunityMember->Status->Value = 1;
            $ComunityMember->Readed->Value = 2;
            $ComunityMember->Save();
        }
     }
     
     /*
      * Ne plue participer
      */
     public static function UnParticipateProjet($core, $disccusId)
     {
         $ComunityMember = new ComunityUser($core);
         $ComunityMember->AddArgument(new Argument("Apps\Comunity\Entity\ComunityUser", "UserId", EQUAL, $core->User->IdEntite));
         $ComunityMember->AddArgument(new Argument("Apps\Comunity\Entity\ComunityUser" ,"ComunityId", EQUAL, $disccusId));
         
         $members= $ComunityMember->GetByArg();
         
         $members[0]->Delete();
         
         //Il faut aussi que supprimer le memebre de la disccussion
         $Comunity = new ComunityComunity($core);
         $Comunity->AddArgument(new Argument("Apps\Comunity\Entity\ComunityComunity" ,"ProjetId", EQUAL, $disccusId));
         $Comunity =  $Comunity->GetByArg();
         
         if(count($Comunity) > 0)
         {
            $ComunityMember = new ComunityUser($core);
            $ComunityMember->AddArgument(new Argument("Apps\Comunity\Entity\ComunityUser", "UserId", EQUAL, $core->User->IdEntite));
            $ComunityMember->AddArgument(new Argument("Apps\Comunity\Entity\ComunityUser" ,"ComunityId", EQUAL, $Comunity[0]->IdEntite));
            $ComunityProjet =  $ComunityMember->GetByArg();
            
            if(count($ComunityProjet)> 0){
                $ComunityProjet[0]->Delete();
            }
         }
     }
     
     /**
      * Verifie si on suit dèjà une renconctre
      */
     public static function Exist($core, $ComunityId)
     {
         $ComunityMember = new ComunityUser($core);
         $ComunityMember->AddArgument(new Argument("Apps\Comunity\Entity\ComunityUser", "UserId", EQUAL, $core->User->IdEntite));
         $ComunityMember->AddArgument(new Argument("Apps\Comunity\Entity\ComunityUser" ,"ComunityId", EQUAL, $ComunityId));
         
         $members= $ComunityMember->GetByArg();
         
         return (count($members) > 0);
     }
     
     /**
      * Permet de suivre une conversation
      */
     public static function AddFollow($core, $disccusId)
     {
        if(!self::UserFollow($core,$disccusId))
        {
            $ComunityFollow = new ComunityFollow($core);
            $ComunityFollow->UserId->Value = $core->User->IdEntite;
            $ComunityFollow->ComunityId->Value = $disccusId;
            $ComunityFollow->Save();
        }
     }
     
     /***
      * L'utilisateur suit le Comunity
      */
     public static function UserFollow($core, $projetId)
     {
        $ComunityFollow = new ComunityFollow($core);
        $ComunityFollow->AddArgument(new Argument("Apps\Comunity\Entity\ComunityFollow", "UserId", EQUAL, $core->User->IdEntite));
        $ComunityFollow->AddArgument(new Argument("Apps\Comunity\Entity\ComunityFollow", "ComunityId",EQUAL,  $projetId));
         
        return (count($ComunityFollow->GetByArg()) > 0);
     }
     
     /*
      * Le membre participe
      */
     public static function UserParticipate($core, $projetId)
     {
        $ComunityUser = new ComunityUser($core);
        $ComunityUser->AddArgument(new Argument("Apps\Comunity\Entity\ComunityUser", "UserId", EQUAL, $core->User->IdEntite));
        $ComunityUser->AddArgument(new Argument("Apps\Comunity\Entity\ComunityUser", "ComunityId", EQUAL, $projetId));
        
        return (count($ComunityUser->GetByArg()) > 0);
     }
     
     /***
      * Ne plus suivre le projet
      */
     public static function  UnFollowProjet($core, $projetId)
     {
       $request = "DELETE FROM ComunityFollow WHERE ComunityId = " .$projetId . " AND UserId = " .$core->User->IdEntite;
       $core->Db->Execute($request);
     }
     
     /**
      * Ajoute un commentaire à un message
      * @param type $core
      * @param type $messageId
      * @param type $message
      */
     public static function AddComment($core, $messageId, $message, $images, $commentType = "message")
     {
         $comment = new ComunityComment($core);
         if($commentType == "message")
         {
             $comment->MessageId->Value = $messageId;
             
             //Envoi d'une notification
             $discusMessage = new ComunityMessage($core);
             $discusMessage->GetById($messageId);

            if($discusMessage->UserId->Value != $core->User->IdEntite){
                
                $notify = DashBoardManager::GetApp("Notify", $core);
                $notify->AddNotify($core->User->IdEntite, "AddComment", $discusMessage->UserId->Value, "Comunity", $messageId);
            }

         }
         else
         {
             $comment->ParentId->Value = $messageId;
         }
         $comment->Message->Value = $message;
         $comment->UserId->Value = $core->User->IdEntite;
         $comment->DateCreated->Value = Date::Now();
         $comment->Save();
         
      
         
         $commentId = $core->Db->GetInsertedId();
         $request = "SELECT ComunityId as ComunityId from ComunityMessage WHERE Id =" . $messageId;
         $result = $core->Db->GetLine($request);
         
         $basePath = "Data/Apps/Comunity/" .$result["ComunityId"] . "/Message/";
     
         if($images != "")
         {
           $images = explode(";", $images);
           $messageImage = "";
           
          // var_dump($images);
           
           //On déplace les images dans le bon répertoire
           foreach($images as $image)
           {
                $path = explode("/", $image);
                   
                
                    if(!file_exists($basePath))
                    {
                        mkdir($basePath);
                    }
                    
                    $basePath .= $messageId;
                        
                    if(!file_exists($basePath))
                    {
                        mkdir($basePath);
                    }
                
                    $basePath .= "/Comment";
                   
                    //TODO RECUPERE L'ID DE L4UTILISATEUR SI C'EST UNE INSCIRPTIon
                    if(!file_exists($basePath))
                    {
                        mkdir($basePath);
                    }
                    
                   $basePath .= "/" . $commentId;
                  
                     //TODO RECUPERE L'ID DE L4UTILISATEUR SI C'EST UNE INSCIRPTIon
                    if(!file_exists($basePath))
                    {
                        mkdir($basePath);
                    }
                   $fileName = $path[count($path) -1];

                   rename("Data/Tmp/" .$fileName, $basePath."/" .$fileName);
                   
                $messageImage .=  "<img src='" . $core->GetPath("/" . $basePath. "/" .$fileName)."' >";  
           }
           
           $message = $messageImage .  $message;
         
           ComunityHelper::UpdateComment($core, $commentId, $message);
           
           //Env
           
         }
         
         return $commentId;
    }
    
    /**
     * Obtient les commentaires
     */
    public static function GetComment($core, $messageId, $commentId = "")
    {
        $comment = new ComunityComment($core);
        if($commentId != ""){
            $comment->AddArgument(new Argument("Apps\Comunity\Entity\ComunityComment", "ParentId", EQUAL, $commentId));
        }
        else
        {
            $comment->AddArgument(new Argument("Apps\Comunity\Entity\ComunityComment", "MessageId", EQUAL, $messageId));
        }
        return $comment->GetByArg();
    }
    
    /***
     * Supprime un message et tous les commentaires associé
     */
    public static function DeleteMessage($core, $messageId)
    {
        
        $request = "DELETE FROM ComunityComment WHERE MessageId = " . $messageId;
        $core->Db->Execute($request);
        
        $message = new ComunityMessage($core);
        $message->GetById($messageId);
             
        //Suppression des images
        File::RemoveAllDir("Data/Apps/Comunity/" .$message->ComunityId->Value . "/Message/" . $messageId);
       
        $message->Delete();
       
        return "OK";
    }
    
    /***
     * Supprime un commentaire
     */
    public static function DeleteComment($core, $messageId)
    {
        //Suppression des commentaire de commentaire
        $request = "DELETE FROM ComunityComment WHERE ParentId = ".$messageId;
        $core->Db->Execute($request);
        
        $message = new ComunityComment($core);
        $message->GetById($messageId);
        //$message->Delete();
        
        $messageParent = new ComunityMessage($core);
        $messageParent->GetById($message->MessageId->Value);
        //Suppression des images
        File::RemoveAllDir("Data/Apps/Comunity/" .$messageParent->ComunityId->Value . "/Message/" . $message->MessageId->Value . "/Comment/" . $messageId );
        
        $request = "DELETE FROM ComunityComment WHERE Id = ".$messageId;
        $core->Db->Execute($request);
        
        return "OK";
    }

    //Met à jour le Subtitle de la conversation
    public static function UpdateLastMessage($core, $ComunityId, $message)
    {
        //Format Francais
        $Comunity = new ComunityComunity($core);
        $Comunity->GetById($ComunityId);
        $Comunity->SubTitle->Value = $message;
        $Comunity->DateLastMessage->Value = Date::Now(true);
        $Comunity->Save();
        
        //On repasse les message en non read
        $request = "UPDATE ComunityUser set Readed = 1 WHERE ComunityId =  " . $ComunityId . " AND UserId <> ". $core->User->IdEntite;
        $core->Db->Execute($request);
    }
    
    /**
     * Le membvre à lu la conversation
     */
    public static function SetUserRead($core, $ComunityId)
    {
        $request = "Update ComunityUser SET Readed = 2 WHERE ComunityId =  " . $ComunityId . " AND UserId = " .$core->User->IdEntite ;
        $core->Db->Execute($request);
        
        //Si c'est une discission d'un projet
        $request = "select ProjetId FROM ComunityComunity WHERE Id = " .$ComunityId;
   
        $projet = $core->Db->GetLine($request);
        
        if($projet["ProjetId"] != null )
        {
            $request = "Update ComunityUser SET Readed = 2 WHERE ComunityId =  " . $projet["ProjetId"] . " AND UserId = " .$core->User->IdEntite ;
            $core->Db->Execute($request);
        }
    }
    
    /**
     * Obitent le nombre de message non lu
     */
    public static function GetNumberNotRead($core)
    {
        if($core->User->IdEntite  != ""){
            $request = "SELECT count(du.Id)  as nbNotRead from ComunityUser as du 
                        JOIN ComunityComunity  as d on d.Id = du.ComunityId
                        WHERE du.UserId ="  .$core->User->IdEntite . " AND du.Status =1 AND du.Readed = 1 AND d.Status <> 10  " ;
            $result = $core->Db->GetLine($request);
        
            return $result["nbNotRead"];
        } else {
            return 0;
        }
    }
    
    /***
     * Obtient l'identifiant de la Comunityion de la page d'accueil
     */
    public static function GetIdByReseau($core, $reseauId)
    {
       $Comunity = new ComunityComunity($core);
       $Comunity->AddArgument(new Argument("Apps\Comunity\Entity\ComunityComunity", "ReseauId", EQUAL, $reseauId)); 
       $Comunity->AddArgument(new Argument("Apps\Comunity\Entity\ComunityComunity", "Type", EQUAL, 4)); 
       
       $disccus = $Comunity->GetByArg();
       
       //Il n'y a pas de disccusion on va la créer
       if(count($disccus[0]) == 0)
       {
           $Comunity = new ComunityComunity($core);
           $Comunity->Type->Value = 4;
           $Comunity->ReseauId->Value = $reseauId ;
           $Comunity->UserId->Value = $core->User->IdEntite;
           $Comunity->DateCreated->Value = Date::Now();
           $Comunity->Status->Value = 1;
           $Comunity->Save();
           
           return self::GetIdByReseau($core, $reseauId);
       }
       return $disccus[0];  
    }
    
    /***
     * Ajotue un like sur un message ou un commentaire
     */
    public static function AddLike($core, $entityId, $type)
    {
        if($type == "message")
        {
            $like = new ComunityLike($core);  
            $like->AddArgument(new Argument("Apps\Comunity\Entity\ComunityLike", "UserId", EQUAL, $core->User->IdEntite));
            $like->AddArgument(new Argument("Apps\Comunity\Entity\ComunityLike", "EntityId", EQUAL, $entityId));
            $like->AddArgument(new Argument("Apps\Comunity\Entity\ComunityLike", "EntityName", EQUAL, $type));
            
            $likes = $like->GetByArg();
            
            if(count($likes) == 0)
            {
                $like = new ComunityLike($core);  
                $like->UserId->Value = $core->User->IdEntite;
                $like->EntityId->Value = $entityId;
                $like->EntityName->Value = "message";
                $like->DateCreated->Value = Date::Now();
                $like->Save();
                
                //Envoi d'une notification
                $discusMessage = new ComunityMessage($core);
                $discusMessage->GetById($entityId);

                if($discusMessage->UserId->Value != $core->User->IdEntite){
                    $notify = DashBoardManager::GetApp("Notify", $core);
                    $notify->AddNotify($core->User->IdEntite, "LikePost", $discusMessage->UserId->Value, "Comunity", $entityId);
                }

            }
            else
            {
                $likes[0]->Delete();
            }
        }
        
        if($type == "comment")
        {
           $like = new ComunityLike($core);  
            $like->AddArgument(new Argument("Apps\Comunity\Entity\ComunityLike", "UserId", EQUAL, $core->User->IdEntite));
            $like->AddArgument(new Argument("Apps\Comunity\Entity\ComunityLike", "EntityId", EQUAL, $entityId));
            $like->AddArgument(new Argument("Apps\Comunity\Entity\ComunityLike", "EntityName", EQUAL, $type));
            
            $likes = $like->GetByArg();
            
            if(count($likes) == 0)
            {
                $like = new ComunityLike($core);  
                $like->UserId->Value = $core->User->IdEntite;
                $like->EntityId->Value = $entityId;
                $like->EntityName->Value = "comment";
                $like->DateCreated->Value = Date::Now();
                $like->Save();
            
                  //Envoi d'une notification
                $discusMessage = new ComunityComment($core);
                $discusMessage->GetById($entityId);

                if($discusMessage->UserId->Value != $core->User->IdEntite){
                  $notify = DashBoardManager::GetApp("Notify", $core);
                  $notify->AddNotify($core->User->IdEntite, "LikeComment", $discusMessage->UserId->Value, "Comunity", $entityId);
                }
            }
            else
            {
                $likes[0]->Delete();
            }        
                
    
        }
    }
    
    /**
     * Obtient le nombre de like d'un commentaire
     */
    public static function GetNumberLike($core, $entityId)
    {
       $like = new ComunityLike($core);  
       $like->AddArgument(new Argument("Apps\Comunity\Entity\ComunityLike", "EntityId", EQUAL, $entityId));
       $like->AddArgument(new Argument("Apps\Comunity\Entity\ComunityLike", "EntityName", EQUAL, "comment"));
            
       return count($like->GetByArg());
    }
    
    /***
     * Crée un nouveau group de discussion
     */
    public static function SaveComunity($core, $data ){
        
        
       $comunityForm = new ComunityForm($core, $data);

        if($comunityForm->Validate($data)){
            
            $comunity = new ComunityComunity($core);
        
            if ($data["Id"] != "") {
              $comunity->GetById($data["Id"]);
            }
            else{
                $comunity = new ComunityComunity($core);
                $comunity->Type->Value = 1;
                $comunity->UserId->Value = $core->User->IdEntite;
            }   
            
            $comunityForm->Populate($comunity);
            $comunity->Save();
            
            return true;
        }
    }
    
    /***
     * Supprime une conversation 
     */
    public static function DeleteComunity($core, $ComunityId )
    {
        //On supprime le répertoire
        //File::RemoveAllDir("Data/Apps/Comunity/" .$ComunityId);
        //TROP compliaque de supprimé on la passe en statut = 99
        $Comunity = new ComunityComunity($core);
        $Comunity->GetById($ComunityId);
        $Comunity->Status->Value = 99;
        $Comunity->DateClotured->Value = Date::Now();
        $Comunity->DateLastMessage->Value = Date::Now(true);
        $Comunity->Save();
    }
    
    /**
     * Archivage du projet
     */
    public static function ArchiveProjet($core, $ComunityId )
    {
        $Comunity = new ComunityComunity($core);
        $Comunity->GetById($ComunityId);
        $Comunity->Status->Value = 10;
        $Comunity->DateClotured->Value = Date::Now();
        $Comunity->Save();
    }
    
    /***
     * Retourne les identifiants des projets que l'utilisateur Suit ou Participate
     * Sur un réseau
     */
    public static function GetUserProjet($core, $reseauId)
    {
        //Le projets dont le participe
        $request = "SELECT group_concat(d.Id) as ids from ComunityComunity as d
                    JOIN ComunityUser as u on u.ComunityId = d.Id and u.UserId = ".$core->User->IdEntite." 
                    AND d.status = 1 AND d.Type = 2
                   AND d.ReseauId=  $reseauId
                ";
        
        $result = $core->Db->GetLine($request);
        $userParticipate = $result["ids"];
        
        
        $request = "SELECT group_concat(d.Id)  as ids from ComunityComunity as d
                    JOIN ComunityFollow as f on f.ComunityId = d.Id and  f.UserId =  ".$core->User->IdEntite." 
                    AND d.status = 1 AND d.Type = 2
                    AND d.ReseauId=  $reseauId                   
                   ";
        
        $result = $core->Db->GetLine($request);
        $userFollow = $result["ids"];
        $projetId = "";
        
        if($userParticipate != ""){
           $projetId .= $userParticipate;
        }
        
        if($userFollow)
        {
            if($projetId != ""){
                $projetId .= ",";
            }
             $projetId .= $userFollow;
        }
        return $projetId;
    }
    
    /***
     * Retrouve une conversation entre deux membre
     * On prend la derniére et là ou il n'y a que deux membre
     * et de type Comunityion Directe
     * 
     */
    public static function FindConversation($core, $userId, $memberId)
    {
        $request = "SELECT Distinct(Comunity.id) lastId FROM `ComunityComunity`  as Comunity
                    JOIN ComunityUser as u on u.ComunityId = Comunity.id
                    JOIN ComunityUser as m on m.ComunityId = Comunity.id
                    WHERE Comunity.Type= 1  AND  u.UserId = $userId and m.UserId = $memberId 
                    AND (SELECT count(ComunityId) FROM ComunityUser WHERE ComunityId = u.ComunityId) = 2 
                    ORDER BY Comunity.id desc limit 0,1";
        $result = $core->Db->GetLine($request);
        return $result['lastId'];
    }
    
    /**
     * Vérifie qu'il existe une conversation entre les membres sur ce projet
     * @param type $core
     * @param type $projetIdV
     */
    public static function HavePrivateComunityion($core, $projetId)
    {
         $Comunity = new ComunityComunity($core);
         return $Comunity->Find("ProjetId = " . $projetId);
    }
    
    /***
     * Si on est ambassadeur ou propriétaire du message on peut le modifier ou le supprimer
     */
    public static function IsUserOrAdmin($core, $messageId, $type)
    {
        if($core->User->GroupId->Value ==  1)
        {
            return true;    
        }

        if($type == "message")
        {
          $message = new ComunityMessage($core);
          $message->GetById($messageId);
          
          if($message->UserId->Value == $core->User->IdEntite){
             return true;
          }
        }
        
        else if($type == "comment")
        {
          $comment = new ComunityComment($core);
          $comment->GetById($messageId);
          
          if($comment->UserId->Value == $core->User->IdEntite){
             return true;
          }
        }
        return false;
    }
    
    /***
     * Ajoute un message dans le fil d'actualité
     */
    public static function AddMessageInActu($core, $reseauCode, $MessageType, $idEntite, $libelle ="")
    {
        
        $reseau = new CoopereReseau($core);
        $reseau = $reseau->GetByCode($reseauCode); 
        
        $reseauCode .":" . $reseau->IdEntite ;
        $disccus = self::GetIdByReseau($core, $reseau->IdEntite);
              
        
        switch($MessageType)
        {
            case "NewProjet":
                
                $projet = new ComunityComunity($core);
                $projet->GetById($idEntite);
                
                
                $message = "<div class='actuMessage'> <h3>Un membre vient de proposer un nouveau projet.</h3>";
                $message .= "<img src='".$projet->GetFirstImageUrl() . "' style='width:100px' />";
                $message .= "<p> Venez le découvrir via ce lien, Projet : <a href='" . $core->GetPath("/" . $reseauCode . "/ParlonsEn/Rencontre/" .$idEntite) ."' > " ;
                $message .= $projet->Title->Value . "</a></p></div>";
                
            break;  
        
            case "NewAnnonce":
                
                $annonce = new AnnonceAnnonce($core);
                $annonce->GetById($idEntite);
                
                
                $message = "<div class='actuMessage'> <h3>Un membre vient de publier une nouvelle annonce.</h3>";
                $message .= "<p> Venez la découvrir via ce lien, Offre : <a href='" . $core->GetPath("/" . $reseauCode . "/Offre/" .$idEntite) ."' > " ;
                $message .= $annonce->GetSimpleTitle() . "</a></p></div>";
                
                
                break;
            
            case "NewMembre" :
                
                $message =   "<div class='actuMessage'><h3>Un nouveau membre vient de rejoindre notre réseau.</h3>";
                $message .= "<p> Venez découvrir le détail de ces offres via ce lien : " ;
                $message .= "<a  href='" .$core->GetPath("/" .$reseauCode. "/User/" . $idEntite)."' >Détail des offres </a></p></div>" ;

                break;
            
            case "NewAvisResource" :
                
                $Resource = new  \Apps\Resource\Entity\ResourceResource($core);
                $Resource->GetById($idEntite);
                       
                $message = "<div class='actuMessage'> <h3>Un membre vient de donner son avis sur un l'eco geste " .$Resource->Name->Value. " .</h3>";
                $message .= "<p> $libelle </p>" ;
                $message .= "</div>" ;
                
                break;
            
        }        
        self::AddMessage($core, $disccus->IdEntite, $message, 5);
    }
    
    /***
     * Obtient les groupes de discussions
     */
    public static function GetGroupes($core)
    {
        $Comunity = new ComunityComunity($core);
        return $Comunity->Find("Type = 2");
    }
    
    public static function GetMessagesWithComment($core, $ComunityId = 1, $page = ""){
         $request = "SELECT message.Id as messageId, message.Message as text, message.DateCreated as dateCreated, 
                    user.Pseudo as pseudo, 
                    user.FirstName as FirstName, 

                    user.Image as image,
                    message.type as messageType,            

                    count(distinct(lk.id)) as nbLike,
                    count(distinct(comment.Id)) as nbComment,
                    GROUP_CONCAT(distinct(userLike.image) SEPARATOR ':') as userLike ,
                    
                    comunity.type as discusType, comunity.Title as discussTitle,
                    comunity.Id as projetId,
                    comment.Message as comment

                    FROM ComunityMessage as message 

                    JOIN ee_user as user on user.Id = message.UserId 
                    JOIN ComunityComunity as comunity on message.ComunityId = comunity.Id   

                    LEFT JOIN ComunityComment as comment on comment.MessageId = message.Id
                    LEFT JOIN ComunityLike as lk on lk.EntityId = message.Id and lk.EntityName = 'message'
                    LEFT JOIN ee_user as userLike on userLike.id = lk.UserId
                    ";
        
        if($messageId != ""){
              $request .=" AND message.Id =  " . $messageId; 
        }
        else if($ComunityId != ""){
            $request .=" Where comunity.Id =  " . $ComunityId; 
        }
        
        if($postId != ""){
            $request .=" WHERE message.Id = ".$postId; 
        }
        
        $request .=" GROUP BY comment.Id Order By message.Id Desc ";
        
        if($page == "")
        {
            $request .= " limit 0,5";
        }
        else
        {
            $request .= " limit ".($page * 5). ",5";
        }
        
        $results = $core->Db->GetArray($request);
       
        $messages = array();
        $currentMessageId = null;
        
        foreach($results as $result){
            
            if($result["messageId"] != $currentMessageId){
                $currentMessageId = $result["messageId"];
                $messages[$currentMessageId] = $result;
                $messages[$currentMessageId]["comments"] = array();
            }
            
            $messages[$currentMessageId]["comments"][] = $result["comment"];
        }
        
        
        return $messages;
        
    }
    
}

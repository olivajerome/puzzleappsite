<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Cms\Helper;

use Core\Entity\Entity\Argument;
use Core\Utility\File\File;
use Apps\Cms\Entity\CmsCms;
use Apps\Cms\Entity\CmsContainer;
use Apps\Cms\Entity\CmsBlock;
use Apps\Cms\Entity\CmsBlockItem;
use Apps\Cms\Widget\BlockForm\BlockForm;
use Apps\Cms\Widget\ContainerForm\ContainerForm;
use Apps\Cms\Widget\BlockItemForm\BlockItemForm;
use Apps\Cms\Widget\BlockImageForm\BlockImageForm;

class BlockHelper {


    public static function GetNextPosition($core, $pageId){

        $request = "Select Max(Position) as position from CmsContainer where PageId= ".$pageId;

        $result = $core->Db->GetLine($request);

        return ($result["position"] +  1);
    }

    public static function GetNextPositionBlock($core, $containerId){

        $request = "Select Max(Position) as position from CmsBlock where ParentId= ".$containerId;

        $result = $core->Db->GetLine($request);

        return ($result["position"] +  1);
    }


    /**
     * Sauvegarde un block dans un container
     * @param type $core
     * @param type $data
     */
    public static function SaveBlock($core, $data) {
        $blockForm = new BlockForm($core, $data);

        if ($blockForm->Validate()) {

            $block = new CmsBlock($core);

            if ($data["Id"]) {
                $block->GetById($data["Id"]);
            }

            $blockForm->Populate($block);
           
            
            if($data["Data"] != ""){
                $block->Content->Value = $data["Data"];
            }
            
            $block->Save();

            return $blockForm->Success(array());
        } else {
            return $blockForm->Error();
        }
    }

    public static function SaveContainer($core, $data) {
        $blockForm = new ContainerForm($core, $data);

        if ($blockForm->Validate()) {

            $block = new CmsContainer($core);
            $block->Code->Value = $data["Name"];

            if ($data["Id"]) {
                $block->GetById($data["Id"]);
            }

            $blockForm->Populate($block);
            $block->Save();

            return $blockForm->Success(array());
        } else {
            return $blockForm->Error();
        }
    }

    /*     * *
     * Update the content
     */

    public static function UpdateContentBlock($core, $data) {
        $block = new CmsBlock($core);
        $block->GetById($data["BlockId"]);
        $block->Content->Value = $data["Content"];
        $block->Save();
    }

    /*     * *
     * Delete a block
     */

    public static function RemoveBlock($core, $id) {
        $block = new CmsBlock($core);
        $block->GetById($id);
        $block->Delete();
    }

    /**
     * Sauvegarde un block dans un container
     * @param type $core
     * @param type $data
     */
    public static function SaveBlockItem($core, $data) {
        $blockItemForm = new BlockItemForm($core, $data);

        if ($blockItemForm->Validate()) {

            $blockItem = new CmsBlockItem($core);

            if ($data["Id"] != "") {
                $blockItem->GetById($data["Id"]);
            }

            $blockItemForm->Populate($blockItem);
            $blockItem->Save();

            return $blockItemForm->Success(array());
        } else {
            return $blockItemForm->Error();
        }
    }

    /*     * *
     * Saivegarde un block Image
     */

    public static function SaveBlockImage($core, $data) {

        $blockImageForm = new BlockImageForm($core, $data);

        if ($blockImageForm->Validate()) {

            $directory = "Data/Apps/Cms/";
            File::CreateArboresence($directory);

            $cmsBlock = new CmsBlock($core);
            $cmsBlock->GetById($data["BlockId"]);

            if ($data["imgSrc"] != "") {

                $cmsBlock->Content->Value = "<img src='" . $data["imgSrc"] . "' />";
            } else {
                $imgs = $data["dvUpload-uploadImageCmsfileToUpload"];

                //Sauvegarde des image Tag
                foreach (explode(";", $imgs) as $img) {

                    $originPath = $core->GetPath("/Data/Tmp/");
                    $tmpFile = str_replace($originPath, "Data/Tmp/", $img);
                    $destinationFile = str_replace("Data/Tmp/", $directory, $tmpFile);

                    rename($tmpFile, $destinationFile);
                }
                $cmsBlock->Content->Value = "<img src='" . $destinationFile . "' />";
            }

            $cmsBlock->Save();

            return $blockImageForm->Success(array());
        } else {
            return $blockImageForm->Error();
        }
    }

    /*     * *
     * Delete a iteml to a menu
     */

    public static function RemoveItemMenu($core, $itemId) {

        $item = new CmsBlockItem($core);
        $item->GetById($itemId);
        $item->Delete();
    }

    /**
     * Delete a container
     */
    public static function RemoveContainer($core, $containerId, $delete = true) {
        $cmsBlock = new CmsBlock($core);
        $blocks = $cmsBlock->Find("ParentId = " . $containerId);

        foreach ($blocks as $block) {

            $request = "DELETE FROM CmsBlockItem WHERE BlockId=" . $block->IdEntite;
            $core->Db->Execute($request);

            $block->Delete();
        }

        if ($delete) {
            $cmsContainer = new CmsContainer($core);
            $cmsContainer->GetById($containerId);
            $cmsContainer->Delete();
        }
    }
}

<div class='col-md-4'>
    <div class='block widgetDashboard'>
        <i class='fa fa-trash removeWidget' data-app='Cms' title='{{GetCode(EeApp.RemoveWidgetDahboard)}}'></i>
        <i class='fa fa-book title'>&nbsp;{{GetCode(Cms.Cms)}}</i>
            
        <ul>
            {{foreach Pages}}
                <li>  {{element->Name->Value}} 
                    <div class='picto'>
                         <a class='fa fa-link' target='_blank' href='{{GetPath(/{{element->Name->Value}} )}}'></a>
                    </div>    
                </li>
            {{/foreach Pages}}
        </ul>

    </div>
</div>

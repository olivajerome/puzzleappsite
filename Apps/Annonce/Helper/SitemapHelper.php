<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Annonce\Helper;

use Apps\Annonce\Entity\AnnonceCategory;
use Apps\Annonce\Entity\AnnonceAnnonce;

class SitemapHelper {
    /*     * *
     * Obtient le site map du blog
     */

    public static function GetSiteMap($core, $returnUrl = false) {
        $urlBase = $core->GetPath("");
        $sitemap .= "<url><loc>$urlBase/Annonce</loc></url>";
        $urls = "";
      
        $category = new AnnonceCategory($core);
        $categorys = $category->GetAll();

        foreach ($categorys as $category) {
            $sitemap .= "<url><loc>$urlBase/Annonce/Category/" . $category->Code->Value . "</loc></url>";

            $url = $urlBase . "/Annonce/Category/" . $category->Code->Value;
            $urls .= '<br/><a target="__blank" href="' . $url . '">' . $url . '</a>';
        }

        $article = new AnnonceAnnonce($core);
        $articles = $article->GetAll();

        foreach ($articles as $article) {
            $sitemap .= "<url><loc>$urlBase/Annonce/Annonce/" . $article->Code->Value . "</loc></url>";

            $url = $urlBase . "/Annonce/Annonce/" . $article->Code->Value;
            $urls .= '<br/><a target="__blank" href="' . $url . '">' . $url . '</a>';
        }

       if ($returnUrl == true) {
            return $urls;
        } else {
            return $sitemap;
        }
    }
}

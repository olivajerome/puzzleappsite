<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Core\Dashboard;

use Core\Core\Core;
use Core\Core\Request;
use Core\Control\Image\Image;
use Core\Control\Libelle\Libelle;
use Core\Control\Button\Button;


/**
 * Description of DashBoardManager
 *
 * @author jerome
 */
class DashBoardManager
{
     /**
   * Obtient le nombre de notification
   * @param type $core
   */
    public static function  GetInfoNotify($core)
    {
        $notify = new \Apps\Notify\Notify();
        return $notify->GetCount(true);
    }

    /*
     * TODO A DEPLACER DANS EEAPP
    * Load app user
    */
   public static function LoadUserApp()
   {
      $core = Core::getInstance();
      $html = "";

       if(Request::GetPost("show") == "")
       {
         $html = "<div id='lstApp'  style='overflow:auto;height:80vh'>";
       }
       //recuperation des app utilisateurs
        $apps = \Apps\EeApp\Helper\AppHelper::GetByUser($core, $core->User->IdEntite);

         if(count($apps)> 0 )
         {
             foreach($apps as $app)
             {
                 $icRemove = new Libelle("<b id='".$app->IdEntite."' class='fa fa-remove removeAppUser' >&nbsp;</b>");
                 $html .= "<div><span class='startAppUser' id='".$app->App->Value->Name->Value."' \">".$core->GetCode($app->App->Value->Name->Value.".Menu").$icRemove->Show()."</span></div>";
             }
         }
         else
         {
             $html .= $core->GetCode("EeApp.NoApp");
         }

         if(Request::GetPost("show") == "")
         {
             $html .= "</div>";
             return $html;
         }
         else
         {
             echo $html;
         }
   }

    /**
    * Obtient le nombre de notification
    * @param type $core
    */
     public static function  GetInfoMessage($core)
     {
         $emessage = self::GetApp("Message" , $core);
         return $emessage->GetNumberNotRead();
     }

     /**
   * Démarre l'application
   * */
  public function StartApp()
  {
    $parametre = explode(":", Request::GetPost("Parameter"));
    $appName = $parametre[1];
    $url = Request::GetPost("Url");
    $params = Request::GetPost("params");

    $app = new $appName(Core::getInstance());
    $app->Url = $url;

    if($params) {
      echo $app->RunMember();
    } else {
      echo $app->Run();
    }
  }

  /*
  * Get element mutlilangue of a code
  */
  public function GetCode($code)
  {
    $core = Core::getInstance();
    return $core->GetCode($code);
  }

  /*
  * Search users
  */
  public function SearchUser()
  {
    $core = Core::getInstance();
    $user = new \Core\Entity\User\User($core);
    return $user->SearchUser();
  }
  
    /**
   * Instancie et retourne l'application
   */
  public static function GetApp($appName, $core)
  {
      
    $app = new \Apps\EeApp\Entity\EeAppApp($core); 
    $app = $app->GetByName($appName); 
      
        
    if($app != null){
        $path = "\\Apps\\".$appName ."\\".$appName;
        $app = new $path($core);
        return $app;
    }
    
    return null;
  }
  
  public function LoadLanguage()
  {
      $core = Core::getInstance();
     return $core->Lang->GetAllCode($core->GetLang());
  }
  
  /***
   * Supprime une image
   */
  public function DeleteImage(){
    
    //TODO RAJOUTER UN PROTECTION SI ON EST EN ADMN OU USER 
    $core = Core::getInstance(GetEnvironnement());
     
    $imgPath = Request::GetPost("Params");
    $img = str_replace($core->GetPath("/"), "", $imgPath);

    unlink($img);
  }
  
  
  public function RequestPlugin($params){
      
      $core = Core::getInstance(GetEnvironnement());
      
      $params = json_decode($params);
      $plugin = $params->plugin;
      $method = $params->method;
      
      $path = "\Plugins\\".$plugin. "\\".$plugin;
      
      $plug = new $path($core);
      return $plug->$method($params); 
  }
}

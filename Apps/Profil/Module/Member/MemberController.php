<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Profil\Module\Member;

use Core\Control\Button\Button;
use Core\Controller\Controller;
use Core\View\View;
use Core\View\ElementView;
use Core\Entity\Entity\Argument;
use Core\Control\TabStrip\TabStrip;

use Apps\Profil\Widget\ProfilForm\ProfilForm;

/*
 * 
 */
 class MemberController extends Controller
 {
    /**
     * Constructeur
     */
    function _construct($core="")
    {
          $this->Core = $core;
    }

    function Index(){
        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);
        
        $tabProfil = new TabStrip("tabProfil"); 
        $tabProfil->AddTab($this->Core->GetCode("Profil.MyInformations"), $this->GetTabProfil());

        $skillPlugin = \Apps\EeApp\Helper\AppHelper::GetPluginType($this->Core, "Profil" , "Skill");
        
        if($skillPlugin != null){
            $tabProfil->AddTab($this->Core->GetCode("Profil.Competence"), $skillPlugin->RenderMember("Profil", $this->Core->User->IdEntite));
        }

        
        $view->AddElement(new ElementView("tabProfil", $tabProfil->Render($forum)));
        return $view->Render();
    }
    
    /***
     * Information du membre
     */
    function GetTabProfil(){
        
        $profilForm = new ProfilForm($this->Core, $this->Core->User, false);
        $profilForm->Load($this->Core->User);
        $profilForm->form->LoadImage($this->Core->User);

        return $profilForm->Render();
    }
}
<?php
namespace Apps\Folders\Helper;

use Apps\Folders\Widget\FolderForm\FolderForm;
use Apps\Folders\Entity\FolderFolder;
use Apps\Folders\Entity\FolderFolderShared;

use Apps\Folders\Widget\FileForm\FileForm;
use Apps\Folders\Entity\FolderFiles;
use Apps\Folders\Entity\FolderFileShared;
use Apps\YourStartup\Entity\YourStartupProjet;
use Core\Utility\File\File;


class FolderHelper{
    
    /***
     * Sauvegarde un dossier
     */
    public static function SaveFolder($core, $data){
         $folderForm = new FolderForm($core, $data);

        //On valide le formulaire
        if ($folderForm->Validate()) {

            $folderFolder = new FolderFolder($core);

            if (isset($data["Id"]) && $data["Id"] != "") {
                $id = $data["Id"];
                $folderFolder->GetById($data["Id"]);
            } else {
                $folderFolder->Code->Value = $data["Name"];
                $folderFolder->Url->Value = "root";
                $folderFolder->Status->Value = 1;
            }

            $folderFolder->UserId->Value = $core->User->IdEntite;

            $folderForm->Populate($folderFolder);

            //On sauvegarde
            $folderFolder->Save();

            if ($id == "") {
                $id = $core->Db->GetInsertedId();
            }

            $files = FolderHelper::GetFiles($core, true, $id);
           
            return $folderForm->Success(array("Id" => $id, 
                                               "folders" => self::GetFolders($core, true),
                                               "files" =>  $files));
        } else {
            return $folderForm->Error();
        }
    }
    
    /****
     * Supprime un dossier et les sous élements
     */
    public static function DeleteFolder($core, $folderId){
      
        $files = FolderHelper::GetFiles($core, false, $folderId);
            
        foreach($files as $file){
            FolderHelper::DeleteFile($core, $file->IdEntite);
        }
        
        $folders = FolderHelper::GetFolders($core, false, $folderId);
        
        foreach($folders as $folder){
            FolderHelper::DeleteFolder($core, $folder->IdEntite);
        }
        
        $folder = new FolderFolder($core);
        $folder->GetById($folderId);
        
        //unlink($file->Url->Value);
        $folder->Delete();
    }
    
    
    /***
     * Obtient les dossiers de l'utilisateur
     */
    public static function GetFolders($core, $toArray = false, $folderId  = ""){
        
        $folder = new FolderFolder($core);
        
        if($folderId != ""){
            $folders = $folder->Find(" ParentId = " .$folderId);
        } else{
            $folders = $folder->Find("UserId=". $core->User->IdEntite . " AND ParentId is null" );
        }
        
        
        if($toArray == false){
            return $folders;
        }
        
        $folderArray = array();
        
        foreach ($folders as $folder){
           $folderArray[] = $folder->ToArray();
        }
        
        return $folderArray;
    }
    
    /***
     * Obtient les ficheirs
     */
    public static function GetFiles($core, $toArray = false, $folderId = null){
        $file = new FolderFiles($core);
        
        if($folderId  == null){
            $files = $file->Find("UserId=". $core->User->IdEntite . " AND FolderId is null" );
        } else {
            $files = $file->Find(" FolderId = " . $folderId );
        }
        
        if($toArray == false){
            return $files;
        }
        
        $filesArray = array();
        
        foreach ($files as $file){
           $filesArray[] = $file->ToArray();
        }
        
        return $filesArray;
    }
    
    /***
     * Obtient les sous dossier et fichiers d'un repertoire
     */
    public static function GetDirectory($core, $folderId){
       
         $folderParent = new FolderFolder($core);
        $folderParent->GetById($folderId);
       
        if($folderId == ""){
        $folder = new FolderFolder($core);
        $folders = $folder->Find("UserId=". $core->User->IdEntite . " AND ParentId is null");
            
        } else{
        
        $folder = new FolderFolder($core);
        $folders = $folder->Find("ParentId=". $folderId );
        }
        $folderArray = array();
        
        foreach ($folders as $folder){
           $folderArray[] = $folder->ToArray();
        }
        
        $files = FolderHelper::GetFiles($core, true, $folderId);
        
      return json_encode(array("status" => "success", 
                                "folderId" => $folderParent->ParentId->Value , 
                                "data" => array("folders" => $folderArray, "files" => $files)));
    }
    
    /***
     * Partage un dosseir avec des utilisateurs
     */
    public static function ShareFolder($core, $folderId, $users){
        
        $usersId = explode(",", $users);
        
        foreach($usersId as $userId){
            
            if(!self::isShared($core, $folderId, $userId)){
                $folderShared = new FolderFolderShared($core);
                $folderShared->FolderId->Value = $folderId;
                $folderShared->UserId->Value = $userId;
                $folderShared->Save();
            }
        }
    }
    
    /***
     * Le dossier est il partagé avec le membre
     */
    public static function isShared($core, $folderId, $userId){
        
        $folderShared = new FolderFolderShared($core);
        $folderUser = $folderShared->Find("FolderId = " . $folderId . " AND UserId=" . $userId ) ;
        
        return (count($folderUser) > 0);
    }
    
    /***
     * Partage un fichier
     */
    public static function ShareFile($core, $fileId, $users){
        
        $usersId = explode(",", $users);
        
        foreach($usersId as $userId){
            
            if(!self::isFileShared($core, $fileId, $userId)){
                $folderShared = new FolderFileShared($core);
                $folderShared->FileId->Value = $fileId;
                $folderShared->UserId->Value = $userId;
                $folderShared->Save();
            }
        }
    }
    
     /***
     * Le fichier est il partagé avec le membre
     */
    public static function isFileShared($core, $fileId, $userId){
        
        $fileShared = new FolderFileShared($core);
        $fileUser = $fileShared->Find("FileId = " . $fileId . " AND UserId=" . $userId ) ;
        
        return (count($fileUser) > 0);
    }
    
    /***
     * Obtients les utilisateur d'un dossier
     */
    public static function GetFolderUsers($core, $folderId)
    {
        $folderShared = new FolderFolderShared($core);
        return $folderShared->Find("FolderId = " . $folderId) ;
    }
    
    /***
     * Obtients les utilisateur d'un dossier
     */
    public static function GetFileUsers($core, $fileId)
    {
        $fileShared = new FolderFileShared($core);
        return $fileShared->Find("FileId = " . $fileId) ;
    }
    
    
    /***
     * Supprime un utilisateur d'un repertoire
     */
    public static function DeleteUserFolder($core, $userFolderId){
        $folderShared = new FolderFolderShared($core);
        $folderShared->GetById($userFolderId);
        $folderShared->Delete();
    }
    
    
    /***
     * Supprime un utilisateur d'un repertoire
     */
    public static function DeleteUserFile($core, $userFolderId){
        $folderShared = new FolderFileShared($core);
        $folderShared->GetById($userFolderId);
        $folderShared->Delete();
    }
    
    /***
     * Sauvegarde un fichier
     */
    public static function SaveFile($core, $data){
        $fileForm = new FolderForm($core, $data);

        //On valide le formulaire
        if ($fileForm->Validate()) {

            $folderFile = new FolderFiles($core);

            if (isset($data["Id"]) && $data["Id"] != "") {
                $id = $data["Id"];
                $folderFile->GetById($data["Id"]);
            }

            $folderFile->UserId->Value = $core->User->IdEntite;

            $fileForm->Populate($folderFile);

            if($data["dvUpload-UploadfileToUpload"] != ""){
                
                $document = $data["dvUpload-UploadfileToUpload"];
                $userDirectory = "Data/Apps/Folders/" . $core->User->IdEntite;
               
                File::CreateDirectory($userDirectory);
                
                $source = str_replace("DOCUMENT:", "", $document);
                $destination = str_replace("Data/Tmp/Folders", $userDirectory, $source);
               
                rename($source, $destination);
            }
            
            //TODO METTRE ME REPERTOIRE COURANT
            //$folderFile->FolderId->Value 
            $folderFile->Url->Value = $destination;
            
             //On sauvegarde
            $folderFile->Save();

            if ($id == "") {
                $id = $core->Db->GetInsertedId();
            }
            
            return $fileForm->Success(array("Id" => $id));
        } else {
            return $fileForm->Error();
        }
    }
    
    /***
     * Supprime un fichier
     */
    public static function DeleteFile($core, $fileId){
        
        $sharedFiles = new FolderFileShared($core);
        $shared = $sharedFiles->Find("FileId = " . $fileId);
        
        foreach($shared as $share){
            $share->Delete();
        }
        
        $file = new FolderFiles($core);
        $file->GetById($fileId);
        
        unlink($file->Url->Value);
        $file->Delete();
    } 
    
    /***
     * Obtient les dossiers et fichiers partagés avec moi
     */
    public static function SharedWithMe($core, $folderId){
        
        $sharedFolder = new FolderFolderShared($core);
        $folders = $sharedFolder->Find("UserId = " . $core->User->IdEntite);
        
        $sharedFiles = new FolderFileShared($core);
        $files = $sharedFiles->Find("UserId = " . $core->User->IdEntite);
        
        $tabFolder = array();
        $tabFiles = array();
            
        foreach($folders as $folder){
            $tabFolder[] = $folder->Folder->Value->ToArray();
        }
    
        foreach($files as $file){
            $tabFiles[] = $file->File->Value->ToArray();
        }
        
        return json_encode(array("folders" => $tabFolder, "files"=> $tabFiles));
        
    }
    
}


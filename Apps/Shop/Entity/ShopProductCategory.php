<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Shop\Entity;

use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class ShopProductCategory extends Entity {

    //Constructeur
    function __construct($core) {
        //Version
        $this->Version = "2.0.0.0";

        //Nom de la table 
        $this->Core = $core;
        $this->TableName = "ShopProductCategory";
        $this->Alias = "ShopProductCategory";

        $this->ProductId = new Property("ProductId", "ProductId", NUMERICBOX, true, $this->Alias);
        $this->CategoryId = new Property("CategoryId", "CategoryId", NUMERICBOX, true, $this->Alias);

        //Creation de l entité 
        $this->Create();
    }
}

?>
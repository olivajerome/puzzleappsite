var RenderAnimation = function(){};

/***
 * Init the form
 * @returns {undefined}
 */
RenderAnimation.Init = function(){
    Event.AddById("btnSave", "click", RenderAnimation.SaveElement);
};

/***
* Get the Id of element 
*/
RenderAnimation.GetId = function(){
    return Form.GetId("RenderAnimationForm");
};

/*
* Save the Element
*/
RenderAnimation.SaveElement = function(e){
   
    if(Form.IsValid("RenderAnimationForm"))
    {

    let data = "Class=RenderAnimation&Methode=SaveElement&App=RenderAnimation";
        data +=  Form.Serialize("RenderAnimationForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.status == "error"){
                Form.RenderError("RenderAnimationForm", data.data.message);
            } else{

                Form.SetId("RenderAnimationForm", data.data.Id);
            }
        });
    }
};


<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


namespace Apps\ParlonsEn\Module\Message;

use Core\Control\Button\Button;

use Core\Controller\Controller;
use Core\View\View;
use Core\View\ElementView;
use Core\Control\Upload\Upload;
use Core\Entity\Entity\Argument;

use Apps\Discuss\Entity\DiscussDiscuss;
use Apps\Discuss\Helper\DiscussHelper;

use Apps\Message\Helper\MessageHelper;
use Apps\Coopere\Entity\CoopereReseau;
use Apps\Coopere\Coopere;


use Apps\ParlonsEn\Widget\UserProjetWidget\UserProjetWidget;
use Apps\ParlonsEn\Widget\ActionProjetWidget\ActionProjetWidget;
use Apps\ParlonsEn\Widget\MemberProjetWidget\MemberProjetWidget;
use Core\Entity\User\User;

use Apps\Discuss\Module\Home\HomeController;


 class MessageController extends Controller
 {
     /***
      * Pop de discussion avec le membre
      * On retrouve les messages précedent pour continuer la conversation
      * Cela doit être des conversation simple et non multiple
      */
     function Index($userId)
     {
         $view = new View(__DIR__."/View/index.tpl", $this->Core);
       
         
         $member = new User($this->Core);
         $member->GetById($userId);
         
        $lastDiscusId = DiscussHelper::FindConversation($this->Core, $this->Core->User->IdEntite,$userId );
         
         $view->AddElement(new ElementView("memberImage", $member->GetImageMini()));
         $view->AddElement(new ElementView("memberPseudo", $member->getPseudo()));
         $homeController = new HomeController($this->Core);
        
         if($lastDiscusId != "")
         {
             $view->AddElement(new ElementView("Messages" , $homeController->LoadDiscuss($lastDiscusId, false) ));
         }
         else
         {
             $view->AddElement(new ElementView("Messages" , "<input type='hidden' id='hdTchatId'><input type='hidden' id='hdMembreId' value='".$userId."' > Ceci est le début de votre conversation" ));
         }
         
         
         return $view->Render();
     }
     
 }
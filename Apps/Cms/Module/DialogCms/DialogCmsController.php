<?php 
/**
 * Module d'accueil
 * */
namespace Apps\Cms\Module\DIalogCms;

use Core\Control\Button\Button;
use Core\Controller\AdministratorController;
use Core\View\View;
use Apps\Cms\Widget\PageForm\PageForm; 
use Apps\Cms\Widget\BlockForm\BlockForm; 
use Apps\Cms\Widget\BlockItemForm\BlockItemForm; 
use Apps\Cms\Widget\BlockImageForm\BlockImageForm; 
use Apps\Cms\Widget\ContainerForm\ContainerForm;
use Apps\Cms\Widget\ImportForm\ImportForm;

use Apps\Cms\Helper\BlockHelper;

 class DialogCmsController extends AdministratorController
 {
    /***
     * Dialogue d'ajout de page
     * */    
    function ShowAddPage($params){
        $pageForm = new PageForm($this->Core);
        
        $page = new \Apps\Cms\Entity\CmsPage($this->Core);
        if($params != ""){
            $page->GetById($params);
        }
        
        $pageForm->Load($page);
        
        return $pageForm->Render();
    }
    
    /***
     * Ajout d'un block
     */
    function ShowAddBlock($params, $parentId = null){
        
        $blockForm = new BlockForm($this->Core);
        $block = new \Apps\Cms\Entity\CmsBlock($this->Core);
        
        if($params =="header"){
            $data["ParentId"] = 1;
            $block->ParentId->Value = $data["ParentId"];
            $block->Position->Value = BlockHelper::GetNextPositionBlock($this->Core, $data["ParentId"]);
        }
        else if($params =="footer"){
            $data["ParentId"] = 3;
            $block->ParentId->Value = $data["ParentId"];
             $block->Position->Value = BlockHelper::GetNextPositionBlock($this->Core, $data["ParentId"]);
        }
        else if($params == null){
            $data["ParentId"] = $parentId;
            $block->ParentId->Value = $data["ParentId"];
            $block->Position->Value = BlockHelper::GetNextPositionBlock($this->Core, $parentId);
        }
        else {
            $block->GetById($params);
           
            if($block->TypeId->Value == \Apps\Cms\Entity\CmsBlock::TYPE_WIDGET){
                $block->Data = $block->Content;
            }
        }
    
        
        $blockForm->Load($block);
        return $blockForm->Render();
    }

    /**
     * Ajotu d'un block sur le container d'une page
     */
    function ShowAddBlockContainer($params){
        return $this->ShowAddBlock(null, $params);
    }

    /***
     * Add Item to a menu
     */
    function AddItemMenu($params){
        $blockItemForm = new BlockItemForm($this->Core, $data);
        $blockItem = new \Apps\Cms\Entity\CmsBlockItem($this->Core);
        $blockItem->BlockId->Value = $params;
       
        $blockItemForm->Load($blockItem);
        
        return $blockItemForm->Render();     
    }

    /***
     * Edit a item Menu
     */
    function EditItemMenu($params){
        $blockItemForm = new BlockItemForm($this->Core, $data);
        $blockItem = new \Apps\Cms\Entity\CmsBlockItem($this->Core);
        $blockItem->GetById($params);
       
        $blockItemForm->Load($blockItem);
        
        return $blockItemForm->Render();     
    }

    /***
     * Module de selection/Ajout d'image
     */
    function EditImage($params){
        $data["Id"] = $params;
        $blockImageForm = new BlockImageForm($this->Core, $data);
        
        return $blockImageForm->Render();  
    }

    /***
     * Module de selection/Ajout d'image
     */
    function AddImage($params){
        $data["Id"] = $params;
        $blockImageForm = new BlockImageForm($this->Core, $data);
        return $blockImageForm->Render();  
    }

    /***
     * Ajout d'une container à une page
     */
    public function ShowAddContainer($params){
        
        $containerForm = new ContainerForm($core);
        $container = new \Apps\Cms\Entity\CmsContainer($this->Core);
        $container->PageId->Value = $params;
        $container->Position->Value = BlockHelper::GetNextPosition($this->Core,$params);

        $containerForm->Load($container);

        return $containerForm->Render();
    }

    /***
     * Edit a container
     */
    public function EditContainer($params){
        
        $containerForm = new ContainerForm($this->Core);
        $container = new \Apps\Cms\Entity\CmsContainer($this->Core);
        $container->GetById($params);
       
        $containerForm->Load($container);

        return $containerForm->Render();
    }

    /***
     * Import Form
     */
    public function ShowImportPage(){

        $importForm = new ImportForm($this->Core);
        return $importForm->Render();
    }
 }
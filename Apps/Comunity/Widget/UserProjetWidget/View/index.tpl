<div id='userProjetWidget'>
    <h1>Projets</h1>
    <ul class='tabState'>
        <li id='1' class='stateProjet {{actif}}'>Actifs</li>
        <li id='0' class='stateProjet {{create}}' >En création</li>
        <li id='10' class='stateProjet {{close}} ' >Terminés</li>
    </ul>
    <div id='lstProjet'>
      
        <div id='allProjet'>
            <h3>Mes projets</h3>
            <ul>
                {{foreach userProjet}}
               
                <li class='linkProjet' id='{{element->IdEntite}}' >
                
                <div class="avatarmini-img avatarmini" style="cursor: pointer;"> 
                    <img src="{{element->GetFirstImageUrl()}}" alt="images"> 
                </div>
               
                {{element->Title->Value}}
                 </li>
            
                {{/foreach userProjet}}
            </ul>
        </div>   
        
        <div id='folowProjet'>
            <h3>Les projets que je suis</h3>
            <ul>
                {{foreach followProjet}}
               
                <li class='linkProjet' id='{{element->IdEntite}}' >
                
                <div class="avatarmini-img avatarmini" style="cursor: pointer;"> 
                    <img src="{{element->GetFirstImageUrl()}}" alt="images"> 
                </div>
               
                {{element->Title->Value}}
                 </li>
            
                {{/foreach followProjet}}
            </ul>
        </div>  
            
        <div id='allProjet'>
            <h3>Tous les projets</h3>
            <ul>
                {{foreach allProjet}}
               
                <li class='linkProjet' id='{{element->IdEntite}}' >
                
                <div class="avatarmini-img avatarmini" style="cursor: pointer;"> 
                    <img src="{{element->GetFirstImageUrl()}}" alt="images"> 
                </div>
               
                {{element->Title->Value}}
                 </li>
            
                {{/foreach allProjet}}
            </ul>
        </div>    
    </div>
</div>


<script>
    UserProjetWidget.Init();
</script>
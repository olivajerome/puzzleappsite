var Shop = function () {};

/*
 * Chargement de l'application
 */
Shop.Load = function (parameter)
{
    this.LoadEvent();
};

/***
 * Start the Event for Member 
 */
Shop.LoadMember = function(){
};

/*
 * Chargement des �venements
 */
Shop.LoadEvent = function ()
{
    Dashboard.AddEventAppMenu(Shop.Execute, "", "Shop");
    Dashboard.AddEventWindowsTool("Shop");

    ShopForm.Init();
    Shop.InitTabCategory();
    Shop.InitTabCaracteristique();
    Shop.InitTabProduct();
    Shop.InitTabCommand();
};

/**
 * Tab des catgéogrie
 * @returns {undefined}
 */
Shop.InitTabCategory = function () {
    //Catégory
    Event.AddById("btnAddCategory", "click", () => {
        Shop.ShowAddCategory("")
    });
    EntityGrid.Initialise('gdCategory');
};


/**
 * Dialogue d'ajout de catégorie
 */
Shop.ShowAddCategory = function (categoryId) {

    Dialog.open('', {"title": Dashboard.GetCode("Shop.AddCategorie"),
        "app": "Shop",
        "class": "DialogAdminShop",
        "method": "AddCategorie",
        "type": "left",
        "params": categoryId
    });
};

/****
 * Edite une catégorie
 * @param {type} categoryId
 * @returns {undefined}
 */
Shop.EditCategory = function (categoryId) {
    Shop.ShowAddCategory(categoryId);
};

/***
 * Supprime une catégorie
 * @param {type} categoryId
 * @returns {undefined}
 */
Shop.DeleteCategory = function (categoryId) {

    Animation.Confirm(Language.GetCode("Shop.ConfirmRemoveCategorie"), () => {

        let data = "Class=Shop&Methode=DeleteCategory&App=Shop";
        data += "&categoryId=" + categoryId;

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            Shop.ReloadCategory(data);
        });

    });
};

/***
 * Rafraichit la tab des catégorie
 * @returns {undefined}
 */
Shop.ReloadCategory = function (data) {
    let gdCategory = Dom.GetById("gdCategory");
    gdCategory.parentNode.innerHTML = data.data;
    Shop.InitTabCategory();
};

/***
 * Rafraichit le tab des Produit
 */
Shop.ReloadProduct = function (data) {
    let gdProduct = Dom.GetById("gdProduct");
    gdProduct.parentNode.innerHTML = data;
    Shop.InitTabProduct();
};

/**
 * Tab des caracteristique
 * @returns {undefined}
 */
Shop.InitTabCaracteristique = function () {
    //Catégory
    Event.AddById("btnAddCaracteristique", "click", () => {
        Shop.ShowAddCaracteristique("")
    });
    EntityGrid.Initialise('gdCaracteristique');
};

/**
 * Dialogue d'ajout de caractéristique
 */
Shop.ShowAddCaracteristique = function (caracteristiqueId) {

    Dialog.open('', {"title": Dashboard.GetCode("Shop.AddCaracteristique"),
        "app": "Shop",
        "class": "DialogAdminShop",
        "method": "AddCaracteristique",
        "type": "left",
        "params": caracteristiqueId
    });
};

/****
 * Edite une caracteristique
 * @param {type} caracteristiqueId
 * @returns {undefined}
 */
Shop.EditCaracteristique = function (caracteristiqueId) {
    Shop.ShowAddCaracteristique(caracteristiqueId);
};


/***
 * Supprime une caractéristique
 * @param {type} categoryId
 * @returns {undefined}
 */
Shop.DeleteCaracteristique = function (caracteristiqueId) {

    Animation.Confirm(Language.GetCode("Shop.ConfirmRemoveCaracteristique"), () => {

        let data = "Class=Shop&Methode=DeleteCaracteristique&App=Shop";
        data += "&caracteristiqueId=" + caracteristiqueId;

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            Shop.ReloadCaracteristique(data);
        });
    });
};


/***
 * Rafraichit la tab des caractéristiques
 * @returns {undefined}
 */
Shop.ReloadCaracteristique = function (data) {
    let gdCaracteristique = Dom.GetById("gdCaracteristique");
    gdCaracteristique.parentNode.innerHTML = data.data;
    Shop.InitTabCaracteristique();
};

/**
 * Init event catégorie 
 */
Shop.InitTabProduct = function () {
    //Catégory
    Event.AddById("btnAddProduct", "click", () => {
        Shop.ShowAddProduct("")
    });
    EntityGrid.Initialise('gdProduct');
};

/****
 * Edite un produit
 * @param {type} productId
 * @returns {undefined}
 */
Shop.EditProduct = function (productId) {
    Shop.ShowAddProduct(productId);
};

/**
 * Edit a product
 */
Shop.ShowAddProduct = function (productId) {
    Dialog.open('', {"title": Dashboard.GetCode("Shop.AddProduct"),
        "app": "Shop",
        "class": "DialogAdminShop",
        "method": "AddProduct",
        "params": productId
    });
};

/***
 * Tab des commandes
 */
Shop.InitTabCommand = function () {
    //Command
    EntityGrid.Initialise('gdCommand');
};

/**
 * Edit a command
 */
Shop.EditCommand = function(commandId){
    
    Dialog.open('', {"title": Dashboard.GetCode("Shop.EditCommand"),
    "app": "Shop",
    "class": "DialogAdminShop",
    "method": "EditCommand",
    "params": commandId,
    });
};

/*
 * Execute une fonction
 */
Shop.Execute = function (e)
{
    //Appel de la fonction
    Dashboard.Execute(this, e, "Shop");
    return false;
};

/***
 * Refresh the plugin
 * @returns {undefined}
 */
Shop.RefreshPlugin =function(){
    Dashboard.StartApp('', 'Shop', '')
};

/*
 *	Affichage de commentaire
 */
Shop.Comment = function ()
{
    Dashboard.Comment("Shop", "1");
};

/*
 *	Affichage de a propos
 */
Shop.About = function ()
{
    Dashboard.About("Shop");
};

/*
 *	Affichage de l'aide
 */
Shop.Help = function ()
{
    Dashboard.OpenBrowser("Shop", "{$BaseUrl}/Help-App-Shop.html");
};

/*
 *	Affichage de report de bug
 */
Shop.ReportBug = function ()
{
    Dashboard.ReportBug("Shop");
};

/*
 * Fermeture
 */
Shop.Quit = function ()
{
    Dashboard.CloseApp("", "Shop");
};

/***
 * Init the Shop App for current Page 
 */
Shop.Init = function(app, method){

    CardWidget.Init();

    if(app == "Shop"){
        switch (method){
            case "Product":
                Shop.Product();
                break; 
            case "Cart":
                Shop.Cart();
            break; 
            default :
                console.log("Page de base");
            break;   
        }
    }
};

/***
 * Init Admin Widget
 * @returns {undefined}
 */
Shop.InitAdminWidget = function(){
    
};

/***
 * Init the Page Product
 */
Shop.Product = function(){
    CardWidget.InitAddToCart();
    Animation.AddViewer("productImages")
};

/**
 * Init the Card Page 
 */
Shop.Cart = function(){
    CommandForm.Init();
};
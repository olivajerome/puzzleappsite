class H1 extends BaseTool {
    getIcone() {
        return "H1";
    }

    getName() {
        return Language.GetCode("Base.TextRichEditorH1");
    }
    execute(e) {
        e.preventDefault();
        this.applyStyle("h1");
        super.execute();
    }
}

class H2 extends BaseTool {
    getIcone() {
        return "H2";
    }

    getName() {
        return Language.GetCode("Base.TextRichEditorH2");
    }
    execute(e) {
        e.preventDefault();
        this.applyStyle("h2");
        super.execute();
    }
}

class H3 extends BaseTool {
    getIcone() {
        return "H3";
    }

    getName() {
        return Language.GetCode("Base.TextRichEditorH3");
    }
    execute(e) {
        e.preventDefault();
        this.applyStyle("h3");
        super.execute();
    }
}
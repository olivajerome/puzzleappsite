<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Downloader\Module\Front;

use Apps\Downloader\Entity\DownloaderRessource;
use Apps\Downloader\Entity\DownloaderRessourceContact;
use Core\Controller\Controller;
use Core\Core\Core;
use Core\Core\Request;
use Core\View\View;
use Core\View\ElementView;


class FrontController extends Controller
{
    /*
     * DownLoad à file
     */
    public function DownLoad($params)
    {
        //Find the Ressource
        $ressource = new DownloaderRessource($this->Core);
        $ressource = $ressource->GetByCode($params);
        
        if($ressource != false)
        {
            if(1 == 1 /*$this->Core->isConnected() || Request::GetPost("email") || Request::GetSession("downloader.email")*/)
            {
                //Save the stat Upload
                $contact = new DownloaderRessourceContact($this->Core);
                
                if($this->Core->isConnected())
                {
                    $contact->UserId->Value = $this->Core->User->IdEntite;
                }
                else if(Request::GetSession("downloader.email"))
                {
                    $contact->Email->Value = Request::GetSession("downloader.email");
                }
                else
                {
                    $contact->Email->Value = $_SERVER['REMOTE_ADDR'] ;//Request::GetPost("email"); 
                    Request::SetSession("downloader.email",  $_SERVER['REMOTE_ADDR']);
                }
                $contact->RessourceId->Value = $ressource->IdEntite;
                $contact->Save();
   
                
                //Update DownloaderRessource set Url = "/Data/Apps/Downloader/1/Core.zip" Where Name = "Core";
                // "Ressources => "  . $ressource->Url->Value;

                header("Content-type:application/octet-stream");
                header("Content-disposition:attachment;filename=".$params.".zip");

               readfile($ressource->GetDocPath());
               
                //header("location:". $this->Core->GetPath("/".$ressource->GetPath()));
                ob_flush();
                exit;
            }
            else
            {
               $view = new View(__DIR__."/View/notConnected.tpl", $this->Core);
               $view->AddElement($ressource);

               return $view->Render();
            }
        }
        else
        {
            $view = new View(__DIR__."/View/unknowRessource.tpl", $this->Core);

            return $view->Render();
        }
    }
    
    /***
     * Documentation 
     */
    function Documentation($ressource){
        
        $view = new View(__DIR__."/View/documentation.tpl", $this->Core);
        
        $view->AddElement(new ElementView("Ressource", $ressource));
        
        return $view->Render();
    }
    
}

<div class='col-md-10 centerBlock'>
    <h1>{{GetCode(RoadMap.YourRoadMap)}}</h1>

    <div class='row'>
    {{foreach MoocUser}}
        <div class='mooc col-md-2' id='{{element->IdEntite}}'> 
            <h3>{{element->MoocName->Value}}</h3>
            
            {{element->MoocDescription->Value}}
            
            <div style='text-align: center'>
                <a target='_blank' class='btn btn-primary' href ='{{GetPath(/Mooc/Mooc/{{element->MoocCode->Value}})}}'>
                {{GetCode(Mooc.ResumeTheTutoriel)}}
                </a>
                <br/>
                <button class='btn btn-dark unFollowMooc' href=''>{{GetCode(Mooc.UnFollowTheTutoriel)}}</button>
            </div>
         </div>
    {{/foreach MoocUser}}
    </div><!-- comment -->
</div>
<?php

/*
 *  PuzzleApp
 *  Webemyos
 * Jérôme Oliva
 *
 */

namespace Apps\Tips\Module\DialogTips;

use Core\Controller\Controller;
use Core\Core\Request;
use Core\View\ElementView;
use Core\View\View;
use Apps\Tips\Helper\TipsHelper;
use Core\Dashboard\DashBoardManager;

class DialogTipsController extends Controller {

    /**
     * Constructeur
     */
    function __construct($core = "") {
        $this->Core = $core;
    }

    /*     * *
     * Affiche le dernier tips important pour l'utilisateur
     */

    function ShowTips($code) {

        if ($code  != "last") {
            $tips = TipsHelper::Get($this->Core, $code);
        } else {
            $tips = TipsHelper::GetLastFoUser($this->Core);

            if($tips != null){
                //On lui a montrer on en revient pas dessus
                TipsHelper::SetShowForUser($this->Core, $tips->IdEntite);
            }
        }

        return $tips->Description->Value;
    }
}

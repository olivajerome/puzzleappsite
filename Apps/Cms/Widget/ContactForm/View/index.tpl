<div>

{{form->Open()}}
{{form->Error()}}
{{form->Success()}}

{{form->Render(Id)}}

    <div>
        <label>{{GetCode(Cms.YourName)}}</label> 
        {{form->Render(Name)}}
    </div>
    
    <div>
        <label>{{GetCode(Cms.YourEmail)}}</label> 
        {{form->Render(Email)}}
    </div>

    
    <div>
        <label>{{GetCode(Cms.YourMessage)}}</label> 
        {{form->Render(Message)}}
    </div>
    <div class='center marginTop' >   
        {{form->Render(btnSend)}}
    </div>  

{{form->Close()}}

</div>

<script>
    ContactForm.Init();
</script>
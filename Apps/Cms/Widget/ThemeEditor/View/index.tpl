        <table class="table">
            <thead>
                <tr>
                  <th>{{GetCode(Name)}}</th>
                  <th>{{GetCode(Description)}}</th>
                  <th>{{GetCode(Actif)}}</th>
                </tr>
            </thead>
            <tbody>
                {{foreach}}
                    <tr>
                        <td>{{element->Name->Value}}</td>
                        <td>{{element->Description->Value}}</td>
                        <td>{{element->GetActifPicto()}}</td>
                        <td>
                            <input type='button' class='btn btn-success' onclick='EeAppAction.DefineTemplateActif({{element->IdEntite}}, this)' value='{{GetCode(EeApp.DefineTemplateActif)}}' />
                        </td>
                        </td>
                    </tr>
                {{/foreach}}            
                </tbody>
            </table>
        </div>

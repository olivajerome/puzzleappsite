<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Ide\Module\Helper;

use Apps\Ide\Helper\HelperHelper;
use Core\Action\AjaxAction\AjaxAction;
use Core\Block\Block;
use Core\Control\Button\Button;
use Core\Control\TextBox\TextBox;
use Core\Controller\AdministratorController;
use Core\Core\Request;

class HelperController extends AdministratorController{

    /**
     * Crée le nouvel helper
     */
    function AddHelper($data) {

        //Creation du projet
        if (HelperHelper::CreateHelper($this->Core, $data["Projet"], $data["Name"])) {
            return true;
        } else {
            return $this->Core->GetCode("Ide.HelperExist");
        }
    }
}

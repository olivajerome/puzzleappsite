<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Message\Module\DialogMessage;

use Core\Control\Button\Button;
use Core\Control\Upload\Upload;
use Core\Controller\Controller;
use Core\View\View;
use Core\View\ElementView;
use Core\Entity\User\User;
use Apps\Message\Module\Member\MemberController;
use Apps\Message\Helper\MessageHelper;

/*
 * 
 */

class DialogMessageController extends Controller {

    /**
     * Constructeur
     */
    function _construct($core = "") {
        $this->Core = $core;
    }

    /**
     * Create
     */
    function Create() {
        
    }

    /**
     * Init
     */
    function Init() {
        
    }

    /**
     * Render
     */
    function Show($all = true) {
        return $this->Index();
    }

    /*
     * Get the home page
     */

    function Index() {
        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);
        return $view->Render();
    }

    /*     * *
     * Search converstion between two user 
     */

    function OpenConversation($userId) {
        $view = new View(__DIR__ . "/View/conversation.tpl", $this->Core);

        $member = new User($this->Core);
        $member->GetById($userId);

        $lastDiscusId = MessageHelper::FindConversation($this->Core, $this->Core->User->IdEntite, $userId);

        $view->AddElement(new ElementView("memberImage", $member->GetImageMini()));
        $view->AddElement(new ElementView("memberPseudo", $member->getPseudo()));
        $view->AddElement(new ElementView("MessageId", $lastDiscusId));
        $view->AddElement(new ElementView("UserId", $userId));

        //Upload Image
        $uploadMessage = new Upload("Message", "uploadMessage", "MessageAction.SendAttachment()", "uploadMessage");
        $uploadMessage->RenderType = "Icone";
        $uploadMessage->Accept = "";
        $view->AddElement(new ElementView("uploadMessage", $uploadMessage->Show()));

        $memberController = new MemberController($this->Core);

        if ($lastDiscusId != "") {
            $view->AddElement(new ElementView("Messages", $memberController->LoadMessage($lastDiscusId, false)));
        } else {
            $view->AddElement(new ElementView("Messages", "<input type='hidden' id='hdTchatId'><input type='hidden' id='hdMembreId' value='" . $userId . "' > " . $this->Core->GetCode("Message.NewConversation")));
        }


        return $view->Render();
    }

    /* action */
}

?>
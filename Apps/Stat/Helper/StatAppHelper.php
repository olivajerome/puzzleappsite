<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */


 namespace Apps\Stat\Helper;
 use Apps\Stat\Entity\StatApp;
 use Core\Utility\Date\Date;
 
class StatAppHelper
{
    
    public static function AddStat($core, $code, $user ){
        
        $statApp = new StatApp($core);
        $statApp->Code->Value =  $code;
        $statApp->User->Value = $user;
        $statApp->Date->Value = Date::Now(true);
        $statApp->Save();
    }
}
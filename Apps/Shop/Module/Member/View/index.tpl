<div class='centerBlock' style='padding-top:0px'>
    <h1>{{GetCode(Shop.YourCommand)}}</h1>

    {{foreach Commands}}
        <div class='col-md-12 command' >
            <div class='headerCommand'>
                <div class='col-md-4'>
                    <label>{{GetCode(Shop.DateCreated)}} :</label>
                    
                     {{element->DateCreated->Value}}
                </div>    
                <div class='col-md-4'>
                    <label>{{GetCode(Shop.DateClotured)}} : </label>
                    {{element->DateClotured->Value}}
                </div>
            </div>
            <div class='bodyCommand'>
                <div class='col-md-6'>
                    {{Apps\Shop\Decorator\CommandDecorator->GetProducts(element)}}
                </div>
                <div class='col-md-6'>
                    {{Apps\Shop\Decorator\CommandDecorator->GetAddress(element)}}
                </div>
            </div>
        </div>
    {{/foreach Commands}}
</div>
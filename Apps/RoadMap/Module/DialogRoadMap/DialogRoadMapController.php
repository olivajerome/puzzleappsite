<?php

/*
 * PuzzleApp
 * Webemyos
 * J�r�me Oliva
 * GNU Licence
 */

namespace Apps\RoadMap\Module\DialogRoadMap;

use Core\Control\Button\Button;
use Core\Controller\Controller;
use Core\View\View;

use Apps\RoadMap\Widget\RoadMapForm\RoadMapForm;
use Apps\RoadMap\Widget\ColumnForm\ColumnForm;
use Apps\RoadMap\Widget\TicketForm\TicketForm;

use Apps\RoadMap\Entity\RoadMapRoadMap;
use Apps\RoadMap\Entity\RoadMapColumn;
use Apps\RoadMap\Entity\RoadMapTicket;

use Core\Core\Request;


/*
 * 
 */
 class DialogRoadMapController extends Controller
 {
    /**
     * Constructeur
     */
    function _construct($core="")
    {
          $this->Core = $core;
    }

    /**
     * Creation
     */
    function Create()
    {
    }

    /**
     * Initialisation
     */
    function Init()
    {
    }

    /**
     * Affichage du module
     */
    function Show($all=true)
    {
         return $this->Index();
    }
    
    /*
    * Get the home page
    */
   function Index()
   {
       $view = new View(__DIR__."/View/index.tpl", $this->Core);
       return $view->Render();
   }
   
   
   /***
    * Dialoge d'ajourt d'une road masp
    **/
   function ShowAddRoadMap($params){
        $roadMapForm = new RoadMapForm($this->Core);

        if ($params != "") {
            $RoadMap = new RoadMapRoadMap($this->Core);
            $RoadMap->GetById($params);
            $roadMapForm->Load($RoadMap);
        }

        return $roadMapForm->Render($params);
   }
   
   /***
    * Dialogue d'ajout de colonne
    **/
   function ShowAddColumn($params){
   
        $columnForm = new ColumnForm($this->Core, Request::GetPost("RoadMapId"));
        $columnForm->Init(Request::GetPost());
    
       if (strstr($params , "columnId") !== false) {
            
            $params = explode("=", $params);
            $column = new RoadMapColumn($this->Core);
            $column->GetById($params[1]);
            
            $column->RoadMapId->Value =  Request::GetPost("roadMapId");
            $columnForm->Load($column);
        }

        return $columnForm->Render($params);
   } 
   
   
    /***
    * Dialogue d'ajout de colonne
    **/
   function ShowAddTicket($params){
   
        $ticketForm = new TicketForm($this->Core, Request::GetPost("columnId"));
        $ticketForm->Init(Request::GetPost());

        if ($params != "" && $params != "new") {
            $ticket = new RoadMapTicket($this->Core);
            $ticket->GetById($params);
            
            //$ticket->ColumnId->Value =  Request::GetPost("columnId");
            $ticketForm->Load($ticket);
        }

        return $ticketForm->Render($params);
   } 
   
   
          
          /*action*/
 }?>
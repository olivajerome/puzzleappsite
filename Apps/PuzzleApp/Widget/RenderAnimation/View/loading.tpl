<section>
    
    <div id='container' style='border:1px solid grey; padding:5px; border-radius:8px'>
  
        <p>Is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
        </p>
        <div id="loading"></div>
    </div>
    
    <div class='center marginTop'>
        
    {{GetControl(Button,btnHide,{LangValue=AddLoading,CssClass=btn btn-primary,Id=btnAddLoading,)})}}
    {{GetControl(Button,btnShow,{LangValue=RemoveLoading,CssClass=btn btn-secondary,Id=btnRemoveLoading,)})}}
    </div>
    
</section>

<script>
    
    Event.AddById("btnAddLoading", "click", ()=>{
    
        Animation.AddLoading("loading");
    });
    
    Event.AddById("btnRemoveLoading", "click", ()=>{
    
        Animation.RemoveLoading("loading");
    });
    
</script>

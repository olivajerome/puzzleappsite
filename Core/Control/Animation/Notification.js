function Notification()
{
    this.Notify = function (message, control, callBackYes, callBackNo)
    {
        var popupNotify = document.getElementById("popupNotify");

        if (dvMessage == undefined)
        {
            var dvMessage = document.createElement("div");
            dvMessage.id = "popupNotify";
            dvMessage.className = "notify";
            dvMessage.style.zIndex = 99999;
            dvMessage.style.position = "fixed";

            if (control != undefined)
            {

                var element = document.getElementById(control);
                var position = element.getBoundingClientRect();

                dvMessage.style.top = (position.top - 50) + "px";
                dvMessage.style.left = (position.left + 150) + "px";
                dvMessage.style.padding = "15px";
            } else
            {
                dvMessage.style.right = "0";
                dvMessage.style.left = "0";
                dvMessage.style.bottom = "100px";
                dvMessage.style.position = "fixed";
                dvMessage.style.margin = "auto";
                dvMessage.style.width = "fit-content";
            }

            document.body.appendChild(dvMessage);
        } else
        {
            dvMessage.style.display = "block";
        }

        dvMessage.innerHTML = "<p style='text-align:center;padding-top:8px;padding-bottom:8px;margin:0px 10px 0px 10px;' class='extrabold upper'>" + message + "</p>";

        if (this.Type == "Confirm") {

            var actionButtons = "<div class='actionButtons'>";
            actionButtons += "<button class='btn btn-primary' id='btnNotifyYes'>" + Language.GetCode('Yes') + "</button>";
            actionButtons += "<button class='btn btn-danger' id='btnNotifyNo' >" + Language.GetCode('No') + "</button>";
            actionButtons += "</div>";

            dvMessage.innerHTML += actionButtons;

            Event.AddById('btnNotifyYes', "click", function () {
                callBackYes();
                Notification.Close();
            });
            Event.AddById('btnNotifyNo', 'click', function(){ 
                
                if(callBackNo){
                    callBackNo();
                }

                Notification.Close();
            });
        } 
        
        else if (this.Type == "Ask") {

            dvMessage.innerHTML += "<input type='text' id='tbNotifyInput'  class='form-control'>";
            dvMessage.innerHTML += "<div  id='notifyError' class='error'></div>";

            var actionButtons = "<div class='actionButtons'>";
            actionButtons += "<button class='btn btn-primary' id='btnNotifyContinue'>" + Language.GetCode('Continue') + "</button>";
            actionButtons += "<button class='btn btn-danger' id='btnNotifyQuit' >" + Language.GetCode('Quit') + "</button>";
            actionButtons += "</div>";

            dvMessage.innerHTML += actionButtons;

            //Appel du call back avec la valeur du champ text
            Event.AddById('btnNotifyContinue', "click", function () {

                var tbNotifyInput = document.getElementById("tbNotifyInput");
                var notifyError = document.getElementById("notifyError");

                if (tbNotifyInput.value != "") {

                    notifyError.innerHTML = "";
                    callBackYes(tbNotifyInput.value);
                    Notification.Close();

                } else {
                    notifyError.innerHTML = Language.GetCode("Required");
                }
            }

            );

            Event.AddById('btnNotifyQuit', 'click', Notification.Close);

        } 

        
        
        else if (this.AutoClose == undefined)
        {
            setTimeout(function () {
                Notification.Close();
            }, 2000);
        }
    }
}
;

/**
 * Fermeture de la notifucation
 */
Notification.Close = function ()
{
    var dvMessage = document.getElementById("popupNotify");
    document.body.removeChild(dvMessage);
};
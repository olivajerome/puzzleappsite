var AdminTools = function () {};

/***
 * Initialise les outils
 * @returns {undefined}
 */
AdminTools.Init = function () {
    Event.AddById("btnAdminLang", "click", AdminTools.OpenLanguageTools);
    Event.AddById("btnAdminLog", "click", AdminTools.OpenLogTools);
    Event.AddById("btnAdminRequest", "click", AdminTools.OpenDebugger);
    Event.AddById("btnCloseAdminTool", "click", AdminTools.CloseDialog);
};

/***
 * Outil de traduction
 * @returns {undefined}
 */
AdminTools.OpenLanguageTools = function () {
    AdminTools.OpenDialog();
    let view = "<input id='langSearch' type='text' class='form-control' placeholder='Terme recherche'>";
    view += "<div id='adminToolResultLang'></div>";

    Animation.Load("adminToolContent", view);

    Event.AddById("langSearch", "keyup", (e) => {

        var data = "Class=Lang&Methode=LoadElement&App=Lang";
        data += "&page=0";
        data += "&all=0";

        data += "&keyWord=" + e.srcElement.value;

        Request.Post("Ajax.php", data).then(data => {

            Animation.Load("adminToolResultLang", data);

            var desktop = document.getElementById("adminToolResultLang");
            var input = desktop.getElementsByTagName("input");

            for (i = 0; i < input.length; i++)
            {
                Event.AddEvent(input[i], "blur", AdminTools.UpdateLangElement);
            }
        });

    });
};

/***
 * Delete a element
 */
AdminTools.UpdateLangElement = function (e)
{
    if (e.srcElement.id != "tbSearch" && e.srcElement.id != "btnSearch" && e.srcElement.id != "tbSearchLangue" && e.srcElement.id != "btnSearchLangue")
    {
        var idElement = e.srcElement.parentNode.parentNode.childNodes[0].nextSibling.innerHTML;

        var JAjax = new ajax();
        JAjax.data = "App=Lang&Methode=UpdateElement";
        JAjax.data += "&idElement=" + idElement;
        JAjax.data += "&value=" + e.srcElement.value;

        JAjax.GetRequest("Ajax.php");

        Animation.Notify(Language.GetCode("Lang.TextUpdated"));
    }
};

/**
 * Ouverture de l'outil du debbugger
 * @returns {undefined}
 */
AdminTools.CloseDialog = function () {
    Animation.Hide("dialogAdminTool");
    Animation.Load("adminToolContent", "");
    clearInterval(AdminTools.intervalLog);
};

/**
 * Ouverture de l'outil du debbugger
 * @returns {undefined}
 */
AdminTools.OpenDialog = function () {
    Animation.Show("dialogAdminTool");
};

/**
 * Fermeture de l'outik du debugger
 * @returns {undefined}
 */
AdminTools.OpenDebugger = function () {
    AdminTools.CloseDialog();

    const adminDebugger = Dom.GetById("adminDebugger");

    if (adminDebugger.style.display == "none") {
        Animation.Show("adminDebugger");

        Event.AddById("btnCloseAdminDebugger", "click", AdminTools.CloseDebugger);

    } else {
        Animation.Hide("adminDebugger");
    }
};

/***
 * Ferme me deb ugger
 * @returns {undefined}
 */
AdminTools.CloseDebugger = function () {
    Animation.Hide("adminDebugger");
};

/**
 * Vide la session de debuggage
 */
AdminTools.CleanDebugger = function () {

    console.log("Clee debiugg");
    var data = "Class=Admin&Methode=CleanDebugger&App=Admin";

    Request.Post("Ajax.php", data).then(data => {
    });
};

/***
 * Ouvre les fichiers de logs
 * @returns {undefined}
 */
AdminTools.OpenLogTools = function () {
    AdminTools.OpenDialog();
    AdminTools.RefreshLog();

    AdminTools.intervalLog = setInterval(AdminTools.RefreshLog, 2000);

    Event.AddById("adminToolContent", "click", function () {

        clearInterval(AdminTools.intervalLog);
    });
};

/***
 * Rafraichit les logs  intervall régulier
 * @returns {undefined}
 */
AdminTools.RefreshLog = function () {
    var data = "Class=Admin&Methode=GetLog&App=Admin";

    Request.Post("Ajax.php", data).then(data => {
        Animation.Load("adminToolContent", data);
        Event.AddById("btnCleanDebugger", "click", AdminTools.CleanDebugger);
    });

    var dialogAdminTool = document.getElementById("dialogAdminTool");
    dialogAdminTool.scrollTo(0, dialogAdminTool.scrollHeight);
};




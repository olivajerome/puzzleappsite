var NotifyTemplateForm = function(){};

/***
 * Init the form
 * @returns {undefined}
 */
NotifyTemplateForm.Init = function(){
    Event.AddById("btnSaveTemplate", "click", NotifyTemplateForm.SaveElement);

    NotifyTemplateForm.txtEditor = new TextRichEditor( Dom.GetById("Content"),
    {tools: ["ImageTool", "SourceCode", "ShowBlock"],
        events: [{"tool": "ImageTool", "events": [{"type": "mousedown", "handler": BlogArticleEditor.OpenImageLibray}]}],
    }
);
};

/***
* Get the Id of element 
*/
NotifyTemplateForm.GetId = function(){
    return Form.GetId("NotifyTemplateFormForm");
};

/*
* Save the Element
*/
NotifyTemplateForm.SaveElement = function(e){
   
    if(Form.IsValid("NotifyTemplateFormForm"))
    {

    let data = "Class=Notify&Methode=SaveTemplate&App=Notify";
        data +=  Form.Serialize("NotifyTemplateFormForm");
        data["Content"] = NotifyTemplateForm.txtEditor.getCleanContent();

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.status == "error"){
                Form.RenderError("NotifyTemplateFormForm", data.data.message);
            } else {

                Form.SetId("NotifyTemplateFormForm", data.data.Id);

                Dialog.Close();

                Notify.ReloadTemplate(data);
            }
        });
    }
};


<section>
    
    <div id='container' style='border:1px solid grey; padding:5px; border-radius:8px'>
        is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
    </div>
    
    <div class='center marginTop'>
    
          {{GetControl(textarea,source,{Id=source,PlaceHolder=Enter text to load in the container)})}}
  
          {{GetControl(Button,btnLoad,{LangValue=Load,CssClass=btn btn-primary,Id=btnLoad,)})}}
  
    
    </div>
    
</section>

<script>
    
    Event.AddById("btnLoad", "click", ()=>{
        Animation.Load("container", Dom.GetById("source").value);
    });
   
       
</script>

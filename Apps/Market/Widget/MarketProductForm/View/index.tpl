{{form->Open()}}
{{form->Error()}}
{{form->Success()}}

{{form->Render(Id)}}
    <div>
        <label>{{GetCode(Market.Title)}}</label> 
        {{form->Render(Title)}}
    </div>

    <div>
        <label>{{GetCode(Market.Description)}}</label> 
        {{form->Render(Description)}}
    </div>

    <div>
        <label>{{GetCode(Market.Price)}}</label> 
        {{form->Render(Price)}}
    </div>
    
    <div class='center marginTop' > 
        {{form->Render(Upload)}}
    </div>

    <div class='center marginTop' >
       {{form->RenderImage()}}
    </div>
    
    <div class='center marginTop' >   
        {{form->Render(btnSaveProduct)}}
    </div>  


    {{tabProduct}}

{{form->Close()}}
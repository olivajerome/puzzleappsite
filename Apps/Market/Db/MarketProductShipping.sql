CREATE TABLE IF NOT EXISTS `MarketProductShipping` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`OfferId` INT  NOT NULL,
`ProductId` INT  NOT NULL,
`TrackingNumber` TEXT  NOT NULL,
`TrackingUrl` TEXT  NOT NULL,
`DateSended` DATE  NOT NULL,
`Status` INT  NOT NULL,
PRIMARY KEY (`Id`),
CONSTRAINT `MarketProduct_MarketProductShipping` FOREIGN KEY (`ProductId`) REFERENCES `MarketProduct`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
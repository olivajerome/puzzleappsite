/***
 * Transforme des div en tutoriel
 * Afin de présenter le site
 * @param {type} control
 * @returns {Tuto}
 */

function Tuto(control){

    var container = document.getElementById(control);
    container.style.display = "none";
    this.currentIndex = 0;

    this.step = container.getElementsByTagName("p");

    this.Start = function()
    {
        
        currentTuto = this;
        
        Animation.Notify("<i class='fa fa-arrow-left' >&nbsp;</i> " + this.step[this.currentIndex].innerHTML + "<br/><a style='cursor:pointer' id='nextStepTuto'>Continuer</a>", 
        this.step[this.currentIndex].dataset.control, false); 
        
        Event.AddById('nextStepTuto', "click", function(){
            currentTuto.NextStep();

        });
    };
    
    /***
     * Tuto suivant
     * @returns {undefined}
     */
    this.NextStep = function()
    {
        Notification.Close();
        this.currentIndex += 1;
     
        if(this.step[this.currentIndex] != undefined){

           Animation.Notify("<i class='fa fa-arrow-left' >&nbsp;</i>" + this.step[this.currentIndex].innerHTML + "<br/><a style='cursor:pointer' id='nextStepTuto'>Continuer</a>", 
                            this.step[this.currentIndex].dataset.control
                            , false); 


            Event.AddById('nextStepTuto', "click", function(){
                  currentTuto.NextStep();
              });
        }
    };
};
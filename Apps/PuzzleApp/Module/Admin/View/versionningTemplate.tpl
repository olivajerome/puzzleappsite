<section>
    
    {{foreach Templates}}

     <div class='row'>
        <label class='col-md-3'>{{element->Name->Value}}</label>
        <p class='col-md-3'>{{element->Version->Value}}</p>
        <p class='col-md-3'>{{GetControl(Button,btnCreateVersionTemplate,{Id=btnCreateVersionTemplate,CssClass=btn btn-primary btnVersionTemplate,LangValue=PuzzleApp.CreateVersionTemplate})}}</p>
        </div>

    {{/foreach Templates}}

</section>
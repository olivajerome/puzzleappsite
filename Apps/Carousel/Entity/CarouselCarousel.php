<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Carousel\Entity;

use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;
use Apps\Carousel\Entity\CarouselItem;

class CarouselCarousel extends Entity {

    //Constructeur
    function __construct($core) {
        //Version
        $this->Version = "2.0.0.0";

        //Nom de la table 
        $this->Core = $core;
        $this->TableName = "CarouselCarousel";
        $this->Alias = "CarouselCarousel";

        $this->Name = new Property("Name", "Name", TEXTBOX, false, $this->Alias);
        $this->Code = new Property("Code", "Code", TEXTBOX, false, $this->Alias);
        $this->Description = new Property("Description", "Description", TEXTAREA, false, $this->Alias);
        $this->Type = new Property("Type", "Type", NUMERICBOX, false, $this->Alias);
        $this->Entity = new Property("Entity", "Entity", TEXTBOX, false, $this->Alias);
        $this->Parameters = new Property("Parameters", "Parameters", TEXTAREA, false, $this->Alias);

        //Creation de l entité 
        $this->Create();
    }

    /*     * *
     * Get the Items of the carousel
     */

    function GetItems($returnArray = false) {

        $item = new CarouselItem($this->Core);
        $items = $item->Find("CarouselId=" . $this->IdEntite);

        if ($returnArray == true) {
            return $item->ToAllArray($items);
        }

        return $items;
    }
    
    /***
     * Get Parameter of the carousel
     */
    function GetParameters(){
        
        $parameters = rtrim($this->Parameters->Value, ';');

        // Conversion en tableau associatif
        $parameters = explode(';', $parameters);
        $arrayParameters = [];
        foreach ($parameters as $paire) {
            list($cle, $valeur) = explode(':', $paire);
            $arrayParameters[$cle] = $valeur;
        }


        return json_encode($arrayParameters);
    }
    
}

?>
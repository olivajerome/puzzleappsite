<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\EeApp\Entity;

use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class EeAppPlugin extends Entity {

    //Constructeur
    function __construct($core) {
        //Version
        $this->Version = "2.0.0.0";

        //Nom de la table 
        $this->Core = $core;
        $this->TableName = "EeAppPlugin";
        $this->Alias = "EeAppPlugin";

        $this->Name = new Property("Name", "Name", TEXTBOX, true, $this->Alias);
        $this->Code = new Property("Code", "Code", TEXTBOX, true, $this->Alias);
        $this->Description = new Property("Description", "Description", TEXTAREA, true, $this->Alias);

        //Creation de l entité 
        $this->Create();
    }
    
    
     /***
     * Obtient la version d'un plugin
     */
    function GetVersion(){
        $appName = $this->Name->Value;
        
        $path = "\\Plugins\\".$appName ."\\".$appName;
        $app = new $path($this->Core);
    
        return $app->Version;
    }
}

?>
CREATE TABLE IF NOT EXISTS `MarketProductOffer` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`ProductId` INT  NOT NULL,
`UserId` INT  NOT NULL,
`Price` VARCHAR(200)  NOT NULL,
`DateCreated` DATE  NOT NULL,
`Status` INT  NOT NULL,
PRIMARY KEY (`Id`),
CONSTRAINT `MarketProduct_MarketProductOffer` FOREIGN KEY (`ProductId`) REFERENCES `MarketProduct`(`Id`),
CONSTRAINT `ee_user_MarketProductOffer` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
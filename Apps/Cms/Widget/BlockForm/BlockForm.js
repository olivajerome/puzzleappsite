var BlockForm = function () {};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
BlockForm.Init = function () {
    Event.AddById("TypeId", "change", BlockForm.ShowTypeBlockData);
    Event.AddById("btnSaveBlock", "click", BlockForm.SaveBlock);
};

/***
 * Obtient l'id de l'itin�raire courant 
 */
BlockForm.GetId = function () {
    return Form.GetId("BlockForm");
};

/***
 * Element supplémentaire selon le type
 * @returns {undefined}
 */
BlockForm.ShowTypeBlockData = function (e) {

    let additionnalData = Dom.GetById("additionnalData");

    switch (e.srcElement.value) {
        case "5" :
            let data = "Class=Cms&Methode=GetListWidgetBlock&App=Cms";

            Request.Post("Ajax.php", data).then(data => {
                additionnalData.innerHTML = data;
                Event.AddById("Widget", "change", function (e) {

                    let descriptionWiget = document.getElementsByClassName("descriptionWidget");
                    let paramsWidget = document.getElementsByClassName("paramsWidget");

                    for (let i = 0; i < descriptionWiget.length; i++) {
                        descriptionWiget[i].style.display = "none";
                        paramsWidget[i].style.display = "none";

                        let Data = paramsWidget[i].querySelector('[id="Data"]');
                        
                        //Formate pour avoir le bon nom de select ou des données complémentaires
                        if (Data != null) {
                            if (descriptionWiget[i].id.indexOf(e.srcElement.value) > 0) {
                                Data.name = "Data";

                            } else {
                                Data.name = Data.name + descriptionWiget[i].id.replace("descriptionWiget", "");
                            }
                        }
                    }

                    Animation.Show("descriptionWiget-" + e.srcElement.value);
                    Animation.Show("paramsWidget-" + e.srcElement.value);
                });
            });
            break;
        default :
            additionnalData.innerHTML = "";
            break;
    }

};

/*
 * Sauvegare l'itineraire
 */
BlockForm.SaveBlock = function (e) {

    if (Form.IsValid("BlockForm"))
    {

        let data = "Class=Cms&Methode=SaveBlock&App=Cms";
        data += Form.Serialize("BlockForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if (data.statut == "Error") {
                Form.RenderError("BlockForm", data.message);
            } else {

                //Form.SetId("BlockForm", data.data.Id);
                Dialog.Close();

                Cms.RefreshTool();
            }
        });
    }
};


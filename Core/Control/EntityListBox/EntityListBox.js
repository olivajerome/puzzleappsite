var EntityListBox = new EntityListBox();

function EntityListBox()
{
    //Ouvre la lite box avec le champ pour filter
    this.Open = function(control){
        
        var html = "<input type='text' id='tbFilterEntityListBox' style='width:100%'>"; 
        
        var select = control.parentNode.parentNode.getElementsByTagName("select");
        var span = control.parentNode.getElementsByTagName("span");
        
        var options = select[0].getElementsByTagName("option");
        
        html += "<ul>";
            
        for (var i = 0; i < options.length; i++) {
            html += "<li class='optionEntityListBox' id='" + options[i].value + "' >" +  options[i].innerHTML + "</li>";
        } 
        
        html += "</ul>";
        
        this.CreateContainer(control, html);
        
        var tbFilterEntityListBox = document.getElementById("tbFilterEntityListBox");
        tbFilterEntityListBox.focus();
        
        Event.AddById("tbFilterEntityListBox", "keyup", function (e) {
        
            value = e.srcElement.value;
            container = e.srcElement.parentNode;
            options = container.getElementsByTagName("li");
                
            for (var i = 0; i < options.length; i++) {
                if (options[i].innerHTML.toLowerCase().indexOf(value.toLowerCase()) > -1) {
                    options[i].style.display = "";
                } else {
                    options[i].style.display = "none";
                }
            }    
        });

        Event.AddByClass("optionEntityListBox", "click", function (e) {
            select[0].value = e.srcElement.id;
            span[0].innerHTML = e.srcElement.innerHTML;
            EntityListBox.Close();
            select[0].click();
        });

    };
    
    this.OpenMultiple = function(control){
        var html = ""; 
        
        var select = control.parentNode.parentNode.getElementsByTagName("select");
        var span = control.parentNode.getElementsByTagName("span");
        
        var options = select[0].getElementsByTagName("option");
        
        html += "<ul>";
            
        for (var i = 0; i < options.length; i++) {
            
            if(options[i].value != ""){
                checkBox =  "<input type='checkbox'  id='cbChoice_" + options[i].value + "' name='"+ options[i].value +"' class='choiceMultipe' />";
            } else {
                checkBox ="";
            }
            html += "<li class='optionEntityListBox'  id='" + options[i].value + "' >" +  checkBox + "<label for='cbChoice_"+ options[i].value +"'>" +options[i].innerHTML + "</label></li>";
        } 
        
        html += "</ul>";
        
        
        html += "<div style='text-align:center'><button id='btnValidMultiple'  class='btn btn-primary' >"+ Language.GetCode("Valider")+"</button></div>";
        this.CreateContainer(control, html);


        Event.AddByClass("choiceMultipe", "click", function (e) {
            e.stopPropagation();
        });
        
        Event.AddById("btnValidMultiple", "click", function (e) {
            
            e.stopPropagation();
            
            var choiceMultipe = document.getElementsByClassName("choiceMultipe");
            var values = Array();
            
            for(var i = 0; i < choiceMultipe.length; i++){
                
                if(choiceMultipe[i].checked){
                    values.push(choiceMultipe[i].parentNode.innerText);
                }
                
                for (var j = 0; j < options.length; j++) {
                    if(options[j].innerText.trim() == choiceMultipe[i].parentNode.innerText.trim()){
                        select[0].options[j].selected = choiceMultipe[i].checked; //choiceMultipe[i].checked;
                    }
                }
                
            }
            select[0].click();
            span[0].innerHTML = values.join(",") ;
            EntityListBox.Close();
        });

    };
    
    this.Close = function () {
        var contextListBox = document.getElementById("contextListBox");
        
        if(contextListBox != null){
            document.body.removeChild(contextListBox);
        }
    };
    
    this.CreateContainer = function (control, html) {
    
        var ClientRect = control.parentNode.getBoundingClientRect();
        
        menu = document.createElement('div');
        menu.id = "contextListBox";
        menu.style.position = 'absolute';
        menu.style.width = ClientRect.width + "px";
        menu.style.overflow = "auto";
        menu.style.border = '1px solid grey';
        
        menu.style.left = ClientRect.left + "px";
        menu.style.padding = "5px";
     
        menu.style.top = (ClientRect.top) + "px";
        menu.style.backgroundColor = "white";
        menu.innerHTML = html;
        
        document.body.appendChild(menu);
        
    };

    this.SetValue = function(controlId){
         var select = document.getElementById(controlId);
        
          var span = select.parentNode.getElementsByTagName("span");
            span[0].innerHTML = select.children[select.selectedIndex].innerHTML;
    }
    

}
;

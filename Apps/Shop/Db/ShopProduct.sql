CREATE TABLE IF NOT EXISTS `ShopProduct` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`ShopId` int(11) NOT NULL, 
`Name` VARCHAR(200)  NULL ,
`Code` VARCHAR(200)  NULL ,
`Description` TEXT  NULL ,
`Status` INT  NULL ,
`Price` float NULL ,

PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
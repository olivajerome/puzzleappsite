{{form->Open()}}
{{form->Error()}}
{{form->Success()}}

{{form->Render(Id)}}

    <div>
        <label>{{GetCode(Shop.Name)}}</label> 
        {{form->Render(Name)}}
    </div>
    <div>
        <label>{{GetCode(Shop.Description)}}</label> 
        {{form->Render(Description)}}
    </div>
   

    <i class='fa fa-plus' id='btnAddItem' title ='{{GetCode(Shop.AddCaracteristiqueItem)}}' ></i>
    <div id='caracteristiqueItem'>
        {{foreach Items}}
            <div style="display: flex; margin-top: 5px;">
                <input id='{{element->IdEntite}}' type="text"
                       value = '{{element->Label->Value}}' 
                 style="width:90%;margin-right:5px" class="form-control">
                <i class="fa fa-trash removeCaracteristique" onclick="MarketCaracteristiqueForm.RemoveItem(this)"></i>
            </div>
        {{/foreach Items}}
    </div>

     <div class='center marginTop' >   
        {{form->Render(btnSaveCaracteristique)}}
    </div>  

{{form->Close()}}
var Notify = function () {};

/*
 * Chargement de l'application
 */
Notify.Load = function (parameter)
{
    this.LoadEvent();
};

/*
 * Chargement des �venements
 */
Notify.LoadEvent = function ()
{
    Dashboard.AddEventAppMenu(Notify.Execute, "", "Notify");
    Dashboard.AddEventWindowsTool("Notify");

    var nbNotif = document.getElementById("nbNotify");

    if (nbNotif) {
        var JAjax = new ajax();
        JAjax.data = "App=Notify&Methode=ViewNotify";
        nbNotif.innerHTML = JAjax.GetRequest("Ajax.php");
    }

    Notify.InitTabParameters();
    Notify.InitTabPlugin();
    Notify.InitTabTemplate();
};

/*
 * Execute une fonction
 */
Notify.Execute = function (e)
{
    //Appel de la fonction
    Dashboard.Execute(this, e, "Notify");
    return false;
};

/*
 *	Affichage de commentaire
 */
Notify.Comment = function ()
{
    Dashboard.Comment("Notify", "1");
};

/*
 *	Affichage de a propos
 */
Notify.About = function ()
{
    Dashboard.About("Notify");
};

/*
 *	Affichage de l'aide
 */
Notify.Help = function ()
{
    Dashboard.OpenBrowser("Notify", "{$BaseUrl}/Help-App-Notify.html");
};

/*
 *	Affichage de report de bug
 */
Notify.ReportBug = function ()
{
    Dashboard.ReportBug("Notify");
};

/*
 * Fermeture
 */
Notify.Quit = function ()
{
    Dashboard.CloseApp("", "Notify");
};

Notify.Init = function () {
  NotifyMenu.Init();
};

Notify.RunMember = function () {

};

/***
 * Tab parameters
 */
Notify.InitTabParameters = function () {
    EntityGrid.Init("gdParameters");

    Event.AddById("btnAddParameters", "click", () => {
        Notify.ShowAddParameters("");
    });
};

/***
 * Edit a parameters
 */
Notify.EditParameter = function (parameterId) {
    Notify.ShowAddParameters(parameterId);
};

/**
 * Dialogue d'ajout de catégorie
 */
Notify.ShowAddParameters = function (parameterId) {

    Dialog.open('', {"title": Dashboard.GetCode("Notify.AddParameter"),
        "app": "Notify",
        "class": "DialogAdminNotify",
        "method": "ShowAddParameter",
        "type": "left",
        "params": parameterId
    });
};

/***
 * Rafraichit la tab des catégorie
 * @returns {undefined}
 */
Notify.ReloadParameters = function (data) {
    let gdParameters = Dom.GetById("gdParameters");

    gdParameters.parentNode.innerHTML = data.data;
    Notify.InitTabParameters();
};

/****
 * Supprime un parametre
 */
Notify.DeleteParameter = function (parameterId) {

    Animation.Confirm(Language.GetCode("Notify.ConfirmDeleteParameter"), function () {
        let request = new Http("Notify", "DeleteParameter");
        request.Add("parameterId", parameterId);

        request.Post(function (data) {
            data = JSON.parse(data);
            Notify.ReloadParameters(data);
        })
    });
};

/***
 * Init tab Plugin 
 */
Notify.InitTabPlugin = function () {
    Event.AddById("btnAddPlugin", "click", Notify.AddPlugin);
    Event.AddByClass("removePlugin", "click", Notify.RemovePlugin);
};

/***
 * Ajout d'un plugin
 */
Notify.AddPlugin = function (e) {

    e.preventDefault();

    Dialog.open('', {"title": Dashboard.GetCode("Annonce.AddPlugin"),
        "app": "EeApp",
        "class": "DialogEeApp",
        "method": "AddPluginApp",
        "params": "Notify",
        "type": "right"
    });
};

/***
 * Rafraichit les plugins
 */
Notify.RefreshPlugin = function () {
    let lstPlugin = Dom.GetById("lstPlugin").parentNode;
    let request = new Http("Notify", "RefreshPlugin");

    request.Post(function (data) {
        lstPlugin.innerHTML = data;
        Notify.InitTabPlugin();
    });
};

/***
 * Remove plugin to the shop 
 * @returns {undefined}
 */
Notify.RemovePlugin = function (e) {

    Animation.Confirm(Language.GetCode("Notify.RemovePlugin"), function () {
        let container = e.srcElement.parentNode;

        let  data = "Class=EeApp&Methode=RemovePluginApp&App=EeApp";
        data += "&pluginId=" + e.srcElement.id;

        Request.Post("Ajax.php", data).then(data => {
            container.parentNode.removeChild(container);
        });
    });
};

/***
 * Init tab Template
 */
Notify.InitTabTemplate = function () {

    EntityGrid.Init("gdTemplate");
    Event.AddById("btnAddTemplate", "click", function () {
        Notify.ShowAddTemplate("")
    });
};


/***
 * Edit a parameters
 */
Notify.EditTemplate = function (templateId) {
    Notify.ShowAddTemplate(templateId);
};

/**
 * Dialogue d'ajout de template pour un email spécifique
 */
Notify.ShowAddTemplate = function (templateId) {

    Dialog.open('', {"title": Dashboard.GetCode("Notify.AddTemplate"),
        "app": "Notify",
        "class": "DialogAdminNotify",
        "method": "ShowAddTemplate",
        "type": "left",
        "params": templateId
    });
};

/***
 * Rafraichit la tab des catégorie
 * @returns {undefined}
 */
Notify.ReloadTemplate = function (data) {
    let gdTemplate = Dom.GetById("gdTemplate");

    gdTemplate.parentNode.innerHTML = data.data;
    Notify.InitTabTemplate();
};

/****
 * Supprime un template
 */
Notify.DeleteTemplate = function (templateId) {

    Animation.Confirm(Language.GetCode("Notify.ConfirmDeleteTemplate"), function () {
        let request = new Http("Notify", "DeleteTemplate");
        request.Add("templateId", templateId);

        request.Post(function (data) {
            data = JSON.parse(data);
            Notify.ReloadTemplate(data);
        })
    });
};


/***
 * Test teh Template
 * @param {type} templateId
 * @returns {undefined}
 */
Notify.TestTemplate = function (templateId) {

    Animation.Ask(Language.GetCode("Email"), function (email) {

        let request = new Http("Notify", "TestTemplate");
        request.Add("TemplateId", templateId);
        request.Add("Email", email);

        request.Post(function (data) {

        });
    })
};
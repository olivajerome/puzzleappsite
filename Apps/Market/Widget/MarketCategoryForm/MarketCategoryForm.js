var MarketCategoryForm = function(){};

/***
 * Init the form
 * @returns {undefined}
 */
MarketCategoryForm.Init = function(){
    Event.AddById("btnSaveCategory", "click", MarketCategoryForm.SaveElement);
};

/***
* Get the Id of element 
*/
MarketCategoryForm.GetId = function(){
    return Form.GetId("MarketCategoryFormForm");
};

/*
* Save the Element
*/
MarketCategoryForm.SaveElement = function(e){
   
    if(Form.IsValid("MarketCategoryFormForm"))
    {
    let request = new Http("Market", "SaveCategory");
        request.SetData(Form.Serialize("MarketCategoryFormForm"));
        
        request.Post(data => {

            data = JSON.parse(data);

            if(data.status == "error"){
                Form.RenderError("MarketCategoryFormForm", data.data.message);
            } else{

                Form.SetId("MarketCategoryFormForm", data.data.Id);

                Dialog.Close();
                Market.ReloadCategory(data);
            }
        });
    }
};


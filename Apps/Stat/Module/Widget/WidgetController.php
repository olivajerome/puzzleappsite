<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Stat\Module\Widget;

use Core\Control\Button\Button;
use Core\Controller\Controller;
use Core\View\View;
use Core\View\ElementView;


/*
 * 
 */

class WidgetController extends Controller {

    /**
     * Constructeur
     */
    function _construct($core = "") {
        $this->Core = $core;
    }

    /**
     * Creation
     */
    function Create() {
        
    }

    /**
     * Initialisation
     */
    function Init() {
        
    }

    /**
     * Affichage du module
     */
    function Show($type, $params) {
        switch ($type) {
            case "AdminDashboard" :
                return $this->AdminDashboard($params);
                break;
        }
    }

    /*
     * Get the home page
     */

    function Index() {
        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);
        return $view->Render();
    }

    /*     * *
     * Widget de base
     */

    function AdminDashboard($params) {
        $view = new View(__DIR__ . "/View/adminDashboard.tpl", $this->Core);
        
        //Nombre d'utilisateur
        $user = new \Core\Entity\User\User($this->Core);
        $users = $user->GetAll();
        $view->AddElement(new ElementView("nbUser", count($users)));
        
      /*  $forumMessage = new \Apps\Forum\Entity\ForumMessage($this->Core);
        $messages = $forumMessage->GetAll();
        $view->AddElement(new ElementView("nbForumMessage", count($messages)));
        */
        
        $product = new \Apps\Swcf\Entity\Product($this->Core);
        $product->AddArgument(new \Core\Entity\Entity\Argument("Apps\Swcf\Entity\Product", "Statut", EQUAL, "2"));
        $products = $product->GetByArg();
        
        $view->AddElement(new ElementView("nbProduct", count($products)));
       
        $proposition = new \Apps\Swcf\Entity\Proposition($this->Core);
        $propositions = $proposition->Find("Statut in (1,3,5) ");
        $view->AddElement(new ElementView("nbProposition", count($propositions)));
    
        $command = new \Apps\Swcf\Entity\ProductCommand($this->Core);
        $command = $command->Find("Statut  < 90 ");
        $view->AddElement(new ElementView("nbCommand", count($command)));
    
        $unvalidProposition = new \Apps\Swcf\Entity\UnvalidProposition($this->Core);
        $view->AddElement(new ElementView("nbUnvalidProposition", count($unvalidProposition->GetAll())));
       
        $unvalidPropositionClotured = new \Apps\Swcf\Entity\UnvalidPropositionClotured($this->Core);
        $view->AddElement(new ElementView("nbUnvalidPropositionClotured", count($unvalidPropositionClotured->GetAll())));
        
        $productFiabilite = new \Apps\Swcf\Entity\ProductFiabilite($this->Core);
        $view->AddElement(new ElementView("nbProductFiabilite", count($productFiabilite->GetAll())));
      
        $productAttractivite = new \Apps\Swcf\Entity\ProductAttractivite($this->Core);
        $view->AddElement(new ElementView("nbProductAttractivite", count($productAttractivite->GetAll())));
      
        $evaluate = new \Apps\Swcf\Entity\Evaluate($this->Core);
        $view->AddElement(new ElementView("nbEvaluate", count($evaluate->GetAll())));
      
        $discuss = new \Apps\Discuss\Entity\DiscussDiscuss($this->Core);
        $view->AddElement(new ElementView("nbDiscuss", count($discuss->GetAll())));
      
        
        
        return $view->Render();
    }

    /* action */
}

?>
var ColumnForm = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
ColumnForm.Init = function(){
    Event.AddById("btnSaveColumn", "click", ColumnForm.SaveColumn);
};

/***
* Obtient l'id de l'itin�raire courant 
*/
ColumnForm.GetId = function(){
    return Form.GetId("columnForm");
};

/*
* Sauvegare l'itineraire
*/
ColumnForm.SaveColumn = function(e){
   
    if(Form.IsValid("columnForm"))
    {

    var data = "Class=RoadMap&Methode=SaveColumn&App=RoadMap";
        data +=  Form.Serialize("columnForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.statut == "Error"){
                Form.RenderError("columnForm", data.message);
            } else{

                Form.SetId("columnForm", data.data.Id);
                
                RoadMap.LoadRoadMap(null, RoadMapWidget.GetCurrentId());
                Dialog.Close();
            }
        });
    }
};


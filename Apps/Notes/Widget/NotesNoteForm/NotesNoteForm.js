var NotesNoteForm = function(){};

/***
 * Init the form
 * @returns {undefined}
 */
NotesNoteForm.Init = function(){
    Event.AddById("btnSave", "click", NotesNoteForm.SaveElement);
};

/***
* Get the Id of element 
*/
NotesNoteForm.GetId = function(){
    return Form.GetId("NotesNoteFormForm");
};

/*
* Save the Element
*/
NotesNoteForm.SaveElement = function(e){
   
    if(Form.IsValid("NotesNoteFormForm"))
    {

    let data = "Class=Notes&Methode=SaveNote&App=Notes";
        data +=  Form.Serialize("NotesNoteFormForm");

        Request.Post("Ajax.php", data).then(data => {
           
            data = JSON.parse(data);

            if(data.status == "error"){
                Form.RenderError("NotesNoteFormForm", data.data.message);
            } else{

               Notes.ReloadNotes(data);
               Dialog.Close();
            }
        });
    }
};


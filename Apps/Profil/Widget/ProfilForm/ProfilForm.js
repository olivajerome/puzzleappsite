var ProfilForm = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
ProfilForm.Init = function(callBack){
    
    ProfilForm.CallBack = callBack;
    Event.AddById("btnSaveProfil", "click", ProfilForm.UpdateProfil);
};

/***
* Obtient l'id de l'itin�raire courant 
*/
ProfilForm.GetId = function(){
    return Form.GetId("ProfilFormForm");
};

/*
* Update the Profil
*/
ProfilForm.UpdateProfil = function(e){
   
    if(Form.IsValid("ProfilFormForm"))
    {

    var data = "Class=Profil&Methode=SaveInformation&App=Profil";
        data +=  Form.Serialize("ProfilFormForm");

        Request.Post("Ajax.php", data).then(data => {

            let response = JSON.parse(data);
            Animation.Notify(response.data.Message);
            
            if( ProfilForm.CallBack != ""){
                ProfilForm.CallBack(data);
            }
           
        });
    }
};


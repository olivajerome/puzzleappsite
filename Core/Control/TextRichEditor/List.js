class List extends BaseTool {
    getIcone() {
        return "<i class='fa fa-list'> </i>";
    }

    getName() {
        return Language.GetCode("Base.TextRichEditorList");
    }
    execute(e) {
        e.preventDefault();

        const selectedText = window.getSelection().toString();
        let newNode = this.applyStyle("ul");
        newNode.innerHTML = "";
        const li = document.createElement('li');
        li.innerHTML = selectedText;
        newNode.appendChild(li);
        super.execute();
    }
}



<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */


namespace Apps\Market\Helper;

use Core\Utility\Format\Format;
use Apps\Market\Helper\MarketHelper;
use Apps\Market\Entity\MarketCategory;

use Core\Entity\Entity\Argument;

class CategoryHelper
{

    /***
     * Get All category of the current Market
     */
    public static function GetAll($core, $json = false){

        $Market = MarketHelper::GetMarket($core);

        if(count($Market) > 0){
            $category = new MarketCategory($core);
            $categories = $category->Find("MarketId=" . $Market[0]->IdEntite);
        
            if($json){
                return $category->ToAllArray($categories);
            }
            return $categories;
        
        }

        return [];
    }

    
}
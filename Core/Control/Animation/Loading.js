function Loading()
{

    /**
     * Affiche un loading sur un control
     * @param {*} controlId 
     */
    this.ShowOn = function(controlId)
    {
        var loadingControl = document.createElement("div");
            loadingControl.className = "loadingControl";
            loadingControl.innerHTML = "<img src='"+document.location.origin+"/images/loading/load.gif' />";

        var control = document.getElementById(controlId);
        control.parentNode.appendChild(loadingControl);
    };

    /**
     * Supprime un loading sur un control
     * @param {*} controlId 
     */
    this.RemoveOn = function(controlId)
    {
       var control = document.getElementById(controlId);
       var loadingControl =  control.parentNode.getElementsByClassName("loadingControl");
       control.parentNode.removeChild(loadingControl[0]);
    };
};
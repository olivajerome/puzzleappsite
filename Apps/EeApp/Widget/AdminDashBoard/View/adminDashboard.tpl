 <div class='col-md-4'>
    <div class='block'>
        <i class='fa fa-trash removeWidget' title="{{GetCode(Admin.RemoveWidget)}}" data-app='EeApp'></i>
        <i class='fa fa-desktop title'>&nbsp;{{GetCode(EeApp.Apps)}}</i>
        
        <ul>
         <li><b>{{nbApp}}</b> {{GetCode(EeApp.apps installed)}}</li>
        {{foreach Apps}}
            <li>{{element->Name->Value}}</li>
        {{/foreach Apps}}
        </ul>
        <div class='buttons'>
            <button onclick="Dashboard.StartApp('', 'EeApp', '')" class='btn btn-primary'>{{GetCode(EeApp.AddApp)}}</button>
        </div>
    </div>
</div>
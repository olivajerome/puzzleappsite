<?php

namespace Apps\Cms\Widget\ThemeEditor;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;

class ThemeEditor extends Form {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "") {
        $this->Core = $core;
        $this->Data = $data;
    }

    /*     * *
     * G�n�rer les formulaire
     */

    function Render() {
        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);
        //Recuperation
       $template = \Apps\EeApp\Helper\AppHelper::GetTemplates($this->Core);
       $view->AddElement($template);

        return $view->Render();
    }
}

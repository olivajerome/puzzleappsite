<ul class='nav-menu'>
    <li>
    {{if connected == true}}
        <a class="last-item-menu"  href='{{GetPath(/Membre)}}' >{{GetCode(Base.Member)}}</a>
    {{/if connected == true}}

    {{if connected == false}}
        <a class="last-item-menu" href='{{GetPath(/Login)}}' >{{GetCode(Base.Login)}}</a>
    {{/if connected == false}}

    </li>
</ul>
<section>
    <div class='row'>
        <label class='col-md-3'>Framework</label>
        <p class='col-md-3'>{{GetCoreVersion()}}</p>
        <p class='col-md-3'>{{GetControl(Button,btnCreateVersionFramework,{Id=btnCreateVersionFramework,CssClass=btn btn-primary,LangValue=PuzzleApp.CreateVersionFramework})}}</p>
    </div>
    <div class='row'>
        <label class='col-md-3'>Package</label>
        <p class='col-md-3'>{{GetPackageVersion()}}</p>
        <p class='col-md-3'>{{GetControl(Button,btnCreateVersionPackage,{Id=btnCreateVersionPackage,CssClass=btn btn-primary,LangValue=PuzzleApp.CreateVersionPackage})}}</p>
    </div>
    
    <div class='row'>
        <label class='col-md-3'>Testing</label>
        <p class='col-md-3'>{{GetPackageVersion()}}</p>
        <p class='col-md-3'>{{GetControl(Button,btnCreateVersionTesting,{Id=btnCreateVersionTesting,CssClass=btn btn-primary,LangValue=PuzzleApp.btnCreateVersionTesting})}}</p>
    </div>
    
    
    
    {{foreach Apps}}

     <div class='row' style='border-top:1px solid grey'>
        <label class='col-md-3'>{{element->Name->Value}}</label>
        <p class='col-md-3'>{{element->GetVersion()}}</p>
        <p class='col-md-3'>{{GetControl(Button,btnCreateVersionApp,{Id=btnCreateVersionApp,CssClass=btn btn-primary btnVersionApp,LangValue=PuzzleApp.CreateVersionApp})}}</p>
        </div>

    {{/foreach Apps}}

</section>
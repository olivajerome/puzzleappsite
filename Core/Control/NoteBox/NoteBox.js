var NoteBox = new NoteBox();

function NoteBox()
{
    this.Init = function(controlId, callBack){
        
        var container = document.getElementById(controlId);
        
        var star = container.getElementsByClassName("fa");
        
        for(var i = 0; i < star.length ; i++){
        
            Event.AddEvent(star[i], "mouseenter", function(e){
            
                currentStar  = e.srcElement;
                
                if(currentStar.className == "fa fa-star"){
                    currentStar.className = "fa fa-star active";
                    
                    //ON doit activer les plus petits
                    for(var j = 0; j < star.length; j++){
                        
                        if(star[j].id < currentStar.id ){
                            star[j].className = "fa fa-star active";
                        }
                    }
                    
                    if(callBack != undefined){
                        callBack(NoteBox.GetNote(controlId),e);
                    }
                    
                } else {
                
                    //on doit desactiver les plus grand
                    currentStar.className = "fa fa-star";
               
                    //ON doit activer les plus petits
                    for(var j = 0; j < star.length; j++){
                        
                        if(star[j].id > currentStar.id ){
                            star[j].className = "fa fa-star";
                        }
                    }
                    
                    if(callBack != undefined){
                        callBack(NoteBox.GetNote(controlId), e);
                    }
                }
            });
        }
    };
    
    /*
    * Obtient la note max
    */
    this.GetNote = function(controlId){
        var container = document.getElementById(controlId);
        var star = container.getElementsByClassName("fa");
        var note = 0;
        
        for(var i = 0; i < star.length; i++){
        
            if(star[i].className == "fa fa-star active" && star[i].id > note){
                note = star[i].id;
            }
        }
        
        return note;
    }
}

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1>Nouveau projet </h1>
            
            <p class="resultCreateDiscuss"> Sujets sur lesquels vous proposez d'échanger autour d'une rencontre, à l'occasion </p>
           
        </div>
	</div>
	<div class="cadredotted">
		<div class="row">
			<div class="citation col-md-12">
                             
                           {{if Connected == true}}
                            
                            <div id="ajaxModel" >
                                <div id="error" class="error"></div>
                                    <input type="hidden" id="app" value="Discuss">
                                    <input type="hidden" id="class" value="">
                                    <input type="hidden" name="reseau" class="form-control" value="{{reseau}}">
                                
                                    <input type="hidden" id="action" value="CreateDiscussRencontre">
                                    <input type="hidden" id="CategoryId" name="CategoryId" required="" value="3"><label>Titre</label>
                                    <input type="text" id="Title" name="Title" class="form-control" required=""><label>Message</label>
                                 
                                    <textarea id="Description" name="Description" class="form-control" required=""></textarea>
                                
                                    {{uploadDiscuss}}
                                            
                                  
                                <div class='center'>    
                                    <input type="button" class="btn btn-success" value="Enregistrer" onclick="Dashboard.UpdateModele()"></div>
                                </div>    
                            </div>
                          
                           {{/if Connected == true}}
                          
                            
                           
                <h3>Charte de fonctionnement  </h3>
        
                <ul>
                    <li>- On propose un sujet* sans prosélytisme.</li>
                    <li>- On voit combien de membres sont intéressés pour échanger sur ce sujet, autour d’un verre ou d’un repas.</li>
                </ul>
                
              	*<b>Exemples :</b>  parler potager ou expérience de compostage, autopartage, comment entourer nos ainées, éducation, faire ensemble des produits ménagers, sorties pédestres ou à vélo, soirées coup de cœur lecture ou cinéma, jeux de société, bons plans ou tourisme de proximité, d’activités enfants, découverte d’une AMAP etc.     
        		
			</div>
		</div>
	</div>
<!--<div-->

<script>
   // Dashboard.SetBasicAdvancedText('Message');
</script> 
  </div>

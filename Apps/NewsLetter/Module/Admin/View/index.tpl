<div>
    <h1>{{GetCode(NewsLetter.NewsLetter)}}</h1>
    
    <input type="button" id="btnAddEmailBase" class="btn btn-primary" value="{{GetCode(NewsLetter.AddEmail)}}">
    <input type="button" id="btnAddListEmail" class="btn btn-primary" value="{{GetCode(NewsLetter.AddListEmail)}}">
    <input type="button" id="btnAddCampagne" class="btn btn-primary" value="{{GetCode(NewsLetter.AddCampagne)}}">
    
    <div class='marginTop'>
        {{tabNewsLetter}}
    </div>
</div>
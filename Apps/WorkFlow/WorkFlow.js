var WorkFlow = function () {};

/*
 * Add Event when the App loaded
 */
WorkFlow.Load = function (parameter)
{
    this.LoadEvent();
};

/*
 * Add event
 */
WorkFlow.LoadEvent = function ()
{
    Dashboard.AddEventAppMenu(WorkFlow.Execute, "", "WorkFlow");
    Dashboard.AddEventWindowsTool("WorkFlow");

    WorkFlow.InitTabWorkflow();
};

/***
 * Start the Event for Member 
 */
WorkFlow.LoadMember = function () {
};

/*
 * Execute a function
 */
WorkFlow.Execute = function (e)
{
    //Call the function
    Dashboard.Execute(this, e, "WorkFlow");
    return false;
};

/***
 * Init the app front page
 */
WorkFlow.Init = function (app, method) {
    if (app == "WorkFlow") {
        switch (method) {
            default :
                break;
        }
    }
};

/***
 * Init the Admin Widget
 */
WorkFlow.InitAdminWidget = function () {

};

/***
 * Init WorkFlow Tab
 */
WorkFlow.InitTabWorkflow = function(){

    Event.AddById("btnAddWorkFlow", "click", () => {
        WorkFlow.ShowAddWorkflow("")
    });
    EntityGrid.Initialise('gdWorkFlow');

    Event.AddByClass("removeAction", "click", WorkFlow.RemoveAction);

};

/**
 * Dialogue d'ajout de workflow
 */
WorkFlow.ShowAddWorkflow = function (workflowId) {

    Dialog.open('', {"title": Dashboard.GetCode("WorkFlow.AddWorkFlow"),
        "app": "WorkFlow",
        "class": "DialogAdminWorkFlow",
        "method": "AddWorkFlow",
        "type": "left",
        "params": workflowId
    });
};

/****
 * Edit a workflow
 * @param {type} workflowId
 * @returns {undefined}
 */
WorkFlow.EditWorkFlow = function (workflowId) {
    WorkFlow.ShowAddWorkflow(workflowId);
};

/***
 * Refresh the Workflow Tab
 */
WorkFlow.RefreshWorkflow = function(data){

    let gdWorkFlow = Dom.GetById("gdWorkFlow");
    gdWorkFlow.parentNode.innerHTML = data.data;

    WorkFlow.InitTabWorkflow();
};

/**
 * Delete a workflow
 * @param {*} workflowId 
 */
WorkFlow.DeleteWorkFlow = function (workflowId) {

    Animation.Confirm(Language.GetCode("WorkFlow.ConfirmDeleteWorkFlow"),function(){

        let Request = new Http("WorkFlow", "DeleteWorkFlow");
        Request.Add("WorkFlowId", workflowId);

        Request.Post(function(data){

            data = JSON.parse(data);
            WorkFlow.RefreshWorkflow(data);
        });
    });
};

/****
 * Add Trigger and Action to a workflow
 */
WorkFlow.AddTaskWorkFlow = function(workflowId){

    Dialog.open('', {"title": Dashboard.GetCode("WorkFlow.AddTaskWorkFlow"),
    "app": "WorkFlow",
    "class": "DialogAdminWorkFlow",
    "method": "AddTaskWorkFlow",
    "type": "left",
    "params": workflowId
});
};

/****
 * Add Action to a workflow
 */
WorkFlow.AddActionWorkFlow = function(workflowId){

    Dialog.open('', {"title": Dashboard.GetCode("WorkFlow.AddActionWorkFlow"),
    "app": "WorkFlow",
    "class": "DialogAdminWorkFlow",
    "method": "AddActionWorkFlow",
    "type": "left",
    "params": workflowId
});
};

/***
 * Remove action to a workFlow 
 */
WorkFlow.RemoveAction = function(e){

   let container = e.srcElement.parentNode;
   let actionId = container.id;
   
   Animation.Confirm(Language.GetCode("WorkFlow.ConfirmRemoveAction"), function(){
   let request = new Http("WorkFlow", "RemoveAction");
        request.Add("ActionId", actionId);

        request.Post(function(data){
        container.parentNode.removeChild(container);
        });    
    });
};
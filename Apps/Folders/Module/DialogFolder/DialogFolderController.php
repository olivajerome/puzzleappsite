<?php
/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */
namespace Apps\Folders\Module\DialogFolder;

use Core\Controller\Controller;
use Core\View\View;
use Core\Core\Request;
use Core\Control\Form\Form;

use Apps\Folders\Widget\FolderForm\FolderForm;
use Apps\Folders\Entity\FolderFolder;

use Apps\Folders\Widget\FileForm\FileForm;
use Apps\Folders\Entity\FolderFiles;


/*
 * 
 */

class DialogFolderController extends Controller {

    /**
     * Constructeur
     */
    function _construct($core = "") {
        $this->Core = $core;
    }
    
    /***
     * Dialogue d'ajout de dossier
     */
    function ShowAddFolder($params){
       $folderForm = new FolderForm($this->Core);
       $folderForm->Load(Request::GetPosts());
       
        if($params != ""){
            $folder = new FolderFolder($this->Core);
            $folder->GetById($params);
            $folderForm->Load($folder);
        }
       
       return $folderForm->Render();    
    }
    
    /***
     * Dialogue d'ajout de fichier
     */
    function ShowAddFile($params){
        $fileForm = new FileForm($this->Core);
       
        if($params != ""){
            $file = new FolderFiles($this->Core);
            $file->GetById($params);
            $fileForm->Load($file);
        }
       
       return $fileForm->Render();    
    }
    
    
}


<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Membre\Module\DashBoard;

use Core\Controller\Controller;
use Core\Core\Core;
use Core\View\View;
use Core\View\ElementView;

use Core\Dashboard\DashBoardManager;
use Core\Control\TabStrip\TabStrip;

class DashBoardController extends Controller 
{
      /**
    * Constructeur
    */
   function __construct($core="")
   {
       $this->Core = Core::getInstance();
   }

   /*
    * Home page
     */
    public function Index()
    {
        //Page View
        $view = new View(__DIR__."/View/index.tpl", $this->Core);

        $tips = DashBoardManager::GetApp("Tips", $this->Core);
        
        if($tips != null){
            $view->AddElement(new ElementView("showTips", $tips->haveTip()));
        } else{
            $view->AddElement(new ElementView("showTips", false));
        }
        
        return $view->Render();
    }
}




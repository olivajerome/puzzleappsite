class Link extends BaseTool {
    getIcone() {
        return "<i class='fa fa-link'></i>";
    }

    getName() {
        return Language.GetCode("Base.TextRichEditorLink");
    }

    setEvent(events) {
        this.events = events;
    }
    execute(e) {
        
        e.preventDefault();
        e.stopPropagation();    

        let instance = this;
        let node = window.getSelection().anchorNode;
        let textValue ="";
        let child = false;
        let removeChid = false;
        
        //ON prend la div parents
        if(node.nodeName == "#text"  ){
            child = node;
            node = node.parentElement;
            
            if( window.getSelection().toString() != ""){
                textValue = node.textContent;
                removeChid = true;
              
            }
        } 
      
        instance.editor.setSelectedNode(node);
        
   
        let view = "<div style='text-align : center'>";
        view += "<form id='linkForm'>"
        view += "<label>" + Language.GetCode("Base.Label") + "</label>";
        view += "<input class='form-control' name='label' type='text' value='" + textValue + "'>";

        view += "<label>" + Language.GetCode("Base.Link") + "</label>";
        view += "<input class='form-control' name='link' type='text' value=''>";
      
        view += "<button class='btn btn-primary' style='margin-top:5px' id='btnApplyLink'>" + Language.GetCode("Base.AppendLink") + "</button>";
        view += "</form></div>";

        this.editor.dialog(view, function () {
            Event.AddById("btnApplyLink", "click", (e) => {

                e.preventDefault();
                let label = Form.GetValue("linkForm", "label");
                let link = Form.GetValue("linkForm", "link");
                
                if(removeChid){
                    
                    console.log("On remplace");
                    console.log(node);
                    
                      let a = document.createElement("a")
                            a.innerHTML = label;
            a.href = link;
        
            window.getSelection().anchorNode.appendChild(a);
                    //instance.editor.setSelectedNode(node);
                    //instance.editor.appendLink(label, link);
                }    
                
                else{
                    instance.editor.appendLink(label, link);
                }
            
                TextRichEditor.removeDialog();
            });
        });
    }
}
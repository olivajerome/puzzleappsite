var Membre = function (){};

/*
 * * Chargement de l'application
*/
Membre.Load = function(parameter)
{
    Dashboard.AddEventById("lkProfil", "click", Membre.StartProfil );
    Dashboard.AddEventById("lkMessage", "click", Membre.StartMessage );
    Dashboard.AddEventById("lkNotify", "click", Membre.StartNotify );
    Dashboard.AddEventById("lkTask", "click", Membre.StartTask );
   
    Event.AddByClass("startAppUser", "click", Membre.StartAppUser);
    
    //Dashboard.StartApp("","EeApp");
};

/***
 * Lance une application
 * @returns {undefined}
 */
Membre.StartAppUser = function(e){
    Dashboard.StartApp('', e.srcElement.id, 'Membre');
};


Membre.Load();

Membre.Init = function(){};
<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Annonce\Module\Member;

use Core\Control\Button\Button;
use Core\Controller\Controller;
use Core\View\View;
use Core\View\ElementView;

use Apps\Annonce\Helper\AnnonceHelper;
use Apps\Annonce\Entity\AnnonceAnnonce;

/*
 * 
 */

class MemberController extends Controller {

    /**
     * Constructeur
     */
    function _construct($core = "") {
        $this->Core = $core;
    }

    /**
     * Create
     */
    function Create() {
        
    }

    /**
     * Init
     */
    function Init() {
        
    }

    /**
     * Render
     */
    function Show($all = true) {
        return $this->Index();
    }

    /*
     * Get the home page
     */

    function Index() {
        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);
        
        $annonces = AnnonceHelper::GetByUser($this->Core, $this->Core->User->IdEntite);
        
        $view->AddElement(new ElementView("Annonces", $annonces));


        return $view->Render();
    }

    /**
     * Update Property of a annonce
     */
    function UpdateAnnonce($data){

        $annonce=new AnnonceAnnonce($this->Core);
        $annonce->GetById($data["AnnonceId"]);

        $property = $data["Property"];
        $annonce->$property->Value = $data["Value"];
        $annonce->Save();

        return true;

    }
    /* action */
}

?>
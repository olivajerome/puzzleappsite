var MoocCategoryForm = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
MoocCategoryForm.Init = function(callBack){

    Event.AddById("btnSaveCategory", "click", MoocCategoryForm.SaveCategory);
    MoocCategoryForm.callBack = callBack;
};

/***
* Obtient l'id de l'itin�raire courant 
*/
MoocCategoryForm.GetId = function(){
    return Form.GetId("categoryForm");
};

/*
* Sauvegare l'itineraire
*/
MoocCategoryForm.SaveCategory = function(e){
   
    if(Form.IsValid("categoryForm"))
    {

    var data = "Class=Mooc&Methode=SaveCategory&App=Mooc";
        data +=  Form.Serialize("categoryForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);
         
            if(data.statut == "Error"){
                Form.RenderError("categoryForm", data.message);
            } else {

                Dialog.Close();
                
                if(MoocCategoryForm.callBack){
                    MoocCategoryForm.callBack(data);
                }
            }
        });
    }
};


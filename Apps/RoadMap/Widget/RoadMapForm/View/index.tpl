{{form->Open()}}
{{form->Error()}}
{{form->Success()}}

{{form->Render(Id)}}


    <div>
        <label>{{GetCode(RoadMap.Name)}}</label> 
        {{form->Render(Name)}}
    </div>

    <div>
        <label>{{GetCode(RoadMap.Description)}}</label> 
        {{form->Render(Description)}}
    </div>
    <div class='center marginTop' >   
        {{form->Render(btnSaveRoadMap)}}
    </div>  

{{form->Close()}}

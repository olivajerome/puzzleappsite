<section>
    <h3>{{GetCode(Folders.SharedFolder)}}</h3>
    <input type='hidden' id='folderShareId' value='{{folderShareId}}' />

    {{tbContact}}

    <ul id='lstUser' class='lstElementAction'>
        {{foreach Users}}
            <li id=' {{element->IdEntite}}'>
                {{element->User->Value->GetPseudo()}}
                <i class='fa fa-trash deleteSharedFolder' >&nbsp</i>
            </li>
        {{/foreach Users}}
    <ul>

</section>

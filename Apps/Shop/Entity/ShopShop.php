<?php 
/*
* PuzzleApp
* Webemyos
* Jérôme Oliva
* GNU Licence
*/

namespace Apps\Shop\Entity;
use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class ShopShop extends Entity  
{
	//Constructeur
	function __construct($core)
	{
		//Version
		$this->Version ="2.0.0.0"; 

		//Nom de la table 
		$this->Core=$core; 
		$this->TableName="ShopShop"; 
		$this->Alias = "ShopShop"; 

		$this->Name = new Property("Name", "Name", TEXTAREA,  true, $this->Alias); 
		$this->Code = new Property("Code", "Code", TEXTAREA,  true, $this->Alias); 
		$this->Description = new Property("Description", "Description", TEXTAREA,  true, $this->Alias); 
		$this->UserId = new Property("UserId", "UserId", NUMERICBOX,  true, $this->Alias); 

		//Creation de l entité 
		$this->Create(); 
	}
}
?>
<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */


namespace Apps\Forum\Helper;

use Apps\Forum\Entity\ForumMessage;
use Core\Dashboard\DashBoardManager;

class NotifyHelper
{
    /***
     * Send notify Response to a annonce
     */
    public static function NewResponse($core, $message){

        $notify = DashBoardManager::GetApp("Notify", $core);
     
        $notify->AddNotify( $core->User->IdEntite,
                            "Forum-NewResponseMessage",
                            $message->UserId->Value, 
                            "Forum",
                            $message->IdEntite,
                            $core->GetCode("Forum.NewReponseMessageTitle"),
                            $core->GetCode("Forum.NewReponseMessageMessage"),
                         );
    }
    
    /***
     * Get Detail Notify
     */
    public static function GetDetailNotify($core, $notify){
        
        switch($notify->Code->Value ){
            
            case "Forum-NewResponseMessage":
                $message = new ForumMessage($core);
                $message->GetById($notify->EntityId->Value);
                
                $view = "<div>";
                $view .= "<h1>".$core->GetCode("Forum.NewResponseMessage")."</h1>";
                $view .= "<p><b>" .$notify->GetUser() ."</b> ". $core->GetCode("Forum.ResponseToYourMessage"); 
                $view .= "<b>".$message->Title->Value."</b></p>";
                $view .= "<span class='date'>".$notify->DateCreate->Value."</span>";
                $view .= "</div>"; 
                
                break;
        }
        
        return $view; 
       
        
    }
    
}
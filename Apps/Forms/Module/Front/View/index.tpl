<section class='container'>


<div class='row'>

    {{if Form->Actif->Value == 0}}
            {{GetCode(Form.NoActif)}}
    {{/if Form->Actif->Value == 0}} 


    {{if Form->Actif->Value == 1}}
        <h1>{{GetCode(Forms.CompleteForm)}}</h1>

       <div class='col-md-12' >
            <h2>{{Form->Libelle->Value}}</h2>
            <p>{{Form->Commentaire->Value}}</p>

            {{GetForm(/Forms/Detail/{{Form->Code->Value}})}}
            
            
            <ul>
            {{foreach Questions}}
              <li>
                  
                   {{element->Libelle->Value}}
                  
                   <p>

                    <div style='display:none'>    
                        {{\Apps\Forms\Decorator\FormDecorator->GetAdminQuestion(element)}}
                    </div>

                    {{if element->Type->Value == 0}}
                     {{GetControl(TextBox,tb_{{element->IdEntite}})}}
                    {{/if element->Type->Value == 0}}

                    {{if element->Type->Value == 1}}
                     {{GetControl(TextArea,ta_{{element->IdEntite}})}}
                    {{/if element->Type->Value == 1}}

                    {{if element->Type->Value == 2}}
                         {{GetControl(ListCheckBox,cb_{{element->IdEntite}},{Elements={{element->GetReponse()}}})}}
                    {{/if element->Type->Value == 2}}

                    {{if element->Type->Value == 3}}
                         {{GetControl(ListRadio,ls_{{element->IdEntite}},{Name=rb_{{element->IdEntite}},Elements={{element->GetReponse()}}})}}
                     {{/if element->Type->Value == 3}}

                    {{if element->Type->Value == 4}}
                     {{GetControl(ListBox, lst_{{element->IdEntite}},{Options={{element->GetReponse()}}})}}
                    {{/if element->Type->Value == 4}}
                   </p>
                   
               </li>
             {{/foreach Questions}}
            </ul>
            </div>
            <div style='text-align:right'>
                {{GetControl(Submit,btnSend,{LangValue=Forms.Send,CssClass=btn btn-success})}}    
            </div>
            
           {{CloseForm()}}
            
    {{/if Form->Actif->Value == 1}}
        
</div>

</section>
       

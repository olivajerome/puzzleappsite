



var ActionProjetWidget = function(){};

/***
 * Initialisation du widget
 * @returns {undefined}
 */
ActionProjetWidget.Init = function(){
    
    var hdComunityId = document.getElementById("hdComunityId");
    ActionProjetWidget.projetId = hdComunityId.value;
    
    Dashboard.AddEventById("btnFollow", "click", ActionProjetWidget.FollowProjet );
    Dashboard.AddEventById("moreActionProjet", "click", ActionProjetWidget.OpenMoreActionProjet);
    Dashboard.AddEventById("btnParticipate", "click", ActionProjetWidget.ParticipateProjet);
    
    Dashboard.AddEventById("actionHome", "click", ActionProjetWidget.LoadHome);
    Dashboard.AddEventById("actionDocument", "click", ActionProjetWidget.LoadDocument);
    Dashboard.AddEventById("actionComunity", "click", ActionProjetWidget.LoadComunity);
    Dashboard.AddEventById("actionEchange", "click", ActionProjetWidget.LoadEchange);
};

/**
 * Menu actions des projets
 * @returns {undefined}
 */
ActionProjetWidget.OpenMoreActionProjet = function(e)
{
     var actionProjetMenu = document.getElementById("actionProjetMenu");
     
     if(actionProjetMenu != null) 
     {
         return;
     }
     
        var hdComunityId = document.getElementById("hdComunityId");
          
        this.core = document.createElement('div');
        this.core.id = "actionProjetMenu";
        this.core.style.position='absolute';
        this.core.style.width= "200px";
        this.core.style.height="250px";
        this.core.style.overflow="auto";
        this.core.style.border='1px solid grey';
        this.core.style.padding="5px";

        this.core.style.left= e.clientX -100 + "px";

        this.core.style.top= e.pageY + "px";
        this.core.style.backgroundColor="white";
        this.core.innerHTML="<div style='text-align:right;width:100%'><i class='fa fa-remove' alt='' title='Fermer' onclick='CloseTool(this)'></i></div>";

        this.core.innerHTML += "<span>Chargement...</span>";

         var data = "App=ParlonsEn&Methode=GetMoreActionProjet";
          data += "&projetId="+ hdComunityId.value;

        var url = window.location.origin + "/Ajax.php";

        Request.Post(url, data).then(data=>{ 

            var content = this.core.getElementsByTagName("span");
            content[0].innerHTML = data;

            Dashboard.AddEventById("unFollow", "click", ActionProjetWidget.Unfollow );
            Dashboard.AddEventById("unParticipate", "click", ActionProjetWidget.unParticipate );
            Dashboard.AddEventById("deleteProjet", "click", ActionProjetWidget.deleteProjet );
            Dashboard.AddEventById("archiveProjet", "click", ActionProjetWidget.archiveProjet );
            Dashboard.AddEventById("pinProjet", "click", ActionProjetWidget.pinProjet );
            Dashboard.AddEventById("modifyProjet", "click", ActionProjetWidget.modifyProjet );
            
            
            
        });

        document.body.appendChild(this.core);
};

/***
 * Suivre un prjet
 * @returns {undefined}
 */
ActionProjetWidget.FollowProjet = function()
{
     var data = "App=ParlonsEn&Methode=FollowProjet";
         data += "&projetId="+ ActionProjetWidget.projetId;
                 
     var url = window.location.origin + "/Ajax.php";

       Request.Post(url, data).then(data=>{ 

          UserProjetWidget.LoadUserProjet();
          ActionProjetWidget.setBtnFollow(false);
      });
};

/**
 * Ne plus suivre le projet
 * @returns {undefined}
 */
ActionProjetWidget.Unfollow = function(){
    
    ActionProjetWidget.CloseActionMenu();
    
    var data = "App=ParlonsEn&Methode=UnFollowProjet";
        data += "&projetId="+ ActionProjetWidget.projetId;
                 
     var url = window.location.origin + "/Ajax.php";

       Request.Post(url, data).then(data=>{ 

          UserProjetWidget.LoadUserProjet();
          
          ActionProjetWidget.setBtnFollow(true);
      });
};

/**
 * Active ou desactive le bouton suivre
 * @param {type} state
 * @returns {undefined}
 */
ActionProjetWidget.setBtnFollow = function(state)
{
    var btnFollow = document.getElementById("btnFollow");
    
    if(state == true){
        btnFollow.disabled = false;
        btnFollow.innerHTML = "Suivre";
    } else
    {
        btnFollow.disabled = true;
        btnFollow.innerHTML = "Vous suivez";
    }
};

/***
 * Participé au projet
 * @returns {undefined}
 */
ActionProjetWidget.ParticipateProjet = function()
{
     var data = "App=ParlonsEn&Methode=ParticipateProjet";
         data += "&projetId="+ ActionProjetWidget.projetId;
                 
     var url = window.location.origin + "/Ajax.php";

       Request.Post(url, data).then(data=>{ 

          MemberProjetWidget.LoadMemberProjet();
          ActionProjetWidget.setBtnParticipate(false);
          UserProjetWidget.LoadUserProjet();
      });
};

/**
 * Ne plus participer
 * @returns {undefined}
 */
ActionProjetWidget.unParticipate = function()
{
    ActionProjetWidget.CloseActionMenu();
    
     var data = "App=ParlonsEn&Methode=UnParticipate";
         data += "&projetId="+ ActionProjetWidget.projetId;
                 
     var url = window.location.origin + "/Ajax.php";

       Request.Post(url, data).then(data=>{ 

          MemberProjetWidget.LoadMemberProjet();
          ActionProjetWidget.setBtnParticipate(true);
          UserProjetWidget.LoadUserProjet();
      });
};

/**
 * Active ou desactive le bouton suivre
 * @param {type} state
 * @returns {undefined}
 */
ActionProjetWidget.setBtnParticipate = function(state)
{
    var btnParticipate = document.getElementById("btnParticipate");
    
    if(state == true){
        btnParticipate.disabled = false;
        btnParticipate.innerHTML = "Participer";
    } else
    {
        btnParticipate.disabled = true;
        btnParticipate.innerHTML = "Vous participez";
    }
};

/***
 * Supprimer un projet
 * @returns {undefined}
 */
ActionProjetWidget.deleteProjet = function()
{
  if(confirm("Supprimer ce projet ?"))
  {
         
     var data = "App=ParlonsEn&Methode=DeleteProjet";
         data += "&projetId="+ ActionProjetWidget.projetId;
                 
     var url = window.location.origin + "/Ajax.php";

       Request.Post(url, data).then(data=>{ 

            var reseauCode = document.getElementById("reseauCode");
            
           document.location.href = window.location.origin + "/" + reseauCode.value  + "/ParlonsEn/Rencontres";
      });
  }
};

/***
 * Archive le projet
 * @returns {undefined}
 */
 ActionProjetWidget.archiveProjet = function()
 {
   if(confirm("Archiver ce projet ? "))
  {
         
     var data = "App=ParlonsEn&Methode=ArchiveProjet";
         data += "&projetId="+ ActionProjetWidget.projetId;
                 
     var url = window.location.origin + "/Ajax.php";

       Request.Post(url, data).then(data=>{ 

            var reseauCode = document.getElementById("reseauCode");
            
           document.location.href = window.location.origin + "/" + reseauCode.value  + "/ParlonsEn/Rencontres";
      });
  }  
 };

/***
 * Ferme le menu
 * @returns {undefined}
 */
ActionProjetWidget.CloseActionMenu = function(){
    
    var actionMenu = document.getElementById("actionProjetMenu");
        actionMenu.parentNode.removeChild(actionMenu);
};

 /*
  * Rechage la page d'accueil
  */
 ActionProjetWidget.LoadHome = function()
 {
      var wallProjet = document.getElementById("wallProjet");
     
      var data = "App=ParlonsEn&Methode=LoadProjet";
         data += "&projetId="+ ActionProjetWidget.projetId;
   
     var url = window.location.origin + "/Ajax.php";

       Request.Post(url, data).then(data=>{ 
   
           wallProjet.innerHTML  = data;
           ParlonsEn.Init();
           ActionProjetWidget.Init();
           MemberProjetWidget.Init();
      });
 };
 
 /**
  * Charge les documents
  * @returns 
  */
 ActionProjetWidget.LoadDocument = function()
 {
     var dashboardDiscusMessage = document.getElementById("dashboardDiscusMessage");
     
      var data = "App=ParlonsEn&Methode=LoadDocument";
         data += "&projetId="+ ActionProjetWidget.projetId;
                 
     var url = window.location.origin + "/Ajax.php";

       Request.Post(url, data).then(data=>{ 
   
           dashboardDiscusMessage.innerHTML  = data;
      });
 };

 ActionProjetWidget.LoadComunity = function()
 {
     var dashboardDiscusMessage = document.getElementById("dashboardDiscusMessage");
     
      var data = "App=ParlonsEn&Methode=LoadComunity";
         data += "&projetId="+ ActionProjetWidget.projetId;
                 
     var url = window.location.origin + "/Ajax.php";

       Request.Post(url, data).then(data=>{ 
   
           dashboardDiscusMessage.innerHTML  = data;
           
           
           Dashboard.AddEventById("btnSendMessageProjet", "click", ActionProjetWidget.SendMessageProjet);  
           
      });
 };
 
 ActionProjetWidget.SendMessageProjet = function(){
 
    var messageProjet = document.getElementById("messageProjet");
    var tbMessageProjet = document.getElementById("tbMessageProjet");
    var projetComunityId = document.getElementById("projetComunityId");
    var uploadImages = document.getElementById("uploadImages-fileToUpload"); 
    var images = new Array();
     
    var data = "App=Comunity&Methode=StartComunity";
        data += "&message="+tbMessageProjet.value;
        data += "&projetId="+ ActionProjetWidget.projetId;
        data += "&ComunityId=" + projetComunityId.value;         
    
     var imgs =  uploadImages.getElementsByTagName("img");   
               
            for(var i = 0; i < imgs.length ; i++ )
            {
                images.push(imgs[i].src);
            }
            data += "&images="+ images.join(";");
            
     var url = window.location.origin + "/Ajax.php";

       Request.Post(url, data).then(data=>{ 
       
            data = JSON.parse(data);
            projetComunityId.value = data.discuId;
            
            messageProjet.innerHTML += data.message;
            
            var tbMessageProjet = document.getElementById("tbMessageProjet");
                tbMessageProjet.style.display = "";
                tbMessageProjet.value ="";
                
            var uploadImages = document.getElementsByClassName("uploadImages");
                uploadImages[0].innerHTML ="";
                
            });
 };
 
 ActionProjetWidget.LoadEchange = function(e)
 {
     
     console.log("Load echange");
     
            ParlonsEn.ShowOverlay();
            
            ParlonsEn.ComunityId = null;
            
             dialog = document.createElement('div');
       dialog.id = "actionProjetMenu";
        dialog.style.position='absolute';
        dialog.style.width= "400px";
        dialog.style.height="250px";
        dialog.style.overflow="auto";
        dialog.style.border='1px solid grey';
        dialog.style.padding="5px";
        dialog.style.zIndex = 9999;
        dialog.style.left= (e.clientX - 400 )+ "px";

        dialog.style.top= e.pageY + "px";
        dialog.style.backgroundColor="white";
        dialog.innerHTML="<div style='text-align:right;width:100%'><i class='fa fa-remove' alt='' title='Fermer' onclick='CloseTool(this)'></i></div>";
     dialog.innerHTML="<div style='text-align:left;width:100%' class='title'><i class='fa fa-remove' alt='' title='Fermer' onclick='CloseTool(this);ParlonsEn.HideOverlay();'></i><div><span  title='Envoyer' style='float:right' ><i id='btnStartDisscus'style='display:none' class='fa fa-arrow-right' >&nbsp;</i></span></div></div>";

                dialog.innerHTML += "<div class='search'>&Agrave; : <input type='text' class='form-control' id='tbSearchUser' placeholder='Recherchez...' onkeyup='ActionProjetWidget.SearchUser(this);' ></div>";
                dialog.innerHTML += "<div id='resultSeachUser'  ></div>";
            
             document.body.appendChild(dialog);
             
             var tbSearchUser = document.getElementById("tbSearchUser");
                 tbSearchUser.focus();
                 
             Dashboard.AddEventById("btnStartDisscus", "click",  ActionProjetWidget.SendNotification);
 };
 
  /**
* Rechercher les utilisateurs correspondant à la recheche
* @returns {undefined}
*/
ActionProjetWidget.SearchUser = function(e)
{
   var searchValue = e.value;
   var resultSeachUser = document.getElementById('resultSeachUser');

   resultSeachUser.innerHTML = "Chargement ..."; 

     var url = window.location.origin + "/Ajax.php";
           var data = "App=Comunity&Methode=SearchUser";
           data += "&search="+  searchValue;

             Request.Post(url, data).then(data=>{

               resultSeachUser.innerHTML = data; 

               Dashboard.AddEventByClass("cbUser", "click", ActionProjetWidget.ShowStarConversation);
           });
};
/***
    * Affiche ou masque la flèche pour démarrer une conversation
    * @returns {undefined}
    */
   ActionProjetWidget.ShowStarConversation = function()
   {
        var resultSeachUser = document.getElementById('resultSeachUser');
        var checkbox = resultSeachUser.getElementsByClassName("cbUser");
        var btnStartDisscus = document.getElementById('btnStartDisscus');
        var OneSelect = false;

        for(var i = 0; i < checkbox.length; i++)
        {
            if(checkbox[i].checked){
                OneSelect = true;
            }
        }

        if(OneSelect == true)
        {
            btnStartDisscus.style.display = "block";
        }
        else
        {
            btnStartDisscus.style.display = "none";
        }
   };   
   
   /***
    * Envoi une notification au membre
    * @returns {undefined}
    */
   ActionProjetWidget.SendNotification  = function()
   {
        ParlonsEn.HideOverlay();
            
             var resultSeachUser = document.getElementById('resultSeachUser');
             var cbUser = resultSeachUser.getElementsByClassName("cbUser");
             var users = Array();
             var reseauCode = document.getElementById("reseauCode");
                
             for(var i= 0; i < cbUser.length; i++)
            {
                if(cbUser[i].type=="checkbox" && cbUser[i].checked )
                {
                    users.push(cbUser[i].id);
                }
            }
            
            ActionProjetWidget.CloseActionMenu();
       
            var data = "App=ParlonsEn&Methode=SendNofication";
                data += "&user="+ users.join();
                data += "&projetId="+ ActionProjetWidget.projetId;
                data += "&reseauCode=" + reseauCode.value;
                
                
            var url = window.location.origin + "/Ajax.php";

            Request.Post(url, data).then(data=>{
                
                Animation.Notify("Merci d'avoir partagé ce projet ! ", "actionEchange");
            });
            
   };
   
   /***
    * Ajoute projet à la page d'accueil
    * @returns {undefined}
    */
   ActionProjetWidget.pinProjet = function()
   {
        var data = "App=ParlonsEn&Methode=PinProjet";
            data += "&projetId="+ ActionProjetWidget.projetId;
                
        var url = window.location.origin + "/Ajax.php";

        Request.Post(url, data).then(data=>{
                Animation.Notify("Ce projet sera affiché sur la page d'accueil");
            });
            
   };
   
   /***
    * Modification du projet
    * @returns {undefined}
    */
   ActionProjetWidget.modifyProjet = function()
   {
        ActionProjetWidget.CloseActionMenu();
        
        Dialog.open("", {title:"Modifier le projet", app:'Coopere', class:"Dialog" , method : "ModifyProjet", params :  ActionProjetWidget.projetId, height : 400});

   };
   
   /***
 * Envoi un message depuis la fenêtre de Tchat
 * @returns {undefined}
 */
 ActionProjetWidget.SendTchatMessage = function()
 {
   var tbTchatMessage = document.getElementById("tbTchatMessage");
   var messageContent = document.getElementById("messageContent");
   var hdTchatId = document.getElementById('hdTchatId');
   var hdMembreId = document.getElementById("hdMembreId");
    
  var data = "App=Comunity&Methode=StartComunity";
            
            if(hdMembreId != undefined && hdMembreId.value != "")
            {
               data += "&users="+ hdMembreId.value;
            }
            else
            {
              data += "&ComunityId="+ hdTchatId.value;
            }
            var url = window.location.origin + "/Ajax.php";
            
            data += "&message="+ tbTchatMessage.value;
            data += "&typeReponse=reponseTchat";
            
            Request.Post(url, data).then(data=>{
                  var data = JSON.parse(data);
              
                    hdTchatId.value = data.discuId;
                    
                    if(hdMembreId != undefined )
                    {
                       hdMembreId.value ="";
                    }
                tbTchatMessage.value ="";
                messageContent.innerHTML += data.message;
                
                
            });
 };
 
 /***
  * On peut envoyer une image plutot que du texte
  * @returns {undefined}
  */
 ActionProjetWidget.SendAttachment = function(){
     
     var tbMessageProjet = document.getElementById("tbMessageProjet");
        tbMessageProjet.style.display = "none";
     
 };
 
/**
* Scroll En base
* @returns {undefined}
*/
ActionProjetWidget.ScrollBottom = function()
{
   var messageContent = document.getElementById("messageContent");
   messageContent.scrollTo(0, messageContent.scrollHeight); 
};
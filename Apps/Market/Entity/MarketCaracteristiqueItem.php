<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Market\Entity;

use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class MarketCaracteristiqueItem extends Entity {

    //Constructeur
    function __construct($core) {
        //Version
        $this->Version = "2.0.0.0";

        //Nom de la table 
        $this->Core = $core;
        $this->TableName = "MarketCaracteristiqueItem";
        $this->Alias = "MarketCaracteristiqueItem";

        $this->CaracteristiqueId = new Property("CaracteristiqueId", "CaracteristiqueId", NUMERICBOX, false, $this->Alias);
        $this->Label = new Property("Label", "Label", TEXTAREA, false, $this->Alias);

        //Creation de l entité 
        $this->Create();
    }
}

?>
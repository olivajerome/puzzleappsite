CREATE TABLE IF NOT EXISTS `MarketCategory` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`MarketId` INT  NOT NULL,
`Code` VARCHAR(200)  NOT NULL,
`Name` VARCHAR(200)  NOT NULL,
`Description` TEXT  NOT NULL,
PRIMARY KEY (`Id`),
CONSTRAINT `MarketMarket_MarketCategory` FOREIGN KEY (`MarketId`) REFERENCES `MarketMarket`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
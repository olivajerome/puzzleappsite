<?php

namespace Apps\Market\Decorator;

class ProductDecorator{

    public function GetProductImages($entity){

        $images = $entity->GetImages();
        $view ="<div id='productImages'>";

        for($i = 0 ; $i < count($images); $i++){

            if($i==0){
                $width ="200px";
            } else {
                $width ="50px";
            }
            $view .= "<img style='width:".$width."' src='".$images[$i]."' />";     
        }

        $view .= "</div>";

        return $view;
    }

}
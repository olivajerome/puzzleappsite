class HtmlToCode extends BaseTool {

    constructor(editor){
        super(editor);
        this.activated = false;
        this.name = "HtmlToCode";
    }

    getIcone() {
        return "<i class='fa fa-code'></i>";
    }

    getName() {
        return Language.GetCode("Base.TextRichEditorShowSourceCode");
    }
    execute(e) {
        
        let instance = this;
        let inputArea = instance.editor.inputArea;
        let codeEditor = document.querySelector("#codeEditor");
        let updatedCode  ="";
        
        if(codeEditor.value.indexOf(":lt") < 0 ){
     
            updatedCode = codeEditor.value.replace(/<codehtml>([\s\S]*?)<\/codehtml>/g, function(match, p1) {
                   // Remplacer les caractères < et > dans le contenu trouvé, puis ajouter les <br> pour les sauts de ligne
                   let transformed = p1
                       .replace(/</g, ":lt:")   // Remplacer < par &lt;
                       .replace(/>/g, ":gt:")   // Remplacer > par &gt;
                       .replace(/'/g, ":q")
                       .replace(/{/g, ":#123;")// Remplacer < par &lt;
                       .replace(/}/g, ":#125;")// Remplacer < par &lt;
                       
                        .replace(/\n/g, ":br:"); // Remplacer les sauts de ligne par <br>

                   // Retourner le contenu modifié avec les balises <codeHtml></codeHtml>
                   return "<codehtml>" + transformed + "</codehtml>";
                   });
        } else{
   
    updatedCode = codeEditor.value.replace(/<codehtml>([\s\S]*?)<\/codehtml>/g, function(match, p1) {
                   // Remplacer les caractères < et > dans le contenu trouvé, puis ajouter les <br> pour les sauts de ligne
                   let transformed = p1
                       .replace(/:lt:/g, "<")   // Remplacer < par &lt;
                       .replace(/:gt:/g, ">")   // Remplacer > par &gt;
                       .replace(/:q/g, "'")   // Remplacer < par &lt;
                        .replace(/:#123;/g, "{")// Remplacer < par &lt;
                 .replace(/:#125;/g, "}")// Remplacer < par &lt;
                           
                .replace(/:br:/g, "\n"); // Remplacer les sauts de ligne par <br>

                   // Retourner le contenu modifié avec les balises <codeHtml></codeHtml>
                   return "<codehtml>" + transformed + "</codehtml>";
                   });
   
   
        }
        
        codeEditor.value = updatedCode;
        
      super.execute();
    }
}
<section class='container'>
    <div class='block'>         
    <h2>{{GetCode(Forum.MessageOfCategory)}} : {{Category->Name->Value}} </h2>
    <p>{{Category->Description->Value}}</p>
    
    <table class='grid width100'>
        <tr>
            <th>{{GetCode(Forum.Sujet)}}</th>
            <th>{{GetCode(Forum.NbMessage)}}</th>
            <th>{{GetCode(Forum.LastReponse)}}</th>
        </tr>
        {{foreach Messages}}
        <tr>
            <td>
                 <div class='imgProfil' style='background-image: url({{element->GetImageUser()}}) '></div>
                <a href='{{GetPath(/Forum/Sujet/{{element->Code->Value}})}}'> 
                    {{element->Title->Value}}
                </a>
            </td>
            <td>{{element->GetNumberReponse()}}</td>
            <td>
                <div>
                {{element->Message->Value}}</div>
                <div style='border-top:1px solid grey;margin-left: 20px'>
                    <h4>{{GetCode(Forum.LastReponse)}} : </h4>
                        {{element->GetLastReponse()}}
                
                    <div class='marginTop center'>    
                        <a class='btn btn-primary' href='{{GetPath(/Forum/Sujet/{{element->Code->Value}})}}'> 
                            {{GetCode(Forum.AddReponse)}}
                        </a>
                    </div>    
                </div>
            
            </td>
        </tr>   
         {{/foreach Messages}}
        
    </table>
         
    <div class='marginTop center'>
        <a href='{{GetPath(/Forum/NewDiscussion/{{Category->Code->Value}})}}' class='btn btn-primary'  >
            {{GetCode(Forum.NewDiscussion)}}
        </a>
    </div>
   
    </div>
</section>
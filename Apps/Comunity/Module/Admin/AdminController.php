<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Comunity\Module\Admin;

use Core\Controller\AdministratorController;
use Core\Entity\Entity\Argument;
use Core\Utility\Format\Format;
use Core\View\ElementView;
use Core\View\View;
use Core\Control\TabStrip\TabStrip;
use Core\Control\EntityGrid\EntityGrid;
use Core\Control\EntityGrid\EntityColumn;
use Core\Control\EntityGrid\EntityIconColumn;
use Core\Control\EntityGrid\EntityFunctionColumn;
use Core\Control\Button\Button;

use Apps\Comunity\Widget\ComunityForm\ComunityForm;

use Apps\Comunity\Helper\ComunityHelper;


class AdminController extends AdministratorController {

    /**
     * Constructeur
     */
    function __construct($core = "") {
        $this->Core = $core;
    }

    /**
     * Affichage du module
     */
    function Show($all = true) {
        return $this->Index();
    }

    /*
     * Get the home page
     */

    function Index() {
        
        $comunityDefault = ComunityHelper::GetDefault($this->Core);
        
        if($comunityDefault){        
        
            $view = new View(__DIR__ . "/View/index.tpl", $this->Core);

            $tabComunity = new TabStrip("tabComunity", "Comunity");
            $tabComunity->AddTab($this->Core->GetCode("Comunity.Comunity"), $this->GetTabComunity($blog));

            $view->AddElement(new ElementView("tabComunity", $tabComunity->Render($blog)));
            return $view->Render();
        }
         else{
            
             $view = new View(__DIR__ . "/View/noComunity.tpl", $this->Core);

             $comunityForm = new ComunityForm($this->Core);
             $view->AddElement(new ElementView("ComunityForm", $comunityForm->Render()));
         
             return $view->Render();
         }
                
    }

    /*     * *
     * Categorie dof the Forum
     */

    function GetTabComunity($Blog) {

        $gdComunity = new EntityGrid("gdComunity", $this->Core);
        $gdComunity->Entity = "Apps\Comunity\Entity\ComunityComunity";
        $gdComunity->AddArgument(new Argument("Apps\Comunity\Entity\ComunityComunity", "BlogId", EQUAL, $Blog->IdEntite));
        $gdComunity->App = "Comunity";
        $gdComunity->Action = "GetTabComunity";

        $btnAdd = new Button(BUTTON, "btnAddComunity");
        $btnAdd->Value = $this->Core->GetCode("Comunity.AddComunity");

        $gdComunity->AddButton($btnAdd);
        $gdComunity->AddColumn(new EntityColumn("Type", "Type"));
     
        $gdComunity->AddColumn(new EntityColumn("Description", "Description"));

        $gdComunity->AddColumn(new EntityColumn("LastMessage", "SubTitle"));

        $gdComunity->AddColumn(new EntityIconColumn("Action",
                        array(array("EditIcone", "Comunity.EditComunity", "Comunity.EditComunity"),
                    array("DeleteIcone", "Comunity.DeleteComunity", "Comunity.DeleteComunity"),
                        )
        ));

        return $gdComunity->Render();
    }
    
    public function DeleteComunity($ComunityId ){
        ComunityHelper::DeleteComunity($this->Core, Request::GetPost(""));
    }

}

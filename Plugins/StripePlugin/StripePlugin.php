<?php

namespace Plugins\StripePlugin;

use Core\View\View;
use Core\View\ElementView;

class StripePlugin{

    function __construct($core){
        $this->Core = $core;
    }
    
    function GetType(){
        return "Paid";
    }
    
    function Render(){
       
        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);
        return $view->Render();
    }
    

    function RenderConfigForm(){
        return "Config Stripe Form";
    }
}
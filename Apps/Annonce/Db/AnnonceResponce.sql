CREATE TABLE IF NOT EXISTS `AnnonceResponce` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`AnnonceId` INT  NOT NULL,
`UserId` INT  NOT NULL,
`Response` TEXT  NOT NULL,
`Satuts` INT  NOT NULL,
PRIMARY KEY (`Id`),
CONSTRAINT `AnnonceAnnonceId_AnnonceResponce` FOREIGN KEY (`AnnonceId`) REFERENCES `AnnonceAnnonce`(`Id`),
CONSTRAINT `ee_user_AnnonceResponce` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
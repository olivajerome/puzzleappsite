<?php

namespace Apps\Cms\Widget\PageEditor;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;
use Core\Entity\Entity\Argument;
use Apps\Cms\Entity\CmsPage;
use Apps\Cms\Entity\CmsContainer;

class PageEditor {
    
    public function __construct($core, $pageId) {
        $this->Core = $core;
        $this->PageId = $pageId;
    }
    
    /***
     * Render the widget
     */
    public function Render(){
        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);
        
        $page = new CmsPage($this->Core);
        $page->GetById($this->PageId);
        $view->AddElement(new ElementView("Page", $page));

        $containers = new CmsContainer($this->Core);
        $view->AddElement(new ElementView("Containers", $containers->Find("PageId=". $page->IdEntite . " ORDER BY Position")));
 

        return $view->Render();
    }
}
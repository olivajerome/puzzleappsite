
<input type='hidden' id='skillAppName' name='skillAppName' value='{{AppName}}' />
<input type='hidden' id='skillEntityId' name='skillEntityId' value='{{EntityId}}' />
 
<div>
    <label>{{GetCode(Base.Category)}}</label>
    <i class='info'></i>
    {{lstSkillCategory}}
</div>

<div id='skillContainer'>
    
</div>

<hr>


<div id='skillEntity'>    
    
    {{foreach Skills}}

        <div class='chips'>
            {{element->SkillName->Value}}

            <i class='fa fa-trash removeSkillEntity' id='{{element->IdEntite}}'  ></i>
        </div>

    {{/foreach Skills}}
</div>

<script id='SkillPlugin'>
    {{pluginScript}}
</script>
<?php

/**
 * PuzzleApp 
 * PuzzleApp.org - The Hybride Framework.
 * Oliva Jérôme
 * GNU Licence
 * */

namespace Core\Core;

use Core\Core\Request;

class Trace {

    public static $Sql;
    public static $Class;
    public static $Request;

    /*
     * Trace the sql Request
     */

    public static function Sql($request) {
        Trace::$Sql[] = $request;
    }

    /*     * *
     * Trace the autoload class
     */

    public static function Class($class) {
        Trace::$Class[] = $class;
    }

    /*     * *
     * Trace the Http Request
     */

    public static function Request($request) {
        Trace::$Request[] = date('d-m-y h:m:s ') . ":" . $request;
    }

    /**
     * Ajoute un trace dans le debugger
     */
    public static function AddDebuger($message) {

        return false;
        if (!Request::GetSession("Debugger")) {
            Request::SetSession("Debugger", date('d-m-y h:m:s ') . "Start debugger");
        } else {

            $debuger = Request::GetSession("Debugger");
            Request::SetSession("Debugger", $debuger . "<br/>" . $message);
        }
    }

    /*     * *
     * Vide la session de debuggaega
     */

    public static function CleanDebugger() {
        Request::SetSession("Debugger", "");
    }
}

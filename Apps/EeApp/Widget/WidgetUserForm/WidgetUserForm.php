<?php

namespace Apps\EeApp\Widget\WidgetUserForm;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;
use Core\Entity\Entity\Argument;

class WidgetUserForm extends Form {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "") {
        $this->Core = $core;
        $this->Data = $data;

        //On le fait ici pour avoir le formuaire � l'affichage mais aussi � la validation
        $this->Init($data);
    }

    /**
     * Initialisation 
     */
    function Init($data) {
        $this->form = new Form("WidgetUserFormForm", $data);

        $this->form->SetView(__DIR__ . "/View/index.tpl", $this->Core);
        $this->form->SetAction("/View/index.tpl");

        $this->form->Add(array("Type" => "Hidden",
            "Id" => "Id",
        ));

        $this->form->Add(array("Type" => "EntityListBox",
            "Id" => "AppId",
            "Field" => "Name",
            "Entity" => "Apps\EeApp\Entity\EeAppApp",
            "Validators" => ["Required"],
            "Argument" => new Argument("Apps\EeApp\Entity\EeAppApp", "Widget", EQUAL, "1")
        ));

        $this->form->Add(array("Type" => "Button",
            "Id" => "btnAdd",
            "Value" => $this->Core->GetCode("Admin.Add"),
            "CssClass" => "btn btn-primary"
        ));
    }

    /*     * *
     * G�n�rer les formulaire
     */

    function Render() {
        return $this->form->Render();
    }

    /*     * *
     * Valide les donn�es selon les diff�rents formulaires
     */

    function Validate($data = "") {
        $errors = $this->form->Validate($this->Data);

        return $errors;
    }

    /*     * *
     * Charge les controls avec les donn�es de l'entit�
     */

    function Load($entity) {
        $this->form->Load($entity);
    }

    /*     * *
     * Charge l'entit� avec les donn�es des diff�rents formulaire
     */

    function Populate($entity) {
        
    }

    /*     * *
     * Gestion des erreurs
     */

    function Error() {
        $errors[] = $this->form->errors;

        return json_encode(array("statut" => "Error", "message" => $errors));
    }
}

  {{form->Open()}}
  {{form->Error()}}
  {{form->Success()}}

  {{form->Render(Id)}}
  {{form->Render(BlockId)}}
 
  <div class='form-group'>
      <label>{{GetCode(Cms.LinkName)}}</label>
      {{form->Render(Name)}}
  </div>  
  
  <div class='form-group'>
      <label>{{GetCode(Cms.Url)}}</label>
      {{form->Render(Url)}}
  </div>  
  
   <div class='form-group' id='additionnalData'>
   </div>
   
   <div class='center form-group ' >   
       {{form->Render(btnSaveBlockItem)}}
   </div>  
  {{form->Close()}}

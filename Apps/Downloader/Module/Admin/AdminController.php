<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Downloader\Module\Admin;

use Core\Control\Button\Button;
use Core\Controller\AdministratorController;
use Core\View\View;
use Core\View\ElementView;
use Core\Utility\Format\Format;
use Core\Control\TabStrip\TabStrip;

use Apps\Downloader\Widget\RessourceForm\RessourceForm;
use Apps\Downloader\Entity\DownloaderRessource;
use Apps\Downloader\Entity\DownloaderUserRessource;

 class AdminController extends AdministratorController
 {
    /**
     * Creation
     */
    function Create()
    {
    }

    /**
     * Initialisation
     */
    function Init()
    {
    }

    /**
     * Affichage du module
     */
    function Show($all=true)
    {
      $view = new View(__DIR__."/View/Home.tpl", $this->Core);
     
         //Bouton pour créer ajouter une ressource
         $btnNewRessource = new Button(BUTTON, "btnNewRessource");
         $btnNewRessource->Value = $this->Core->GetCode("Downloader.NewRessource");
         $btnNewRessource->CssClass = "btn btn-info";
         $btnNewRessource->OnClick = "DownloaderAction.ShowAddRessource();";
         $view->AddElement($btnNewRessource);
      
        $tabRessource = new TabStrip("tabRessource", "Downloader");
        $tabRessource->AddTab($this->Core->GetCode("Downloader.ressource"), $this->GetTabRessource());
        $tabRessource->AddTab($this->Core->GetCode("Downloader.userRessource"), $this->GetTabUserRessource());
        //$tabMooc->AddTab($this->Core->GetCode("Mooc.Mooc"), $this->GetTabMooc());

        $view->AddElement(new ElementView("tabRessource", $tabRessource->Render()));

        return $view->Render();
    }


    /**
     * Charge les ressources de l'utilisateur
     */
    function GetTabRessource()
    {
        $ressource = new DownloaderRessource($this->Core);
        $ressources = $ressource->GetAll();

        $view = new View(__DIR__."/View/tabRessource.tpl", $this->Core);
        
        $view->AddElement($ressources);
        
        return $view->Render();
    }


    /***
     * Usser Ressource
     */
    function GetTabUserRessource(){
        $ressource = new DownloaderUserRessource($this->Core);
        $ressources = $ressource->GetAll();

        $view = new View(__DIR__."/View/tabUserRessource.tpl", $this->Core);
        
        $view->AddElement($ressources);
        
        return $view->Render();
    }


    /***
     * Save a ressource
     */
    function SaveRessource($data){

      $ressourceForm = new RessourceForm($this->Core, $data);

        if ($ressourceForm->Validate($data)) {

            $ressource = new DownloaderRessource($this->Core);

            if ($data["Id"] != "") {
                $ressource->GetById($data["Id"]);
            } else {
               $ressource->Code->Value = Format::ReplaceForUrl($data["Name"]);
               $ressource->UserId->Value = $this->Core->User->IdEntite;
            }

            $ressourceForm->Populate($ressource);
            $ressource->Save();
            
            if($data["Id"] == ""){
                $ressource->IdEntite = $this->Core->Db->GetInsertedId();
            }
            $ressource->SaveDocument($data["dvUpload-UploadfileToUpload"]);
           
            //Sauvegarde les images
            $ressource->SaveImage($data["dvUpload-UploadImagefileToUpload"], true, false);
              
            return true;
        }

        return false;
    }

    /***
     * Delete the ressource
     */
    public function DeleteRessource($ressourceId){
        $ressource = new DownloaderRessource($this->Core);
        $ressource->GetById($ressourceId);
        $ressource->Delete();
    }
 }?>

<?php

namespace Apps\Profil\Widget\ProfilForm;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;

class ProfilForm extends Form {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "", $modeAdmin = true) {
        $this->Core = $core;
        $this->Data = $data;

        //On le fait ici pour avoir le formuaire � l'affichage mais aussi � la validation
        $this->Init($data, $modeAdmin);
    }

    /**
     * Initialisation 
     */
    function Init($data, $modeAdmin) {
        $this->form = new Form("ProfilFormForm", $data);

        $this->form->SetView(__DIR__ . "/View/index.tpl", $this->Core);
        $this->form->SetAction("/View/index.tpl");

        $this->form->Add(array("Type" => "Hidden",
            "Id" => "Id",
        ));
        
        $this->form->AddElementView(new ElementView("modeAdmin", $modeAdmin));
        
        $this->form->Add(array("Type" => "EntityListBox",
            "Id" => "GroupeId",
             "Entity" => "Core\Entity\Group\Group",
        ));
        
        $this->form->Add(array("Type" => "TextBox",
            "Id" => "Name",
            "Validators" => ["Required"]
        ));
        
        $this->form->Add(array("Type" => "TextBox",
            "Id" => "FirstName",
            "Validators" => ["Required"]
        ));
        
        $this->form->Add(array("Type" => "DateBox",
            "Id" => "BirthDate",
        ));

        $this->form->Add(array("Type" => "TextArea",
            "Id" => "Description",
        ));
         
         $this->form->Add(array("Type" => "ListBox",
            "Id" => "IsPublic",
             "Values" => array($this->Core->GetCode("Base.No") => "2", $this->Core->GetCode("Base.Yes") => "1"),
            "Value" => 1
        ));
        
        $this->form->Add(array("Type" => "Upload",
            "Id"  => "Upload",
            "App" => "Profil",
            "Method" => "SaveImageProfil",
        ));

        $this->form->Add(array("Type" => "Button",
            "Id" => "btnSaveProfil",
            "Value" => $this->Core->GetCode("Profil.UpdateProfil"),
            "CssClass" => "btn btn-primary"
        ));
    }

    /*     * *
     * G�n�rer les formulaire
     */

    function Render() {
        return $this->form->Render();
    }

    /*     * *
     * Valide les donn�es selon les diff�rents formulaires
     */

    function Validate($data = "") {
        $errors = $this->form->Validate($this->Data);

        return $errors;
    }

    /*     * *
     * Charge les controls avec les donn�es de l'entit�
     */

    function Load($entity) {
        $this->form->Load($entity);
    }

    /*     * *
     * Charge l'entit� avec les donn�es des diff�rents formulaire
     */

    function Populate($entity) {
        
    }

    /*     * *
     * Gestion des erreurs
     */

    function Error() {
        $errors[] = $this->form->errors;

        return json_encode(array("statut" => "Error", "message" => $errors));
    }

}

<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Shop\Entity;

use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class ShopProduct extends Entity {

    //Constructeur
    function __construct($core) {
        //Version
        $this->Version = "2.0.0.0";

        //Nom de la table 
        $this->Core = $core;
        $this->TableName = "ShopProduct";
        $this->Alias = "ShopProduct";

        $this->ShopId = new Property("ShopId", "ShopId", NUMERICBOX, true, $this->Alias);
        $this->Name = new Property("Name", "Name", TEXTBOX, true, $this->Alias);
        $this->Code = new Property("Code", "Code", TEXTBOX, true, $this->Alias);
        $this->Description = new Property("Description", "Description", TEXTAREA, true, $this->Alias);
        $this->Status = new Property("Status", "Status", NUMERICBOX, true, $this->Alias);
        $this->Price = new Property("Price", "Price", TEXTBOX, false, $this->Alias);

        //Creation de l entité 
        $this->Create();

        //Repertoire de stockage des images    
        $this->DirectoryImage = "Data/Apps/Shop/Product/";
    }
    
    public function GetImageMiniPath() {
         
        if (file_exists("Data/Apps/Shop/Product/" . $this->IdEntite)) {
        
            $images = $this->GetImages();
            return $images[0];
            
        } else {
            return $this->Core->GetPath("/images/nophoto.png");
        }
    }
}

?>
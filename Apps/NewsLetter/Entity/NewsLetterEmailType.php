<?php 
/*
* PuzzleApp
* Webemyos
* Jérôme Oliva
* GNU Licence
*/

namespace Apps\NewsLetter\Entity;
use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class NewsLetterEmailType extends Entity  
{
	//Constructeur
	function __construct($core)
	{
		//Version
		$this->Version ="2.0.0.0"; 

		//Nom de la table 
		$this->Core=$core; 
		$this->TableName="NewsLetterEmailType"; 
		$this->Alias = "NewsLetterEmailType"; 

		$this->Code = new Property("Code", "Code", TEXTAREA,  false, $this->Alias); 
		$this->Libelle = new Property("Libelle", "Libelle", TEXTAREA,  false, $this->Alias); 

		//Creation de l entité 
		$this->Create(); 
	}
}
?>
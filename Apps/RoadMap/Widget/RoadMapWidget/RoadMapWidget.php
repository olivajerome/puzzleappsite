<?php

namespace Apps\RoadMap\Widget\RoadMapWidget;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;

class RoadMap  {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "") {
        $this->Core = $core;
        $this->Data = $data;

        //On le fait ici pour avoir le formuaire � l'affichage mais aussi � la validation
        $this->Init($data);
    }

    /*     * *
     * G�n�rer les formulaire
     */

    function Render() {
        return $this->form->Render();
    }
}

<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Comunity\Module\Member;

use Core\Control\Button\Button;
use Core\Controller\Controller;
use Core\View\View;
use Core\View\ElementView;
use Core\Entity\Entity\Argument;
use Apps\Forum\Helper\MessageHelper;

use Core\Dashboard\DashBoardManager;

/*
 * 
 */

class MemberController extends Controller {

    /**
     * Constructeur
     */
    function _construct($core = "") {
        $this->Core = $core;
    }

    function Index() {
        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);
        
        
        $appComunity = DashBoardManager::GetApp("Comunity", $this->Core);
        $view->AddElement(new ElementView("wall",  $appComunity->LoadMyWall()));

        
        return $view->Render();
    }
}

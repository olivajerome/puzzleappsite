<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


namespace Apps\Comunity\Module\Home;

use Core\Control\Button\Button;

use Core\Controller\Controller;
use Core\View\View;
use Core\View\ElementView;
use Core\Control\Upload\Upload;
use Core\Entity\Entity\Argument;

use Apps\Comunity\Widget\UserProjetWidget\UserProjetWidget;
use Apps\Comunity\Widget\ActionProjetWidget\ActionProjetWidget;
use Apps\Comunity\Widget\MemberProjetWidget\MemberProjetWidget;

use Apps\Comunity\Entity\ComunityComunity;
use Apps\Comunity\Entity\ComunityFollow;

use Apps\Comunity\Helper\ComunityHelper;


use Core\Dashboard\DashBoardManager;

 class HomeController extends Controller
 {
    /**
     * Constructeur
     */
    function __construct($core="")
    {
          $this->Core = $core;
    }
    
    /**
     * Création d'une Comunityion
     */
    function NewComunityion()
    {
        $view = new View(__DIR__."/View/newComunityion.tpl", $this->Core);
         
        $view->AddElement(new ElementView("Connected", $this->Core->IsConnected()));
         
        $uploadComunity = new Upload("Comunity", "uploadComunity", "", "uploadComunity");
        $view->AddElement(new ElementView("uploadComunity",$uploadComunity->Show()));
        $view->AddElement(new ElementView("reseau", $this->Core->Reseau));
     
        return  $view->Render(); 
    }
    
    /**
     * Détail d'un rencontre thématique
     */
    function Group($groupId, $completePage = true)
    {
        if(!$this->Core->IsConnected())
        {
           $view = new View(__DIR__."/View/notConnected.tpl", $this->Core);
           
           return $view->Render(); 
        }
        
        $Comunity = new ComunityComunity($this->Core);
        
        
            $view = new View(__DIR__."/View/detailGroup.tpl", $this->Core);
       
            $view->AddElement(new ElementView('completePage', $completePage));
            $Comunity->GetById($groupId);
            
            $userProjetWidget = new UserProjetWidget($this->Core);
            $view->AddElement(new ElementView("userProjetWidget", $userProjetWidget->Render()));
           
            $actionProjetWidget = new ActionProjetWidget($this->Core, $groupId);
            $view->AddElement(new ElementView("ActionProjetWidget", $actionProjetWidget->Render()));
           
            $memberProjetWidget = new MemberProjetWidget($this->Core, $groupId, $Comunity);
            $view->AddElement(new ElementView("MemberProjetWidget", $memberProjetWidget->Render()));
           
            
            $view->AddElement(new ElementView("membres", $this->GeMembers($Comunity) ));
            $view->AddElement(new ElementView("followers", $this->GetFollower($Comunity)));
        
        $view->AddElement(new ElementView("Comunity", $Comunity));
        
        if($this->Core->IsConnected()){        
            $view->AddElement(new ElementView("userImage", $this->Core->User->GetImage()));
        } 
        
        $view->AddElement(new ElementView("isConnected", $this->Core->IsConnected()));
        
        
        $messages = $Comunity->GetFirstMessage();
            
        $view->AddElement(new ElementView("Messages", $this->GetMessages($groupId, "",  $reseauId)));
        $view->AddElement(new ElementView("nbCommentaire", count($messages)));
        
        //Upload Image
       // $uploadComunity = new Upload("Comunity", "uploadComunity", "", "uploadComunity");
        
       $uploadComunity = new Upload("Comunity", "uploadComunity", "", "uploadComunity");
       $uploadComunity->RenderType = "Icone";
       $uploadComunity->Accept ="";
       
        $view->AddElement(new ElementView("uploadComunity",$uploadComunity->Show()));
       
        return $view->Render();
    }
      
       /***
    * Obitent les projets qui sont épinglé à la page d'accueil
    */
   function GetPinReseauProjet()
   {
       $reseau = new CoopereReseau($this->Core);
       $reseau =  $reseau->GetByCode(Coopere::$reseauName);
        
       $Comunity = new ComunityComunity($this->Core);
       $Comunity->AddArgument(new Argument("Apps\Comunity\Entity\ComunityComunity", "ReseauId", EQUAL, $reseau->IdEntite));
       $Comunity->AddArgument(new Argument("Apps\Comunity\Entity\ComunityComunity", "HomePage", EQUAL, 1));
       $Comunity->AddArgument(new Argument("Apps\Comunity\Entity\ComunityComunity", "Status", EQUAL, 1));

       $projets = $Comunity->GetByArg();
       
       if(count($projets) > 0){
           
           foreach($projets as $projet)
           {
               $html .= "<div class='col-md-4 '><div class='projetTumb' style='background-image: url(\"".$projet->GetFirstImageUrl()."\"); ' >";
               $html .=  "<a href='".$this->Core->GetPath("/" .Coopere::$reseauName . "/ParlonsEn/Rencontre/" . $projet->IdEntite )."'>";
               //$html .= $projet->GetFirstImage();
               $html .= "<label>".$projet->Title->Value."</label>";
               $html .= "</a></div></div>";
           }
       } 
      
       return $html;
   }
   
    /***
     * Retourne les photos des membres
     */
    function GeMembers($projet)
    {
        $members = $projet->GetProjetMember();
        
        $html ="";
        foreach($members as $member)
        {
            $html .='<div class="avatarmini-img avatarmini" style="cursor: pointer;">'; 
            $html .='<img src="'.$member->User->Value->GetImage().'" alt="images" /> '; 
            $html .=' </div>';   
        }
    
        if(count($members)> 1){
            $memberNumber =  count($members) . " membres";
        }
        else
        {
            $memberNumber =  count($members) . " membre";
        }
        
        return $html . "<span style='margin-left: 50px'>" . $memberNumber . "</span>";
    }
    
    /***
     * Obitient le nombre de follower
     */
    function GetFollower($projet)
    {
        
        $follow = new ComunityFollow($this->Core);
        $follower = $follow->Find("ComunityId=" . $projet->IdEntite);
        
        if(count($follower)> 1)
        {
           return   count($follower) . " abonnés";
        }
        else
        {
          return count($follower) . " abonné";
        }
    }
    
    /***
     * Retourne tous les messages
     */
    function GetMessages($ComunityId, $messageId = "", $reseauId = "" , $projetId = "", $page = "", $postId ="")
    {
        $view = new View(__DIR__."/View/detailMessage.tpl", $this->Core);
     
        $request = "SELECT message.Id as messageId, message.Message as text, message.DateCreated as dateCreated, user.Pseudo as pseudo, user.Image as image,
                    message.type as messageType,            

                    count(distinct(lk.id)) as nbLike,
                    count(distinct(comment.Id)) as nbComment,
                    GROUP_CONCAT(distinct(userLike.image) SEPARATOR ':') as userLike ,
                    
                    disccus.type as discusType, disccus.Title as ComunityTitle,
                    
                    disccus.Id as projetId


                    FROM ComunityMessage as message 

                    JOIN ee_user as user on user.Id = message.UserId 
                    JOIN ComunityComunity as disccus on message.ComunityId = disccus.Id   

                    LEFT JOIN ComunityComment as comment on comment.MessageId = message.Id
                    LEFT JOIN ComunityLike as lk on lk.EntityId = message.Id and lk.EntityName = 'message'
                    LEFT JOIN ee_user as userLike on userLike.id = lk.UserId
                    ";
        
        

        if($messageId != ""){
              $request .=" Where message.Id =  " . $messageId; 
        } 
        else if($ComunityId != ""){
            $request .=" Where disccus.Id =  " . $ComunityId; 
        }
         
        if($projetId != "")
        {
              $request .=" WHERE ComunityId in(".$projetId.")"; 
        }
        
        if($postId != ""){
            $request .=" WHERE message.Id = ".$postId; 
        }
        
        $request .=" GROUP BY message.Id Order By message.Id Desc ";
        
        if($page == "")
        {
            $request .= " limit 0,5";
        }
        else
        {
            $request .= " limit ".($page * 5). ",5";
        }
        
       // echo $request;
        
        $results = $this->Core->Db->GetArray($request);
       
        foreach($results as $message)
        {
            
            if($message["messageType"] == 5 )
            {
               $view = new View(__DIR__."/View/otherMessage.tpl", $this->Core);
            }
            else
            {
               $view = new View(__DIR__."/View/detailMessage.tpl", $this->Core);
            }
            
          //  $view->ClearElement();
            
            $view->AddElement(new ElementView("image",$this->Core->GetPath(str_replace("../Data", "/Data", $message["image"]))));
            $view->AddElement(new ElementView("pseudo",$message["pseudo"] ));
            $view->AddElement(new ElementView("text", str_replace("&acute;",  "'", $message["text"])));
            $view->AddElement(new ElementView("nbLike",$message["nbLike"] ));
            $view->AddElement(new ElementView("nbComment",$message["nbComment"] ));
            $view->AddElement(new ElementView("messageId",$message["messageId"] ));
            $view->AddElement(new ElementView("userImage", $this->Core->User->GetImage()));
            //$view->AddElement(new ElementView("userImage", $this->Core->User->GetImage()));
            $view->AddElement(new ElementView("projet", ($message["discusType"] == 2 ) ? "<span class='fa fa-circle' style='font-size:6px;margin-left:5px'></span><span class='projet'><a href='".$this->Core->GetPath("/" .$reseau->Code->Value . "/ParlonsEn/Rencontre/" .$message["projetId"])."'>" . $message["ComunityTitle"] . "</a></span>" : "" ));
            
            $dateNow = new \DateTime("now");
            $dateModif = new \DateTime($message["dateCreated"]);
            $intervalle = $dateNow->diff($dateModif);
         
            $view->AddElement(new ElementView("day", $intervalle->d . " jours" ));
            
            if($postId != ""){
                
                $Comunity = DashBoardManager::GetApp("Comunity", $this->Core);
                $view->AddElement(new ElementView("comments", $Comunity->GetComment("", $postId)));
            }
            else
            {
             $view->AddElement(new ElementView("comments", ""));
            }
            
            if($message["userLike"] != "")
            {
                $users = explode(":", $message["userLike"] );
                $userLikeHtml ="";
                
                foreach($users as $user)
                {
                    $userLikeHtml .= "<div class='avatarmini-img avatarmini'>
                        <img src='".$this->Core->GetPath('\\'.$user)."' /></div>"; 
                }
                
                 $view->AddElement(new ElementView("userLike", $userLikeHtml));
            } 
            else
            {
                 $view->AddElement(new ElementView("userLike", ""));
           }
            
            $html .= $view->Render();
        }
        
        return $html;
    }
    
    
    
    
    /**
     * Toutes les rencontres d'un réseau
     */
    function Rencontres($reseauCode)
    {
     
       if(!$this->Core->IsConnected())
       {
           $reseau = new CoopereReseau($this->Core);
           $reseau =  $reseau->GetByCode($reseauCode);
           
           $view = new View(__DIR__."/View/notConnected.tpl", $this->Core);
           $view->AddElement(new ElementView("Reseau", $reseau));
           
               $reseau = new CoopereReseau($this->Core);
           $reseau =  $reseau->GetByCode(Coopere::$reseauName);
           
           $view = new View(__DIR__."/View/notConnected.tpl", $this->Core);
           $view->AddElement(new ElementView("Reseau", $reseau));
           $view->AddElement(new ElementView("pinProject", $this->GetPinReseauProjet()));
         
           
           return $view->Render(); 
       }
        
        if(!(MembreHelper::IsAutorized($this->Core, Coopere::$reseauName,  $this->Core->User->Email->Value) || MembreHelper::IsMemberAutorized($this->Core, Coopere::$reseauName,  $this->Core->User->Email->Value))) 
        {
           $reseau = new CoopereReseau($this->Core);
           $reseau =  $reseau->GetByCode(Coopere::$reseauName);
           
           $view = new View(__DIR__."/View/notAutorized.tpl", $this->Core);
           $view->AddElement(new ElementView("Reseau", $reseau));
           $view->AddElement(new ElementView("pinProject", $this->GetPinReseauProjet()));
           
           return $view->Render(); 
        } 
       
       $view = new View(__DIR__."/View/rencontres.tpl", $this->Core);
      
       $reseau = new CoopereReseau($this->Core);
       $reseau =  $reseau->GetByCode($reseauCode);
        
       $Comunity = new ComunityComunity($this->Core);
       $Comunity->AddArgument(new Argument("Apps\Comunity\Entity\ComunityComunity", "ReseauId", EQUAL, $reseau->IdEntite));
       
       $view->AddElement(new ElementView("Comunity", $Comunity->GetByArg()));
       $view->AddElement(new ElementView("Path", $this->Core->GetPath("/" . $reseauCode)));
       
       $userProjetWidget = new UserProjetWidget($this->Core, $reseauCode);
       $view->AddElement(new ElementView("userProjetWidget", $userProjetWidget->Render()));
       
       $view->AddElement(new ElementView("reseauCode", $reseauCode));
       
       $view->AddElement(new ElementView("HomeWall", $this->LoadHome($reseau->IdEntite)));
       
       
       return $view->Render();
    }
    
    /***
     * Charge la pages des projets
     */
    public function LoadHome($reseauId)
    {
        $userProjets = ComunityHelper::GetUserProjet($this->Core, $reseauId);
        
        if($userProjets == "")
        {
            $view = new View(__DIR__."/View/noprojet.tpl", $this->Core);
            return $view->Render();
        }
        else
        {
            $view = new View(__DIR__."/View/homrProjet.tpl", $this->Core);
            
            $html = "<input type='hidden' id='hdComunityId' value='false'/> <div id='dashboardDiscus'>" .$this->GetMessages("", "", $reseauId, $userProjets)."</div>";
            
            return $html;
        }
    }
    
    /**
     * Ajoute un message à la Comunityion courante
     * @param type $users
     * @param type $message
     * @param type $objet
     * @param type $ComunityId
     * @param type $images
     * @param type $type
     * @param type $reseauCode
     * @param type $documents
     * @param type $typeMessage
     */
    function AddMessage($users, $message, $objet, $ComunityId, $images = "", $type = 0, $reseauCode = "", $documents ="", $typeMessage = "")
    {
       //TODO DEPLACER CETTE METHOSE DANS UN HELPER
       if($images != "")
       {
           $images = explode(";", $images);
           $messageImage = "";
           
           $messageId =  ComunityHelper::AddMessage($this->Core, $ComunityId, $message);
           
           //On déplace les images dans le bon répertoire
           foreach($images as $image)
           {
                $path = explode("/", $image);
                   
                    if(!file_exists("Data/Apps/Comunity/" .$ComunityId))
                    {
                        mkdir("Data/Apps/Comunity/" . $ComunityId);
                    }
                    if(!file_exists("Data/Apps/Comunity/" .$ComunityId. "/Message"))
                    {
                        mkdir("Data/Apps/Comunity/" . $ComunityId . "/Message" );
                    }
                    
                    //TODO RECUPERE L'ID DE L4UTILISATEUR SI C'EST UNE INSCIRPTIon
                    if(!file_exists("Data/Apps/Comunity/" .$ComunityId . "/Message/" . $messageId))
                    {
                        mkdir("Data/Apps/Comunity/" . $ComunityId . "/Message/" . $messageId );
                    }

                   $fileName = $path[count($path) -1];

                   rename("Data/Tmp/" .$fileName, "Data/Apps/Comunity/". $ComunityId . "/Message/" . $messageId ."/" .$fileName);
                   
                  $messageImage .=  "<img src='".$this->Core->GetPath("/Data/Apps/Comunity/". $ComunityId . "/Message/" . $messageId ."/" .$fileName)."' >";  
           }
           
           // C'est un fichier Type Image
           $message = "<p>". $message . "</p>" .  $messageImage;
           
           ComunityHelper::UpdateMessage($this->Core, $messageId, $message);
       }
       
       else if($documents != "")
       {
           $documents = explode(";", $documents);
           $messageImage = "";
           
           $messageId =  ComunityHelper::AddMessage($this->Core, $ComunityId, $message);
           
           //On déplace les images dans le bon répertoire
           foreach($documents as $document)
           {
                $path = explode("/", $document);
                   
                    if(!file_exists("Data/Apps/Comunity/" .$ComunityId))
                    {
                        mkdir("Data/Apps/Comunity/" . $ComunityId );
                    
                    }
                    
                    if(!file_exists("Data/Apps/Comunity/" . $ComunityId . "/Message"))
                    {
                       mkdir("Data/Apps/Comunity/" . $ComunityId . "/Message" );
                    }
                    
                    //TODO RECUPERE L'ID DE L4UTILISATEUR SI C'EST UNE INSCIRPTIon
                    if(!file_exists("Data/Apps/Comunity/" .$ComunityId . "/Message/" . $messageId))
                    {
                        mkdir("Data/Apps/Comunity/" . $ComunityId . "/Message/" . $messageId );
                    }

                   $fileName = $path[count($path) -1];

                   rename("Data/Tmp/" .$fileName, "Data/Apps/Comunity/". $ComunityId . "/Message/" . $messageId ."/" .$fileName);
                   
                  $messageDocument .=  "<a  target='_blank' href='" . $this->Core->GetPath("/Data/Apps/Comunity/". $ComunityId . "/Message/" . $messageId ."/" .$fileName)."' ><i class='fa fa-file fa-2x' >&nbsp;</i>".$fileName."</a>";  
           }
           
           // C'est un fichier Type Document
           $message = "<p>". $message . "</p>" .  $messageDocument;
           
           ComunityHelper::UpdateMessage($this->Core, $messageId, $message);
       }
       
       else if($ComunityId == "")
       { 
         $ComunityId = ComunityHelper::CreateConversation($this->Core, $users, $message, $objet);
         $messageId = $this->Core->Db->GetInsertedId();
       }
       else{
           
          $message = $message;
            
          $messageId =  ComunityHelper::AddMessage($this->Core, $ComunityId, $message);
          ComunityHelper::UpdateLastMessage($this->Core, $ComunityId, $message);
       }
    
       return json_encode(array("discuId" => $ComunityId, "message" => $this->GetMessages($ComunityId, $messageId)  ));
    } 
    
    /**
     * Charge les documents d'un projet
     */
    function LoadDocument($projetId)
    {
        $view = new View(__DIR__."/View/document.tpl", $this->Core);
        $Comunity = new ComunityComunity($this->Core);
        $Comunity->GetById($projetId);
                
        $view->AddElement(new ElementView("Comunity", $Comunity));
        
        return $view->Render();
    }
    
    public function LoadComunityProjet($projetId)
    {
        $view = new View(__DIR__."/View/loadProjetComunity.tpl", $this->Core);

         $ComunityId = ComunityHelper::HavePrivateComunityion($this->Core, $projetId);

         
         if(count($ComunityId) > 0)
         {
           $discus = $ComunityId[0];
             
           $view->AddElement(new ElementView("projetComunityId", $discus->IdEntite ));
           $homeController = new \Apps\Comunity\Module\Home\HomeController($this->Core);
           
           $view->AddElement(new ElementView("messages", $homeController->LoadComunity($discus->IdEntite) ));
         } else {
             
           $view->AddElement(new ElementView("messages", "Ceci est le début de la conversation sur ce projet" ));
           $view->AddElement(new ElementView("projetComunityId", "" ));
         }

         $uploadComunity = new Upload("Comunity", "uploadComunity", "ActionProjetWidget.SendAttachment()", "uploadComunity");
         $uploadComunity->RenderType = "Icone";
         $uploadComunity->Accept ="";
         $uploadComunity->RenderDocument = false;
         
         
         $view->AddElement(new ElementView("uploadComunity",$uploadComunity->Show()));
         
         return $view->Render();
    }
    
    
 }
 
<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Tips\Module\Admin;

use Core\Control\Button\Button;
use Core\Controller\AdministratorController;
use Core\View\View;
use Core\View\ElementView;
use Core\Control\TabStrip\TabStrip;
use Core\Control\EntityGrid\EntityGrid;
use Core\Control\EntityGrid\EntityColumn;
use Core\Control\EntityGrid\EntityIconColumn;
use Core\Control\EntityGrid\EntityFunctionColumn;
use Core\Entity\Entity\Argument;

/*
 * 
 */
 class AdminController extends AdministratorController
 {
    /**
     * Constructeur
     */
    function _construct($core="")
    {
          $this->Core = $core;
    }

    /**
     * Creation
     */
    function Create()
    {
    }

    /**
     * Initialisation
     */
    function Init()
    {
    }

    /**
     * Affichage du module
     */
    function Show($all=true)
    {
         return $this->Index();
    }
    
    /*
     * Get the home page
     */

     function Index() {

        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);

        $tabTips = new TabStrip("tabTips", "Tips");
        $tabTips->AddTab($this->Core->GetCode("Tips.Tips"), $this->GetTabTips());
        $tabTips->AddTab($this->Core->GetCode("Tips.TipsByUser"), $this->GetTabTipsByUser());
      
        $view->AddElement(new ElementView("tabTips", $tabTips->Render()));
        return $view->Render();
    }
    /*     * *
     * Categorie dof the Forum
     */

     function GetTabTips() {

        $gdTips = new EntityGrid("gdTips", $this->Core);
        $gdTips->Entity = "Apps\Tips\Entity\TipsTips";
        $gdTips->App = "Tips";
        $gdTips->Action = "GetTabTips";

        $gdTips->AddArgument(new Argument("Apps\Tips\Entity\TipsTips", "Status", NOTEQUAL, 99));
        $btnAdd = new Button(BUTTON, "btnAddTips");
        $btnAdd->Value = $this->Core->GetCode("Tips.AddTips");

        $gdTips->AddButton($btnAdd);

        $gdTips->AddColumn(new EntityColumn("Name", "Name"));


        $gdTips->AddColumn(new EntityIconColumn("Action",
                        array(array("EditIcone", "Tips.EditTips", "Tips.EditTips"),
                    array("DeleteIcone", "Tips.DeleteTips", "Tips.DeleteTips"),
                        )
        ));

        return $gdTips->Render();
    }

    /***
     * Get Tips By user
     */
    function GetTabTipsByUser(){
        $gdTips = new EntityGrid("gdTipsUser", $this->Core);
        $gdTips->Entity = "Apps\Tips\Entity\TipsUser";
        $gdTips->App = "Tips";
        $gdTips->Action = "GetTabTipsByUser";

        $gdTips->AddColumn(new EntityFunctionColumn("User", "GetUser"));
        $gdTips->AddColumn(new EntityFunctionColumn("Tips", "GetTips"));


       
        return $gdTips->Render();
    }
          
          /*action*/
 }?>
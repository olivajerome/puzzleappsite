<?php

namespace Apps\Profil\Widget\AdminDashBoard;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;
use Core\Entity\Entity\Argument;
use Core\Entity\User\User;

class AdminDashBoard {
    
    public function __construct($core) {
        $this->Core = $core;
    }
    
    /***
     * Render the widget
     */
    public function Render(){
        $view = new View(__DIR__ . "/View/adminDashboard.tpl", $this->Core);
       
        $user = new User($this->Core);
        
        $view->AddElement(new ElementView("User", $user->Find(" GroupId = 2 order by Id desc limit 0,5")));
        
        return $view->Render();
    }
}

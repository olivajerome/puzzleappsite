<?php 
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Apps\RoadMap\Helper;

use Core\Utility\Date\Date;
use Core\Utility\Format\Format;
use Core\Entity\Entity\Argument;


use Apps\RoadMap\Widget\RoadMapForm\RoadMapForm;
use Apps\RoadMap\Entity\RoadMapRoadMap;

use Apps\RoadMap\Widget\ColumnForm\ColumnForm;
use Apps\RoadMap\Entity\RoadMapColumn;

use Apps\RoadMap\Widget\TicketForm\TicketForm;
use Apps\RoadMap\Entity\RoadMapTicket;

use Apps\YourStartup\Entity\YourStartupProjet;

use Apps\Comunity\Helper\ComunityHelper;

/***
 * Class utilitaire 
 */
class RoadMapHelper
{
    /***
     *  Sauvegarde une road Map
     **/
    public static function SaveRoadMap($core, $data){
    
        $roadMapForm = new RoadMapForm($core, $data);

        //On valide le formulaire
        if ($roadMapForm->Validate()) {

            $roadMap= new RoadMapRoadMap($core);

            if (isset($data["Id"]) && $data["Id"] != "") {
                $id = $data["Id"];
                $roadMap->GetById($data["Id"]);
            } else {
                $roadMap->UserId->Value= $core->User->IdEntite;
            }   

            $roadMap->Code->Value = Format::ReplaceForUrl($data["Name"]);
          
            $roadMapForm->Populate($roadMap);

            //On sauvegarde
            $roadMap->Save();

            if ($id == "") {
                $id = $core->Db->GetInsertedId();
            }

            return $roadMapForm->Success(array("Id" => $id));
        } else {
            return $roadMapForm->Error();
        }
    }   
    
    /***
     * Supprime une RoadMap
     */
    public static function DeleteRoadMap($core, $roadMapId){
       
        $roadMap = new RoadMapRoadMap($core);
        $roadMap->GetById($roadMapId);
        
        $column = new RoadMapColumn($core);
        $columns = $column->Find("RoadMapId = " .$roadMapId);

        foreach($columns as $column){

            $ticket = new RoadMapTicket($core);
            $tickets = $ticket->Find("ColumnId = " . $column->IdEntite);
            
            foreach($tickets as $ticket){
            //Suppression des messages de la communauté
             self::DeleteTicket($core,$ticket->IdEntite);
            }
            
            $column->Delete();
        }
        
        $roadMap->Delete();
    }
    
     /***
     *  Sauvegarde une colonne
     **/
    public static function SaveColumn($core, $data){
    
        $columnForm = new ColumnForm($core, $data);

        //On valide le formulaire
        if ($columnForm->Validate()) {

            $column= new RoadMapColumn($core);

            if (isset($data["Id"]) && $data["Id"] != "") {
                $id = $data["Id"];
                $column->GetById($data["Id"]);
            }  

            $column->Code->Value = Format::ReplaceForUrl($data["Name"]);
          
            $columnForm->Populate($column);

            //On sauvegarde
            $column->Save();

            if ($id == "") {
                $id = $core->Db->GetInsertedId();
            }

            return $columnForm->Success(array("Id" => $id));
        } else {
            return $columnForm->Error();
        }
    }  
    
     /***
     *  Sauvegarde un ticket
     **/
    public static function SaveTicket($core, $data){
    
        $ticketForm = new TicketForm($core, $data);

        //On valide le formulaire
        if ($ticketForm->Validate()) {

            $ticket= new RoadMapTicket($core);

            if (isset($data["Id"]) && $data["Id"] != "") {
                $id = $data["Id"];
                $ticket->GetById($data["Id"]);
            }  else {
                $ticket->Status->Value = 1;
            }

            $ticket->Code->Value = Format::ReplaceForUrl($data["Name"]);
          
            $ticketForm->Populate($ticket);

            //On sauvegarde
            $ticket->Save();

            if ($id == "") {
                $id = $core->Db->GetInsertedId();
            }

            return $ticketForm->Success(array("Id" => $id));
        } else {
            return $ticketForm->Error();
        }
    }  
    
   
    /***
     * Obtient les roadMap d'un utilisateur
     **/
    public static function GetByUser($core, $userId, $projetId =""){
        $roadMaps = new RoadMapRoadMap($core);
        
        if($projetId != ""){
            $projet = new YourStartupProjet($core);
            $projet->GetById($projetId);
            
            return  $roadMaps->Find("UserId= " . $projet->UserId->Value );
        } else{
            return  $roadMaps->Find("UserId= " . $userId );
        }
    }
    
    /**
     * Obtient les Colonnes d'une roadMap
     **/
    public static function GetColumns($core, $roadMapId){
    
        $columns = new RoadMapColumn($core);
        return $columns->Find('RoadMapId=' .$roadMapId);     
    }
    
    /***
     * Change la colonne d'un ticket
     */
    public static function ChangeColumn($core, $data){
        
        $ticket = new RoadMapTicket($core);
        $ticket->GetById($data["ticketId"]);
        $ticket->ColumnId->Value = $data["columnId"];
        $ticket->Save();
    }
    
    /***
     * Supprime une colonne et tout ces tickers
     */
    public static function DeleteColumn($core, $columnId){
        $column = new RoadMapColumn($core);
        $column->GetById($columnId);
       
        $ticket = new RoadMapTicket($core);
        $tickets = $ticket->Find("ColumnId = " .$columnId );
        
        foreach($tickets as $ticket){
            self::DeleteTicket($core,$ticket->IdEntite);
        }
        
        $column->Delete();
    }
    
    /***
     * Supprime un ticket
     */
    public static function DeleteTicket($core, $ticketId){
        
        //ComunityHelper::DeleteByEntity($core, "Ticket", $ticketId);
        $ticket = new RoadMapTicket($core);
        $ticket->GetById($ticketId);
        $ticket->Delete();
    }
}    
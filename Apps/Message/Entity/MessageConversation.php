<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Message\Entity;

use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Argument;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;
use Apps\Message\Entity\MessageUser;
use Apps\Message\Entity\MessageMessage;
use Core\Control\Image\Image;
use Core\Entity\User\User;
use Core\Utility\Date\Date;

class MessageConversation extends Entity {

    //Constructeur
    function __construct($core) {
        //Version
        $this->Version = "2.0.0.0";

        //Nom de la table 
        $this->Core = $core;
        $this->TableName = "MessageConversation";
        $this->Alias = "MessageConversation";

        $this->Type = new Property("Type", "Type", NUMERICBOX, true, $this->Alias);
        $this->UserId = new Property("UserId", "UserId", NUMERICBOX, true, $this->Alias);
        $this->DateCreated = new Property("DateCreated", "DateCreated", DATEBOX, false, $this->Alias);
        $this->Title = new Property("Title", "Title", TEXTAREA, false, $this->Alias);
        $this->SubTitle = new Property("SubTitle", "SubTitle", TEXTAREA,  false, $this->Alias); 
        $this->Description = new Property("Description", "Description", TEXTAREA, false, $this->Alias);
        $this->Status = new Property("Status", "Status", NUMERICBOX, true, $this->Alias);
        $this->DateClotured = new Property("DateClotured", "DateClotured", DATEBOX, false, $this->Alias);
        $this->DateLastMessage = new Property("DateLastMessage", "DateLastMessage", DATETIMEBOX, false, $this->Alias);
        //      $this->HomePage = new Property("HomePage", "HomePage", NUMERICBOX,  false, $this->Alias); 
        //   $this->ProjetId = new Property("ProjetId", "ProjetId", NUMERICBOX,  false, $this->Alias); 
        //Creation de l entité 
        $this->Create();
    }

    /**
     * Retourne les membres de la conversation
     */
    public function GetUserImage($limit = null) {
        $users = new DiscussUser($this->Core);
        $users->AddArgument(new Argument("Apps\Discuss\Entity\DiscussUser", "DiscussId", EQUAL, $this->IdEntite));

        $users = $users->GetByArg();

        $html = "";

        foreach ($users as $user) {
            if ($user->UserId->Value != $this->Core->User->IdEntite) {

                $member = $user->User->Value;
                $html .= "<div title='" . $member->GetPseudo() . "'  class='avatarmini-img avatarmini' style='cursor: pointer;'>
                                <img src=\"" . $member->GetImage() . "\" alt='images'>
                            </div>";
            }
        }

        return $html;
    }

    /**
     * Obtient le premiers message du projet
     */
    public function GetFirstMessage() {
        $discusMessage = new DiscussMessage($this->Core);
        $discusMessage->AddArgument(new Argument("Apps\Discuss\Entity\DiscussMessage", "DiscussId", EQUAL, $this->IdEntite));

        return $discusMessage->GetByArg();
    }

    /**
     * Url de la premiére image
     * @return type
     */
    public function GetFirstImageUrl() {
        return $this->GetImages(true, true);
    }

    /**
     * Obtient la premier image
     */
    public function GetFirstImage() {
        return $this->GetImages(true);
    }

    /**
     * Obtient les images
     */
    public function GetImages($first = false, $url = false) {
        if (file_exists("../Web/Data/Apps/Discuss/" . $this->IdEntite)) {
            $html = "";
            $mini = "<div class='mini'>";
            $i = 0;

            $dh = @opendir("../Web/Data/Apps/Discuss/" . $this->IdEntite);
            if ($dh) {
                while (($fname = readdir($dh)) !== false) {

                    if ($fname != "." && $fname != "..") {
                        $img = new Image($this->Core->GetPath("/Data/Apps/Discuss/" . $this->IdEntite . "/" . $fname));

                        if ($first) {

                            if ($url == true) {
                                return $this->Core->GetPath("/Data/Apps/Discuss/" . $this->IdEntite . "/" . $fname);
                            } else {
                                //  $img->AddStyle("width", "100%");
                                return $img->Show();
                            }
                        }
                        $img->AddStyle("width", "100px");
                        $mini .= $img->Show();

                        $i++;
                    }
                }

                $html .= $mini . "</div>";

                closedir($dh);
            }

            return $html;
        } else {
            if ($url) {
                return $this->Core->GetPath("/images/validation.png");
            } else {
                $img = new Image($this->Core->GetPath("/images/validation.png"));
                return $img->Show();
            }
        }
    }

    /**
     * Obtient l'id des membres
     */
    public function GetMembresIds() {
        $sql = "select GROUP_concat(distinct(UserId)) as ids FROM DiscussUser where DiscussId = " . $this->IdEntite;
        $result = $this->Core->Db->GetLine($sql);

        return $result["ids"];
    }

    /**
     * Retourne les memebre
     */
    public function GetMembres() {
        $sql = "select GROUP_concat(distinct(UserId)) as ids FROM DiscussMessage where DiscussId = " . $this->IdEntite;
        $result = $this->Core->Db->GetLine($sql);

        $users = new User($this->Core);
        $users = $users->Find("id in (" . $result["ids"] . ")");

        $html = "<p class='date'>" . count($users) . " Membres</p>";
        foreach ($users as $user) {
            $html .= "<div title='" . $user->GetPseudo() . "' class='avatarmini-img avatarmini'>";
            $html .= "<img src='" . $user->GetImageMini() . "' alt='images'>";
            $html .= "</div>";
        }

        return $html;
    }

    /**
     * Nombre de message
     */
    function GetNumberMessage() {
        $request = "SELECT count(Id) as nbMessage from  DiscussMessage WHERE DiscussId = " . $this->IdEntite;
        $result = $this->Core->Db->GetLine($request);

        return $result["nbMessage"];
    }

    public function GetLimitTitle() {
        return $this->GetTitle(7);
    }

    /*     * *
     * Selon le type de conversation ,il faut 
     */

    function GetTitle($limit = null) {
        //TODO Selon le type de conversation ile titre peut être différents

        $users = new MessageUser($this->Core);
        $users->AddArgument(new Argument("Apps\Message\Entity\MessageUser", "ConversationId", EQUAL, $this->IdEntite));

        if ($limit != null) {
            $users->SetLimit(1, $limit);
        }

        $users = $users->GetByArg();
        $html = "";
        $userImage = "<div class=''>";
        $userPseudo = "";
        
        foreach ($users as $user) {
            if ($user->UserId->Value != $this->Core->User->IdEntite) {

                if ($userPseudo != "") {
                    $userPseudo .= ",";
                }

                $userPseudo .= $user->User->Value->GetPseudo();
                if(count($users) < 6){
                        $userImage .= "<div  class='imgProfil' style='background-image: url(". $user->User->Value->GetImageMini() . ")' ></div>";
                }
            }
        }

        return $userImage . "</div><div class='userTitle'>" . $userPseudo . "</div>";
    }

    /**
     * Sous Titre
     */
    function GetSubTitle() {
        if ($this->DateLastMessage->Value == "") {
            return "<div>" . $this->SubTitle->Value . "</div>";
        }

        $dateModif = explode("/", $this->DateLastMessage->Value);

        $dateNow = new \DateTime("now");
        $dateModifStamp = new \DateTime($dateModif[0] . "-" . $dateModif[1] . "-" . $dateModif[2]);

        $heure = explode(" ", $dateModif[2]);

        $intervalle = $dateNow->diff($dateModifStamp);

        if ($intervalle->d == 0) {
            $infoDate = "Aujourd'hui (" . $heure[1] . ")";
        } else {
            $infoDate = $intervalle->d . " jour(s)";
        }


        return "<div>" . $this->SubTitle->Value . "</div><div class='day'>" . $infoDate . "</div>";
    }

    /*     * *
     * L'utilisateur à t'il lu la discussion
     */

    public function GetClassReaded() {
        $request = "SELECT Readed FROM DiscussUser WHERE DiscussId =  " . $this->IdEntite . " AND UserId = " . $this->Core->User->IdEntite;
        $result = $this->Core->Db->GetLine($request);

        if ($result["Readed"] == 1) {
            return 'NotRead';
        } else {
            return "Read";
        }
    }

    /*     * *
     * Retourne les membres qui opn rejoint le projet
     */

    public function GetProjetMember() {
        $discusUser = new DiscussUser($this->Core);
        return ($discusUser->find(" DiscussId = " . $this->IdEntite));
    }

    /**
     * Obtient les documents du projet
     */
    public function GetDocument() {
        $documentPath = "../Web/Data/Apps/Discuss/" . $this->IdEntite . "/Message";

        $photos = "<div class='projetImages'><h3>Photos</h3>";
        $documents = "<div class='projetDocument'><h3>Documents</h3><ul>";

        if (file_exists($documentPath)) {
            $dh = @opendir($documentPath);
            if ($dh) {
                //OUveture du repertoire
                while (($fname = readdir($dh)) !== false) {

                    if ($fname != "." && $fname != "..") {
                        $messagePath = $documentPath . "/" . $fname;

                        $df = @opendir($messagePath);
                        if ($df) { {
                                while (($fileName = readdir($df)) !== false) {
                                    if ($fileName != "." && $fileName != "..") {
                                        if ((strstr($fileName, ".txt") != false || strstr($fileName, ".TXT") != false ||
                                                strstr($fileName, ".pdf") || strstr($fileName, ".PDF") ||
                                                strstr($fileName, ".doc") || strstr($fileName, ".DOC"))) {
                                            $documents .= "<li><i class='fa fa-file fa-2x'>&nbsp;</i><a target='_blank' href='" . $this->Core->GetPath(str_replace("../Web", "", $messagePath . "/" . $fileName)) . "'>" . $fileName . "</a></li>";
                                        }
                                    }
                                    if ($fileName != "." && $fileName != "..") {
                                        if ((strstr($fileName, ".jpg") != false || strstr($fileName, ".JPG") != false ||
                                                strstr($fileName, ".jpeg") || strstr($fileName, ".JPEG") ||
                                                strstr($fileName, ".png") || strstr($fileName, ".PNG")
                                                )) {
                                            $photos .= "<a target='_blank' href='" . $this->Core->GetPath(str_replace("../Web", "", $messagePath . "/" . $fileName)) . "'><img src='" . $this->Core->GetPath(str_replace("../Web", "", $messagePath . "/" . $fileName)) . "' /></a>";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }


            return $photos . "</div>" . $documents . "</ul></div>";
        } else {
            return "Il n'y a pas de document";
        }
    }
}

?>
var DetailCommandForm = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
DetailCommandForm.Init = function(){
    Event.AddById("btnUpdateCommand", "click", DetailCommandForm.UpdateCommand);
};

/***
* Obtient l'id de l'itin�raire courant 
*/
DetailCommandForm.GetId = function(){
    return Form.GetId("DetailCommandFormForm");
};

/*
* Sauvegare l'itineraire
*/
DetailCommandForm.UpdateCommand = function(e){
   
    if(Form.IsValid("DetailCommandFormForm"))
    {

    var data = "Class=Shop&Methode=UpdateCommand&App=Shop";
        data +=  Form.Serialize("DetailCommandFormForm");

        Request.Post("Ajax.php", data).then(data => {
            Dialog.Close();                
        });
    }
};


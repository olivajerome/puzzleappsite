
<section class="container">

    <h1>{{GetCode(Shop.Product)}} :
        {{Product->Name->Value}}
    </h1>

    <div class='row'>
        <div class='col-md-6'>
            {{Apps\Market\Decorator\ProductDecorator->GetProductImages()}}


            {{socialPlugin}}
        </div>

        <div class='col-md-6'>
        <h1>{{Product->Price->Value}}&euro;</h1>
        <h2>{{GetCode(Shop.Description)}}</h2>   
        <p>{{Product->Description->Value}}</p>
        <h2>{{GetCode(Shop.Caracteristiques)}}</h2>
            {{foreach Caracteristiques}}
            <div class='col-md-4'>
                <label>{{element->CaracteristiqueName->Value}}</label>
                <p>{{element->CaracteristiqueItemName->Value}}</p>
            </div>
            {{/foreach Caracteristiques}}
       
            {{GetWidget(Shop,AddToCard,{{Product->IdEntite}})}}
          
            
        </div>
 </div>
</section>

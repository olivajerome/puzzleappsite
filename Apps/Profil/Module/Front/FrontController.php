<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Profil\Module\Front;

use Core\Controller\Controller;
use Core\View\ElementView;
use Core\View\View;
use Core\Entity\User\User;

/**
 * Description of FrontBlock
 *
 * @author jerome
 */
class FrontController extends Controller {

    /**
     * Constructeur
     */
    function __construct($core = "") {
        $this->Core = $core;
    }


    /**
     * Détail d'un profil
     * @param type $userId
     */
    function User($userId, $myProfil = false) {
        
        $user = new User($this->Core);
        $user->GetById($userId);

        if ($user->Email->Value != "") {

            if ($user->IsPublic->Value == 1) {
                $view = new View(__DIR__ . "/View/detailUser.tpl", $this->Core);
                $view->AddElement(new ElementView("User", $user));
            } else {
                $view = new View(__DIR__ . "/View/notPublicUser.tpl", $this->Core);
            }
        } else {
            $view = new View(__DIR__ . "/View/unknowUser.tpl", $this->Core);
        }

        return $view->Render();
    }
    
}
<?php

namespace Apps\Annonce\Widget\AdminDashBoard;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;
use Core\Entity\Entity\Argument;
use Apps\Annonce\Entity\AnnonceAnnonce;

class AdminDashBoard {
    
    public function __construct($core) {
        $this->Core = $core;
    }
    
    /***
     * Render the widget
     */
    public function Render(){
        $view = new View(__DIR__ . "/View/adminDashboard.tpl", $this->Core);
       
        $annonces = new AnnonceAnnonce($this->Core);
        $view->AddElement(new ElementView("Annonces", $annonces->Find( " Id > 0 Order by Id desc limit 0,5 " )));
        
        return $view->Render();
    }
}

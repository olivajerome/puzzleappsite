{{form->Open()}}
{{form->Error()}}
{{form->Success()}}

{{form->Render(Id)}}


    {{if modeAdmin == true}}
    <div>
        <label>{{GetCode(Profil.Group)}}</label> 
        {{form->Render(GroupeId)}}
    </div>    

    {{/if modeAdmin == true}}
    
    <div>
        <label>{{GetCode(Profil.Name)}}</label> 
        {{form->Render(Name)}}
    </div>

    <div>
        <label>{{GetCode(Profil.FirstName)}}</label> 
        {{form->Render(FirstName)}}
    </div>
    
    <div>
        <label>{{GetCode(Profil.BirthDate)}}</label> 
        {{form->Render(BirthDate)}}
    </div>
    
    <div>
        <label>{{GetCode(Profil.Image)}}</label> 
        {{form->Render(Upload)}}
    </div>
    
    <div>
        <label>{{GetCode(Profil.IsPublic)}}</label> 
        {{form->Render(IsPublic)}}
    </div>
    
    <div>
        image
       {{form->RenderImage()}}
    </div>

     <div>
        <label>{{GetCode(Profil.Description)}}</label> 
        {{form->Render(Description)}}
    </div>
    
    <div class='center marginTop' >   
        {{form->Render(btnSaveProfil)}}
    </div>  

{{form->Close()}}
{{form->Open()}}
{{form->Error()}}
{{form->Success()}}

{{form->Render(Id)}}
    <div>
        <label>{{GetCode(Ressource.Name)}}</label> 
        {{form->Render(Name)}}
    </div>

    <div>
        <label>{{GetCode(Ressource.ShortDescription)}}</label> 
        {{form->Render(Description)}}
    </div>

    <div>
        <label>{{GetCode(Ressource.Category)}}</label> 
        {{form->Render(Category)}}
    </div>

    <div>
        <label>{{GetCode(Ressource.LongDescription)}}</label> 
        {{form->Render(LongDescription)}}
    </div>
    
    <div>
        <label>{{GetCode(Ressource.Upload)}}</label> 
        {{form->Render(Upload)}}
    </div>

     <div>
        <label>{{GetCode(Ressource.Images)}}</label> 
        {{form->Render(UploadImage)}}
    </div>

    <div>
        <label>{{GetCode(Ressource.File)}}</label> 
        {{form->RenderImage()}}
    </div>

    <div class='center marginTop' >   
        {{form->Render(btnSaveRessource)}}
    </div>  

{{form->Close()}}
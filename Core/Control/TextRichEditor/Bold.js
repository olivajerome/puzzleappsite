class Bold extends BaseTool {
    getIcone() {
        return "B";
    }

    getName() {
        return Language.GetCode("Base.TextRichEditorBold");
    }

    execute(e) {
        e.preventDefault();
        this.applyStyle("B");
        super.execute();
    }
}


<?php 

namespace Apps\WorkFlow\Decorator;

use Core\View\ElementView;
use Core\View\View;

use Apps\WorkFlow\Entity\WorkFlowAction;

class WorkFlowDecorator{

    public function __construct($core){
        $this->Core = $core;
    }

    /***
     * Render category and is lesson
     */
    public function RenderActions($workflow){

        $view = new View(__DIR__."/View/actions.tpl", $this->Core);

		$action = new WorkFlowAction($this->Core);
		$actions = $action->Find("WorkFlowId=" . $workflow->IdEntite);

        $view->AddElement(new ElementView("Actions", $actions));
        return $view->Render();
    }
}
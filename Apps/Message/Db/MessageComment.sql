CREATE TABLE IF NOT EXISTS `MessageComment` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`UserId` INT  NULL ,
`Message` TEXT  NULL ,
`DateCreated` DATE  NULL ,
`MessageId` INT  NULL ,
PRIMARY KEY (`Id`),
CONSTRAINT `MessageMessage_MessageComment` FOREIGN KEY (`MessageId`) REFERENCES `MessageMessage`(`Id`),
CONSTRAINT `ee_user_MessageComment` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Core\Control\NoteBox;

use Core\Control\IControl;
use Core\Control\Control;

class NoteBox extends Control implements IControl
{
	//Constructeur
	function __construct($name, $note = 0)
	{
            //Version
            $this->Version ="2.0.0.0";

            $this->Id=$name;
            $this->Name=$name;
            $this->Type="number";
            $this->RegExp="'^[0-9]*$'";
            $this->CssClass="form-control";
            $this->Note = $note;
	}
    
    function Show(){
        
       $html = "<div class='noteBox' id='".$this->Id."'>";
       
       $html .= "<input type='hidden' value='1' />";
       
       for($i=1; $i <= 5; $i++){
           if($this->Note >= $i){
               $active = " active"; 
           } else {
               $active = "";
           }
           
           $html .=  "<i id='" .$i. "' class='fa fa-star $active'>&nbsp</i>";
       }
       
       $html .= "</div>";
        
       
       return $html;
    }
}
?>
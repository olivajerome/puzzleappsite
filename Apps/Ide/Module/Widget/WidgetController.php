<?php

/*
 * PuzzleApp
 * Webemyos
 * J�r�me Oliva
 * GNU Licence
 */

namespace Apps\Ide\Module\Widget;

use Core\Control\Button\Button;
use Core\Control\CheckBox\CheckBox;
use Core\Control\ListEditTemplate\ListEditTemplate;
use Core\Control\TextBox\TextBox;
use Core\Controller\AdministratorController;
use Core\Core\Request;
use Core\Dashboard\DashBoardManager;
use Apps\Ide\Helper\WidgetHelper;

class WidgetController extends AdministratorController {

    /**
     * Add New Widget at the projet 
     * @param type $data
     * @return bool
     */
    function AddWidget($data) {

        //Creation du widget
        if (WidgetHelper::CreateWidget($this->Core, $data["Name"], $data["Projet"])) {
            return true;
        } else {
            return $this->Core->GetCode("Ide.WidgetExist");
        }
    }
}

CREATE TABLE IF NOT EXISTS `ShopShop` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`Name` TEXT  NULL ,
`Code` TEXT  NULL ,
`Description` TEXT  NULL ,
`UserId` int  NULL ,
PRIMARY KEY (`Id`),
CONSTRAINT `ee_user_ShopShop` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `ShopCategory` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`ShopId` int(11) NOT NULL, 
`Name` TEXT  NULL ,
`Code` TEXT  NULL ,
`Description` TEXT  NULL ,
PRIMARY KEY (`Id`),
CONSTRAINT `ShopCategoryShopShop` FOREIGN KEY (`ShopId`) REFERENCES `ShopShop`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `ShopCaracteristique` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`ShopId` int(11) NOT NULL, 
`Name` VARCHAR(200)  NULL ,
`Description` TEXT  NULL ,
PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `ShopCaracteristiqueItem` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`CaracteristiqueId` INT  NULL ,
`Label` VARCHAR(200)  NULL ,
PRIMARY KEY (`Id`),
CONSTRAINT `ShopCaracteristique_ShopCaracteristiqueItem` FOREIGN KEY (`CaracteristiqueId`) REFERENCES `ShopCaracteristique`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `ShopProduct` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`ShopId` int(11) NOT NULL, 
`Name` VARCHAR(200)  NULL ,
`Code` VARCHAR(200)  NULL ,
`Description` TEXT  NULL ,
`Status` INT  NULL ,
`Price` float NULL ,

PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `ShopProductCategory` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`ProductId` INT  NULL ,
`CategoryId` INT  NULL ,
PRIMARY KEY (`Id`),
CONSTRAINT `ShopCategory_ShopProductCategory` FOREIGN KEY (`CategoryId`) REFERENCES `ShopCategory`(`Id`),
CONSTRAINT `ShopProduct_ShopProductCategory` FOREIGN KEY (`ProductId`) REFERENCES `ShopProduct`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `ShopProductCaracteristique` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`ProductId` INT  NULL ,
`CaracteristiqueId` INT  NULL ,
`ItemId` INT  NULL ,
PRIMARY KEY (`Id`),
CONSTRAINT `ShopProduct_ShopProductCaracteristique` FOREIGN KEY (`ProductId`) REFERENCES `ShopProduct`(`Id`),
CONSTRAINT `ShopCaracteristique_ShopProductCaracteristique` FOREIGN KEY (`CaracteristiqueId`) REFERENCES `ShopCaracteristique`(`Id`),
CONSTRAINT `ShopCaracteristiqueItem_ShopProductCaracteristique` FOREIGN KEY (`ItemId`) REFERENCES `ShopCaracteristiqueItem`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `ShopCommand` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`UserId` INT  NOT NULL,
`Name` VARCHAR(200)  NOT NULL,
`CartId` VARCHAR(200)  NOT NULL,
`Status` INT  NOT NULL,
`Total` float  NOT NULL,
`DateCreated` DATE  NOT NULL,
`DateClotured` DATE NULL,

`Adress` VARCHAR(500)  NOT NULL,
`AdressFacturation` VARCHAR(500)  NOT NULL,


PRIMARY KEY (`Id`),
CONSTRAINT `ee_user_Command` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `ShopCommandLine` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`CommandId` INT  NOT NULL,
`EntityName` VARCHAR(200)  NOT NULL,
`EntityId` INT  NOT NULL,
`Quantity` INT  NOT NULL,
`Price` float  NOT NULL,
`Data` text  NULL,

PRIMARY KEY (`Id`),
CONSTRAINT `Command_CommandLine` FOREIGN KEY (`CommandId`) REFERENCES `ShopCommand`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `ShopPlugin` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`PluginId` INT  NULL ,
`Parameters` TEXT  NULL ,
PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
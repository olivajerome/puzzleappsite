var TemplateForm = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
TemplateForm.Init = function(){
    Event.AddById("btnSave", "click", TemplateForm.SaveElement);
};

/***
* Obtient l'id de l'itin�raire courant 
*/
TemplateForm.GetId = function(){
    return Form.GetId("TemplateFormForm");
};

/*
* Sauvegare l'itineraire
*/
TemplateForm.SaveElement = function(e){
   
    if(Form.IsValid("TemplateFormForm"))
    {

    var data = "Class=EeApp&Methode=SaveTemplate&App=EeApp";
        data +=  Form.Serialize("TemplateFormForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            Dialog.Close();

            if(data.statut == "Error"){
                Form.RenderError("TemplateFormForm", data.message);
            } else{

                Form.SetId("TemplateFormForm", data.data.Id);
            }
        });
    }
};


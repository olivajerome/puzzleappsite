{{form->Open()}}
{{form->Error()}}
{{form->Success()}}

{{form->Render(Id)}}
{{form->Render(WorkFlowId)}}

    <div>
        <label>{{GetCode(WorkFlow.Name)}}</label> 
        {{form->Render(Name)}}
    </div>

    <div>
        <label>{{GetCode(WorkFlow.Action)}}</label> 
        {{form->Render(Action)}}
    </div>


    <div class='center marginTop' >   
        {{form->Render(btnSaveAction)}}
    </div>  

{{form->Close()}}
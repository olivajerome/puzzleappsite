<div class='col-md-4'>
    <div class='block widgetDashboard'>
        <i class='fa fa-trash removeWidget' data-app='Cms' title='{{GetCode(EeApp.RemoveWidgetDahboard)}}'></i>
        <i class='fa fa-user title'>&nbsp;{{GetCode(Profil.Profil)}}</i>
            
        <ul>
            {{foreach User}}
                <li>
                    <div class='imgProfil' style='background-image: url({{element->GetImageMini()}}) '>
                    </div>
                     {{element->GetPseudo()}} 
                   <div class='picto'>
                       <i class='fa fa-edit editProfil' id='{{element->IdEntite}}' ></i>
                       <a class='fa fa-link' target='_blank' href='{{GetPath(/Profil/User/{{element->IdEntite}} )}}'>
                    </a>
                   </div> 
                </li>
            {{/foreach User}}
        </ul>


    </div>
</div>

var FormFormForm = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
FormFormForm.Init = function(){

    Event.AddById("editForm","click", FormFormForm.ShowForm);
    Event.AddById("editQuestion","click", FormFormForm.ShowQuestion);
    Event.AddById("btnSaveForm", "click", FormFormForm.SaveElement);
    Event.AddById("btnAddQuestion","click", FormFormForm.AddQuestion);
    
    FormFormForm.AddEventQuestion();
};

/***
* Obtient l'id de l'itin�raire courant 
*/
FormFormForm.GetId = function(){
    return Form.GetId("FormFormFormForm");
};

/**
 * Show Form Rool
 */
FormFormForm.ShowForm = function(){
    Animation.Show("formTool");
    Animation.Hide("questionTool");
};

/***
 * Show Question Tool
 */
FormFormForm.ShowQuestion = function(){
    Animation.Hide("formTool");
    Animation.Show("questionTool");
};

/*
* Sauvegare l'itineraire
*/
FormFormForm.SaveElement = function(e){
   
    if(Form.IsValid("FormFormFormForm"))
    {

    let data = "Class=Forms&Methode=SaveForm&App=Forms";
        data +=  Form.Serialize("FormFormFormForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.statut == "Error"){
                Form.RenderError("FormFormFormForm", data.message);

            } else{

                Form.SetId("FormFormFormForm", data.data.Id);
                Dashboard.StartApp("",'Forms', "");
                Animation.Notify(Language.GetCode("Forms.FormSaved"));
            }
        });
    }
};

/***
 * Add Question to the Current Fom
 * @returns {undefined}
 */
FormFormForm.AddQuestion = function(){
    
    let formId = Form.GetValue("FormFormFormForm", "Id");
    
    if(formId != ""){

    Animation.Ask(Language.GetCode("Forms.NewLibelleQuestion"), (libelle)=>{
        
        let questionType = Dom.GetById("questionType").value;
        
        var data = "Class=Admin&Methode=AddQuestion&App=Forms";
            data += "&FormId=" + formId;
            data += "&Type=" + questionType;
            data += "&Libelle=" + libelle;

        Request.Post("Ajax.php", data).then(data => {
            let questionContainer = Dom.GetById("questionContainer");
            questionContainer.innerHTML += "<div class='formQuestion'>" + data+ "</div>";

            FormFormForm.AddEventQuestion();

        });
    });
    }else{
        Animation.Notify(Language.GetCode("Forms.YouMustSaveFormBefore"));
    }
};

/**
 * Init Event on Question
 */
FormFormForm.AddEventQuestion = function(){
    
  Event.AddByClass("btnAddReponse", "click", FormFormForm.AddReponse);
  Event.AddByClass("btnEditQuestion", "click", FormFormForm.EditQuestion);
  Event.AddByClass("btnDeleteQuestion", "click", FormFormForm.DeleteQuestion);
  Event.AddByClass("btnEditReponse", "click", FormFormForm.EditReponse);
  Event.AddByClass("btnDeleteReponse", "click", FormFormForm.DeleteReponse);
};

/**
 * Add New Question
 * @param {*} e 
 */
FormFormForm.AddReponse = function(e){

    let element= e.srcElement;
        let elementId = element.id;
        let container = element.parentNode;
        
        Animation.Ask(Language.GetCode("Forms.NewLibelleReponse"), (libelle)=>{
                 
       let data = "Class=Admin&Methode=AddReponse&App=Forms";
               data += "&QuestionId=" + elementId;
               data += "&Libelle=" + libelle;
   
           Request.Post("Ajax.php", data).then(data => {
               container.innerHTML += data;
               FormFormForm.AddEventQuestion();
           });
           
           });
  };

  /***
   * Edit a question
   */
  FormFormForm.EditQuestion = function(e) {

    let container = e.srcElement.parentNode.parentNode;
    let questionLabel = container.getElementsByTagName("label")[0];
    let newInput = document.createElement("input");
    newInput.value = questionLabel.innerHTML;
    questionLabel.style.display = "none";
    
    newInput.addEventListener("blur", ()=>{

        let data = "Class=Admin&Methode=UpdateQuestion&App=Forms";
        data += "&QuestionId=" + container.id;
        data += "&Libelle=" + newInput.value;

        Request.Post("Ajax.php", data).then(data => {
            questionLabel.innerHTML = newInput.value;
            questionLabel.style.display = "";
            container.removeChild(newInput);
        });
    });

    container.prepend(newInput);
  };
    
  /***
   * Delete a question
   */
  FormFormForm.DeleteQuestion = function(e){

    let container = e.srcElement.parentNode.parentNode;
    
    Animation.Confirm(Language.GetCode("Forms.ConfirmRemoveQuestion"), ()=>{
     
        let data = "Class=Admin&Methode=DeleteQuestion&App=Forms";
            data += "&QuestionId=" + container.id;
     
        Request.Post("Ajax.php", data).then(data => {
            container.parentNode.removeChild(container);
        }); 
    });
  };

  /***
   * Edit a reponse
   */
  FormFormForm.EditReponse = function(e) {

    let container = e.srcElement.parentNode.parentNode;
    let questionLabel = container.getElementsByTagName("label")[0];
    let newInput = document.createElement("input");
    newInput.value = questionLabel.innerHTML;
    questionLabel.style.display = "none";
    
    newInput.addEventListener("blur", ()=>{

        var data = "Class=Admin&Methode=UpdateReponse&App=Forms";
        data += "&ReponseId=" + container.id;
        data += "&Libelle=" + newInput.value;

     Request.Post("Ajax.php", data).then(data => {
        questionLabel.innerHTML = newInput.value;
        questionLabel.style.display = "";
        container.removeChild(newInput);

    });
    });

    container.prepend(newInput);
  };

  /***
   * Delete a reponse
   */
  FormFormForm.DeleteReponse = function(e){

    let container = e.srcElement.parentNode.parentNode;
    
    Animation.Confirm(Language.GetCode("Forms.ConfirmRemoveReponse"), ()=>{
     
        let data = "Class=Admin&Methode=DeleteReponse&App=Forms";
            data += "&ReponseId=" + container.id;
     
        Request.Post("Ajax.php", data).then(data => {
            container.parentNode.removeChild(container);
        }); 
    });
  };
   
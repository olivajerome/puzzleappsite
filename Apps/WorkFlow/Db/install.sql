CREATE TABLE IF NOT EXISTS `WorkFlowWorkFlow` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`Name` VARCHAR(200)  NOT NULL,
`Description` TEXT  NOT NULL,
`Status` INT  NOT NULL,
PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `WorkFlowTrigger` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`WorkFlowId` INT  NOT NULL,
`Event` TEXT  NOT NULL,
`DateExecution` TEXT NULL,
PRIMARY KEY (`Id`),
CONSTRAINT `WorkFlowWorkFlow_WorkFlowTrigger` FOREIGN KEY (`WorkFlowId`) REFERENCES `WorkFlowWorkFlow`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `WorkFlowAction` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`WorkFlowId` INT  NOT NULL,
`Name` TEXT  NOT NULL,
`Action` TEXT  NOT NULL,
`DateExec` TEXT  NULL,
PRIMARY KEY (`Id`),
CONSTRAINT `WorkFlowWorkFlow_WorkFlowAction` FOREIGN KEY (`WorkFlowId`) REFERENCES `WorkFlowWorkFlow`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
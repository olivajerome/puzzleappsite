<?php 
/*
* PuzzleApp
* Webemyos
* Jérôme Oliva
* GNU Licence
*/

namespace Apps\Market\Entity;
use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class MarketCaracteristique extends Entity  
{
	//Constructeur
	function __construct($core)
	{
		//Version
		$this->Version ="2.0.0.0"; 

		//Nom de la table 
		$this->Core=$core; 
		$this->TableName="MarketCaracteristique"; 
		$this->Alias = "MarketCaracteristique"; 

		$this->MarketId = new Property("MarketId", "MarketId", NUMERICBOX,  false, $this->Alias); 
		$this->Name = new Property("Name", "Name", TEXTBOX,  false, $this->Alias); 
		$this->Description = new Property("Description", "Description", TEXTAREA,  false, $this->Alias); 

		//Creation de l entité 
		$this->Create(); 
	}
}
?>
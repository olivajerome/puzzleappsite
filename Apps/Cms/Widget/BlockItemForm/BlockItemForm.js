var BlockItemForm = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
BlockItemForm.Init = function(){
    Event.AddById("btnSaveBlockItem", "click", BlockItemForm.SaveBlockItem);
};

/***
* Obtient l'id de l'item courant 
*/
BlockItemForm.GetId = function(){
    return Form.GetId("BlockItemForm");
};

/*
* Sauvegare l'itineraire
*/
BlockItemForm.SaveBlockItem = function(e){
   
    if(Form.IsValid("BlockItemForm"))
    {

    let data = "Class=Cms&Methode=SaveBlockItem&App=Cms";
        data +=  Form.Serialize("BlockItemForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.statut == "Error"){
                Form.RenderError("BlockItemForm", data.message);
            } else {

                //Form.SetId("BlockItemForm", data.data.Id);
                Dialog.Close();
                
                Cms.RefreshTool();
            }
        });
    }
};


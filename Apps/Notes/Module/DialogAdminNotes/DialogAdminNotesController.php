<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Notes\Module\DialogAdminNotes;

use Core\Control\Button\Button;
use Core\Controller\AdministratorController;
use Core\View\View;

use Apps\Notes\Widget\NotesNoteForm\NotesNoteForm;
use Apps\Notes\Entity\NotesNote;

/*
 * 
 */
 class DialogAdminNotesController extends AdministratorController
 {
    /**
     * Create
     */
    function Create()
    {
    }

    /**
     * Init
     */
    function Init()
    {
    }

    /**
     * Render
     */
    function Show($all=true)
    {
         return $this->Index();
    }
    
    /*
    * Get the home page
    */
   function Index()
   {
       $view = new View(__DIR__."/View/index.tpl", $this->Core);
       return $view->Render();
   }
   
   /***
    * Add a note
    */
   function ShowAddNote($noteId){
       
       $NotesNoteForm = new NotesNoteForm($this->Core);
       
       if($noteId != ""){
           $note = new NotesNote($this->Core);
           $note->GetById($noteId);
           $NotesNoteForm->Load($note);
       }
       return $NotesNoteForm->Render();
       
       
   }
          
          /*action*/
 }?>
<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Admin\Module\Admin;

use Core\Control\Button\Button;
use Core\Controller\Controller;
use Core\View\View;
use Core\View\ElementView;
use Core\Control\TabStrip\TabStrip;
use Core\Control\EntityGrid\EntityGrid;
use Core\Control\EntityGrid\EntityColumn;
use Core\Control\EntityGrid\EntityIconColumn;
use Core\Control\EntityGrid\EntityFunctionColumn;
use Core\Entity\Entity\Argument;

/*
 * 
 */
 class AdminController extends Controller
 {
    /**
     * Constructeur
     */
    function _construct($core="")
    {
          $this->Core = $core;
    }

    /**
     * Creation
     */
    function Create()
    {
    }

    /**
     * Initialisation
     */
    function Init()
    {
    }

    /**
     * Affichage du module
     */
    function Show($all=true)
    {
         return $this->Index();
    }
    
    /*
    * Get the home page
    */
   function Index()
   {
         
         $shop = $shop[0];
         $view = new View(__DIR__."/View/index.tpl", $this->Core);
        
         $tabAdmin = new TabStrip("tabAdmin", "Admin"); 
         $tabAdmin->AddTab($this->Core->GetCode("Admin.Users"), $this->GetTabUser());
       
         $view->AddElement(new ElementView("tabAdmin", $tabAdmin->Render()));

       return $view->Render();
   }

   /***
    * Categorie de la shop
    */
   function GetTabUser(){

      $gdAdmin = new EntityGrid("gdAdmin", $this->Core);
      $gdAdmin->Entity = "Core\Entity\User\User";
      $gdAdmin->App = "Admin";
      $gdAdmin->Action = "GetTabUser";

      $gdAdmin->AddColumn(new EntityColumn("Name", "Name"));
      $gdAdmin->AddColumn(new EntityColumn("FirstName", "FirstName"));
      
      $gdAdmin->AddColumn(new EntityIconColumn("Action", 
                                                array(array("EditIcone", "Admin.EditUser", "Admin.EditUser")
                                                )    
                        ));

      return $gdAdmin->Render();
   }
  
   /*action*/
 }
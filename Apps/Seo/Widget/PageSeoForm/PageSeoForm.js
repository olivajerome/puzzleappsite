var PageSeoForm = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
PageSeoForm.Init = function(){
    Event.AddById("btnSavePage", "click", PageSeoForm.SavePage);
};

/***
* Obtient l'id de l'itin�raire courant 
*/
PageSeoForm.GetId = function(){
    return Form.GetId("itineraireForm");
};

/*
* Sauvegare l'itineraire
*/
PageSeoForm.SavePage = function(e){
   
    if(Form.IsValid("pageSeoForm"))
    {

    var data = "Class=Seo&Methode=SavePageSeoForm&App=Seo";
        
        data +=  Form.Serialize("pageSeoForm");
      
        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.statut == "Error"){
                Form.RenderError("pageSeoForm", data.message);
            } else{

                Form.SetId("pageSeoForm", data.data.Id);
                Form.SetDataSource("pagesSeo", data.data.pages);
                Seo.InitAdminWidget();
                Dialog.close();
            }
        });
    }
};


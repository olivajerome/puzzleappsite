<?php 
/*
* PuzzleApp
* Webemyos
* Jérôme Oliva
* GNU Licence
*/

namespace Apps\EeApp\Entity;
use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class EeWidgetUser extends Entity  
{
	protected $App;

	//Constructeur
	function __construct($core)
	{
		//Version
		$this->Version ="2.0.0.0"; 

		//Nom de la table 
		$this->Core=$core; 
		$this->TableName="EeWidgetUser"; 
		$this->Alias = "EeWidgetUser"; 

		$this->UserId = new Property("UserId", "UserId", NUMERICBOX,  true, $this->Alias); 
		$this->AppId = new Property("AppId", "AppId", NUMERICBOX,  true, $this->Alias); 
		$this->App = new EntityProperty("Apps\EeApp\Entity\EeAppApp", "AppId"); 
		
		$this->Ordre = new Property("Ordre", "Ordre", NUMERICBOX,  true, $this->Alias); 

		//Creation de l entité 
		$this->Create(); 
	}
}
?>
<h1>{{GetCode(Forum.NoForum)}}</h1>

<div class='container'>
    <div class='row'>
        <div class='col-md-6'>
            <div class='info'><i class='fa fa-info'></i>
               {{GetCode(Forum.NewForumDescription)}}
            </div>
        </div>
        <div class='col-md-6'>
            {{forumForm}}
        </div>
    </div>
</div>
        

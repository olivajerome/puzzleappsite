<?php

namespace Apps\Cms\Widget\BlockContent;

use Core\View\View;
use Core\View\ElementView;
use Apps\Cms\Entity\CmsContainer;
use Apps\Cms\Entity\CmsBlock;

class BlockContent {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "") {
        $this->Core = $core;
    }

    /*     * *
     * Get the block content of a container
     */

    function Render($params) {

        if($this->Core->Db == null){
            return;
        }
        
        $view = "";
        $container = new CmsContainer($this->Core);
        $container = $container->GetByCode($params);

        if ($container != null) {

            $block = new CmsBlock($this->Core);
            $blocks = $block->Find("ParentId=" . $container->IdEntite . " ORDER BY Position");

            foreach ($blocks as $block) {
                
                $view .= "<div class='col-md-" . $block->Size->Value . "  ".$block->CssClass->Value. "   ' style='" . $block->Style->Value . "' >";

                switch ($block->TypeId->Value) {
                    case 1 :
                        $view .= $this->RenderTypeText($block);
                        break;
                    case 2 :
                        $view .= $this->RenderTypeImage($block);
                        break;
                    case 3 :
                        $view .= $this->RenderTypeLink($block);
                        break;
                    case 4 :
                        $view .= $this->RenderTypeMenu($block);
                        break;
                    case 5 :
                        $view .= $this->RenderTypeWidget($block);
                        break;
                }

                $view .= "</div>";
            }
        }
        return $view;
    }

    /*     * *
     * Génération type text
     */

    function RenderTypeText($block) {
        return $this->renderText($block->Content->Value);
    }

    function RenderTypeImage($block) {
        return $block->Content->Value;
    }

    function renderText($text) {
        $text = str_replace(array("[", "]"), array("{{", "}}"), $text);
        return \Core\View\FunctionManager::LoadSpecialFunction($this->Core, $text);
    }

    /**
     * Render the link
     * */
    function RenderTypeLink($block) {

        $items = $block->GetItems();

        if (count($items) > 0) {
            $view .= "<ul>";

            foreach ($items as $item) {
                $view .= "<li><a href='" . $item->Url->Value . "'>" . $this->renderText($item->Name->Value) . "</a></li>";
            }

            $view .= "</ul>";
        }

        return $view;
    }

    /**
     * Render the menu type
     * */
    function RenderTypeMenu($block) {

        $items = $block->GetItems();

        if (count($items) > 0) {
            $view = "<nav id='nav-menu-container'>";
            $view .= "<ul class='nav-menu'>";

            foreach ($items as $item) {
                $view .= "<li><a href='" . $this->Core->GetPath("/".$item->Url->Value) . "'>" . \Core\View\FunctionManager::LoadSpecialFunction($this->Core, $item->Name->Value) . "</a></li>";
            }

            $view .= "</ul>";
            $view .= "</nav>";
        }

        return $view;
    }

    /***
     * Render The Widget of the App
     */
    function RenderTypeWidget($block){
        $widget  = explode("-", $block->Widget->Value);
        $app = $widget[0];
        $name = $widget[1];

        $path = "\Apps\\" . $app . "\\" . $app;
        $app = new $path($core);
        return $app->GetWidget($name, "");
    }
}

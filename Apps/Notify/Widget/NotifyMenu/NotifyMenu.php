<?php

namespace Apps\Notify\Widget\NotifyMenu;

use Core\View\View;
use Core\View\ElementView;

use Apps\Notify\Helper\NotifyHelper;

class NotifyMenu {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "") {
        $this->Core = $core;
        $this->Data = $data;

        $this->Init($data);
    }

    /**
     * Initialisation 
     */
    function Init($data) {
    }

    /*     * *
     * Genere les formulaire
     */

    function Render() {
        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);
        $view->AddElement(new ElementView("connected", $this->Core->isConnected()));
        
        $notifys = NotifyHelper::GetByUser($this->Core, $this->Core->UserId, "", true);

        $view->AddElement(new ElementView("numberNotify", count($notifys)));
        
        return $view->Render();

    }
}

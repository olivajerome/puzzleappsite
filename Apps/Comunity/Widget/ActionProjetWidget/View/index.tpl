<div class='col-md-5' style='display: flex;'>
    <button id='btnFollow' {{state}}  class='btn btn-blue' style='margin-right: 7px;'>{{btnFollowText}}</button>
    <button id='btnParticipate'  {{stateParticipate}} class='btn btn-blue'>{{btnParticipateText}}</button>
</div>

<div class='col-md-3' >
	<div style="float: right; padding-right: 10px;">
		<i id='actionHome' class='fa fa-home' title='accueil'>&nbsp;</i>
		<i id='actionDocument' class='fa fa-book' title='Documents'>&nbsp;</i>
		<i id='actionDiscuss' class='fa fa-comment' title='Echanger'>&nbsp;</i>
		<i id='actionEchange'  class='fa fa-share' title='Envoyer à un membre'>&nbsp;</i>
		<span data-type='project' id='moreActionProjet' class='fa'>...</span>
	</div>
</div>

<script>
    ActionProjetWidget.Init();
</script>
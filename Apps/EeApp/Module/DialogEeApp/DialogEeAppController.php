<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\EeApp\Module\DialogEeApp;

use Core\Control\Button\Button;
use Core\Controller\Controller;
use Core\View\View;
use Core\View\ElementView;

use Apps\EeApp\Entity\EeAppApp;
use Apps\EeApp\Entity\EeAppPlugin;
use Apps\EeApp\Widget\PluginForm\PluginForm;
use Apps\EeApp\Widget\PluginAppForm\PluginAppForm;
use Apps\EeApp\Widget\TemplateForm\TemplateForm;

/*
 * 
 */
 class DialogEeAppController extends Controller
 {
    /**
     * Constructeur
     */
    function _construct($core="")
    {
          $this->Core = $core;
    }

    /**
     * Creation
     */
    function Create()
    {
    }

    /**
     * Initialisation
     */
    function Init()
    {
    }

    /**
     * Affichage du module
     */
    function Show($all=true)
    {
         return $this->Index();
    }
    
    /*
    * Get the home page
    */
   function Index()
   {
       $view = new View(__DIR__."/View/index.tpl", $this->Core);
       return $view->Render();
   }

   /***
    * Mise a jour des version du coeur et des application
    */
   function ShowUpdateVersion(){
        $view = new View(__DIR__."/View/version.tpl", $this->Core);

        $view->AddElement(new ElementView("frameworkVersion", $this->Core->GetVersion()));

        $apps = new EeAppApp($this->Core);
        $view->AddElement(new ElementView("Apps", $apps->GetAll()));
 

        return $view->Render();
   }

   /**
    * Add plugin
    */
   function ShowAddPlugin(){
     $PluginForm = new PluginForm($this->Core, $params);
     return $PluginForm->Render($params);
   }
   
   /**
    * Add Plugin To a app
    */
   function AddPluginApp($params){
     $PluginAppForm = new PluginAppForm($this->Core, $params);
     return $PluginAppForm->Render($params);
   }
    
   /***
    * Edit a plugin
    */
   function EditPlugin($params){

      $plugin = new EeAppPlugin($this->Core);
      $plugin->GetById($params);
      
      $pluginName = $plugin->Code->Value;
            
      $pluginPath = "\\Plugins\\" .$pluginName . "\\".$pluginName;
      $plugin = new $pluginPath($core); 
  
      return $plugin->RenderConfigForm();
   }

   /**
    * Add plugin
    */
   function ShowAddTemplate(){
     $PluginForm = new TemplateForm($this->Core, $params);
     return $PluginForm->Render($params);
   }
   
   
          /*action*/
 }?>
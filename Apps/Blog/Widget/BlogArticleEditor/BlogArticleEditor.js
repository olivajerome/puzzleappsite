var BlogArticleEditor = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
BlogArticleEditor.Init = function(callBack){

    Event.AddById("btnUpdateContentArticle", "click", BlogArticleEditor.UpdateContent);
    
    BlogArticleEditor.txtEditor = new TextRichEditor( Dom.GetById("Content"),
            {tools: ["ImageTool", "SourceCode", "ShowBlock"],
                events: [{"tool": "ImageTool", "events": [{"type": "mousedown", "handler": BlogArticleEditor.OpenImageLibray}]}],
            }
    );
};

/***
 * Open the libray image
 * @param {type} editor
 * @param {type} node
 * @returns {undefined}
 */
BlogArticleEditor.OpenImageLibray = function(editor, node){
       
        if (node.tagName != "DIV") {
            node = node.parentElement
        }

        editor.setSelectedNode(node);
        Dashboard.currentEditor = editor;

        let blockId = editor.sourceControl.parentNode.id;

        Dialog.open('', {"title": Dashboard.GetCode("Cms.Addmage"),
            "app": "Cms",
            "class": "DialogCms",
            "method": "AddImage",
            "type": "left",
            "params": ""
        });
   } ;

/***
* Obtient l'id de l'itin�raire courant 
*/
BlogArticleEditor.GetId = function(){
    return Form.GetId("categoryForm");
};

/*
* Sauvegare l'itineraire
*/
BlogArticleEditor.UpdateContent = function(e){
   
    if(Form.IsValid("articleForm"))
    {

    var data = "Class=Blog&Methode=UpdateContent&App=Blog";
        data +=  "&Id= "  + Form.GetValue("articleForm", "Id");
        data += "&Content=" + BlogArticleEditor.txtEditor.getJSONContent(); 
        
        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);
         
            if(data.statut == "Error"){
                Form.RenderError("articleForm", data.message);
            } else {

                Animation.Notify(Language.GetCode("Blog.ContentUpdated"));

                //Dialog.Close();
                
                if(BlogArticleEditor.callBack){
                    BlogArticleEditor.callBack(data);
                }
            }
        });
    }
};


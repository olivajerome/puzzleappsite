var DialogRoadMapController = function(){};

DialogRoadMapController.ShowAddRoadMap = function(){
    RoadMapForm.Init();
};

DialogRoadMapController.ShowAddColumn = function(){
    ColumnForm.Init();
};

DialogRoadMapController.ShowAddTicket = function(){
    TicketForm.Init();
};
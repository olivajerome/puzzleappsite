var ImportForm = function(){};

/***
 * Init the form
 * @returns {undefined}
 */
ImportForm.Init = function(){
    Event.AddById("btnImport", "click", ImportForm.Import);
};

/*
* Import
*/
ImportForm.Import = function(e){
   
    if(Form.IsValid("ImportFormForm"))
    {

    var data = "Class=Cms&Methode=ImportPages&App=Cms";
        data +=  Form.Serialize("ImportFormForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.statut == "Error"){
                Form.RenderError("ImportFormForm", data.message);
            } else{
                 Dialog.Close();  
                  Dashboard.StartApp('', 'Cms', '');
            }
        });
    }
};


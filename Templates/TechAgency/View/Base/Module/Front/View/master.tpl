<!DOCTYPE html>
<html lang="fr">
  <head>
    <title>{{Title}}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="{{Description}}">
    <meta name="author" content="Webemyos.com">
    
    <link href="{{GetPath(/asset/boostrap/css/bootstrap.min.css)}}" rel="stylesheet">
    <link href="{{GetPath(/asset/fontawersome/css/all.css)}}" rel="stylesheet">
  
    <link href="{{GetPath(/style.php?t=TechAgency)}}" rel="stylesheet">
     
    <script src='{{GetPath(/script.php)}}' ></script>
    <script src='{{GetPath(/script.php?apps=all)}}' ></script>
 
  </head>

  <body>
       <header id="header" class="header header-hide scroll-header">
        <div class='row'>
          {{GetWidget(Cms,BlockContent,Header)}}
        </div>
      </header>
        
        <div class="content mainContent">
          <div id="dvCente">
            <div  class="App row-fluid">
              <div id='appCenter'>
                 {{content}}
              </div>
            </div>
          </div>
        </div>

<!-- Footer
    ================================================== -->
    <footer class='footer'>
      <div class="container">
        <div class='row'>
          {{GetWidget(Cms,BlockContent,Footer)}}
        </div>
      </div>
    </footer>
  </body> 
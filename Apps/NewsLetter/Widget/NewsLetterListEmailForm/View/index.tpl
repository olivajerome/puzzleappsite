{{form->Open()}}
{{form->Error()}}
{{form->Success()}}

{{form->Render(Id)}}


    <div>
        <label>{{GetCode(NewsLetter.Libelle)}}</label> 
        {{form->Render(Libelle)}}
    </div>

   
    
    <div>
        <label>{{GetCode(NewsLetter.Description)}}</label> 
        {{form->Render(Description)}}
    </div>
    
    <div>
        <label>{{GetCode(NewsLetter.Emails)}}</label> 
        
        <div class='row'>
            <div class='col-md-11'>
                <input placeholder ='{{GetCode(NewsLetter.EnterEmail)}}' type='email' class='form-control ' id='emailToAdd' />
            </div>
            <div class='col-md-1' id='btnAddEmail' tile='{{GetCode(NewsLetter.AddThisEmail)}}'>
                <i class='fa fa-plus'></i>
            </div>   
             <ul id='listEmail' style='height:50vh; overflow:auto;width:95%'>
        
             </ul>    
                
        </div>
        
        {{form->Render(Emails)}}
    </div>
    
    <div class='center marginTop' >   
        {{form->Render(btnSaveListEmail)}}
    </div>  

{{form->Close()}}
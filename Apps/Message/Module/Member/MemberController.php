<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Message\Module\Member;

use Core\Control\Button\Button;
use Core\Controller\Controller;
use Core\View\View;
use Core\View\ElementView;
use Core\Control\AutoCompleteBox\AutoCompleteBox;
use Core\Control\Upload\Upload;
use Core\Entity\User\User;
use Core\Entity\Entity\Argument;
use Apps\Message\Helper\MessageHelper;
use Apps\Message\Entity\MessageConversation;
use Apps\Message\Entity\MessageMessage;

/*
 * 
 */

class MemberController extends Controller {

    /**
     * Constructeur
     */
    function _construct($core = "") {
        $this->Core = $core;
    }

    /**
     * Creation
     */
    function Create() {
        
    }

    /**
     * Initialisation
     */
    function Init() {
        
    }

    /**
     * Affichage du module
     */
    function Show($all = true) {
        return $this->Index();
    }

    /**
     * Obtient les discsssion de l'utilisateur
     */
    function RefreshMessage() {
        $view = new View(__DIR__ . "/View/refreshMessage.tpl", $this->Core);
        $view->AddElement(new ElementView("Message", MessageHelper::GetConversationActive($this->Core)));
        return $view->Render();
    }

    /*
     * Get the home page
     */

    function Index() {
        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);

        $view->AddElement(new ElementView("userPseudo", $this->Core->User->GetPseudo()));

        //Récuperation des Dicussion courante de l'utilisateur
        $view->AddElement(new ElementView("Conversations", MessageHelper::GetConversationActive($this->Core)));

        $tbContact = new AutoCompleteBox("tbContact", $this->Core);
        $tbContact->PlaceHolder = $this->Core->GetCode("Message.SearchUser");
        $tbContact->Entity = "Apps/Message/Message";
        $tbContact->Methode = "SearchUser";
        $tbContact->Parameter = "AddAction=MessageAction.SelectUser()";
        $view->AddElement($tbContact);

        //Upload Image
        $uploadMessage = new Upload("Message", "uploadMessage", "MessageAction.SendAttachment()", "uploadMessage");
        $uploadMessage->RenderType = "Icone";
        $uploadMessage->Accept = "";
        $view->AddElement(new ElementView("uploadMessage", $uploadMessage->Show()));

        return $view->Render();
    }

    /**
     * Début d'un conversation
     */
    function CreateMessage($users) {
        $view = new View(__DIR__ . "/View/createMessage.tpl", $this->Core);
        $view->AddElement(new ElementView("userId", $users));

        $user = new User($this->Core);
        $user->AddArgument(new Argument("Core\Entity\User\User", "Id", IN, $users));
        $users = $user->GetByArg();
        $userName = "";

        foreach ($users as $user) {
            if ($userName != "") {
                $userName .= ",";
            }
            $userName .= $user->GetPseudo();
        }

        $view->AddElement(new ElementView("userName", $userName));

        return $view->Render();
    }

    /**
     * Démarre une discussion et retourne le message
     */
    function StartMessage($users, $message, $objet, $discussId, $images = "", $type = 1, $reseauCode = "", $documents = "", $typeMessage = "", $typeReponse = "", $projetId = "") {
        //Save message type image
        if ($images != "") {
            $images = explode(";", $images);
            $messageImage = "";

            $messageId = MessageHelper::AddMessage($this->Core, $discussId, $message);

            //On déplace les images dans le bon répertoire
            foreach ($images as $image) {
                $path = explode("/", $image);

                if (!file_exists("Data/Apps/Message/" . $discussId)) {
                    mkdir("Data/Apps/Message/" . $discussId);
                    mkdir("Data/Apps/Message/" . $discussId . "/Message");
                }

                if (!file_exists("Data/Apps/Message/" . $discussId . "/Message/" . $messageId)) {
                    mkdir("Data/Apps/Message/" . $discussId . "/Message/" . $messageId);
                }

                $fileName = $path[count($path) - 1];

                rename("Data/Tmp/" . $fileName, "Data/Apps/Message/" . $discussId . "/Message/" . $messageId . "/" . $fileName);

                $messageImage .= "<img src='" . $this->Core->GetPath("/Data/Apps/Message/" . $discussId . "/Message/" . $messageId . "/" . $fileName) . "' >";
            }

            // C'est un fichier Type Image
            $message = $messageImage;

            MessageHelper::UpdateMessage($this->Core, $messageId, $message);
        } else if ($documents != "") {
            $documents = explode(";", $documents);
            $messageImage = "";

            $messageId = MessageHelper::AddMessage($this->Core, $discussId, $message);

            //On déplace les images dans le bon répertoire
            foreach ($documents as $document) {
                $path = explode("/", $document);

                if (!file_exists("Data/Apps/Message/" . $discussId)) {
                    mkdir("Data/Apps/Message/" . $discussId);
                }

                if (!file_exists("Data/Apps/Message/" . $discussId . "/Message")) {
                    mkdir("Data/Apps/Message/" . $discussId . "/Message");
                }

                if (!file_exists("Data/Apps/Message/" . $discussId . "/Message/" . $messageId)) {
                    mkdir("Data/Apps/Message/" . $discussId . "/Message/" . $messageId);
                }

                $fileName = $path[count($path) - 1];

                rename("Data/Tmp/" . $fileName, "Data/Apps/Message/" . $discussId . "/Message/" . $messageId . "/" . $fileName);

                $messageDocument .= "<a  target='_blank' href='" . $this->Core->GetPath("/Data/Apps/Message/" . $discussId . "/Message/" . $messageId . "/" . $fileName) . "' >" . $fileName . "</a>";
            }

            // C'est un fichier Type Document
            $message = $messageDocument;

            MessageHelper::UpdateMessage($this->Core, $messageId, $message);
        } else if ($discussId == "") {
            $discussId = MessageHelper::CreateConversation($this->Core, $users, $message, $objet, 1, "", $projetId);
            $messageId = $this->Core->Db->GetInsertedId();
        } else {
            $message = $message;

            $messageId = MessageHelper::AddMessage($this->Core, $discussId, $message);
            MessageHelper::UpdateLastMessage($this->Core, $discussId, $message);
        }

        return json_encode(array("discuId" => $discussId, "message" => $this->GetMessageUser($this->Core->User, $message, $messageId, $typeReponse != "reponseTchat"), "messageOther" => $this->GetMessageOther($this->Core->User, $message, $messageId, $typeReponse != "reponseTchat")));
    }

    /**
     * génere et retourne le message
     * @param type $userId
     * @param type $message
     * @return string
     */
    function GetMessage($user, $message, $messageId = "", $nbComment = 0) {

        if ($user->IdEntite == $this->Core->User->IdEntite) {
            $class = 'userConnect';
        } else {
            $class = "otherUser";
        }

        $html = "<div class='message'>";
        $html .= "<div title='" . $user->GetPseudo() . "' class='avatarmini-img avatarmini'> <img src='" . $user->GetImageMini() . "' alt='images'></div>";
        $html .= "<div class='messageUser'> ";
        $html .= "<div class='moreAction' data-type='message' id='" . $messageId . "' style='float:right'>";
        $html .= "...";
        $html .= "</div>";

        $html .= "<p class='membresTitre2  text-left user'>" . $user->GetPseudo() . "
					<br>
					<span class='membreville '>" . $user->City->Value . "</span>
				</p> ";

        $html .= "<p>" . $message . "</p>";
        $html .= "<div class='buttons' id='" . $messageId . "' >";
        $html .= "<i class='fa fa-heart likeMessage'  title='Aimer la conversation'>&nbsp;J'aime</i>";
        $html .= "<i class='fa fa-comment commentMessage' title='Commenter' >&nbsp;Commenter</i>";
        $html .= "                         <span class='commentaires' ><b id='nbCommentaire'>" . $nbComment . "</b> commentaires</span>";
        $html .= "                   </div>";
        $html .= "                   <div class='lstComment'></div>";
        $html .= "                   ";
        $html .= "                </div>";
        $html .= "            </div>";

        return $html;
    }

    /*     * *
     * Retourne le message de l'utilisateur
     */

    function GetMessageUser($user, $message, $messageId, $showAll = true) {
        $html .= "<div class='userMessage row'>";
        if ($showAll) {
            $html .= "<div class='col-md-9 message' ><span class='bulleMessage'>" . $message . "</span></div>";
        } else {
            $html .= "<div class='col-md-10 message'><span class='bulleMessage'>" . $message . "</span></div>";
        }
        $html .= "<div class='col-md-2'><div title='" . $user->GetPseudo() . "' >";

        $html .= "<div  class='imgProfil' style='background-image: url(" . $user->GetImageMini() . ")' ></div>";
        $html .= "</div></div>";

        if ($showAll) {

            $html .= "<div class='col-md-1 moreAction' data-type='message' id='" . $messageId . "'>";
            $html .= "<i class='fa fa-trash removeMessage' title='" . $this->Core->GetCode("Message.DeleteMessage") . "'></i>";
            $html .= "</div>";
        }

        $html .= "</div>";

        return $html;
    }

    /**
     * Retourne un message formaté des autre utilisateur
     */
    function GetMessageOther($user, $message, $messageId = "", $showAll = true) {
        $html .= "<div class='otherUserMessage row'>";
        $html .= "<div class='col-md-3'><div title='" . $user->GetPseudo() . "'>";
        $html .= "<div  class='imgProfil' style='background-image: url(" . $user->GetImageMini() . ")' ></div></div></div>";

        if ($showAll) {
            $html .= "<div class='col-md-8'><div class='message'><span class='bulleMessageOther'>" . $message . "</span></div></div>";
        } else {
            $html .= "<div class='col-md-9'><div class='message'><span class='bulleMessageOther'>" . $message . "<span class='bulleMessage'></div></div>";
        }
        $html .= "</div>";

        return $html;
    }

    /**
     * Charge les messages de la conversation
     * @param type $discussId
     */
    function LoadMessage($discussId, $showAll = true) {
        $discuss = new MessageConversation($this->Core);
        $discuss->GetById($discussId);

        if ($showAll) {
            $html = "<input type='hidden' id='hdMessageId' value='" . $discussId . "' />";
        } else {
            $html = "<input type='hidden' id='hdTchatId' value='" . $discussId . "' />";
        }

        if ($showAll) {
            $html .= "<div class='titleDiscssus'>" . $discuss->GetTitle() . "<span class='moreActionMessage'>...</span> </div>";
        }

        $html .= "<div style='margin-top:80px'>";

        $messages = MessageHelper::GetMessages($this->Core, $discussId);

        //On estime que l'utilisateur à lu la conversation
        MessageHelper::SetUserRead($this->Core, $discussId);

        foreach ($messages as $message) {

            $user = $message->User->Value;

            if ($message->User->Value->IdEntite == $this->Core->User->IdEntite) {
                $html .= $this->GetMessageUser($user, $message->Message->Value, $message->IdEntite, $showAll);
            } else {
                $html .= $this->GetMessageOther($user, $message->Message->Value, $message->IdEntite, $showAll);
            }
        }

        $html .= "</div>";
        return $html;
    }

    /* action */
}

?>
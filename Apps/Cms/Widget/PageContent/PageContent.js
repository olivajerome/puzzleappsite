var PageContent = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
PageContent.Init = function(){
    Event.AddById("btnSave", "click", PageContent.SaveElement);
};

/***
* Obtient l'id de l'itin�raire courant 
*/
PageContent.GetId = function(){
    return Form.GetId("PageContentForm");
};

/*
* Sauvegare l'itineraire
*/
PageContent.SaveElement = function(e){
   
    if(Form.IsValid("PageContentForm"))
    {

    var data = "Class=PageContent&Methode=SaveElement&App=PageContent";
        data +=  Form.Serialize("PageContentForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.statut == "Error"){
                Form.RenderError("PageContentForm", data.message);
            } else{

                Form.SetId("PageContentForm", data.data.Id);
            }
        });
    }
};


<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Tips\Entity;

use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class TipsTips extends Entity {

    //Constructeur
    function __construct($core) {
        //Version
        $this->Version = "2.0.0.0";

        //Nom de la table 
        $this->Core = $core;
        $this->TableName = "TipsTips";
        $this->Alias = "TipsTips";

        $this->Name = new Property("Name", "Name", TEXTBOX, true, $this->Alias);
        $this->Code = new Property("Code", "Code", TEXTBOX, true, $this->Alias);
        $this->Description = new Property("Description", "Description", TEXTAREA, true, $this->Alias);
        $this->DateCreated = new Property("DateCreated", "DateCreated", DATEBOX, true, $this->Alias);
        $this->App = new Property("App", "App", TEXTBOX, false, $this->Alias);
        $this->Widget = new Property("Widget", "Widget", TEXTBOX, false, $this->Alias);
        $this->Status = new Property("Status", "Status", NUMERICBOX, false, $this->Alias);
       
        //Creation de l entité 
        $this->Create();
    }

}

?>
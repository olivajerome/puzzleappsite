<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Notify\Module\Member;

use Apps\Notify\Helper\NotifyHelper;
use Core\Controller\Controller;
use Core\View\View;

class MemberController extends Controller {

    /**
     * Constructeur
     */
    function __construct($core = "") {
        $this->Core = $core;
    }

    /**
     * Creation
     */
    function Create() {
        
    }

    /**
     * Initialisation
     */
    function Init() {
        
    }

    /**
     * Render the index page
     */
    function Index($all = true) {
        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);

        $notifys = NotifyHelper::GetByUser($this->Core, $this->Core->User->IdEntite, 10, false);

        //Recuperation 
        $view->AddElement($notifys);

        return $view->Render();
    }
}

?>
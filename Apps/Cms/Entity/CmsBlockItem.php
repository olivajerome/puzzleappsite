<?php 
/*
* PuzzleApp
* Webemyos
* Jérôme Oliva
* GNU Licence
*/

namespace Apps\Cms\Entity;
use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class CmsBlockItem extends Entity  
{
	//Constructeur
	function __construct($core)
	{
		//Version
		$this->Version ="2.0.0.0"; 

		//Nom de la table 
		$this->Core=$core; 
		$this->TableName="CmsBlockItem"; 
		$this->Alias = "CmsBlockItem"; 

		$this->BlockId = new Property("BlockId", "BlockId", NUMERICBOX,  true, $this->Alias); 
		$this->Name = new Property("Name", "Name", TEXTAREA,  true, $this->Alias); 
		$this->Url = new Property("Url", "Url", TEXTAREA,  true, $this->Alias); 

		//Creation de l entité 
		$this->Create(); 
	}
}
?>
<div class='col-md-4'>
    <div class='block widgetDashboard'>
        <i class='fa fa-trash removeWidget' data-app='Stat' title='{{GetCode(EeApp.RemoveWidgetDahboard)}}'></i>
        <i class='fa fa-list title'>&nbsp;{{GetCode(Stat.Stat)}}</i>
        <ul>
            <li><b>{{nbUser}}</b> {{GetCode(Stat.Users)}}</li>
            <li><b>{{nbProduct}}</b> {{GetCode(Stat.Product)}}</li>
            <li><b>{{nbProposition}}</b> {{GetCode(Stat.Proposition)}}</li>
            <li><b>{{nbCommand}}</b> {{GetCode(Stat.Command)}}</li>
            
            <li><b>{{nbUnvalidProposition}}</b> {{GetCode(Stat.UnvalidPropositionCurrent)}}</li>
            <li><b>{{nbUnvalidPropositionClotured}}</b> {{GetCode(Stat.UnvalidPropositionClotured)}}</li>
            <li><b>{{nbDiscuss}}</b> {{GetCode(Stat.Discussion)}}
            <li><b>{{nbProductFiabilite}}</b> {{GetCode(Stat.ProductFiabilite)}}</li>
            <li><b>{{nbProductAttractivite}}</b> {{GetCode(Stat.ProductAtractivite)}}</li>
            <li><b>{{nbEvaluate}}</b> {{GetCode(Stat.Evaluate)}}</li>
            
        </ul>
        
    </div>
</div>

<section class='container'>
    <div class='row'>
        <div class='col-md-3'>
            <div class='block'>
                <h2>Les groupes</h2>
                    <ul>
                        {{foreach groupes}}
                        <li><a href ='{{GetPath(/Comunity/Group/{{element->IdEntite}})}}'>  

                                {{element->Name->Value}}
                            </a>
                        </li>

                        {{/foreach groupes}}
                    </ul>
            </div>
        </div>
        <div class='col-md-9'>
            <div class='centerBlock'>
                <input type='hidden' id='hdComunityId' value='{{Comunity->IdEntite}}'/> 
                <div id='appRunDiscuss' >

                    <div id='dashboardDiscus' >

                        {{if isConnected == true}}

                        <div class='messageBlock row'>
                            
                            <div class="imgProfil" style="background-image: url({{userImage}}) "></div>
                         
                            <textarea id="tbMessage" type="text" class="form-control" placeholder="Publier quelque chose"></textarea>
                            {{uploadDiscuss}}
                        </div>   

                        {{/if isConnected == true}}

                        <div class='messages' id='messages' >  
                            {{Messages}}
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
    <div class='clear'></div>
</section>                        


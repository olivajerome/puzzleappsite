var ShopForm = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
ShopForm.Init = function(){
    Event.AddById("btnSave", "click", ShopForm.SaveElement);
    Event.AddById("btnAddPlugin", "click", ShopForm.AddPlugin);
    Event.AddByClass("removePlugin", "click" , ShopForm.RemovePlugin);
};

/***
* Obtient l'id de l'itin�raire courant 
*/
ShopForm.GetId = function(){
    return Form.GetId("ShopFormForm");
};

/*
* Sauvegare l'itineraire
*/
ShopForm.SaveElement = function(e){
   
    if(Form.IsValid("ShopFormForm"))
    {

    var data = "Class=Shop&Methode=SaveShop&App=Shop";
        data +=  Form.Serialize("ShopFormForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.statut == "Error"){
                Form.RenderError("ShopFormForm", data.message);
            } else{

                Form.SetId("ShopFormForm", data.data.Id);
            }
        });
    }
};

/***
 * Ajout d'un plugin
 */
ShopForm.AddPlugin = function(e){
  
   e.preventDefault();
   
    Dialog.open('', {"title": Dashboard.GetCode("Shop.AddPlugin"),
    "app": "EeApp",
    "class": "DialogEeApp",
    "method": "AddPluginApp",
    "params": "Shop",
    "type" : "right"
    });
};

/***
 * Remove plugin to the shop 
 * @returns {undefined}
 */
ShopForm.RemovePlugin = function(e){
    
    Animation.Confirm(Language.GetCode("Shop.RemovePlugin"), function(){
        let container = e.srcElement.parentNode;
        
        let  data = "Class=EeApp&Methode=RemovePluginApp&App=EeApp";
             data += "&pluginId="+e.srcElement.id;
             
        Request.Post("Ajax.php", data).then(data => {
            container.parentNode.removeChild(container);
        });
    });
};

CREATE TABLE IF NOT EXISTS `MessageUser` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`ConversationId` INT  NULL ,
`UserId` INT  NULL ,
`Status` INT  NULL ,
`Readed` INT  NULL ,
PRIMARY KEY (`Id`),
CONSTRAINT `ee_user_MessageUser` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
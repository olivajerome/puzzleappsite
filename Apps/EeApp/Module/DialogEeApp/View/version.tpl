<section>
    <h2>{{GetCode(EeApp.Version)}}</h2>
    <div class='row'> 
        <label class='col-md-4'>
            <b>{{GetCode(EeApp.FrameworkVersion)}}</b>
            <i>- {{frameworkVersion}}</i>
        </label>
        <p class='col-md-8'>
        {{GetControl(Button,btnUpdateFramework,{LangValue=EeApp.UpdateFramework,CssClass=btn btn-primary,Id=btnUpdateFramework})}}</p>
    </div>

    {{foreach Apps}}

        <div class='row'> 
            <label class='col-md-4'>
                <b>{{element->Name->Value}}</b><br/>
                <i>- {{element->GetVersion()}}</i>
            </label>
            <p class='col-md-8'>
            <input type='hidden' value='{{element->Name->Value}}'  />
            {{GetControl(Button,BtnUpdateApp,{LangValue=EeApp.UpdateApp,CssClass=btn btn-primary btnUpdateApp,})}}</p>
           </p>
        </div>

    {{/foreach Apps}}


</section>
CREATE TABLE IF NOT EXISTS `WorkFlowAction` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`WorkFlowId` INT  NOT NULL,
`Name` TEXT  NOT NULL,
`Action` TEXT  NOT NULL,
`DateExec` TEXT NULL,
PRIMARY KEY (`Id`),
CONSTRAINT `WorkFlowWorkFlow_WorkFlowAction` FOREIGN KEY (`WorkFlowId`) REFERENCES `WorkFlowWorkFlow`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
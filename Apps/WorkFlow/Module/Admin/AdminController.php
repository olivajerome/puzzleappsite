<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\WorkFlow\Module\Admin;

use Core\Control\Button\Button;
use Core\Controller\AdministratorController;
use Core\View\View;
use Core\View\ElementView;
use Core\Control\TabStrip\TabStrip;
use Core\Control\EntityGrid\EntityGrid;
use Core\Control\EntityGrid\EntityColumn;
use Core\Control\EntityGrid\EntityIconColumn;
use Core\Control\EntityGrid\EntityFunctionColumn;

use Apps\WorkFlow\Widget\WorkFlowWorkFlowForm\WorkFlowWorkFlowForm;
use Apps\WorkFlow\Entity\WorkFlowWorkFlow;


use Apps\WorkFlow\Widget\WorkFlowTriggerActionForm\WorkFlowTriggerActionForm;
use Apps\WorkFlow\Widget\WorkFlowActionForm\WorkFlowActionForm;
use Apps\WorkFlow\Entity\WorkFlowTrigger;
use Apps\WorkFlow\Entity\WorkFlowAction;


/*
 * 
 */
 class AdminController extends AdministratorController
 {
    /**
     * Constructeur
     */
    function _construct($core="")
    {
          $this->Core = $core;
    }

    /**
     * Create
     */
    function Create()
    {
    }

    /**
     * Init
     */
    function Init()
    {
    }

    /**
     * Render
     */
    function Show($all=true)
    {
         return $this->Index();
    }
    
    /*
    * Get the home page
    */
   function Index()
   {
       $view = new View(__DIR__."/View/index.tpl", $this->Core);

       $tabWorkflow = new TabStrip("tabWorkFlow", "WorkFlow");
       $tabWorkflow->AddTab($this->Core->GetCode("Mooc.Workflow"), $this->GetTabWorkflow());
      
       $view->AddElement(new ElementView("tabWorkFlow", $tabWorkflow->Render()));

       return $view->Render();
   }

   /***
    * WorkFlowTab
    */
   function GetTabWorkflow(){

    $gdWorkFlow = new EntityGrid("gdWorkFlow", $this->Core);
    $gdWorkFlow->Entity = "Apps\WorkFlow\Entity\WorkFlowWorkFlow";
    $gdWorkFlow->App = "WorkFlow";
    $gdWorkFlow->Action = "GetTabWorkflow";

    $btnAdd = new Button(BUTTON, "btnAddWorkFlow");
    $btnAdd->Value = $this->Core->GetCode("WorkFlow.AddWorkFlow");

    $gdWorkFlow->AddButton($btnAdd);
    $gdWorkFlow->AddColumn(new EntityColumn("Name", "Name"));
    $gdWorkFlow->AddColumn(new EntityFunctionColumn("Trigger", "GetTrigger"));
    $gdWorkFlow->AddColumn(new EntityFunctionColumn("Action", "GetAction"));
    
    $gdWorkFlow->AddColumn(new EntityIconColumn("Action", 
                                              array(array("EditIcone", "WorkFlow.EditWorkFlow", "WorkFlow.EditWorkFlow"),
                                                    array("ParameterIcone", "WorkFlow.AddTaskWorkFlow", "WorkFlow.AddTaskWorkFlow"),
                                                    array("AddIcone", "WorkFlow.AddActionWorkFlow", "WorkFlow.AddActionWorkFlow"),
                                                    array("DeleteIcone", "WorkFlow.DeleteWorkFlow", "WorkFlow.DeleteWorkFlow"),
                                              )    
                      ));

    return $gdWorkFlow->Render();
   }

   /***
    * Save the Worfklow
    */
   function SaveWorkFlow($data){

      $workflowForm = new WorkFlowWorkFlowForm($this->Core, $data);

      if($workflowForm->Validate($data)){

            $workflow = new WorkFlowWorkFlow($this->Core);

            if($data["Id"] != ""){
                  $workflow->GetById($data["Id"]);   
            } else {
                  $workflow->Status->Value = 1;    
            }

            $workflowForm->Populate($workflow);
            $workflow->Save();

            return true;
      } else {
            return $workflowForm->errors;
      }

   }

   /***
    * Delete the workFlow
    */
   function DeleteWorkFlow($workFlowId){

      $workflow = new WorkFlowWorkFlow($this->Core);  
      $workflow->GetById($data["Id"]);   
      $workflow->Delete();

      return true;
   }
    
   /***
    * Ajout d'un trigger et des actions sur un workflow
    */
   function AddTriggerActions($data){

      $workflowId = $data["WorkFlowId"];

      $this->SaveTrigger($workflowId, $data);

      return true;
   }

   /***
    * Save a trigger for a workflow
    */
   function SaveTrigger($workflowId, $data){

      $trigger = new WorkFlowTrigger($this->Core);
      $triggers = $trigger->Find("WorkFlowId=".$workflowId);

      if(count($triggers) == 0){
            $trigger = new WorkFlowTrigger($this->Core);
            $trigger->WorkFlowId->Value = $workflowId;
      } else {
            $trigger = $triggers[0];
      }

      $trigger->Event->Value = $data["Event"];
      $trigger->Save();

      return true;
      
   }

   /***
    * Save Action for the WorkFlow
    */
   function SaveAction($data){

      $actionForm = new WorkFlowActionForm($this->Core, $data);

      if($actionForm->Validate($data)){
     
            $action = new WorkFlowAction($this->Core);
     
            $actionForm->Populate($action);
            $action->Save();

            return true;
      }

      return false;
   }

   /***
    * Delete a action
    */
   function RemoveAction($actionId){
      
      $action = new WorkFlowAction($this->Core);
      $action->GetById($actionId);
      $action->Delete();
   }


          /*action*/
 }?>
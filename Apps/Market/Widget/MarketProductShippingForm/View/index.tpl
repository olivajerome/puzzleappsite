{{form->Open()}}
{{form->Error()}}
{{form->Success()}}

{{form->Render(Id)}}
{{form->Render(OfferId)}}
{{form->Render(ProductId)}}

    <div>
        <label>{{GetCode(Market.TrackingNumber)}}</label> 
        {{form->Render(TrackingNumber)}}
    </div>

    <div>
        <label>{{GetCode(Market.TrackingUrl)}}</label> 
        {{form->Render(TrackingUrl)}}
    </div>

    <div class='center marginTop' >   
        {{form->Render(btnSaveShipping)}}
    </div>  

{{form->Close()}}
var MarketProductShippingForm = function(){};

/***
 * Init the form
 * @returns {undefined}
 */
MarketProductShippingForm.Init = function(){
    Event.AddById("btnSaveShipping", "click", MarketProductShippingForm.SaveElement);
};

/***
* Get the Id of element 
*/
MarketProductShippingForm.GetId = function(){
    return Form.GetId("MarketProductShippingFormForm");
};

/*
* Save the Element
*/
MarketProductShippingForm.SaveElement = function(e){
   
    if(Form.IsValid("MarketProductShippingFormForm"))
    {
    let request = new Http("Market", "SaveProductShipping");
        request.SetData(Form.Serialize("MarketProductShippingFormForm"));

        request.Post(data => {

            data = JSON.parse(data);

            if(data.status == "error"){
                Form.RenderError("MarketProductShippingFormForm", data.data.message);
            } else{

                Dialog.Close();
            }
        });
    }
};


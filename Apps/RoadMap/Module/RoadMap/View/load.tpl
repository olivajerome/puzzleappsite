<section>
    <input type='hidden' id='hdRoadMapId'  value='{{roadMapId}}' />

    <div class='tools'> 
        <a id='btnNewColumn'><i class='fa fa-plus'></i>{{GetCode(RoadMap.AddColumn)}}</a>
        <div style='float:right'>
            <i class='fa fa-gear' id='btnParameters'>&nbsp;</i>
        </div>    
    </div>
    <div class="scroller">    
        <div class='columns'>

            {{foreach columns}} 
            <div class ='column' id='{{element->IdEntite}}' >

                <h1>{{element->Name->Value}}</h1>

                <div class='buttons'>

                    <i class='fa fa-plus btnNewTicket' title='{{GetCode(RoadMap.AddTicket)}}' >
                        {{GetCode(RoadMap.AddTicket)}}</i> 

                    <i class='fa fa-edit bntEditColumn' title='{{GetCode(RoadMap.EditColumn)}}'>&nbsp;</i>
                    <i class='fa fa-trash bntDeleteColumn' title='{{GetCode(RoadMap.DeleteColumn)}}'>&nbsp;</i>

                </div>

                {{Apps\RoadMap\Decorator\RoadMapDecorator->GetTickets(element)}}


            </div> 
            {{/foreach columns}} 

        </div>    
    </div>

</section>
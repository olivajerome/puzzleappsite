<?php

namespace Apps\Cms\Widget\LayoutEditor;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;
use Core\Entity\Entity\Argument;
use Apps\Cms\Entity\CmsPage;
use Apps\Cms\Entity\CmsBlock;

class LayoutEditor {
    
    public function __construct($core) {
        $this->Core = $core;
    }
    
    /***
     * Render the widget
     */
    public function Render(){
        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);
        
        $pages = new CmsPage($this->Core);
        $view->AddElement(new ElementView("Pages",$pages->Find("CmsId = 1")));
        
        $blocks = new CmsBlock($this->Core);
        
        $view->AddElement(new ElementView("HeaderBlock",$blocks->Find("ParentId = 1 order by Position")));
        $view->AddElement(new ElementView("FooterBlock",$blocks->Find("ParentId = 3 order by Position")));
        
        return $view->Render();
    }
}
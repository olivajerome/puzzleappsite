
function ToolTip(control)
{
	this.data = "";
	this.url="";
	this.control = document.getElementById(control);


	this.Open=function()
	{
    
        controlRectangle = this.control.getBoundingClientRect();
    
		this.page = "Ajax.php";

		this.core = document.createElement('div');
                this.core.className = "toolTip";
        this.core.style.position='absolute';
        this.core.style.padding = '10px';
	    //this.core.style.width= "200px";
	    // this.core.style.height="200px";
        this.core.style.overflow="auto";
        this.core.style.border='1px solid black';
        this.core.style.left= controlRectangle.x + 10 + "px";

        	this.core.style.top= controlRectangle.y + 10 + "px";
   		//this.core.style.backgroundColor="white";
   		//this.core.innerHTML="<table  style='width:100%;' class='titre'><tr><td  style='text-align:left;' ></td><td style='text-align:right;'><i class='fa fa-remove' alt='' title='Fermer' onclick='CloseTool(this)'></td><td style='width:15px'>&nbsp;</i></td></tr></table>";

		document.body.appendChild(this.core);

		this.core.innerHTML += Language.GetCode(this.control.dataset.title);
		this.data = "";
	};


	this.Send=function()
	{
	  var JAjax = new ajax();
	  JAjax.data = this.data;
	  return JAjax.GetRequest(this.page);
	};
    
    this.Close = function()
    {
        this.core.parentNode.removeChild(this.core);
    }
;}

CloseTool = function(control)
{
	control.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.removeChild(control.parentNode.parentNode.parentNode.parentNode.parentNode);
};

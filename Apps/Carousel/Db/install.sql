CREATE TABLE IF NOT EXISTS `CarouselCarousel` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`Name` VARCHAR(200)  NOT NULL,
`Code` VARCHAR(200)  NOT NULL,
`Type` INT  NOT NULL,
`Description` TEXT  NULL,
`Entity` VARCHAR(200)  NULL,
`Parameters` TEXT  NULL,
PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `CarouselItem` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`CarouselId` int(11) NOT NULL , 
`Name` VARCHAR(200)  NOT NULL,
`Content` TEXT  NOT NULL,
PRIMARY KEY (`Id`),
CONSTRAINT `CarouselItem_CarouselCarousel` FOREIGN KEY (`CarouselId`) REFERENCES `CarouselCarousel`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
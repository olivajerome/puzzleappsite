<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Notify\Helper;

use Apps\Notify\Entity\NotifyParameters;

class EmailHelper {
    /*     * *
     * Send Email by Smtp Serveur
     */

    public static function SendMail($core, $email, $subject, $message) {
        $parameter = new NotifyParameters($core);
        $parameters = $parameter->Find("Id > 0");

        if (count($parameters) > 0) {

            $parameter = $parameters[0];

            //Entete du mail
            $headers = array(
                'From' => $parameter->Sender->Value,
                'Reply-To' => $parameter->ReplyTo->Value,
                'Content-type' => 'text/html; charset="UTF-8'
            );
        } else {
            $headers = array();
        }

        //Send Mail by Smpt Php
        mail($email, $subject, $message, $headers);
    }
}

CREATE TABLE IF NOT EXISTS `MarketProductCaracteristique` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`ProductId` INT  NOT NULL,
`CaracteristiqueId` INT  NOT NULL,
`ItemId` INT  NOT NULL,
PRIMARY KEY (`Id`),
CONSTRAINT `MarketProduct_MarketProductCaracteristique` FOREIGN KEY (`ProductId`) REFERENCES `MarketProduct`(`Id`),
CONSTRAINT `MarketCaracteristique_MarketProductCaracteristique` FOREIGN KEY (`CaracteristiqueId`) REFERENCES `MarketCaracteristique`(`Id`),
CONSTRAINT `MarketCaracteristiqueItem_MarketProductCaracteristique` FOREIGN KEY (`ItemId`) REFERENCES `MarketCaracteristiqueItem`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
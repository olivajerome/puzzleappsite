<?php 
/*
* PuzzleApp
* Webemyos
* Jérôme Oliva
* GNU Licence
*/

namespace Apps\Notify\Entity;
use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class NotifyTemplate extends Entity  
{
	//Constructeur
	function __construct($core)
	{
		//Version
		$this->Version ="2.0.0.0"; 

		//Nom de la table 
		$this->Core=$core; 
		$this->TableName="NotifyTemplate"; 
		$this->Alias = "NotifyTemplate"; 

		$this->Code = new Property("Code", "Code", TEXTBOX,  true, $this->Alias); 
		$this->Description = new Property("Description", "Description", TEXTAREA,  false, $this->Alias); 
		$this->Title = new Property("Title", "Title", TEXTAREA,  false, $this->Alias); 
		$this->Content = new Property("Content", "Content", TEXTAREA,  false, $this->Alias); 

		//Creation de l entité 
		$this->Create(); 
	}
}
?>
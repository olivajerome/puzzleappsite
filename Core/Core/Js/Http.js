var  Http = function(app, methode){

    this.app = app;
    this.methode = methode ;
    this.parameters = new Array();
    
    this.Add = function(key, value){
        this.parameters[key] = value;

        return this;
    };

    this.SetData = function(data){
        this.data = data;
    };

    this.Post=function(callBack){

        let data = "App="+this.app;
            data += "&Class=" + this.app;
            data += "&Methode=" + this.methode;
        
        for(parameter in this.parameters){
            data += "&"+parameter+"=" + this.parameters[parameter];
        }

        if(this.data != undefined){
            data += this.data;   
        }

        Request.Post("Ajax.php", data).then(data => {
            callBack(data);
        });
    };
};

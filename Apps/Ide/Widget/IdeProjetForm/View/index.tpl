{{form->Open()}}
{{form->Error()}}
{{form->Success()}}

{{form->Render(Id)}}

    <div>
        <label>{{GetCode(Ide.ProjetName)}}</label> 
        {{form->Render(Name)}}
    </div>

    <div>
        <label>{{GetCode(Ide.ProjetDescription)}}</label> 
        {{form->Render(Description)}}
    </div>

    <div class='center marginTop' >   
        {{form->Render(btnSaveProjet)}}
    </div>  

{{form->Close()}}
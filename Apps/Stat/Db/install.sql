CREATE TABLE IF NOT EXISTS `StatApp` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`Code` VARCHAR(200)  NOT NULL,
`Date` VARCHAR(200)  NOT NULL,
`User` VARCHAR(200)  NOT NULL,
PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
<?php

namespace Apps\Shop\Widget\CardWidget;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;

use Apps\Shop\Helper\CartHelper;

class CardWidget extends Form {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "") {
        $this->Core = $core;
        $this->Data = $data;

        //On le fait ici pour avoir le formuaire � l'affichage mais aussi � la validation
        $this->Init($data);
    }

    /**
     * Initialisation 
     */
    function Init($data) {
        $this->form = new Form("CardWidgetForm", $data);

        $this->form->SetView(__DIR__ . "/View/index.tpl", $this->Core);
        $this->form->SetAction("/View/index.tpl");

        $this->form->Add(array("Type" => "Hidden",
            "Id" => "Id",
        ));
        
        $this->form->Add(array("Type" => "TextBox",
            "Id" => "Name",
            "Validators" => ["Required"]
        ));
        
        $this->form->Add(array("Type" => "Button",
            "Id" => "btnSave",
            "Value" => "Save",
            "CssClass" => "btn btn-primary"
        ));
    }

    /*     * *
     * G�n�rer les formulaire
     */

    function Render() {
        return $this->form->Render();
    }

    /*     * *
     * Valide les donn�es selon les diff�rents formulaires
     */

    function Validate($data = "") {
        $errors = $this->form->Validate($this->Data);

        return $errors;
    }

    /*     * *
     * Charge les controls avec les donn�es de l'entit�
     */

    function Load($entity) {
        $this->form->Load($entity);
    }

    /*     * *
     * Charge l'entit� avec les donn�es des diff�rents formulaire
     */

    function Populate($entity) {
        
    }

    /*     * *
     * Gestion des erreurs
     */

    function Error() {
        $errors[] = $this->form->errors;

        return json_encode(array("statut" => "Error", "message" => $errors));
    }


    /***
     * Add To Card Button
     */
    function RenderAddToCard($data){

        $form = new Form("CardWidgetForm", $data);

        $form->SetView(__DIR__ . "/View/addToCard.tpl", $this->Core);
        $form->SetAction("/View/addToCard.tpl");

        $form->Add(array("Type" => "Hidden",
            "Id" => "Id",
            "Value" => $data
        ));
        
        $form->Add(array("Type" => "TextBox",
            "Id" => "Name",
            "Validators" => ["Required"]
        ));
        
        $form->Add(array("Type" => "Button",
            "Id" => "btnAddToCart",
            "Value" => $this->Core->GetCode("Shop.AddToCard"),
            "CssClass" => "btn btn-primary"
        ));

        return $form->Render();
    }

    /**
     * Icon du panier
     */
    function RenderCardIcon(){

        $view = new View(__DIR__ . "/View/cardIcon.tpl", $this->Core);

        $view->AddElement(new ElementView("nbCart", CartHelper::GetNumberElement()));

        return $view->Render();
    }
}

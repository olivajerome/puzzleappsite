CREATE TABLE IF NOT EXISTS `FolderFolder` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`UserId` INT  NOT NULL,
`Name` VARCHAR(200)  NOT NULL,
`Code` VARCHAR(200)  NOT NULL,
`Description` TEXT  NOT NULL,
`Url` VARCHAR(500)  NOT NULL,
`Status` INT  NOT NULL,
`ParentId` INT  NULL ,
`AppName` VARCHAR(200)  NULL ,
`AppId` INT  NULL ,
`EntityName` VARCHAR(200)  NULL ,
`EntityId` INT  NULL ,
PRIMARY KEY (`Id`),
CONSTRAINT `ee_user_FolderFolder` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
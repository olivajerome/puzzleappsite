<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Market\Module\Member;

use Core\Controller\Controller;
use Core\View\View;
use Core\View\ElementView;
use Core\Utility\Format\Format;
use Core\Utility\Date\Date;
use Core\Control\TabStrip\TabStrip;


use Apps\Market\Entity\MarketProduct;
use Apps\Market\Widget\MarketProductForm\MarketProductForm;
use Apps\Market\Helper\MarketHelper;
use Apps\Market\Helper\ProductHelper;
use Apps\Market\Widget\MarketOfferForm\MarketOfferForm;
use Apps\Market\Entity\MarketProductOffer;
use Apps\Market\Helper\NotifyHelper;
use Apps\Market\Decorator\OfferDecorator;
use Apps\Market\Widget\MarketProductShippingForm\MarketProductShippingForm;
use Apps\Market\Entity\MarketProductShipping;


/*
 * 
 */
 class MemberController extends Controller
 {
    /**
     * Constructeur
     */
    function _construct($core="")
    {
          $this->Core = $core;
    }

    /**
     * Create
     */
    function Create()
    {
    }

    /**
     * Init
     */
    function Init()
    {
    }

    /**
     * Render
     */
    function Show($all=true)
    {
         return $this->Index();
    }
    
    /*
    * Get the home page
    */
   function Index()
   {
       $view = new View(__DIR__."/View/index.tpl", $this->Core);
       $tabMarket = new TabStrip("tabMarket");
       $tabMarket->AddTab($this->Core->GetCode("Market.MyProduct"), $this->GetTabProduct());
       $tabMarket->AddTab($this->Core->GetCode("Market.OffersSended"), $this->GetTabOfferSended());
       $tabMarket->AddTab($this->Core->GetCode("Market.OffersRecevied"), $this->GetTabOfferRecevied());
       
       $view->AddElement(new ElementView("tabMarket", $tabMarket->Show()));

       return $view->Render();
   }
   
   /****
    * Product of the User 
    */
   function GetTabProduct(){

       $view = new View(__DIR__."/View/tabProduct.tpl", $this->Core);

       $products = ProductHelper::GetByUser($this->Core, $this->Core->User->IdEntite);
       $view->AddElement(new ElementView("Products", $products));

       return $view->Render();       
   }
   
   /***
    * Offer Rcevied
    */
    function GetTabOfferSended(){

        $view = new View(__DIR__."/View/tabOfferSended.tpl", $this->Core);
        $offers = new MarketProductOffer($this->Core);
        
        $view->AddElement(new ElementView("Offers", $offers->Find("UserId = ".$this->Core->User->IdEntite." ORDER BY ID DESC")));
  
        return $view->Render();       
    }

   /***
    * Offer recevied on the User Product
    */
   function GetTabOfferRecevied(){
      
      $view = new View(__DIR__."/View/tabOfferRecevied.tpl", $this->Core);

      $products = ProductHelper::GetByUser($this->Core, $this->Core->User->IdEntite);
      $productId = array();
      
      foreach($products as $product){
          $productId[] = $product->IdEntite;
      }
      
      if(count($productId) > 0 ){
        $offers = new MarketProductOffer($this->Core);
        $view->AddElement(new ElementView("Offers", $offers->Find("ProductId in (".implode(',', $productId).") ORDER BY ID DESC")));
      } else {
        $view->AddElement(new ElementView("Offers", array()));
      }

      return $view->Render();       
   }
   
   /***
    * Save a product
    */
   function SaveProduct($data){
       
       $productForm = new MarketProductForm($this->Core, $data);
       
       if($productForm->Validate($data)){
       
           $product = new MarketProduct($this->Core);
           
           if($data["Id"]){
               $product->GetById($data["Id"]);
           } else {
               $product->UserId->Value = $this->Core->User->IdEntite;
               
               $market = MarketHelper::GetMarket($this->Core);
               $product->MarketId->Value = $market[0]->IdEntite;
               
               $product->Code->Value = Format::ReplaceForUrl($data["Title"]);
               
               $product->Status->Value = 1;
           }
           
           $productForm->Populate($product);
           $product->Save();

           if($data["Id"] == ""){
                $product->IdEntite = $core->Db->GetInsertedId();
            }
        
            $product->SaveImage($data["dvUpload-UploadfileToUpload"], true);
           
           return true;
       }
       
       return $productForm->errors;
   }
   
   /**
    * Send offer on 
    * @param type $data
    */
   function SendOffer($data){
    
       $offerForm = new MarketOfferForm($this->Core, $data);
       
       if($offerForm->Validate($data)){
           
           $offer = new MarketProductOffer($this->Core);
           $offerForm->Populate($offer);
           $offer->UserId->Value = $this->Core->User->IdEntite;
           $offer->DateCreated->Value = Date::Now();
           $offer->Status->Value = MarketProductOffer::SENDED;
           $offer->Save();
           
           
           $offer->IdEntite = $this->Core->Db->GetInsertedId();
            
           //Notify the Vendeur  
           NotifyHelper::SendOffer($this->Core, $offer);
           
           return true;
       } else {
           
           return $offerForm->errors;
       }
   }
   
   /***
    * Accept an offer
    */
   function AcceptOffer($offerId){
        
        $offer = new MarketProductOffer($this->Core);
        $offer->GetById($offerId);
        $offer->Status->Value = MarketProductOffer::ACCEPTED;
        $offer->Save();

        //Set Product Status = 2;
        $offer->Product->Value->Status->Value = MarketProduct::SEALED;
        $offer->Product->Value->Save();

        //Notify the Vendeur  
        NotifyHelper::AcceptOffer($this->Core, $offer);
    
        return true;
    }
   
   /***
    * Refuse an offer
    */
    function RefuseOffer($offerId){
        
        $offer = new MarketProductOffer($this->Core);
        $offer->GetById($offerId);
        $offer->Status->Value = MarketProductOffer::REJECTED;
        $offer->Save();

        return true;
    }
    
    /***
    * Confirm Recept the Product
    */
    function ConfirmReceptProduct($offerId){
        
        $offer = new MarketProductOffer($this->Core);
        $offer->GetById($offerId);
        $offer->Status->Value = MarketProductOffer::PRODUCT_RECEVEIVED;
        $offer->Save();

        return true;
    }
    
     /***
    * Confirm Recept the Product
    */
    function ConfirmNoReceptProduct($offerId){
        
        $offer = new MarketProductOffer($this->Core);
        $offer->GetById($offerId);
        $offer->Status->Value = MarketProductOffer::PRODUCT_NO_RECEVEIVED;
        $offer->Save();

        return true;
    }
    
    
    

    /***
     * Shipping Infprmation of a product
     */
    function SaveProductShipping($data){
    
        $shippingForm = new MarketProductShippingForm($this->Core, $data);
       
        if($shippingForm->Validate($data)){
            
            $shipping = new MarketProductShipping($this->Core);
            $shippingForm->Populate($shipping);
            $shipping->DateSended->Value = Date::Now();
            $shipping->Status->Value = MarketProductOffer::SENDED;
            $shipping->Save();

            //Set Offer Status = 3;
            $offer = new MarketProductOffer($this->Core);
            $offer->GetById($data["OfferId"]);
            $offer->Status->Value = MarketProductOffer::PRODUCT_SENDED;
            $offer->Save();
            
            
            return true;
        } else {
            
            return $shippingForm->errors;
        }
    
    }

    /***
     * Return the status button of an offer
     */
    function GetStatus($offer){
        $offer = new MarketProductOffer($this->Core);
        $offer->GetById($offerId);
      
        $offerDecorator = new OfferDecorator($this->Core);
        return $offerDecorator->RenderButtonStatut($offer);
    }
    
   /*action*/
 }?>
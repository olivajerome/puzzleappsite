CREATE TABLE IF NOT EXISTS `SkillCategory` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`Name` varchar(200) NOT NULL, 
`AppName` varchar(200) NULL, 
PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `Skill` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`Name` varchar(200) NOT NULL, 
`Description` Text NULL,
`CategoryId` int(11) NOT NULL,
PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `SkillEntity` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`EntityName` Text NOT NULL, 
`EntityId` int(11) NOT NULL, 
`SkillId` int(11) NOT NULL,
`Description` Text NULL,
PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
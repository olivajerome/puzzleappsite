<?php

namespace Apps\Carousel\Widget\CarouselForm;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;

class CarouselForm extends Form {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "") {
        $this->Core = $core;
        $this->Data = $data;

        $this->Type = array( $this->Core->GetCode("Carousel.Single") => 1,
                             $this->Core->GetCode("Carousel.Multi") => 2,
        );

        $this->Init($data);
    }

    /**
     * Initialisation 
     */
    function Init($data) {
        $this->form = new Form("CarouselFormForm", $data);

        $this->form->SetView(__DIR__ . "/View/index.tpl", $this->Core);
        $this->form->SetAction("/View/index.tpl");

        $this->form->Add(array("Type" => "Hidden",
            "Id" => "Id",
        ));
        
        $this->form->Add(array("Type" => "TextBox",
            "Id" => "Name",
            "Validators" => ["Required"]
        ));
        
        $this->form->Add(array("Type" => "TextBox",
            "Id" => "Description",
        ));

        $this->form->Add(array("Type" => "ListBox",
            "Id" => "Type",
            "Values" => $this->Type,
            "Validators" => ["Required"]
        ));

        $this->form->Add(array("Type" => "TextBox",
            "Id" => "Entity",
        ));

        $this->form->Add(array("Type" => "TextBox",
            "Id" => "Parameters",
        ));


        if ($data->IdEntite != "") {
            $this->form->AddElementView(new ElementView("Items", $data->GetItems()));
        } else {
            $this->form->AddElementView(new ElementView("Items", array()));
        }    

        $this->form->Add(array("Type" => "Button",
            "Id" => "btnSaveCarousel",
            "Value" => $this->Core->GetCode("Carousel.Save"),
            "CssClass" => "btn btn-primary"
        ));
    }

    /*     * *
     * Render the html form
     */

    function Render() {
        return $this->form->Render();
    }

    /*     * *
     * Validate the data
     */

    function Validate($data = "") {
        $errors = $this->form->Validate($this->Data);

        return $errors;
    }

    /*     * *
     * Load control with the entity
     */

    function Load($entity) {
        $this->form->Load($entity);
    }

    /*     * *
     * Populate the entity with the form Data
     */

    function Populate($entity) {
        
    }

    /*     * *
     * Error message
     */

    function Error() {
        $errors[] = $this->form->errors;

        return json_encode(array("statut" => "Error", "message" => $errors));
    }

}

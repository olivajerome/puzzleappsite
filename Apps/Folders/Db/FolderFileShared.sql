CREATE TABLE IF NOT EXISTS `FolderFileShared` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`FileId` INT  NOT NULL,
`UserId` INT  NOT NULL,
PRIMARY KEY (`Id`),
CONSTRAINT `FolderFile_FolderFileShared` FOREIGN KEY (`FileId`) REFERENCES `FolderFiles`(`Id`),
CONSTRAINT `ee_user_FolderFileShared` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
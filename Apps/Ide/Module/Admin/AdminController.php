<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Ide\Module\Admin;

use Apps\Ide\Helper\ProjetHelper;
use Core\Controller\AdministratorController;
use Core\View\View;
use Core\View\ElementView;
use Core\Control\Button\Button;
use Core\Control\Link\Link;

class AdminController extends AdministratorController {

    /*
     * Show the User Projet
     */

    function Show() {
        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);

        $btnNewProjet = new Button(BUTTON, "btnNewProjet");
        $btnNewProjet->CssClass = "btn btn-success";
        $btnNewProjet->Value = $this->Core->GetCode("Ide.StartProjet");
        $btnNewProjet->OnClick = "IdeAction.NewProjet()";

        $view->AddElement($btnNewProjet);

        $projects = ProjetHelper::GetRecentProjet($this->Core);

        $view->AddElement(new ElementView("Projects", $projects));
        return $view->Render();
    }
}


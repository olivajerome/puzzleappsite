{{form->Open()}}
{{form->Error()}}
{{form->Success()}}

{{form->Render(Id)}}

    <div>
        <label>{{GetCode(Market.Name)}}</label> 
        {{form->Render(Name)}}
    </div>

    <div>
        <label>{{GetCode(Market.Description)}}</label> 
        {{form->Render(Description)}}
    </div>

    <div>
        {{form->Render(Upload)}}
    </div>

    <div>
       {{form->RenderImage()}}
    </div>

    <div class='center marginTop' >   
        {{form->Render(btnSaveCategory)}}
    </div>  

{{form->Close()}}
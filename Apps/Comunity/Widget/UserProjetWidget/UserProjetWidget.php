<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Apps\Comunity\Widget\UserProjetWidget;

use Core\View\View;
use Core\View\ElementView;
use Apps\Discuss\Helper\DiscussHelper;

class UserProjetWidget
{
    
    /***
     * 
     */
    function __construct($core, $status ="" )
    {
        $this->Core= $core;
        $this->Status = $status;
    }
    
    /**
     * Gere le rendu HTML du wodget
     */
    function Render()
    {
       $view = new View(__DIR__."/View/index.tpl", $this->Core);
       
       //Récupération  des Projets Actifs
      // $projets = DiscussHelper::GetLastRencontreByReseau($this->Core, $this->CodeReseau, $this->Status, false);
      
      // TODO Prendre les Group actif
        $projets = array();
        
       $view->AddElement(new ElementView("actif", (($this->Status == 1  || $this->Status =="")  ? "active" : "")));
       $view->AddElement(new ElementView("create", ($this->Status == "0" ? "active" : "")));
       $view->AddElement(new ElementView("close", ($this->Status == 10 ? "active" : "")));
       
         
       
       $allProjet = array();
       $userProjet = array();
       $followProjet = array();
       
       foreach($projets as $projet)
       {
           if(DiscussHelper::UserParticipate($this->Core, $projet->IdEntite))
           {
               $userProjet[] = $projet;
           } 
           else if(DiscussHelper::UserFollow($this->Core, $projet->IdEntite))
           {
                $followProjet[] = $projet;
           }
           else
           {
               $allProjet[] = $projet;
           }
       }
       
       $view->AddElement(new ElementView("userProjet",  $userProjet));
       $view->AddElement(new ElementView("followProjet",  $followProjet));
       $view->AddElement(new ElementView("allProjet",  $allProjet));
       
       
       
       return $view->Render();
    }
}
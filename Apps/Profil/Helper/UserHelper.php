<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Profil\Helper;

use Core\Entity\City\City;
use Core\Entity\User\User;
use Apps\Profil\Entity\ProfilCompetenceEntity;
use Apps\Profil\Widget\ProfilForm\ProfilForm;

class UserHelper
{
    /***
     * Update the User information
     */
    public static function Update($core, $data){

        $user = new User($core);
        $user->GetById($data["Id"]);
        
        $profilForm = new ProfilForm($core, $data);

        if($profilForm->Validate($data)){

             $profilForm->Load($user);
            if($data["GroupeId"] != ""){
                $user->GroupeId->Value = $data["GroupeId"];
            }
           
            $user->Save();
            $user->SaveImage($data["dvUpload-UploadfileToUpload"], true);

            return true;
        }

        return false;
    }

    /**
     * Sauvegarde les informations de l'utilisateur
     */
    public static function Save($core, $firstName, $name, $description)
    {
        if($firstName != "" && $name != "")
        {
            $core->User->FirstName->Value = $firstName;
            $core->User->Name->Value = $name;
            $core->User->Description->Value = $description;
         
                      
            $core->User->Save();
        
            return "<div class='success'>".$core->GetCode("SaveOk")."</div>";
        }
        else
        {
           return "<div class='error'>".$core->GetCode("CompetenceProfil.SaveInformation")."</div>";
        }
    }
    
     /**
     * Enregistre les compétences d'un utilisateur
     * @param type $core
     * @param type $userId
     * @param type $competenceId
     */
    public static function SaveCompetence($core, $userId, $competenceId)
    {
        //Suppression avant enregistrement
        self::DeleteCompetence($core, $userId);
        
        $competences = explode(";", $competenceId);
        
        foreach($competences as $competence)
        {
            $competenceUser = new ProfilCompetenceEntity($core);
            $competenceUser->UserId->Value = $userId;
            $competenceUser->CompetenceId->Value = $competence;
            
            $competenceUser->Save();
        }
    
        echo "<div class='success'>".$core->GetCode("SaveOk")."</div>";
    
    }
    
    /**
     * Supprime les compétences d'un utilisateur
     * @param type $core
     * @param type $userId
     */
    public static function DeleteCompetence($core, $userId)
    {
        $request = "DELETE FROM ProfilCompetenceEntity WHERE UserId=".$userId;
        $core->Db->Execute($request);
    }
    
    
}

?>

<div class='block'>
    <h2>{{GetCode(Market.LastProduct)}}</h2>
    <p>{{Product->Title->Value}}</p>
    <b>{{Product->Price->Value}}</b>

    <img style="width:100%"  src='{{Product->GetImagePath()}}' />

    <div class='center'>
        <a class='btn btn-primary' href='{{GetPath(/Market/Product/{{Product->Code->Value}})}}'>
            {{GetCode(Market.Discover)}}
        </a>
    </div>
</div>
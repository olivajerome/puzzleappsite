<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Forum\Helper;

use Apps\Forum\Entity\ForumCategory;
use Apps\Forum\Entity\ForumMessage;

class SitemapHelper {
    /*     * *
     * Obtient le site map du Forum
     */

    public static function GetSiteMap($core, $returnUrl = false) {
        $urlBase = $core->GetPath("");

        $sitemap .= "<url><loc>$urlBase/Forum</loc></url>";
        $urls = "";
     
        $category = new ForumCategory($core);
        $categorys = $category->GetAll();

        foreach ($categorys as $category) {
            $sitemap .= "<url><loc>$urlBase/Forum/Category/" . $category->Code->Value . "</loc></url>";

            $url = $urlBase . "/Forum/Category/" . $category->Code->Value;
            $urls .= '<br/><a target="__blank" href="' . $url . '">' . $url . '</a>';
        }

        $message = new ForumMessage($core);
        $messages = $message->GetAll();

        foreach ($messages as $message) {
            $sitemap .= "<url><loc>$urlBase/Forum/Sujet/" . $message->Code->Value . "</loc></url>";

            $url = $urlBase . "/Forum/Sujet/" . $message->Code->Value;
            $urls .= '<br/><a target="__blank" href="' . $url . '">' . $url . '</a>';
        }

        if ($returnUrl == true) {
            return $urls;
        } else {
            return $sitemap;
        }
    }
}

<?php

namespace Apps\Notes\Widget\LastNotes;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;

use Apps\Notes\Entity\NotesNote;

class LastNotes extends Form {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "") {
        $this->Core = $core;
    }

    /*     * *
     * Render the html form
     */

    function Render() {
        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);
        $notes = new NotesNote($this->Core);
        $view->AddElement(new ElementView("Notes", $notes->Find("Id > 0 Order by Id Desc limit 0,3")));
        
        return $view->Render();
    }
}

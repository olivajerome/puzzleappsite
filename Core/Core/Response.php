<?php

/**
 * PuzzleApp 
 * PuzzleApp.org - The Hybride Framework.
 * Oliva Jérôme
 * GNU Licence
 * */

namespace Core\Core;

class Response {

    /*
     * Return a formated success response
     */
    public static function Success($data, $json = true) {
        return json_encode(array("status" => "success", "data" => $data));
    }

    /*
     * Return a formated error response
     */
    public static function Error($data = [], $json = true) {
        return json_encode(array("status" => "error", "data" => $data));
    }
}

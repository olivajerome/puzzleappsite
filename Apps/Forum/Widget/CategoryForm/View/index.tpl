{{form->Open()}}
{{form->Error()}}
{{form->Success()}}

{{form->Render(Id)}}
    <div>
        <label>{{GetCode(Forum.CategoryName)}}</label> 
        {{form->Render(Name)}}
    </div>
    <div>
        <label>{{GetCode(Forum.CategoryDescription)}}</label> 
        {{form->Render(Description)}}
    </div>
    <div class='center marginTop' >   
        {{form->Render(btnSaveCategory)}}
    </div>  

{{form->Close()}}
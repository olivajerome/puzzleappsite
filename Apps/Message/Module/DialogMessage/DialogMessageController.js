var DialogMessageController = function(){};


DialogMessageController.OpenConversation = function(){
  
    Dashboard.AddEventById("btnSendTchatMessage", "click", MessageAction.AddMessage);

    let tbTchatMessage = document.getElementById("tbMessage");
    tbTchatMessage.focus();

    MessageAction.ScrollBottom();
};
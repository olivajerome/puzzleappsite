<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Folders\Module\Home;

use Core\Control\Button\Button;
use Core\Controller\Controller;
use Core\View\View;
use Core\View\ElementView;

use Apps\Folders\Helper\FolderHelper;

/*
 * 
 */
 class HomeController extends Controller
 {
    /**
     * Constructeur
     */
    function _construct($core="")
    {
          $this->Core = $core;
    }

    /**
     * Creation
     */
    function Create()
    {
    }

    /**
     * Initialisation
     */
    function Init()
    {
    }

    /**
     * Affichage du module
     */
    function Show($all=true)
    {
         return $this->Index();
    }
    
    /*
    * Get the home page
    */
   function Index($projetId = "")
   {
       $view = new View(__DIR__."/View/index.tpl", $this->Core);
       
       $folders = FolderHelper::GetFolders($this->Core, false,  $projetId);
       
       $view->AddElement(new ElementView("Folders", $folders));
       
       $files = FolderHelper::GetFiles($this->Core);
       $view->AddElement(new ElementView("Files", $files));
       
       return $view->Render();
   }
          
          /*action*/
 }?>
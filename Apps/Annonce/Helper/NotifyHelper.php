<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */


namespace Apps\Annonce\Helper;

use Apps\Annonce\Entity\AnnonceAnnonce;
use Core\Dashboard\DashBoardManager;

class NotifyHelper
{
    /***
     * Send notify Response to a annonce
     */
    public static function NewResponse($core, $annonceId){

        $annonce = new AnnonceAnnonce($core);
        $annonce->GetById($annonceId);

        $notify = DashBoardManager::GetApp("Notify", $core);
     
        $notify->AddNotify( $core->User->IdEntite,
                            "Annonce-NewResponseAnnonce",
                            $annonce->UserId->Value, 
                            "Annonce",
                            $annonceId,
                            $core->GetCode("Annonce.NewReponseAnnonceTitle"),
                            $core->GetCode("Annonce.NewReponseAnnonceMessage"),
                         );
    }
    
    /***
     * Get Detail Notify
     */
    public static function GetDetailNotify($core, $notify){
        
        switch($notify->Code->Value ){
            
            case "Annonce-NewResponseAnnonce":
                $annonce = new AnnonceAnnonce($core);
                $annonce->GetById($notify->EntityId->Value);
                
                $view = "<div>";
                $view .= "<h1>".$core->GetCode("Annonce.NewResponseAnnonce")."</h1>";
                $view .= "<p><b>" .$notify->GetUser() ."</b> ". $core->GetCode("Annonce.ResponseToYourAnnonce"); 
                $view .= "<b>".$annonce->Title->Value."</b></p>";
                $view .= "<span class='date'>".$notify->DateCreate->Value."</span>";
                $view .= "</div>"; 
                
                break;
        }
        
        return $view; 
       
        
    }
    
}
{{form->Open()}}
{{form->Error()}}
{{form->Success()}}

{{form->Render(Id)}}
    <div>
        <label>{{GetCode(Shop.CategoryName)}}</label> 
        {{form->Render(Name)}}
    </div>
    <div>
        <label>{{GetCode(Shop.CategoryDescription)}}</label> 
        {{form->Render(Description)}}
    </div>
    
    <div>
        {{form->Render(Upload)}}
    </div>

    <div>
       {{form->RenderImage()}}
    </div>
    
    <div class='center marginTop' >   
        {{form->Render(btnSaveCategory)}}
    </div>  

{{form->Close()}}
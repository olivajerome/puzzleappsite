var IdeWidgetForm = function(){};

/***
 * Init the form
 * @returns {undefined}
 */
IdeWidgetForm.Init = function(){
    Event.AddById("btnSave", "click", IdeWidgetForm.SaveWidget);
};

/***
* Get the Id of element 
*/
IdeWidgetForm.GetId = function(){
    return Form.GetId("IdeWidgetFormForm");
};

/*
* Save the Element
*/
IdeWidgetForm.SaveWidget = function(e){
   
    if(Form.IsValid("IdeWidgetFormForm"))
    {

    let data = "Class=Ide&Methode=AddWidget&App=Ide";
        data +=  Form.Serialize("IdeWidgetFormForm");
        data += "&Projet=" + Ide.Projet;

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.status == "error"){
                 Form.RenderError("IdeWidgetFormForm", data.data.message);
            } else{

                Form.SetId("IdeWidgetFormForm", data.data.Id);
                IdeElement.LoadRefreshWidget();
                Dialog.Close();
            }
        });
    }
};


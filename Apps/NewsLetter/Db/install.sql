CREATE TABLE IF NOT EXISTS `NewsLetterEmailType` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`Code` TEXT  NOT NULL,
`Libelle` TEXT  NOT NULL,
PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `NewsLetterEmail` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`TypeId` INT  NOT NULL,
`Code` VARCHAR(200)  NOT NULL,
`Libelle` VARCHAR(200)  NOT NULL,
`Description` TEXT  NOT NULL,
`Content` TEXT  NOT NULL,
`DateCreated` DATE  NOT NULL,
`Status` INT  NOT NULL,
PRIMARY KEY (`Id`),
CONSTRAINT `NewsLetterEmailType_NewsLetterEmail` FOREIGN KEY (`TypeId`) REFERENCES `NewsLetterEmailType`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 


INSERT INTO  NewsLetterEmailType (Code, Libelle)
VALUES 
('BaseEmail', "Email du site"),
('ScheduleEmail', "Email programmé"),
('EventEmail', "Email d'évenement");


CREATE TABLE IF NOT EXISTS `NewsLetterConfig` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`EmailFrom` VARCHAR(200)  NOT NULL,
`EmailReplyTo` VARCHAR(200)  NOT NULL,
PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `NewsLetterListeEmail` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`Libelle` VARCHAR(200)  NOT NULL,
`Description` TEXT  NOT NULL,
`Emails` TEXT  NOT NULL,
PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `NewsLetterEmailCampagne` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`EmailId` int(11) NOT NULL, 
`Libelle` varchar(200)  NOT NULL,
`Description` TEXT  NOT NULL,
`DateCreated` DATE  NOT NULL,
`DateSended` DATE  DEFAULT NULL,
`EmailSended` INT  DEFAULT NULL,
PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 


CREATE TABLE IF NOT EXISTS `NewsLetterEmailCampagneListEmail` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`CampagneId` INT  NOT NULL,
`ListEmailId` INT  NOT NULL,
PRIMARY KEY (`Id`),
CONSTRAINT `NewsLetterEmailCampagne_NewsLetterEmailCampagneListEmail` FOREIGN KEY (`CampagneId`) REFERENCES `NewsLetterEmailCampagne`(`Id`),
CONSTRAINT `NewsLetterListeEmail_NewsLetterEmailCampagneListEmail` FOREIGN KEY (`ListEmailId`) REFERENCES `NewsLetterListeEmail`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 

CREATE TABLE IF NOT EXISTS `NewsLetterEmailCampagneSend` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`CampagneId` INT  NOT NULL,
`DateSended` DATE  NOT NULL,
`Emails` TEXT  NOT NULL,
PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
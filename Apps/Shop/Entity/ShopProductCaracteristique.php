<?php 
/*
* PuzzleApp
* Webemyos
* Jérôme Oliva
* GNU Licence
*/

namespace Apps\Shop\Entity;
use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class ShopProductCaracteristique extends Entity  
{
	//Constructeur
	function __construct($core)
	{
		//Version
		$this->Version ="2.0.0.0"; 

		//Nom de la table 
		$this->Core=$core; 
		$this->TableName="ShopProductCaracteristique"; 
		$this->Alias = "ShopProductCaracteristique"; 

		$this->ProductId = new Property("ProductId", "ProductId", NUMERICBOX,  true, $this->Alias); 
		$this->CaracteristiqueId = new Property("CaracteristiqueId", "CaracteristiqueId", NUMERICBOX,  true, $this->Alias); 
		$this->ItemId = new Property("ItemId", "ItemId", NUMERICBOX,  true, $this->Alias); 

		//Creation de l entité 
		$this->Create(); 
	}
}
?>
<?php
/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Core\Control\EntityGrid;

use Core\Control\Control;
use Core\Control\Grid\IColumn;
use Core\Core\Core;

//Classe de base qui appele une fonction de classe quelconque
class EntityIconColumn extends Control implements IColumn
{
  //Propriete
  private $HeaderName;
  private $Icons;

  //Constructeur
  function __construct($headerName, $icons)
  {
     $this->Core= Core::getInstance(); 
    $this->HeaderName = $headerName;
    $this->Icons = $icons;
  }
   
  /*
   * Get the Header
   */
  public function GetHeader()
  {
      $html = $this->HeaderName;
      $html .= "<span style='display:none' class='iconAction'>" .$this->GetCell(0, false) . "</span>";
      
      return $html;
      
  }
  
  function GetCell($Entite, $all = true)
  {
    $html = ($all ? "<td>" : "");
    
    foreach($this->Icons as $icone)
    {
        $type = "Core\Control\Icone\\". $icone[0];
        $title = $this->Core->GetCode($icone[1]);
        $action = $icone[2];
        $params = $icone[3];

        $icone = new $type();
        $icone->CssClass .= " actionIcon";
        $icone->Title = $title;
        $icone->Action = $action;
        $icone->IdEntite = $Entite->IdEntite ?? "all" ;

        if($params != null)
        {
	        foreach ($params as $param) {
		        if ($icone->Params != "") {
			        $icone->Params .= ";";
		        }
		        $icone->Params .= $Entite->$param->Value;
	        }
        }
        
        $html .= $icone->Show();
    }
    
    $html .= ($all ?  "</td>" : "");
    
    return $html;
      
  }
  //asseceurs
  public function __get($name)
  {
    return $this->$name;
  }

  public function __set($name,$value)
  {
    $this->$name=$value;
  }
}
class AlignLeft extends BaseTool {
    getIcone() {
        return "<i class='fa fa-align-left'></i>";
    }

    getName() {
        return Language.GetCode("Base.TextRichEditorAlignLeft");
    }
    execute(e) {
        let instance = this;
        let newNode = instance.addStyle("textAlign", "left");
        newNode.style.width = "100%";
        newNode.style.display = "inline-block";
        super.execute();
    }
}
class AlignCenter extends BaseTool {
    getIcone() {
        return "<i class='fa fa-align-center'></i>";
    }

    getName() {
        return Language.GetCode("Base.TextRichEditorAlignCenter");
    }
    execute(e) {
        let instance = this;
        let newNode = instance.addStyle("textAlign", "center");
        newNode.style.width = "100%";
        newNode.style.display = "inline-block";
        super.execute();
    }
}


class AlignRight extends BaseTool {
    getIcone() {
        return "<i class='fa fa-align-right'></i>";
    }

    getName() {
        return Language.GetCode("Base.TextRichEditorAlignRight");
    }
    execute(e) {
        let instance = this;
        let newNode = instance.addStyle("textAlign", "right");
        newNode.style.width = "100%";
        newNode.style.display = "inline-block";
        super.execute();
    }
}

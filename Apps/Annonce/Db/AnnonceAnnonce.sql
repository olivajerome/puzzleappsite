CREATE TABLE IF NOT EXISTS `AnnonceAnnonce` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`Title` VARCHAR(200)  NOT NULL,
`Code` VARCHAR(200)  NOT NULL,
`Description` TEXT  NOT NULL,
`CategoryId` INT  NOT NULL,
`UserId` INT  NOT NULL,
`Status` INT  NOT NULL,
`AppName` VARCHAR(200)  NULL ,
`AppId` INT  NULL ,
`EntityName` VARCHAR(200)  NULL ,
`EntityId` INT  NULL ,
PRIMARY KEY (`Id`),
CONSTRAINT `AnnonceCategory_AnnonceAnnonce` FOREIGN KEY (`CategoryId`) REFERENCES `AnnonceCategory`(`Id`),
CONSTRAINT `ee_user_AnnonceAnnonce` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
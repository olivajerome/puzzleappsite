var FolderForm = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
FolderForm.Init = function(){
    Event.AddById("btnSaveFolder", "click", FolderForm.SendForm);
};

/***
* Obtient l'id de l'itinéraire courant 
*/
FolderForm.GetId = function(){
    return Form.GetId(" folderForm");
};

/*
* Sauvegare l'itineraire
*/
FolderForm.SendForm = function(e){
   
    if(Form.IsValid("folderForm"))
    {

    var data = "Class=Folders&Methode=SaveFolder&App=Folders";
        data +=  Form.Serialize("folderForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.statut == "Error"){
                Form.RenderError("folderForm", data.message);
            } else{

                Form.SetId("folderForm", data.data.Id);
                Dialog.Close();
                
                Folders.LoadDirectory(data.data.folders, data.data.Id, data.data.files);
            }
        });
    }
};


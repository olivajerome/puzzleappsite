<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Mooc\Module\Member;

use Core\Control\Button\Button;
use Core\Controller\Controller;
use Core\View\View;
use Core\View\ElementView;
use Core\Entity\Entity\Argument;

use Apps\Mooc\Entity\MoocMoocUser;

/*
 * 
 */

class MemberController extends Controller {

    /**
     * Constructeur
     */
    function _construct($core = "") {
        $this->Core = $core;
    }

    /***
     * list the Moooc Of the User 
     */
    function Index() {
        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);
        
        $mooc = new MoocMoocUser($this->Core);
        $mooc->Select("MoocMooc.Name", "MoocName");
        $mooc->Select("Concat(SUBSTRING(MoocMooc.Description, 1, 100),'...')" , "MoocDescription");
        $mooc->Select("MoocMooc.Code", "MoocCode");
       
        $mooc->Join("MoocMooc", "MoocMooc" , "Left",  "MoocMooc.Id = MoocMoocUser.MoocId" );
        
        $view->AddElement(new ElementView("MoocUser" , $mooc->Find("MoocMoocUser.UserId=" . $this->Core->User->IdEntite)));
        
        return $view->Render();
    }
}

var ContainerForm = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
ContainerForm.Init = function(){
    Event.AddById("btnSave", "click", ContainerForm.SaveElement);
};

/***
* Obtient l'id de l'itin�raire courant 
*/
ContainerForm.GetId = function(){
    return Form.GetId("ContainerFormForm");
};

/*
* Sauvegare l'itineraire
*/
ContainerForm.SaveElement = function(e){
   
    if(Form.IsValid("ContainerFormForm"))
    {

    var data = "Class=Cms&Methode=SaveContainer&App=Cms";
        data +=  Form.Serialize("ContainerFormForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.statut == "Error"){
                Form.RenderError("ContainerFormForm", data.message);
            } else{

                Form.SetId("ContainerFormForm", data.data.Id);
                Dialog.Close();
                Cms.RefreshTool();
            }
        });
    }
};


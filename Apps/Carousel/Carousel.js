var Carousel = function () {};

/*
 * Add Event when the App loaded
 */
Carousel.Load = function (parameter)
{
    this.LoadEvent();

    Carousel.InitTabCarousel();
};

/*
 * Add event
 */
Carousel.LoadEvent = function ()
{
    Dashboard.AddEventAppMenu(Carousel.Execute, "", "Carousel");
    Dashboard.AddEventWindowsTool("Carousel");
};

/***
 * Start the Event for Member 
 */
Carousel.LoadMember = function () {
};

/*
 * Execute a function
 */
Carousel.Execute = function (e)
{
    //Call the function
    Dashboard.Execute(this, e, "Carousel");
    return false;
};

/***
 * Init the app front page
 */
Carousel.Init = function (app, method) {
    if (app == "Carousel") {
        switch (method) {
            default :
                break;
        }
    }
};

/***
 * Init the Admin Widget
 */
Carousel.InitAdminWidget = function () {

};


/*
 * Init the Event for tabCarousel 
 */
Carousel.InitTabCarousel = function(){
    //Catégory
   Event.AddById("btnAddCarousel", "click", () => {
       Carousel.ShowAddCarousel("");
   });
   EntityGrid.Initialise('gdCarousel');
   
};

/**
* Edit Carousel
* @param {type} CarouselId
* @returns {undefined}
*/
Carousel.EditCarousel = function (CarouselId) {
   Carousel.ShowAddCarousel(CarouselId);
};

/**
* Dialogue add a carousel
*/
Carousel.ShowAddCarousel = function (CarouselId) {

   Dialog.open('', {"title": Dashboard.GetCode("Carousel.AddCarousel"),
       "app": "Carousel",
       "class": "DialogAdminCarousel",
       "method": "AddCarousel",
       "type": "full",
       "params": CarouselId
   });
};

//Reload the Carousel grid
Carousel.ReloadCarousel = function(html){
   let CarouselContainer = Dom.GetById("gdCarousel").parentNode;
   CarouselContainer.innerHTML = html;
   Carousel.InitTabCarousel();
};

//Delete a Carousel
Carousel.DeleteCarousel = function(CarouselId){
     Animation.Confirm(Language.GetCode("Carousel.ConfirmRemoveCarousel"), () => {

       let data = "Class=Carousel&Methode=DeleteCarousel&App=Carousel";
       data += "&CarouselId=" + CarouselId;

       Request.Post("Ajax.php", data).then(data => {

           data = JSON.parse(data);

           Carousel.ReloadCarousel(data.data);
       });
   });
};

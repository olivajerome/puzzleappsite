<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Admin\Module\Log;

use Core\Controller\Controller;
use Core\Core\Core;
use Core\Control\TabStrip\TabStrip;

use Core\Core\Request;

/**
 * Description of FrontBlock
 *
 * @author jerome
 */
class LogController extends Controller {

    /**
     * Constructeur
     */
    function __construct($core = "") {
        $this->Core = Core::getInstance();
    }
  
    function GetLog(){
        
        $tabLog = new TabStrip("tbLog");
        $tabLog->AddTab("Debugger", self::GetDebugger());
        $tabLog->AddTab("Db" , self::GetLogType(DB, INFO));
        $tabLog->AddTab("Ajax", self::GetLogType(AJAX, INFO));
        $tabLog->AddTab("Runner" , self::GetLogType(RUNNER, INFO));
        
        return $tabLog->Show();
    }

    /***
     * Obtient l'écran de débuggage
     */
    function GetDebugger(){
        
        $view = "<div class='debuggerTool'><i class='fa fa-trash' id='btnCleanDebugger' ></i></div>";
        $view .= Request::GetSession("Debugger");

        return $view;

    }
    
    function GetLogType($type, $level){
        $Name = $type . date('dmY');
        $Directory = "../Log/" . $type . "/" . $level ."-";
        $File = $Directory . $Name . ".JLog";
        $content =  file_get_contents($File);
        $content = str_replace("\n", "<br/>", $content);
        return $content;
    }
}


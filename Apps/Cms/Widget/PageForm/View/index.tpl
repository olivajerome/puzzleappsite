

{{form->Open()}}
{{form->Error()}}
{{form->Success()}}

{{form->Render(Id)}}

    <div>
        <label>{{GetCode(Cms.Name)}}</label> 
        {{form->Render(Name)}}
    </div>

    <div>
        <label>{{GetCode(Cms.Title)}}</label> 
        {{form->Render(Title)}}
    </div>

    <div>
        <label>{{GetCode(Cms.Description)}}</label> 
        {{form->Render(Description)}}
    </div>


    <div class='center marginTop' >   
        {{form->Render(btnSave)}}
    </div>  

{{form->Close()}}
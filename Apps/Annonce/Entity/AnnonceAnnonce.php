<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Annonce\Entity;

use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class AnnonceAnnonce extends Entity {

     //Entité liée
    protected $Category;
    protected $User;
    
    //Constructeur
    function __construct($core) {
        //Version
        $this->Version = "2.0.0.0";

        //Nom de la table 
        $this->Core = $core;
        $this->TableName = "AnnonceAnnonce";
        $this->Alias = "AnnonceAnnonce";

        $this->Title = new Property("Title", "Title", TEXTBOX, false, $this->Alias);
        $this->Code = new Property("Code", "Code", TEXTBOX, false, $this->Alias);
        $this->Description = new Property("Description", "Description", TEXTAREA, false, $this->Alias);
        $this->CategoryId = new Property("CategoryId", "CategoryId", NUMERICBOX, false, $this->Alias);
        $this->UserId = new Property("UserId", "UserId", NUMERICBOX, false, $this->Alias);
        $this->Status = new Property("Status", "Status", NUMERICBOX, false, $this->Alias);

        $this->Category = new EntityProperty("Apps\Annonce\Entity\AnnonceCategory", "CategoryId"); 
        $this->User = new EntityProperty("Core\Entity\User\User", "UserId"); 
     
        //Image directory    
        $this->DirectoryImage = "Data/Apps/Annonce/Annonce/";
        
        //Partage entre application 
        $this->AddSharedProperty();

        //Creation de l entité 
        $this->Create();
    }
    
    /***
     * GetThumbImage 
     */
    function GetThumbImage(){
        
        if(file_exists($this->DirectoryImage . $this->IdEntite ."/thumb.png")){
            $view = "<img src='".$this->DirectoryImage . $this->IdEntite ."/thumb.png' />";
             return  $view; 
        } 
        
        return "";
    }
    
    /***
     * Get Image Path
     */
    function GetImagePath(){
        
        if(file_exists($this->DirectoryImage . $this->IdEntite ."/full.png")){
             return "/" . $this->DirectoryImage . $this->IdEntite ."/full.png" ;
        } 
        
        return $this->Core->GetPath("/images/nophoto.png");
    }
    
    function GetImagesPath(){
        return implode(";" , $this->GetImages());
    }

    /***
     * Get className for Render images
     */
    function GetClassRenderImage(){

        if(count($this->GetImages()) > 0){
            return "col-md-4";
        } else {
            return "";
        }
    }
    
    
    function GetNumberImage(){
       return count($this->GetImages());
    }
}

?>
<?php 

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Blog\Entity;

use Core\Control\Image\Image;
use Core\Entity\Entity\Entity;
use Core\Entity\Entity\EntityProperty;
use Core\Entity\Entity\Property;
use Core\Utility\Format\Format;

use Apps\Blog\Helper\CommentHelper;


class BlogArticle extends Entity  
{
    //Entite lié
    protected $Category;

    //Constructeur
    function __construct($core)
    {
        //Version
        $this->Version ="2.0.0.0"; 

        //Nom de la table 
        $this->Core=$core; 
        $this->TableName="BlogArticle"; 
        $this->Alias = "BlogArticle"; 

        $this->BlogId = new Property("BlogId", "BlogId", NUMERICBOX,  true, $this->Alias); 
        $this->UserId = new Property("UserId", "UserId", NUMERICBOX,  true, $this->Alias); 
        $this->Actif = new Property("Actif", "Actif", TEXTBOX,  false, $this->Alias); 
        $this->Name = new Property("Name", "Name", TEXTBOX,  true, $this->Alias); 
        $this->Code = new Property("Code", "Code", TEXTBOX,  true, $this->Alias); 
        $this->KeyWork = new Property("KeyWork", "KeyWork", TEXTAREA,  false, $this->Alias); 
        $this->Description = new Property("Description", "Description", TEXTAREA,  false, $this->Alias); 
        $this->Content = new Property("Content", "Content", TEXTAREA,  false, $this->Alias); 
        $this->DateCreated = new Property("DateCreated", "DateCreated", DATEBOX,  false, $this->Alias); 

        $this->CategoryId = new Property("CategoryId", "CategoryId", NUMERICBOX,  true, $this->Alias); 
        $this->Category = new EntityProperty("Apps\Blog\Entity\BlogCategory", "CategoryId"); 

        //Repertoire de stockage des images    
        $this->DirectoryImage = "Data/Apps/Blog/Article/";
        
        //Creation de l entité 
        $this->Create(); 
    }

    /*
    * Get the Name for url
    */
   function GetUrlCode()
   {
       return Format::ReplaceForUrl($this->Code->Value, false);
   }

    /*
     * Get the Image og the article
     */
    function GetImage()
    {
        $fileName = "Data/Apps/Blog/Article/".$this->IdEntite. "/thumb.png";

        if(file_exists($fileName))
        {
            $image = new Image($this->Core->GetPath("/".$fileName));
            $image->Title = $this->Name->Value;
           
        }
        else
        {
            $image = new Image($this->Core->GetPath("/images/nophoto.png"));
        }

        $image->Style ='width:100px';
         
        return $image->Show();
    }
    
    /*
     * Get the Image og the article
     */
    function GetImagePath()
    {
        $fileName = "/Data/Apps/Blog/Article/".$this->IdEntite. "/thumb.png";

        return $this->Core->GetPath($fileName);
    }
    
    /*
     * Get the Image og the article
     */
    function GetImageFullPath()
    {
        $fileName = "/Data/Apps/Blog/Article/".$this->IdEntite. "/full.png";

        return $this->Core->GetPath($fileName);
    }
    
    
    /*
     * Get A small Description
     */
    function GetSmallDescription()
    {
        return Format::Tronquer($this->Description->Value, 250);
    }
    
    /***
     * Get The Comment
     */
    function GetComments() {
        
      $commentPlugin = \Apps\EeApp\Helper\AppHelper::GetPluginType($this->Core, "Blog", "Comment");

        if($commentPlugin != null ){

              $comments = $commentPlugin->GetByEntity($this->Core, "BlogArticle", $this->IdEntite);

               if (count($comments) > 0) {

              $view = "<ul style='text-align:left;'>";

              foreach ($comments as $comment) {
                  $view .= "<li><b>" . $comment->DateCreated->Value . "</b> .  " . $comment->UserName->Value  . "<span>" .$comment->Message->Value . "</span>";
                  $view .= "<div class='tool'><i id='$comment->IdEntite' class='publishComment fa " .( $comment->Status->Value == "1" ? "fa-eye" : "fa-eye-slash published"  ) . "'></i>";
                  $view .= "<i id='$comment->IdEntite' class='fa fa-trash removeComment'></i>";
                  $view .= "</div>";   
                  $view .= "</span></li>";
              }

              $view .= "</ul>";
          }

          return $view;
      }
    }
    
    /***
     * Dernier commentaire
     */
    function GetLastComment(){
        $commentPlugin = \Apps\EeApp\Helper\AppHelper::GetPluginType($this->Core, "Blog", "Comment");

        if($commentPlugin != null ){

            $comments = $commentPlugin->GetByEntity($this->Core, "BlogArticle", $this->IdEntite);
            
            if(count($comments) > 0){
                $comment = $comments[0];
                return "<b>" . $comment->DateCreated->Value. "- " . $comment->UserName->Value . "</b>";
            }
        }
    }
}
?>
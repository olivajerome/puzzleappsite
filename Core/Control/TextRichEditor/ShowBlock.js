class ShowBlock extends BaseTool {

    constructor(editor){
        super(editor);
        this.activated = false;
        this.name = "ShowBlock";
    }

    getIcone() {
        return "<i class='fa fa-eye'></i>";
    }

    getName() {
        return Language.GetCode("Base.TextRichEditorShowBlock");
    }
    execute(e) {
        
        let instance = this;
        let inputArea = instance.editor.inputArea;
        let elements = inputArea.querySelectorAll("div,h1,h2,h3, p, img");

        if(instance.activated){
            instance.activated = false;

            let infoDiv = document.querySelectorAll(".detailBlock.infoDiv");

            for(let i =0; i < infoDiv.length; i++){
                infoDiv[i].parentNode.removeChild(infoDiv[i]);
            }
        } else {
            instance.activated = true;
        }   

        for(let i =0 ; i < elements.length; i++){

            if(instance.activated == false){
                    elements[i].classList.remove("detailBlock");
                } else { 
                    elements[i].classList.add("detailBlock");
                    let infoDiv = document.createElement("div");
                    infoDiv.innerHTML = elements[i].tagName;
                    infoDiv.className = "detailBlock infoDiv";
                    elements[i].prepend(infoDiv);
                }
          }

      super.execute();
    }
}
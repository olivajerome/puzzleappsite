
<div class='col-md-2'>
    <ul>
        <li>
            <i class='fa fa-gears' id='editForm' style='cursor:pointer'>{{GetCode(Forms.Parameters)}}</i>
        </li>    
        <li>    
        <i class='fa fa-question' id='editQuestion' style='cursor:pointer' >{{GetCode(Forms.Questions)}}</i>
        </li>
    </ul>
</div>

<div class='col-md-10'>

    <div id='formTool'>
        {{form->Open()}}
        {{form->Error()}}
        {{form->Success()}}

        {{form->Render(Id)}}
    <div>
        <label>{{GetCode(Forms.Libelle)}}</label> 
        {{form->Render(Libelle)}}
    </div>

    <div>
        <label>{{GetCode(Forms.Commentaire)}}</label> 
        {{form->Render(Commentaire)}}
    </div>
   
    <div class='center marginTop' >   
        {{form->Render(btnSaveForm)}}
    </div>  

   {{form->Close()}}
    </div>
    <div id ='questionTool' style='display:none'>
  
  <div class='col-md-12'>
        <div class='center'>
            <label>{{GetCode(Forms.AddQuestion)}}</label><br/>
            {{form->Render(questionType)}}
        </div>
        <div class='center marginTop' >
            <button id='btnAddQuestion' class='btn btn-primary'>{{GetCode(Forms.Add)}}</button>   
        </div>
   </div> 
    <div class='col-md-12' id='questionContainer'>

        {{foreach Questions}}
            <div class='formQuestion' id="{{element->IdEntite}}">
                {{\Apps\Forms\Decorator\FormDecorator->GetAdminQuestion(element)}}
            </div>
        {{/foreach Questions}}
    </div>

    </div>
 
</div>



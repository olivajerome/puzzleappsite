<?php

namespace Apps\RoadMap\Widget\GetShared;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;

use Apps\RoadMap\Module\Home\HomeController;


class GetShared {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "") {
        $this->Core = $core;
    }
    
    /**
     * 
     * @param type $params
     */
    function Index($params){
    
        $homeController = new HomeController($this->Core);
        return $homeController->Index($params);
    }
}
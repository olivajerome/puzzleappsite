<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\NewsLetter\Module\DialogAdminNewsLetter;

use Core\Control\Button\Button;
use Core\Controller\Controller;

use Apps\NewsLetter\Widget\NewsLetterEmailForm\NewsLetterEmailForm;
use Apps\NewsLetter\Entity\NewsLetterEmail;

use Apps\NewsLetter\Widget\NewsLetterListEmailForm\NewsLetterListEmailForm;
use Apps\NewsLetter\Entity\NewsLetterListeEmail;

use Apps\NewsLetter\Widget\NewsLetterCampagneForm\NewsLetterCampagneForm;
use Apps\NewsLetter\Entity\NewsLetterEmailCampagne;


use Apps\NewsLetter\Widget\SendCampagneForm\SendCampagneForm;

class DialogAdminNewsLetterController extends Controller{
    
    /***
     * Add A Email
     */
    function AddEmailBase($emailId){

        $email = new NewsLetterEmail($this->Core);
        if($emailId != ""){
            $email->GetById($emailId);
        }

        $emailForm = new NewsLetterEmailForm($this->Core, $email);
        $emailForm->Load($email);

        return $emailForm->Render();
    }
    
    /***
     * Add A list Email
     */
    function AddListEmail($lstEmailId){

        $listEmail = new NewsLetterListeEmail($this->Core);
        if($lstEmailId != ""){
            $listEmail->GetById($lstEmailId);
        }

        $listEmailForm = new NewsLetterListEmailForm($this->Core, $listEmail);
        $listEmailForm->Load($listEmail);

        return $listEmailForm->Render();
    }
    
    /***
     * Add A Campagne
     */
    function AddCampagne($campagneId){

        $campagne = new NewsLetterEmailCampagne($this->Core);
        if($campagneId != ""){
            $campagne->GetById($campagneId);
        }

        $campagneForm = new NewsLetterCampagneForm($this->Core, $campagne);
        $campagneForm->Load($campagne);

        return $campagneForm->Render();
    }

    /***
     * 
     */
    function SendCampagne($campagneId){

        $sendCampagneForm = new SendCampagneForm($this->Core, $campagneId);
      
        return $sendCampagneForm->Render();
    }
    
}
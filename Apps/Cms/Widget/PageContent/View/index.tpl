{{foreach Container}}
    <div class='{{element->CssClass->Value}}' 
         style="{{element->Style->Value}}"  >
        {{element->RenderFrontBlocks()}}
    </div>
{{/foreach Container}}

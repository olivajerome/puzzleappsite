var Seo = function() {};

	/*
	* Chargement de l'application
	*/
	Seo.Load = function(parameter)
	{
		this.LoadEvent();
	};

	/*
	* Chargement des �venements
	*/
	Seo.LoadEvent = function()
	{
		Dashboard.AddEventAppMenu(Seo.Execute, "", "Seo");
		Dashboard.AddEventWindowsTool("Seo");
	};

   /*
	* Execute une fonction
	*/
	Seo.Execute = function(e)
	{
		//Appel de la fonction
		Dashboard.Execute(this, e, "Seo");
		return false;
	};

	/*
	*	Affichage de commentaire
	*/
	Seo.Comment = function()
	{
		Dashboard.Comment("Seo", "1");
	};

	/*
	*	Affichage de a propos
	*/
	Seo.About = function()
	{
		Dashboard.About("Seo");
	};

	/*
	*	Affichage de l'aide
	*/
	Seo.Help = function()
	{
		Dashboard.OpenBrowser("Seo","{$BaseUrl}/Help-App-Seo.html");
	};

   /*
	*	Affichage de report de bug
	*/
	Seo.ReportBug = function()
	{
		Dashboard.ReportBug("Seo");
	};

	/*
	* Fermeture
	*/
	Seo.Quit = function()
	{
		Dashboard.CloseApp("","Seo");
	};
        
        
        /***
 * Initialisation du widget Admin
 * @returns {undefined}
 */
Seo.InitAdminWidget = function () {

    View.Bind("pagesSeoContainer");

    Event.AddById("btnAddSeoPageAdminWidget", "click", function () {
        Seo.ShowAddPage("");
    });


    Event.AddByClass("editSeoPage", "click", function (e) {
        var container = e.srcElement.parentNode.parentNode;
        var hidden = container.getElementsByTagName("input");
        Seo.ShowAddPage(hidden[0].value);
    });

    Event.AddByClass("deleteSeoPage", "click", function (e) {

        Animation.Confirm(Dashboard.GetCode("Seo.DeletePage"), function () {

            var container = e.srcElement.parentNode.parentNode;
            var hidden = container.getElementsByTagName("input");

            var data = "App=Seo&Methode=DeletePage";
            data += "&PageId=" + hidden[0].value;

            Request.Post("Ajax.php", data).then(data => {
                data = JSON.parse(data);

                Form.SetDataSource("pages", data.pages);
                Seo.InitAdminWidget();
            });
        });

    });

};

Seo.Init = function(){
    
};

/***
 * Dialog d'ajout de page
 * @returns
 */
Seo.ShowAddPage = function (id) {

    Dialog.open('', {"title": Dashboard.GetCode("Seo.AddPage"),
        "app": "Seo",
        "class": "DialogSeo",
        "method": "ShowAddPage",
        "params": id,
    });

};
<?php 
/*
* PuzzleApp
* Webemyos
* Jérôme Oliva
* GNU Licence
*/

namespace Apps\Folders\Entity;
use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class FolderFolder extends Entity  
{
	//Constructeur
	function __construct($core)
	{
		//Version
		$this->Version ="2.0.0.0"; 

		//Nom de la table 
		$this->Core=$core; 
		$this->TableName="FolderFolder"; 
		$this->Alias = "FolderFolder"; 

		$this->UserId = new Property("UserId", "UserId", NUMERICBOX,  false, $this->Alias); 
		$this->Name = new Property("Name", "Name", TEXTBOX,  false, $this->Alias); 
		$this->Code = new Property("Code", "Code", TEXTBOX,  false, $this->Alias); 
		$this->Description = new Property("Description", "Description", TEXTAREA,  false, $this->Alias); 
		$this->Url = new Property("Url", "Url", TEXTAREA,  false, $this->Alias); 
		$this->Status = new Property("Status", "Status", NUMERICBOX,  false, $this->Alias); 
        $this->ParentId = new Property("ParentId", "ParentId", NUMERICBOX,  false, $this->Alias); 
		
		//Partage entre application 
		$this->AddSharedProperty();

		//Creation de l entité 
		$this->Create(); 
	}
}
?>
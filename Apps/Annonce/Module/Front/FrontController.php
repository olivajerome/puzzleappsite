<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Annonce\Module\Front;

use Core\Control\Button\Button;
use Core\Controller\Controller;
use Core\View\View;
use Core\View\ElementView;
use Core\Utility\Format\Format;
use Apps\Annonce\Entity\AnnonceCategory;
use Apps\Annonce\Entity\AnnonceAnnonce;
use Apps\Annonce\Entity\AnnonceResponse;

use Apps\Annonce\Widget\AnnonceAnnonceForm\AnnonceAnnonceForm;
use Apps\Annonce\Helper\AnnonceHelper;
use Apps\Annonce\Helper\NotifyHelper;

use Apps\Annonce\Widget\AnnonceResponseForm\AnnonceResponseForm;

/*
 * 
 */
 class FrontController extends Controller
 {
    /**
     * Constructeur
     */
    function _construct($core="")
    {
          $this->Core = $core;
    }

    /**
     * Create
     */
    function Create()
    {
    }

    /**
     * Init
     */
    function Init()
    {
    }

    /**
     * Render
     */
    function Show($all=true)
    {
         return $this->Index();
    }
    
    /*
    * Get the home page
    */
   function Index()
   {
       $view = new View(__DIR__."/View/index.tpl", $this->Core);
       $category = new AnnonceCategory($this->Core);
       
       $view->AddElement(new ElementView("Category", $category->GetAll()));
       
       return $view->Render();
   }
   
   /***
    * Show Category and this annonce
    */
   function Category($categoryCode){
       
       $view = new View(__DIR__."/View/category.tpl", $this->Core);
       $category = new AnnonceCategory($this->Core);
       $category = $category->GetByCode($categoryCode);
       
       $view->AddElement(new ElementView("Category", $category));
       
       $view->AddElement(new ElementView("ShowAddAnnonce", $this->Core->IsConnected()));
    
       $view->AddElement(new ElementView("Annonces", AnnonceHelper::GetPublishedByCategory($this->Core, $category->IdEntite)));
     
       return  $view->Render();
   }
   
   /***
    * Show annonce and this respones
    */
   function Annonce($annoceCode){
    
       $view = new View(__DIR__."/View/annonce.tpl", $this->Core);
       $annonce = new AnnonceAnnonce($this->Core);
       $annonce = $annonce->GetByCode($annoceCode);
      
       $view->AddElement(new ElementView("Annonce", $annonce));
     
       $isConnected = $this->Core->IsConnected();
       $view->AddElement(new ElementView("ShowAddReponse", $isConnected));
    
       $reponseForm = new AnnonceResponseForm($this->Core);
       $view->AddElement(new ElementView("ResponseForm", $isConnected ? $reponseForm->Render() : ""));
       
       $pluginVote = \Apps\EeApp\Helper\AppHelper::GetPluginType($this->Core, "Annonce", "Vote");
       $view->AddElement(new ElementView("votePlugin", $pluginVote ? $pluginVote->Render("AnnonceAnnonce", $annonce->IdEntite) : ""));
       $view->AddElement(new ElementView("registerPlugin", $pluginVote ? $pluginVote->GetRegisterPlugin() : ""));
       
       return $view->Render();
   }
   
   /***
    * Save annonce
    */
   function SaveAnnonce($data){
       $annonceAnnonceForm = new AnnonceAnnonceForm($this->Core, $data);

        if ($annonceAnnonceForm->Validate($data)) {

            $annonce = new AnnonceAnnonce($this->Core);

            $annonce->Status->Value = 1;
            $annonce->UserId->Value = $this->Core->User->IdEntite;
            $annonce->Code->Value = Format::ReplaceForUrl($data["Title"]);
            $annonceAnnonceForm->Populate($annonce);
            $annonce->Save();
          
           $annonce->IdEntite = $this->Core->Db->GetInsertedId();;
            
            $annonce->SaveImage($data["dvUpload-UploadfileToUpload"], true);

            return true;
        } else {
            return $annonceAnnonceForm->errors;
        }
   }
   
   /***
    * Add Reponse to a annonce
    */
   function AddResponse($data){
       
        $annonceResponseForm = new AnnonceResponseForm($this->Core, $data);

        if ($annonceResponseForm->Validate($data)) {

            $response = new AnnonceResponse($this->Core);

            $response->Status->Value = 1;
            $response->UserId->Value = $this->Core->User->IdEntite;
            $annonceResponseForm->Populate($response);
            $response->Save();
    
            //Send notify to the User
            NotifyHelper::NewResponse($this->Core, $data["AnnonceId"]);

            return true;
        } else {
            return $annonceAnnonceForm->errors;
        }
   }
          /*action*/
 }?>
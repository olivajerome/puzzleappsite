var Stat = function () {};

/*
 * Chargement de l'application
 */
Stat.Load = function (parameter)
{
    Dashboard.IncludeJs("","",undefined, "/js/chart.js"); 
    
    this.LoadEvent();

    setTimeout(Stat.LoadChartUser, 2000);
};

/*
 * Chargement des �venements
 */
Stat.LoadEvent = function ()
{
    Dashboard.AddEventAppMenu(Stat.Execute, "", "Stat");
    Dashboard.AddEventWindowsTool("Stat");

    Event.AddById("lstOrder", "change", Stat.OrderUser);
};

/*
 * Execute une fonction
 */
Stat.Execute = function (e)
{
    //Appel de la fonction
    Dashboard.Execute(this, e, "Stat");
    return false;
};

/*
 *	Affichage de commentaire
 */
Stat.Comment = function ()
{
    Dashboard.Comment("Stat", "1");
};

/*
 *	Affichage de a propos
 */
Stat.About = function ()
{
    Dashboard.About("Stat");
};

/*
 *	Affichage de l'aide
 */
Stat.Help = function ()
{
    Dashboard.OpenBrowser("Stat", "{$BaseUrl}/Help-App-Stat.html");
};

/*
 *	Affichage de report de bug
 */
Stat.ReportBug = function ()
{
    Dashboard.ReportBug("Stat");
};

/*
 * Fermeture
 */
Stat.Quit = function ()
{
    Dashboard.CloseApp("", "Stat");
};

/*
 ** Trie le tableau des utilisateurs
 */
Stat.OrderUser = function (e) {
    console.log(e.srcElement.value);

    var table = document.querySelector("#tab_0Stat table");
    var container = table.parentNode;
    container.removeChild(table);


    var data = "Class=Stat&Methode=OrderUser&App=Stat";
    data += "&order=" + e.srcElement.value;

    Request.Post("Ajax.php", data).then(data => {
        container.innerHTML += data;

        Stat.LoadEvent();
    });
};

/***
 * Charge un chart de test
 * @returns {undefined}
 */
Stat.LoadChartUser = function () {

    //TYPOLOGIE    
    const typeUser = document.getElementById('typeUser');

   

    const dateCreateCtx = document.getElementById('dateCreateCtx');

    data = JSON.parse(document.getElementById("dateCreate").value);
    dataConnect = JSON.parse(document.getElementById("dateConnect").value);

    labels = Array();
    number = Array();
    numberConnexion = Array();

    for (nbProduct in data) {
        labels.push(nbProduct);
        number.push(data[nbProduct]);
        numberConnexion.push(dataConnect[nbProduct]);
    }

    new Chart(dateCreateCtx, {
        type: 'bar',
        data: {
            labels: labels,
            datasets: [{
                    label: '# inscription',
                    data: number,
                    borderWidth: 1
                }, {
                    label: '# connexion',
                    data: numberConnexion,
                    borderWidth: 1
                }

            ],
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
};

/***
 * Charge un chart de test
 * @returns {undefined}
 */
Stat.LoadChartProduct = function () {

    //Produit par marques    
    const ctxMarques = document.getElementById('ctxMarques');
    data = JSON.parse(document.getElementById("productByMarque").value);

    let labels = Array();
    let number = Array();

    for (nbProduct in data) {
        labels.push(nbProduct);
        number.push(data[nbProduct]);
    }

    new Chart(ctxMarques, {
        type: 'doughnut',
        data: {
            labels: labels,
            datasets: [{
                    label: '# Marques',
                    data: number,
                    borderWidth: 1
                }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
    
    //Produit publié par mois
    const ctxProductMonth = document.getElementById('ctxProductMonth');
    data = JSON.parse(document.getElementById("productByMonth").value);

    labels = Array();
    number = Array();

    for (nbProduct in data) {
        labels.push(nbProduct);
        number.push(data[nbProduct]);
    }

    new Chart(ctxProductMonth, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                    label: '# Produit par mois',
                    data: number,
                    borderWidth: 1
                }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
    
    //Fiabilité    
    const fiabilitesCtx = document.getElementById('fiabilitesCtx');
    data = JSON.parse(document.getElementById("fiabilites").value);
  
    labels = Array();
    number = Array();

    for (nbProduct in data) {
        labels.push(nbProduct);
        number.push(data[nbProduct]);
    }

    new Chart(fiabilitesCtx, {
        type: 'doughnut',
        data: {
            labels: labels,
            datasets: [{
                    label: '# Marques',
                    data: number,
                    borderWidth: 1
                }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
};


/***
 * Charge un chart des proposition
 * @returns {undefined}
 */
Stat.LoadChartProposition = function () {

    //Produit par marques    
    const ctxStatus = document.getElementById('ctxStatus');
    data = JSON.parse(document.getElementById("status").value);

    let labels = Array();
    let number = Array();

    for (nbProduct in data) {
        labels.push(nbProduct);
        number.push(data[nbProduct]);
    }

    new Chart(ctxStatus, {
        type: 'polarArea',
        data: {
            labels: labels,
            datasets: [{
                    label: '# Status',
                    data: number,
                    borderWidth: 1
                }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
    
    //Produit publié par mois
    const ctxSended = document.getElementById('ctxSended');
    data = JSON.parse(document.getElementById("propositionSendedByMonth").value);

    labels = Array();
    number = Array();

    for (nbProduct in data) {
        labels.push(nbProduct);
        number.push(data[nbProduct]);
    }

    new Chart(ctxSended, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                    label: '# Proposition envoyées par mois',
                    data: number,
                    borderWidth: 1
                }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
    
    //Fiabilité    
    const ctxEnded = document.getElementById('ctxEnded');
    data = JSON.parse(document.getElementById("propositionEndedByMonth").value);
  
    labels = Array();
    number = Array();

    for (nbProduct in data) {
        labels.push(nbProduct);
        number.push(data[nbProduct]);
    }

    new Chart(ctxEnded, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                    label: '# Propositions terminée',
                    data: number,
                    borderWidth: 1
                }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
};


/***
 * Charge un chart de test
 * @returns {undefined}
 */
Stat.LoadChartCommand = function () {

    //Produit par marques    
    const ctxStatus = document.getElementById('ctxCommandStatus');
    data = JSON.parse(document.getElementById("commandeStatus").value);

    let labels = Array();
    let number = Array();

    for (nbProduct in data) {
        labels.push(nbProduct);
        number.push(data[nbProduct]);
    }

    new Chart(ctxStatus, {
        type: 'polarArea',
        data: {
            labels: labels,
            datasets: [{
                    label: '# Status',
                    data: number,
                    borderWidth: 1
                }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
    
    //Produit publié par mois
    const ctxSended = document.getElementById('ctxCommandSended');
    data = JSON.parse(document.getElementById("commandSendedByMonth").value);

    labels = Array();
    number = Array();

    for (nbProduct in data) {
        labels.push(nbProduct);
        number.push(data[nbProduct]);
    }

    new Chart(ctxSended, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                    label: '# Commandes envoyées par mois',
                    data: number,
                    borderWidth: 1
                }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
    
    //Fiabilité    
    const ctxEnded = document.getElementById('ctxCommandEnded');
    data = JSON.parse(document.getElementById("commandEndedByMonth").value);
  
    labels = Array();
    number = Array();

    for (nbProduct in data) {
        labels.push(nbProduct);
        number.push(data[nbProduct]);
    }

    new Chart(ctxEnded, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                    label: '# Commandes terminée',
                    data: number,
                    borderWidth: 1
                }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
};

Stat.Init = function(){
    
};
      
      
/**
 * Initialise le widget Admin
 */
Stat.InitAdminWidget = function () {
 };



<section class='container'>
    <div class='row'>
        <input type='hidden' id='hdComunityId' value='{{Comunity->IdEntite}}'/> 

        <div class='col-md-3'>
            <div class='block'>
                <div class='projet'>
                    <h3>{{GetCode(Community.AboutThisGroup)}}</h3>
                    <p style="font-size: 12px;">
                        {{Comunity->Description->Value}}
                    </p>
                </div>
            </div>

        </div>

        <div id='wallProjet' class='col-md-9'>        
            <div class='centerBlock'>    
                <div class='col-md-12' id='dashboardDiscus'>

                    <div id='dashboardDiscusMessage'>

                        {{if isConnected == true}}

                        <div class='messageBlock row'>
                            <div class='col-md-2'>
                                <div title="Jean391" class="avatarmini-img avatarmini" style="cursor: pointer;">
                                    <img src="{{userImage}}" alt="images">
                                </div>
                            </div>
                            <div class='col-md-10'>
                                <input id="tbMessage" type="text" class="form-control" placeholder="Publier quelque chose">


                            </div>
                            <div class='col-md-1'>

                            </div>    
                            <div class='col-md-12 center imagesButton'>
                                {{uploadComunity}} 
                            </div>
                        </div>   

                        {{/if isConnected == true}}



                        <div class='messages' id='messages' >  
                            {{Messages}}
                        </div>
                    </div> 
                </div>

            </div

        </div>
    </div>
</div>
</div>
</section>

<script>
    Comunity.Init();
</script>

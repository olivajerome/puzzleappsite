<h3>TextBox</h3>
<p>This control allows you to enter text.</p>

[[GetWidget(PuzzleApp,Control,TextBox)]]<div><br></div><div><br></div><div><br></div><div><br></div>


<br/><h2>In php Code</h2>
<code>

$tbName = new TextBox("tbName");
return $tbName->Render(); 

</code>
<br/><h2>In view</h2>
<codehtml>
    {{GetControl(TextBox,tbName,{PlaceHolder=Enter you  Name,Id=tbName,)})}}
</codehtml>

<br/><h2>In form Widget</h2>
<code>
  $this->form->Add(array("Type" => "TextBox",
            "Id" => "Name",
            "Validators" => ["Required"]
        ));
</code>

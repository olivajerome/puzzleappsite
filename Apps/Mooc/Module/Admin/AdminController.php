<?php
/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Mooc\Module\Admin;


use Core\Block\AjaxFormBlock\AjaxFormBlock;
use Core\Controller\AdministratorController;
use Core\View\View;
use Core\View\ElementView;
use Core\Control\Icone\EditIcone;
use Core\Control\Libelle\Libelle;
use Core\Control\TabStrip\TabStrip;
use Core\Control\EntityGrid\EntityGrid;
use Core\Control\EntityGrid\EntityColumn;
use Core\Control\EntityGrid\EntityIconColumn;
use Core\Control\EntityGrid\EntityFunctionColumn;
use Core\Entity\Entity\Argument;
use Core\Control\Button\Button;

use Apps\Mooc\Helper\CategoryHelper;
use Apps\Mooc\Helper\MoocHelper;
use Apps\Mooc\Entity\MoocCategory;
use Apps\Mooc\Entity\MoocLesson;
use Apps\Mooc\Widget\MoocLessonForm\MoocLessonForm;

 class AdminController extends AdministratorController
 {
    /**
     * Creation
     */
    function Create()
    {
    }

    /**
     * Initialisation
     */
    function Init()
    {
    }

    /**
     * Affichage du module
     */
    function Show($all=true)
    {
         return $this->Index();
    }

    /**
     * Affichage du module
     */
    function Index($all=true)
    {
      $view = new View(__DIR__."/View/index.tpl", $this->Core);
      
      $tabMooc = new TabStrip("tabMooc", "Mooc");
      $tabMooc->AddTab($this->Core->GetCode("Mooc.Category"), $this->GetTabCategory());
      $tabMooc->AddTab($this->Core->GetCode("Mooc.Mooc"), $this->GetTabMooc());

      $view->AddElement(new ElementView("tabMooc", $tabMooc->Render()));

      return $view->Render();
    }

    /***
    * Categorie dof the Forum
    */
    function GetTabCategory(){
     
      $gdCategory = new EntityGrid("gdCategory", $this->Core);
      $gdCategory->Entity = "Apps\Mooc\Entity\MoocCategory";
      $gdCategory->App = "Mooc";
      $gdCategory->Action = "GetTabCategory";

      $btnAdd = new Button(BUTTON, "btnAddCategory");
      $btnAdd->Value = $this->Core->GetCode("Mooc.AddCategory");

      $gdCategory->AddButton($btnAdd);
      $gdCategory->AddColumn(new EntityColumn("Name", "Name"));
      
      $gdCategory->AddColumn(new EntityIconColumn("Action", 
                                                array(array("EditIcone", "Mooc.EditCategory", "Mooc.EditCategory"),
                                                      array("DeleteIcone", "Mooc.DeleteCategory", "Mooc.DeleteCategory"),
                                                )    
                        ));

      return $gdCategory->Render();
   }
   
   /***
    * Artile of the Mooc
    */
    function GetTabMooc(){
     
      $gdMooc = new EntityGrid("gdMooc", $this->Core);
      $gdMooc->Entity = "Apps\Mooc\Entity\MoocMooc";
      $gdMooc->AddArgument(new Argument("Apps\Mooc\Entity\MoocMooc", "MoocId", EQUAL, $Mooc->IdEntite));
      $gdMooc->AddOrder("Id Desc");
      
      $gdMooc->App = "Mooc";
      $gdMooc->Action = "GetTabMooc";

      $btnAdd = new Button(BUTTON, "btnAddMooc");
      $btnAdd->Value = $this->Core->GetCode("Mooc.AddMooc");

      $gdMooc->AddButton($btnAdd);
      
      $gdMooc->AddColumn(new EntityFunctionColumn("Image", "GetImage"));
      $gdMooc->AddColumn(new EntityColumn("Name", "Name"));
      
      $commentPlugin = \Apps\EeApp\Helper\AppHelper::GetPluginType($this->Core, "Mooc", "Comment");
     
      if($commentPlugin != null ){
         $gdMooc->AddColumn(new EntityFunctionColumn("Comments", "GetComments"));
      }
      
      $gdMooc->AddColumn(new EntityIconColumn("Action", 
                                                array(array("EditIcone", "Mooc.EditMooc", "Mooc.EditMooc"),
                                                      array("DeleteIcone", "Mooc.DeleteMooc", "Mooc.DeleteMooc"),
                                                )    
                        ));

      return $gdMooc->Render();
   }

   /***
    * Edit a lesson
    */
   function EditLesson($lessonId, $moocId){
       
        $lesson = new MoocLesson($this->Core);
        
        if($lessonId != ""){
            $lesson->GetById($lessonId);
        } 
        
        $lesson->MoocId->Value = $moocId;
        
        $moocLessonForm = new MoocLessonForm($this->Core, $lesson, $moocId);
        $moocLessonForm->Load($lesson);
        
        return $moocLessonForm->Render();
   }

   /***
    * Save a category
    */
   function SaveCategory($core, $data){
      return CategoryHelper::SaveCategory($core, $data);
   }

   /**
    * Delete a category
    */
   function DeleteCategory($core, $categoryId){
      return CategoryHelper::DeleteCategory($core, $categoryId);
   }
   
   /***
    * Save a Mooc
    */
   function SaveMooc($core, $data){
      return MoocHelper::SaveMooc($core, $data);
   }

   /***
    * Delete a Mooc
    */
   function DeleteMooc($core, $moocId){
      return MoocHelper::DeleteMooc($core, $moocId);
   }

   /**
    * Save a lesson
    */
   function SaveLesson($core, $data){
      return MoocHelper::SaveLesson($core, $data);
   }

   /***
    * Remove a lesson
    */
   function RemoveLesson($core, $lessonId){
      MoocHelper::RemoveLesson($core, $lessonId);
   }


    /*action*/
 }?>
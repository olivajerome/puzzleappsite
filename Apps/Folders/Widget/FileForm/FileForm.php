<?php

namespace Apps\Folders\Widget\FileForm;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;

class FileForm extends Form {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "") {
        $this->Core = $core;
        $this->Data = $data;

        //On le fait ici pour avoir le formuaire à l'affichage mais aussi à la validation
        $this->Init($data);
    }

    /**
     * Initialisation 
     */
    function Init($data) {
        $this->form = new Form("fileForm", $data);

        $this->form->SetView(__DIR__ . "/View/index.tpl", $this->Core);
        $this->form->SetAction("/View/index.tpl");

        $this->form->Add(array("Type" => "Hidden",
            "Id" => "Id",
        ));
        
        $this->form->Add(array("Type" => "Hidden",
            "Id" => "FolderId",
        ));
        
        $this->form->Add(array("Type" => "TextBox",
            "Id" => "Name",
            "Validators" => ["Required"]
        ));

        $this->form->Add(array("Type" => "TextArea",
            "Id" => "Description",
        ));
 
        $this->form->Add(array("Type" => "Upload",
            "Id" => "Upload",
            "App" => "Folders",
            "Method" => "SaveDocument",
            "Accept" => "doc,png,txt,pdf",
            "Validators" => ["Required"]
        ));
        
        $this->form->Add(array("Type" => "Button",
            "Id" => "btnSaveFile",
            "Value" => $this->Core->GetCode("Base.Save"),
            "CssClass" => "btn btn-primary"
        ));
    }

    /*     * *
     * Générer les formulaire
     */

    function Render() {
        return $this->form->Render();
    }

    /*     * *
     * Valide les données selon les différents formulaires
     */

    function Validate($data = "") {
        $errors = $this->form->Validate($this->Data);

        return $errors;
    }

    /*     * *
     * Charge les controls avec les données de l'entité
     */

    function Load($formPage) {
        $this->form->Load($formPage);
    }

    /*     * *
     * Charge l'entité avec les données des différents formulaire
     */

    function Populate($formPage) {
        
    }

    /*     * *
     * Gestion des erreurs
     */

    function Error() {
        $errors[] = $this->form->errors;

        return json_encode(array("statut" => "Error", "message" => $errors));
    }

}

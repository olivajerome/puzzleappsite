<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Forum\Helper;

use Core\Entity\Entity\Argument;
use Core\Utility\Format\Format;
use Apps\Forum\Entity\ForumCategory;
use Apps\Forum\Entity\ForumMessage;
use Apps\Forum\Helper\MessageHelper;

use Apps\Forum\Widget\CategoryForm\CategoryForm;

class CategoryHelper
{   
    /***
     * Sauvegarde une catégorie
     */
    public static function SaveCategory($core, $data){

        $categoryForm = new CategoryForm($core, $data);

        if($categoryForm->Validate($data)){

            $category = new ForumCategory($core);
            
            if($data["Id"] != "" ){
                $category->GetById($data["Id"]);
            } else {
                $forum = ForumHelper::GetFirst($core);
                $category->ForumId->Value = $forum->IdEntite;
            }

            $category->Code->Value = Format::ReplaceForUrl($data["Name"]);
         
            $categoryForm->Populate($category);
            $category->Save();

            return true;
        }

        return false;
    }
    
    /**
     * Retourne les catégories d'un forum
     * @param type $core
     * @param type $forumId
     */
    public static function GetByForum($core, $forumId, $toArray = false)
    { 
        $category = new ForumCategory($core);
        $category->AddArgument(new Argument("Apps\Forum\Entity\ForumCategory", "ForumId" ,EQUAL, $forumId));
        $categories = $category->GetByArg();
        
        if($toArray == false){
            return $categories;
        } else {
            
            $categoryArray = array();
                
            foreach($categories as $categorie){
                $categorie = $categorie->toArray();
                $lastMessage  = MessageHelper::GetLastMessage($core, $categorie["IdEntite"]);
                $categorie["LastMessage"] = $lastMessage ? $lastMessage->ToArray() : "";
                        
                $categoryArray[] = $categorie;
            }    
            
           return $categoryArray; 
        }
    }
    
    /*
     * Supprime une catégorie
     */
    public static function DeleteCategory($core, $categoryId)
    {
        
        //Suppression de la categorie
        $categorie = new ForumCategory($core);
        $categorie->GetById($categoryId);
        $categorie->Delete();
        
        return true;
        //TODO supprimé les message liées
    }
    
    /**
     * Obtient le nombre de message d'une categorie
     * @param type $core
     * @param type $category
     */
    public static function GetNumberMessage($core, $category)
    {
       return (count(self::GetMessages($core, $category)));
    }
    
    /*
     * Obtient les message d'une categorie
     */
    public static function GetMessages($core, $category)
    {
        $message = new ForumMessage($core);
        $message->AddArgument(new Argument("Apps\Forum\Entity\ForumMessage", "CategoryId", EQUAL, $category));
        
        return $message->GetByArg();
    }
    
    public static function GetCategory($core, $categorieId){
     
       $categorie = new ForumCategory($core);
       $categorie->GetById($categorieId);
      
       $messages = self::GetMessages($core, $categorieId);
       
       $message = new ForumMessage($core);
       
       return array("Categorie" => $categorie->ToArray(), "Messages" => $message->ToAllArray($messages));
    }
    
    
}


?>

CREATE TABLE IF NOT EXISTS `ShopCategory` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`ShopId` int(11) NOT NULL, 
`Name` TEXT  NULL ,
`Code` TEXT  NULL ,
`Description` TEXT  NULL ,
PRIMARY KEY (`Id`),
CONSTRAINT `ShopCategoryShopShop` FOREIGN KEY (`ShopId`) REFERENCES `ShopShop`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
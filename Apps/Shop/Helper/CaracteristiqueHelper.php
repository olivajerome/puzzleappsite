<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Shop\Helper;

use Core\Utility\Format\Format;
use Core\Entity\Entity\Argument;

use Apps\Shop\Helper\ShopHelper;
use Apps\Shop\Entity\ShopCaracteristique;
use Apps\Shop\Entity\ShopCaracteristiqueItem;
use Apps\Shop\Widget\CaracteristiqueForm\CaracteristiqueForm;


class CaracteristiqueHelper {

    /**
     * Sauvegarde une caractériqtiques et ses valeurs
     */
    public static function SaveCaracteristique($core, $data) {
        $caracteristiqueForm = new caracteristiqueForm($core, $data);

        if ($caracteristiqueForm->Validate($data)) {

            $caracteristique = new ShopCaracteristique($core);

            if ($data["Id"] != "") {
                $caracteristique->GetById($data["Id"]);
            } else {
                $shop = ShopHelper::GetShop($core);
                $caracteristique->ShopId->Value = $shop[0]->IdEntite;
                // $caracteristique->Code->Value = Format::ReplaceForUrl($data["Name"]);
            }

            $caracteristiqueForm->Populate($caracteristique);
            $caracteristique->Save();

            //Sauvegarde des items
            foreach (json_decode($data["items"]) as $caracteristiqueItem) {

                $item = new ShopCaracteristiqueItem($core);
                if ($caracteristiqueItem->id != 0) {
                    $item->GetById($caracteristiqueItem->id);
                }
                $item->CaracteristiqueId->Value = $data["Id"];
                $item->Label->Value = $caracteristiqueItem->label;
                $item->Save();
            }

            return true;
        }

        return false;
    }

    /*     * *
     * Remove a caractristic item
     */

    public static function RemoveCaracteristiqueItem($core, $itemId) {

        $item = new ShopCaracteristiqueItem($core);
        $item->GetById($itemId);
        $item->Delete();
    }

    /*     * *
     * Get the items of a caracteristique
     */

    public static function GetItemByCaracteristique($core, $caracteristiqueId) {
        $item = new ShopCaracteristiqueItem($core);
        $items = $item->Find("CaracteristiqueId=" . $caracteristiqueId);

        return $item->ToAllArray($items);
    }

    /*     * *
     * Delete a caracteristiques 
     */

    public static function DeleteCaracteristique($core, $caracteristiqueId) {

        $items = new ShopCaracteristiqueItem($core);
        $items->AddArgument(new Argument("Apps\Shop\Entity\ShopCaracteristiqueItem", "CaracteristiqueId", EQUAL, $caracteristiqueId));
        $items->DeleteByArg();

        $caracteristique = new ShopCaracteristique($core);

        $caracteristique->GetById($caracteristiqueId);
        $caracteristique->Delete();
        
        return true;
    }
}
        
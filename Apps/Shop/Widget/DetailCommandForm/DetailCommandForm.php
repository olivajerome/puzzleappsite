<?php

namespace Apps\Shop\Widget\DetailCommandForm;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;

use Apps\Shop\Entity\ShopProduct;
use Apps\Shop\Entity\ShopCommandLine;


class DetailCommandForm extends Form {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "" , $renderAddres = true) {
        $this->Core = $core;
        $this->Data = $data;

        //On le fait ici pour avoir le formuaire � l'affichage mais aussi � la validation
        $this->Init($data, $renderAddres);
    }

    /**
     * Initialisation 
     */
    function Init($data, $renderAddres) {
        $this->form = new Form("DetailCommandFormForm", $data);

        $this->form->SetView(__DIR__ . "/View/index.tpl", $this->Core);
        $this->form->SetAction("/View/index.tpl");

        $this->form->Add(array("Type" => "Hidden",
            "Id" => "Id",
        ));
        
        $this->form->Add(array("Type" => "TextBox",
            "Id" => "Name",
            "Validators" => ["Required"]
        ));

        $this->form->Add(array("Type" => "ListBox",
            "Id" => "Status",
            "Values" => array( "En cours"=> 1,  "Payer"=> 2,  "A Livrer" => 3),
            "Validators" => ["Required"]
        ));

        
        $this->form->Add(array("Type" => "DateBox",
            "Id" => "DateCreated",
            "Validators" => ["Required"]
        ));

        if($renderAddres){
            $this->form->AddElementView(new ElementView('products', $this->RenderProduct($data)));
            $this->form->AddElementView(new ElementView('address', $this->RenderAddress($data)));
        }

        $this->form->Add(array("Type" => "Button",
            "Id" => "btnUpdateCommand",
            "Value" => $this->Core->GetCode("Shop.UpdateCommande"),
            "CssClass" => "btn btn-primary"
        ));
    }

    /****
     * Tableau des produits
     */
    function RenderProduct($command){
     
        $view  = "<table class='grid'>";
        $view .= "<tr><th>Product</th><th>Price</th></tr>";
        
        $line = new ShopCommandLine($this->Core);
        $lines = $line->Find("CommandId=" . $command->IdEntite);
        $total = 0;

        foreach($lines as $line){
            $total += (float)$line->Price->Value;
            
            $product = new ShopProduct($this->Core);
            $product->GetById($line->Entity->Value);

            $view .= "<tr>";
            $view .= "<td>".$product->Name->Value."</td>";
            $view .= "<td>".$line->Price->Value."</td>";
            $view .= "<tr>";
        }

        $view .= "<tr><td>" .$this->Core->GetCode("Shop.Total"). "</td>";  
        $view .= "<td>".$total."</td></tr>";

        $view .= "</table>";  

        return $view;
    }

    /***
     * Addres de facturation et de livraison
     */
    function RenderAddress($command){
     return \Apps\Shop\Decorator\CommandDecorator::GetAddress($command)   ;
    }
    
    /*     * *
     * G�n�rer les formulaire
     */

    function Render() {
        return $this->form->Render();
    }

    /*     * *
     * Valide les donn�es selon les diff�rents formulaires
     */

    function Validate($data = "") {
        $errors = $this->form->Validate($this->Data);

        return $errors;
    }

    /*     * *
     * Charge les controls avec les donn�es de l'entit�
     */

    function Load($entity) {
        $this->form->Load($entity);
    }

    /*     * *
     * Charge l'entit� avec les donn�es des diff�rents formulaire
     */

    function Populate($entity) {
        
    }

    /*     * *
     * Gestion des erreurs
     */

    function Error() {
        $errors[] = $this->form->errors;

        return json_encode(array("statut" => "Error", "message" => $errors));
    }

}


 DROP TABLE IF EXISTS ShopPlugin;
 DROP TABLE IF EXISTS ShopProductCaracteristique;
 DROP TABLE IF EXISTS ShopProductCategory;
 DROP TABLE IF EXISTS ShopProduct;
 DROP TABLE IF EXISTS ShopCaracteristiqueItem;
 DROP TABLE IF EXISTS ShopCaracteristique;
 DROP TABLE IF EXISTS ShopCategory;
 DROP TABLE IF EXISTS ShopShop;
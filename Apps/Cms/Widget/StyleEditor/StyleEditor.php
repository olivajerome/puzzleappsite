<?php

namespace Apps\Cms\Widget\StyleEditor;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;
use Core\Entity\Entity\Argument;
use Apps\Cms\Entity\CmsCms;
use Apps\Cms\Entity\CmsPage;
use Apps\Cms\Entity\CmsContainer;

class StyleEditor {
    
    public function __construct($core) {
        $this->Core = $core;
    }
    
    /***
     * Render the widget
     */
    public function Render(){
       
        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);
        $cms = new CmsCms($this->Core);
        $cms->GetById(1);
        $view->AddElement(new ElementView("Cms", $cms));

        return $view->Render();
    }
}
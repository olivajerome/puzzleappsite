var EntityGrid = function(){};

EntityGrid.Initialise = function(grid, actionLine)
{
    setTimeout("EntityGrid.Init('"+grid+"' , '"+ actionLine +"')", 1000);
};

EntityGrid.Init = function(grid, actionLine)
{
    EntityGrid.GridName = grid;
    
    //Init the Action of the column 
    let gd = document.getElementById(grid);
    
    if(gd == null){
        return;
    }
   
    let actionIcon = gd.getElementsByClassName("actionIcon");
   
    if(actionLine != undefined)
    {
        var tr = gd.getElementsByTagName("tr");
        
        for(i = 0; i < tr.length ; i++)
        {
           // Dashboard.AddEvent(tr[i], "click", EntityGrid.executeFirstAction);
        }
    }
    
    for(i = 0; i < actionIcon.length ; i++)
    {
        Dashboard.AddEvent(actionIcon[i], "click", EntityGrid.execute);
    }
    
    var pager = gd.getElementsByClassName("entityPager");
    
    for(i = 0; i < pager.length ; i++)
    {
        Dashboard.AddEvent(pager[i], "click", EntityGrid.loadPage);
    }
    
     var orders = gd.getElementsByClassName("order");
    
    for(i = 0; i < orders.length ; i++)
    {
        //Dashboard.AddEvent(orders[i], "click", EntityGrid.sortPage);
    }
    
};

EntityGrid.execute = function(e)
{
    control = e.srcElement;
  
    action = control.dataset.action;
    idEntite = control.dataset.identite;
    params = control.dataset.params;
    
    eval(action + "("+ idEntite +", '"+params+"', control)");
          
};

/**
 * Clique sur le premier icone
 **/
EntityGrid.executeFirstAction = function(e)
{
   if(e.srcElement.className.indexOf("actionIcon") == -1  )
    {
      var parent = e.srcElement.parentNode.parentNode;
      var actionIcon = parent.getElementsByClassName("actionIcon");
      actionIcon[0].click();
   }
};

EntityGrid.loadPage =function (e)
{
   control = e.srcElement;
   grid = control.parentNode.parentNode.parentNode.parentNode;
  
  if(grid != undefined)
  {
  
   app = grid.dataset.app.toString();
   action = grid.dataset.action.toString();
   order = grid.dataset.order;
   params = grid.dataset.params;

   //Lance une requete ajax pour charger le element suivant
   //La grid tout avoir tous les élements our souvri sur quoi requete
    grid.innerHTML = "Loading ...";
           var JAjax = new ajax();
               JAjax.data = "App=" + app +"&Methode=" + action;
               JAjax.data += "&Page=" + control.innerHTML;
               JAjax.data += "&Params=" + params;
         
        if(order != "")
        {
             JAjax.data += "&Sort=" + order;
        }
        
        grid.innerHTML = JAjax.GetRequest("Ajax.php");
    
    setTimeout("EntityGrid.Init('"+EntityGrid.GridName+"')", 1000);
    }
    
};

EntityGrid.sortPage =function (e)
{
   control = e.srcElement;
   grid = control.parentNode.parentNode.parentNode;
  
   if(grid != undefined)
  {
  
    app = grid.dataset.app.toString();
    action = grid.dataset.action.toString();
    params = grid.dataset.params;
   
    grid.dataset.order = control.innerHTML;

    //Lance une requete ajax pour charger le element suivant
    //La grid tout avoir tous les élements ou s'ouvrir sur quoi requete
     grid.innerHTML = "Loading ...";
            var JAjax = new ajax();
                JAjax.data = "App=" + app +"&Methode=" + action;
                JAjax.data += "&Sort=" + control.innerHTML;
                JAjax.data += "&Params=" + params;

            grid.innerHTML = JAjax.GetRequest("Ajax.php");

     setTimeout("EntityGrid.Init('"+ EntityGrid.GridName +"')", 1000);
  }

};

/***
 * Charge la grille avec les données recus
 * @param {type} gridId
 * @param {type} data
 * @returns {undefined}
 */
EntityGrid.Load = function(gridId, data){
    
    let grid = Dom.GetById(gridId);
    let lines = grid.getElementsByTagName("tr");
    let numberLines = lines.length;
    let header = lines[0];
    let columns = header.getElementsByTagName("th");
    let headers = new Array(); 
    let actions = "";
    
    //On récupére les entêtes et les colonne d'actions
    for(var i = 0; i < columns.length; i++){
        //Colonne d'action
        headers.push(columns[i].innerHTML);
    }
    
    //On supprime les lignes
    for(var i = numberLines - 1 ; i > 0 ; i-- ){
        lines[i].parentNode.removeChild(lines[i]);
    }
 
    //On ajoute les nouvelles lignes
    for(var i; i <data.length; i++ ){
        newLine = '<tr>';
        
        for(j = 0; j < headers.length - 1 ; j++){
            newLine += "<td>" +data[i][headers[j]] + "</td>";
        }
        
        //Ajout de la colone d'action
        newLine += "<td>" + (headers[headers.length - 1]).replaceAll('data-identite="all', 'data-identite="'+data[i].IdEntite+'"') + "</td>";
        
        //Ajout des la colone d'action
        newLine += "</tr>";
        
        grid.innerHTML += newLine;
    }

   // actionColumn = grid.querySelectorAll("td .iconAction");
    Animation.Show("#" + gridId + " td .iconAction ");

    EntityGrid.Init(gridId);
};

var UserActionColumn=new UserActionColumn();

function UserActionColumn()
{
	this.DoAction = function (UserAction, sender, id)
	{
		document.forms[sender].UserAction.value = UserAction;
		document.forms[sender].IdEntity.value = id;
	    document.forms[sender].submit();
	};
};
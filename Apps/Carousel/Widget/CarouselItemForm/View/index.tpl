{{form->Open()}}
{{form->Error()}}
{{form->Success()}}

{{form->Render(Id)}}
{{form->Render(CarouselId)}}


    <div>
        <label>{{GetCode(Carousel.Name)}}</label> 
        {{form->Render(Name)}}
    </div>

    <div>
        <label>{{GetCode(Carousel.Content)}}</label> 
        {{form->Render(Content)}}
    </div>
    
    <div class='center marginTop' >   
        {{form->Render(btnSaveItem)}}
    </div>  

{{form->Close()}}
<h1 >{{GetCode(Blog.Articles)}}</h1>
{{foreach Article}}

<div class='col-md-3 block-terc'>

    <div class='' style='height:350px'>
        <span style='display:block; width:100%; height: 100px; background-size: cover;background-image: url({{element->GetImagePath()}})'>
        </span>

        <h3 style="min-height:60px"> {{element->Name->Value}}
            <p style='font-size:8px'>
                {{element->DateCreated->Value}}
            </p>
        </h3>

        <div style='min-height:120px'>{{element->Description->Value}}</div>

        <a href ='{{GetPath(/Blog/Article/{{element->Code->Value}})}}' >

            {{GetCode(Blog.ReadThisArticle)}}
        </a>
    </div>
</div>


{{/foreach Article}}


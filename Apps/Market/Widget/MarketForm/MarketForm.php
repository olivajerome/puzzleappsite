<?php

namespace Apps\Market\Widget\MarketForm;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;

class MarketForm extends Form {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "") {
        $this->Core = $core;
        $this->Data = $data;

        $this->Init($data);
    }

    /**
     * Initialisation 
     */
    function Init($data) {
        $this->form = new Form("MarketFormForm", $data);

        $this->form->SetView(__DIR__ . "/View/index.tpl", $this->Core);
        $this->form->SetAction("/View/index.tpl");

        $this->form->Add(array("Type" => "Hidden",
            "Id" => "Id",
        ));
        
        $this->form->Add(array("Type" => "TextBox",
            "Id" => "Name",
            "Validators" => ["Required"]
        ));
        
        $this->form->Add(array("Type" => "TextArea",
            "Id" => "Description",
            "Validators" => ["Required"]
        ));

        $this->form->Add(array("Type" => "Button",
            "Id" => "btnSaveMarket",
            "Value" => $this->Core->GetCode("Base.Save"),
            "CssClass" => "btn btn-primary"
        ));

        $plugins = \Apps\EeApp\Helper\AppHelper::GetPluginApp($this->Core, "Market");
        
        $commentPlugin =   \Apps\EeApp\Helper\AppHelper::GetPluginType($this->Core, "Market","Comment");
        $this->form->AddElementView(new ElementView("PluginCommentScript", $commentPlugin ? "<div style='display:none' id='pluginCommentScript'>" .$commentPlugin->GetRegister() . "</div>" : ""));
        
        $this->form->AddElementView(new ElementView("Plugins", $plugins));
        $this->form->AddElementView(new ElementView("ShowPlugin", $data != null ? true : false));
    }

    /*     * *
     * Render the html form
     */

    function Render() {
        return $this->form->Render();
    }

    /*     * *
     * Validate the data
     */

    function Validate($data = "") {
        $errors = $this->form->Validate($this->Data);

        return $errors;
    }

    /*     * *
     * Load control with the entity
     */

    function Load($entity) {
        $this->form->Load($entity);
    }

    /*     * *
     * Populate the entity with the form Data
     */

    function Populate($entity) {
        
    }

    /*     * *
     * Error message
     */

    function Error() {
        $errors[] = $this->form->errors;

        return json_encode(array("statut" => "Error", "message" => $errors));
    }

}

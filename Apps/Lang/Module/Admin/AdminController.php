<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Lang\Module\Admin;

use Core\Controller\AdministratorController;
use Core\View\View;
use Core\View\ElementView;
use Core\Control\TabStrip\TabStrip;
use Core\Control\EntityGrid\EntityGrid;
use Core\Control\EntityGrid\EntityColumn;
use Core\Control\EntityGrid\EntityIconColumn;
use Core\Control\EntityGrid\EntityFunctionColumn;
use Core\Control\Button\Button;
use Apps\Lang\Module\Element\ElementController;

class AdminController extends AdministratorController {

    /**
     * Creation
     */
    function Create() {
        
    }

    /**
     * Initialisation
     */
    function Init() {
        
    }

    /**
     * Affichage du module
     */
    function Show($all = true) {
        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);

        $tabLang = new TabStrip("tabLang", "Lang");
        $tabLang->AddTab($this->Core->GetCode("Lang.Langs"), $this->GetTabLangs());
        $tabLang->AddTab($this->Core->GetCode("Lang.LangElement"), $this->GetTabLangElement());

        $view->AddElement(new ElementView("tabLang", $tabLang->Render()));

        return $view->Render();
    }

    /*     * *
     * Get The Lang Avialable for the Site
     */

    function GetTabLangs() {

        $gdLang = new EntityGrid("gdLang", $this->Core);
        $gdLang->Entity = "Core\Entity\Langs\Langs";
        $gdLang->App = "Lang";
        $gdLang->Action = "GetTabLangs";

        $btnAdd = new Button(BUTTON, "btnAddLang");
        $btnAdd->Value = $this->Core->GetCode("Lang.AddLang");

        $gdLang->AddButton($btnAdd);
        $gdLang->AddColumn(new EntityColumn("Name", "Name"));
        $gdLang->AddColumn(new EntityColumn("Code", "Code"));

        return $gdLang->Render();
    }

    /**
     * Get Tab Element
     * @return type
     */
    function GetTabLangElement() {
        $elementController = new ElementController($this->Core);
        return $elementController->LoadElement(0);
    }
}

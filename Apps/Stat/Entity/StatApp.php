<?php 
/*
* PuzzleApp
* Webemyos
* Jérôme Oliva
* GNU Licence
*/

namespace Apps\Stat\Entity;
use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class StatApp extends Entity  
{
	//Constructeur
	function __construct($core)
	{
            
		//Version
		$this->Version ="2.0.0.0"; 

		//Nom de la table 
		$this->Core=$core; 
		$this->TableName="StatApp"; 
		$this->Alias = "StatApp"; 

		$this->Code = new Property("Code", "Code", TEXTBOX,  false, $this->Alias); 
		$this->Date = new Property("Date", "Date", TEXTBOX,  false, $this->Alias); 
		$this->User = new Property("User", "User", TEXTBOX,  false, $this->Alias); 

		//Creation de l entité 
		$this->Create(); 
	}
}
?>
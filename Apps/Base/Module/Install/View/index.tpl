<section class='container'>
    <div class='row'>
        <h1>Install PuzzleApp</h1>
        <div class='col-md-6'>
            <h2>Instruction</h2>
            <p>
                Before use your new website, you need run this install progam.<br/>
                Check this requirement : 
                <ul>
                    <li>You have à mysql Database</li>
                    <li>Set the email user admin</li>
                    <li>Set a new password for the admin</li>
                    <li>The server have the right to write in all folders</li>
                </ul>
            </p>
        </div>
        <div class='col-md-6'> 
        <h2>Configuration</h2>
            
        {{GetForm(/Install)}}

            <div class="form-group"> 
                <label>Database server</label>
                {{GetControl(TextBox,Serveur,{Required=true})}}
            </div>
            <div class="form-group"> 
                <label>Database Name</label>
                {{GetControl(TextBox,DataBase,{Required=true})}}
            </div>
            <div class="form-group"> 
                <label>Database user</label>
                {{GetControl(TextBox,User,{Required=true})}}
            </div>
            <div class="form-group"> 
                <label>Database Password</label>
                {{GetControl(PassWord,Password)}}
            </div>
            <div class="form-group"> 
                <label>Email administrator</label>
                {{GetControl(EmailBox,Admin,{Required=true})}}
            </div>
            <div class="form-group"> 
                <label>Password administrator</label>
                {{GetControl(PassWord,PassAdmin,{Required=true})}}
            </div>
            <div>
            {{GetControl(Submit,btnSubmit,{Value=Continue})}}
            </div>
        </div>
        
        
        {{CloseForm()}}
     </div>
</section>
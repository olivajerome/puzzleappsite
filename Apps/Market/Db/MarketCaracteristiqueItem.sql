CREATE TABLE IF NOT EXISTS `MarketCaracteristiqueItem` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`CaracteristiqueId` INT  NOT NULL,
`Label` TEXT  NOT NULL,
PRIMARY KEY (`Id`),
CONSTRAINT `MarketCaracteristique_MarketCaracteristiqueItem` FOREIGN KEY (`CaracteristiqueId`) REFERENCES `MarketCaracteristique`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
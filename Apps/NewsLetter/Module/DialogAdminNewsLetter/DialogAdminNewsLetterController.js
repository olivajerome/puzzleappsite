var DialogAdminNewsLetterController = function(){};

DialogAdminNewsLetterController.AddEmailBase = function(data){
    NewsLetterEmailForm.Init(function(data){NewsLetter.ReloadEmailBase(data)});
};

DialogAdminNewsLetterController.AddListEmail = function(data){
    NewsLetterListEmailForm.Init(function(data){NewsLetter.ReloadListEmail(data)});
};

DialogAdminNewsLetterController.AddCampagne = function(data){
    NewsLetterCampagneForm.Init(function(data){NewsLetter.ReloadCampagne(data)});
};

DialogAdminNewsLetterController.SendCampagne = function(data){
    SendCampagneForm.Init();
};
<?php 
/*
* PuzzleApp
* Webemyos
* Jérôme Oliva
* GNU Licence
*/

namespace Apps\RoadMap\Entity;
use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class RoadMapTicket extends Entity  
{
	//Constructeur
	function __construct($core)
	{
		//Version
		$this->Version ="2.0.0.0"; 

		//Nom de la table 
		$this->Core=$core; 
		$this->TableName="RoadMapTicket"; 
		$this->Alias = "RoadMapTicket"; 

		$this->ColumnId = new Property("ColumnId", "ColumnId", NUMERICBOX,  true, $this->Alias); 
		$this->Name = new Property("Name", "Name", TEXTBOX,  true, $this->Alias); 
		$this->Code = new Property("Code", "Code", TEXTBOX,  true, $this->Alias); 
		$this->Description = new Property("Description", "Description", TEXTAREA,  true, $this->Alias); 
		$this->Status = new Property("Status", "Status", NUMERICBOX,  true, $this->Alias); 

		//Creation de l entité 
		$this->Create(); 
	}
}
?>
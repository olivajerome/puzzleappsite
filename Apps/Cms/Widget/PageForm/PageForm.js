var PageForm = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
PageForm.Init = function(){
    Event.AddById("btnSave", "click", PageForm.SaveElement);
};

/***
* Obtient l'id de l'itin�raire courant 
*/
PageForm.GetId = function(){
    return Form.GetId("PageForm");
};

/*
* Sauvegare l'itineraire
*/
PageForm.SaveElement = function(e){
   
    if(Form.IsValid("PageForm"))
    {

    var data = "Class=Cms&Methode=SavePage&App=Cms";
        data +=  Form.Serialize("PageForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.statut == "Error"){
                Form.RenderError("PageForm", data.message);
            } else{

                Form.SetId("PageForm", data.data.Id);
                Dialog.Close();
                Dashboard.StartApp("", "Cms", "");
            }
        });
    }
};


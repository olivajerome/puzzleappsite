<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Profil\Module\Member;

use Core\Control\Button\Button;
use Core\Controller\Controller;
use Core\View\View;
use Core\View\ElementView;
use Core\Entity\Entity\Argument;
use Apps\Forum\Helper\MessageHelper;

/*
 * 
 */

class MemberController extends Controller {

    /**
     * Constructeur
     */
    function _construct($core = "") {
        $this->Core = $core;
    }

    function Index() {
        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);
        return $view->Render();
    }
}

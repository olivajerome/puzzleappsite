<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Notify\Module\Admin;

use Core\Controller\AdministratorController;
use Core\View\View;
use Core\View\ElementView;
use Core\Control\Icone\EditIcone;
use Core\Control\Libelle\Libelle;
use Core\Control\TabStrip\TabStrip;
use Core\Control\EntityGrid\EntityGrid;
use Core\Control\EntityGrid\EntityColumn;
use Core\Control\EntityGrid\EntityIconColumn;
use Core\Control\EntityGrid\EntityFunctionColumn;
use Core\Entity\Entity\Argument;
use Core\Control\Button\Button;

use Apps\Notify\Widget\NotifyParametersForm\NotifyParametersForm;
use Apps\Notify\Entity\NotifyParameters;

use Apps\Notify\Widget\NotifyTemplateForm\NotifyTemplateForm;
use Apps\Notify\Entity\NotifyTemplate;


use Apps\Notify\Helper\MessageHelper;

/*
 * 
 */
 class AdminController extends AdministratorController
 {
    /**
     * Constructeur
     */
    function _construct($core="")
    {
          $this->Core = $core;
    }

    /**
     * Create
     */
    function Create()
    {
    }

    /**
     * Init
     */
    function Init()
    {
    }

    /**
     * Render
     */
    function Show($all=true)
    {
         return $this->Index();
    }
    
    /*
    * Get the home page
    */
   function Index()
   {
    $view = new View(__DIR__."/View/index.tpl", $this->Core);
      
    $tabNotify = new TabStrip("tabNotify", "Notify");
    $tabNotify->AddTab($this->Core->GetCode("Notify.Parameters"), $this->GetTabParameters());
    $tabNotify->AddTab($this->Core->GetCode("Notify.Extensions"), $this->GetTabExtension());
    $tabNotify->AddTab($this->Core->GetCode("Notify.Template"), $this->GetTabTemplate());
    $tabNotify->AddTab($this->Core->GetCode("Notify.Notify"), $this->GetTabNotify());

    $view->AddElement(new ElementView("tabNotify", $tabNotify->Render()));

    return $view->Render();
   }

   /**
    * Parameters of notify 
    * for sender / Sender, Reply to ...
    */
   function GetTabParameters(){

    $gdParameters = new EntityGrid("gdParameters", $this->Core);
    $gdParameters->Entity = "Apps\Notify\Entity\NotifyParameters";
    $gdParameters->App = "Notify";
    $gdParameters->Action = "GetTabParameters";

    $btnAdd = new Button(BUTTON, "btnAddParameters");
    $btnAdd->Value = $this->Core->GetCode("Notify.AddParameters");

    $gdParameters->AddButton($btnAdd);
    $gdParameters->AddColumn(new EntityColumn("Name", "Name"));
    $gdParameters->AddColumn(new EntityColumn("Description", "Description"));
    
    $gdParameters->AddColumn(new EntityIconColumn("Action", 
                                            array(array("EditIcone", "Notify.EditParameter", "Notify.EditParameter"),
                                                    array("DeleteIcone", "Notify.DeleteParameter", "Notify.DeleteParameter"),
                                            )    
                    ));

    return $gdParameters->Render();
   }

   /***
    * Save the parameters
    */
   function SaveParameter($data){

      $parameterForm = new NotifyParametersForm($this->Core, $data);

      if($parameterForm->Validate($data)){

         $notifyParameter = new NotifyParameters($this->Core);

         if($data["Id"]){
            $notifyParameter->GetById($data["Id"]);
         }

         $parameterForm->Populate($notifyParameter);
         $notifyParameter->Save();
         
         return true;
      } 

      return $parameterForm->errors;
   }

   /***
    * Delete a parameter
    */
   function DeleteParameter($parameterId){
      
      $notifyParameter = new NotifyParameters($this->Core);
      $notifyParameter->GetById($parameterId);
      $notifyParameter->Delete();

      return true;
   }

   /***
     * Extension de l'application
     */
    function GetTabExtension(){
        $view = new View(__DIR__ . "/View/extension.tpl", $this->Core);
         
        $plugins = \Apps\EeApp\Helper\AppHelper::GetPluginApp($this->Core, "Notify");
        $view->AddElement(new ElementView("Plugins", $plugins));
 
        return $view->Render();
     }
    
     /***
      * Template of email
      */
     function GetTabTemplate(){
        $gdTemplate = new EntityGrid("gdTemplate", $this->Core);
        $gdTemplate->Entity = "Apps\Notify\Entity\NotifyTemplate";
        $gdTemplate->App = "Mooc";
        $gdTemplate->Action = "GetTabTemplate";

        $btnAdd = new Button(BUTTON, "btnAddTemplate");
        $btnAdd->Value = $this->Core->GetCode("Notify.AddTemplate");
        $gdTemplate->AddButton($btnAdd);

        $gdTemplate->AddColumn(new EntityColumn("Code", "Code"));
        $gdTemplate->AddColumn(new EntityColumn("Description", "Description"));
        $gdTemplate->AddColumn(new EntityColumn("Title", "Title"));
        

        $gdTemplate->AddColumn(new EntityIconColumn("Action", 
         array(array("EditIcone", "Notify.EditTemplate", "Notify.EditTemplate"),
               array("ShareIcone", "Notify.TestTemplate", "Notify.TestTemplate"),
               array("DeleteIcone", "Notify.DeleteTemplate", "Notify.DeleteTemplate"),
         )    
       ));

        return $gdTemplate->Render();
     }

     /**
    * Parameters of notify 
    * for sender / Sender, Reply to ...
    */
   function GetTabNotify(){

    $gdNotify = new EntityGrid("gdNotify", $this->Core);
    $gdNotify->Entity = "Apps\Notify\Entity\NotifyNotify";
    $gdNotify->App = "Mooc";
    $gdNotify->Action = "GetTabParameters";

    $gdNotify->AddColumn(new EntityColumn("DateCreate", "DateCreate"));
    $gdNotify->AddColumn(new EntityColumn("UserId", "UserId"));
    $gdNotify->AddColumn(new EntityColumn("Code", "Code"));
    $gdNotify->AddColumn(new EntityColumn("DestinataireId", "DestinataireId"));
    $gdNotify->AddColumn(new EntityColumn("AppName", "AppName"));
    $gdNotify->AddColumn(new EntityColumn("EntityName", "EntityName"));
    $gdNotify->AddColumn(new EntityColumn("EntityId", "EntityId"));
    
    return $gdNotify->Render();
   }

   /***
    * Save a template
    */
   function SaveTemplate($data){

      $templateForm = new NotifyTemplateForm($this->Core, $data);

      if($templateForm->Validate($data)){

         $notifyTemplate = new NotifyTemplate($this->Core);

         if($data["Id"]){
            $notifyTemplate->GetById($data["Id"]);
         }

         $templateForm->Populate($notifyTemplate);
         $notifyTemplate->Save();
         
         return true;
      } 

      return $templateForm->errors;
   }

   /***
    * Delete a template
    */
   function DeleteTemplate($templateId){
      $notifyTemplate = new NotifyTemplate($this->Core);
      $notifyTemplate->GetById($templateId);
      $notifyTemplate->Delete();
      
      return true;
   }
   
   /****
    * Test a template
    */
   function TestTemplate($data){
       
      $notifyTemplate = new NotifyTemplate($this->Core);
      $notifyTemplate->GetById($data["TemplateId"]);
      
      MessageHelper::SendMail($this->Core, $data["Email"], $notifyTemplate->Title->Value, $notifyTemplate->Content->Value);
   }

          /*action*/
 }?>
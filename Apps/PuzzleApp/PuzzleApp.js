var PuzzleApp = function () {};

/*
 * Chargement de l'application
 */
PuzzleApp.Load = function (parameter)
{
    this.LoadEvent();
};

/*
 * Chargement des �venements
 */
PuzzleApp.LoadEvent = function ()
{
    Dashboard.AddEventAppMenu(PuzzleApp.Execute, "", "PuzzleApp");
    Dashboard.AddEventWindowsTool("PuzzleApp");

    Event.AddById("btnCreateVersionFramework", "click", PuzzleApp.CreateVersionFramework);
    Event.AddById("btnCreateVersionPackage", "click", PuzzleApp.CreateVersionPackage);
    Event.AddById("btnCreateVersionTesting", "click", PuzzleApp.btnCreateVersionTesting);
    
    Event.AddByClass("btnVersionApp", "click", PuzzleApp.CreateVersionApp);
    Event.AddByClass("btnVersionPlugin", "click", PuzzleApp.CreateVersionPlugin);
    Event.AddByClass("btnVersionTemplate", "click", PuzzleApp.CreateVersionTemplate);
};

/*
 * Execute une fonction
 */
PuzzleApp.Execute = function (e)
{
    //Appel de la fonction
    Dashboard.Execute(this, e, "PuzzleApp");
    return false;
};

/*
 *	Affichage de commentaire
 */
PuzzleApp.Comment = function ()
{
    Dashboard.Comment("PuzzleApp", "1");
};

/*
 *	Affichage de a propos
 */
PuzzleApp.About = function ()
{
    Dashboard.About("PuzzleApp");
};

/*
 *	Affichage de l'aide
 */
PuzzleApp.Help = function ()
{
    Dashboard.OpenBrowser("PuzzleApp", "{$BaseUrl}/Help-App-PuzzleApp.html");
};

/*
 *	Affichage de report de bug
 */
PuzzleApp.ReportBug = function ()
{
    Dashboard.ReportBug("PuzzleApp");
};

/*
 * Fermeture
 */
PuzzleApp.Quit = function ()
{
    Dashboard.CloseApp("", "PuzzleApp");
};

/***
 * Création d'une nouvelle version du framework et mise à disposition
 */
PuzzleApp.CreateVersionFramework = function () {

    Animation.Confirm(Language.GetCode("PuzzleApp.ConfirmCreateVersion"), function () {

        var data = "Class=PuzzleApp&Methode=CreateVersionFramework&App=PuzzleApp";

        Request.Post("Ajax.php", data).then(data => {
            Animation.Notify(data);
        });
    });
};

/***
 * Création d'une nouvelle version du package d'installation
 */
PuzzleApp.CreateVersionPackage = function () {

    Animation.Confirm(Language.GetCode("PuzzleApp.ConfirmCreateVersionPackage"), function () {

        var data = "Class=PuzzleApp&Methode=CreateVersionPackage&App=PuzzleApp";

        Request.Post("Ajax.php", data).then(data => {
            Animation.Notify(data);
        });
    });
};

/***
 * Création d'une version de test
 * @returns {undefined}
 * e, test.puzzleapp
 */
PuzzleApp.btnCreateVersionTesting = function(){
        var data = "Class=PuzzleApp&Methode=CreateVersionTesting&App=PuzzleApp";

        Request.Post("Ajax.php", data).then(data => {
            Animation.Notify(data);
        });
};

/***
 * Création d'une nouvelle version d'une application
 */
PuzzleApp.CreateVersionApp = function (e) {
    let container = e.srcElement.parentNode.parentNode;
    let app = container.getElementsByTagName("label");

    Animation.Confirm(Language.GetCode("PuzzleApp.ConfirmCreateVersionApp"), function () {

        var data = "Class=PuzzleApp&Methode=CreateVersionApp&App=PuzzleApp";
        data += "&AppName=" + app[0].innerHTML;

        Request.Post("Ajax.php", data).then(data => {
            Animation.Notify(data);
        });
    });
};

/***
 * Création d'un nouvelle version du plugin
 * @returns {undefined}
 */
PuzzleApp.CreateVersionPlugin = function (e) {
    let container = e.srcElement.parentNode.parentNode;
    let app = container.getElementsByTagName("label");

    Animation.Confirm(Language.GetCode("PuzzleApp.ConfirmCreateVersionPlugin"), function () {

        var data = "Class=PuzzleApp&Methode=CreateVersionPlugin&App=PuzzleApp";
        data += "&AppName=" + app[0].innerHTML;

        Request.Post("Ajax.php", data).then(data => {
            Animation.Notify(data);
        });
    });
};

/***
 * Création d'un nouvelle version du template
 * @returns {undefined}
 */
PuzzleApp.CreateVersionTemplate = function (e) {
    let container = e.srcElement.parentNode.parentNode;
    let app = container.getElementsByTagName("label");

    Animation.Confirm(Language.GetCode("PuzzleApp.ConfirmCreateVersionTemplate"), function () {

        var data = "Class=PuzzleApp&Methode=CreateVersionTemplate&App=PuzzleApp";
        data += "&AppName=" + app[0].innerHTML;

        Request.Post("Ajax.php", data).then(data => {
            Animation.Notify(data);
        });
    });
};

PuzzleApp.Init = function(){
    
}
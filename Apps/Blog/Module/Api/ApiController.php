<?php

/**
 * Module d'accueil
 * */

namespace Apps\Blog\Module\Api;

use Core\Controller\Controller;
use Core\View\View;

use Apps\Blog\Entity\BlogCategory;
use Apps\Blog\Entity\BlogArticle;

class ApiController extends Controller { 
    
    /***
     * Return list of all Category
     */
    public function GetListCategory(){
        
        $category = new BlogCategory($this->Core);
        $categorys = $category->GetAll();
        
        return $category->ToAllArray($categorys);
    }
    
    /***
     * Return a list of article of a category
     */
    public function GetListArticles($code){
        
       $category = new BlogCategory($this->Core);
       $category = $category->GetByCode($code);
       
       $article = new BlogArticle($this->Core);
       $articles = $article->GetAll("CategoryId = " . $category->IdEntite);
       
       return $article->ToAllArray($articles);
    }
    
    /***
     * Get Detail Article
     */
    public function GetArticle($code){
      
       $article = new BlogArticle($this->Core);
       $article = $article->GetByCode($code);
       
       return $article->ToArray();
      
    }
}
{{form->Open()}}
{{form->Error()}}
{{form->Success()}}

{{form->Render(Id)}}


    <div>
        <label>{{GetCode(NewsLetter.Libelle)}}</label> 
        {{form->Render(Libelle)}}
    </div>
    
    <div>
        <label>{{GetCode(NewsLetter.Description)}}</label> 
        {{form->Render(Description)}}
    </div>
    
    <div>
        <label>{{GetCode(NewsLetter.Email)}}</label> 
        {{form->Render(EmailId)}}
    </div>
   
    <div class='center marginTop' >   
        {{form->Render(btnSaveCampagne)}}
    </div>  

{{form->Close()}}
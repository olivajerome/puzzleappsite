{{form->Open()}}
{{form->Error()}}
{{form->Success()}}

{{form->Render(Id)}}

    <div>
        <label>{{GetCode(Downloader.Name)}}</label> 
        {{form->Render(Name)}}
    </div>

    <div>
        <label>{{GetCode(Downloader.Description)}}</label> 
        {{form->Render(Description)}}
    </div>

    <div>
        <label>{{GetCode(Downloader.File)}}</label> 
        {{form->Render(Upload)}}
    </div>


    <div class='center marginTop' >   
        {{form->Render(btnSendRessource)}}
    </div>  

{{form->Close()}}
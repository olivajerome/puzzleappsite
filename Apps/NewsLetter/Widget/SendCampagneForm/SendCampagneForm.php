<?php

namespace Apps\NewsLetter\Widget\SendCampagneForm;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;

class SendCampagneForm extends Form {

    /**
     * Constructeur
     */
    function __construct($core = "", $campagneId = "") {
        $this->Core = $core;
        $this->Data = $data;

        //On le fait ici pour avoir le formuaire � l'affichage mais aussi � la validation
        $this->Init($campagneId);
    }

    /**
     * Initialisation 
     */
    function Init($campagneId) {
        $this->form = new Form("SendCampagneFormForm", $data);

        $this->form->SetView(__DIR__ . "/View/index.tpl", $this->Core);
        $this->form->SetAction("/View/index.tpl");

        $this->form->Add(array("Type" => "Hidden",
            "Id" => "Id",
        ));
        
        $this->form->Add(array("Type" => "Hidden",
            "Id" => "CampagneId",
            "Value" => $campagneId
        ));

        $this->form->Add(array("Type" => "EntityListBox",
            "Id" => "ListId",
            "Entity" => "Apps\NewsLetter\Entity\NewsLetterListeEmail",
            "Field" => "Libelle",
            "Validators" => ["Required"]
        ));
        
        $this->form->Add(array("Type" => "Button",
            "Id" => "btnSend",
            "Value" => $this->Core->GetCode("NewsLetter.Send"),
            "CssClass" => "btn btn-primary"
        ));
    }

    /*     * *
     * G�n�rer les formulaire
     */

    function Render() {
        return $this->form->Render();
    }

    /*     * *
     * Valide les donn�es selon les diff�rents formulaires
     */

    function Validate($data = "") {
        $errors = $this->form->Validate($this->Data);

        return $errors;
    }

    /*     * *
     * Charge les controls avec les donn�es de l'entit�
     */

    function Load($entity) {
        $this->form->Load($entity);
    }

    /*     * *
     * Charge l'entit� avec les donn�es des diff�rents formulaire
     */

    function Populate($entity) {
        
    }

    /*     * *
     * Gestion des erreurs
     */

    function Error() {
        $errors[] = $this->form->errors;

        return json_encode(array("statut" => "Error", "message" => $errors));
    }

}

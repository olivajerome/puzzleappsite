<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Downloader;

use Apps\Downloader\Entity\DownloaderRessource;
use Apps\Downloader\Entity\DownloaderRessourceContact;
use Apps\Downloader\Helper\RessourceHelper;
use Apps\Downloader\Helper\SitemapHelper;
use Apps\Downloader\Module\Front\FrontController;
use Apps\Downloader\Module\Admin\AdminController;
use Apps\Downloader\Module\Member\MemberController;
use Apps\Downloader\Module\Ressource\RessourceController;

use Apps\Base\Base;
use Core\Core\Core;
use Core\Core\Request;
use Core\Core\Response;
use Core\Utility\File\File;


class Downloader extends Base
{
    /**
     * Auteur et version
     * */
    public $Author = 'Eemmys';
    public $Version = '2.1.1.0';
    public static $Directory = "../Apps/Downloader";

    /**
     * Constructeur
     * */
    function __construct($core="")
    {
        $this->Core = Core::getInstance();
        parent::__construct($core, "Downloader");
    }

     /**
    * Add The Route
    */
    function GetRoute($route ="")
    {
      return parent::GetRoute(array("Download", "Documentation", "ShowAddRessource"));
    }

    /*     * *
     * Execute action after install
     */

     function PostInstall() {
        \Apps\EeApp\Helper\AppHelper::UpdateAppParams($this->Core,
                "Downloader",
                $this->Version,
                "Gestionnaire de téléchargement",
                0,
                0
        );

        \Apps\EeApp\Helper\AppHelper::CreateDataDir("Downloader");
    }

    /*
     * Download a Document
     */
    function Download($params)
    {
        $this->Core->MasterView->Set("Title", "Download");
         
        $frontController = new FrontController($this->Core);
        return $frontController->DownLoad($params);
    }
    
    /***
     * Documentaion d'une app
     */
    function Documentation($params){
        $this->Core->MasterView->Set("Title", "Documentation");
        
        $ressource = new DownloaderRessource($this->Core);
        $ressource = $ressource->GetByCode($params);
       
        $this->Core->MasterView->Set("Description", $ressource->Description->Value);
       
        
        $frontController = new FrontController($this->Core);
        return $frontController->Documentation($ressource);
    }
    
     /**
      * Execution de l'application
      */
    function Run()
    {
       echo parent::RunApp($this->Core, "Downloader", "Downloader");
    }
    
    
    /**
     * Get The siteMap 
     */
    public function GetSiteMap($url = false) {
        return SitemapHelper::GetSiteMap($this->Core, $url);
    }
    
    /*
     * Save the ressource
     */
    public function SaveRessource()
    {
        $adminController = new AdminController($this->Core);
        if($adminController->SaveRessource(Request::GetPosts())){
             echo Response::Success(array("message" => "Ressource.ressouceUploaded"));
        } else {
             echo Response::Error(array("message" => "Ressource.ressouceUploaded"));
        }
    }


    /***
     * Save User ressource 
     */
    public function SaveUserRessource(){
        $memberController = new MemberController($this->Core);
        if($memberController->SaveUserRessource(Request::GetPosts())){
             echo Response::Success(array("message" => "Ressource.ressouceUploaded"));
        } else {
             echo Response::Error(array("message" => "Ressource.ressouceUploaded"));
        }
    }
    
    /***
     * Delete a ressources 
     */
    public function DeleteRessource(){
        $adminController = new AdminController($this->Core);
        if($adminController->DeleteRessource(Request::GetPost("ressourceId"))){
             echo Response::Success(array("message" => "Ressource.ressouceDeleted"));
        } else {
             echo Response::Error(array("message" => "Ressource.ressouceDeleted"));
        }
    }

    /**
     * Sauvegare les images de presentation
     */
    function DoUploadFile($idElement, $tmpFileName, $fileName, $action)
    {
        $directory = "Data/Tmp";

        echo $action;
        switch($action)
        {
            case "SaveRessource":
 
                move_uploaded_file($tmpFileName, $directory . "/" . $fileName);
            
                echo Response::Success(array("type" => "DOCUMENT", "file" => $directory . "/". $fileName));
                return;
            case "SaveImage" :
              
                move_uploaded_file($tmpFileName, $directory . $folder . "/" . $idElement . ".jpg");

                 echo Response::Success(array("type:" => "IMAGE", "MESSAGE" => "OK"));
                return ;
        }

        return;
    }

    /*
     * Charge les ressources de l'utilisater
     */
    function LoadMyRessource()
    {
        $ressourceController = new RessourceController($this->Core);
        echo $ressourceController->LoadMyRessource();
    }

    /**
     * Affiche le lien d'une ressources avec un champ de saisie
     * d'email
     */
    function ShowRessource($ressourceId, $content)
    {
        $html = "<script type ='text/javascript' src='Apps/Downloader/Downloader.js' ></script>";
        $html .= "<span><p style='cursor:pointer' onclick='DownloaderAction.ShowEmailDownload(".$ressourceId.", this)' >".$content."</p><span>";

        return $html;
    }

    /**
     * Sauvegarde l'email pour la ressource
     */
    function SaveEmail()
    {
        //Recuperationde la ressource
        $ressource = new DownloaderRessource($this->Core);
        $ressource->GetById(Request::GetPost("ressourceId"));

        //Enregistrement du contact
        $contact = new DownloaderRessourceContact($this->Core);
        $contact->RessourceId->Value = $ressource->IdEntite;
        $contact->Email->Value =  Request::GetPost("email");
        $contact->Save();

        echo $ressource->Url->Value;
    }

    /**
     * Affiche les contacts de la ressource
     */
    function ShowContact()
    {
        $ressourceController = new RessourceController($this->Core);
        echo $ressourceController->ShowContact(Request::GetPost("RessourceId"));
    }

    /**
    * Obtient le nombre d element
    */
   public function GetNumber($entity)
   {
       $projet = new $entity($this->Core);
       return count($projet->GetAll());
   }
   
   /*
    * Retourne les ressources disponible en téléchargement
    */
   public function GetRessources()
   {
       return RessourceHelper::GetAll($this->Core);
   }
   
   /**
     * Obtient le widget spécifique
     */
    public function GetWidget($type = "", $params = "") {

        switch ($type) {
            case "DetailRessource" :
                $widget = new \Apps\Downloader\Widget\DetailRessource\DetailRessource($this->Core);
                break;
            case "UploaderRessource" :
                    $widget = new \Apps\Downloader\Widget\UploaderRessource\UploaderRessource($this->Core);
                    break;
        }

        return $widget->Render($params);
    }
    
    /*     * **
     * List of AddAble Widget on CMS
     */

    public function GetListWidget() {
        return array(array("Name" => "DetailRessource",
                "Description" => $this->Core->GetCode("Donloader.DescriptionDetailRessource"),
                "Parameters"  => $this->GetListRessource()
                ),
                array("Name" => "UploaderRessource",
                "Description" => $this->Core->GetCode("Donloader.DescriptionUploaderRessource"))
        );
    }
    
     /***
     * List of ressource
     */
    public function GetListRessource(){
        $list = new \Core\Control\EntityListBox\EntityListBox("Data", $this->Core);
        $list->Entity = "\Apps\Downloader\Entity\DownloaderRessource"; 
        $list->AddField("Name");
        return $list->Render();
    }
}
?>

<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\PuzzleApp\Helper;

use Core\Core\Core;
use Core\Utility\File\File;
use Apps\EeApp\Entity\EeAppApp;
use Apps\EeApp\Entity\EeAppPlugin;
use Apps\EeApp\Entity\EeAppTemplate;
use Apps\EeApp\Helper\UploadHelper;

class VersionningHelper {
    
    public const DownlaoderFolder = array(
        "Package" => 2,
        "Core" => 4,
        "Cms" => 5,
        "Base" => 7,
        "EeApp" => 8,
        "Blog" => 10,
        "Forum" => 11,
        "Form" => 12,
        "Profil" => 13,
        "Membre" => 14, 
        "Admin" => 15,
        "Ide" => 16,
        "Lang" => 17,
        "Carousel" => 18,
        "Annonce" => 19,
        "Notes" => 20,
        "Downloader" => 21,
        "Mooc" => 22,
        "RoadMap" => 23,
        "Sitemap" => 24,
        "Message" => 25,
        "VotePlugin" => 26,
        "CommentPlugin" => 27,
        "Notify" => 28,
        "Tips" => 29,
        "RatingPlugin" => 30,
        "SkillPlugin"  => 31,
        "WebAgency"    => 32
    );
    
    /*     * *
     * Créer une version du framework
     */

    public static function CreateVersionFramework($core) {
        $html = "Création d'une nouvelle version";
        $html .= "<ul>";
        $html .= "<li>1.Création du zip</li>";

        $route = explode("/", $_SERVER["DOCUMENT_ROOT"]);
        array_pop($route);

        $basePath = implode('/', $route);

        $folder = $basePath . "/Core/";
       echo $destination = "Data/Apps/Downloader/".VersionningHelper::DownlaoderFolder["Core"]."/Core.zip";

        self::CreateZip($core, $folder, $destination, $folder);

        $html .= "</ul>";

        return $html;
    }

    /*     * *
     * Crée une version du package d'installation
     */

    public static function CreateVersionPackage($core) {
        $html = "Création d'une nouvelle version";
        $html .= "<ul>";
        $html .= "<li>1.Création du zip</li>";
        $route = explode("/", $_SERVER["DOCUMENT_ROOT"]);
        array_pop($route);

        $basePath = implode('/', $route);
        $folder = $basePath;
        $destination = "Data/Apps/Downloader/1/PuzzleAppTmp";
        File::CreateDirectory($destination);
        //Dossier Web
        File::CreateDirectory($destination . "/Web");
        File::CreateDirectory($destination . "/Web/Data");
        File::CreateDirectory($destination . "/Web/Data/Tmp");

        copy($folder . "/autoload.php", $destination . "/autoload.php");
        copy($folder . "/environment.php", $destination . "/environment.php");
        copy($folder . "/.gitignore", $destination . "/.gitignore");

        copy($folder . "/Web/.htaccess", $destination . "/Web/.htaccess");
        copy($folder . "/Web/Ajax.php", $destination . "/Web/Ajax.php");
        copy($folder . "/Web/indexBase.php", $destination . "/Web/index.php");
        copy($folder . "/Web/script.php", $destination . "/Web/script.php");
        copy($folder . "/Web/style.php", $destination . "/Web/style.php");

        File::copyfolder($folder . "/Web/asset/", $destination . "/Web/asset/");
        File::copyfolder($folder . "/Web/css/", $destination . "/Web/css/");
        File::copyfolder($folder . "/Web/fonts/", $destination . "/Web/fonts/");

        //Dossier Apps avec les app de base
        File::CreateDirectory($destination . "/Apps");
        File::copyfolder($folder . "/Core/", $destination . "/Core/");
        File::copyfolder($folder . "/Apps/Base/", $destination . "/Apps/Base/");
        File::copyfolder($folder . "/Apps/EeApp/", $destination . "/Apps/EeApp/");
        File::copyfolder($folder . "/Apps/Cms/", $destination . "/Apps/Cms/");
        File::copyfolder($folder . "/Apps/Lang/", $destination . "/Apps/Lang/");
        File::copyfolder($folder . "/Apps/Admin/", $destination . "/Apps/Admin/");
        File::copyfolder($folder . "/Apps/Membre/", $destination . "/Apps/Membre/");
        File::copyfolder($folder . "/Apps/Ide/", $destination . "/Apps/Ide/");
        File::copyfolder($folder . "/Apps/Notify/", $destination . "/Apps/Notify/");
        File::copyfolder($folder . "/Apps/Api/", $destination . "/Apps/Api/");

        File::copyfolder($folder . "/View/", $destination . "/View/");

        //Config
        File::CreateDirectory($destination . "/Config");
        copy($folder . "/Config/empty.xml", $destination . "/Config/dev.xml");

        //Dossier de plugin et de template
        File::CreateDirectory($destination . "/Plugins");
        File::CreateDirectory($destination . "/Templates");

        //Dossier de Data
        File::CreateDirectory($destination . "/Web/Data");
        File::CreateDirectory($destination . "/Web/Data/Tmp");
        File::CreateDirectory($destination . "/Web/Data/Apps");

        //Création du zip
        $destination = $folder . "/Web/Data/Apps/Downloader/1/PuzzleAppTmp/";
        $destinationZip = $folder . "/Web/Data/Apps/Downloader/".VersionningHelper::DownlaoderFolder["Package"]."/puzzleApp.zip";
        self::CreateZip($core, $destination, $destinationZip, $destination);

        return $html;
    }

    /*     * *
     * Version de test totalement indépoendant
     */

    public static function CreateVersionTesting($core) {

        self::CreateVersionPackage($core);

        $route = explode("/", $_SERVER["DOCUMENT_ROOT"]);
        array_pop($route);

        $basePath = implode('/', $route);
        $folder = $basePath;

        $tmpSite = $folder . "/Web/Data/Apps/Downloader/1/PuzzleAppTmp/";
        File::copyfolder($tmpSite, "/var/www/PuzzleApp/puzzleapptest/");

        $request = "DROP DATABASE puzzlAppTest";
        $core->Db->Execute($request);

        $request = "CREATE DATABASE puzzlAppTest";
        $core->Db->Execute($request);
    }

    /*     * *
     * Copie les fichiers dans le dossier temporaire du downloader
     */

    public static function CopyFile() {
        
    }

    /*
     * Création du zip
     */

    public static function CreateZip($core, $folder, $destination, $basePath) {
        File::CreateZip($folder, $destination, $basePath);
    }

    /*     * *
     * Crée un nouvelle version d'une application
     */

    public static function CreateVersionApp($core, $app) {

        $html = "Création d'une nouvelle version de l'application " . $app;
        $html .= "<ul>";
        $html .= "<li>1.Création des fichiers de traduction</li>";

        $html .= "<li>1.1 traduction francais</li>";
        self::CreateLangFile($core, $app ,"Fr");
        $html .= "<li>1.1 traduction anglais</li>";
        self::CreateLangFile($core, $app ,"En");
        
        $html .= "<li>2.Mise à jour du numéro de version</li>";
        $html .= "<li>3.Création du Zip</li>";

        $route = explode("/", $_SERVER["DOCUMENT_ROOT"]);
        array_pop($route);

        $basePath = implode('/', $route);

        echo $folder = $basePath . "/Apps/" . $app . "/";
        echo $destination = "Data/Apps/Downloader/".VersionningHelper::DownlaoderFolder[$app]."/$app.zip";

        self::CreateZip($core, $folder, $destination, $folder);

        $html .= "</ul>";

        return $html;
    }

    /*     * *
     * Créer une version du plugin
     */

    public static function CreateVersionPlugin($core, $app) {
        $html = "Création d'une nouvelle version du plugin" . $app;
        $html .= "<ul>";

        $route = explode("/", $_SERVER["DOCUMENT_ROOT"]);
        array_pop($route);

        $basePath = implode('/', $route);

        echo $folder = $basePath . "/Plugins/" . $app . "/";
        echo $destination = "Data/Apps/Downloader/".VersionningHelper::DownlaoderFolder[$app]."/$app.zip";

        self::CreateZip($core, $folder, $destination, $folder);

        $html .= "</ul>";

        return $html;
    }

    /*     * *
     * Créer un version du template
     */

    public static function CreateVersionTemplate($core, $app) {
        $html = "Création d'une nouvelle version du template" . $app;
        $html .= "<ul>";

        $route = explode("/", $_SERVER["DOCUMENT_ROOT"]);
        array_pop($route);

        $basePath = implode('/', $route);

        echo $folder = $basePath . "/Templates/" . $app . "/";
        echo $destination = "Data/Apps/Downloader/".VersionningHelper::DownlaoderFolder[$app]."/$app.zip";

        self::CreateZip($core, $folder, $destination, $folder);

        $html .= "</ul>";

        return $html;
    }

    /*     * *
     * Crée le fichier de langue
     */

    public static function CreateLangFile($core, $app, $lang = "") {
        
        if($lang == ""){        
            $currentLang =  $core->GetLang();
        } else{
            $currentLang = $lang;
        }
        
        $request = 'Select langCode.Code as Code,langElement.Libelle as Libelle from ee_lang_code as langCode 
                   join ee_lang_element langElement on langElement.CodeId = langCode.id 
                   join ee_lang as lang on langElement.LangId = lang.Id
                   Where langCode.Code like "%' . $app . '%" and lang.Code ="'.$currentLang.'"';
                
        $result = $core->Db->GetArray($request);

        $data = json_encode(array("lang" => $currentLang , "data" => $result));

        $directory = "../Apps/" . $app . "/Lang";

        File::CreateDirectory($directory);

        File::SetFileContent($directory . "/lang".$currentLang .".json", $data);
    }

    /*     * *
     * Obtient les applications pour le store
     */

    public static function GetAppStore($core) {

       //Recuperation depuis le downloader     
        $apps = \Apps\Downloader\Helper\RessourceHelper::GetByCategory($core , 1);
        $plugins = \Apps\Downloader\Helper\RessourceHelper::GetByCategory($core , 2);
        $templates = \Apps\Downloader\Helper\RessourceHelper::GetByCategory($core , 3);
  
        return array("Apps" => $apps, "Plugins" => $plugins, "Templates" => $templates);
    }

    /*     * *
     * Met à jour le framework
     */

    public static function UpdateFramework($core) {

        $url = $core->Config->GetKey("puzzleAppUrl");
        $baseUrl = $url ."Downloader/Download/PuzzleApp";
        $package = file_get_contents($url);

        //Enregistrement temp
        file_put_contents("/Data/Tmp/puzzleApp.zip", $package);
        return UploadHelper::DoUpdateFramework($app, "/Data/Tmp/" . $app . ".zip", $package);
    }
}

<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Notes\Entity;

use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class NotesNote extends Entity {

    //Constructeur
    function __construct($core) {
        //Version
        $this->Version = "2.0.0.0";

        //Nom de la table 
        $this->Core = $core;
        $this->TableName = "NotesNote";
        $this->Alias = "NotesNote";

        $this->DateCreated = new Property("DateCreated", "DateCreated", DATEBOX, false, $this->Alias);
        $this->Text = new Property("Text", "Text", TEXTAREA, false, $this->Alias);
        $this->UserId = new Property("UserId", "UserId", NUMERICBOX, false, $this->Alias);
        $this->Visible = new Property("Visible", "Visible", NUMERICBOX, false, $this->Alias);
        $this->Status = new Property("Status", "Status", NUMERICBOX, false, $this->Alias);

        //Creation de l entité 
        $this->Create();
    }
}

?>
<?php

namespace Apps\Downloader\Decorator;

class RessourceDecorator{
    
    function RenderImages($entity){

      $documents = $entity->GetImages();
      $view ="";
          
       foreach($documents as $document){
     
           if(strpos($document, '.zip') === false && strpos($document, 'thumb') === false && strpos($document, 'full') === false ){
            $view .= "<div class='col-md-3' >";
            $view .= "<div class='block' style='background-color : #dedede'>"; 
            $view .= "<img style='width:100%' src='".$document."'/>";
            $view .= "</div></div>";
           }
       } 
                 
       return $view;
        
    }
    
}

Admin = function (){};


Admin.Init = function(){
    Event.AddById("btnAddWidget", "click", Admin.ShowAddWidget);
    Event.AddById("btnAddApp", "click", Admin.ShowAddApp);
    Event.AddById("btnDashBoard", "click", Admin.LoadDashBoard);
    Admin.InitAppUser(); };

/***
 * Init App User
 * @returns {undefined}
 */
Admin.InitAppUser = function(){
    Event.AddByClass("removeAppUser", "click", Admin.RemoveAppUser);
    Event.AddByClass("startAppUser", "click", Admin.StartAppUser);
};

/***
 * Delete User App
 * @param {type} e
 * @returns {Boolean}
 */
Admin.RemoveAppUser = function(e){
     e.preventDefault();
        e.stopPropagation();
        Dashboard.RemoveAppUser(e.srcElement.id, e.srcElement);
        return false;
};

/***
 * Lance une application
 * @returns {undefined}
 */
Admin.StartAppUser = function(e){
    Dashboard.StartApp('', e.srcElement.id, '');
};

/**
 * Dialogue d'ajout de widget aux dashboard
 */
Admin.ShowAddWidget = function(){
 
     Dialog.open('', {"title": Dashboard.GetCode("Admin.AddWidget"),
          "app": "Admin",
          "class": "DialogAdmin",
          "method": "ShowAddAppDasboard",
          "type": "right",
          "params" : ""
          });
};

/**
 * Charges les widgets de l'utilisateur
 */
Admin.LoadWidget = function(){

    var data = "Class=DashBoard&Methode=LoadWidget&App=Admin";
      
    Request.Post("Ajax.php", data).then(data => {
        
        let widgets = Dom.GetById("widgets");
        widgets.innerHTML = data;

        let widgetName = document.getElementsByClassName("widgetName");
        let widgetApp="";
        
        for(var i = 0; i < widgetName.length; i++){

            widgetApp = widgetName[i].value;
            eval(widgetApp+".InitAdminWidget()");

            Event.AddByClass("removeWidget", "click", Admin.RemoveWidget);
        }
    });
};

/***
 * Suppression d'un widget du tableau de bord Admin 
 */
Admin.RemoveWidget = function(e){

    let App = e.srcElement.dataset.app;

    Animation.Confirm(Language.GetCode("Admin.ConfirmRemoveWidget"), ()=>{

        var data = "Class=Admin&Methode=RemoveWidgetUser&App=Admin";
            data += "&appName="+App; 
      
        Request.Post("Ajax.php", data).then(data => {
            Admin.LoadWidget();          
        });

    });
};

/***
 * Start EeApp
 * @returns {undefined}
 */
Admin.ShowAddApp = function(){
     Dashboard.StartApp('', 'EeApp', '');
};

/**
 * Recherche le tableau de bord
 * @returns {undefined}
 */
Admin.LoadDashBoard = function ()
{
    var appCenter = document.getElementById("dvCenter");

    var data = "Class=Admin&Methode=LoadDashBoard&App=Admin";

    Request.Post("Ajax.php", data).then(data => {
        appCenter.innerHTML = data;

        Event.AddById("btnAddWidget", "click", Admin.ShowAddWidget);
        Admin.LoadWidget();
    });
};

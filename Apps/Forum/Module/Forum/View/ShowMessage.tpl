<section class='container' style='text-align:left'>
    <div class='row centerBlock'>
    <div id='dvMessage' class='col-md-12'>
        <table class='grid width100'>
            <tr>
                <th style='width:20%'></th>
                <th>{{Message->Title->Value}}</th>
            </tr>
            <tr>
                <td>
                    <p>{{Message->DateCreated->Value}}</p>
                    
                    <p>
                    <div class='imgProfil' style='background-image: url({{Message->GetImageUser()}}) '></div>
                          <b title='Contacter ce membre en MP' href='#'>{{Message->GetUser()}}</b>
                      </div>
                    </p>
                </td>
                <td>
                    <p>{{Message->Message->Value}}</p>
                </td>
            </tr>
            <tr>
                <th></th>
                <th>{{GetCode(Forum.Reponses)}}</th>

          
            </tr>
            {{foreach Reponses}}
                <tr>
                    <td>
                        <p> {{element->DateCreated->Value}}</p>
                        <div class='imgProfil' style='background-image: url({{element->GetImageUser()}}) '></div>
                         <b title='Contacter ce membre en MP' href='#'>{{element->GetUser()}}</b>
                    </td>
                    <td>
                       <p> {{element->Message->Value}}</p>
                       
                        {{element->GetVotePlugin()}}
                    </td>   
                    </td>
                </tr>
                   
            
            {{/foreach Reponses}}
        </table>
    </div>
        
    <div class='borderTop col-md-12'>
        
        {{if Connected == true}}
        
            {{if Model->State = Init}}
                <h2>{{GetCode(Forum.AddReponse)}}</h2>
            
                {{RenderModel()}}
            {{/if Model->State = Init}}

            {{if Model->State = Updated}}

                <div class='success'>
                  {{GetCode(Forum.MessageSaved)}}
                </div>

            {{/if Model->State = Updated}}
        {{/if Connected == true}}
        
        {{if Connected == false}}
        
             {{GetCode(Forum.MustBeConnected)}}
       
            <br/>
            <a class='btn btn-primary' href='{{GetPath(/singup)}}' >
                 {{GetCode(Singup)}}
             </a>
        
        {{/if Connected == false}}
        
    </div> 
    </div>
</section>

{{VotePlugin}}
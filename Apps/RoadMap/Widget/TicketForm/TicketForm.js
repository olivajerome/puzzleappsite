var TicketForm = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
TicketForm.Init = function(){
    Event.AddById("btnSaveTicket", "click", TicketForm.SaveTicket);
 
    let Description = document.querySelector("#ticketForm #Description");


    LayoutEditor.txtEditor = new TextRichEditor(Description,
        {tools: [ "ImageTool"],
            events: [{"tool": "SaveContent", "events": [{"type": "mousedown", "handler": LayoutEditor.SaveContent}]},
                {"tool": "ImageTool", "events": [{"type": "mousedown", "handler": LayoutEditor.OpenImageLibrary}]},

            ],
        }
);

    
    Comunity.Init();
};

/***
* Obtient l'id de l'itin�raire courant 
*/
TicketForm.GetId = function(){
    return Form.GetId("ticketForm");
};

/*
* Sauvegare l'itineraire
*/
TicketForm.SaveTicket = function(e){
   
    if(Form.IsValid("ticketForm"))
    {

    var data = "Class=RoadMap&Methode=SaveTicket&App=RoadMap";
        data +=  Form.Serialize("ticketForm");

        let columnId = Form.GetValue("ticketForm", "ColumnId");
        
        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.statut == "Error"){
                Form.RenderError("ticketForm", data.message);
            } else{

                Form.SetId("ticketForm", data.data.Id);
                
               //TODO A FAIRE CAR SINON on recharge tous RoadMap.AddOrUpdateTicket(data.data.Id);
                RoadMap.LoadRoadMap(null, RoadMapWidget.GetCurrentId());
                Dialog.Close();
            }
        });
    }
};


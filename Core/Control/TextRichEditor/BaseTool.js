class BaseTool {
    constructor(editor) {
        
        this.container = document.createElement("div");
        this.container.innerHTML = this.getIcone();
        this.container.title = this.getName();
        this.editor = editor;
        this.name = "";
        let instance = this;

        this.container.addEventListener("mousedown", (e) => {
            instance.execute(e);
        });
    }

    getIconeContainer() {
        return this.container;
    }

    findParentWithTag(node, tagName) {
        while (node && node !== document) {
            if (node.nodeName === tagName.toUpperCase()) {
                return node;
            }
            node = node.parentNode;
        }
        return null;
    }

    applyStyle(balise) {

        const selection = window.getSelection();
        if (!selection.rangeCount)
            return;

        const range = selection.getRangeAt(0);
        const selectedText = range.extractContents();

        //const balise = 'strong'; // Exemple: 'strong'
        const parentWithTag = this.findParentWithTag(selection.anchorNode, balise);

        let newNode;
        if (parentWithTag) {
            // Si un parent avec la balise spécifiée est trouvé, retirer cette balise en replaçant son contenu
            newNode = document.createDocumentFragment(); // Créer un fragment pour contenir le texte
            newNode.appendChild(selectedText); // Ajouter le texte sélectionné au fragment
            parentWithTag.parentNode.replaceChild(newNode, parentWithTag); // Remplacer la balise par le fragment
        } else {
            // Si la balise n'est pas trouvée, appliquer la balise au texte sélectionné
            newNode = document.createElement(balise);
            newNode.appendChild(selectedText);
            range.insertNode(newNode);
        }

        // Nettoyer et réinitialiser la sélection pour englober le nouveau noeud
        selection.removeAllRanges();
        const newRange = document.createRange();
        newRange.selectNodeContents(newNode);
        selection.addRange(newRange);

        return newNode;
    }

    addStyle(style, value) {
        const selection = window.getSelection();
        if (!selection.rangeCount)
            return;

        if (selection.anchorNode.parentNode.tagName == "DIV") {
            let newNode = this.applyStyle("span");
            newNode.style[style] = value;
            return newNode;
        } else {
            selection.anchorNode.parentNode.style[style] = value;
            return selection.anchorNode.parentNode;
        }
    }

    setEvent(events) {
        this.events = events;
    }
    execute(){
        this.editor.inputAreaChanged();
    }
}




{{foreach Actions}}

    <div class='chips' id='{{element->IdEntite}}'>
        {{element->Name->Value}}
        <i class='fa fa-edit editAction ' title='{{GetCode(Workflow.EditAction)}}'></i>
        <i class='fa fa-trash removeAction' title='{{GetCode(Workflow.RemoveAction)}}'></i>
    </div>

{{/foreach Actions}}
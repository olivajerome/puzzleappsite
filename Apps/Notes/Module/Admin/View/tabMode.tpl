<section class='container' id='tabPostIt'>
    
    
    {{foreach notes}}
        <div class='col-md-3'>
            <div class='block' style='height:250px'>
               <b>{{element->DateCreated->Value}}</b>
               <i class='fa fa-edit editNote' id='{{element->IdEntite}}' ></i>
               <p>{{element->Text->Value}}</p>
            </div>
        </div>
    {{/foreach notes}}

    
</section>
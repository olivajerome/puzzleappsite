  {{form->Open()}}
  {{form->Error()}}
  {{form->Success()}}

  {{form->Render(Id)}}

    <div>
       <label>{{GetCode(Seo.Url)}}</label> 
       {{form->Render(Url)}}
   </div>
   <div>
   <label>{{GetCode(Seo.PageTitle)}}</label> 
    {{form->Render(Title)}}
   </div>
   <div> 
   <label>{{GetCode(Seo.PageDescription)}}</label> 
    {{form->Render(Description)}}
   </div>
   
    <div class='center' >   
       {{form->Render(btnSavePage)}}
    </div>  


  {{form->Close()}}

<section class='container'>
    <h1>{{GetCode(Annonce.Category)}}</h1>
    
    
    <div class='row'>
    
    {{foreach Category}}
    
        <div class='col-md-3 block-first'>
            <a href='{{GetPath(/Annonce/Category/{{element->Code->Value}})}}'>
                <div  style='height:350px'>
                    <span style='display:block; width:100%; height: 100px; background-size: cover;background-image: url({{element->GetImagePath()}})'>
                    </span>

                    <h3 style="min-height:60px"> {{element->Name->Value}}
                    </h3>
                    <div style='height:130px;margin'>
                       <p>{{element->Description->Value}}</p>
                    </div>
                </div>
            </a>
       </div>
    {{/foreach Category}}
    
    </div>
</section>
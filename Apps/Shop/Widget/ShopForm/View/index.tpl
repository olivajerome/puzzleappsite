{{form->Open()}}
{{form->Error()}}
{{form->Success()}}

{{form->Render(Id)}}

    <h2>{{GetCode(Shop.Configuration)}}</h2>
    
    <div>
        <label>{{GetCode(Shop.Name)}}</label> 
        {{form->Render(Name)}}
    </div>

    <div>
        <label>{{GetCode(Shop.Description)}}</label> 
        {{form->Render(Description)}}
    </div>
    <div class='center marginTop' >   
        {{form->Render(btnSave)}}
    </div>  

    <div class='marginTop borderTop'>
        <h2>{{GetCode(Shop.Plugin)}}</h2>
        <div class='right'>
            <button id='btnAddPlugin'  class='btn btn-primary' >{{GetCode(Shop.AddPlugin)}}</button>
        </div>

        <div id='lstPlugin' class='right'>
            {{foreach Plugins}}   
                <div class='chips'>

                    {{element->PluginName->Value}}

                    <i id='{{element->IdEntite}}' class='fa fa-trash removePlugin'></i>


                </div>
            {{/foreach Plugins}}   
        </div>    
    </div>
    
{{form->Close()}}
var UserForm = function(){};

/***
 * Init the form
 * @returns {undefined}
 */
UserForm.Init = function(){
    Event.AddById("btnSave", "click", UserForm.SaveElement);
};

/***
* Get the Id of element 
*/
UserForm.GetId = function(){
    return Form.GetId("UserFormForm");
};

/*
* Save the Element
*/
UserForm.SaveElement = function(e){
   
    if(Form.IsValid("UserFormForm"))
    {

    let data = "Class=UserForm&Methode=SaveElement&App=UserForm";
        data +=  Form.Serialize("UserFormForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.status == "error"){
                Form.RenderError("UserFormForm", data.data.message);
            } else{

                Form.SetId("UserFormForm", data.data.Id);
            }
        });
    }
};


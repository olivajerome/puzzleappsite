<?php 
/*
* PuzzleApp
* Webemyos
* Jérôme Oliva
* GNU Licence
*/

namespace Apps\WorkFlow\Entity;
use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

use Apps\WorkFlow\Entity\WorkFlowTrigger; 
use Apps\WorkFlow\Entity\WorkFlowAction;

use Apps\WorkFlow\Decorator\WorkFlowDecorator;


class WorkFlowWorkFlow extends Entity  
{
	//Constructeur
	function __construct($core)
	{
		//Version
		$this->Version ="2.0.0.0"; 

		//Nom de la table 
		$this->Core=$core; 
		$this->TableName="WorkFlowWorkFlow"; 
		$this->Alias = "WorkFlowWorkFlow"; 

		$this->Name = new Property("Name", "Name", TEXTBOX,  false, $this->Alias); 
		$this->Description = new Property("Description", "Description", TEXTAREA,  false, $this->Alias); 
		$this->Status = new Property("Status", "Status", NUMERICBOX,  false, $this->Alias); 

		//Creation de l entité 
		$this->Create(); 
	}

	/***
	 * Get The trigger
	 */
	function GetTrigger(){

		$trigger = new WorkFlowTrigger($this->Core);
		$triggers = $trigger->Find("WorkFlowId=" . $this->IdEntite);
		
		if(count($triggers) > 0){
			return $triggers[0]->Event->Value;
		}

		return "";
	}

	/***
	 * Get Action on the WorkFlow
	 */
	function GetAction(){

		$decorator = new WorkFlowDecorator($this->Core);
		return $decorator->RenderActions($this);

	}
}
?>
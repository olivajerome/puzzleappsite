<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Notes\Module\Admin;

use Core\Control\Button\Button;
use Core\Controller\AdministratorController;
use Core\View\View;
use Core\View\ElementView;
use Core\Control\TabStrip\TabStrip;
use Core\Control\EntityGrid\EntityGrid;
use Core\Control\EntityGrid\EntityColumn;
use Core\Control\EntityGrid\EntityIconColumn;
use Core\Control\EntityGrid\EntityFunctionColumn;
use Core\Utility\Date\Date;
use Apps\Notes\Widget\NotesNoteForm\NotesNoteForm;
use Apps\Notes\Entity\NotesNote;

/*
 * 
 */

class AdminController extends AdministratorController {

    

    /**
     * Create
     */
    function Create() {
        
    }

    /**
     * Init
     */
    function Init() {
        
    }

    /**
     * Render
     */
    function Show($all = true) {
        return $this->Index();
    }

    /*
     * Get the home page
     */

    function Index() {
        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);

        $tabNote = new TabStrip("tbNote", "Notes");
        $tabNote->AddTab($this->Core->GetCode("Notes.postIt"), $this->GetTabTabNote());
        $tabNote->AddTab($this->Core->GetCode("Notes.grid"), $this->GetTabNote());
        $view->AddElement(new ElementView("tabNote", $tabNote->Render()));

        return $view->Render();
    }
    
    /***
     * Mode tableau
     */
    function GetTabTabNote(){
        $view = new View(__DIR__ . "/View/tabMode.tpl", $this->Core);
        $notes = new NotesNote($this->Core);

        
        $view->AddElement(new ElementView("notes", $notes->GetAll()));

        return $view->Render();
    }
    

    /*     * *
     * Notes
     */

    function GetTabNote() {

        $gdNote = new EntityGrid("gdNote", $this->Core);
        $gdNote->Entity = "Apps\Notes\Entity\NotesNote";
        $gdNote->App = "Note";
        $gdNote->Action = "GetTabNote";

        $btnAdd = new Button(BUTTON, "btnAddNote");
        $btnAdd->Value = $this->Core->GetCode("Notes.AddNote");

        $gdNote->AddButton($btnAdd);
        $gdNote->AddColumn(new EntityColumn("DateCreated", "DateCreated"));
        $gdNote->AddColumn(new EntityColumn("Text", "Text"));

        $gdNote->AddColumn(new EntityIconColumn("Action",
                        array(array("EditIcone", "Notes.EditNote", "Notes.EditNote"),
                    array("DeleteIcone", "Notes.DeleteNote", "Notes.DeleteNote"),
                        )
        ));

        return $gdNote->Render();
    }

    /*     * **
     * Save/Update a note
     */

    function SaveNote($data) {

        $noteForm = new NotesNoteForm($this->Core, $data);

        if ($noteForm->Validate($data)) {

            $note = new NotesNote($this->Core);

            if ($data["Id"]) {
                $note->GetById($data["Id"]);
            } else {
                $note->DateCreated->Value = Date::Now();
                $note->UserId->Value = $this->Core->User->IdEntite;
                $note->Status->Value = 1;
            }

            $noteForm->Populate($note);
            $note->Save();

            return true;
        } else {
            return $noteForm->errors;
        }
    }

    /* action */
}

?>
<div class="block" style='min-height: 500px'>
    
    <div style='background-image: url({{Ressource->GetImageFullPath()}}); width= 100%; height:200px;background-size:cover'  ></div>
    
    <h2>{{Ressource->Name->Value}}</h2>
    
    <p>{{Ressource->Description->Value}}</p>
    
    <div class='center'>
        <a class='btn btn-primary' href='{{Ressource->GetDownloadPath()}}'>
            {{GetCode(Downloader.Download)}}
        </a><br/>

        <a href='{{Ressource->GetDoocPath()}}'>
            {{GetCode(Downloader.Documentation)}}
        </a>
    </div>
</div>

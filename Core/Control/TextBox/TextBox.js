var TextBox = new TextBox();


function TextBox()
{
	this.Verify = function(control,exp,message)
	{
		var Expression = new RegExp(exp);
		if(control.value.length!=0 && !Expression.test(control.value))
                {
			alert(message);
                        control.value = "";
                        control.innerHTML = "";
                }
	};
};



TextBox.VerifyTel = function(value)
{

  	var Expression = new RegExp("^0[1-9]([-. ]?[0-9]{2}){4}$");
	return !Expression.test(value);
};  

TextBox.VerifyNumber = function(control)
{
    control.value = control.value.replace(/[^0-9.]/g,'');
};

TextBox.VerifyEmpty = function(control)
{
    if(control.value == 0)
    {
        alert("Merci de saisir un chiffre supérieur à 0");
         control.value = "";
    }
};



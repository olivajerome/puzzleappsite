


var MemberProjetWidget = function(){};

/***
 * Initialisation du widget
 * @returns {undefined}
 */
MemberProjetWidget.Init = function(){

  var hdComunityId = document.getElementById("hdComunityId");
      MemberProjetWidget.projetId = hdComunityId.value;
  
  var lstMembre  = document.getElementById("lstMembre");
  
  Dashboard.AddEventByClass("contactMember", "click", MemberProjetWidget.ContactMember);
  
};

/***
 * Recharge les membres du projet
 * @returns {undefined}
 */
MemberProjetWidget.LoadMemberProjet = function()
{
    var data = "App=ParlonsEn&Methode=LoadMemberProjet";
        data += "&projetId=" +   MemberProjetWidget.projetId ;
     var url = window.location.origin + "/Ajax.php";

     Request.Post(url, data).then(data=>{
         
           lstMembre.parentNode.innerHTML = data;
            
           MemberProjetWidget.Init();
    }); 
};

/***
 * popup de contact d'un membre 
 * @param {type} e
 * @returns nothing
 */
MemberProjetWidget.ContactMember = function(e){

        ParlonsEn.ShowOverlay();
    
        var hdComunityId = document.getElementById("hdComunityId");
          
        this.core = document.createElement('div');
        this.core.id = "tchatMessage";
        this.core.style.position='fixed';
        this.core.style.width= "400px";
        this.core.style.height="550px";
        this.core.style.overflow="auto";
        this.core.style.border='1px solid grey';
        this.core.style.padding="5px";
        this.core.style.right= "0px";
        this.core.style.bottom= "0px";
        this.core.style.backgroundColor="white";
        this.core.style.zIndex = 9999;

        this.core.innerHTML="<div style='text-align:right;width:100%'><i class='fa fa-remove' alt='' title='Fermer' onclick='CloseTool(this); ParlonsEn.HideOverlay()'></i></div>";

        this.core.innerHTML += "<span>Chargement...</span>";

         var data = "App=ParlonsEn&Methode=GetTchatMessage";
          data += "&userId="+ e.srcElement.id;

        var url = window.location.origin + "/Ajax.php";

        Request.Post(url, data).then(data=>{ 

            var content = this.core.getElementsByTagName("span");
            content[0].innerHTML = data;

            Dashboard.AddEventById("btnSendTchatMessage", "click", ActionProjetWidget.SendTchatMessage );
            
            var tbTchatMessage = document.getElementById("tbTchatMessage");
            tbTchatMessage.focus();
            
            ActionProjetWidget.ScrollBottom();
        });

        document.body.appendChild(this.core);
        
    return ;
};
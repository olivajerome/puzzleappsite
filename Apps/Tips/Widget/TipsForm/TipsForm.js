var TipsForm = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
TipsForm.Init = function(callBack){
    Event.AddById("btnSaveTips", "click", TipsForm.SaveElement);
    
    
    let content = document.querySelector("#TipsForm #Description");
    
    TipsForm.txtEditor = new TextRichEditor(content,
            {tools: ["SourceCode" , "HtmlToCode"],
                events: [{"tool": "ImageTool", "events": [{"type": "mousedown", "handler": MoocLessonForm.OpenImageLibrary}]}],
            }
    );
    
    TipsForm.CallBack = callBack;
};

/***
* Obtient l'id de l'itin�raire courant 
*/
TipsForm.GetId = function(){
    return Form.GetId("TipsFormForm");
};

/*
* Sauvegare l'itineraire
*/
TipsForm.SaveElement = function(e){
   
    if(Form.IsValid("TipsForm"))
    {
    let data = "Class=Tips&Methode=SaveTips&App=Tips";
        data +=  Form.Serialize("TipsForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.statut == "Error"){
                Form.RenderError("TipsForm", data.message);
            } else{

                Form.SetId("TipsForm", data.data.Id);

                Dialog.Close();

                if (TipsForm.CallBack) {
                    TipsForm.CallBack();
                }
            }
        });
    }
};


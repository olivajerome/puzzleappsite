var NewsLetter = function () {};

/*
 * Chargement de l'application
 */
NewsLetter.Load = function (parameter)
{
    this.LoadEvent();
};

/*
 * Chargement des �venements
 */
NewsLetter.LoadEvent = function ()
{
    Dashboard.AddEventAppMenu(NewsLetter.Execute, "", "NewsLetter");
    Dashboard.AddEventWindowsTool("NewsLetter");

    NewsLetter.LoadAdmin();
};

/*
 * Execute une fonction
 */
NewsLetter.Execute = function (e)
{
    //Appel de la fonction
    Dashboard.Execute(this, e, "NewsLetter");
    return false;
};

/*
 *	Affichage de commentaire
 */
NewsLetter.Comment = function ()
{
    Dashboard.Comment("NewsLetter", "1");
};

/*
 *	Affichage de a propos
 */
NewsLetter.About = function ()
{
    Dashboard.About("NewsLetter");
};

/*
 *	Affichage de l'aide
 */
NewsLetter.Help = function ()
{
    Dashboard.OpenBrowser("NewsLetter", "{$BaseUrl}/Help-App-NewsLetter.html");
};

/*
 *	Affichage de report de bug
 */
NewsLetter.ReportBug = function ()
{
    Dashboard.ReportBug("NewsLetter");
};

/*
 * Fermeture
 */
NewsLetter.Quit = function ()
{
    Dashboard.CloseApp("", "NewsLetter");
};


NewsLetter.Init = function () {

};

/**
 * Init Admin App
 */
NewsLetter.LoadAdmin = function () {

    NewsLetterConfigForm.Init();
    NewsLetter.InitTabEmailBase();
    NewsLetter.InitTabListEmail();
    NewsLetter.InitTabCampagne();
};

/**
 * Tab des Email de base
 * @returns {undefined}
 */
NewsLetter.InitTabEmailBase = function () {

    Event.AddById("btnAddEmailBase", "click", () => {
        NewsLetter.ShowAddEmailBase("");
    });

    EntityGrid.Initialise('gdEmailBase');
    EntityGrid.Initialise('gdEmailProgram');
    EntityGrid.Initialise('gdEmailEvent');
};

/**
 * Dialogue for add Email
 */
NewsLetter.ShowAddEmailBase = function (emailId) {

    Dialog.open('', {"title": Dashboard.GetCode("NewsLetter.AddEmail"),
        "app": "NewsLetter",
        "class": "DialogAdminNewsLetter",
        "method": "AddEmailBase",
        "type": "left",
        "params": emailId
    });
};

/****
 * Edite une catégorie
 * @param {type} categoryId
 * @returns {undefined}
 */
NewsLetter.EditEmail = function (emailId) {
    NewsLetter.ShowAddEmailBase(emailId);
};

/***
 * Supprime une catégorie
 * @param {type} categoryId
 * @returns {undefined}
 */
NewsLetter.DeleteEmail = function (emailId) {

    Animation.Confirm(Language.GetCode("NewsLetter.ConfirmRemoveEmail"), () => {

        let data = "Class=NewsLetter&Methode=DeleteEmail&App=NewsLetter";
        data += "&id=" + emailId;

        Request.Post("Ajax.php", data).then(data => {
            NewsLetter.ReloadEmailBase(data);
        });
    });
};

/***
 * Rafraichit Tha base Email Tab
 * @returns {undefined}
 */
NewsLetter.ReloadEmailBase = function (data) {

    switch (data.data.typeId) {
        case "1":
            dataGrid = Dom.GetById("gdEmailBase");
            dataGrid.parentNode.innerHTML = data.data.html;
            EntityGrid.Initialise('gdEmailBase');
            break;

        case "2" :
            dataGrid = Dom.GetById("gdEmailProgram");
            dataGrid.parentNode.innerHTML = data.data.html;
            EntityGrid.Initialise('gdEmailProgram');
            break;
        case "3" :
            dataGrid = Dom.GetById("gdEmailEvent");
            dataGrid.parentNode.innerHTML = data.data.html;
            EntityGrid.Initialise('gdEmailEvent');
            break;

    }
};

/***
 * Init tab list Email
 * @returns {undefined}
 */
NewsLetter.InitTabListEmail = function () {

    Event.AddById("btnAddListEmail", "click", () => {
        NewsLetter.ShowAddListEmail("");
    });

    EntityGrid.Initialise('gdListEmail');
};

/**
 * Dialogue for add list Email
 */
NewsLetter.ShowAddListEmail = function (listId) {

    Dialog.open('', {"title": Dashboard.GetCode("NewsLetter.AddListEmail"),
        "app": "NewsLetter",
        "class": "DialogAdminNewsLetter",
        "method": "AddListEmail",
        "type": "left",
        "params": listId
    });
};

/****
 * Edite une liste d'email
 * @param {type} categoryId
 * @returns {undefined}
 */
NewsLetter.EditListEmail = function (listId) {
    NewsLetter.ShowAddListEmail(listId);
};

/***
 * Rafraichit Tha base Email Tab
 * @returns {undefined}
 */
NewsLetter.ReloadListEmail = function (data) {

    let dataGrid = Dom.GetById("gdListEmail");
    dataGrid.parentNode.innerHTML = data.data.html;
    
    EntityGrid.Initialise('gdListEmail');
};

/***
 * Supprime une catégorie
 * @param {type} categoryId
 * @returns {undefined}
 */
NewsLetter.DeleteListEmail = function (emailId) {

    Animation.Confirm(Language.GetCode("NewsLetter.ConfirmRemoveListEmail"), () => {

        let data = "Class=NewsLetter&Methode=DeleteListEmail&App=NewsLetter";
        data += "&id=" + emailId;

        Request.Post("Ajax.php", data).then(data => {
            NewsLetter.ReloadListEmail(JSON.parse(data));
        });
    });
};

/***
 * Init campagne tab
 * @returns {undefined}
 */
NewsLetter.InitTabCampagne = function(){
     Event.AddById("btnAddCampagne", "click", () => {
        NewsLetter.ShowAddCampagne("");
    });

    EntityGrid.Initialise('gdCampagne');
};

/**
 * Dialogue for add list Email
 */
NewsLetter.ShowAddCampagne = function (campagneId) {

    Dialog.open('', {"title": Dashboard.GetCode("NewsLetter.AddCampagne"),
        "app": "NewsLetter",
        "class": "DialogAdminNewsLetter",
        "method": "AddCampagne",
        "type": "left",
        "params": campagneId
    });
};

/****
 * Edite une campagne
 * @param {type} campagneId
 * @returns {undefined}
 */
NewsLetter.EditCampagne = function (campagneId) {
    NewsLetter.ShowAddCampagne(campagneId);
};

/***
 * Supprime une campagne
 * @param {type} categoryId
 * @returns {undefined}
 */
NewsLetter.DeleteCampagne = function (campagneId) {

    Animation.Confirm(Language.GetCode("NewsLetter.ConfirmRemoveCampagne"), () => {

        let data = "Class=NewsLetter&Methode=DeleteCampagne&App=NewsLetter";
        data += "&id=" + campagneId;

        Request.Post("Ajax.php", data).then(data => {
            NewsLetter.ReloadCampagne(JSON.parse(data));
        });
    });
};


/***
 * Rafraichit Tha base Campagne Tab
 * @returns {undefined}
 */
NewsLetter.ReloadCampagne = function (data) {

    let dataGrid = Dom.GetById("gdCampagne");
    dataGrid.parentNode.innerHTML = data.data.html;
    
    EntityGrid.Initialise('gdCampagne');
};

/**
 * SendCampagne To a list of Email
 */
NewsLetter.SendCampagne = function (campagneId) {

    Dialog.open('', {"title": Dashboard.GetCode("NewsLetter.SendCampagne"),
        "app": "NewsLetter",
        "class": "DialogAdminNewsLetter",
        "method": "SendCampagne",
        "type": "left",
        "params": campagneId
    });
};


/***
 * Refresh the plugin
 * @returns {undefined}
 */
NewsLetter.RefreshPlugin =function(){
    Dashboard.StartApp('', 'NewsLetter', '')
};
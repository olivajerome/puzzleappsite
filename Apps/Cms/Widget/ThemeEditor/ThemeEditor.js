var ThemeEditor = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
ThemeEditor.Init = function(){
    Event.AddById("btnSave", "click", ThemeEditor.SaveElement);
};

/***
* Obtient l'id de l'itin�raire courant 
*/
ThemeEditor.GetId = function(){
    return Form.GetId("ThemeEditorForm");
};

/*
* Sauvegare l'itineraire
*/
ThemeEditor.SaveElement = function(e){
   
    if(Form.IsValid("ThemeEditorForm"))
    {

    var data = "Class=ThemeEditor&Methode=SaveElement&App=ThemeEditor";
        data +=  Form.Serialize("ThemeEditorForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.statut == "Error"){
                Form.RenderError("ThemeEditorForm", data.message);
            } else{

                Form.SetId("ThemeEditorForm", data.data.Id);
            }
        });
    }
};


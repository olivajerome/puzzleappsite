var VotePlugin = function () {};

VotePlugin.Init = function () {

    Event.AddBySelector(".votePlugin .addPlus", "click", function (e) {
        VotePlugin.AddVote(e, '1');
    });
    Event.AddBySelector(".votePlugin .addMinus", "click", function (e) {
        VotePlugin.AddVote(e, '-1');
    });

};

VotePlugin.AddVote = function (e, vote) {

    let container = e.srcElement.parentNode.parentNode;

    Plugin.SendRequest({"plugin": "VotePlugin",
        "method": "Vote",
        "EntityName": container.dataset.entity,
        "EntityId": container.dataset.id,
        "Vote": vote}, (data) => {

        let reponse = JSON.parse(data);

        if (reponse.status == "error") {
            Animation.Notify(Language.GetCode("Base.YouMustBeConnected"));
        } else {
            let Stat = reponse.data;
            let Plus = container.getElementsByClassName("plus")[0];
            let Minus = container.getElementsByClassName("minus")[0];

            Plus.innerHTML = Stat.plus;
            Minus.innerHTML = Stat.minus;
        }
    });
};
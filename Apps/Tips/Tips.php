<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Tips;

use Core\Core\Core;
use Core\App\Application;
use Core\Core\Request;

use Apps\Tips\Helper\TipsHelper;

class Tips extends Application {

    /**
     * Auteur et version
     * */
    public $Author = 'Webemyos';
    public $Version = '1.0.0.0';

    /**
     * Constructeur
     * */
    function __construct($core) {
        parent::__construct($core, "Tips");
        $this->Core = $core;
    }

    /**
     * Execution de l'application
     */
    function Run() {
        echo parent::RunApp($this->Core, "Tips", "Tips");
    }
    
    /*
     * Doit on afficher une nouvelle astuce au membre connecté
     */
    function haveTip(){
        return TipsHelper::HaveTips($this->Core);
    }

    /*
    * Save Tips
    */
    function SaveTips(){
        return TipsHelper::SaveTips($this->Core, Request::GetPosts());
    }

    /***
     * Delete a Tips
     */
    function DeleteTips(){
        return TipsHelper::DeleteTips($this->Core, Request::GetPost("tipsId"));
    }
}

?>
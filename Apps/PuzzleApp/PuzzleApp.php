<?php


/**
 * Description of PuzzleApp
 *
 * @author jerome
 */

namespace Apps\PuzzleApp;

use Apps\Base\Base;
use Apps\PuzzleApp\Module\Front\FrontController;
use Apps\PuzzleApp\Helper\VersionningHelper;
use Core\Core\Core;
use Core\Core\Request;
use Core\View\View;

/**
 * Description of Base
 *
 * @author jerome
 */
class PuzzleApp extends Base
{
    /**
    * Constructeur
    */
   function __construct($core="")
   {
       $this->Core = Core::getInstance();
       parent::__construct($this->Core, "PuzzleApp");
   }

   /**
    * Add The Route
    */
   function GetRoute($route ="")
   {
      return parent::GetRoute(array("Store", "Temoignages", "Community" , "GetDemoActionMenu"));
   }

   /**
     * Execution de l'application
     */
    function Run() {
      echo parent::RunApp($this->Core, "PuzzleApp", "PuzzleApp");
  }

   /*
    * Home page
    */
   public function Index()
   {
      $this->Core->MasterView->Set("Title", "Le framework qui vous permet de tout faire sans coder");
      $this->Core->MasterView->Set("Description", "Vous allez lancer un site, bravo. Mais que vous faut il ? Un Cms, un blog, un site Ecommerce ? Ne cherchez plus avec PuzzleApp vos commencer avec une base simple puis vous ajoutez ce que vous avez besoin au fur et à mesure. PuzzleApp fait à peu prêt tout sauf le café.");

      $frontController = new FrontController($this->Core);
      return $frontController->Index();
   }
   
    /*
    * Get The contact Page
   */
   /*function Contact()
   {
      $this->Core->MasterView->Set("Title", $this->Core->GetCode("ContactUs"));
      $this->Core->MasterView->Set("Description", "Une question, envie de discuter musique ou nous présenter vos objets connectés, vos instruments ou simplement envie de nous dire un petit mot ? Contactez nous.");
      
      if(Request::IsPost())
      {
        $email = Request::GetPost("tbEmail");
        $name = Request::GetPost("tbName");
        $message = Request::GetPost("tbMessage");
        
        mail("jerome.oliva@gmail.com",
             "Nouveau message du pupitre digital", 
             " Email : ". $email . " Nom :" .$name . "Message : " .$message );
        
        return "<div class='col-md-12' >Merci pour votre message, nous vous répondrons rapidement.</div>";
      }
      else
      {
        return parent::contact();
      }
   }
   */
  
   /*
    * Store du framework et des applications
    */
   function Store()
   {
      $this->Core->MasterView->Set("Title", $this->Core->GetCode("LeStore"));
      $this->Core->MasterView->Set("Description", "Besoin d'un forum, d'un blog, d'un système d'envoi d'email ? Trouvez votre application dans notre store. Faite les évoluer selon votre besoin ou métier. ");
    
      $frontController = new FrontController($this->Core);
      return $frontController->Store();
   }

    /**
     * Les des avis su rle site
     */
    function Temoignages()
    {
      $this->Core->MasterView->Set("Title", "Temoignages");
      $this->Core->MasterView->Set("Description", "PuzzleApp, ce sont les utilisateurs qui en parlent le mieux.Retrouvez tout leur temoignagnes et ce que puzzleApp leur a apporté.");
    
      $frontController = new FrontController($this->Core);
      return $frontController->Temoignages($token);
    }
    
    //Acces au reseau social et aux foruls
    function Community()
    {
      $this->Core->MasterView->Set("Title", "La communauté");
      $this->Core->MasterView->Set("Description", "PuzzleApp, ce sont les utilisateurs qui en parlent le mieux.Retrouvez tout leur temoignagnes et ce que puzzleApp leur a apporté.");

      $frontController = new FrontController($this->Core);
      return $frontController->Community($token);
    
    }

    /***
     * Crée une nouvelle version du framework
     */
  function CreateVersionFramework(){
     return VersionningHelper::CreateVersionFramework($this->Core);
  }

  /***
   * Créer une version du package d'installation
   */
  function CreateVersionPackage(){
      return VersionningHelper::CreateVersionPackage($this->Core);
  }
  
  /***
   * Créer une version du package d'installation
   */
  function CreateVersionTesting(){
      return VersionningHelper::CreateVersionTesting($this->Core);
  }
  
  /***
   * Crée une nouvelle version du framework
   */
  function CreateVersionApp(){
    return VersionningHelper::CreateVersionApp($this->Core, Request::GetPost("AppName"));
  }
  
   /***
   * Crée une nouvelle version du plugin
   */
  function CreateVersionPlugin(){
    return VersionningHelper::CreateVersionPlugin($this->Core, Request::GetPost("AppName"));
  }

   /***
   * Crée une nouvelle version du template
   */
  function CreateVersionTemplate(){
    return VersionningHelper::CreateVersionTemplate($this->Core, Request::GetPost("AppName"));
  }
  
  /***
   * Obtient les app por le store
   */
  function GetAppStore(){
    return  json_encode(VersionningHelper::GetAppStore($this->Core));
  }

  /***
   * Met à jour le frameworek depuis le store
   */
  function UpdateFramework(){
    return VersionningHelper::UpdateFramework($this->Core);
  }

  function GetWidget($type = "", $params = ""){

    switch($type){
      case "Control" : 
        $widget = new \Apps\PuzzleApp\Widget\RenderControl\RenderControl($this->Core);
        break;
      case "Animation" : 
        $widget = new \Apps\PuzzleApp\Widget\RenderAnimation\RenderAnimation($this->Core);
        break;
    
    }

    return $widget->Render($params);
  }
  
  /***
   * Demo Menu
   */
  function GetDemoActionMenu(){
      $view = "<ul>";
      $view .= "<li id='mn1'>Menu 1</li>";
      $view .= "<li id='mn2'>Menu 2</li>";
      $view .= "</ul>";
      
      return $view;
  }
  
  /***
   * Data 
   */
  function GetInfoForRequest(){
      echo "Data recevied : This is reponse by the Server :)" ;
  }
}

<section class='container'>
    <div class="block content-panel">
        <hr>
        <table class="table">
            <thead>
                <tr>
                    <th>{{GetCode(Application)}}</th>
                    <th colspan="2">{{GetCode(Message)}}</th>
            </tr>
            </thead>
            <tbody>
                {{foreach}}
                <tr>
                    <td>{{element->AppName->Value}}</td>
                    <td>{{element->GetMessage()}}</td>
                    <td>
                        <input type='button' class='btn btn-primary' value="{{GetCode(Notify.ShowDetail)}}" onclick="Dashboard.StartApp('', '{{element->AppName->Value}}','Membre')" />
                    </td>    
                </tr>
                {{/foreach}}            
            </tbody>
        </table>
    </div>
</div>
</section>

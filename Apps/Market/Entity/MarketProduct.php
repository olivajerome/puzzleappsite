<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Market\Entity;

use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;
use Core\Control\NoteBox\NoteBox;

class MarketProduct extends Entity {


    const VISIBLE =1;
    const SEALED = 2;


    //Constructeur
    function __construct($core) {
        //Version
        $this->Version = "2.0.0.0";

        //Nom de la table 
        $this->Core = $core;
        $this->TableName = "MarketProduct";
        $this->Alias = "MarketProduct";

        $this->Title = new Property("Title", "Title", TEXTBOX, false, $this->Alias);
        $this->Code = new Property("Code", "Code", TEXTBOX, false, $this->Alias);
        $this->Description = new Property("Description", "Description", TEXTAREA, false, $this->Alias);
        $this->Status = new Property("Status", "Status", NUMERICBOX, false, $this->Alias);
        $this->Price = new Property("Price", "Price", TEXTAREA, false, $this->Alias);
        $this->MarketId = new Property("MarketId", "MarketId", NUMERICBOX, false, $this->Alias);
        $this->UserId = new Property("UserId", "UserId", NUMERICBOX, false, $this->Alias);

        //Repertoire de stockage des images    
        $this->DirectoryImage = "Data/Apps/Market/Product/";

        //Creation de l entité 
        $this->Create();
    }

    
	 /***
     * Get Image Path
     */
    function GetImagePath(){
        
        if(file_exists($this->DirectoryImage . $this->IdEntite ."/full.png")){
             return $this->DirectoryImage . $this->IdEntite ."/full.png" ;
        } 
        
        return $this->Core->GetPath("/images/nophoto.png");
    }

    /***
     * Get The Comment
     */
    function GetComments() {
        
        $commentPlugin = \Apps\EeApp\Helper\AppHelper::GetPluginType($this->Core, "Market", "Comment");
  
          if($commentPlugin != null ){
  
                $comments = $commentPlugin->GetByEntity($this->Core, "MarketProduct", $this->IdEntite);
  
                 if (count($comments) > 0) {
  
                $view = "<ul style='text-align:left;'>";
  
                foreach ($comments as $comment) {
                    $view .= "<li><b>" . $comment->DateCreated->Value . "</b> .  " . $comment->UserName->Value  . "<span>" .$comment->Message->Value . "</span>";
                    $view .= "<div class='tool'><i id='$comment->IdEntite' class='publishComment fa " .( $comment->Status->Value == "1" ? "fa-eye" : "fa-eye-slash published"  ) . "'></i>";
                    $view .= "<i id='$comment->IdEntite' class='fa fa-trash removeComment'></i>";
                    $view .= "</div>";   
                    $view .= "</span></li>";
                }
  
                $view .= "</ul>";
            }
  
            return $view;
        }
      }

    /***
     * Get Votes
     */
    function GetVotes() {
        
        $votePlugin = \Apps\EeApp\Helper\AppHelper::GetPluginType($this->Core, "Market", "Vote");
  
          if($votePlugin != null ){
  
                $votes = $votePlugin->GetByEntity($this->Core, "MarketProduct", $this->IdEntite);
  
                 if (count($votes) > 0) {
  
                $view = "<ul style='text-align:left;'>";
  
                foreach ($votes as $vote) {
                    $view .= "<li><b>" . $vote->DateCreated->Value . "</b> .  " . $vote->UserName->Value  . "<b> " .$vote->Vote->Value . "</b>";
                    $view .= "</li>";
                }
  
                $view .= "</ul>";
            }
  
            return $view;
        }
      }

      /***
     * Get Notes
     */
    function GetNotes() {
        
        $notePlugin = \Apps\EeApp\Helper\AppHelper::GetPluginType($this->Core, "Market", "Rate");
  
          if($notePlugin != null ){
  
                $notes = $notePlugin->GetByEntity($this->Core, "MarketProduct", $this->IdEntite);
  
                 if (count($notes) > 0) {
  
                $view = "<ul style='text-align:left;'>";
  
                foreach ($notes as $note) {

                    $noteBox = new NoteBox("note", $note->Note->Value);

                    $view .= "<li><b>" . $note->DateCreated->Value . "</b> .  " . $note->UserName->Value  . "<b> " . $noteBox->Show() . "</b>";
                    $view .= "</li>";
                }
  
                $view .= "</ul>";
            }
  
            return $view;
        }
      }
}

?>
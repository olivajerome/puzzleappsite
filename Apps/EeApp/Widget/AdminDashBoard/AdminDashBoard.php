<?php

namespace Apps\EeApp\Widget\AdminDashBoard;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;
use Core\Entity\Entity\Argument;
use Apps\EeApp\Entity\EeAppApp;

class AdminDashBoard {
    
    public function __construct($core) {
        $this->Core = $core;
    }
    
    /***
     * Render the widget
     */
    public function Render(){
        $view = new View(__DIR__ . "/View/adminDashboard.tpl", $this->Core);

        $app = new EeAppApp($this->Core);
        $apps = $app->GetAll();

        $view->AddElement(new ElementView("nbApp", count($apps)));
        $view->AddElement(new ElementView("Apps", $apps));

        return $view->Render();
    }
}

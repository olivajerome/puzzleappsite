<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Stat\Module\Admin;

use Core\Control\Button\Button;
use Core\Controller\Controller;
use Core\View\View;
use Core\View\ElementView;
use Core\Control\TabStrip\TabStrip;

use Core\Entity\User\User;
use Core\Control\ListBox\ListBox;

/*
 * 
 */
 class AdminController extends Controller
 {
    /**
     * Constructeur
     */
    function _construct($core="")
    {
          $this->Core = $core;
    }

    /**
     * Creation
     */
    function Create()
    {
    }

    /**
     * Initialisation
     */
    function Init()
    {
    }

    /**
     * Affichage du module
     */
    function Show($all=true)
    {
         return $this->Index();
    }
    
    /*
    * Get the home page
    */
   function Index()
   {
       $view = new View(__DIR__."/View/index.tpl", $this->Core);
       
       $tabStat = new TabStrip("tabStat", "Stat");
       $tabStat->AddTab("Utilisateurs", $this->GetTabUser());
   /*    $tabStat->AddTab("Produits",  $this->GetTabProduct());
       $tabStat->AddTab("Proposition",  $this->GetTabProposition());
       $tabStat->AddTab("Commande",  $this->GetTabCommand());
     
       $tabStat->AddTab("Application mobile",  $this->GetTabAppMobile());*/
     
       //$tabStat->AddTab("Propositions", $control);
       $view->AddElement($tabStat);
       
       return $view->Render();
   }
   
   /***
    * Statistiques sur les utilisateurs
    */
   function GetTabUser(){
    
     $view = new View(__DIR__."/View/tabUser.tpl", $this->Core);
  
     //Utilisateur avec le nombre de produit
     $request = "select u.email, concat(Month(u.DateCreate),'-', Year(u.DateCreate)) as DateCreate,
                concat(Month(u.DateConnect),'-', Year(u.DateConnect)) as DateConnect
                from ee_user as u 
                 GROUP BY u.Id order BY u.dateCreate ";
     
     $result = $this->Core->Db->GetArray($request);
     
     $dateCreate = array();
     $dateConnect = array();
     
     foreach($result as $data){
         
         $totalUser ++;
         
         $dateCreate[$data["DateCreate"]] += 1;
         $dateConnect[$data["DateConnect"]] += 1;
     }
     
     
     //Date d'inscription
     $view->AddElement(new ElementView("dateCreate", json_encode($dateCreate)));
     $view->AddElement(new ElementView("dateConnect", json_encode($dateConnect)));
   
     return $view;
       
   }
   
  
   
   
   
          
          /*action*/
 }?>
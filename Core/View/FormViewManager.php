<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Core\View;

use Core\Utility\Date\Date;

/**
 * Replace element by executed fuction
 *
 * @author OLIVA
 */
class FormViewManager
{
    /***
     * Remplace les element
     */
    public static function ReplaceForm($html, $form)
    {
        $html = self::ReplaceOpen($html, $form);
        $html = self::ReplaceClose($html, $form);
        $html = self::ReplaceError($html, $form);
        $html = self::ReplaceSuccess($html, $form);
        $html = self::ReplaceRender($html, $form);
        $html = self::ReplaceRenderImage($html, $form);
    
        return $html;
    }

    /**
     * Remplace les fonction Form->Open();
     */
    public static function ReplaceOpen($html, $form)
    {
        $html = str_replace("{{form->Open()}}", $form->RenderOpen(), $html);
        
        return $html;
    }

    /**
     * Remplace les fonction Form->Close();
     */
    public static function ReplaceClose($html, $form)
    {
        $html = str_replace("{{form->Close()}}", $form->RenderClose(), $html);
        
        return $html;
    }

    /**
     * Remplace les Form->Error();
     */
    public static function ReplaceError($html, $form)
    {
        $html = str_replace("{{form->Error()}}", $form->RenderError(), $html);
        
        return $html;
    } 

    /**
     * Remplace les Form->Success();
     */
    public static function ReplaceSuccess($html, $form)
    {
        $html = str_replace("{{form->Success()}}", $form->RenderSuccess(), $html);
        
        return $html;
    }

    /***
     * Remplace les Form->Render()
     * Par chaque controll qui a été définie
     */
    public static function ReplaceRender($html, $form)
    {
          $html = $form->RenderControl($html);
          return $html;
    }
    
    /***
     * Remplace les Form->RenderImages()
     * Par chaque controll qui a été définie
     */
    public static function ReplaceRenderImage($html, $form)
    {
          $html = str_replace("{{form->RenderImage()}}",  $form->RenderImage(), $html);  
          return $html;
    }
}

<div>
    
    <h3>{{GetCode(Annonce.Responses)}}</h3>
    
    {{foreach Responses}}
    <div class='reponse'>
        {{element->GetUser()}}
        
        {{if ShowContactMember == true}}
            {{GetWidget(Message,ContactMember,AnnonceResponse-{{element->IdEntite}})}}
        {{/if ShowContactMember == true}}
   
        <p>{{element->Response->Value}}</p> 
    </div>
   
    {{/foreach Responses}}
   
</div>


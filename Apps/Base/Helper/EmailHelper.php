<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Base\Helper;

use Apps\EeApp\Entity\EeAppAdmin;
use Core\Core\DataBase;
use Core\Entity\User\User;
use Core\Utility\File\File;


/**
 * Helper for Send Email
 * Send Base Email or by Email App
 * @author jerome
 */
class EmailHelper 
{
    public static function SendEmail($core, $code, $email, $params) {

        $app = \Apps\EeApp\Helper\AppHelper::HaveApp($core, "NewsLetter");

        if($app != false){

            $appPath = "\Apps\\" .$app->Name->Value . "\\". $app->Name->Value;  
            $appNewsLetter = new $appPath($core);
            return $appNewsLetter->SendEmail($code, $email, $params);
        } 
        else {
            echo "App Non TROUVE";
        }
    }
}
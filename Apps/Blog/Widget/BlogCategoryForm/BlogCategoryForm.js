var BlogCategoryForm = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
BlogCategoryForm.Init = function(callBack){

    Event.AddById("btnSaveCategory", "click", BlogCategoryForm.SaveCategory);
    BlogCategoryForm.callBack = callBack;
};

/***
* Obtient l'id de l'itin�raire courant 
*/
BlogCategoryForm.GetId = function(){
    return Form.GetId("categoryForm");
};

/*
* Sauvegare l'itineraire
*/
BlogCategoryForm.SaveCategory = function(e){
   
    if(Form.IsValid("categoryForm"))
    {

    var data = "Class=Blog&Methode=SaveCategory&App=Blog";
        data +=  Form.Serialize("categoryForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);
         
            if(data.statut == "Error"){
                Form.RenderError("categoryForm", data.message);
            } else {

                Dialog.Close();
                
                if(BlogCategoryForm.callBack){
                    BlogCategoryForm.callBack(data);
                }
            }
        });
    }
};


<div class='message' id='{{messageId}}'>
    
        <div class='moreAction' data-type='message'  style='float:right'>
                ...  
        </div>
        <div class='title'> 
            <div title="{{pseudo}}" class="avatarmini-img avatarmini" style="cursor: pointer;">
                <img src="{{image}}" alt="images">
            </div>
            <div>
                <b>{{pseudo}}</b> <span>{{day}}</span> {{projet}}
            </div>    
        </div> 

        <div class='messageUser'  > 
         
          <div class='text'>{{text}}</div>


          <div class='buttons' >
              <div class='infoLike'>
                  <span class='likes'>{{userLike}} <div style='width: 65px !important; margin-left: 30px; color: #000;'> {{nbLike}} j'aime(s)</div></span> - <span class ='nbComment' style='color: #000;' >{{nbComment}} commentaire(s)</span>
              </div>
              <div class='action'>
                <i data-type='message'  class='fa fa-thumbs-o-up nbLike likeMessage bgkBtnLikeComment'  title='Aimer la conversation'></i>
                <i class='fa fa-comment-o btnComment bgkBtnLikeComment' title='Commenter' ></i>
              </div>      
            </div> 
           <div class='lstComment'>{{comments}}</div>
           <div class='commentBox'>
               <div class="avatarmini-img avatarmini" style="cursor: pointer; zoom: 0.7;">
                    <img src="{{userImage}}" alt="images">
               </div>
               <div style='width:86%'>
                   <textarea  style='height:35px'  class='form-control tbComment' type='text' placeholder='Quelque chose à ajouter' class='form-control' ></textarea>
               </div>
           </div>
        </div>
    </div>

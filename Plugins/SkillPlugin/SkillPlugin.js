var SkillPlugin = function(){};

SkillPlugin.Init = function(){
    
    //Admin Event
    Event.AddById('btnAddCategory', "click" , SkillPlugin.AddCategory);
    Event.AddByClass("removeSkill", "click", SkillPlugin.RemoveSkill);
    Event.AddByClass("removeSkillEntity", "click", SkillPlugin.RemoveToEntity);
    EntityGrid.Init("gdCategory");

    //Member Event
    Event.AddById('lstSkillCategory', "change" , SkillPlugin.LoadSkill);
};

/*
* Add a competence
*/
SkillPlugin.AddCategory = function(){
    let name = Dom.GetById("newSkillCategoryName");
    let appName = Dom.GetById("skillAppName");

    if(name.value != ""){

        Plugin.SendRequest({"plugin": "SkillPlugin",
            "method": "AddCategory",
            "name": name.value, "appName" : appName.value}, (data) => {

            let gdCategory = Dom.GetById("gdCategory");
            gdCategory.parentNode.parentNode.innerHTML = data;
            SkillPlugin.Init();
        });
    } else{
        Animation.Notify("Skill.YouMustEnterName");
    }
};

/***
 * Delete a category
 */
SkillPlugin.DeleteCategory = function(categoryId){

     Animation.Confirm(Language.GetCode("Base.ConfirmRemoveCategory"), function(){

        let appName = Dom.GetById("skillAppName");

        Plugin.SendRequest({"plugin": "SkillPlugin",
        "method": "DeleteCategory",
        "id" : categoryId,
        "appName" : appName.value
        }, (data) => {

            let gdCategory = Dom.GetById("gdCategory");
            gdCategory.parentNode.parentNode.innerHTML = data;
            SkillPlugin.Init();
      });
    });
};

/***
 * Add Competence to a categorie
 */
SkillPlugin.AddSkill = function(categoryId){

    Animation.Ask(Language.GetCode("Base.Name"), function(response){

        Plugin.SendRequest({"plugin": "SkillPlugin",
        "method": "AddSkill",
        "categoryId" : categoryId,
        "name": response}, (data) => {

            Animation.Load("competenceCategory-" +categoryId , data);
      });
    });
};

/***
 * Remove a skill
 */
SkillPlugin.RemoveSkill = function(e){

    Animation.Confirm(Language.GetCode("Skill.RemoveSkill"), function(){

        Plugin.SendRequest({"plugin": "SkillPlugin",
        "method": "RemoveSkill",
        "id" : e.srcElement.id,
        }, (data) => {

            let container = e.srcElement.parentNode;
            container.parentNode.removeChild(container);
      });
    });
};

/***
 * Load all Skill of a category
 * @returns {undefined}
 */
SkillPlugin.LoadSkill = function(e){
     
    Plugin.SendRequest({"plugin": "SkillPlugin",
        "method": "GetSkillsByCategory",
        "id" : e.srcElement.value,
        }, (data) => {

            let container = Dom.GetById("skillContainer");
            container.innerHTML = data;
            
            Event.AddByClass("addSkillEntity", "click", SkillPlugin.AddToEntity);
      });
 }; 
 
/***
 * Add Skill to a entity
 * @returns {undefined}
 */ 
SkillPlugin.AddToEntity = function(e){
    let appName = Dom.GetById("skillAppName");
    let entityId = Dom.GetById("skillEntityId");  
    
    Plugin.SendRequest({"plugin": "SkillPlugin",
        "method": "AddToEntity",
        "id" : e.srcElement.id,
        "appName" : appName.value,
        "entityId" : entityId.value
    }, (data) => {

            let container = Dom.GetById("skillEntity");
            container.innerHTML = data;
            
            Event.AddByClass("removeSkillEntity", "click", SkillPlugin.RemoveToEntity);
      });
 };
 
 /***
  * Remove a skill to a entity
  * @param {type} e
  * @returns {undefined}
  */
 SkillPlugin.RemoveToEntity = function(e){
     
     Animation.Confirm(Language.GetCode("Skill.RemoveSkillEntity"), function(){

        Plugin.SendRequest({"plugin": "SkillPlugin",
        "method": "RemoveToEntity",
        "id" : e.srcElement.id,
        }, (data) => {

            let container = e.srcElement.parentNode;
            container.parentNode.removeChild(container);
      });
    });
 };
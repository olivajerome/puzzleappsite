<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Plugins\SkillPlugin\Entity;

use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

use Plugins\SkillPlugin\Entity\Skill;

class SkillCategory extends Entity {

    //Constructeur
    function __construct($core) {
        //Version
        $this->Version = "2.0.0.0";

        //Nom de la table 
        $this->Core = $core;
        $this->TableName = "SkillCategory";
        $this->Alias = "SkillCategory";

        $this->Name = new Property("Name", "Name", TEXTBOX, false, $this->Alias);
        $this->AppName = new Property("AppName", "AppName", TEXTBOX, false, $this->Alias);
        
        //Creation de l entité 
        $this->Create();
    }

    /***
     * Get the Skill of a category
     */
    function GetSkills($add = false, $remove = true ){

        $skill = new Skill($this->Core);
        $skills = $skill->Find("CategoryId=".$this->IdEntite);

        $view = "<div id='competenceCategory-".$this->IdEntite."'>";
        
        foreach($skills as $skill ){
            $icon = "";
       
            if($add){
                $icon .=  "<i class='fa fa-plus addSkillEntity' id='".$skill->IdEntite."'  ></i>";
            }

            if($remove){
                $icon .=  "<i class='fa fa-trash removeSkill' id='".$skill->IdEntite."'  ></i>";
            }
      
            $view .= "<div class='chips'>" .$skill->Name->Value . $icon . "</i></div>";  
        }

        $view .= "</div>";
        return $view;
    }
}

?>
<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Stat;

use Core\Core\Core;
use Apps\Base\Base;
use Core\Core\Request;

use Apps\Stat\Module\Widget\WidgetController;
use Apps\Stat\Module\Admin\AdminController;


class Stat extends Base {

    /**
     * Auteur et version
     * */
    public $Author = 'Webemyos';
    public $Version = '1.0.0';

    /**
     * Constructeur
     * */
    function __construct($core) {
        $this->Core = Core::getInstance();
        parent::__construct($this->Core, "Stat");
    }

    /**
     * Execution de l'application
     */
    function Run() {
        echo parent::RunApp($this->Core, "Stat", "Stat");
    }

    /**
     * Définie les routes publiques
     */
    function GetRoute($routes = "") {
        parent::GetRoute(array());
        return $this->Route;
    }

    /*     * *
     * Obtient les widget
     */

    public function GetWidget($type, $params) {

        $widget = new WidgetController($this->Core);
        return $widget->Show($type, $params);
    }
    
    /***
     * Tri les stats utilisateurs
     */
    public function OrderUser(){
        
        $adminController = new AdminController($this->Core);
        return $adminController->GetTabUser(Request::GetPost("order"));
    }

}

?>
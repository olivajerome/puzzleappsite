var DialogAdminBlogController = function(){};

DialogAdminBlogController.AddCategorie = function(){
    BlogCategoryForm.Init(function(data){ Blog.ReloadCategory(data)});
};

DialogAdminBlogController.AddArticle = function(){
    BlogArticleForm.Init(function(data){ Blog.ReloadArticle(data)});
};

DialogAdminBlogController.WriteArticle = function(){
    BlogArticleEditor.Init();
};

DialogAdminBlogController.ShowImport = function(){
    ImportBlogForm.Init();
};
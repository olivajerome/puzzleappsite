<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Market\Module\DialogAdminMarket;

use Core\Control\Button\Button;
use Core\Controller\Controller;
use Core\View\View;

use Apps\Market\Widget\MarketCategoryForm\MarketCategoryForm;
use Apps\Market\Widget\MarketCaracteristiqueForm\MarketCaracteristiqueForm;
use Apps\Market\Entity\MarketCategory;
use Apps\Market\Entity\MarketCaracteristique;

/*
 * 
 */
 class DialogAdminMarketController extends Controller
 {
    /**
     * Constructeur
     */
    function _construct($core="")
    {
          $this->Core = $core;
    }

    /**
     * Create
     */
    function Create()
    {
    }

    /**
     * Init
     */
    function Init()
    {
    }

    /**
     * Render
     */
    function Show($all=true)
    {
         return $this->Index();
    }
    
    /*
    * Get the home page
    */
    function Index()
    {
       $view = new View(__DIR__."/View/index.tpl", $this->Core);
       return $view->Render();
    }

   /***
     * Add a catégorie
     */
    function AddCategory($categoryId){
      
        $category = new MarketCategory($this->Core);
        
        if($categoryId != ""){
            $category->GetById($categoryId);
        }
        
        $categoryForm = new MarketCategoryForm($this->Core);
        $categoryForm->Load($category);
        $categoryForm->form->LoadImage($category);

        
        return $categoryForm->Render();
    }
    
    /***
     * Add a caractéristique
     */
    function AddCaracteristique($caracteristiqueId){
     
        $caracteristique = new MarketCaracteristique($this->Core);
 
        if($caracteristiqueId != ""){
            $caracteristique->GetById($caracteristiqueId);
        }
    
       $data["Id"] = $caracteristiqueId;
       $caracteristiqueForm = new MarketCaracteristiqueForm($this->Core, $data);
       $caracteristiqueForm->Load($caracteristique);
     
       return $caracteristiqueForm->Render();
    }
    
          
          /*action*/
 }?>
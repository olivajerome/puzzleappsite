<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Annonce\Module\Admin;

use Core\Control\Button\Button;
use Core\Controller\AdministratorController;
use Core\View\View;
use Core\View\ElementView;
use Core\Control\TabStrip\TabStrip;
use Core\Control\EntityGrid\EntityGrid;
use Core\Control\EntityGrid\EntityColumn;
use Core\Control\EntityGrid\EntityIconColumn;
use Core\Control\EntityGrid\EntityFunctionColumn;
use Core\Utility\Format\Format;
use Apps\Annonce\Widget\AnnonceCategoryForm\AnnonceCategoryForm;
use Apps\Annonce\Widget\AnnonceAdminForm\AnnonceAdminForm;

use Apps\Annonce\Entity\AnnonceCategory;
use Apps\Annonce\Entity\AnnonceAnnonce;


/*
 * 
 */

class AdminController extends AdministratorController {

    /**
     * Create
     */
    function Create() {
        
    }

    /**
     * Init
     */
    function Init() {
        
    }

    /**
     * Render
     */
    function Show($all = true) {
        return $this->Index();
    }

    /*
     * Get the Admin Page
     */

    function Index() {
        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);

        $tabAnnonce = new TabStrip("tabAnnonce", "Annonce"); 
        $tabAnnonce->AddTab($this->Core->GetCode("Annonce.Category"), $this->GetTabCategory());
        $tabAnnonce->AddTab($this->Core->GetCode("Annonce.Annonce"), $this->GetTabAnnonce());
        $tabAnnonce->AddTab($this->Core->GetCode("Annonce.Extensions"), $this->GetTabExtension());

        $view->AddElement(new ElementView("tabAnnonce", $tabAnnonce->Render()));

        return $view->Render();
    }

    /*     * *
     * Categorie of the Annonce
     */

    function GetTabCategory() {

        $gdCategory = new EntityGrid("gdCategory", $this->Core);
        $gdCategory->Entity = "Apps\Annonce\Entity\AnnonceCategory";
        $gdCategory->App = "Annonce";
        $gdCategory->Action = "GetTabCategory";

        $btnAdd = new Button(BUTTON, "btnAddCategory");
        $btnAdd->Value = $this->Core->GetCode("Annonce.AddCategory");

        $gdCategory->AddButton($btnAdd);
        $gdCategory->AddColumn(new EntityFunctionColumn("Image", "GetThumbImage"));

        $gdCategory->AddColumn(new EntityColumn($this->Core->GetCode("Annonce.Name"), "Name"));
        $gdCategory->AddColumn(new EntityFunctionColumn($this->Core->GetCode("Annonce.NumberAnnonce"), "GetNumberAnnonce"));

        $gdCategory->AddColumn(new EntityIconColumn("Action",
                        array(array("EditIcone", "Annonce.EditCategory", "Annonce.EditCategory"),
                    array("DeleteIcone", "Annonce.DeleteCategory", "Annonce.DeleteCategory"),
                        )
        ));

        return $gdCategory->Render();
    }

    /*     * *
     * Categorie of the Annonce
     */

    function GetTabAnnonce() {

        $gdAnnonce = new EntityGrid("gdAnnonce", $this->Core);
        $gdAnnonce->Entity = "Apps\Annonce\Entity\AnnonceAnnonce";
        $gdAnnonce->App = "Annonce";
        $gdAnnonce->Action = "GetTabAnnonce";
        $gdAnnonce->AddOrder("Id desc");

        $gdAnnonce->AddColumn(new EntityFunctionColumn("Image", "GetThumbImage"));
        $gdAnnonce->AddColumn(new EntityColumn("Title", "Title"));

        $gdAnnonce->AddColumn(new EntityIconColumn("Action",
                        array(array("EditIcone", "Annonce.EditAnnonce", "Annonce.EditAnnonce"),
                        )
        ));

        return $gdAnnonce->Render();
    }
    
    /***
     * Extension de l'application
     */
    function GetTabExtension(){
       $view = new View(__DIR__ . "/View/extension.tpl", $this->Core);
        
       $plugins = \Apps\EeApp\Helper\AppHelper::GetPluginApp($this->Core, "Annonce");
       $view->AddElement(new ElementView("Plugins", $plugins));

       return $view->Render();
    }

    /**
     * Save a category
     */
    function SaveCategory($core, $data) {

        $annonceCategoryForm = new AnnonceCategoryForm($this->Core, $data);

        if ($annonceCategoryForm->Validate($data)) {

            $category = new AnnonceCategory($this->Core);

            if ($data["Id"]) {
                $category->GetById($data["Id"]);
            }
            $category->Code->Value = Format::ReplaceForUrl($data["Name"]);
            $annonceCategoryForm->Populate($category);
            $category->Save();
            
            if($data["Id"] == ""){
                $category->IdEntite = $this->Core->Db->GetInsertedId();;
            }
            
            $category->SaveImage($data["dvUpload-UploadfileToUpload"], true);

            return true;
        } else {
            return $annonceCategoryForm->errors;
        }
    }
    
    /***
     * Save Admin Annonce 
     */
    function SaveAdminAnnonce($data){
         $annonceAdminForm = new AnnonceAdminForm($this->Core, $data);

        if ($annonceAdminForm->Validate($data)) {

            $annonce = new AnnonceAnnonce($this->Core);
            $annonce->GetById($data["Id"]);
            $annonceAdminForm->Populate($annonce);
            $annonce->Save();
            
            return true;
        } else {
            return $annonceCategoryForm->errors;
        }
    }
    /***
     * Delete a category
     */
    function DeleteCategory($categoryId){
        
        //Remove Annonce before 
        $annonce = new AnnonceAnnonce($this->Core);
        $annonces = $annonce->Find("CategoryId=" .$categoryId);
        
        //Call Delete of each annonce because other element can by linked to Annonce
        foreach($annonces as $annonce){
            $annonce->Delete();
        }
        
        $annonceCategory = new AnnonceCategory($this->Core);
        $annonceCategory->GetById($categoryId);
        $annonceCategory->Delete();
        
        return true;
    }

    /* action */
}

?>
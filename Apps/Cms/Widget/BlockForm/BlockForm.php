<?php

namespace Apps\Cms\Widget\BlockForm;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;

class BlockForm extends Form {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "") {
        $this->Core = $core;
        $this->Data = $data;

        //On le fait ici pour avoir le formuaire � l'affichage mais aussi � la validation
        $this->Init($data);
    }

    /**
     * Initialisation 
     */
    function Init($data) {
        $this->form = new Form("BlockForm", $data);

        $this->form->SetView(__DIR__ . "/View/index.tpl", $this->Core);
        $this->form->SetAction("/View/index.tpl");

         $this->form->Add(array("Type" => "Hidden",
            "Id" => "Id",
        ));
         
        $this->form->Add(array("Type" => "Hidden",
            "Id" => "ParentId",
        ));
        
        $this->form->Add(array("Type" => "ListBox",
            "Id" => "Size",
            "Values" => array( $this->Core->GetCode("Cms.ChooseSize") =>"", "1" => "1", "2"=>"2", "3"=>"3",
                                                     "4"=>"4","5"=> "5", "6"=>"6", "7"=>"7", "8"=>"8", "9","9",
                                                     "10"=>"10", "11"=>"11", "12"=>"12" ),
            "Validators" => ["Required"]
        ));
        
        $this->form->Add(array("Type" => "NumericBox",
            "Id" => "Position",
        ));

        $this->form->Add(array("Type" => "TextBox",
            "Id" => "Style",
        ));
        

        $this->form->Add(array("Type" => "TextBox",
            "Id" => "CssClass",
        ));
      
        $this->form->Add(array("Type" => "EntityListBox",
            "Id" => "TypeId",
            "Field" => "Name",
            "Values" => array("" => $this->Core->GetCode("Cms.ChoseTypeBlock")),
            "Entity" => "Apps\Cms\Entity\CmsBlockType",
            "Validators" => ["Required"]
        ));
        
        $this->form->Add(array("Type" => "Button",
            "Id" => "btnSaveBlock",
            "Value" => $this->Core->GetCode("Cms.AddBlock"),
            "CssClass" => "btn btn-primary"
        ));
    }

    /*     * *
     * G�n�rer les formulaire
     */

    function Render() {
        return $this->form->Render();
    }

    /*     * *
     * Valide les donn�es selon les diff�rents formulaires
     */

    function Validate($data = "") {
        $errors = $this->form->Validate($this->Data);

        return $errors;
    }

    /*     * *
     * Charge les controls avec les donn�es de l'entit�
     */

    function Load($block) {
        $this->form->Load($block);
    }

    /*     * *
     * Charge l'entit� avec les donn�es des diff�rents formulaire
     */

    function Populate($product) {
    }

    /*     * *
     * Gestion des erreurs
     */

    function Error() {
        $errors[] = $this->form->errors;

        return json_encode(array("statut" => "Error", "message" => $errors));
    }

}

var NewsLetterEmailForm = function () {};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
NewsLetterEmailForm.Init = function (callBack) {

    NewsLetterEmailForm.CallBack = callBack;
    Event.AddById("btnSaveEmail", "click", NewsLetterEmailForm.SaveElement);    
    Event.AddById("AppId", "change", NewsLetterEmailForm.LoadEmailApp);

    NewsLetterEmailForm.txtEditor = new TextRichEditor(Dom.GetById("Content"));
};

/***
 * Obtient l'id de l'itin�raire courant 
 */
NewsLetterEmailForm.GetId = function () {
    return Form.GetId("NewsLetterEmailFormForm");
};

/*
 * Sauvegare l'itineraire
 */
NewsLetterEmailForm.SaveElement = function (e) {

    if (Form.IsValid("NewsLetterEmailFormForm"))
    {

        let data = "Class=NewsLetter&Methode=SaveEmail&App=NewsLetter";
        data += Form.Serialize("NewsLetterEmailFormForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if (data.statut == "Error") {
                Form.RenderError("NewsLetterEmailFormForm", data.message);
            } else {

                Dialog.Close();

                if (NewsLetterEmailForm.CallBack) {
                    NewsLetterEmailForm.CallBack(data);
                }
            }
        });
    }
};

/***
 * Load Email FOr a App
 */
NewsLetterEmailForm.LoadEmailApp = function(e) {
    let App = e.srcElement.value;

    let data = "Class=NewsLetter&Methode=LoadEmailAppByApp&App=NewsLetter";
        data += "&AppId="+ App;

    Request.Post("Ajax.php", data).then(data => {
        Animation.Load("lstEmailApp", data);
    
        Event.AddById("Widget", "change", (e)=>{
            let widget = e.srcElement;
            
            Form.SetValue("NewsLetterEmailFormForm", "Code", widget.value);

        } );
    });

};
CREATE TABLE IF NOT EXISTS `ComunityComment` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`UserId` INT  NULL ,
`Message` TEXT  NULL ,
`DateCreated` DATE  NULL ,
`MessageId` INT  NULL ,
`ParentId` INT  NULL ,
`ParentType` INT  NULL ,
PRIMARY KEY (`Id`),
CONSTRAINT `ComunityMessage_ComunityComment` FOREIGN KEY (`MessageId`) REFERENCES `ComunityMessage`(`Id`),
CONSTRAINT `ee_user_ComunityComment` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
var CarouselForm = function(){};

/***
 * Init the form
 * @returns {undefined}
 */
CarouselForm.Init = function(callBack){
    Event.AddById("btnSaveCarousel", "click", CarouselForm.SaveElement);
    Event.AddById("editParameter", "click", CarouselForm.EditParameter);
    Event.AddById("btnAddItem", "click", () => {
        CarouselForm.EditItem("");
    });
    
    CarouselForm.InitItem();
    
    CarouselForm.callBack = callBack;
};

/***
 * Remove a lesson
 * @returns {undefined}
 */
CarouselForm.InitItem = function () {

    Event.AddByClass("editItem", "click", CarouselForm.EditItem);
    Event.AddByClass("removeItem", "click", CarouselForm.RemoveItem);
};


/**
 * Edit the Parameter
 * @returns {undefined}
 */
CarouselForm.EditParameter = function () {
    Animation.Hide("itemTool");
    Animation.Show("formTool");
};

/***
 * Edit a lesson
 * @returns {undefined}
 */
CarouselForm.EditItem = function (e) {

    Animation.Hide("formTool");
    Animation.Show("itemTool");

    let itemId = "";
    let data = "Class=Carousel&Methode=EditItem&App=Carousel";
    data += "&CarouselId=" + CarouselForm.GetId();

    if (e != "") {
        itemId = e.srcElement.id;
    }

    data += "&itemId=" + itemId;

    Request.Post("Ajax.php", data).then(data => {
        Animation.Load("itemTool", data);

        CarouselItemForm.Init(CarouselForm.RefresItem);
    });
};

/***
* Get the Id of element 
*/
CarouselForm.GetId = function(){
    return Form.GetId("CarouselFormForm");
};

/*
* Save the Element
*/
CarouselForm.SaveElement = function(e){
   
    if(Form.IsValid("CarouselFormForm"))
    {

    let data = "Class=Carousel&Methode=SaveCarousel&App=Carousel";
        data +=  Form.Serialize("CarouselFormForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.status == "error"){
                Form.RenderError("CarouselFormForm", data.data.message);
            } else{

                Form.SetId("CarouselFormForm", data.data.Id);
                Carousel.ReloadCarousel( data.data.html);
                
                Animation.Notify(Language.GetCode("Carousel.CarouselUpdated"));
            }
        });
    }
};

/***
 * Rafresh lecon of a mooc
 * @param {type} data
 * @returns {undefined}
 */
CarouselForm.RefresItem = function (Items) {

    let view = "";

    for (let i = 0; i < Items.length; i++) {

        view += "<li class='lesson editItem' id='" + Items[i].IdEntite + "'>" + Items[i].Name + "<i class='fa fa-trash removeItem'></i></li>";
    }

    Animation.Load("listItem", view);
    CarouselForm.InitItem();
};

/***
 * Remove a lesson
 * @param {type} e
 * @returns {undefined}
 */
CarouselForm.RemoveItem = function (e) {
    let lessonId = e.srcElement.parentNode.id;
    let container = e.srcElement.parentNode;

    Animation.Confirm(Language.GetCode("Carousel.ConfirmRemoveItem"), function () {

        let data = "Class=Carousel&Methode=RemoveItem&App=Carousel";
        data += "&itemId=" + lessonId;

        Request.Post("Ajax.php", data).then(data => {
            container.parentNode.removeChild(container);
            
            CarouselForm.EditParameter();
        });
    });
};


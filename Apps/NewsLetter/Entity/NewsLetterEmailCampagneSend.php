<?php 
/*
* PuzzleApp
* Webemyos
* Jérôme Oliva
* GNU Licence
*/

namespace Apps\NewsLetter\Entity;
use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class NewsLetterEmailCampagneSend extends Entity  
{
	//Constructeur
	function __construct($core)
	{
		//Version
		$this->Version ="2.0.0.0"; 

		//Nom de la table 
		$this->Core=$core; 
		$this->TableName="NewsLetterEmailCampagneSend"; 
		$this->Alias = "NewsLetterEmailCampagneSend"; 

		$this->CampagneId = new Property("CampagneId", "CampagneId", NUMERICBOX,  false, $this->Alias); 
		$this->DateSended = new Property("DateSended", "DateSended", DATEBOX,  false, $this->Alias); 
		$this->Emails = new Property("Emails", "Emails", TEXTBOX,  false, $this->Alias); 

		//Creation de l entité 
		$this->Create(); 
	}
}
?>
var AnnonceAdminForm = function(){};

/***
 * Init the form
 * @returns {undefined}
 */
AnnonceAdminForm.Init = function(){
    Event.AddById("btnSave", "click", AnnonceAdminForm.SaveElement);
};

/***
* Get the Id of element 
*/
AnnonceAdminForm.GetId = function(){
    return Form.GetId("AnnonceAdminFormForm");
};

/*
* Save the Element
*/
AnnonceAdminForm.SaveElement = function(e){
   
    if(Form.IsValid("AnnonceAdminFormForm"))
    {

    let data = "Class=Annonce&Methode=SaveAdminAnnonce&App=Annonce";
        data +=  Form.Serialize("AnnonceAdminFormForm");
      
        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.status == "error"){
                Form.RenderError("AnnonceAdminFormForm", data.data.message);
            } else{
                Dialog.Close();
            }
        });
    }
};


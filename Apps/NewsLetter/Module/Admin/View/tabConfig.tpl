<section>
    <div>
        {{newsLetterConfigForm}}
    </div>

    <div class='marginTop borderTop'>
    
        <h2>{{GetCode(NewsLetter.Plugins)}}</h2>
        <button id='btnAddPlugin' class='btn btn-primary'>{{GetCode(NewsLetter.AddPlugin)}}</button>


        <div id='lstPlugin' class='right'>
            {{foreach Plugins}}   
                <div class='chips'>

                    {{element->PluginName->Value}}

                    <i id='{{element->IdEntite}}' class='fa fa-trash removePlugin'></i>


                </div>
            {{/foreach Plugins}}   
        </div>   
    </div>

</section>
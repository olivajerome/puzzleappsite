<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Core\Control\ListCheckBox;

use Core\Control\IControl;
use Core\Control\Control;

class ListCheckBox extends Control {

    //Constructeur
    function __construct($name) {
        //Version
        $this->Version = "2.0.0.0";

        $this->Id = $name;
        $this->Name = $name;
        $this->CssClass = "form-control";
    }

    /*
     * List of Element   
     */

    public $Elements;

    //Ajout d'un element à la liste
    function Add($key, $value) {
        $this->Elements[$key] = $value;
    }

    function Show() {
        $html = "<ul >";

        if (is_array($this->Elements)) {

            foreach ($this->Elements as $key => $value) {
                $html .= "<li><input name='".$this->Name."_".$value."".$key." '    type='checkBox' value='$value'><label>&nbsp;$key</label></input></li>";
            }
        } else {

            $this->Element = explode(";", $this->Elements);

            $Elements = explode(";", $this->Elements);

            foreach ($Elements as $element) {
                
                $data = explode(":", $element);
                $html .= "<li><input type='checkBox' name = '" . $this->Name . "_$data[0]'  value='$data[0]'>$data[1]</input></li>";
            }
        }

        $html .= "</ul>";

        return $html;
    }
}

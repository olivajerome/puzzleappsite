<?php 
/*
* PuzzleApp
* Webemyos
* Jérôme Oliva
* GNU Licence
*/

namespace Apps\Market\Entity;
use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class MarketProductCaracteristique extends Entity  
{
	//Constructeur
	function __construct($core)
	{
		//Version
		$this->Version ="2.0.0.0"; 

		//Nom de la table 
		$this->Core=$core; 
		$this->TableName="MarketProductCaracteristique"; 
		$this->Alias = "MarketProductCaracteristique"; 

		$this->ProductId = new Property("ProductId", "ProductId", NUMERICBOX,  false, $this->Alias); 
		$this->CaracteristiqueId = new Property("CaracteristiqueId", "CaracteristiqueId", NUMERICBOX,  false, $this->Alias); 
		$this->ItemId = new Property("ItemId", "ItemId", NUMERICBOX,  false, $this->Alias); 

		//Creation de l entité 
		$this->Create(); 
	}
}
?>
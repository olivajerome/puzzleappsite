var Comunity = function (){};

Comunity.Load = function(){
    ComunityForm.Init();
    
    Comunity.InitTabComunity();
};


Comunity.LoadMember = function(){
    Comunity.Init("Comunity");
};

Comunity.InitTabComunity = function(){
     Event.AddById("btnAddComunity", "click", () => {
        Comunity.ShowAddComunity("")
    });
    EntityGrid.Init("gdComunity");
  
};

/**
 * Edit category
 * @param {type} categoryId
 * @returns {undefined}
 */
Comunity.EditComunity = function (comunityId) {
    Comunity.ShowAddComunity(comunityId);
};

/**
 * Dialogue d'ajout de catégorie
 */
Comunity.ShowAddComunity = function (comunityId) {

    Dialog.open('', {"title": Dashboard.GetCode("Comunity.AddComunity"),
        "app": "Comunity",
        "class": "DialogAdminComunity",
        "method": "AddComunity",
        "type": "left",
        "params": comunityId
    });
};


/***
 * Delete a comunitu
 * @returns {undefined}
 */
Comunity.DeleteComunity = function(comunityId){
    
    Animation.Confirm(Language.GetCode("Comunity.ConfirmDelete"), function(){
        let data = "App=Comunity&Methode=DeleteComunity&Class=Admin";
        data += "&comunityId=" +comunityId;
        
        Request.Post(url, data).then(data=>{
            Dashboard.StartApp("", "Comunity" ,"");
        }         
    );
    });
};

Comunity.Init = function(app, method){
  
   if(app == "Comunity"){
  
    Comunity.MessageId = null;
    Comunity.IsConnected = null;
    Comunity.MessageId = null;
    Comunity.Container = null;
    Comunity.Action = null;
    Comunity.CommentId = null;
    Comunity.CurrentPage = 0; 
    
    var hdComunityId = document.getElementById("hdComunityId");
    Comunity.ComunityId = hdComunityId.value;
      
   var data = "App=Base&Methode=IsConnected";
         var url = window.location.origin + "/Ajax.php";
        //On verifie si l'utilisateur est connecté
        Request.Post(url, data).then(data=>{

            if(data == "OK"){
               Comunity.IsConnected = true; 
            } else{
               Comunity.IsConnected = false; 
            }
        }         
    );
    window.removeEventListener("keypress", Comunity.SendToEnterTouche);
    
    //Touche entrée
    window.addEventListener("keypress", Comunity.SendToEnterTouche);
    
    window.onscroll = function() { 
        
       if(document.body.clientWidth < 1000){
        
        var agenda = document.getElementsByClassName("agenda");
        var projet = document.getElementsByClassName("projet");
        var leftColumn = document.getElementsByClassName("leftColumn ");
        
        if(window.scrollY > 199){
            
            if(agenda.length > 0){
                agenda[0].style.display ="none";
                projet[0].style.display = "none";
            } 
            else if(leftColumn.length > 0){
                leftColumn[0].style.display = "none";
            }
        } 
        else if(window.scrollY < 190)
        {
            if(agenda.length > 0){
            
            agenda[0].style.display ="";
            projet[0].style.display = "";
            }
            else if(leftColumn[0])
            {
             leftColumn[0].style.display = "";
            }
        }
    }
        
        if((window.innerHeight + window.scrollY ) >=  (document.body.offsetHeight) &&  (window.innerHeight + window.scrollY)  < document.body.offsetHeight + 50 /*document.documentElement.scrollTop == 1001 && Comunity.CurrentPage  == 0*/){
           
           
           Comunity.CurrentPage += 1;
           
            var hdComunityId = document.getElementById("hdComunityId");
           
            var data = "App=Comunity&Methode=LoadNextPage";
                data += "&projetId=" +hdComunityId.value;
                data += "&page=" + Comunity.CurrentPage;
                
            var url = window.location.origin + "/Ajax.php";
            
            //On verifie si l'utilisateur est connecté
            Request.Post(url, data).then(data=>{
                
                
                if(data.indexOf("Veuillez") > 0)
                {
                    Animation.Notify("Vous session a expiré, veuillez vous reconnecter !");
                    return;
                }
                
                if(data != "" )
                {                
                var messages = document.getElementById("messages");
                    messages.innerHTML += data;
                    
                    Comunity.AddEventMessage();
                    Comunity.AddEventMoreAction();
                    Comunity.ResizeImage();
              }
            });         
          
        }
    };
    
    /**
     * Ajout d'un message ou d'un commentaire
     * @type type
     */
    Event.AddById("btnSendMessage", "click", function(){
        Comunity.SendMessage();
    });
    
    /*
     * Bouton pour suivre une reoncontre thématique
     */
    Event.AddById("btnLike", "click", function(){
        
         var hdComunityId = document.getElementById("hdComunityId");
         var data = "App=Comunity&Methode=Follow";
             data += "&disccusId=" + hdComunityId.value; 
             
          var url = window.location.origin + "/Ajax.php";

          Request.Post(url, data).then(data=>{
                if(data == "OK"){     
                 Animation.Notify("Vous suivez ce sujet");
              }
              else
              {
                  Animation.Notify(data); 
              }
            });
    });
    
    /**
     * Ajout d'un commentaire
     */
    Event.AddById("btnComment", "click", function(){
        
        if( Comunity.IsConnected){
             var messageBlock = document.getElementById("messageBlock");
            messageBlock.style.display = "block";
          }
          else
          {
            Animation.Notify("Vous devez êtres connecté"); 
          }
    });
    
    Comunity.AddEventMessage();
    Comunity.AddEventMoreAction();
    //Comunity.ResizeImage();
    
    }
};

Comunity.SendToEnterTouche = function(){
    
     element = event.srcElement;
     
    if(event.keyCode == 13){
      
      
        var tchatMessage = document.getElementById("tchatMessage");
        if(tchatMessage == undefined)
        {
            Comunity.SendMessage();
            
        }
        else
        {
             ActionProjetWidget.SendTchatMessage();
        }
        
         element.style.height = "35px";
    } else {
        
       
        if(element.value.length > 60 && element.value.length < 70){
            
           // element.value += "\n";
            element.style.height =  "100px";
            
        }
        //console.log(event.keyCode);
        
    }
};

function calcHeight(value) {
  let numberOfLineBreaks = (value.match(/\n/g) || []).length;
  // min-height + lines x line-height + padding + border
  let newHeight = 20 + numberOfLineBreaks * 20 + 12 + 2;
  return newHeight;
}

/***
 * Resize les images
 * @returns {undefined}
 */
Comunity.ResizeImage = function(){
    var message = document.getElementsByClassName("text");
    
    for(var i = 0; i <message.length ; i++ ){
        
        var images = message[i].getElementsByTagName("img");
        if(images.length > 0 ){        
            images[0].style.width = "100%";
        }
    }
};

/**
  * Met à jour le nombre de commentaires
  * @returns {undefined}
  */
 Comunity.UpdateNbComment = function(add)
 {
     return;
     var currentNumber = Comunity.nbComment;
     if(add)
     {
         currentNumber.innerHTML = (parseInt(currentNumber.innerHTML) + 1 )+ " commentaires" ;
     }
     else
     {
         currentNumber.innerHTML = parseInt(currentNumber.innerHTML) -1 + " commentaires" ;
     }
 };

 /**
  * Met à jour le nombre de message
  * @param {type} add
  * @returns {undefined}
  */
 Comunity.UpdateNbMessage = function(add){
    
     // Ce N'est plus utilisé ?
    return; 
     var currentNumber = document.getElementById("nbCommentaire");
     if(add)
     {
         currentNumber.innerHTML = parseInt(currentNumber.innerHTML) + 1;
     }
     else
     {
         currentNumber.innerHTML = parseInt(currentNumber.innerHTML) -1;
     }
 };
    
/**
 * Supprimes l'élement selectionné
 * @returns {undefined}
 */
Comunity.ClearSelectedItem = function(){
    
  Comunity.MessageId = null;
  Comunity.Container = null;
  Comunity.Action = null;  
};

/**
 * Ajoute les event sur les div message
 * @returns {undefined}
 */
Comunity.AddEventMessage = function()
{
      /**
     * Commentaire sur les messages
     */
    Event.AddByClass("commentMessage", "click", function(e){
        
        if( Comunity.IsConnected)
        {
            var messageBlock = document.getElementById("messageBlock");
            messageBlock.style.display = "block";
            Comunity.MessageId = e.srcElement.parentNode.id;
            Comunity.MessageBlock = e.srcElement.parentNode;
            
        }
        else
        {
          Animation.Notify("Vous devez êtres connecté"); 
        }
      }, true);
      
    /**
     * Affiche les commentaires
     */
    Event.AddByClass("nbComment", "click", function(e){
         
        if( Comunity.IsConnected)
        {
        var data = "App=Comunity&Methode=GetComment";
              data += "&messageId="+  Comunity.GetMessageId(e.srcElement);
              
             var url = window.location.origin + "/Ajax.php";

             var lstComment = e.srcElement.parentNode.parentNode.parentNode.getElementsByClassName("lstComment");
              
              if(lstComment[0].innerHTML != ""){
                  lstComment[0].innerHTML = "";
                  return ;
              }
              
            Request.Post(url, data).then(data=>{ 
            
                var lstComment = e.srcElement.parentNode.parentNode.parentNode.getElementsByClassName("lstComment");
                    lstComment[0].innerHTML =  data;
                    
                    Comunity.MessageBlock = e.srcElement.parentNode;
                    
                    Comunity.AddEventMoreAction();
                    Comunity.AddEventComment();
            });
        }
        else
        {
          Animation.Notify("Vous devez êtres connecté"); 
        }
    }, true);
    
     /***
     * Like d'un message
     */
    Event.AddByClass("likeMessage", "click", function(e){
        
        if( Comunity.IsConnected)
        {
            var data = "App=Comunity&Methode=AddLike";
                data += "&messageId="+ Comunity.GetMessageId(e.srcElement);
                data += "&type="+ e.srcElement.dataset.type;
              
             var url = window.location.origin + "/Ajax.php";

            Request.Post(url, data).then(data=>{ 
               
               var likes = e.srcElement.parentNode.parentNode.getElementsByClassName("likes");
               likes[0].innerHTML = data;
                       
            });
        }
        else
        {
          Animation.Notify("Vous devez êtres connecté"); 
        }
    }, true);
    
    Event.AddByClass("btnComment", "click", function(e){
        
        if( Comunity.IsConnected)
        {
            var container = e.srcElement.parentNode.parentNode.parentNode;
            var commentBox = container.getElementsByClassName("commentBox");
          
            if( commentBox[0].style.display == "block")
            {    
                commentBox[0].style = "none";
            }
            else
            {
             
                    commentBox[0].style.display = "block";

                Comunity.containerSelected( container);
            }
        }
        else{
            Animation.Notify("Vous devez êtres connecté"); 
        }
     });
     
     /**
      * Pour se positionner sur le bon élement
      */
     Event.AddByClass("tbComment", "mousedown", function(e){
        if(Comunity.Action == null)
        {
            var container = e.srcElement.parentNode.parentNode.parentNode;
            Comunity.containerSelected(container);
        }
     });
     
     Event.AddById("tbMessage", "mousedown", function(){
        Comunity.ClearSelectedItem();
     });
     
     Comunity.AddEventComment();    
};

/**
 * Remonte jusqu'au parent pour trouver le message Id
 * @returns {undefined}*
 */
Comunity.GetMessageId = function(element)
 {
     var element = element;
     
     while(element.className != 'message'){
         element = element.parentNode;
     }
     
     return element.id;
 };
 
 /**
  * Récuper et initialise les bon élement
  * @param {type} e
  * @returns {undefined}
  */
 Comunity.containerSelected = function(container){
     
    var tbComment = container.getElementsByClassName("tbComment");  
    tbComment[0].focus();
            
    var nbComment =  container.getElementsByClassName("nbComment");     
    var lstComment =  container.getElementsByClassName("lstComment");  
   
    Comunity.MessageId = container.parentNode.id;
    Comunity.tbComment =  tbComment[0];
    Comunity.lstComment = lstComment[0];
    Comunity.nbComment = nbComment[0];
    Comunity.commentMessage = container;
 };

/**
 * Ajout des Event sur les more Actions
 * @returns {undefined}
 */
Comunity.AddEventMoreAction = function()
{
            
    Event.AddByClass("moreAction", "click", function(e){
        
        var actionDiv =  e.srcElement;
        Comunity.Container = actionDiv.parentNode; 
        Comunity.MessageId = actionDiv.parentNode.id;
        Comunity.Type =  e.srcElement.dataset.type;   
        
        if(Comunity.Type == "comment"){
            
            Comunity.MessageId = e.srcElement.id;
            Comunity.Container = e.srcElement.parentNode.parentNode; 
            
            var nbComment =  Comunity.Container.parentNode.parentNode.getElementsByClassName("nbComment");     
                
            Comunity.nbComment = nbComment[0];
    
           // Comunity.containerSelected(e.srcElement.parentNode.parentNode.parentNode.parentNode);  
        } else if( Comunity.Type == "message")
        {
           // Comunity.containerSelected(e.srcElement.parentNode.parentNode);
        }
    
        this.core = document.createElement('div');
        this.core.id = "actionMenu";
            this.core.style.position='absolute';
            this.core.style.width= "150px";
            this.core.style.height="150px";
            this.core.style.overflow="auto";
            this.core.style.border='1px solid grey';
            this.core.style.padding="5px";
            
            this.core.style.left= (e.clientX - 150 )+ "px";

            this.core.style.top= e.pageY + "px";
            this.core.style.backgroundColor="white";
            this.core.innerHTML="<div style='text-align:right;width:100%'><i class='fa fa-remove' alt='' title='Fermer' onclick='CloseTool(this)'></i></div>";

            this.core.innerHTML += "<span>Chargement...</span>";
            
             var data = "App=Comunity&Methode=GetMoreAction";
              data += "&messageId="+ e.srcElement.parentNode.id;
              data += "&type=" + Comunity.Type;
              
            var url = window.location.origin + "/Ajax.php";

            Request.Post(url, data).then(data=>{ 
            
                var content = this.core.getElementsByTagName("span");
                content[0].innerHTML = data;
                
                Event.AddById("editAction", "click", Comunity.EditAction );
                Event.AddById("removeAction", "click", Comunity.RemoveAction );
                Event.AddById("signalAction", "click", Comunity.SignalAction );
           
            });
            
            document.body.appendChild(this.core);
    }, true);
};

/**
 * Ferme le menu d'aide
 */
Comunity.CloseActionMenu = function()
{
    var actionMenu = document.getElementById("actionMenu");
        actionMenu.parentNode.removeChild(actionMenu);
};

/***
 * Affiche le block de
 * @returns {undefined}
 */
Comunity.ShowMessageBlock = function(message)
{
    var messageBlock = document.getElementById("messageBlock");
        messageBlock.style.display = "block";
        
    if(message != ""){
        
        var tbMessage = document.getElementById("tbMessage");
        tbMessage.value = message;
    }    
        
};
/**
 * Edite un message ou en commentaire
 * @param {type} e
 * @returns {undefined}
 */
Comunity.EditAction = function(e){
    
    Comunity.CloseActionMenu();
    Comunity.Action = "UpdateMessage";
      
    if(Comunity.Type == "message"){
      
        var message = Comunity.Container.getElementsByClassName("text");
        
        //Vérification si c'est un message type image 
        var paragraphe = message[0].getElementsByTagName("p");
        
        if(paragraphe.length > 0)
        {
            message = paragraphe;
        }
        
        var commentBox = Comunity.Container.getElementsByClassName("commentBox");
    } 
    else
    {
        var message = Comunity.Container.getElementsByTagName("p");
        
        if(Comunity.Container.parentNode.parentNode.parentNode.className == "lstComment"){
            var commentBox = Comunity.Container.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("commentBox");
        } else{
              var commentBox = Comunity.Container.parentNode.parentNode.parentNode.getElementsByClassName("commentBox");
        }
    
    }

    Comunity.EditMessage = message[0];
    
    //Comunity.MessageId = Comunity.Container.id;
    
    
    //Comunity.EditMessage.container = message[0].parentNode;
    
    commentBox[0].style.display = "block";
    
    var tbComment = commentBox[0].getElementsByClassName("tbComment");
        tbComment[0].value = Comunity.EditMessage.innerHTML;
        
        tbComment[0].focus();
    
    Comunity.tbComment = tbComment[0];
    
    //Comunity.EditMessage.style.display = "none";
 };

/**
 * Edite un message ou en commentaire
 * @param {type} e
 * @returns {undefined}
 */
Comunity.RemoveAction = function(e){
   
   Comunity.CloseActionMenu();
    
   Animation.Confirm(Language.GetCode("Comunity.deleteThisMessage"), function()
   {
        if(Comunity.Type == "message")
        {
            Comunity.UpdateNbMessage(false);
            var data = "App=Comunity&Methode=DeleteMessage";
        } 
        else if(Comunity.Type == "comment")
        {
            Comunity.UpdateNbComment(false);
            var data = "App=Comunity&Methode=DeleteComment";
        }
        
        data += "&messageId=" + Comunity.MessageId; 
             
          var url = window.location.origin + "/Ajax.php";

          Request.Post(url, data).then(data=>{
                if(data == "OK"){     
            
                 //Suppression de l'élement
                 Comunity.Container.parentNode.removeChild(Comunity.Container);
                 Comunity.ClearSelectedItem();
                }
            });
   });
};

/**
 * Signal au ambassadeur qu'une message n'est pas correct
 * @param {type} e
 * @returns {undefined}
 */
Comunity.SignalAction = function(e){
    
  Comunity.CloseActionMenu();
  Dialog.open("", {title:"Signaler un message", app:'Coopere', class:"Dialog" , method :"Signal", params: Comunity.Type + ":" + Comunity.MessageId, height : 250});
};


/**
 * Envoi un message
 * @returns {undefined}
 */
Comunity.SendMessage = function()
{
     var tbObjet = document.getElementById("tbObjet");
        var tbMessage = document.getElementById("tbMessage");
        var hdComunityId = document.getElementById("hdComunityId");
        var EventDiscus = document.getElementById("messages");
        var nbCommentaire = document.getElementById("nbCommentaire");
        var uploadImages = document.getElementById("uploadImages-fileToUpload");
        var hdUserId = document.getElementById("hdUserId");
        var images = new Array();   
        var tbMessage = document.getElementById("tbMessage");
        var documents = new Array();
          
        if(Comunity.MessageId == null && Comunity.CommentId == null)
        {
            
             var data = "App=Comunity&Methode=AddMessage";
                 data += "&message="+ tbMessage.value;
                 data += "&comunityId="+ hdComunityId.value;
                 data += "&type=multi";

            //Conversation entre user        
            if(hdUserId != undefined)
            {
                data += "&users="+ hdUserId.value;
            }
            
            if(uploadImages != null)
            {
                var imgs =  uploadImages.getElementsByTagName("img");   
                for(var i = 0; i < imgs.length ; i++ )
                {
                   images.push(imgs[i].src);
                }
            }
            data += "&images="+ images.join(";");
            
            //Document
            if(uploadImages != null)
            {
                var span =  uploadImages.getElementsByTagName("span");   
                for(var i = 0; i < span.length ; i++ )
                {
                    documents.push(span[i].innerHTML);
                }
            }
            data += "&documents="+ documents.join(";");
            
            var url = window.location.origin + "/Ajax.php";

            Request.Post(url, data).then(data=>{
              var data = JSON.parse(data);
              
              EventDiscus.innerHTML = data.message + EventDiscus.innerHTML;
              
              tbMessage.value = "";
              uploadImages.innerHTML = "";
              
              Comunity.AddEventMessage();
              Comunity.AddEventMoreAction();
              Comunity.ResizeImage();
              Comunity.AddEventComment();
            });
        } 
        else if( Comunity.Action == "UpdateMessage")
        {
            if(Comunity.Type == "message"){
                 var data = "App=Comunity&Methode=UpdateMessage";
                 var text = Comunity.Container.getElementsByClassName("text");
                 var imgs = text[0].getElementsByTagName("img");
           
            } 
            else if(Comunity.Type == "comment")
            {
                 var data = "App=Comunity&Methode=UpdateComment";
                 
                 var text = Comunity.Container.getElementsByTagName("p");
                 var imgs = text[0].getElementsByTagName("img");
            }
            
            Comunity.EditMessage.innerHTML = tbMessage.value ;
         
            var newMessage = Comunity.tbComment.value;;
           
            if(imgs.length > 0 )
            {
                newMessage = "<p>"+newMessage+ "</p>";
                for(var i = 0; i < imgs.length; i++)
                {
                    newMessage += "<img src='"+imgs[i].src+"' />" ;
                }
            }
            
            var a = text[0].getElementsByTagName("a");
            
            if(a.length > 0 )
            {
                newMessage = "<p>"+newMessage+ "</p>";
                for(var i = 0; i < a.length; i++)
                {
                    newMessage += "<a target='_blank' href='"+a[i].href+"' />" + a[i].innerHTML + "</a>" ;
                }
            }
             
                 data += "&newMessage="+  newMessage;
                 data += "&messageId="+ Comunity.MessageId;
              
            var url = window.location.origin + "/Ajax.php";

            Request.Post(url, data).then(data=>{

                
                Comunity.EditMessage.innerHTML = Comunity.tbComment.value;
                Comunity.EditMessage.style.display = "block";
                Comunity.tbComment.value ="";   
              
                uploadImages.innerHTML = "";
                
                Comunity.ClearSelectedItem(); 
            });
        } 
        //Commentaire sur un commentaire
        else if(Comunity.CommentId != null)
        {
             var data = "App=Comunity&Methode=AddCommentComment";
                 data += "&message="+ Comunity.tbComment.value;
                 data += "&commentId="+ Comunity.CommentId;
                 
             Request.Post(url, data).then(data=>{ 
            
                Comunity.lstComment.innerHTML =  data;
                Comunity.tbComment.value  = "";
                Comunity.commentCommentBox.parentNode.removeChild(Comunity.commentCommentBox); 
                 
                Comunity.commentContainer.innnerHTML = "NONONO";
                
                Comunity.AddEventMoreAction();
                //Comunity.AddEventMoreAction();
                //Comunity.UpdateNbComment(true);
                //Comunity.ClearSelectedItem(); 
            });
            
        }
        else if(Comunity.MessageId != "")
        {
             var data = "App=Comunity&Methode=AddComment";
                 data += "&message="+ Comunity.tbComment.value;
                 data += "&messageId="+ Comunity.MessageId;
             
            if(uploadImages != null)
            {
                 var imgs =  uploadImages.getElementsByTagName("img"); 
            
                for(var i = 0; i < imgs.length ; i++ )
                {
                   images.push(imgs[i].src);
                }
             }
            data += "&images="+ images.join(";");
            
            var url = window.location.origin + "/Ajax.php";

            Request.Post(url, data).then(data=>{ 
            
                Comunity.lstComment.innerHTML =  data;
                Comunity.tbComment.value  = "";
                if(uploadImages != null)
                {
                    uploadImages.innerHTML = "";
                }
                
                Comunity.AddEventMoreAction();
                Comunity.UpdateNbComment(true);
                Comunity.AddEventComment();
                //Comunity.ClearSelectedItem(); 
            });
        }
};

/***
 * Ajoute les évenements sur les commentaires
 * @returns {undefined}
 */
Comunity.AddEventComment = function()
{
    Event.AddByClass("likeComent", "click", function(){
        
    } , true);
    
    Event.AddByClass("commentComent", "click", function(e){
       
       Comunity.CommentId = e.srcElement.parentNode.dataset.comment;
        //Récuperation d'un commentBox
        var commentBox = document.getElementsByClassName("commentBox");
        
        var container = e.srcElement.parentNode;
           container.innerHTML += "<div class='commentCommentBox'>" + commentBox[0].innerHTML + "</div>";
       
        var tbComment = container.getElementsByClassName("tbComment");
        var lstComment = container.parentNode.getElementsByClassName("lstComment");
        var commentCommentBox = container.parentNode.getElementsByClassName("commentCommentBox");
        
       Comunity.tbComment = tbComment[0];
       Comunity.commentContainer = container;
       Comunity.lstComment = lstComment[0];
       Comunity.commentCommentBox = commentCommentBox[0];
       
    } , true);
    
    Event.AddByClass("likeComent", "click", function(e){
        
        Comunity.CommentId = e.srcElement.parentNode.dataset.comment;
        container = e.srcElement.parentNode.parentNode;
        
        var nbLikeComment = container.getElementsByClassName("nbLikeComment"); 
        
        var data = "App=Comunity&Methode=LikeComment";
            data += "&commentId="+ Comunity.CommentId;
                 
          var url = window.location.origin + "/Ajax.php";

            Request.Post(url, data).then(data=>{ 
            
            nbLikeComment[0].innerHTML = data;
        });
          
    }, true);
    
};
/**
 * Initialisation du widget de la page d'accueil
 * @returns {undefined}
 */
Comunity.InitWidget = function()
{
   var projetImage = document.getElementById("projetImage");
   var projet = projetImage.getElementsByTagName("div");
       projet[0].style.display = "block";
   var index = 0;
  
    setInterval(function(){
       
       projet[index].style.display = "none";
       
       index += 1;
       
       if(index == projet.length){
           index = 0;
       }
      
       projet[index].style.display = "block";
      
   } , 2000 );
};

 /**
* Show Overlay
* @param {type} e
* @returns {undefined}
*/
Comunity.ShowOverlay = function()
{
      var dialog = document.createElement('div');
          dialog.id ="overlay",
          dialog.className = "overlay";

   document.body.appendChild(dialog);
};

/***
* cache l'overlay
* @returns {undefined}
*/
Comunity.HideOverlay = function()
{
   var overlay = document.getElementById("overlay");
   document.body.removeChild(overlay);
};


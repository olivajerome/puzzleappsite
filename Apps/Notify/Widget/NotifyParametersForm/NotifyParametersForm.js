var NotifyParametersForm = function(){};

/***
 * Init the form
 * @returns {undefined}
 */
NotifyParametersForm.Init = function(){
    Event.AddById("btnSaveParameter", "click", NotifyParametersForm.SaveElement);
};

/***
* Get the Id of element 
*/
NotifyParametersForm.GetId = function(){
    return Form.GetId("NotifyParametersFormForm");
};

/*
* Save the Element
*/
NotifyParametersForm.SaveElement = function(e){
   
    if(Form.IsValid("NotifyParametersFormForm"))
    {

    let data = "Class=Notify&Methode=SaveParameter&App=Notify";
        data +=  Form.Serialize("NotifyParametersFormForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.status == "error"){
                Form.RenderError("NotifyParametersFormForm", data.data.message);
            } else{

                Form.SetId("NotifyParametersFormForm", data.data.Id);

                Dialog.Close();

                Notify.ReloadParameters(data);
            }
        });
    }
};


{{form->Open()}}
{{form->Error()}}
{{form->Success()}}

{{form->Render(Id)}}

    <div>
        <label>{{GetCode(EeApp.PluginName)}}</label> 
        {{form->Render(Name)}}
    </div>

    <div>
        <label>{{GetCode(EeApp.PluginDescription)}}</label> 
        {{form->Render(Description)}}
    </div>
    
    
    <div class='center marginTop' >   
        {{form->Render(btnSave)}}
    </div>  

{{form->Close()}}
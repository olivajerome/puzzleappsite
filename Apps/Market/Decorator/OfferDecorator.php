<?php

namespace Apps\Market\Decorator;

use Core\View\View;

use Apps\Market\Entity\MarketProduct;
use Apps\Market\Entity\MarketProductOffer;

class OfferDecorator{

    /***
     * Render Image of a product
     */
    public function RenderImageProduct($offer){

        $product = new MarketProduct($offer->Core);
        $product->GetById($offer->ProductId->Value);

        return "<img src='" . $product->GetImagePath() ."' style='width:100%'/>" ;
    }

    /***
     * Affiche les boutons selon le status
     */
    public function RenderButtonStatut($offer){

        switch($offer->Status->Value){
            case MarketProductOffer::SENDED :
                if($offer->UserId->Value == $offer->Core->User->IdEntite){
                    $view = new View(__DIR__."/View/waitAccept.tpl", $offer->Core);
                } else {
                    $view = new View(__DIR__."/View/acceptRefuse.tpl", $offer->Core);
                }
            break;
            case MarketProductOffer::REJECTED :
                if($offer->UserId->Value == $offer->Core->User->IdEntite){
                    $view = new View(__DIR__."/View/userRejected.tpl", $offer->Core);
                } else {
                    $view = new View(__DIR__."/View/rejected.tpl", $offer->Core);
                }
            break;
            case MarketProductOffer::ACCEPTED :
                if($offer->UserId->Value == $offer->Core->User->IdEntite){
                    $view = new View(__DIR__."/View/waitSendProduct.tpl", $offer->Core);
                } else {
                    $view = new View(__DIR__."/View/accepted.tpl", $offer->Core);
                }
            break;
            case MarketProductOffer::PRODUCT_SENDED :
                if($offer->UserId->Value == $offer->Core->User->IdEntite){
                    $view = new View(__DIR__."/View/confirmRecept.tpl", $offer->Core);
                } else {
                    $view = new View(__DIR__."/View/productSended.tpl", $offer->Core);
                }
            break;
             case MarketProductOffer::PRODUCT_RECEVEIVED :
                if($offer->UserId->Value == $offer->Core->User->IdEntite){
                    $view = new View(__DIR__."/View/productReceveid.tpl", $offer->Core);
                } else {
                    $view = new View(__DIR__."/View/userConfirmProductReceveid.tpl", $offer->Core);
                }
            break;
            case MarketProductOffer::PRODUCT_NO_RECEVEIVED :
                if($offer->UserId->Value == $offer->Core->User->IdEntite){
                    $view = new View(__DIR__."/View/productNotReceveid.tpl", $offer->Core);
                } else {
                    $view = new View(__DIR__."/View/userConfirmProductNotReceveid.tpl", $offer->Core);
                }
            break;
        }

        return $view->Render();
    }
}
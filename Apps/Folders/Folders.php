<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

 namespace Apps\Folders;

use Core\App\Application;
use Core\Core\Request;
use Core\Utility\File\File;

use Apps\Folders\Helper\FolderHelper;
use Apps\Folders\Module\Share\ShareController;
 
class Folders extends Application
{
	/**
	 * Auteur et version
	 * */
	public $Author = 'Webemyos';
	public $Version = '1.0.0';
   
	/**
	 * Constructeur
	 * */
	function __construct($core ="")
	{
		parent::__construct($core, "Folders");
		$this->Core = $core;
	}

	 /**
	  * Execution de l'application
	  */
	 function Run()
	 {
		echo parent::RunApp($this->Core, "Folders", "Folders");
	 }
     
     /***
      * Sauvegarde un dossier
      */
     function SaveFolder(){
         return FolderHelper::SaveFolder($this->Core, Request::GetPosts());
     }
     
     /***
      * Supprime un dossier et tous ces sous élements
      */
     function DeleteFolder(){
        return FolderHelper::DeleteFolder($this->Core, Request::GetPost("folderId")); 
     }
     
     /***
      * Sauvehgarde un fichier
      */
     function SaveFile(){
         return FolderHelper::SaveFile($this->Core, Request::GetPosts());
     }
     
     /***
      * Supprime un fichier
      */
     function DeleteFile(){
         return FolderHelper::DeleteFile($this->Core, Request::GetPost("fileId")); 
     }
     
     /***
      * Obtients les dossiers et fichiers d'un repertoire
      */
     function GetDirectory(){
          return FolderHelper::GetDirectory($this->Core, Request::GetPost("folderId"));
     }
     
     /***
      * Context menu d'ajout de repertoire ou de ficheir
      */
     function GetNewActionMenu(){
         
        $html = "<ul>";
        $html .= "<li id='mnAddFolder'><i class='fa fa-folder'>&nbsp;" .$this->Core->GetCode("Folder.Directory"). "</i></li>";
        $html .= "<li id='mnAddFile'><i class='fa fa-file'>&nbsp;" .$this->Core->GetCode("Folder.File"). "</i></li>";
        $html .= "</ul>";
        
        return $html;
     }
     
     /***
      * Partage d'un dossier
      */
     function GetShareMenu($params){
         
         $folderId = Request::GetPost("folderId");
         
         $shareController = new ShareController($this->Core);
         return $shareController->Share($folderId);
    }
    
    /***
      * Partage d'un dossier
      */
    function GetShareMenuFile($params){
         
         $fileId = Request::GetPost("fileId");
         
         $shareController = new ShareController($this->Core);
         return $shareController->ShareFile($fileId);
    }
    
    /***
     * Partage un dossier 
     */
    function ShareFolder(){
        return FolderHelper::ShareFolder($this->Core, 
                                        Request::GetPost("folderId"), 
                                        Request::GetPost("users"));
    }
    
     /***
     * Partage un fichier 
     */
    function ShareFile(){
        return FolderHelper::ShareFile($this->Core, 
                                        Request::GetPost("fileId"), 
                                        Request::GetPost("users"));
    }
    
    /***
     * Supprime un utilisateur d'un dossier
     */
    function DeleteUserFolder(){
        return FolderHelper::DeleteUserFolder($this->Core,
                                              Request::GetPost("userFolderId"));
    }
    
    /***
     * Supprime un utilisateur d'un fichier
     */
    function DeleteUserFile(){
        return FolderHelper::DeleteUserFile($this->Core,
                                              Request::GetPost("userFolderId"));
    }
    
    
    
      /**
     * Sauvegare les images de presentation
     */
    function DoUploadFile($idElement, $tmpFileName, $fileName, $action)
    {
        switch($action)
        {
            case "SaveDocument" :

                $directory = "Data/Tmp/Folders";

                File::CreateDirectory($directory);
                move_uploaded_file($tmpFileName, $directory."/".$fileName);
    
                echo "DOCUMENT:".$directory."/".$fileName.":";
                break;
       }
    }
    
    /***
     * Selectionne et affiche le bon widget
     */
    function GetWidget($type, $params){
        
        switch($type){
            case "GetShared" : 
                $widget = new \Apps\Folders\Widget\GetShared\GetShared($this->Core);
                break;
        }
        
        if(isset($widget)){
            return $widget->Index($params);
        }
    }
    
    /***
     * Charge les dossiers et fichiers partagé de l'utilisateur
     */
    function LoadSharedWithMe(){
        return FolderHelper::SharedWithMe($this->Core, Request::GetPost("folderId"));
    }
}
?>
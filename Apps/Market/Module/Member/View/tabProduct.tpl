
<section>
    <h1>{{GetCode(Market.MyProduct)}}</h1>
    
    {{GetControl(Button,btnAddProduct,{LangValue=Market.AddProduct,Id=btnAddProduct,})}}
    
    <div class='products marginTop'>

        {{foreach Products}}
            <div class='col-md-4'>
                <div class='block' id='{{element->IdEntite}}'>
                    <span style="display:block; width:100%; height: 150px; background-size: cover;background-image: url({{element->GetImagePath()}})">
                    </span>
                    <h2>{{element->Title->Value}}</h2>
                    <p>{{element->Description->Value}}</p>

                    <button id='{{element->IdEntite}}' class='btn btn-primary editProduct' >{{GetCode(Market.EditProduct)}}</button>

                </div>
            </div>     
        {{/foreach Products}}

    </div>

</section>

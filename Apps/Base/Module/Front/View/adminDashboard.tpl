<div class='col-md-4'>
    <div class='block'>
        <i class='fa fa-book title'>&nbsp;{{GetCode(Cms.Cms)}}</i>
       
        <input type='hidden' data-sourceName='pages' id='pages' value='{{pages}}' />
           <ul id='pagesContainer' data-source='pages'>
                <li class='model page ' style='display:none'  >
                    <input type='hidden' data-property = 'IdEntite'  />
                    <input type='hidden' data-property = 'Code'  />
                    <b data-property = 'Name' ></b> 
                    <div class='icones'>
                        <i class='fa fa-gear editPage' ></i>
                        <i class='fa fa-edit writePage' ></i>
                        <i class='fa fa-link openPage' ></i>
                        <i class='fa fa-trash deletePage' ></i>

                    </div>        
                </li>
            </ul>

        <div class='buttons'>
            <button id='btnAddPageAdminWidget' onclick="" class='btn btn-primary'>{{GetCode(Cms.NewPage)}}</button>
        </div>

    </div>
</div>


<?php

namespace Apps\Downloader\Widget\UploaderRessource;

use Core\Control\Form\Form;
use Core\View\View;
use Core\View\ElementView;



class UploaderRessource extends Form {

    /**
     * Constructeur
     */
    function __construct($core = "", $data = "") {
        $this->Core = $core;
    }
  /*     * *
     * Render the html form
     */

    function Render() {
       $view = new View(__DIR__ . "/View/index.tpl", $this->Core);
         
       return $view->Render();
    }
}

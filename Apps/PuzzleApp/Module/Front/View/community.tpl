<section class='container'>
    <div class='row'>
        <h1>La communauté</h1>
        <div class='col-md-6' >
            <div class='block height300 centerButton'>
                <h2>Le forum</h2>
                <p>
                    Vous êtes bloqué sur un point ? N'hesitez pas à consulter le forum et à poser vos questions    
                </p>
                    <a class='btn btn-primary' href='{{GetPath(/Forum)}}' /> Accèder au forum</a>
            </div>
        </div>
        <div class='col-md-6' >
            <div class='block height300 centerButton'>
                <h2>Le réseau social</h2>
                <p>
                    Rejoignez et participer sur notre réseau social, on parle de dev mais pas que.
                </p>
                    <a class='btn btn-primary' href='{{GetPath(/Comunity)}}' /> Accèder au réseau social</a>
            </div>
        </div>
    </div>
</section>        




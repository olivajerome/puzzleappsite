<?php 
/*
* PuzzleApp
* Webemyos
* Jérôme Oliva
* GNU Licence
*/

namespace Apps\Market\Entity;
use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class MarketCategory extends Entity  
{
	//Constructeur
	function __construct($core)
	{
		//Version
		$this->Version ="2.0.0.0"; 

		//Nom de la table 
		$this->Core=$core; 
		$this->TableName="MarketCategory"; 
		$this->Alias = "MarketCategory"; 

		$this->MarketId = new Property("MarketId", "MarketId", NUMERICBOX,  false, $this->Alias); 
		$this->Code = new Property("Code", "Code", TEXTBOX,  false, $this->Alias); 
		$this->Name = new Property("Name", "Name", TEXTBOX,  false, $this->Alias); 
		$this->Description = new Property("Description", "Description", TEXTAREA,  false, $this->Alias); 

		//Repertoire de stockage des images    
		$this->DirectoryImage = "Data/Apps/Market/Category/";

		//Creation de l entité 
		$this->Create(); 
	}

	 /***
     * Get Image Path
     */
    function GetImagePath(){
        
        if(file_exists($this->DirectoryImage . $this->IdEntite ."/full.png")){
             return $this->DirectoryImage . $this->IdEntite ."/full.png" ;
        } 
        
        return $this->Core->GetPath("/images/nophoto.png");
    }
}
?>
<section style='padding:10px'>
<div class='row'>
     <div class='col-md-2 tools'  >
         <h1>{{GetCode(Cms.Tools)}}</h1>
         <ul id='CmsTool'>
             <li class='cmsTool' data-tool='ThemeEditor'><i class='fa fa-barcode'></i>{{GetCode(Cms.Template)}}</li> 
             <li class='cmsTool' data-tool='LayoutEditor'><i class='fa fa-list'></i>{{GetCode(Cms.Layout)}}</li> 
             <li class='cmsTool' data-tool='StyleEditor'><i class='fa fa-pencil'></i>{{GetCode(Cms.Style)}}</li> 
             <li class='cmsTool' data-tool='ExportImportEditor'><i class='fa fa-gears'></i>{{GetCode(Cms.ExportImport)}}</li> 
             <li><i class='fa fa-book'></i>{{GetCode(Cms.Pages)}}
              <div class='iconTools'>
                 <i class='fa fa-plus' id="btnAddPage" title="{{GetCode(Cms.NewPage)}}"></i>
              </div>   
                <ul class='cmsTool'   data-tool='PagesEditor'>
                    {{foreach Pages}}
                        <li data-tool='PageEditor' class='editPage' id='{{element->IdEntite}}'>
                            {{element->Name->Value}}
                            <div class='iconTools'>
                            <i title='{{GetCode(Cms.EditPage)}}'  class='fa fa-edit btnEditPage' ></i>
                            <a href='/{{element->Name->Value}}' 
                               target='_blank' title='{{GetCode(Cms.ShowPage)}}'  class='fa fa-link btnOpenPage' ></a>
                            <i title='{{GetCode(Cms.RemovePage)}}'  class='fa fa-trash btnRemovePage' ></i>
                            </div>
                        </li>
                    {{/foreach Pages}}
                </ul>
             </li>
         </ul>
     </div>
     <div class='col-md-10 ' id='cmsWidget'>
         <div class='info'><i class='fa fa-info'></i>
            {{GetCode(Cms.DescriptionApp)}}
         </div>
     </div>
</div>
</section> 

<section class='global-sub-block'>
    <h1>{{GetCode(ContactUs)}}</h1>
      
   {{GetForm(/contact)}}
        <div class='form-group'>
            <label>{{GetCode(Email)}}</label>
            {{GetControl(EmailBox, tbEmail,{PlaceHolder=votre Email,Required=true})}}
        </div>
        <div class='form-group'>
            <label>{{GetCode(Name)}}</label>
            {{GetControl(TextBox, tbName,{PlaceHolder=votre Nom,Required=true})}}
        </div>
        <div class='form-group'>
            <label>{{GetCode(Message)}}</label>
            {{GetControl(TextArea, tbMessage,{PlaceHolder=votre Message,Required=true})}}
        </div>

        <div class='center'>
        {{if valide == false}}
            {{GetCode(Captcha.Incorrect)}}
        {{/if valide == false}}
        </div>
        <br/>
        
        {{GetWidget(Captcha)}}

        <br/>
        {{GetControl(Submit,btnSubmit,{Value=Send,CssClass=btn btn-success,)})}}
    {{CloseForm()}}
</section>   

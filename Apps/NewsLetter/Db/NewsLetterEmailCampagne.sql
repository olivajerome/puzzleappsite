CREATE TABLE IF NOT EXISTS `NewsLetterEmailCampagne` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`EmailId` int(11) NOT NULL, 
`Libelle` varchar(200)  NOT NULL,
`Description` TEXT  NOT NULL,
`DateCreated` DATE  NOT NULL,
`DateSended` DATE  DEFAULT NULL,
`EmailSended` INT  DEFAULT NULL,
PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
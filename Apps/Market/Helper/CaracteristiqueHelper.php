<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Market\Helper;

use Core\Utility\Format\Format;
use Core\Entity\Entity\Argument;

use Apps\Market\Entity\MarketCaracteristiqueItem;

class CaracteristiqueHelper {

    /*     * *
     * Get the items of a caracteristique
     */

    public static function GetItemByCaracteristique($core, $caracteristiqueId) {
        $item = new MarketCaracteristiqueItem($core);
        $items = $item->Find("CaracteristiqueId=" . $caracteristiqueId);

        return $item->ToAllArray($items);
    }
}
        
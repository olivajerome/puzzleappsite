{{form->Open()}}
{{form->Error()}}
{{form->Success()}}

{{form->Render(Id)}}

    <div>
        <label>{{GetCode(Notify.Title)}}</label> 
        {{form->Render(Title)}}
    </div>

    <div>
        <label>{{GetCode(Notify.Code)}}</label>
        {{form->Render(Code)}}
    </div>

    <div>
        <label>{{GetCode(Notify.Description)}}</label> 
        {{form->Render(Description)}}
    </div>

    <div>
        <label>{{GetCode(Notify.Content)}}</label> 
        {{form->Render(Content)}}
    </div>

    <div class='center marginTop' >   
        {{form->Render(btnSaveTemplate)}}
    </div>  

{{form->Close()}}
var View = function(){};

/***
 * OPn vied le container sauf le model
 * @param {type} controlId
 * @returns {undefined}
 */
View.Clear = function(controlId){
 
  var control = Dom.GetById(controlId);
  //On vide avant de remplir
  var oldChilds = control.childNodes;
  var nbChild =oldChilds.length;
  
  for(var k = nbChild; k > 0; k--){
      if(oldChilds[oldChilds.length - 1].className != undefined &&   oldChilds[oldChilds.length - 1].className.indexOf("model") < 0){
          control.removeChild(oldChilds[oldChilds.length - 1]);
        }
  }
};

/***
 * Charge un element HTML avec des datata
 * @param {type} controlId
 * @returns {undefined}
 */
View.Bind = function(controlId, dataSource){
  
  View.Clear(controlId);
  
  var control = Dom.GetById(controlId);
 
  if(dataSource != undefined){
     var data = dataSource;
  } else {
    var sourceId = control.dataset.source;
    var source = Dom.GetById(sourceId);
    var data = JSON.parse(source.value);
  }
  for(var i = 0; i < data.length; i++){
      
      var childs = control.getElementsByClassName("model");
      var model = childs[0];
      var newChild = model.cloneNode(true);
          newChild.className = newChild.className.replace("model", "");
          newChild.style.display = "block";  
      
      var subChild = newChild.childNodes;
      
      for(var j = 0; j < subChild.length; j++){
          
            if(subChild[j].dataset != undefined){
                
                    if(subChild[j].dataset.property != undefined){
                        if(subChild[j].type == "hidden"){
                            subChild[j].value = data[i][subChild[j].dataset.property];
                        } else{                        
                            subChild[j].innerHTML = data[i][subChild[j].dataset.property];
                        }
                    }
            }
      }
      
      control.appendChild(newChild);
  } 
};



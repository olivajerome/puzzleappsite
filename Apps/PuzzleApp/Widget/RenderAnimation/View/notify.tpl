<section>
    
    <div class='center marginTop'>
        
    {{GetControl(Button,btnNotify,{LangValue=Notify,CssClass=btn btn-primary,Id=btnNotify,)})}}
    {{GetControl(Button,btnConfim,{LangValue=Confirm,CssClass=btn btn-secondary,Id=btnConfim,)})}}
    {{GetControl(Button,btnAsk,{LangValue=Ask,CssClass=btn btn-secondary,Id=btnAsk,)})}}
    
    </div>
    
</section>

<script>
    
    Event.AddById("btnNotify", "click", ()=>{
    
        Animation.Notify("Hello ;)");
    });
    
    Event.AddById("btnConfim", "click", ()=>{
    
        Animation.Confirm("Do you like development ? ", function(){
            Animation.Notify("Good :)")
        }, function(){Animation.Notify("Why ? "); });
    });
    
    Event.AddById("btnAsk", "click", ()=>{
    
        Animation.Ask("How hold are you? ", function(reponse){
            Animation.Notify("You have " + reponse + " Years" );
        });
    });
    
    
    
</script>

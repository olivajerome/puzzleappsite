class TableTool extends BaseTool {
    getIcone() {
        return "<i class='fa fa-table'></i>";
    }

    getName() {
        return Language.GetCode("Base.TextRichEditorTable");
    }

    setEvent(events) {
        this.events = events;
    }
    execute(e) {
        e.preventDefault();

        let imgSource = e.srcElement;
        let instance = this;
        let node = window.getSelection().anchorNode;
        instance.editor.setSelectedNode(node);
   
        let view = "<div style='text-align : center'>";
        view += "<form id='tableForm'>"
        view += "<label>" + Language.GetCode("Base.NumerColum") + "</label>";
        view += "<input class='form-control' name='column', type='number' value='3'>";

        view += "<label>" + Language.GetCode("Base.NumerRow") + "</label>";
        view += "<input class='form-control' name='row' type='number' value='3'>";
      
        view += "<button class='btn btn-primary' style='margin-top:5px' id='btnAddTable'>" + Language.GetCode("Base.AppendTable") + "</button>";
        view += "</form></div>";

        this.editor.dialog(view, function () {
            Event.AddById("btnAddTable", "click", (e) => {

                e.preventDefault();
                let column = Form.GetValue("tableForm", "column");
                let row = Form.GetValue("tableForm", "row");
                
                instance.editor.appendTable(column, row);
           
                TextRichEditor.removeDialog();
            });
        });
    }
}
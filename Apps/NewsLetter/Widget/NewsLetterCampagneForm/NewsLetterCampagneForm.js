var NewsLetterCampagneForm = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
NewsLetterCampagneForm.Init = function(callBack){

    NewsLetterCampagneForm.CallBack = callBack;
   
    Event.AddById("btnSaveCampagne", "click", NewsLetterCampagneForm.SaveCampagne);
};

/***
* Obtient l'id de l'itin�raire courant 
*/
NewsLetterCampagneForm.GetId = function(){
    return Form.GetId("NewsLetterCampagneFormForm");
};

/*
* Sauvegare l'itineraire
*/
NewsLetterCampagneForm.SaveCampagne = function(e){
   
    if(Form.IsValid("NewsLetterCampagneFormForm"))
    {

    var data = "Class=NewsLetter&Methode=SaveCampagne&App=NewsLetter";
        data +=  Form.Serialize("NewsLetterCampagneFormForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.statut == "Error"){
                Form.RenderError("NewsLetterCampagneFormForm", data.message);
            } else{

                Dialog.Close();

                if (NewsLetterCampagneForm.CallBack) {
                    NewsLetterCampagneForm.CallBack(data);
                }
            }
        });
    }
};


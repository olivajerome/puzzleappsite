<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Message;

use Core\App\Application;
use Core\Core\Request;
use Core\Entity\User\User;
use Apps\Message\Module\Member\MemberController;
use Apps\Message\Helper\MessageHelper;
use Apps\Message\Entity\MessageMessage;
use Core\Utility\Format\Format;
use Core\Control\Image\Image;
use Core\Control\CheckBox\CheckBox;

class Message extends Application {

    /**
     * Auteur et version
     * */
    public $Author = 'Webemyos';
    public $Version = '2.1.0.0';

    /**
     * Constructeur
     * */
    function __construct($core = "") {
        parent::__construct($core, "Message");
        $this->Core = $core;
    }

    /*     * *
     * Execute action after install
     */

    function PostInstall() {
        \Apps\EeApp\Helper\AppHelper::UpdateAppParams($this->Core,
                "Message",
                $this->Version,
                "Application de messagerie",
                0,
                1
        );

        \Apps\EeApp\Helper\AppHelper::CreateDataDir("Message");
    }

    /**
     * Execution de l'application
     */
    function Run() {
        echo parent::RunApp($this->Core, "Message", "Message");
    }

    /*     * *
     * Run member Controller
     */

    function RunMember() {
        $memberController = new MemberController($this->Core);
        return $memberController->Index();
    }

    /**
     * Début d'une conversation avec un ou plusiers membres
     */
    function CreateMessage() {
        $memberController = new MemberController($this->Core);
        return $memberController->CreateMessage(Request::GetPost("user"));
    }

    /**
     * Lance une discussion
     */
    function StartMessage() {
        $memberController = new MemberController($this->Core);
        return $memberController->StartMessage(Request::GetPost("users"),
                        Request::GetPost("message"),
                        Request::GetPost("objet"),
                        Request::GetPost("discussId"),
                        Request::GetPost("images"),
                        0,
                        "",
                        Request::GetPost("documents"),
                        Request::GetPost("type"),
                        Request::GetPost("typeReponse"),
                        Request::GetPost("projetId")
                );
    }

    /**
     * Charge une discussion
     */
    function LoadMessage() {
        $memberController = new MemberController($this->Core);
        return $memberController->LoadMessage(Request::GetPost("discussId"));
    }

    /**
     * Supprime un message
     */
    function RemoveMessage() {
        return MessageHelper::RemoveMessage($this->Core,
                        Request::GetPost("messageId"));
    }

    /**
     * Met à jour un message
     */
    function UpdateMessage() {
        return MessageHelper::UpdateMessage($this->Core,
                        Request::GetPost("messageId"),
                        Request::GetPost("newMessage")
                );
    }

    /**
     * Met à jour un commentaire
     */
    function UpdateComment() {
        return MessageHelper::UpdateComment($this->Core,
                        Request::GetPost("messageId"),
                        Request::GetPost("newMessage")
                );
    }

    /**
     * Cloture la conversation
     */
    function RemoveConversation() {
        return MessageHelper::RemoveConversation($this->Core,
                        Request::GetPost("discussId"));
    }

    /**
     * Charge les images dans les conversations
     */
    function DoUploadFile($idElement, $tmpFileName, $fileName, $action) {
        switch ($action) {
            case "uploadMessage" :

                $file = explode(".", $fileName);

                //C'est un document
                if (in_array($file[1], array("txt", "pdf", "doc", "docx", "xlsx", "TXT", "PDF", "DOC", "DOCX", "XSLX"))) {
                    //Ajout de l'image dans le repertoire correspondant
                    $directory = "Data/Tmp/" . $file[0] . "." . $file[1];

                    echo "DOCUMENT:" . $directory . ":";

                    //Sauvegarde
                    move_uploaded_file($tmpFileName, $directory);

                    return;
                }

                //Ce n'est pas une image
                if (!in_array($file[1], array("jpg", "jpeg", "png", "bmp", "JPG", "PNG", "BMP", "JPEG"))) {
                    //  echo "ERROR";
                    //  return;
                }
                //Ajout de l'image dans le repertoire correspondant
                $directory = "Data/Tmp/";

                //Sauvegarde
                move_uploaded_file($tmpFileName, $directory . "/" . $idElement . ".jpg");

                echo $directory . "/" . $idElement . ".jpg";

                if (file_exists($directory . "/" . $idElement . ".jpg")) {
                    echo "OK";
                } else {
                    echo "ERROR";
                }

                break;
        }
    }

    /**
     * Créer une discussion type rencontre thématique
     */
    /*     public function CreateMessageRencontre()
      {
      $memberController = new MemberController($this->Core);
      return $memberController->StartMessage("",
      Request::GetPost("Description"),
      Request::GetPost("Title"),
      Request::GetPost("discussId"),
      Request::GetPost("imgs") ,
      2,
      Request::GetPost("reseau")
      );


      }
     */

    /**
     * Ajotu un commentaire sur un message
     */
    public function AddComment() {
        $commentId = MessageHelper::AddComment($this->Core,
                Request::GetPost("messageId"),
                Request::GetPost("message"),
                Request::GetPost("images")
        );

        return $this->GetComment();
    }

    /**
     * Obtient les commentaires d'un message
     */
    public function GetComment($commentId = "", $messageId = "") {
        if ($messageId != "") {
            $comments = MessageHelper::GetComment($this->Core, $messageId);
        } else if ($commentId == "") {
            $comments = MessageHelper::GetComment($this->Core, Request::GetPost("messageId"));
        } else {
            $comments = MessageHelper::GetComment($this->Core, "", $commentId);
        }
        $html = "";

        if (count($comments) > 0) {

            foreach ($comments as $comment) {
                $user = new User($this->Core);
                $user->GetById($comment->UserId->Value);

                $html .= "<section> ";
                $html .= "<div style='width: 70px; text-align: center; zoom: 0.7; top: -10px;position: relative;' title='" . $user->GetPseudo() . "' class='avatarmini-img avatarmini'>";
                $html .= "<img src='" . $user->GetImageMini() . "' /></div>";

                $html .= "<div class='commentMessageUser'  id='" . $comment->IdEntite . "'>";

                $html .= "<div class='moreAction' id='" . $comment->IdEntite . "' data-type ='comment' style='float:right'>...</div>";

                $html .= "<div><h6>" . $user->GetPseudo() . "</h6><p>" . $comment->Message->Value . "</p></div>";
                $html .= "</div>";

                if ($commentId == "") {


                    $html .= "<div class='actionComment' data-comment='" . $comment->IdEntite . "'><div class='likeComment' id='" . $comment->IdEntite . "' data-type ='comment' style=''><b class='nbLikeComment'>" . MessageHelper::GetNumberLike($this->Core, $comment->IdEntite) . "</b>&nbsp;<i class='fa fa-thumbs-o-up '>&nbsp;</i></div>";

                    $html .= "<b class='likeComent'> J'aime(s)</b><b class='commentComent' > - répondre</b></div>";
                    $html .= "<div class='lstComment'>" . $this->GetComment($comment->IdEntite) . "</div>";
                }

                $html .= "</section>";
            }
        }

        return $html;
    }

    /**
     * Supprime un message et tous les commentaires associés
     */
    public function DeleteMessage() {
        return MessageHelper::DeleteMessage($this->Core,
                        Request::GetPost("messageId"));
    }

    /*     * *
     * Supprime un commentaire
     */

    public function DeleteComment() {
        return MessageHelper::DeleteComment($this->Core,
                        Request::GetPost("messageId"));
    }

    /**
     * Retourne les actions supplémentaitre
     */
    public function GetMoreAction() {
        $html = "<ul>";

        if (MessageHelper::IsUserOrAmbassadeur($this->Core, Request::GetPost("messageId"), Request::GetPost("type"))) {

            if (Request::GetPost("type") == "message") {
                $message = new MessageMessage($this->Core);
                $message->GetById(Request::GetPost("messageId"));

                if ($message->Type->Value != 5) {
                    $html .= "<li ><i class='fa fa-edit' id='editAction' >&nbsp;Modifier</i></li>";
                }
            }

            $html .= "<li><i class='fa fa-trash' id='removeAction' >&nbsp;Supprimer</i></li>";
        }

        $html .= "<li><i class='fa fa-share' id='signalAction' >&nbsp;Signaler</i></li>";
        $html .= "</ul>";

        return $html;
    }

    /**
     * Plus d'action sur les messages
     */
    public function GetMoreActionMessage() {
        $html = "<ul>";
        $html .= "<li><i class='fa fa-trash' id='removeActionMessage' >&nbsp;".$this->Core->GetCode("Message.Archive")."</i></li>";
        $html .= "<li><i class='fa fa-eye' id='setAllView' >&nbsp;".$this->Core->GetCode("Message.SetAllView")."</i></li>";

        return $html;
    }

    /**
     * Recherche un utilisateur
     * @return type
     */
    public function SearchUser() {
        $searchValue = Request::GetPost("search");

        $user = new User($this->Core);
        $html = $this->SearchUserMessage($searchValue);

        return $html;
    }

    /*     * *
     * Rafraichit les discussions de l'utilisateur
     */

    public function RefreshMessage() {
        $memberController = new MemberController($this->Core);
        return $memberController->RefreshMessage();
    }

    /**
     * Obtient le nombre de message non lu
     * @return string
     */
    public function GetNumberNotRead() {
        return MessageHelper::GetNumberNotRead($this->Core);
    }

    /**
     * Retourne un module de Messagerie
     */
    public function GetWidget($type = "", $params = "") {

        switch ($type) {
            case "ContactMember" :
                $widget = new \Apps\Message\Widget\ContactMember\ContactMember($this->Core);
                break;
        }

        return $widget->Render($params);
    }

    /**
     * Ajotu d'un like sur une dissus ou un message
     */
    public function AddLike() {
        MessageHelper::AddLike($this->Core,
                Request::GetPost("messageId"),
                Request::GetPost("type")
        );

        $request = "Select GROUP_CONCAT(userLike.image SEPARATOR ':') as userLike 
                        FROM MessageLike as lk 
                        JOIN ee_user as userLike on userLike.Id = lk.UserId

                        WHERE lk.EntityId = " . Request::GetPost("messageId") . " and lk.EntityName = 'message' ";

        $result = $this->Core->Db->GetLine($request);

        if ($result["userLike"] != "") {
            $users = explode(":", $result["userLike"]);
            $userLikeHtml = "";

            foreach ($users as $user) {
                $userLikeHtml .= "<div class='avatarmini-img avatarmini'>
                        <img src='" . $this->Core->GetPath('\\' . $user) . "' /></div>";
            }
            $userLikeHtml .= "<div style='width:56px !important;margin-left:50px'> " . count($users) . " j'aime(s)</div>";

            return $userLikeHtml;
        } else {
            return " j'aime";
        }
    }

    /*     * *
     * Ajoute un message sur un commentaire
     */

    public function AddCommentComment() {
        $commentId = MessageHelper::AddComment($this->Core,
                Request::GetPost("commentId"),
                Request::GetPost("message"),
                "",
                "comment"
        );

        return $this->GetComment(Request::GetPost("commentId"));
    }

    /**
     * Ajout d'un like sur un comment
     */
    public function LikeComment() {
        MessageHelper::AddLike($this->Core,
                Request::GetPost("commentId"),
                "comment"
        );

        return MessageHelper::GetNumberLike($this->Core, Request::GetPost("commentId"));
    }

    /*     * *
     * Tout marqué comme vu
     */

    public function SetAllView() {
        $request = "Update MessageUser SET Readed = 2 WHERE UserId = " . $this->Core->User->IdEntite;
        $this->Core->Db->Execute($request);
    }

    /**
     *  Recherche les utilisateurs pour lancer une conversation
     * */
    public function SearchUserMessage($search) {
        $reseauCode = Request::GetSession("ReseauCode");

        $request = "  SELECT Id FROM `ee_user` WHERE (Name like '" . Format::EscapeString($search) . "%'";
        $request .= " OR FirstName like '" . Format::EscapeString($search) . "%' ";
        $request .= " OR Pseudo like '" . Format::EscapeString($search) . "%' )";

        //TODO Faire la jointure sur les user qui ne sont pas encore dans mes contacts
        $results = $this->Core->Db->GetArray($request);

        if (sizeof($results) > 0) {
            $TextControl = "<dt style='width:200px;text-align:left;'>";

            foreach ($results as $result) {
                //Reccuperation des informations du membre
                $user = new User($this->Core);
                $user->GetById($result["Id"]);

                $TextControl .= "<dl>";

                //Affichage de l'image en mini
                $imgUser = new Image($user->GetImageMini());
                $imgUser->AddStyle("width", "50px");

                //Ajout d'une case a cocher pour envoyer des invitations
                $cbUser = new CheckBox($result["Id"]);
                $cbUser->CssClass = "cbUser";
                $cbUser->Name = $user->GetPseudo();

                $TextControl .= "<div class='row'>";
                $TextControl .= "<div class='col-md-4'>";
                $TextControl .= "<div class='imgProfil' style='background-image: url(" . $user->GetImageMini() . ")'></div></div>";

//$TextControl .= "<div class='avatarmini-img avatarmini'>" .$imgUser->Show() . "</div></div>";
                $TextControl .= "<div class='col-md-8 userName'>" . $user->GetPseudo() . $cbUser->Show() . "</div>";

                $TextControl .= "</dl>";
            }

            $TextControl .= "<dt>";
        } else {
            $TextControl = "<span  class='FormUserError''>" . $this->Core->GetCode("NoMember") . "</span>";
        }

        $TextControl .= "<br/>";
        echo $TextControl;
    }
}

?>
<section>
    
    {{foreach Plugins}}

     <div class='row'>
        <label class='col-md-3'>{{element->Name->Value}}</label>
        <p class='col-md-3'>{{element->GetVersion()}}</p>
        <p class='col-md-3'>{{GetControl(Button,btnCreateVersionPlugin,{Id=btnCreateVersionPlugin,CssClass=btn btn-primary btnVersionPlugin,LangValue=PuzzleApp.CreateVersionPlugin})}}</p>
        </div>

    {{/foreach Plugins}}

</section>
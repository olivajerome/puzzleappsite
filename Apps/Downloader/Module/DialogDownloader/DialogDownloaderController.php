<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Downloader\Module\DialogDownloader;

use Core\Control\Button\Button;
use Core\Controller\Controller;
use Core\View\View;

use Apps\Downloader\Widget\UserRessourceForm\UserRessourceForm;

/*
 * 
 */
 class DialogDownloaderController extends Controller
 {
    /**
     * Constructeur
     */
    function _construct($core="")
    {
          $this->Core = $core;
    }

    /**
     * Create
     */
    function Create()
    {
    }

    /**
     * Init
     */
    function Init()
    {
    }

    /**
     * Render
     */
    function Show($all=true)
    {
         return $this->Index();
    }
    
    /*
    * Get the home page
    */
   function Index()
   {
       $view = new View(__DIR__."/View/index.tpl", $this->Core);
       return $view->Render();
   }

   /***
    * Upload Ressouces User
    */
   function ShowAddRessource(){

    if($this->Core->IsConnected()){
   
        $userRessourceForm = new UserRessourceForm($this->Core);
        return $userRessourceForm->Render();
   
    } else {
        return $this->Core->GetCode("Downloader.MustBeConnectedToProposeRessource");     
    }
   }
          
          /*action*/
 }?>
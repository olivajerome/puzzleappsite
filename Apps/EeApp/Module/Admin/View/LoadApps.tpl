<div class="content-panel">
      {{GetControl(Button,AddApp,{LangValue=EeApp.AddApp,CssClass=btn btn-info,OnClick=EeAppAction.ShowAddApp()})}}
      {{GetControl(Button,UploadApp,{LangValue=EeApp.UploadApp,CssClass=btn btn-info,OnClick=EeAppAction.ShowUploadApp()})}}
      {{GetControl(Button,UploadLanguage,{LangValue=EeApp.UploadLanguage,CssClass=btn btn-info,OnClick=EeAppAction.ShowUploadLanguage()})}}
      {{GetControl(Button,AddPlugin,{LangValue=EeApp.AddPlugin,CssClass=btn btn-success,OnClick=EeAppAction.ShowAddPlugin()})}}
      {{GetControl(Button,AddTemplate,{LangValue=EeApp.AddTemplate,CssClass=btn btn-success,OnClick=EeAppAction.ShowAddTemplate()})}}

      {{GetControl(Button,UpdateVersion,{LangValue=EeApp.UploadVersion,CssClass=btn btn-warning,OnClick=EeAppAction.ShowUpdateVersion()})}}

        <table class="table">
            <thead>
                <tr>
                  <th>{{GetCode(Category)}}</th>
                  <th>{{GetCode(Name)}}</th>
                  <th>{{GetCode(Description)}}</th>
                  <th>{{GetCode(Version)}}</th>
                </tr>
            </thead>
                {{foreach}}
                    <tr>
                        <td>{{element->Category->Value->Name->Value}}</td>
                        <td>{{element->Name->Value}}</td>
                        <td>{{element->Description->Value}}</td>
                        <td>{{element->GetVersion()}}</td>
                        <td>
                            {{GetControl(EditIcone,Serveur,{OnClick=EeAppAction.ShowAddApp({{element->IdEntite}})})}}
                            {{GetControl(GroupIcone,Serveur,{OnClick=EeAppAction.ShowAdmin({{element->IdEntite}})})}}
                            {{GetControl(DeleteIcone,Serveur,{Id={{element->IdEntite}},OnClick=EeAppAction.RemoveApp(this)})}}
                        </td>
                    </tr>
                {{/foreach}}
                </tbody>
            </table>
        </div>
</div>

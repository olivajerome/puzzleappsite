<section>
   
    <input type='hidden' value='{{Page->IdEntite}}' id='PageId'/> 
    <h1> {{GetCode(Cms.PageName)}} : <span id='pageName' contenteditable>{{Page->Name->Value}}</span></h1>
    <input type='hidden' id='pageId' value='{{Page->IdEntite}}'>
    <div class='pageIconTools'>
        <i class='fa fa-plus ' id="btnAddContainer" title='{{GetCode(Cms.AddContainer)}}'></i>
    </div>    
    <div class=''>
        {{foreach Containers}}   
            <div id='{{element->IdEntite}}' class='row subBlockContainer editBlock'>
                <div class='tools'>
                    <i class='fa fa-edit btnEditContainer' title='{{GetCode(Cms.EditContainer)}}'></i>
                    <i class='fa fa-plus btnAddBlock' title='{{GetCode(Cms.AddBlock)}}'></i>
                    <i class='fa fa-trash btnRemoveContainer' title='{{GetCode(Cms.RemoveContainer)}}'></i>
                </div>
                {{element->RenderBlocks()}}
            </div>     
        {{/foreach Containers}}
    </div>
</section>

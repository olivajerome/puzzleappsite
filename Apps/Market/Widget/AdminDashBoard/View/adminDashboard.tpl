<div class='col-md-5'>
    <div class='block widgetDashboard' style='height:250px;overflow:auto;'>
        <i class='fa fa-trash removeWidget' data-app='Market' title='{{GetCode(EeApp.RemoveWidgetDahboard)}}'></i>
        <i class='fa fa-listore title'>&nbsp;{{GetCode(Market.Market)}}</i>
       
        <dl style='height:calc(45%))'>
            <li>{{OffersSended}} {{GetCode(Market.OffersSended)}}</li>
            <li>{{OffersAccepted}} {{GetCode(Market.OffersAccepted)}}</li>
            <li>{{OffersRejected}} {{GetCode(Market.OffersRejected)}}</li>
        </dl>

        <dl style='height:calc(45%))'>
            {{foreach Products}}
                <li>  
                   {{element->Title->Value}} 
                    <a target='_blank' href='{{GetPath(/Market/Product/{{element->Code->Value}})}}'
                    <i class='fa fa-link'></i>
                    </a>
                </li>
            {{/foreach Products}}
        </dl>

        
    </div>
</div>

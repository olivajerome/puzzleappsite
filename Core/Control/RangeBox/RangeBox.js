var rangeBox = new RangeBox();

function RangeBox(controlId)
{
    this.controlId = controlId;

    this.Init = function(){
        this.clignotant = document.querySelector('.clignotant');
        this.slider = document.getElementById(this.controlId);

        if (this.slider != null) {
            this.selector = document.getElementById('s' + this.controlId);
            this.bFiabilite = document.getElementById("b" + this.controlId);
            this.progressBar = document.getElementById('progressBar' + this.controlId);

            const instance = this;

            this.slider.addEventListener('input', function (e) {
                instance.updateSliderPosition();
                instance.updateValueText();
            });

            this.updateSliderPosition();
            this.updateValueText();
        }
    };

    this.updateSliderPosition = function () {
        const value = this.slider.value;
        const newValue = value * (this.slider.offsetWidth - 15) / this.slider.max;

        this.selector.style.left = `${newValue}px`;
        this.progressBar.style.width = `${newValue}px`;
    };

    this.updateValueText = function () {
       const value = this.slider.value;
        this.bFiabilite.innerHTML = value + "";
    };
}
;
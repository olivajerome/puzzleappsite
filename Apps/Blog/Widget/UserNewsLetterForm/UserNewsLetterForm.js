var UserNewsLetterForm = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
UserNewsLetterForm.Init = function(){
    Event.AddById("btnSubscribe", "click", UserNewsLetterForm.Subscribe);
};

/***
* Obtient l'id de l'itin�raire courant 
*/
UserNewsLetterForm.GetId = function(){
    return Form.GetId("UserNewsLetterFormForm");
};

/*
* Sauvegare l'itineraire
*/
UserNewsLetterForm.Subscribe = function(e){
   
    if(Form.IsValid("UserNewsLetterFormForm"))
    {

    var data = "Class=Blog&Methode=Subscribe&App=Blog";
        data +=  Form.Serialize("UserNewsLetterFormForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);
            Animation.Notify(data.data.Message)    
        });
    }
};


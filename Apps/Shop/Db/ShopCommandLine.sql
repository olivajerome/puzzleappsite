CREATE TABLE IF NOT EXISTS `ShopCommandLine` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`CommandId` INT  NOT NULL,
`EntityName` VARCHAR(200)  NOT NULL,
`EntityId` INT  NOT NULL,
`Quantity` INT  NOT NULL,
`Price` float  NOT NULL,
`Data` text  NULL,


PRIMARY KEY (`Id`),
CONSTRAINT `Command_CommandLine` FOREIGN KEY (`CommandId`) REFERENCES `ShopCommand`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
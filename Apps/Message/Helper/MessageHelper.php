<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Apps\Message\Helper;

use Apps\Message\Entity\MessageConversation;
use Apps\Message\Entity\MessageMessage;
use Apps\Message\Entity\MessageUser;
use Apps\Message\Entity\MessageComment;
use Apps\Message\Entity\MessageLike;
use Core\Utility\Date\Date;
use Core\Entity\Entity\Argument;
use Apps\Coopere\Entity\CoopereReseau;
use Core\Utility\File\File;
use Apps\Message\Entity\MessageFollow;
use Apps\Coopere\Helper\MembreHelper;
use Core\Dashboard\DashBoardManager;
use Apps\Annonce\Entity\AnnonceAnnonce;

/* * *
 * Class utilitaire 
 */

class MessageHelper {

    public static function GetUser() {
        
    }

    /**
     * Créer une conversation
     * @param type $core
     * @param type $title
     */
    public static function CreateConversation($core, $users, $message, $title = "", $type = 1, $reseauCode = "", $projetId = "") {
        //Conversation a plusieurs
        if ($users != "") {

            $request = "SELECT GROUP_CONCAT(Pseudo) as pseudo  FROM ee_user WHERE ID in(" . $users . ")";
            $result = $core->Db->GetLine($request);

            if ($title == "") {
                $title = $result["pseudo"];
            }
        }

        $conversation = new MessageConversation($core);
        $conversation->UserId->Value = $core->User->IdEntite;
        //  $conversation->Title->Value = $title; 
        //  $conversation->SubTitle->Value = $message; 
        $conversation->DateCreated->Value = Date::Now();
        //$conversation->DateLastMessage->Value = Date::Now(true);
        $conversation->Type->Value = $type;
        $conversation->Status->Value = 1;
        $conversation->Save();
        $conversationId = $core->Db->GetInsertedId();

        //Ajoute les utilisateurs
        $conversationMember = new MessageUser($core);
        $conversationMember->ConversationId->Value = $conversationId;
        $conversationMember->UserId->Value = $core->User->IdEntite;
        $conversationMember->Status->Value = 1;
        $conversationMember->Readed->Value = 2;
        $conversationMember->Save();

        if ($users != "") {
            foreach (explode(",", $users) as $user) {
                $conversationMember = new MessageUser($core);
                $conversationMember->ConversationId->Value = $conversationId;
                $conversationMember->UserId->Value = $user;
                $conversationMember->Status->Value = 1;
                $conversationMember->Readed->Value = 1;
                $conversationMember->Save();
            }
        }

        //Création du premier message
        $discusMessage = new MessageMessage($core);
        $discusMessage->UserId->Value = $core->User->IdEntite;
        $discusMessage->DateCreated->Value = Date::Now();
        $discusMessage->Message->Value = $message;
        $discusMessage->Type->Value = 1;
        $discusMessage->ConversationId->Value = $conversationId;
        $discusMessage->Save();

        return $discussId;
    }

    /**
     * Ajoute un message à la disccusion 
     */
    public static function AddMessage($core, $conversationId, $message, $type = 1) {
        $discusMessage = new MessageMessage($core);
        $discusMessage->UserId->Value = $core->User->IdEntite;
        $discusMessage->DateCreated->Value = Date::Now();
        $discusMessage->Message->Value = $message;
        $discusMessage->Type->Value = $type;
        $discusMessage->ConversationId->Value = $conversationId;
        //$discusMessage->Status->Value = 1;

        $discusMessage->Save();

        return $core->Db->GetInsertedId();
    }

    /**
     * Obtient les conversation active de l'utilisateur
     * @param type $core
     */
    public static function GetConversationActive($core) {
        $request = "SELECT group_concat(ConversationId) as conversationIds FROM MessageUser";
        $request .= " JOIN MessageConversation as conversation on conversation.Id = MessageUser.ConversationId  ";
        $request .= "WHERE MessageUser.Status = 1 AND MessageUser.UserId = " . $core->User->IdEntite;

        $result = $core->Db->GetLine($request);

        $conversation = new MessageConversation($core);

        if ($result["conversationIds"] != null) {
            return $conversation->Find("Id in (" . $result["conversationIds"] . ") Order By DateLastMessage desc ");
        } else {
            return array();
        }
    }

    /*     * *
     * Obitent les discussions actives
     */

    public static function GetRencontreActive($core, $reseauCode) {
        $reseau = new CoopereReseau($core);
        $reseau = $reseau->GetByCode($reseauCode);

        $request = "SELECT group_concat(MessageId) as discussIds FROM MessageUser";
        $request .= " JOIN MessageMessage as discuss on discuss.Id = MessageUser.MessageId  WHERE MessageUser.Status = 1 AND MessageUser.UserId = " . $core->User->IdEntite;
        $request .= " AND discuss.ReseauId = " . $reseau->IdEntite;

        $result = $core->Db->GetLine($request);

        $discuss = new MessageMessage($core);

        if ($result["discussIds"] != null) {
            return $discuss->Find("Id in (" . $result["discussIds"] . ") Order By Id desc ");
        } else {
            return array();
        }
    }

    /**
     * Obtient les message de la conversation
     * @param type $core
     * @param type $discussId
     */
    public static function GetMessages($core, $discussId) {
        $discusMessage = new MessageMessage($core);
        $discusMessage->AddArgument(new Argument("Apps\Message\Entity\MessageMessage", "ConversationId", EQUAL, $discussId));

        return $discusMessage->GetByArg();
    }

    /**
     * Supprime un message 
     * @param type $core
     * @param type $messageId
     */
    public static function RemoveMessage($core, $messageId) {
        $discusMessage = new MessageMessage($core);
        $discusMessage->GetById($messageId);
        $discusMessage->Delete();
    }

    /**
     * Met à jour un message
     */
    public static function UpdateMessage($core, $messageId, $newMessage) {
        $discusMessage = new MessageMessage($core);
        $discusMessage->GetById($messageId);
        $discusMessage->Message->Value = $newMessage;

        $discusMessage->Save();
    }

    /**
     * Met à jour un message
     */
    public static function UpdateComment($core, $messageId, $newMessage) {
        $discusMessage = new MessageComment($core);
        $discusMessage->GetById($messageId);
        $discusMessage->Message->Value = $newMessage;
        $discusMessage->Save();
    }

    /**
     * Cloture la conversation
     */
    public static function RemoveConversation($core, $discussId) {
        $discuss = new MessageConversation($core);
        $discuss->GetById($discussId);

        if ($discuss->UserId->Value == $core->User->IdEntite) {
            $discuss->Status->Value = 2;
            $discuss->DateClotured->Value = Date::Now();
            $discuss->Save();

            $request = " UPDATE MessageUser Set Status = 2 WHERE ConversationId =   " . $discussId;

            $core->Db->Execute($request);
        }
        //On clot que pour Moi 
        else {
            $request = " UPDATE MessageUser Set Status = 2 WHERE ConversationId =   " . $discussId . " and UserId = " . $core->User->IdEntite;
            $core->Db->Execute($request);
        }
    }

    /**
     * Obtient les rencontres thématiques d'un reseau
     */
    public static function GetLastRencontreByReseau($core, $reseauCode, $status = "", $limit = true) {
        $reseau = new CoopereReseau($core);
        $reseau = $reseau->GetByCode($reseauCode);

        $discuss = new MessageMessage($core);
        $discuss->AddArgument(new Argument("Apps\Message\Entity\MessageMessage", "ReseauId", EQUAL, $reseau->IdEntite));
        $discuss->AddArgument(new Argument("Apps\Message\Entity\MessageMessage", "Type", NOTEQUAL, 4));

        if ($status == "") {
            $discuss->AddArgument(new Argument("Apps\Message\Entity\MessageMessage", "Status", EQUAL, 1));
        } else {
            $discuss->AddArgument(new Argument("Apps\Message\Entity\MessageMessage", "Status", EQUAL, $status));
        }

        $discuss->AddOrder("Id desc");

        if ($limit) {
            $discuss->SetLimit(1, 3);
        }

        return $discuss->GetByArg();
    }

    /**
     * Permet de suivres les conversations de rencontres thématiques
     * @param type $core
     * @param type $disccusId
     */
    public static function ParticipateProjet($core, $disccusId) {
        if (!self::Exist($core, $disccusId)) {
            $discussMember = new MessageUser($core);
            $discussMember->UserId->Value = $core->User->IdEntite;
            $discussMember->MessageId->Value = $disccusId;
            $discussMember->Status->Value = 1;
            $discussMember->Readed->Value = 2;
            $discussMember->Save();
        }

        $discuss = new MessageMessage($core);
        $discuss->GetById($disccusId);

        //Un membre rejoint le projet il passe donc en Actifs
        if ($discuss->Status->Value == 0) {
            $request = "Update MessageMessage SET Status =  1 WHERE Id = " . $disccusId;
            $core->Db->Execute($request);
        }

        //Il faut aussi que supprimer le memebre de la disccussion
        $discuss = new MessageMessage($core);
        $discuss->AddArgument(new Argument("Apps\Message\Entity\MessageMessage", "ProjetId", EQUAL, $disccusId));
        $discuss = $discuss->GetByArg();

        if (count($discuss) > 0) {
            $discussMember = new MessageUser($core);
            $discussMember->UserId->Value = $core->User->IdEntite;
            $discussMember->MessageId->Value = $discuss[0]->IdEntite;
            $discussMember->Status->Value = 1;
            $discussMember->Readed->Value = 2;
            $discussMember->Save();
        }
    }

    /*
     * Ne plue participer
     */

    public static function UnParticipateProjet($core, $disccusId) {
        $discussMember = new MessageUser($core);
        $discussMember->AddArgument(new Argument("Apps\Message\Entity\MessageUser", "UserId", EQUAL, $core->User->IdEntite));
        $discussMember->AddArgument(new Argument("Apps\Message\Entity\MessageUser", "MessageId", EQUAL, $disccusId));

        $members = $discussMember->GetByArg();

        $members[0]->Delete();

        //Il faut aussi que supprimer le memebre de la disccussion
        $discuss = new MessageMessage($core);
        $discuss->AddArgument(new Argument("Apps\Message\Entity\MessageMessage", "ProjetId", EQUAL, $disccusId));
        $discuss = $discuss->GetByArg();

        if (count($discuss) > 0) {
            $discussMember = new MessageUser($core);
            $discussMember->AddArgument(new Argument("Apps\Message\Entity\MessageUser", "UserId", EQUAL, $core->User->IdEntite));
            $discussMember->AddArgument(new Argument("Apps\Message\Entity\MessageUser", "MessageId", EQUAL, $discuss[0]->IdEntite));
            $discussProjet = $discussMember->GetByArg();

            if (count($discussProjet) > 0) {
                $discussProjet[0]->Delete();
            }
        }
    }

    /**
     * Verifie si on suit dèjà une renconctre
     */
    public static function Exist($core, $discussId) {
        $discussMember = new MessageUser($core);
        $discussMember->AddArgument(new Argument("Apps\Message\Entity\MessageUser", "UserId", EQUAL, $core->User->IdEntite));
        $discussMember->AddArgument(new Argument("Apps\Message\Entity\MessageUser", "MessageId", EQUAL, $discussId));

        $members = $discussMember->GetByArg();

        return (count($members) > 0);
    }

    /**
     * Permet de suivre une conversation
     */
    public static function AddFollow($core, $disccusId) {
        if (!self::UserFollow($core, $disccusId)) {
            $discussFollow = new MessageFollow($core);
            $discussFollow->UserId->Value = $core->User->IdEntite;
            $discussFollow->MessageId->Value = $disccusId;
            $discussFollow->Save();
        }
    }

    /*     * *
     * L'utilisateur suit le discuss
     */

    public function UserFollow($core, $projetId) {
        $discussFollow = new MessageFollow($core);
        $discussFollow->AddArgument(new Argument("Apps\Message\Entity\MessageFollow", "UserId", EQUAL, $core->User->IdEntite));
        $discussFollow->AddArgument(new Argument("Apps\Message\Entity\MessageFollow", "MessageId", EQUAL, $projetId));

        return (count($discussFollow->GetByArg()) > 0);
    }

    /*
     * Le membre participe
     */

    public function UserParticipate($core, $projetId) {
        $discussUser = new MessageUser($core);
        $discussUser->AddArgument(new Argument("Apps\Message\Entity\MessageUser", "UserId", EQUAL, $core->User->IdEntite));
        $discussUser->AddArgument(new Argument("Apps\Message\Entity\MessageUser", "MessageId", EQUAL, $projetId));

        return (count($discussUser->GetByArg()) > 0);
    }

    /*     * *
     * Ne plus suivre le projet
     */

    public function UnFollowProjet($core, $projetId) {
        $request = "DELETE FROM MessageFollow WHERE MessageId = " . $projetId . " AND UserId = " . $core->User->IdEntite;
        $core->Db->Execute($request);
    }

    /**
     * Ajoute un commentaire à un message
     * @param type $core
     * @param type $messageId
     * @param type $message
     */
    public static function AddComment($core, $messageId, $message, $images, $commentType = "message") {
        $comment = new MessageComment($core);
        if ($commentType == "message") {
            $comment->MessageId->Value = $messageId;

            //Envoi d'une notification
            $discusMessage = new MessageMessage($core);
            $discusMessage->GetById($messageId);

            if ($discusMessage->UserId->Value != $core->User->IdEntite) {

                $notify = DashBoardManager::GetApp("Notify", $core);
                $notify->AddNotify($core->User->IdEntite, "AddComment", $discusMessage->UserId->Value, "Message", $messageId);
            }
        } else {
            $comment->ParentId->Value = $messageId;
        }
        $comment->Message->Value = $message;
        $comment->UserId->Value = $core->User->IdEntite;
        $comment->DateCreated->Value = Date::Now();
        $comment->Save();

        $commentId = $core->Db->GetInsertedId();
        $request = "SELECT MessageId as discussId from MessageMessage WHERE Id =" . $messageId;
        $result = $core->Db->GetLine($request);

        $basePath = "Data/Apps/Message/" . $result["discussId"] . "/Message/";

        if ($images != "") {
            $images = explode(";", $images);
            $messageImage = "";

            // var_dump($images);
            //On déplace les images dans le bon répertoire
            foreach ($images as $image) {
                $path = explode("/", $image);

                if (!file_exists($basePath)) {
                    mkdir($basePath);
                }

                $basePath .= $messageId;

                if (!file_exists($basePath)) {
                    mkdir($basePath);
                }

                $basePath .= "/Comment";

                //TODO RECUPERE L'ID DE L4UTILISATEUR SI C'EST UNE INSCIRPTIon
                if (!file_exists($basePath)) {
                    mkdir($basePath);
                }

                $basePath .= "/" . $commentId;

                //TODO RECUPERE L'ID DE L4UTILISATEUR SI C'EST UNE INSCIRPTIon
                if (!file_exists($basePath)) {
                    mkdir($basePath);
                }
                $fileName = $path[count($path) - 1];

                rename("Data/Tmp/" . $fileName, $basePath . "/" . $fileName);

                $messageImage .= "<img src='" . $core->GetPath("/" . $basePath . "/" . $fileName) . "' >";
            }

            $message = $messageImage . $message;

            MessageHelper::UpdateComment($core, $commentId, $message);

            //Env
        }

        return $commentId;
    }

    /**
     * Obtient les commentaires
     */
    public function GetComment($core, $messageId, $commentId = "") {
        $comment = new MessageComment($core);
        if ($commentId != "") {
            $comment->AddArgument(new Argument("Apps\Message\Entity\MessageComment", "ParentId", EQUAL, $commentId));
        } else {
            $comment->AddArgument(new Argument("Apps\Message\Entity\MessageComment", "MessageId", EQUAL, $messageId));
        }
        return $comment->GetByArg();
    }

    /*     * *
     * Supprime un message et tous les commentaires associé
     */

    public static function DeleteMessage($core, $messageId) {

        $request = "DELETE FROM MessageComment WHERE MessageId = " . $messageId;
        $core->Db->Execute($request);

        $message = new MessageMessage($core);
        $message->GetById($messageId);

        //Suppression des images
        File::RemoveAllDir("Data/Apps/Message/" . $message->MessageId->Value . "/Message/" . $messageId);

        $message->Delete();

        return "OK";
    }

    /*     * *
     * Supprime un commentaire
     */

    public static function DeleteComment($core, $messageId) {
        //Suppression des commentaire de commentaire
        $request = "DELETE FROM MessageComment WHERE ParentId = " . $messageId;
        $core->Db->Execute($request);

        $message = new MessageComment($core);
        $message->GetById($messageId);
        //$message->Delete();

        $messageParent = new MessageMessage($core);
        $messageParent->GetById($message->MessageId->Value);
        //Suppression des images
        File::RemoveAllDir("Data/Apps/Message/" . $messageParent->MessageId->Value . "/Message/" . $message->MessageId->Value . "/Comment/" . $messageId);

        $request = "DELETE FROM MessageComment WHERE Id = " . $messageId;
        $core->Db->Execute($request);

        return "OK";
    }

    //Met à jour le Subtitle de la conversation
    public static function UpdateLastMessage($core, $discussId, $message) {
        //Format Francais
        $discuss = new MessageConversation($core);
        $discuss->GetById($discussId);
        $discuss->SubTitle->Value = $message;
        $discuss->DateLastMessage->Value = Date::Now(true);
        $discuss->Save();

        //On repasse les message en non read
        $request = "UPDATE MessageUser set Readed = 1 WHERE ConversationId =  " . $discussId . " AND UserId <> " . $core->User->IdEntite;
        $core->Db->Execute($request);
    }

    /**
     * Le membvre à lu la conversation
     */
    public static function SetUserRead($core, $discussId) {
        $request = "Update MessageUser SET Readed = 2 WHERE ConversationId =  " . $discussId . " AND UserId = " . $core->User->IdEntite;
        $core->Db->Execute($request);
    }

    /**
     * Obitent le nombre de message non lu
     */
    public function GetNumberNotRead($core) {
        if ($core->User->IdEntite != "") {
            $request = "SELECT count(du.Id)  as nbNotRead from MessageUser as du 
                        JOIN MessageMessage  as d on d.Id = du.MessageId
                        WHERE du.UserId =" . $core->User->IdEntite . " AND du.Status =1 AND du.Readed = 1 AND d.Status <> 10  ";
            $result = $core->Db->GetLine($request);

            return $result["nbNotRead"];
        } else {
            return 0;
        }
    }

    /*     * *
     * Obtient l'identifiant de la discussion de la page d'accueil
     */

    public static function GetIdByReseau($core, $reseauId) {
        $discuss = new MessageMessage($core);
        $discuss->AddArgument(new Argument("Apps\Message\Entity\MessageMessage", "ReseauId", EQUAL, $reseauId));
        $discuss->AddArgument(new Argument("Apps\Message\Entity\MessageMessage", "Type", EQUAL, 4));

        $disccus = $discuss->GetByArg();

        //Il n'y a pas de disccusion on va la créer
        if (count($disccus[0]) == 0) {
            $discuss = new MessageMessage($core);
            $discuss->Type->Value = 4;
            $discuss->ReseauId->Value = $reseauId;
            $discuss->UserId->Value = $core->User->IdEntite;
            $discuss->DateCreated->Value = Date::Now();
            $discuss->Status->Value = 1;
            $discuss->Save();

            return self::GetIdByReseau($core, $reseauId);
        }
        return $disccus[0];
    }

    /*     * *
     * Ajotue un like sur un message ou un commentaire
     */

    public static function AddLike($core, $entityId, $type) {
        if ($type == "message") {
            $like = new MessageLike($core);
            $like->AddArgument(new Argument("Apps\Message\Entity\MessageLike", "UserId", EQUAL, $core->User->IdEntite));
            $like->AddArgument(new Argument("Apps\Message\Entity\MessageLike", "EntityId", EQUAL, $entityId));
            $like->AddArgument(new Argument("Apps\Message\Entity\MessageLike", "EntityName", EQUAL, $type));

            $likes = $like->GetByArg();

            if (count($likes) == 0) {
                $like = new MessageLike($core);
                $like->UserId->Value = $core->User->IdEntite;
                $like->EntityId->Value = $entityId;
                $like->EntityName->Value = "message";
                $like->DateCreated->Value = Date::Now();
                $like->Save();

                //Envoi d'une notification
                $discusMessage = new MessageMessage($core);
                $discusMessage->GetById($entityId);

                if ($discusMessage->UserId->Value != $core->User->IdEntite) {
                    $notify = DashBoardManager::GetApp("Notify", $core);
                    $notify->AddNotify($core->User->IdEntite, "LikePost", $discusMessage->UserId->Value, "Message", $entityId);
                }
            } else {
                $likes[0]->Delete();
            }
        }

        if ($type == "comment") {
            $like = new MessageLike($core);
            $like->AddArgument(new Argument("Apps\Message\Entity\MessageLike", "UserId", EQUAL, $core->User->IdEntite));
            $like->AddArgument(new Argument("Apps\Message\Entity\MessageLike", "EntityId", EQUAL, $entityId));
            $like->AddArgument(new Argument("Apps\Message\Entity\MessageLike", "EntityName", EQUAL, $type));

            $likes = $like->GetByArg();

            if (count($likes) == 0) {
                $like = new MessageLike($core);
                $like->UserId->Value = $core->User->IdEntite;
                $like->EntityId->Value = $entityId;
                $like->EntityName->Value = "comment";
                $like->DateCreated->Value = Date::Now();
                $like->Save();

                //Envoi d'une notification
                $discusMessage = new MessageComment($core);
                $discusMessage->GetById($entityId);

                if ($discusMessage->UserId->Value != $core->User->IdEntite) {
                    $notify = DashBoardManager::GetApp("Notify", $core);
                    $notify->AddNotify($core->User->IdEntite, "LikeComment", $discusMessage->UserId->Value, "Message", $entityId);
                }
            } else {
                $likes[0]->Delete();
            }
        }
    }

    /**
     * Obtient le nombre de like d'un commentaire
     */
    public function GetNumberLike($core, $entityId) {
        $like = new MessageLike($core);
        $like->AddArgument(new Argument("Apps\Message\Entity\MessageLike", "EntityId", EQUAL, $entityId));
        $like->AddArgument(new Argument("Apps\Message\Entity\MessageLike", "EntityName", EQUAL, "comment"));

        return count($like->GetByArg());
    }

    /*     * *
     * Supprime une conversation 
     */

    public static function DeleteConversation($core, $discussId) {
        //On supprime le répertoire
        //File::RemoveAllDir("Data/Apps/Message/" .$discussId);
        //TROP compliaque de supprimé on la passe en statut = 99
        $discuss = new MessageConversation($core);
        $discuss->GetById($discussId);
        $discuss->Status->Value = 99;
        $discuss->DateClotured->Value = Date::Now();
        $discuss->DateLastMessage->Value = Date::Now(true);
        $discuss->Save();
    }

    /**
     * Archivage du projet
     */
    public static function ArchiveProjet($core, $discussId) {
        $discuss = new MessageMessage($core);
        $discuss->GetById($discussId);
        $discuss->Status->Value = 10;
        $discuss->DateClotured->Value = Date::Now();
        $discuss->Save();
    }

    /*     * *
     * Retourne les identifiants des projets que l'utilisateur Suit ou Participate
     * Sur un réseau
     */

    public static function GetUserProjet($core, $reseauId) {
        //Le projets dont le participe
        $request = "SELECT group_concat(d.Id) as ids from MessageMessage as d
                    JOIN MessageUser as u on u.MessageId = d.Id and u.UserId = " . $core->User->IdEntite . " 
                    AND d.status = 1 AND d.Type = 2
                   AND d.ReseauId=  $reseauId
                ";

        $result = $core->Db->GetLine($request);
        $userParticipate = $result["ids"];

        $request = "SELECT group_concat(d.Id)  as ids from MessageMessage as d
                    JOIN MessageFollow as f on f.MessageId = d.Id and  f.UserId =  " . $core->User->IdEntite . " 
                    AND d.status = 1 AND d.Type = 2
                    AND d.ReseauId=  $reseauId                   
                   ";

        $result = $core->Db->GetLine($request);
        $userFollow = $result["ids"];
        $projetId = "";

        if ($userParticipate != "") {
            $projetId .= $userParticipate;
        }

        if ($userFollow) {
            if ($projetId != "") {
                $projetId .= ",";
            }
            $projetId .= $userFollow;
        }
        return $projetId;
    }

    /*     * *
     * Retrouve une conversation entre deux membre
     * On prend la derniére et là ou il n'y a que deux membre
     * et de type discussion Directe
     * 
     */

    public static function FindConversation($core, $userId, $memberId) {
        $request = "SELECT Distinct(discuss.id) lastId FROM `MessageMessage` as discuss
                    JOIN MessageUser as u on u.ConversationId = discuss.id
                    JOIN MessageUser as m on m.ConversationId = discuss.id
                    WHERE discuss.Type= 1  AND  u.UserId = $userId and m.UserId = $memberId 
                    AND (SELECT count(ConversationId) FROM MessageUser WHERE ConversationId = u.ConversationId) = 2 
                    ORDER BY discuss.id desc limit 0,1";
        $result = $core->Db->GetLine($request);
        return $result['lastId'];
    }

    /**
     * Vérifie qu'il existe une conversation entre les membres sur ce projet
     * @param type $core
     * @param type $projetIdV
     */
    public static function HavePrivateMessageion($core, $projetId) {
        $discuss = new MessageMessage($core);
        return $discuss->Find("ProjetId = " . $projetId);
    }

    /*     * *
     * Si on est ambassadeur ou propriétaire du message on peut le modifier ou le supprimer
     */

    public static function IsUserOrAmbassadeur($core, $messageId, $type) {
        if (MembreHelper::IsAmbassadeur($core, $core->User->IdEntite)) {
            return true;
        }

        if ($type == "message") {
            $message = new MessageMessage($core);
            $message->GetById($messageId);

            if ($message->UserId->Value == $core->User->IdEntite) {
                return true;
            }
        } else if ($type == "comment") {
            $comment = new MessageComment($core);
            $comment->GetById($messageId);

            if ($comment->UserId->Value == $core->User->IdEntite) {
                return true;
            }
        }
        return false;
    }

    /*     * *
     * Ajoute un message dans le fil d'actualité
     */

    public static function AddMessageInActu($core, $reseauCode, $MessageType, $idEntite, $libelle = "") {

        $reseau = new CoopereReseau($core);
        $reseau = $reseau->GetByCode($reseauCode);

        $reseauCode . ":" . $reseau->IdEntite;
        $disccus = self::GetIdByReseau($core, $reseau->IdEntite);

        switch ($MessageType) {
            case "NewProjet":

                $projet = new MessageMessage($core);
                $projet->GetById($idEntite);

                $message = "<div class='actuMessage'> <h3>Un membre vient de proposer un nouveau projet.</h3>";
                $message .= "<img src='" . $projet->GetFirstImageUrl() . "' style='width:100px' />";
                $message .= "<p> Venez le découvrir via ce lien, Projet : <a href='" . $core->GetPath("/" . $reseauCode . "/ParlonsEn/Rencontre/" . $idEntite) . "' > ";
                $message .= $projet->Title->Value . "</a></p></div>";

                break;

            case "NewAnnonce":

                $annonce = new AnnonceAnnonce($core);
                $annonce->GetById($idEntite);

                $message = "<div class='actuMessage'> <h3>Un membre vient de publier une nouvelle annonce.</h3>";
                $message .= "<p> Venez la découvrir via ce lien, Offre : <a href='" . $core->GetPath("/" . $reseauCode . "/Offre/" . $idEntite) . "' > ";
                $message .= $annonce->GetSimpleTitle() . "</a></p></div>";

                break;

            case "NewMembre" :

                $message = "<div class='actuMessage'><h3>Un nouveau membre vient de rejoindre notre réseau.</h3>";
                $message .= "<p> Venez découvrir le détail de ces offres via ce lien : ";
                $message .= "<a  href='" . $core->GetPath("/" . $reseauCode . "/User/" . $idEntite) . "' >Détail des offres </a></p></div>";

                break;

            case "NewAvisResource" :

                $Resource = new \Apps\Resource\Entity\ResourceResource($core);
                $Resource->GetById($idEntite);

                $message = "<div class='actuMessage'> <h3>Un membre vient de donner son avis sur un l'eco geste " . $Resource->Name->Value . " .</h3>";
                $message .= "<p> $libelle </p>";
                $message .= "</div>";

                break;
        }
        self::AddMessage($core, $disccus->IdEntite, $message, 5);
    }
}

  {{form->Open()}}
  {{form->Error()}}
  {{form->Success()}}

  {{form->Render(BlockId)}}
  {{form->Render(imgSrc)}}
 
  <div class='form-group'>
      <label>{{GetCode(Cms.Upload)}}</label>
      {{form->Render(uploadImageCms)}}
  </div>  
  
  <div class=''>
      <h3>{{GetCode(Cms.Library)}}</h3>
      {{Library}}
  </div>
  
  <div class='form-group'>
      <label>{{GetCode(Cms.ImageWidth)}}</label>
      {{form->Render(Width)}}
  </div>  
  
  <div class='center form-group ' >   
       {{form->Render(btnSaveBlockImage)}}
   </div>  
  {{form->Close()}}
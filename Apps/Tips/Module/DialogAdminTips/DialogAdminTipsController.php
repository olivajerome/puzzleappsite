<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Tips\Module\DialogAdminTips;

use Core\Control\Button\Button;
use Core\Controller\Controller;
use Apps\Tips\Entity\TipsTips;
use Apps\Tips\Widget\TipsForm\TipsForm;

class DialogAdminTipsController extends Controller{

    /***
     * Ajout d'un Mooc
     */
    function AddTips($TipId){
      
        $Tip = new TipsTips($this->Core);
        if($TipId != ""){
            $Tip->GetById($TipId);
        }
        
        $TipTip = new TipsForm($this->Core, $Tip);
        $TipTip->Load($Tip);
        
        return $TipTip->Render();
    }
}
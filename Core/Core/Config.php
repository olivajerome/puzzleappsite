<?php

/**
 * PuzzleApp 
 * PuzzleApp.org - The Hybride Framework.
 * Oliva Jérôme
 * GNU Licence
 * */

namespace Core\Core;

class Config {

    /**
     * Properties
     * @var type
     */
    public $File;
    public $Version;
    public $FileName;

    /**
     * 
     * @param type $File
     * @throws \Exception
     */
    function __construct($File = "") {
        //Version
        $this->Version = "2.0.1.1";
        $this->FileName = $File;

        if ($File != "") {
            if ($File == "../.xml") {
                $content = (string) file_get_contents("../Config.xml");
                $this->File = new \SimpleXMLElement($content);
            } else if (file_exists($File)) {
                $content = (string) file_get_contents($File);
                $this->File = simplexml_load_string($content, 'SimpleXMLElement');
            } else {
                throw new \Exception('Config file not found : ' . $File);
            }
        }
    }

    /*     * *
     * Get the config Key
     */

    function GetKey($Key) {
        return $this->File->$Key;
    }

    /*     * *
     * Set key config
     */

    function SetKey($Key, $Value) {
        $this->File->$Key = $Value;
        $this->File->saveXML($this->FileName);
    }
}

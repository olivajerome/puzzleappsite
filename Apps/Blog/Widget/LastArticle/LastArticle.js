var LastArticle = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
LastArticle.Init = function(){
    Event.AddById("btnSave", "click", LastArticle.SaveElement);
};

/***
* Obtient l'id de l'itin�raire courant 
*/
LastArticle.GetId = function(){
    return Form.GetId("LastArticleForm");
};

/*
* Sauvegare l'itineraire
*/
LastArticle.SaveElement = function(e){
   
    if(Form.IsValid("LastArticleForm"))
    {

    var data = "Class=LastArticle&Methode=SaveElement&App=LastArticle";
        data +=  Form.Serialize("LastArticleForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.statut == "Error"){
                Form.RenderError("LastArticleForm", data.message);
            } else{

                Form.SetId("LastArticleForm", data.data.Id);
            }
        });
    }
};


<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Ide\Module\Projet;

use Apps\Ide\Helper\ProjetHelper;
use Apps\Ide\Helper\ToolHelper;
use Apps\Ide\Ide;
use Apps\Ide\Helper\ElementHelper;
use Core\Action\AjaxAction\AjaxAction;
use Core\Block\Block;
use Core\Control\Button\Button;
use Core\Control\Libelle\Libelle;
use Core\Control\TabStrip\TabStrip;
use Core\Control\TextBox\TextBox;
use Core\Controller\AdministratorController;
use Core\Core\Request;
use Apps\Ide\Entity\IdeProjet;

use Apps\EeApp\Entity\EeAppApp;
use Apps\EeApp\Entity\EeAppUser;


class ProjetController extends AdministratorController {

    /**
     * Création du projet
     */
    function CreateProjet($data) {

        //Creation du projet
        if (ProjetHelper::CreateProjet($this->Core, $data["Name"], $data["Description"])) {
            return true;
        } else {
            return $this->Core->GetCode("Ide.ProjetExist");
        }
    }

    /**
     * Charge les outils et le projets
     */
    function LoadProjet() {
        $this->SetTemplate(__DIR__ . "/View/Projet.tpl");

        //Recuperation du projet
        $projet = Request::GetPost("Projet");

        //TabStrip de l'editeur
        $tsEditor = new TabStrip("tsEditor", "Ide");
        $tsEditor->AddTab("information", new Libelle("Votre projet"));

        //Passage des parametres à la vue
        $this->AddParameters(array('!lstTools' => ToolHelper::GetTool($this->Core, $projet),
            '!lstElement' => ElementHelper::GetAll($this->Core, $projet),
            '!lstEditor' => $tsEditor->Show(),
        ));

        return $this->Render();
    }

    /*     * *
     * Remove a projet
     */

    function RemoveProjet($projetId) {
        $projet = new IdeProjet($this->Core);
        $projet->GetById($projetId);
        $projet->Delete();
    }
    
    
    /***
     * Ajoute le projet au menu
     */
    function AddProjetToMenu($projetName){
        
        $projet = new IdeProjet($this->Core);
        $projet = $projet->GetByName($projetName);
        
        $app = new EeAppApp($this->Core);
        $app->Name->Value = $projet->Name->Value;
        $app->Description->Value = $projet->Description->Value;
        $app->CategoryId->Value  = 1;
        $app->Save();
        
        $appId = $this->Core->Db->GetInsertedId();
        $appUser = new EeAppUser($this->Core);
        $appUser->UserId->Value = $this->Core->User->IdEntite;
        $appUser->AppId->Value = $appId;
        $appUser->Save();
    }
}

?>

<?php

/**
 * Module d'accueil
 * */

namespace Apps\Cms\Module\Admin;

use Core\Core\Request;
use Core\Core\Response;

use Core\Control\Button\Button;
use Core\Controller\AdministratorController;
use Core\View\View;
use Core\View\ElementView;

use Apps\Cms\Entity\CmsPage;
use Apps\Cms\Entity\CmsContainer;
use Apps\Cms\Entity\CmsBlock;
use Apps\Cms\Entity\CmsBlockItem;
use Apps\Cms\Helper\PageHelper;
use Apps\Cms\Helper\BlockHelper;
use Apps\Cms\Helper\CmsHelper;
use Apps\Cms\Helper\ImportHelper;
use Apps\Cms\Widget\LayoutEditor\LayoutEditor;
use Apps\Cms\Widget\PageEditor\PageEditor;
use Apps\Cms\Widget\StyleEditor\StyleEditor;
use Apps\Cms\Widget\ExportImportEditor\ExportImportEditor;
use Apps\Cms\Widget\ThemeEditor\ThemeEditor;

class AdminController extends AdministratorController {

    /**
     * Creation
     */
    function Create() {
        
    }

    /**
     * Initialisation
     */
    function Init() {
        
    }

    /**
     * Affichage du module
     */
    function Show($all = true) {
        $view = new View(__DIR__ . "/View/index.tpl", $this->Core);

        $pages = new CmsPage($this->Core);
        $view->AddElement(new ElementView("Pages", $pages->Find("1 =1 ")));

        return $view->Render();
    }

    /**
     * Save a container
     */
    function SaveContainer($data){
        return BlockHelper::SaveContainer($this->Core, $data);
    }

    /***
     * Remove a container
     */
    function RemoveContainer($containerId){
        return BlockHelper::RemoveContainer($this->Core, $containerId);
    }

    /***
     * Upload Image to the cms Library
     */
    function UplodImageToLibrary($data){
        return  CmsHelper::UplodImageToLibrary($this->Core, Request::GetPost());
    }

    /***
     * Save Block
     */
    function SaveBlock($data){
        return BlockHelper::SaveBlock($this->Core, $data);
     }

    /***
     * Update content Block
     */
    function UpdateContentBlock($data){
        return BlockHelper::UpdateContentBlock($this->Core, $data);
     }

     /***
      * Remove a block
      */
    function RemoveBlock($blockId){
        return BlockHelper::RemoveBlock($this->Core, $blockId);
    }
    /**
     * Add Item to a  block
     */
    function SaveBlockItem($data){
        return BlockHelper::SaveBlockItem($this->Core, $data);
    }

    /***
     * Remove item to a block
     */
    function RemoveItemMenu($itemId){
        return BlockHelper::RemoveItemMenu($this->Core, $itemId);
    }
   
    /**
     * Save image block
     */
    function SaveBlockImage($data){
        return BlockHelper::SaveBlockImage($this->Core, $data);
    }
   
    /***
     * Save the Style
     */
    function SaveStyle($css){
        return CmsHelper::SaveStyle($this->Core, $css);
    }

    /**
     * Save a page
     *  */   
    function SavePage($data){
        $title = $data["Title"];

        if ($title != "") {
            PageHelper::Save($this->Core,
                    1,
                    $data["Id"],
                    $data["Name"],
                    $data["Title"],
                    $data["Description"]
            );

            return Response::Success(array("Message"=> $this->Core->GetCode("Cms.PageSaved")));
        } else {
             return Response::Error(array("Message"=> $this->Core->GetCode("Cms.ErrorPage")));
        }
    }

    /***
     * Remove a page
     */
    function RemovePage($pageId){
        return CmsHelper::RemovePage($this->Core, $pageId);
    }  

    /***
     * Export all pages
     */
    function ExportPages(){

        $page = new CmsPage($this->Core);
        $pages = $page->GetAll();
        $pageArray = array();

        //Add Header, Body, Footer Container    
        $pages[] = new CmsPage($this->Core); 

        foreach($pages as $page){

            $currentPage = $page->ToArray();

            $container = new CmsContainer($this->Core);
            if($page->IdEntite){
                $containers = $container->Find("PageId=" .$page->IdEntite); 
            } else{
                $containers = $container->Find("PageId is null"); 
            }

            foreach($containers as $container){

                $currentContainer = $container->ToArray();

                $block = new CmsBlock($this->Core);
                $blocks = $block->Find("ParentId=".$container->IdEntite);     

                foreach($blocks as $block){

                    $currentBlock = $block->ToArray();

                    $item = new CmsBlockItem($this->Core);
                    $items = $item->Find("BlockId=".$block->IdEntite);

                    foreach($items as $item){
                        $currentBlock["Items"][] = $item->ToArray(); 
                    }

                    $currentContainer["Blocks"][] = $currentBlock;
                }

                $currentPage["Containers"][] = $currentContainer;
            }

            $pageArray[] = $currentPage;
        }

        //Force download
        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary"); 
        header("Content-disposition: attachment; filename=ExportCms.json"); 

        return json_encode($pageArray);
    }

    /**
     * Import all page and container
     *  
     */
    function ImportPages($core, $data){
        
       $file = $data["dvUpload-UploadfileToUpload"];
       $data = json_decode(file_get_contents($file));
       
       $message ="Debut du traitement";
         
       foreach($data as $page){
       
           $message .= "<br>--------------"; 
           $message .= ImportHelper::ImportPage($this->Core, $page);
       }
    }

    /*     * *
     * Obtient les widget
     */

     public function GetWidget($type = "", $params = "") {

        if ($type == "") {
            $type = Request::GetPost("type");
        }
       
        switch ($type) {
            case "AdminDashboard" :
                $widget = new AdminDashBoard($this->Core);
                break;
             case "ThemeEditor" :
                $widget = new ThemeEditor($this->Core);
                break;
            case "LayoutEditor" :
                $widget = new LayoutEditor($this->Core);
                break;
            case "StyleEditor" :
                $widget = new StyleEditor($this->Core);
                break;
            case "PageEditor" :
                $widget = new PageEditor($this->Core, Request::GetPost("id"));
            break;
            case "ExportImportEditor" :
                $widget = new ExportImportEditor($this->Core, Request::GetPost("id"));
            break;
            case "BlockContent" :
                $widget = new BlockContent($this->Core);
                break;
            case "PageContent" :
                $widget = new PageContent($this->Core);
                break;
             case "LoginMenu" :
                $widget = new LoginMenu($this->Core);
                break;
            case "ContactForm" :
                $widget = new ContactForm($this->Core);
                break;
        }

        return $widget->Render($params);
    }
}
<?php

namespace Core\Control\RangeBox;

use Core\Control\IControl;
use Core\Control\Control;

class RangeBox extends Control implements IControl {

    //Constructeur
    function __construct($name, $score = 0, $min = 0, $max = 100) {
        //Version
        $this->Version = "2.0.0.0";
        $this->Id = $name;
        $this->Name = $name;
        $this->Value = $score;
        $this->Min = $min;
        $this->Max = $max;
        $this->Enabled = true;
    }

    public function Show() {

        $html = "<div class='clignotant clickable' >
                    <div class='inputSelector'>
                        <input  class='rangeBox'  id='" . $this->Id . "' name='" . $this->Id . "' type='range' value='" . $this->Value . "' min='" . $this->Min . "' max='" . $this->Max . "' " . (!$this->Enabled ? "Disabled" : "") . " />
                        <div id='s" . $this->Id . "' class='selector'>
                            <b class='brangeBox' id='b" . $this->Id . "'>" . $this->Value . "</b>
                        </div>
                        <div class='progressBarrangeBox' id='progressBar" . $this->Id . "'></div>
                    </div>
                </div>";

        return $html;
    }
}
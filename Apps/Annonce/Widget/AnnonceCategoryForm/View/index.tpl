{{form->Open()}}
{{form->Error()}}
{{form->Success()}}

{{form->Render(Id)}}

<div>
    <label>{{GetCode(Annonce.Name)}}</label> 
    {{form->Render(Name)}}
</div>

<div>
    <label>{{GetCode(Annonce.Description)}}</label> 
    {{form->Render(Description)}}
</div>


<div>
    <label>{{GetCode(Annonce.CategoryImage)}}</label> 
    {{form->Render(Upload)}}
</div>

<div>
    {{form->RenderImage()}}
</div>
<div class='center marginTop' >   
    {{form->Render(btnSave)}}
</div>  

{{form->Close()}}
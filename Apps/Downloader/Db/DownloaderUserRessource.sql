CREATE TABLE IF NOT EXISTS `DownloaderUserRessource` ( 
`Id` int(11) NOT NULL AUTO_INCREMENT, 
`Name` VARCHAR(200)  NOT NULL,
`Code` VARCHAR(200)  NOT NULL,
`Description` TEXT  NOT NULL,
`UserId` INT  NOT NULL,
PRIMARY KEY (`Id`),
CONSTRAINT `ee_user_DownloaderUserRessource` FOREIGN KEY (`UserId`) REFERENCES `ee_user`(`Id`)
) ENGINE=InnoDB  DEFAULT CHARACTER SET `utf8`; 
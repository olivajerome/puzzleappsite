<div class='message' id='{{messageId}}'>
    
        
        {{moreAction}}

        <div class='title'> 
            
            <div class="imgProfilMedium" style="background-image: url({{image}}) "></div>
               
            <div>
                <b>{{pseudo}}</b> <span>{{day}}</span> {{projet}}
            </div>    
        </div> 

        <div class='messageUser'  > 
         
          <div class='text'>{{text}}</div>

            <div class='buttons' >
              <div class='infoLike'>
                  <span class='likes'>{{userLike}} <div style=' margin-left: 30px; color: #000;'> {{nbLike}} j'aime(s)</div></span> - <span class ='nbComment' style='color: #000;' >{{nbComment}} commentaire(s)</span>
              </div>
              <div class='action'>
                <i data-type='message'  class='fa fa-heart  nbLike likeMessage bgkBtnLikeComment'  title='Aimer la conversation'></i>
                <i class='fa fa-comment btnComment bgkBtnLikeComment' title='Commenter' ></i>
              </div>      
            </div> 
           <div class='lstComment'>{{comments}}</div>
           
          {{if isConnected == true}}
                <div class='commentBox'>
                    
                    <div class="imgProfilSmall" style="background-image: url({{userImage}}) "></div>
                  
                    <div style='width:86%'>
                        <textarea  style='height:35px'  class='form-control tbComment' type='text' placeholder='Quelque chose à ajouter' class='form-control' ></textarea>
                    </div>
                </div>
          {{/if isConnected == true}}
              
        </div>


    </div>

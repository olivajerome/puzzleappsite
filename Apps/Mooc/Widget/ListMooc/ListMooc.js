var ListMooc = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
ListMooc.Init = function(){
    Event.AddById("btnSave", "click", ListMooc.SaveElement);
};

/***
* Obtient l'id de l'itin�raire courant 
*/
ListMooc.GetId = function(){
    return Form.GetId("ListMoocForm");
};

/*
* Sauvegare l'itineraire
*/
ListMooc.SaveElement = function(e){
   
    if(Form.IsValid("ListMoocForm"))
    {

    var data = "Class=ListMooc&Methode=SaveElement&App=ListMooc";
        data +=  Form.Serialize("ListMoocForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.statut == "Error"){
                Form.RenderError("ListMoocForm", data.message);
            } else{

                Form.SetId("ListMoocForm", data.data.Id);
            }
        });
    }
};


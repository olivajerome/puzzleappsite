<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Lang;

use Core\App\Application;
use Core\Core\Core;
use Core\Core\Request;

use Apps\Lang\Module\Element\ElementController;
use Apps\Lang\Helper\ElementHelper;
use Apps\Lang\Module\Widget\WidgetController;
use Core\Entity\Langs\Langs;

class Lang extends Application
{
    /**
     * Auteur et version
     * */
    public $Author = 'Eemmys';
    public $Version = '2.1.0.0';
    public static $Directory = "../Apps/Lang";

    /**
     * Constructeur
     * */
     function __construct($core)
     {
        parent::__construct($core, "Lang");
        $this->Core = Core::getInstance();
     }

     /**
      * Execution de l'application
      */
     function Run()
     {
        echo parent::RunApp($this->Core, "Lang", "Lang");
     }

    /**
     * Charge les elemernts multilangue courants
     */
    public function LoadElement()
    {
        $elementController = new ElementController($this->Core);
        echo $elementController->LoadElement(Request::GetPost("page"), Request::GetPost("keyWord"), Request::GetPost("keyValue"), "Rerfresh");
    }

    /**
     * Supprime un element multilangue
     */
    public function RemoveElement()
    {
        ElementHelper::RemoveElement($this->Core, Request::GetPost("idElement"));
    }

     /**
     * Supprime un element multilangue
     */
    public function UpdateElement()
    {
        ElementHelper::UpdateElement($this->Core, Request::GetPost("idElement"), Request::GetPost("value"));
    }

    /*
     * Edit un element
     */
    public function EditElement()
    {
        $elementController = new ElementController($this->Core);
        echo $elementController->EditElement(Request::GetPost("elementId"));
    }

    /*
     * Sauvegarde un element
     */
    public function SaveElement()
    {
       ElementHelper::UpdateElement($this->Core, Request::GetPost("elementId"), Request::GetPost("tbLibelle"));

       echo "<span class='sucess'>".$this->Core->GetCode("SaveOk")."</span>";
    }
    
    /***
     * Ajoute une langue
     */
    public function AddLang(){
        
        $langName = Request::GetPost("langName");
        $lang = new \Core\Entity\Langs\Langs($this->Core);
        $lang->Name->Value = $langName;
    
        $code = iconv('UTF-8', 'ASCII//TRANSLIT', $langName);
        $code = strtoupper(substr($code, 0, 1)). strtolower(substr($code, 1,1));
        
        $lang->Code->Value = $code;
        $lang->Save();
    }
    
    /**
     * Obtient les différents widget
     */
    public function GetWidget($type, $params){

        switch ($type) {
            case "AdminDashboard" :
                $widgetController = new WidgetController($this->Core);
                return $widgetController->Show($type, $params);
   
                break;
            case "LangSelector" :
                $widget = new \Apps\Lang\Widget\LangSelector\LangSelector($this->Core);
                break;
        }

        return $widget->Render();

    }


    /*     * **
     * List of AddAble Widget on CMS
     */

     public function GetListWidget() {
        return array(array("Name" => "LangSelector",
                "Description" => $this->Core->GetCode("Lang.DescriptionLangSelector")),
        );
    }

    /***
     * Set the current Language
     */
    public function SetLanguage(){
        
        $lang = new Langs($this->Core);
        $lang->GetById(Request::GetPost("langId"));
        $this->Core->SetLang($lang->Code->Value);
        
        var_dump( $_COOKIE);
    }
}
?>
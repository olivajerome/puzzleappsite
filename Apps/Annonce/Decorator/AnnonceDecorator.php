<?php

namespace Apps\Annonce\Decorator;

use Apps\Annonce\Helper\AnnonceHelper;
use Core\View\View;
use Core\View\ElementView;

class AnnonceDecorator {

    public function __construct($core) {
        $this->Core = $core;
    }

    /***
     * Shox the imahe of an annonce
     */
    public function RenderImages($Annonce) {

        $images = $Annonce->GetImages();

        $view = "<div class='images'>";

        $i = 0;
        foreach ($images as $image) {

            $regex = "/full\.png|thumb\.png/";

            if (preg_match($regex, $image) === 0) {

                if ($i == 0) {
                    $width = "100%";
                } else {
                    $width = "50%";
                }

                $view .= "<img style='width:$width' src='$image' />";

                $i++;
            }
        }
        $view .= "</div>";

        return $view;
    }
    
    /***
     * Redner all reponse of a annocne
     */
    function RenderResponses($Annonce){
        
        $view = new View(__DIR__ . "/View/responses.tpl", $this->Core);
      
        $appMessage = \Apps\EeApp\Helper\AppHelper::HaveApp($this->Core, "Message");    
        $view->AddElement(new ElementView("ShowContactMember", ($appMessage !== false)  ));

        $responses = AnnonceHelper::GetResponses($this->Core, $Annonce->IdEntite);
        $view->AddElement(new ElementView("Responses", $responses));

        return $view->Render(); 
    }
}
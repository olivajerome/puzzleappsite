<?php

header('Access-Control-Allow-Origin: *');  //I have also tried the * wildcard and get the same response
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000000');
header('Access-Control-Allow-Headers: Access-Control-Allow-Origin, Content-Type, Content-Range, Content-Disposition, Content-Description');

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */
include("../environment.php");
include("../autoload.php");
include("../Core/Runner.php");
        
$env = GetEnvironnement();

Runner::Run("Base", $env, false);


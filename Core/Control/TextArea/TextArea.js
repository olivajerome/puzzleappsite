

function FitToContent(id, maxHeight)
{
    var text = id && id.style ? id : document.getElementById(id);
    if (!text)
        return;

    var adjustedHeight = text.clientHeight;
    if (!maxHeight || maxHeight > adjustedHeight)
    {
        adjustedHeight = Math.max(text.scrollHeight, adjustedHeight);
        if (maxHeight)
            adjustedHeight = Math.min(maxHeight, adjustedHeight);
        if (adjustedHeight > text.clientHeight)
            text.style.height = 10 + adjustedHeight + "px";
    }
    ;
}
;






var TextArea = function () {};

    
/***
 * Add tools to format texte 
 * @param {type} e
 * @returns {undefined}
 */
TextArea.ShowToolContentBlock = function (control, callBack, params) {

    if(control == null){ 
        return;
    }
           
    let parent = control.parentNode;
   
    if (control.type == "textarea")
    {
        let txtEditor = document.createElement("div");
        txtEditor.id = "txtEditor";
        txtEditor.setAttribute("contentEditable", true);
        
        if(params != undefined){
            
            switch(params.type){
                case "full" :
                    txtEditor.style.height = "80vh";
                    txtEditor.style.border = "1px solid grey";
                    txtEditor.style.borderRadius = "8px;";
                    break;
            }
            
        }
        
        
         if(control.innerHTML != ""){
            txtEditor.innerHTML = control.value;
        } else{
            txtEditor.innerHTML = "<p id='placeHolderStart'>Entrez votre texte</p>";
        }
        
        txtEditor.focus();

        parent.insertBefore(txtEditor, control);

        txtEditor.focus();

        Event.AddById("txtEditor", "keydown", function(){
            placeHolderStart = Dom.GetById("placeHolderStart");
            
            if(placeHolderStart){
                placeHolderStart.parentNode.removeChild(placeHolderStart);
            }
        });

        Event.AddById("txtEditor", "keyup", function () {
            control.innerHTML = txtEditor.innerHTML;
        });
        
        control.style.display = "none";
    }

    /* while(parent.className.indexOf("blockContent") == - 1){
     parent = parent.parentNode;
     }
     */
    TextArea.currentBlock = parent;


    TextArea.CreateTextTool(parent);

    Event.AddByClass("textTool", "mousedown", function (e) {

        e.preventDefault();
        e.stopImmediatePropagation();
        let value = "";

        if (e.srcElement.dataset.value != undefined) {
            value = e.srcElement.dataset.value;
        }

        //TODO A REMPLACER
        document.execCommand(e.srcElement.dataset.tool, false, value);
    });

    Event.AddByClass("selectTool", "change", function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();

        document.execCommand(e.srcElement.dataset.tool, false, e.srcElement.value);
    });

    Event.AddById('checkIcon', "click", function (e) {
        TextArea.UpdateContentBlock(e), callBack;
    });

    Event.AddById("libraryIcon", "click", function (e) {
        let block = e.srcElement;

        Dialog.open('', {"title": Dashboard.GetCode("Cms.Addmage"),
            "app": "Cms",
            "class": "DialogCms",
            "method": "AddImage",
            "type": "left",
            "params": block.parentNode.parentNode.id
        });
    });
};


/**
 * Update the content of a block
 * @returns {undefined}
 */
TextArea.UpdateContentBlock = function (e, callBack) {

    let TextTools = document.getElementById("texTools");
    TextTools.parentNode.removeChild(TextTools);

    callBack();
};

/***
 * Create tools to format text
 * @param {type} node
 * @returns {undefined}
 */
TextArea.CreateTextTool = function (node) {

    if (document.getElementById("texTools") == null) {
        let textTool = document.createElement("div");
        textTool.id = "texTools";
        textTool.style.background = "white";
      
        let view = "<i class='textTool' data-tool='Bold'>B</i>";
        view += "<i class='textTool' data-tool='italic'>I</i>";
        view += "<i class='textTool' data-tool='underline'>U</i>";
        view += "<i class='textTool' data-value='<h1>' data-tool='formatBlock'>H1</i>";
        view += "<i class='textTool' data-value='<h2>' data-tool='formatBlock'>H2</i>";
        view += "<i class='textTool' data-value='<h3>' data-tool='formatBlock'>H3</i>";
        view += "<i class='textTool' data-value='<code>' data-tool='formatBlock'>Code</i>";
        // view += "<i id='libraryIcon' class='textTool fa fa-edit' data-value='<img>' data-tool='imageBlock'>img</i>";

        view += TextArea.CreateFontSizeTool();
        view += TextArea.CreateForeColorTool();

        view += "<i id='checkIcon' title='" + Language.GetCode("Cms.ValidContentText") + "' class='fa fa-check' ></i>";

        textTool.innerHTML = view;

        node.prepend(textTool);
    }
};

/***
 * Select color
 * @returns {String}
 */
TextArea.CreateForeColorTool = function () {

    let view = "<select class='selectTool' data-tool='foreColor' >";

    let colors = ["black", "red", "green", "purple", "violet"];

    for (let i = 0; i < colors.length; i++) {
        view += "<option value='" + colors[i] + "' >" + colors[i] + "</option>";
    }

    view += "</select>";

    return view;
};

/***
 * Font size
 * @returns {String}
 */
TextArea.CreateFontSizeTool = function () {

    let view = "<select class='selectTool' data-tool='fontsize' >";

    for (let i = 0; i < 20; i++) {
        view += "<option value='" + i + "px' >" + i + "</option>";
    }

    view += "</select>";

    return view;
};

TextArea.GetRichContent = function(){
    let txtEditor = Dom.GetById("txtEditor");
    return txtEditor.innerHTML;
};


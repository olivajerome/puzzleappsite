var PluginForm = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
PluginForm.Init = function(){
    Event.AddById("btnSave", "click", PluginForm.SaveElement);
};

/***
* Obtient l'id de l'itin�raire courant 
*/
PluginForm.GetId = function(){
    return Form.GetId("PluginFormForm");
};

/*
* Sauvegare l'itineraire
*/
PluginForm.SaveElement = function(e){
   
    if(Form.IsValid("PluginFormForm"))
    {

    var data = "Class=EeApp&Methode=SavePlugin&App=EeApp";
        data +=  Form.Serialize("PluginFormForm");

        Request.Post("Ajax.php", data).then(data => {

            Dialog.Close();
        });
    }
};


<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Apps\Comunity\Widget\MemberProjetWidget;

use Core\View\View;
use Core\View\ElementView;
use Apps\Comunity\Helper\ComunityHelper;

class MemberProjetWidget
{
    
    /***
     * 
     */
    function __construct($core, $projetId, $Comunity)
    {
        $this->Core= $core;
        $this->ProjetId = $projetId;
        $this->Comunity = $Comunity;
    }
    
    /**
     * Gere le rendu HTML du wodget
     */
    function Render()
    {
       $view = new View(__DIR__."/View/index.tpl", $this->Core);
       
       $view->AddElement(new ElementView("Users",$this->Comunity->GetProjetMember()));
       
       return $view->Render();
    }
}
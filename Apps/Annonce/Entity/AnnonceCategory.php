<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Annonce\Entity;

use Core\Entity\Entity\Entity;
use Core\Entity\Entity\Property;
use Core\Entity\Entity\EntityProperty;

class AnnonceCategory extends Entity {

    //Constructeur
    function __construct($core) {
        //Version
        $this->Version = "2.0.0.0";

        //Nom de la table 
        $this->Core = $core;
        $this->TableName = "AnnonceCategory";
        $this->Alias = "AnnonceCategory";

        $this->Name = new Property("Name", "Name", TEXTAREA, false, $this->Alias);
        $this->Code = new Property("Code", "Code", TEXTAREA, false, $this->Alias);
        $this->Description = new Property("Description", "Description", TEXTBOX, false, $this->Alias);

         //Image directory    
        $this->DirectoryImage = "Data/Apps/Annonce/Category/";
        
        
        //Creation de l entité 
        $this->Create();
    }
    
    /***
     * Get Number Annonce of the category
     */
    function GetNumberAnnonce(){
        
        $annonce = new AnnonceAnnonce($this->Core);
        return  $annonce->GetCount("CategoryId=" . $this->IdEntite);
    }
    
    /***
     * GetThumbImage 
     */
    function GetThumbImage(){
        
        if(file_exists($this->DirectoryImage . $this->IdEntite ."/thumb.png")){
            $view = "<img src='".$this->DirectoryImage . $this->IdEntite ."/thumb.png' />";
             return  $view; 
        } 
        
        return "";
    }
    
    /***
     * Get Image Path
     */
    function GetImagePath(){
        
        if(file_exists($this->DirectoryImage . $this->IdEntite ."/full.png")){
             return $this->DirectoryImage . $this->IdEntite ."/full.png" ;
        } 
        
        return $this->Core->GetPath("/images/nophoto.png");
    }
}

?>
var IdeProjetForm = function(){};

/***
 * Initialise le formulaire
 * @returns {undefined}
 */
IdeProjetForm.Init = function(){
    Event.AddById("btnSaveProjet", "click", IdeProjetForm.SaveProjet);
};

/***
* Obtient l'id de l'itin�raire courant 
*/
IdeProjetForm.GetId = function(){
    return Form.GetId("IdeProjetFormForm");
};

/*
* Sauvegare l'itineraire
*/
IdeProjetForm.SaveProjet = function(e){
   
    if(Form.IsValid("IdeProjetFormForm"))
    {

    var data = "Class=Ide&Methode=CreateProjet&App=Ide";
        data +=  Form.Serialize("IdeProjetFormForm");

        Request.Post("Ajax.php", data).then(data => {

            data = JSON.parse(data);

            if(data.status == "error"){
                 Form.RenderError("IdeProjetFormForm", data.data.message);
            } else{

                Form.SetId("IdeProjetFormForm", data.data.Id);
                Dialog.Close();
                Dashboard.StartApp("","Ide", "");
            }
        });
    }
};


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function Viewer() {

    /***
     * Ajout les events sur le control
     */
    this.AddOnControl = function (control)
    {
        var currentViewer = this;

        if (typeof (control) == "string") {
            control = document.getElementById(control);
        }

        Dashboard.AddEvent(control, "click", function () {

            currentViewer.ShowOverlay();
            Viewer.currentIndex = 0;

            var container = control;

            dialog = document.createElement('div');
            dialog.id = "viewerContainer";
            dialog.style.position = 'fixed';
            dialog.style.width = "800px";
            // dialog.style.height="500px";
            dialog.style.overflow = "auto";
            dialog.style.border = '1px solid grey';
            dialog.style.padding = "5px";
            dialog.style.zIndex = 9999;
            dialog.style.left = "25%";
            dialog.style.top = "10%";

            dialog.style.backgroundColor = "white";


            //Récuperation des images
            imgs = container.getElementsByTagName("img");
            Viewer.imgs = imgs;

            var viewer = "<div class='thumb'>";
            viewer += "<div class='naviguate'>";

            if (imgs.length > 1) {
                viewer += "<div id='viewerLeft' class='left' style='display:none'><i class='fa fa-arrow-left'></i></div>";
            }
            viewer += "<div id='viewerCenter' class='center'><img id='imgCenter' src='" + imgs[0].src + "'> </div>";

            if (imgs.length > 1) {
                viewer += "<div id='viewerRight' class='right'><i class='fa fa-arrow-right'></i></div>";
            }
            viewer += "</div>";


            viewer += "</div>";

            if (imgs.length > 1) {
                viewer += "<div class='mini'>";

                for (var i = 0; i < imgs.length; i++)
                {
                    viewer += "<img class='imgContainerMini' src='" + imgs[i].src + "'>";
                }

                viewer += "</div>";

            }


            dialog.innerHTML = "<div style='text-align:right;width:100%'><i class='fa fa-remove fa-times' alt='' title='Fermer' onclick='Viewer.Close(this)'></i></div>";
            dialog.innerHTML += viewer;

            document.body.appendChild(dialog);
            var imgCenter = document.getElementById("imgCenter");

            Dashboard.AddEventById("viewerRight", "click", function (e) {

                var viewerLeft = document.getElementById("viewerLeft");
                viewerLeft.style.display = "";

                if (Viewer.currentIndex < Viewer.imgs.length - 1) {

                    Viewer.currentIndex += 1;

                    imgCenter.src = Viewer.imgs[Viewer.currentIndex].src;

                    if (Viewer.currentIndex == Viewer.imgs.length - 1) {

                        e.srcElement.parentNode.style.display = "none";
                    }
                }
            }
            );

            Dashboard.AddEventById("viewerLeft", "click", function (e) {

                var viewerRight = document.getElementById("viewerRight");
                viewerRight.style.display = "";

                if (Viewer.currentIndex > 0) {

                    Viewer.currentIndex -= 1;


                    imgCenter.src = Viewer.imgs[Viewer.currentIndex].src;

                    if (Viewer.currentIndex == 0) {
                        e.srcElement.parentNode.style.display = "none";
                    }
                }
            }
            );


            Dashboard.AddEventByClass("imgContainerMini", "click", function (e) {
                imgCenter.src = e.srcElement.src;
            });

        })
    };

    /**
     * Show Overlay
     * @param {type} e
     * @returns {undefined}
     */
    this.ShowOverlay = function ()
    {
        var dialog = document.createElement('div');
        dialog.id = "overlay",
                dialog.className = "overlay";

        document.body.appendChild(dialog);
    };



}
;

Viewer.Close = function (element)
{
    Viewer.HideOverlay();

    document.body.removeChild(element.parentNode.parentNode);


};

Viewer.HideOverlay = function ()
{
    var dialog = document.getElementById('overlay');
    document.body.removeChild(dialog);
};



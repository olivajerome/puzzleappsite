<?php

/*
 * PuzzleApp
 * Webemyos
 * Jérôme Oliva
 * GNU Licence
 */

namespace Apps\Profil;

use Apps\Profil\Module\Admin\AdminController;
use Apps\Profil\Helper\CompetenceHelper;
use Apps\Profil\Helper\UserHelper;
use Apps\Profil\Module\Competence\CompetenceController;
use Apps\Profil\Module\Information\InformationController;
use Apps\Profil\Module\Member\MemberController;
use Apps\Profil\Module\Front\FrontController;

use Core\App\Application;
use Core\Control\Button\Button;
use Core\Core\Request;
use Core\Core\Response;


use Core\Utility\File\File;
use Core\Utility\ImageHelper\ImageHelper;



class Profil extends Application {

    /**
     * Auteur et version
     * */
    public $Author = 'DashBoardManager';
    public $Version = '2.1.1.0';
    public static $Directory = "../Apps/Profil";

    /**
     * Constructeur
     * */
    function Profil($core = "") {
        $this->Core = Core::getInstance();
        $this->Core = $core;
    }

      /**
     * Set the Public Routes
     */
     public function GetRoute()
     {
        $this->Route->SetPublic(array("User"));
       
        return $this->Route;
     }
     
     
    /**
     * Execution de l'application
     */
    function Run() {
        echo parent::RunApp($this->Core, "Profil", "Profil");
    }

    /*     * *
     * Run member Controller
     */

    function RunMember() {
        $memberController = new MemberController($this->Core);
        return $memberController->Index();
    }

    /***
      * Execute action after install
      */
     function PostInstall() {
        \Apps\EeApp\Helper\AppHelper::UpdateAppParams($this->Core,
                "Profil",
                $this->Version,
                "Gestionnaire des utilisateurs",
                1,
                1
        );
        
        \Apps\EeApp\Helper\AppHelper::CreateDataDir("Profil");
    }
    
    /**
     * Charge les information de base du profil
     */
    public function LoadInformation($showAll = true) {
        $informationController = new InformationController($this->Core);
        echo $informationController->Load($showAll);
    }

    /**
     * Enregistre les informations du profil
     */
    public function SaveInformation() {
     
        UserHelper::Update($this->Core, Request::GetPosts());
        return Response::Success(array("Message" => $this->Core->GetCode("Profil.ProfilUpdated")));
    }

    /**
     * Sauvegare les images de presentation
     */
    function DoUploadFile($idElement, $tmpFileName, $fileName, $action) {
        //Ajout de l'image dans le repertoire correspondant
        $directory = "Data/Apps/Profil/";
        File::CreateDirectory($directory);

        switch ($action) {
            case "SaveImageProfil":


                $directory = "Data/Tmp";

                move_uploaded_file($tmpFileName, $directory."/".$idElement.".jpg");
                echo "OK";

                
                break;
        }
    }

    /**
     * Charge les compétences du profil
     */
    public function LoadCompetence() {
        $competenceController = new CompetenceController($this->Core);
        echo $competenceController->Load();
    }

    /*
     * Obtient les competences
     */

    public function GetCompetence() {
        $competenceController = new CompetenceController($this->Core);
        return $competenceController->GetCompetence();
    }

    //Enregistre les competences
    public function SaveCompetence() {
        echo UserHelper::SaveCompetence($this->Core, $this->Core->User->IdEntite, Request::GetPost("competenceId"));

        echo $this->LoadCompetence();
    }

    /**
     * Récupere l'image du profil
     */
    public function GetProfil($user = false, $cssClass = "", $addInvitation = false) {
        $html = "<div class='$cssClass'>";

        $informationController = new InformationController($this->Core);
        return $informationController->GetProfil($user, $cssClass, $addInvitation);
    }

    /*
     * Load Admin Section
     */

    function LoadAdmin() {
        $adminController = new AdminController($this->Core);
        echo $adminController->Show();
    }

    
      /***
     * Get the widget
     */
    public function GetWidget($type = "", $params = "") {
   
        switch ($type) {
            case "AdminDashboard" :
                $widget = new \Apps\Profil\Widget\AdminDashBoard\AdminDashBoard($this->Core);
                break;
        }
        
        return $widget->Render();
    }
    
    /*
     * PopIn d'ajout de categorie
     */

    function ShowAddCategory() {
        $adminController = new AdminController($this->Core);
        echo $adminController->ShowAddCategory(Request::GetPost("entityId"));
    }

    /*
     * Get The Categorie Tab for Refresh
     */

    function GetTabCategory() {
        $adminController = new AdminController($this->Core);
        echo $adminController->GetTabCategory();
    }

    /*
     * PopIn d'ajout de comptence
     */

    function ShowAddCompetence() {
        $adminController = new AdminController($this->Core);
        echo $adminController->ShowAddCompetence(Request::GetPost("entityId"));
    }

    /*
     * Get The Competence Tab for Refresh
     */

    function GetTabCompetence() {
        $adminController = new AdminController($this->Core);
        echo $adminController->GetTabCompetence();
    }

    /*
     * Détail of a profil
     */

    function Display() {
        $html = "<h1>" . $this->Core->GetCode("Profil.DetailProfil") . "</h1>";

        $user = new User($this->Core);
        $user->GetById($this->IdEntity);

        $html .= "<div class='row'><div class='col-md-4'><h2 class='fa fa-user'>&nbsp;" . $this->Core->GetCode("Profil.Information") . "</h2>" . $this->GetProfil($user) . "</div>";

        $competences = CompetenceHelper::GetByUser($this->Core, $this->IdEntity);

        if (count($competences) > 0) {
            $html .= "<div class='col-md-8' ><h2 class='fa fa-align-left' >&nbsp;" . $this->Core->GetCode("Profil.Competence") . "</h2>";

            $categorie = "";

            // $html .= "<ul>";

            foreach ($competences as $competence) {
                //  $html .=  $competence->Competence->Value->Category->Value->Name->Value;

                if ($competence["categoryName"] != $categorie) {
                    $categorie = $competence["categoryName"];

                    if ($categorie != "")
                        $html .= "</ul>";

                    $html .= "<li><h3 class='width100 titleBlue'>" . $categorie . "</h3><ul><li>" . $competence["CompetenceName"] . "</li>";
                } else {
                    $html .= "<li><span class='width100'></span>" . $competence["CompetenceName"] . "</li>";
                }
            }

            $html .= "</ul></div>";
            $html .= "</div>";
        }
        return $html;
    }

    /**
     * Affiche le détail d'un utilisateur
     */
    public function User($userId) {
         $this->Core->MasterView->Set("Title", $this->Core->GetCode("Profil.UserProfil"));
        $this->Core->MasterView->Set("Description", "Venez découvir le profil de notre membre");
  
        $frontController = new FrontController($this->Core);
        return $frontController->User($userId);
    }
}
